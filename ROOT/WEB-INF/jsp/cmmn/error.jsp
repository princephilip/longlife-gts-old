<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
/**
 *<pre>
 * Statements
 *</pre>
 
 * @Class Name : error.jsp
 * @Descriptio	  에러처리화면
 * @Modification Information
 * @
 * @  수정일        		 수정자                   수정내용
 * @ -------    --------    ---------------------------
 * @ 2013.03.03    박영찬        최초 생성
 * @
 */
%> 

<%@ page isErrorPage="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Error 발생 /></title>
<link type="text/css" rel="stylesheet" href="<c:url value='/Theme/css/error.css'/>">
</head>
<body>
<!-- 전체 레이어 시작 -->
<div id="wrap">
	<!-- header 시작 -->
	<div id="header"><%@ include file="/WEB-INF/jsp/cmmn/header.jsp" %></div>
	<!-- //header 끝 -->	
	<!-- container 시작 -->
		<!-- content 시작 -->
		<div id="content_pop">
		작업중 오류가 발생하였습니다.<br>
		시스템 관리자에게 문의하시기 바랍니다.<br>
		<%= exception %>

	</div>
	<!-- //container 끝-->
	<!-- footer 시작 -->
	<div id="footer"><%@ include file="/WEB-INF/jsp/cmmn/footer.jsp" %></div>
	<!-- //footer 끝 -->
</div>
<!--// 전체 레이어 끝 -->
</body>
</html>

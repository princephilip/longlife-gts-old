<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div id="logoarea" style="padding-top: 10px;">
	<h1><a href="javascript:history.back()"><font color=black><img src="<c:url value='/Theme/images/main/logo.png'/>"  alt="go main" /></font></a></h1>
</div>
<div id="project_title" style="padding-top: 10px;">
	<span class="maintitle"><spring:message code="main.egov" /></span>
	<strong>엑스인터넷정보</strong>
	<!-- strong><spring:message code="main.rte" /></strong -->
</div>

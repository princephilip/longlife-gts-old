<%--
  Class Name : PopupNotice.jsp
  Description : 공지사항팝업
  Modification Information

      수정일        		 수정자                   수정내용
  ------=--    --------    ---------------------------
  20150420     	박영찬          		최초 생성
--%>
<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<base target="_self" />
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
<link href="<c:url value='/css/popup.css' />" rel="stylesheet" type="text/css">
<title>공지사항팝업 게시물</title>
<script type="text/javaScript" src="/Mighty/jQuery/js/jquery-1.11.2.min.js"></script>
<script type="text/javaScript" language="javascript">
/* ********************************************************
 * 쿠키설정
 ******************************************************** */
function fnSetCookiePopup( name, value, expiredays ) {
	  var todayDate = new Date();
	  todayDate.setDate( todayDate.getDate() + expiredays );
	  document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";"
}
/* ********************************************************
* 체크버튼 클릭시
******************************************************** */
function fnPopupCheck() {
	fnSetCookiePopup( "${popupId}", "done" , 365);
	window.close();
}

function fn_set_imgover(a_obj) {
	a_obj.src="../../images/bbs/"+a_obj.id+"_over.gif";
};

function fn_set_imgout(a_obj) {
	a_obj.src="../../images/bbs/"+a_obj.id+".gif";
};

</script>
</head>
<body>
<body class="pu_bg01">
<div id="popup">
	<div class="pop_tit">
		<h1>
			<img src="../../images/bbs/pop_logo.jpg" alt="자산관리시스템" />
		</h1>
	</div>
	<div id="con">
		<div class="con">
			<h2><span style="color:#ff0000;"><b><c:out value="${result.nttSj}"/></b></span></h2>
			<table class="bbs_view">
       		<tbody>
       			<tr>
           			<td style="border-left: 0px; border-right: 0px;">
						<span id="contents_cn" style="overflow-x:hidden;overflow-y:auto;width:100%;height:430px">
							<div id="bbs_cn">
								<c:out value="${result.nttCn}" escapeXml="false" />
							</div>
                           <hr size=1 noshade style='text-align:center; width:98%; border:1 dotted brown;'/><br/>
                        	<b>※ 첨부파일</b><br/><br/>&nbsp;&nbsp;-&nbsp;
              					<c:import url="/fms/selectFileInfs.do" charEncoding="utf-8">
									<c:param name="param_atchFileId" value="${result.atchFileId}" />
								</c:import>
                        </span>
					</td>
				</tr>
			</tbody>
			</table>
			<div class="bbs_btn">
	            <p class="left">
				<c:if test="${stopVewAt eq 'Y'}">
	                <input type="checkbox" id="chkPopup" onclick="javascript:fnPopupCheck();"  title="다음부터창열지않기체크"/>
	                <label for="chkCloseOneDay" style="cursor:pointer">다음부터 이 창을 열지 않음 </label>
				</c:if>
	            </p>
	            <p class="right">
	          		<input type="image" id="x_close" onclick="javascript:window.close();" src="<c:url value='/images/bbs/x_close.gif' />"  onmouseover="fn_set_imgover(this)" onmouseout="fn_set_imgout(this)">
	            </p>
	        </div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#bbs_cn').each(function(){
	    var $this = $(this);
	    var t = $this.text();
	    $this.html(t.replace('&lt','<').replace('&gt', '>'));
	});
</script>
</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%
 /**
  * @Class Name : EgovNoticeReply.jsp
  * @Description : 게시물 답글 생성 화면
  * @Modification Information
  * @
  * @  수정일      수정자            수정내용
  * @ -------        --------    ---------------------------
  * @ 2009.03.24   이삼섭          최초 생성
  *
  *  @author 공통서비스 개발팀 이삼섭
  *  @since 2009.03.24
  *  @version 1.0
  *  @see
  *
  */
%>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
<link type="text/css" charset="utf-8" href="/Mighty/3rd/jQuery/css/Aristo/Aristo.X-1.1.1.css" rel="stylesheet" media="all" />
<link href="<c:url value='/Theme/css/bbs.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='/Theme/css/button.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='/Theme/css/egovbbsTemplate.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='/Mighty/css/mighty-2.0.1.css' />" rel="stylesheet" type="text/css">
	
<script>var _CPATH = '<%=request.getContextPath()%>';</script>
<script type="text/javascript" src="<c:url value='/Mighty/egov/js/bbs/EgovBBSMng.js' />" ></script>
<script type="text/javaScript" src="<c:url value='/Mighty/3rd/ckeditor/ckeditor.js'/>"></script>  
<script type="text/javaScript" src="<c:url value='/Mighty/3rd/ckeditor/config.js'/>"></script> 
<script type="text/javaScript" src="<c:url value='/Mighty/3rd/ckeditor/adapters/jquery.js'/>"></script>
<script type="text/javascript" src="<c:url value='/Mighty/egov/js/cmm/EgovMultiFile.js'/>" ></script>
<script type="text/javascript" src="<c:url value='/Mighty/egov/js/cal/EgovCalPopup.js'/>" ></script>
<script type="text/javascript" src="<c:url value="/com/validator.do"/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/3rd/jQuery/js/jquery-1.11.2.min.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/3rd/jQuery/js/jquery.ui.datepicker-ko.min.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/js/old/mighty-1.2.7.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/js/old/mighty-win-1.2.7.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/js/old/mighty-mdi-1.2.7.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/3rd/jQuery/js/jquery-ui-1.11.3.custom/jquery-ui.min.js'/>"></script>
	
<validator:javascript formName="board" staticJavascript="false" xhtml="true" cdata="false"/>
<c:if test="${anonymous == 'true'}"><c:set var="prefix" value="/anonymous"/></c:if>
<script type="text/javascript">

	function fn_egov_validateForm(obj) {
		return true;
	}

	function fn_SetDate(a_val,a_obj) {
		$("#"+a_obj.id).val(a_val.replace(/\-/g,''));
	}
	
	function fn_egov_regist_notice() {

		document.board.submit();

		if (!validateBoard(document.board)){
			return;
		}

		if (confirm('<spring:message code="common.regist.msg" />')) {
			document.board.action = "<c:url value='/bbs${prefix}/replyBoardArticle.do'/>";
			document.board.submit();
		}
	}

	function fn_egov_select_noticeList() {
		document.board.action = "<c:url value='/bbs${prefix}/selectBoardList.do'/>";
		document.board.submit();
	}
	
	function makeFileAttachment(){
	<c:if test="${bdMstr.fileAtchPosblAt == 'Y'}">
		 var maxFileNum = document.board.posblAtchFileNumber.value;
	     if (maxFileNum==null || maxFileNum=="") {
	    	 maxFileNum = 3;
	     }
		 var multi_selector = new MultiSelector( document.getElementById( 'egovComFileList' ), maxFileNum );
		 multi_selector.addElement( document.getElementById( 'egovComFileUploader' ) );
	</c:if>	
	}
</script>
<style type="text/css">
  .noStyle {background:ButtonFace; BORDER-TOP:0px; BORDER-bottom:0px; BORDER-left:0px; BORDER-right:0px;}
  .noStyle th{background:ButtonFace; padding-left:0px;padding-right:0px}
  .noStyle td{background:ButtonFace; padding-left:0px;padding-right:0px}
</style>
<title><c:out value='${bdMstr.bbsNm}'/> - 답글쓰기</title>

<style type="text/css">
	h1 {font-size:12px;}
	caption {visibility:hidden; font-size:0; height:0; margin:0; padding:0; line-height:0;}
</style>

</head>
<!-- body onload="javascript:editor_generate('nttCn');"-->
<body onLoad="document.board.nttSj.focus(); makeFileAttachment();">

<form:form commandName="board" name="board" method="post" enctype="multipart/form-data" >
<input type="hidden" name="replyAt" value="Y" />
<input type="hidden" name="pageIndex"  value="<c:out value='${searchVO.pageIndex}'/>"/>
<input type="hidden" name="nttId" value="<c:out value='${searchVO.nttId}'/>" />
<input type="hidden" name="parnts" value="<c:out value='${searchVO.parnts}'/>" />
<input type="hidden" name="sortOrdr" value="<c:out value='${searchVO.sortOrdr}'/>" />
<input type="hidden" name="replyLc" value="<c:out value='${searchVO.replyLc}'/>" />
<input type="hidden" name="bbsId" value="<c:out value='${bdMstr.bbsId}'/>" />
<input type="hidden" name="companyCode" value="<c:out value='${bdMstr.companyCode}'/>" />
<input type="hidden" name="bbsAttrbCode" value="<c:out value='${bdMstr.bbsAttrbCode}'/>" />
<input type="hidden" name="bbsTyCode" value="<c:out value='${bdMstr.bbsTyCode}'/>" />
<input type="hidden" name="replyPosblAt" value="<c:out value='${bdMstr.replyPosblAt}'/>" />
<input type="hidden" name="fileAtchPosblAt" value="<c:out value='${bdMstr.fileAtchPosblAt}'/>" />
<input type="hidden" name="posblAtchFileNumber" value="<c:out value='${bdMstr.posblAtchFileNumber}'/>" />
<input type="hidden" name="posblAtchFileSize" value="<c:out value='${bdMstr.posblAtchFileSize}'/>" />
<input type="hidden" name="tmplatId" value="<c:out value='${bdMstr.tmplatId}'/>" />

<input type="hidden" name="cal_url" value="<c:url value='/cal/EgovNormalCalPopup.do'/>" />

<c:if test="${anonymous != 'true'}">
<input type="hidden" name="ntcrNm" value="dummy">	<!-- validator 처리를 위해 지정 -->
<input type="hidden" name="password" value="dummy">	<!-- validator 처리를 위해 지정 -->
</c:if>

<c:if test="${bdMstr.bbsAttrbCode != 'BBSA01'}">
   <input name="ntceBgnde" type="hidden" value="10000101">
   <input name="ntceEndde" type="hidden" value="99991231">
</c:if>

<div id="border">

	<table width="100%" cellpadding="8" class="table-search" border="0">
	 <tr>
	  <td width="100%"class="title_left">
	   <img src="<c:url value='/Theme/images/bbs/tit_icon.gif' />" width="3" height="16" hspace="3" align="middle" alt="제목버튼이미지">
	   &nbsp;<c:out value='${bdMstr.bbsNm}'/> - 답글쓰기</td>
	 </tr>
	</table>
	<table width="100%" border="0" cellpadding="0" cellspacing="1" class="generalTable">
	  <tr>
	    <th width="20%" height="23" class="emphasisRight" nowrap >*<spring:message code="cop.nttSj" />
	    <td width="80%" nowrap colspan="3">
	      <input name="nttSj" type="text" size="60" value="RE: <c:out value='${result.nttSj}'/>"  class="detail_input" maxlength="60" title="답글제목입력">
	      <br/><form:errors path="nttSj" />
	    </td>
	  </tr>
	  <tr>
	    <th height="23" class="emphasisRight" >*<spring:message code="cop.nttCn" />
	    <td colspan="3">
	      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="noStyle">
	      <tr><td>
	      <textarea id="nttCn" name="nttCn" class="textarea" rows="20"  style="width:99%"  title="답글내용입력"></textarea>
	      <form:errors path="nttCn" />
	      </td></tr>
	      </table>
	    </td>
	  </tr>
	  <c:if test="${bdMstr.bbsAttrbCode == 'BBSA01'}">
		  <tr>
		    <th height="23" class="emphasisRight">*<spring:message code="cop.noticeTerm" />
		    <td colspan="3">
		      <input id="ntceBgnde" name="ntceBgnde" type="hidden">
		      <input name="ntceBgndeView" type="text" size="10" value=""  onchange="javascript:fn_SetDate(this.value,ntceBgnde)" readOnly class="detail_date" title="게시시작일자입력">
		      ~
		      <input id="ntceEndde" name="ntceEndde" type="hidden"  />
		      <input name="ntceEnddeView" type="text" size="10" value=""  onchange="javascript:fn_SetDate(this.value,ntceEndde)" readOnly class="detail_date"	 title="게시종료일자입력">
		    </td>
		  </tr>
	  </c:if>
	  <c:if test="${anonymous == 'true'}">
		  <tr>
		    <th height="23" class="emphasisRight">*작성자
		    <td colspan="3">
		      <input name="ntcrNm" type="text" size="20" value=""  maxlength="10" title="작성자이름입력">
		      <br/><form:errors path="ntcrNm" />
		    </td>
		  </tr>
		  <tr>
		    <th height="23" class="emphasisRight">*<spring:message code="cop.password" />
		    <td colspan="3">
		      <input name="password" type="password" size="20" value="" class="detail_input" maxlength="20" title="비밀번호입력">
		    </td>
		  </tr>
	  </c:if>
	  <c:if test="${bdMstr.fileAtchPosblAt == 'Y'}">
	  <tr>
	    <th height="28">파일첨부</th>
	    <td colspan="2">
            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
			    <tr>
			        <td><input name="file_1" id="egovComFileUploader" type="file" title="첨부파일명입력"/></td>
			    </tr>
			    <tr>
			        <td>
			        	<div id="egovComFileList"></div>
			        </td>
			    </tr>
   	        </table>
	    </td>
	  </tr>
   
	  </c:if>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td height="5"></td>
	  </tr>
	</table>
	<div align="center" style='height:30px;'>
	<table border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
	 <c:if test="${bdMstr.authFlag == 'Y' || bdMstr.bbsTyCode == 'BBST01'}">
     <td align='center'>
 				<span style="width:100%;height:100%;vertical-align:middle;">
					<ul id="buttons">
						<li><a class="save"  id="x_save"  href="javascript:;"onclick="fn_egov_regist_notice();" 		title="데이타를 등록합니다."></a></li>
						<li><a class="close" id="x_list" 	href="javascript:;"onclick="fn_egov_select_noticeList();" title="현재창을 닫고 목록으로 돌아갑니다."></a></li>
					</ul>		
				</span>
			</td>
      <td width="5"></td>
   </c:if>
	</tr>
	</table>
	</div>
</div>
</form:form>
<script>
	var editor = null;
	editor = CKEDITOR.replace('nttCn', {
		fullpage: true,
		allowedContent: true,
		language: 'ko',
		height: 350
	});
	$('.detail_date').datepicker( {
	    changeMonth: true,
	    changeYear: true,
	    dayNames: ['월요일', '화요일', '수요일', '목요일', '금요일', '토요일', '일요일'],         
	    dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],          
	    monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'],         
	    monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
	    nextText: '다음 달',
	    prevText: '이전 달',
	    showButtonPanel: true,          
	    currentText: '오늘 날짜',          
	    closeText: '닫기',
	    dateFormat: "yy-mm-dd",
	    showMonthAfterYear: true,   
	    yearSuffix: ' 년 ',   
	    monthSuffix: ' 월',   
	    showOtherMonths: true, // 나머지 날짜도 화면에 표시   
	    selectOtherMonths: true
	});
</script>
</body>
</html>

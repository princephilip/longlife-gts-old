<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>   

<c:set var="ImgUrl" value="/images/bbs/"/>
<jsp:useBean id="now" class="java.util.Date"/>

<%
 /**
  * @Class Name : EgovGalleryList.jsp
  * @Description : 이미지갤러리 목록화면
  * @Modification Information
  * @
  * @  수정일      수정자            수정내용
  * @ -------        --------    ---------------------------
  * @ 2009.03.19   이삼섭          최초 생성
  * @ 2011.11.11   이기하          익명게시판 검색시 작성자 제거
  *
  *  @author 공통서비스 개발팀 이삼섭
  *  @since 2009.03.19
  *  @version 1.0
  *  @see
  *
  */
%>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
<link href="<c:url value='theme/css/bbs.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='theme/css/button.css' />" rel="stylesheet" type="text/css">
<link type="text/css" charset="utf-8" href="/Mighty/3rd/jQuery/css/Aristo/Aristo.X-1.1.1.css" rel="stylesheet" media="all" />
<link href="<c:url value='${brdMstrVO.tmplatCours}' />" rel="stylesheet" type="text/css">
<c:if test="${anonymous == 'true'}"><c:set var="prefix" value="/anonymous"/></c:if>
<script type="text/javascript" src="<c:url value='/js/bbs/EgovBBSMng.js' />" ></script>
<script type="text/javascript" src="<c:url value='/js/cal/EgovCalPopup.js'/>" ></script>
<script type="text/javaScript" src="<c:url value='/Mighty/jQuery/js/jquery-1.11.2.min.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/jQuery/js/jquery.ui.datepicker-ko.min.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/js/mighty-1.2.7.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/js/mighty-win-1.2.7.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/js/mighty-mdi-1.2.7.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/jQuery/js/jquery-ui-1.11.3.custom/jquery-ui.min.js'/>"></script>
<c:choose>
<c:when test="${preview == 'true'}">
<script type="text/javascript">
<!--
	function press(event) {
	}

	function fn_egov_addNotice() {
	}

	function fn_egov_select_noticeList(pageNo) {
	}

	function fn_egov_inqire_notice(nttId, bbsId) {
	}
//-->
</script>
</c:when>
<c:otherwise>
<script type="text/javascript">
<!--
	function press(event) {
		if (event.keyCode==13) {
			fn_egov_select_noticeList('1');
		}
	}

	function fn_egov_addNotice() {
		document.frm.action = "<c:url value='/bbs${prefix}/addGalleryArticle.do'/>";
		document.frm.submit();
	}

	function fn_egov_select_noticeList(pageNo) {
		document.frm.pageIndex.value = pageNo;
		document.frm.action = "<c:url value='/bbs${prefix}/selectGalleryList.do'/>";
		document.frm.submit();
	}

	function fn_egov_inqire_notice(i, nttId, bbsId) {
		document.subForm.nttId.value = nttId;
		document.subForm.bbsId.value = bbsId;
		document.subForm.action = "<c:url value='/bbs${prefix}/selectGallerydArticle.do'/>";
		document.subForm.submit();
	}
	
	function fn_egov_window_close(a_win) {
		var _IsModal = (window.dialogArguments?true:false);
		var _IsPopup = (window.opener?true:false);
		if(_IsModal || _IsPopup) {
			window.close();
		} else {
			a_win.mytop._X.MDI_Close(a_win.mytop._CurMenuIndex);	
		}
	}

    function fn_egov_downFile(atchFileId, fileSn){
    	window.open("<c:url value='/fms/FileDown.do?atchFileId="+atchFileId+"&fileSn="+fileSn+"'/>");
    }
    
//-->
</script>
</c:otherwise>
</c:choose>
<title><c:out value="${brdMstrVO.bbsNm}"/></title>

<style type="text/css">
	h1 {font-size:12px;}
	caption {visibility:hidden; font-size:0; height:0; margin:0; padding:0; line-height:0;}
</style>

</head>
<body>

<div id="border">

	<form name="frm" action ="<c:url value='/bbs${prefix}/selectGalleryList.do'/>" method="post">
	<input type="hidden" name="companyCode" value="<c:out value='${boardVO.companyCode}'/>" />
	<input type="hidden" name="bbsId" value="<c:out value='${boardVO.bbsId}'/>" />
	<input type="hidden" name="nttId"  value="0" />
	<input type="hidden" name="bbsTyCode" value="<c:out value='${brdMstrVO.bbsTyCode}'/>" />
	<input type="hidden" name="bbsAttrbCode" value="<c:out value='${brdMstrVO.bbsAttrbCode}'/>" />
	<input type="hidden" name="authFlag" value="<c:out value='${brdMstrVO.authFlag}'/>" />
	<input name="pageIndex" type="hidden" value="<c:out value='${searchVO.pageIndex}'/>"/>
	<input type="hidden" name="cal_url" value="<c:url value='/cal/EgovNormalCalPopup.do'/>" />
	<input type="hidden" name="searchAuth" value="<c:out value='${searchVO.searchAuth}'/>"/>
	<input type="hidden" name="searchFileSn" value="<c:out value='0'/>"/>
	
	<table width="100%" cellpadding="8" class="table-search" border="0">
	 <tr>
	  <td width="30%" class="title_left">
	  	<spring:message code="cop.regisTerm" />
	    <input name="searchBgnDe" class="search_date" type="text" size="10" maxlength="10" value="<c:out value="${searchVO.searchBgnDe}"/>" title="조회시작일자 입력">
		 ~
		<input name="searchEndDe" class="search_date" type="text" size="10" maxlength="10" value="<c:out value="${searchVO.searchEndDe}"/>" title="조회종료일자 입력">
	  </td>
	  <td width="40%" class="title_left">
	   	<label for="searchDataType">
	   		<spring:message code="cop.searchCond" />
	   	</label>
	  	<select name="searchCnd" class="select" title="검색조건선택">
			   <option value="0" <c:if test="${searchVO.searchCnd == '0'}">selected="selected"</c:if> >제목</option>
			   <option value="1" <c:if test="${searchVO.searchCnd == '1'}">selected="selected"</c:if> >내용</option>
			   <c:if test="${anonymous != 'true'}">
			   <option value="2" <c:if test="${searchVO.searchCnd == '2'}">selected="selected"</c:if> >작성자</option>
			   </c:if>
		</select>
	    <input name="searchWrd" type="text" size="20" value='<c:out value="${searchVO.searchWrd}"/>' maxlength="20" onkeypress="press(event);" title="검색어 입력">&nbsp
	    <c:if test="${brdMstrVO.bbsTyCode == 'BBST05'}">
		   	<label for="searchDataType">
		   		<spring:message code="cop.dataTyeCode" />
		   	</label>
		    <select name="searchDataType">
			  <option value="">선택</option>
		      <c:forEach var="items" items="${attrbList}" varStatus="status">
		        <option value="${items.code}" <c:if test="${items.code eq searchVO.searchDataType}">selected</c:if>>${items.codeNm}</option>
		      </c:forEach>
		    </select>
	    </c:if>
	    
	  </td>
	  <th width="10%">
	   <table border="0" cellspacing="0" cellpadding="0">
	    <tr>
	      <c:if test="${brdMstrVO.authFlag == 'Y'}">
	        <td>
	        	<span class="bbsbutton">
	        		<input type="image" id="x_retrieve" onclick="fn_egov_select_noticeList('1'); return false;" src="<c:url value='theme/images/bbs/x_retrieve.gif' />"  onmouseover="fn_set_imgover(this)" onmouseout="fn_set_imgout(this)">
	        	</span>
	        </td>
	      </c:if>
	     	<td>
	     		<span class="bbsbutton">
	         		<input type="image" id="x_regist" onclick="javascript:fn_egov_addNotice(); return false;" src="<c:url value='theme/images/bbs/x_regist.gif' />"  onmouseover="fn_set_imgover(this)" onmouseout="fn_set_imgout(this)">
	     		</span>
	   		</td>
	     	<td>
	     		<span class="bbsbutton">
	         		<input type="image" id="x_close" onclick="fn_egov_window_close(window); return false;" src="<c:url value='theme/images/bbs/x_close.gif' />"  onmouseover="fn_set_imgover(this)" onmouseout="fn_set_imgout(this)">
	     		</span>
	     	</td>
	    </tr>
	   </table>	
	  </th>
	 </tr>
	 </table>
	 </form>
	
	<table width="100%" cellpadding="4" class="imageTable">
	<tr>
	<td>
	 <fmt:formatDate value="${now}" pattern="yyyy-MM-dd" var="now"/>
	  <c:set var="_addCnt" value="${'1'}"/>
		<div id="contents">
			 <c:forEach var="result" items="${resultList}" varStatus="status">
				<c:if test="${_addCnt=='1'}">
					<ul class="photolist">		 
				</c:if>
				<li>
				    <dl>
						<form name="subForm" method="post" action="<c:url value='/bbs${prefix}/selectGallerydArticle.do'/>">
							<input type="hidden" name="companyCode" value="<c:out value='${result.companyCode}'/>" />
							<input type="hidden" name="bbsId" value="<c:out value='${result.bbsId}'/>" />
							<input type="hidden" name="nttId"  value="<c:out value="${result.nttId}"/>" />
							<input type="hidden" name="bbsTyCode" value="<c:out value='${brdMstrVO.bbsTyCode}'/>" />
							<input type="hidden" name="bbsAttrbCode" value="<c:out value='${brdMstrVO.bbsAttrbCode}'/>" />
							<input type="hidden" name="authFlag" value="<c:out value='${brdMstrVO.authFlag}'/>" />
							<input name="pageIndex" type="hidden" value="<c:out value='${searchVO.pageIndex}'/>"/>
				        <dt>
							<input  TYPE="IMAGE" src='<c:url value="/fms/getImage.do?atchFileId=${result.atchFileId}"/>' name="Submit" value="Submit"  align="absmiddle" width="166px" height="117px" border="0">
			        	</dt>
				        <dd>
				            <ul>
				                <li class="lietc">제목 : ${result.nttSj}</li>
							    <c:if test="${anonymous != 'true'}">
							    	<li class="lietc">작성자 : ${result.frstRegisterNm}</li>
							    </c:if>
								<li class="lietc">등록일 : ${result.frstRegisterPnttm}</li>
				            </ul>
				        </dd>
				    	</form>				                
				    </dl>
				</li>
				<c:set var="_addCnt" value="${_addCnt+1}"/>				
				<c:if test="${_addCnt>'4'}">
					</ul>
					<c:set var="_addCnt" value="${'1'}"/>
				</c:if>
			 </c:forEach>
			<c:if test="${_addCnt=='2' or _addCnt=='3' or _addCnt=='4'}">
				</ul>
			</c:if>
			<c:remove var="_addCnt"/>
		</div>
		</td>
		</tr>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td height="10"></td>
	  </tr>
	</table>
	
	<div align="center" class="PageNumber">
		<ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fn_egov_select_noticeList" />
	</div>
</div>
<script>
$('.search_date').datepicker( {
    changeMonth: true,
    changeYear: true,
    dayNames: ['월요일', '화요일', '수요일', '목요일', '금요일', '토요일', '일요일'],         
    dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],          
    monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'],         
    monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
    nextText: '다음 달',
    prevText: '이전 달',
    showButtonPanel: true,          
    currentText: '오늘 날짜',          
    closeText: '닫기',
    dateFormat: "yy-mm-dd",
    showMonthAfterYear: true,   
    yearSuffix: ' 년 ',   
    monthSuffix: ' 월',   
    showOtherMonths: true, // 나머지 날짜도 화면에 표시   
    selectOtherMonths: true
});
</script>
</body>
</html>

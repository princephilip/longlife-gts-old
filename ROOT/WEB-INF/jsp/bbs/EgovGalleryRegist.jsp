<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%
 /**
  * @Class Name : EgovGalleryRegist.jsp
  * @Description :  갤러리  생성 화면
  * @Modification Information
  * @
  * @  수정일      수정자            수정내용
  * @ -------        --------    ---------------------------
  * @ 2009.03.24   이삼섭          최초 생성
  *   2011.09.15   서준식          유효기간 시작일이 종료일보다 빠른지 체크하는 로직 추가
  *  @author 공통서비스 개발팀 이삼섭
  *  @since 2009.03.24
  *  @version 1.0
  *  @see
  *
  */
%>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
<link type="text/css" charset="utf-8" href="/Mighty/3rd/jQuery/css/Aristo/Aristo.X-1.1.1.css" rel="stylesheet" media="all" />
<link href="<c:url value='theme/css/bbs.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='theme/css/button.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='${brdMstrVO.tmplatCours}' />" rel="stylesheet" type="text/css">
<script>var _CPATH = '<%=request.getContextPath()%>';</script>
<script type="text/javascript" src="<c:url value='/js/bbs/EgovBBSMng.js' />"></script>
<script type="text/javaScript" src="<c:url value='/ckeditor/ckeditor.js'/>"></script>  
<script type="text/javaScript" src="<c:url value='/ckeditor/config.js'/>"></script> 
<script type="text/javaScript" src="<c:url value='/ckeditor/adapters/jquery.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/cmm/EgovMultiFile.js'/>" ></script>
<script type="text/javascript" src="<c:url value='/js/cal/EgovCalPopup.js'/>" ></script>
<script type="text/javascript" src="<c:url value="/com/validator.do"/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/jQuery/js/jquery-1.11.2.min.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/jQuery/js/jquery.ui.datepicker-ko.min.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/js/mighty-1.2.7.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/js/mighty-win-1.2.7.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/js/mighty-mdi-1.2.7.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/jQuery/js/jquery-ui-1.11.3.custom/jquery-ui.min.js'/>"></script>
<validator:javascript formName="board" staticJavascript="false" xhtml="true" cdata="false"/>
<c:if test="${anonymous == 'true'}"><c:set var="prefix" value="/anonymous"/></c:if>
<script type="text/javascript">
	function fn_egov_validateForm(obj) {
		return true;
	}

	function fn_SetDate(a_val,a_obj) {
		$("#"+a_obj.id).val(a_val.replace(/\-/g,''));
	}
	
	function fn_egov_regist_notice() {
		//첨부파일 확장자 체크(2015.04.18 KYY)
		var afiles = $(':file');
		for(var i=0; i<afiles.length; i++) {
	    if(/.*\.(jsp)|(asp)|(aspx)|(do)|(exe)|(php)|(cgi)$/.test(afiles[i].value.toLowerCase())){
	 	   	alert(afiles[i].value + "\r\n허용되지 않는 형식의 첨부파일입니다.");
				return;
	    }
		}
	
		document.board.submit();


		var ntceBgnde = document.getElementById("ntceBgnde").value;
		var ntceEndde = document.getElementById("ntceEndde").value;


		if(ntceBgnde > ntceEndde){
			alert("게시기간 종료일이 시작일보다 빠릅니다.");
			return;
		}

		if (!validateBoard(document.board)){
			return;
		}

		if (confirm('<spring:message code="common.regist.msg" />')) {
			//document.board.onsubmit();
			document.board.action = "<c:url value='/bbs${prefix}/insertGalleryArticle.do'/>";
			document.board.submit();
		}
	}

	function fn_egov_select_noticeList() {
		document.board.action = "<c:url value='/bbs${prefix}/selectGalleryList.do'/>";
		document.board.submit();
	}
	function makeFileAttachment(){
		 <c:if test="${bdMstr.fileAtchPosblAt == 'Y'}">
			 var maxFileNum = document.board.posblAtchFileNumber.value;
		     if(maxFileNum==null || maxFileNum==""){
		    	 maxFileNum = 3;
		     }
			 var multi_selector = new MultiSelector( document.getElementById( 'egovComFileList' ), maxFileNum );
			 multi_selector.addElement( document.getElementById( 'egovComFileUploader' ) );
		</c:if>	
	}
	
</script>
<style type="text/css">
.noStyle {background:ButtonFace; BORDER-TOP:0px; BORDER-bottom:0px; BORDER-left:0px; BORDER-right:0px;}
  .noStyle th{background:ButtonFace; padding-left:0px;padding-right:0px}
  .noStyle td{background:ButtonFace; padding-left:0px;padding-right:0px}
</style>
<title><c:out value='${bdMstr.bbsNm}'/> - 게시글쓰기</title>

<style type="text/css">
	h1 {font-size:12px;}
	caption {visibility:hidden; font-size:0; height:0; margin:0; padding:0; line-height:0;}
</style>

</head>
<body onLoad="document.board.nttSj.focus(); makeFileAttachment();">

<form:form commandName="board" name="board" method="post" enctype="multipart/form-data" >

<input name="pageIndex" type="hidden" value="<c:out value='${searchVO.pageIndex}'/>"/>
<input type="hidden" name="companyCode" value="<c:out value='${bdMstr.companyCode}'/>" />
<input type="hidden" name="bbsId" value="<c:out value='${bdMstr.bbsId}'/>" />
<input type="hidden" name="bbsAttrbCode" value="<c:out value='${bdMstr.bbsAttrbCode}'/>" />
<input type="hidden" name="bbsTyCode" value="<c:out value='${bdMstr.bbsTyCode}'/>" />
<input type="hidden" name="replyPosblAt" value="<c:out value='${bdMstr.replyPosblAt}'/>" />
<input type="hidden" name="fileAtchPosblAt" value="<c:out value='${bdMstr.fileAtchPosblAt}'/>" />
<input type="hidden" name="posblAtchFileNumber" value="<c:out value='${bdMstr.posblAtchFileNumber}'/>" />
<input type="hidden" name="posblAtchFileSize" value="<c:out value='${bdMstr.posblAtchFileSize}'/>" />
<input type="hidden" name="tmplatId" value="<c:out value='${bdMstr.tmplatId}'/>" />

<input type="hidden" name="cal_url" value="<c:url value='/cal/EgovNormalCalPopup.do'/>" />
<input type="hidden" name="authFlag" value="<c:out value='${bdMstr.authFlag}'/>" />

<c:if test="${anonymous != 'true'}">
<input type="hidden" name="ntcrNm" value="dummy">	<!-- validator 처리를 위해 지정 -->
<input type="hidden" name="password" value="dummy">	<!-- validator 처리를 위해 지정 -->
</c:if>

<c:if test="${bdMstr.bbsAttrbCode != 'BBSA01'}">
   <input id="ntceBgnde" name="ntceBgnde" type="hidden" value="10000101">
   <input id="ntceEndde" name="ntceEndde" type="hidden" value="99991231">
</c:if>

<div id="border">
	<table width="100%" cellpadding="8" class="table-search" border="0">
	 <tr>
	  <td width="100%" class="title_left">
	   <img src="<c:url value='/images/bbs/tit_icon.gif' />" width="3" height="16" hspace="3" align="middle" alt="제목버튼이미지">
	   &nbsp;<c:out value='${bdMstr.bbsNm}'/> - 게시글쓰기</td>
	 </tr>
	</table>
	<table width="100%" border="0" cellpadding="0" cellspacing="1" class="generalTable">
	  <tr>
	    <th width="20%" height="23" class="emphasisRight" nowrap>*<spring:message code="cop.nttSj" />
	    <td width="80%" nowrap colspan="3">
	      <input name="nttSj" type="text" size="60" value="" class="detail_input"  maxlength="60" title="제목입력">
	      <br/><form:errors path="nttSj" />
	    </td>
	  </tr>
	  <tr>
	    <th height="23" class="emphasisRight" >*<spring:message code="cop.nttCn" />
	    <td colspan="3">
	     <table width="100%" border="0" cellpadding="0" cellspacing="0" class="noStyle">
	     <tr><td>
	      <textarea id="nttCn" name="nttCn" class="textarea" rows="20" style="width:100%;" title="내용입력" class="detail_input"></textarea>
	      <form:errors path="nttCn" />
	      </td></tr>
	     </table>
	    </td>
	  </tr>
	  <c:choose>
	  	<c:when test="${bdMstr.bbsAttrbCode == 'BBSA01'}">
		  <tr>
		    <th height="23" class="emphasisRight">*<spring:message code="cop.noticeTerm" />
			    <td colspan="3">
			      <input id="ntceBgnde" name="ntceBgnde" type="hidden">
			      <input name="ntceBgndeView" type="text" size="10" value=""  onchange="javascript:fn_SetDate(this.value,ntceBgnde)" readOnly 	class="detail_date" title="게시시작일자입력">
			      ~
			      <input id="ntceEndde" name="ntceEndde" type="hidden"  />
			      <input name="ntceEnddeView" type="text" size="10" value=""  onchange="javascript:fn_SetDate(this.value,ntceEndde)" readOnly class="detail_date"	 title="게시종료일자입력">
			    </td>
		  </tr>
	  	</c:when>
	  	<c:otherwise>
	  	</c:otherwise>
	  </c:choose>
	  <c:if test="${bdMstr.fileAtchPosblAt == 'Y'}">
	  <tr>
	    <th height="23" class="emphasisRight"><spring:message code="cop.atchFile" /></th>
	    <td colspan="3">
            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
			    <tr>
			        <td><input name="file_1" id="egovComFileUploader" type="file" title="첨부파일입력"/>&nbsp1번쨰 선택은 표지사진을 반드시 선택...</td>
			    </tr>
			    <tr>
			        <td>
			        	<div id="egovComFileList"></div>
			        </td>
			    </tr>
   	        </table>
	    </td>
	  </tr>
	  </c:if>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td height="10"></td>
	  </tr>
	</table>
  	<div align="center">
	<table border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
      <c:if test="${bdMstr.authFlag == 'Y'}">
	  	<td>
			  	<span class="bbsbutton">
		     		<input type="image" id="x_save" onclick="javascript:fn_egov_regist_notice(); return false;" src="<c:url value='/images/bbs/x_save.gif' />"  onmouseover="fn_set_imgover(this)" onmouseout="fn_set_imgout(this)">
			  	</span>
	    </td>
	  	<td width="10"></td>
      </c:if>
   		<td>
	    	<span class="bbsbutton">
   				<input type="image" id="x_list" onclick="javascript:fn_egov_select_noticeList(); return false;" src="<c:url value='/images/bbs/x_list.gif' />"  onmouseover="fn_set_imgover(this)" onmouseout="fn_set_imgout(this)">
   			</span>
   		</td>
	</tr>
	</table>
	</div>
</div>
</form:form>
<script>
	var editor = null;
	editor = CKEDITOR.replace('nttCn', {
		fullpage: true,
		allowedContent: true,
		language: 'ko',
		height: 300
	});
	$('.detail_date').datepicker( {
	    changeMonth: true,
	    changeYear: true,
	    dayNames: ['월요일', '화요일', '수요일', '목요일', '금요일', '토요일', '일요일'],         
	    dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],          
	    monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'],         
	    monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
	    nextText: '다음 달',
	    prevText: '이전 달',
	    showButtonPanel: true,          
	    currentText: '오늘 날짜',          
	    closeText: '닫기',
	    dateFormat: "yy-mm-dd",
	    showMonthAfterYear: true,   
	    yearSuffix: ' 년 ',   
	    monthSuffix: ' 월',   
	    showOtherMonths: true, // 나머지 날짜도 화면에 표시   
	    selectOtherMonths: true
	});
</script>
</body>
</html>

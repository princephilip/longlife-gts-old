<!DOCTYPE>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
 /**
  * @Class Name : EgovNoticeInqire.jsp
  * @Description : 게시물 조회 화면 (detail)
  * @Modification Information
  * @
  * @  수정일      수정자            수정내용
  * @ -------        --------    ---------------------------
  * @ 2009.03.23   이삼섭          최초 생성
  * @ 2009.06.26   한성곤          2단계 기능 추가 (댓글관리, 만족도조사)
  *
  *  @author 공통서비스 개발팀 이삼섭
  *  @since 2009.03.23
  *  @version 1.0
  *  @see
  *
  */
%>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
<link href="<c:url value='/Theme/css/bbs.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='/Theme/css/egovbbsTemplate.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='/Mighty/css/mighty-2.0.1.css' />" rel="stylesheet" type="text/css">
	
<script type="text/javascript" src="<c:url value='/Mighty/egov/js/bbs/EgovBBSMng.js' />" ></script>
<c:if test="${anonymous == 'true'}"><c:set var="prefix" value="/anonymous"/></c:if>

<script type="text/javaScript" src="<c:url value='/Mighty/3rd/jQuery/js/jquery-1.11.2.min.js'/>"></script>
<script type="text/javascript">
	
	function onloading() {
		if ("<c:out value='${msg}'/>" != "") {
			alert("<c:out value='${msg}'/>");
		}
	}

	function fn_egov_select_noticeList(pageNo) {
		document.frm.pageIndex.value = pageNo;
		document.frm.action = "<c:url value='/bbs${prefix}/selectBoardList.do'/>";
		document.frm.submit();
	}

	function fn_egov_delete_notice() {
		if ("<c:out value='${anonymous}'/>" == "true" && document.frm.password.value == '') {
			alert('등록시 사용한 패스워드를 입력해 주세요.');
			document.frm.password.focus();
			return;
		}

		if (confirm('<spring:message code="common.delete.msg" />')) {
			document.frm.action = "<c:url value='/bbs${prefix}/deleteBoardArticle.do'/>";
			document.frm.submit();
		}
	}

	function fn_egov_moveUpdt_notice() {
		if ("<c:out value='${anonymous}'/>" == "true" && document.frm.password.value == '') {
			alert('등록시 사용한 패스워드를 입력해 주세요.');
			document.frm.password.focus();
			return;
		}

		document.frm.action = "<c:url value='/bbs${prefix}/forUpdateBoardArticle.do'/>";
		document.frm.submit();
	}

	function fn_egov_addReply() {
		document.frm.action = "<c:url value='/bbs${prefix}/addReplyBoardArticle.do'/>";
		document.frm.submit();
	}
</script>
<!-- 2009.06.29 : 2단계 기능 추가  -->
<c:if test="${useComment == 'true'}">
<c:import url="/bbs/selectCommentList.do" charEncoding="utf-8">
	<c:param name="type" value="head" />
</c:import>
</c:if>
<c:if test="${useScrap == 'true'}">
<script type="text/javascript">
	function fn_egov_addScrap() {
		document.frm.action = "<c:url value='/bbs/addScrap.do'/>";
		document.frm.submit();
	}
</script>
</c:if>
<!-- 2009.06.29 : 2단계 기능 추가  -->
<title><c:out value='${result.bbsNm}'/> - 글조회</title>

<style type="text/css">
	h1 {font-size:12px;}
	caption {visibility:hidden; font-size:0; height:0; margin:0; padding:0; line-height:0;}
</style>


</head>
<body onload="onloading();">
<form name="frm" method="post" action="">
<input type="hidden" name="pageIndex" value="<c:out value='${searchVO.pageIndex}'/>">
<input type="hidden" name="companyCode" value="<c:out value='${result.companyCode}'/>" >
<input type="hidden" name="bbsId" value="<c:out value='${result.bbsId}'/>" >
<input type="hidden" name="nttId" value="<c:out value='${result.nttId}'/>" >
<input type="hidden" name="parnts" value="<c:out value='${result.parnts}'/>" >
<input type="hidden" name="sortOrdr" value="<c:out value='${result.sortOrdr}'/>" >
<input type="hidden" name="replyLc" value="<c:out value='${result.replyLc}'/>" >
<input type="hidden" name="nttSj" value="<c:out value='${result.nttSj}'/>" >

<div id="border">

	<table width="100%" cellpadding="8" class="table-search" border="0">
	 <tr>
	  <td width="100%" class="title_left">
	   <img src="<c:url value='/Theme/images/bbs/tit_icon.gif' />" width="3" height="16" hspace="3" style="vertical-align: middle" alt="제목아이콘이미지">
	   &nbsp;<c:out value='${result.bbsNm}'/> - 글조회</td>
	 </tr>
	</table>
	<table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="ffffff" class="generalTable">
	  <tr>
	    <th width="15%" height="23" nowrap class="title_left">제목</th>
	    <c:choose>
	    	<c:when test="${brdMstrVO.bbsTyCode == 'BBST05'}">
			    <td width="65%" colspan="3" nowrap><c:out value="${result.nttSj}" />
	    	</c:when>
	    	<c:otherwise>
			    <td width="85%" colspan="5" nowrap><c:out value="${result.nttSj}" />
	    	</c:otherwise>
	    </c:choose>
	    <c:if test="${brdMstrVO.bbsTyCode == 'BBST05'}">
		    <th width="15%" height="23" nowrap class="title_left" >자료분류</th>
		    <td width="15%" class="listCenter" nowrap><c:out value="${result.ntceSelName}" />
		    </td>
	    </c:if>
	  </tr>
	  <tr>
	    <th width="15%" height="23" nowrap  class="title_left">작성자</th>
	    <td width="15%" class="lt_text3" nowrap>
	    <c:choose>
	    	<c:when test="${anonymous == 'true'}">
	    		******
	    	</c:when>
	    	<c:when test="${result.ntcrNm == null || result.ntcrNm == ''}">
	    		<c:out value="${result.frstRegisterNm}" />
	    	</c:when>
	    	<c:otherwise>
	    		<c:out value="${result.ntcrNm}" />
	    	</c:otherwise>
	    </c:choose>

	    </td>
	    <th width="15%" height="23" nowrap  class="title_left">작성시간</th>
	    <td width="15%" class="listCenter" nowrap><c:out value="${result.frstRegisterPnttm}" />
	    </td>
	    <th width="15%" height="23" nowrap class="title_left" >조회수</th>
	    <td width="15%" class="listCenter" nowrap><c:out value="${result.inqireCo}" />
	    </td>
	  </tr>
	  <tr>
	    <th height="23"  class="title_left">글내용</th>
	    <td colspan="5">
	     <div id="bbs_cn">
				<c:out value="${result.nttCn}" escapeXml="true" />
	     </div>
	    </td>
	  </tr>
	  <c:if test="${not empty result.atchFileId}">
		  <c:if test="${result.bbsAttrbCode == 'BBSA02'}">
		  <tr>
		    <th height="23"  class="title_left">첨부이미지</th>
		    <td colspan="5">
					<c:import url="/fms/selectImageFileInfs.do" charEncoding="utf-8">
						<c:param name="atchFileId" value="${result.atchFileId}" />
					</c:import>
		    </td>
		  </tr>
		  </c:if>
		  <tr>
		    <th height="23" class="title_left">첨부파일 목록</th>
		    <td colspan="5">
				<c:import url="/fms/selectFileInfs.do" charEncoding="utf-8">
					<c:param name="param_atchFileId" value="${result.atchFileId}" />
				</c:import>
		    </td>
		  </tr>
	  </c:if>
	  <c:if test="${anonymous == 'true'}">
	  <tr>
	    <th height="23" class="title_left"><spring:message code="cop.password" /></th>
	    <td colspan="5">
	    	<input name="password" type="password" size="20" value="" maxlength="20" title="비밀번호입력">
	    </td>
	  </tr>
	  </c:if>
	</table>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td height="5"></td>
	  </tr>
	</table>
	<div align="center">
	<table width="100%"  border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
	 <td width="100%">
	 <div align="center">
		<table border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
		     <c:if test="${result.frstRegisterId == sessionUniqId || brdMstrVO.authFlag=='Y'}">
			    <td align='center'>
		 				<span style="width:100%;height:100%;vertical-align:middle;">
							<ul id="buttons2">
								<li><a class="modify"  id="x_modify"  href="javascript:;"onclick="fn_egov_moveUpdt_notice();" 		title="현재 조회중인 내역을 수정합니다."></a></li>
							</ul>		
						</span>
					</td>
					<td align='center'>
						<span style="width:100%;height:100%;vertical-align:middle;">
							<ul id="buttons">
								<li><a class="delete" id="x_delete" 	href="javascript:;"onclick="fn_egov_delete_notice();" title="조회중인 데이타를 삭제합니다."></a></li>
							</ul>
						</span>
					</td>
		     </c:if>
		     <c:if test="${result.replyPosblAt == 'Y'}">
		  		<td align='center'>
				  	<span style="width:100%;height:100%;vertical-align:middle;">
							<ul id="buttons2">
								<li><a class="reply" id="x_reply" 	href="javascript:;"onclick="fn_egov_addReply();" title="답글을 새로 작성합니다."></a></li>
							</ul>
						</span>
			    </td>
	        </c:if>
	    		
	    		<td align='center'>
				  	<span style="width:100%;height:100%;vertical-align:middle;">
							<ul id="buttons2">
								<li><a class="listup" id="x_list" 	href="javascript:;"onclick="fn_egov_select_noticeList(1);" title="목록으로 돌아갑니다."></a></li>
							</ul>
						</span>
			    </td>
	    		
	    		<!--
	    		<td align='center'>
		 				<span style="width:100%;height:100%;vertical-align:middle;">
							<ul id="buttons">
								<li><a class="save"  id="x_save"  href="javascript:;"onclick="fn_egov_regist_notice();" 		title="데이타를 등록합니다."></a></li>
								<li><a class="close" id="x_list" 	href="javascript:;"onclick="fn_egov_select_noticeList();" title="현재창을 닫고 목록으로 돌아갑니다."></a></li>
							</ul>		
						</span>
					</td>
	    		-->
	    		
		      <!-- 2009.06.29 : 2단계 기능 추가 
		      <c:if test="${useScrap == 'true'}">
			      <td width="10"></td>
			      <td><img src="<c:url value='/images/bbs/bu2_left.gif'/>" width="8" height="20" alt="버튼이미지"></td>
			      <td style="background-image:URL(<c:url value='/images/bbs/bu2_bg.gif'/>);" class="text_left" nowrap>
			      <a href="javascript:fn_egov_addScrap()">스크랩</a>
			      </td>
			      <td><img src="<c:url value='/images/bbs/bu2_right.gif'/>" width="8" height="20" alt="버튼이미지"></td>
	          </c:if>
	          -->
			</tr>
		</table>
	</div>
	<!-- 2009.06.29 : 2단계 기능 추가  -->
	<c:if test="${useComment == 'true'}">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td height="10"></td>
	  </tr>
	</table>

	<c:import url="/bbs${prefix}/selectCommentList.do" charEncoding="utf-8">
		<c:param name="type" value="body" />
	</c:import>
	</c:if>
	<!-- 2009.06.29 : 2단계 기능 추가  -->

	 </td>
	</tr>
	</table>
	</div>
</div>
</form>
<script type="text/javascript">
	$('#bbs_cn').each(function(){
	    var $this = $(this);
	    var t = $this.text();
	    $this.html(t.replace('&lt','<').replace('&gt', '>'));
	});
</script>

</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%
 /**
  * @Class Name : EgovGalleryUpdt.jsp
  * @Description : 게시물 수정 화면
  * @Modification Information
  * @
  * @  수정일      수정자            수정내용
  * @ -------        --------    ---------------------------
  * @ 2009.03.19   이삼섭          최초 생성
  *   2011.09.15   서준식          유효기간 시작일이 종료일보다 빠른지 체크하는 로직 추가
  *  @author 공통서비스 개발팀 이삼섭
  *  @since 2009.03.19
  *  @version 1.0
  *  @see
  *
  */
%>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
<link type="text/css" charset="utf-8" href="/Mighty/3rd/jQuery/css/Aristo/Aristo.X-1.1.1.css" rel="stylesheet" media="all" />
<link href="<c:url value='theme/css/bbs.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='theme/css/button.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='${brdMstrVO.tmplatCours}' />" rel="stylesheet" type="text/css">
<script>var _CPATH = '<%=request.getContextPath()%>';</script>
<script type="text/javascript" src="<c:url value='/js/bbs/EgovBBSMng.js' />"></script>
<script type="text/javaScript" src="<c:url value='/ckeditor/ckeditor.js'/>"></script>  
<script type="text/javaScript" src="<c:url value='/ckeditor/config.js'/>"></script> 
<script type="text/javaScript" src="<c:url value='/ckeditor/adapters/jquery.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/cmm/EgovMultiFile.js'/>" ></script>
<script type="text/javascript" src="<c:url value='/js/cal/EgovCalPopup.js'/>" ></script>
<script type="text/javascript" src="<c:url value="/com/validator.do"/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/jQuery/js/jquery-1.11.2.min.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/jQuery/js/jquery.ui.datepicker-ko.min.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/js/mighty-1.2.7.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/js/mighty-win-1.2.7.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/js/mighty-mdi-1.2.7.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/jQuery/js/jquery-ui-1.11.3.custom/jquery-ui.min.js'/>"></script>
<script type="text/javaScript" src="${pageContext.request.contextPath}/Mighty/jQuery/js/jquery-1.11.2.min.js"></script>
<validator:javascript formName="board" staticJavascript="false" xhtml="true" cdata="false"/>
<c:if test="${anonymous == 'true'}"><c:set var="prefix" value="/anonymous"/></c:if>
<script type="text/javascript">
	function fn_egov_validateForm(obj){
		return true;
	}

	function fncManageChecked() {
		<c:if test="${brdMstrVO.bbsTyCode == 'BBST05'||brdMstrVO.bbsTyCode == 'BBST03'}">
		    var checkField = document.board.ntceAuth;
		    var returnValue = "";
		    if(checkField) {
		        if(checkField.length > 1) {
		            for(var i=0; i<checkField.length; i++) {
		                if(checkField[i].checked)
		                	returnValue = returnValue + "1";
		                else 
		                    returnValue = returnValue + "0";
		            }
		        }
		    } 
		    document.board.ntceAuthCode.value = returnValue;
		</c:if>	
	}
	
	function fn_SetDate(a_val,a_obj) {
		$("#"+a_obj.id).val(a_val.replace(/\-/g,''));
	}

	function fn_egov_regist_notice(){
		//첨부파일 확장자 체크(2015.04.18 KYY)
		var afiles = $(':file');
		for(var i=0; i<afiles.length; i++) {
	    if(/.*\.(jsp)|(asp)|(aspx)|(do)|(exe)|(php)|(cgi)$/.test(afiles[i].value.toLowerCase())){
	 	   	alert(afiles[i].value + "\r\n허용되지 않는 형식의 첨부파일입니다.");
				return;
	    }
		}

		fncManageChecked();
		
		document.board.submit();


		var ntceBgnde = document.getElementById("ntceBgnde").value;
		var ntceEndde = document.getElementById("ntceEndde").value;

		if(ntceBgnde > ntceEndde){
			alert("게시기간 종료일이 시작일보다 빠릅니다.");
			return;
		}

		if (!validateBoard(document.board)){
			return;
		}

		if (confirm('<spring:message code="common.update.msg" />')) {
			document.board.action = "<c:url value='/bbs${prefix}/updateGalleryArticle.do'/>";
			document.board.submit();
		}
	}

	function fn_egov_select_noticeList() {
		document.board.action = "<c:url value='/bbs${prefix}/selectGalleryList.do'/>";
		document.board.submit();
	}

	function fn_egov_check_file(flag) {
		if (flag=="Y") {
			document.getElementById('file_upload_posbl').style.display = "block";
			document.getElementById('file_upload_imposbl').style.display = "none";
		} else {
			document.getElementById('file_upload_posbl').style.display = "none";
			document.getElementById('file_upload_imposbl').style.display = "block";
		}
	}
	function makeFileAttachment(){
	<c:if test="${bdMstr.fileAtchPosblAt == 'Y'}">
	
		var existFileNum = document.board.fileListCnt.value;
		var maxFileNum = document.board.posblAtchFileNumber.value;
	
		if (existFileNum=="undefined" || existFileNum ==null) {
			existFileNum = 0;
		}
		if (maxFileNum=="undefined" || maxFileNum ==null) {
			maxFileNum = 0;
		}
		var uploadableFileNum = maxFileNum - existFileNum;
		if (uploadableFileNum<0) {
			uploadableFileNum = 0;
		}
		if (uploadableFileNum != 0) {
			fn_egov_check_file('Y');
			var multi_selector = new MultiSelector( document.getElementById( 'egovComFileList' ), uploadableFileNum );
			multi_selector.addElement( document.getElementById( 'egovComFileUploader' ) );
		} else {
			fn_egov_check_file('N');
		}
	</c:if>	
	}
</script>
<style type="text/css">
.noStyle {background:ButtonFace; BORDER-TOP:0px; BORDER-bottom:0px; BORDER-left:0px; BORDER-right:0px;}
  .noStyle th{background:ButtonFace; padding-left:0px;padding-right:0px}
  .noStyle td{background:ButtonFace; padding-left:0px;padding-right:0px}
</style>
<title><c:out value='${bdMstr.bbsNm}'/> - 게시글 수정</title>

<style type="text/css">
	h1 {font-size:12px;}
	caption {visibility:hidden; font-size:0; height:0; margin:0; padding:0; line-height:0;}
</style>

</head>
<!-- body onload="javascript:editor_generate('nttCn');"-->
<body onLoad="document.board.nttSj.focus(); makeFileAttachment();">

<form:form commandName="board" name="board" method="post" enctype="multipart/form-data" >
<input type="hidden" name="pageIndex" value="<c:out value='${searchVO.pageIndex}'/>"/>
<input type="hidden" name="returnUrl" value="<c:url value='/bbs/forUpdateGalleryArticle.do'/>"/>
<input type="hidden" name="companyCode" value="<c:out value='${bdMstr.companyCode}'/>" />
<input type="hidden" name="bbsId" value="<c:out value='${result.bbsId}'/>" />
<input type="hidden" name="nttId" value="<c:out value='${result.nttId}'/>" />
<input type="hidden" name="bbsAttrbCode" value="<c:out value='${bdMstr.bbsAttrbCode}'/>" />
<input type="hidden" name="bbsTyCode" value="<c:out value='${bdMstr.bbsTyCode}'/>" />
<input type="hidden" name="replyPosblAt" value="<c:out value='${bdMstr.replyPosblAt}'/>" />
<input type="hidden" name="fileAtchPosblAt" value="<c:out value='${bdMstr.fileAtchPosblAt}'/>" />
<input type="hidden" name="posblAtchFileNumber" value="<c:out value='${bdMstr.posblAtchFileNumber}'/>" />
<input type="hidden" name="posblAtchFileSize" value="<c:out value='${bdMstr.posblAtchFileSize}'/>" />
<input type="hidden" name="tmplatId" value="<c:out value='${bdMstr.tmplatId}'/>" />

<input type="hidden" name="cal_url" value="<c:url value='/cal/EgovNormalCalPopup.do'/>" />

<c:if test="${anonymous != 'true'}">
<input type="hidden" name="ntcrNm" value="dummy">	<!-- validator 처리를 위해 지정 -->
<input type="hidden" name="password" value="dummy">	<!-- validator 처리를 위해 지정 -->
</c:if>

<c:if test="${bdMstr.bbsAttrbCode != 'BBSA01'}">
   <input id="ntceBgnde" name="ntceBgnde" type="hidden" value="10000101">
   <input id="ntceEndde" name="ntceEndde" type="hidden" value="99991231">
</c:if>

<div id="border">

	<table width="100%" cellpadding="8" class="table-search" border="0">
	 <tr>
	  <td width="100%"class="title_left">
	   <img src="<c:url value='/images/bbs/tit_icon.gif' />" width="3" height="16" hspace="3" align="middle" alt="제목버튼이미지">
	   &nbsp;<c:out value='${bdMstr.bbsNm}'/> - 게시글 수정</td>
	 </tr>
	</table>
	<table width="100%" border="0" cellpadding="0" cellspacing="1" class="generalTable">
	  <tr>
	    <th width="20%" height="23" class="emphasisRight" nowrap >*<spring:message code="cop.nttSj" />
	    <td width="80%" nowrap colspan="3">
	      <input name="nttSj" type="text" size="60" value='<c:out value="${result.nttSj}" />' class="detail_input" maxlength="60" title="제목수정">
	       <br/><form:errors path="nttSj" />
	    </td>
	  </tr>
	  <c:if test="${brdMstrVO.bbsTyCode == 'BBST05' || brdMstrVO.bbsTyCode == 'BBST03'}">
		  <tr>
		    <th width="20%" height="25" class="required_text" nowrap >
		    	<label for="ntceAuthCode">
		    		*<spring:message code="cop.ntceAuthCode" />
		    	</label>
		    </th>
		    <td width="30%" nowrap>
				<input id="ntceAuthCode" name="ntceAuthCode" type="hidden" value="${result.ntceAuthCode}">
   		    	<input type="checkbox" name="ntceAuth" value="" <c:if test="${fn:substring(result.ntceAuthCode,0,1)==1}"> checked </c:if>>본사
		    	<input type="checkbox" name="ntceAuth" value="" <c:if test="${fn:substring(result.ntceAuthCode,1,2)==1}"> checked </c:if>>고객 
		    	<input type="checkbox" name="ntceAuth" value="" <c:if test="${fn:substring(result.ntceAuthCode,2,3)==1}"> checked </c:if>>협력업체 
		    	<input type="checkbox" name="ntceAuth" value="" <c:if test="${fn:substring(result.ntceAuthCode,3,4)==1}"> checked </c:if>>모바일
		    </td>
		  </tr>
	    </c:if>
	     <c:if test="${brdMstrVO.bbsTyCode == 'BBST05'}">
		  <tr>
		    <th width="20%" height="23" class="required_text" nowrap >
		    	<label for="ntceSelCode">
		    		*<spring:message code="cop.ntceSelCode" />
		    	</label>
		    </th>
		    <td width="30%" nowrap>
		        <form:select path="ntceSelCode" title="자료분류수정">
		            <c:forEach var="items" items="${attrbList}" varStatus="status">
		            <option value='<c:out value="${items.code}"/>' <c:if test="${items.code == result.ntceSelCode}">selected="selected"</c:if> ><c:out value="${items.codeNm}"/></option>
		            </c:forEach>
	      		</form:select>
		  	   <br/><form:errors path="ntceSelCode" />
		    </td>
		  </tr>
	    </c:if>
	  <tr>
	    <th height="23" class="emphasisRight" >*<spring:message code="cop.nttCn" />
	    <td colspan="3">
	     <table width="100%" border="0" cellpadding="0" cellspacing="0" class="noStyle">
	     <tr><td>
	      <textarea id="nttCn" name="nttCn" class="textarea" rows="20"  style="width:100%;"><c:out value="${result.nttCn}" escapeXml="false" /></textarea>
	      <form:errors path="nttCn" />
	      </td></tr>
	     </table>
	    </td>
	  </tr>
	  <c:if test="${bdMstr.bbsAttrbCode == 'BBSA01'}">
		  <tr>
		    <th height="23" class="emphasisRight">*<spring:message code="cop.noticeTerm" />
		    <td colspan="3">

		      <input id="ntceBgnde" name="ntceBgnde" type="hidden" value='<c:out value="${result.ntceBgnde}" />'>
			  <input name="ntceBgndeView" type="text" size="10" value="${fn:substring(result.ntceBgnde, 0, 4)}-${fn:substring(result.ntceBgnde, 4, 6)}-${fn:substring(result.ntceBgnde, 6, 8)}"  onchange="javascript:fn_SetDate(this.value,ntceBgnde)" readOnly 	class="detail_date" title="게시시작일자입력">
		      ~
		      <input id="ntceEndde" name="ntceEndde" type="hidden"  value='<c:out value="${result.ntceEndde}" />'>
		      <input name="ntceEnddeView" type="text" size="10" value="${fn:substring(result.ntceEndde, 0, 4)}-${fn:substring(result.ntceEndde, 4, 6)}-${fn:substring(result.ntceEndde, 6, 8)}"  onchange="javascript:fn_SetDate(this.value,ntceEndde)" readOnly class="detail_date"	 title="게시종료일자입력">
		    </td>
		  </tr>
	  </c:if>
	  <c:if test="${anonymous == 'true'}">
		  <tr>
		    <th height="23" class="emphasisRight">*<spring:message code="cop.ntcrNm" />
		    <td colspan="3">
		      <input name="ntcrNm" type="text" size="20" value='<c:out value="${result.ntcrNm}" />'  class="detail_input" maxlength="10" title="작성자이름">
		    </td>

		  </tr>
		  <tr>
		    <th height="23" class="emphasisRight">*<spring:message code="cop.password" />
		    <td colspan="3">
		      <input name="password" type="password" size="20" value="" maxlength="20" class="detail_input" title="비밀번호입력">
		    </td>
		  </tr>
	  </c:if>
	  <c:if test="${not empty result.atchFileId}">
		  <tr>
		    <th height="28"><spring:message code="cop.atchFileList" /></th>
		    <td colspan="6">
				<c:import url="/fms/selectFileInfsForUpdate.do" charEncoding="utf-8">
					<c:param name="param_atchFileId" value="${result.atchFileId}" />
				</c:import>
		    </td>
		  </tr>
	  </c:if>
	  <c:if test="${bdMstr.fileAtchPosblAt == 'Y'}">
	  	<c:if test="${result.atchFileId == ''}">
	  		<input type="hidden" name="fileListCnt" value="0" />
	  	</c:if>
		  <tr>
		    <th height="28"><spring:message code="cop.atchFile" /></th>
		    <td colspan="3">
		    <div id="file_upload_posbl"  style="display:none;" >
	            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
				    <tr>
				        <td><input name="file_1" id="egovComFileUploader" type="file" title="첨부파일명 입력"/>&nbsp1번쨰 선택은 표지사진을 반드시 선택...</td>
				    </tr>
				    <tr>
				        <td>
				        	<div id="egovComFileList"></div>
				        </td>
				    </tr>
	   	        </table>
			</div>
			<div id="file_upload_imposbl"  style="display:none;" >
	            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
				    <tr>
				        <td><spring:message code="common.imposbl.fileupload" /></td>
				    </tr>
	   	        </table>
			</div>
		  </tr>


	  </c:if>
	</table>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td height="10"></td>
	  </tr>
	</table>
	<div align="center">
	<table border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
	 <c:if test="${bdMstr.authFlag == 'Y'}">
	     <c:if test="${result.frstRegisterId == searchVO.frstRegisterId}">
	  		<td>
			  	<span class="bbsbutton">
		     		<input type="image" id="x_save" onclick="javascript:fn_egov_regist_notice(); return false;" src="<c:url value='theme/images/bbs/x_save.gif' />"  onmouseover="fn_set_imgover(this)" onmouseout="fn_set_imgout(this)">
			  	</span>
		    </td>
	        <td width="5"></td>
	      </c:if>
      </c:if>
   		<td>
	    	<span class="bbsbutton">
   				<input type="image" id="x_list" onclick="javascript:fn_egov_select_noticeList(); return false;" src="<c:url value='theme/images/bbs/x_list.gif' />"  onmouseover="fn_set_imgover(this)" onmouseout="fn_set_imgout(this)">
   			</span>
   		</td>
	</tr>
	</table>
	</div>
</div>
</form:form>
<script>
	var editor = null;
	editor = CKEDITOR.replace('nttCn', {
		fullpage: true,
		allowedContent: true,
		language: 'ko',
		height: 300
	});
</script></body>
</html>

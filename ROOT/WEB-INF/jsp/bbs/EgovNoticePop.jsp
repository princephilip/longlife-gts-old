<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>   
<c:set var="ImgUrl" value="/images/bbs/"/>
<jsp:useBean id="now" class="java.util.Date"/>

<%
 /**
  * @Class Name : EgovNoticeList.jsp
  * @Description : 게시물 목록화면
  * @Modification Information
  * @
  * @  수정일      수정자            수정내용
  * @ -------        --------    ---------------------------
  * @ 2009.03.19   이삼섭          최초 생성
  * @ 2011.11.11   이기하          익명게시판 검색시 작성자 제거
  *
  *  @author 공통서비스 개발팀 이삼섭
  *  @since 2009.03.19
  *  @version 1.0
  *  @see
  *
  */
%>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
<link href="<c:url value='/css/bbs.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='/css/button.css' />" rel="stylesheet" type="text/css">
<link type="text/css" charset="utf-8" href="/Mighty/3rd/jQuery/css/Aristo/Aristo.X-1.1.1.css" rel="stylesheet" media="all" />
<link href="<c:url value='${brdMstrVO.tmplatCours}' />" rel="stylesheet" type="text/css">

<c:if test="${anonymous == 'true'}"><c:set var="prefix" value="/anonymous"/></c:if>
<script type="text/javascript" src="<c:url value='/Mighty/egov/js/bbs/EgovBBSMng.js' />" ></script>
<script type="text/javaScript" src="<c:url value='/Mighty/jQuery/js/jquery-1.11.2.min.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/jQuery/js/jquery.ui.datepicker-ko.min.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/js/mighty-1.2.7.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/js/mighty-win-1.2.7.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/js/mighty-mdi-1.2.7.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/jQuery/js/jquery-ui-1.11.3.custom/jquery-ui.min.js'/>"></script>


<c:choose>
<c:when test="${preview == 'true'}">
<script type="text/javascript">
<!--
	function press(event) {
	}

	function fn_egov_addNotice() {
	}

	function fn_egov_select_noticeList(pageNo) {
	}

	function fn_egov_inqire_notice(nttId, bbsId) {
	}
//-->
</script>
</c:when>
<c:otherwise>
<script type="text/javascript">
<!--
	function press(event) {
		if (event.keyCode==13) {
			fn_egov_select_noticeList('1');
		}
	}

	function fn_egov_select_noticeList(pageNo) {
		document.frm.pageIndex.value = pageNo;
		document.frm.action = "<c:url value='/bbs${prefix}/selectBoardPop.do'/>";
		document.frm.submit();
	}

	function fn_egov_window_close(a_win) {
		var _IsModal = (window.dialogArguments?true:false);
		var _IsPopup = (window.opener?true:false);
		if(_IsModal || _IsPopup) {
			window.close();
		} else {
			a_win.mytop._X.MDI_Close(a_win.mytop._CurMenuIndex);	
		}
	}

	function fn_egov_select_notice(nttId, nttSj){
		var retVal = nttId +"|"+nttSj;
		parent.fn_egov_returnValue(retVal);
	}
	
//-->
</script>
</c:otherwise>
</c:choose>
<title><c:out value="${brdMstrVO.bbsNm}"/></title>

<style type="text/css">
	h1 {font-size:12px;}
	caption {visibility:hidden; font-size:0; height:0; margin:0; padding:0; line-height:0;}
</style>

</head>
<body>

<div id="border">

<form name="frm" action ="<c:url value='/bbs${prefix}/selectBoardPop.do'/>" method="post">
<input type="hidden" name="companyCode" value="<c:out value='${boardVO.companyCode}'/>" />
<input type="hidden" name="bbsId" value="<c:out value='${boardVO.bbsId}'/>" />
<input type="hidden" name="nttId"  value="0" />
<input type="hidden" name="bbsTyCode" value="<c:out value='${brdMstrVO.bbsTyCode}'/>" />
<input type="hidden" name="bbsAttrbCode" value="<c:out value='${brdMstrVO.bbsAttrbCode}'/>" />
<input type="hidden" name="authFlag" value="<c:out value='${brdMstrVO.authFlag}'/>" />
<input name="pageIndex" type="hidden" value="<c:out value='${searchVO.pageIndex}'/>"/>
<input type="hidden" name="searchAuth" value="<c:out value='${searchVO.searchAuth}'/>"/>

<table width="100%" cellpadding="8" class="table-search" border="0">
 <tr>
  <td width="30%" class="title_left">
  	<spring:message code="cop.regisTerm" />
    <input name="searchBgnDe" class="search_date" type="text" size="10" maxlength="10" value="<c:out value="${searchVO.searchBgnDe}"/>" title="조회시작일자 입력">
	 ~
	<input name="searchEndDe" class="search_date" type="text" size="10" maxlength="10" value="<c:out value="${searchVO.searchEndDe}"/>" title="조회종료일자 입력">
  </td>
  <td width="60%" class="title_left">
   	<label for="searchDataType">
   		<spring:message code="cop.searchCond" />
   	</label>
  	<select name="searchCnd" class="select" title="검색조건선택">
		   <option value="0" <c:if test="${searchVO.searchCnd == '0'}">selected="selected"</c:if> >제목</option>
		   <option value="1" <c:if test="${searchVO.searchCnd == '1'}">selected="selected"</c:if> >내용</option>
		   <c:if test="${anonymous != 'true'}">
		   <option value="2" <c:if test="${searchVO.searchCnd == '2'}">selected="selected"</c:if> >작성자</option>
		   </c:if>
	</select>
    <input name="searchWrd" type="text" size="60" value='<c:out value="${searchVO.searchWrd}"/>' maxlength="60" onkeypress="press(event);" title="검색어 입력">
    <c:if test="${brdMstrVO.bbsTyCode == 'BBST05'}">
	   	<label for="searchDataType">
	   		<spring:message code="cop.dataTyeCode" />
	   	</label>
	    <select name="searchDataType">
		  <option value="">선택</option>
	      <c:forEach var="items" items="${attrbList}" varStatus="status">
	        <option value="${items.code}" <c:if test="${items.code eq searchVO.searchDataType}">selected</c:if>>${items.codeNm}</option>
	      </c:forEach>
	    </select>
    </c:if>
  </td>
  <th width="10%">
   <table border="0" cellspacing="0" cellpadding="0" width="100%" align="right">
    <tr>
     	<td>
     		<span class="bbsbutton">
         		<input type="image" id="x_close" onclick="fn_egov_window_close(window); return false;" src="<c:url value='/images/bbs/x_close.gif' />"  onmouseover="fn_set_imgover(this)" onmouseout="fn_set_imgout(this)">
     		</span>
     	</td>
    </tr>
   </table>
   </th>
 </tr>
 </table>
 </form>

<table width="100%" cellpadding="4" class="listTable" summary="번호, 제목, 게시시작일, 게시종료일, 작성자, 작성일, 선택   입니다">
 <thead>
  <tr>
    <!-- th class="title" width="3%" nowrap><input type="checkbox" name="all_check" class="check2"></th-->
    <th scope="col" class="listTitle" width="10%" nowrap>번호</th>
    <th scope="col" class="listTitle" width="44%" nowrap>제목</th>
   	<c:if test="${brdMstrVO.bbsAttrbCode == 'BBSA01'}">
	    <th scope="col" class="listTitle" width="20%" nowrap>게시시작일</th>
	    <th scope="col" class="listTitle" width="20%" nowrap>게시종료일</th>
   	</c:if>
   	<c:if test="${anonymous != 'true'}">
    	<th scope="col" class="listTitle" width="20%" nowrap>작성자</th>
    </c:if>
    <c:if test="${brdMstrVO.bbsTyCode == 'BBST05'}">
	    <th scope="col" class="listTitle" width="15%" nowrap>자료분류</th>
    </c:if>
    <th scope="col" class="listTitle" width="15%" nowrap>작성일</th>
    <th scope="col" class="listTitle" width="8%" nowrap>선택</th>
  </tr>
 </thead>

 <tbody>
 	<fmt:formatDate value="${now}" pattern="yyyy-MM-dd" var="now"/>
	 <c:forEach var="result" items="${resultList}" varStatus="status">
	  <tr>
	    <!--td class="lt_text3" nowrap><input type="checkbox" name="check1" class="check2"></td-->
	    <td class="listCenter" nowrap><c:out value="${(searchVO.pageIndex-1) * searchVO.pageSize + status.count}"/></td>
	    <td class="listLeft" nowrap>
	    	<c:choose>
	    		<c:when test="${result.isExpired=='Y' || result.useAt == 'N'}">
			    	<c:if test="${result.replyLc!=0}">
			    		<c:forEach begin="0" end="${result.replyLc}" step="1">
			    			&nbsp;
			    		</c:forEach>
			    		<img src="<c:url value='/Theme/images/bbs/reply_arrow.gif'/>" alt="reply arrow">
			    	</c:if>
	    			<c:out value="${result.nttSj}" />
			    	<c:if test="${now==result.frstRegisterPnttm}">
			    		<img src="<c:url value='/Theme/images/bbs/icon_new.gif'/>" alt="신규" align="middle" >
			    	</c:if>
			    	<c:if test="${fn:substring(result.atchFileId,0,4)=='FILE'}">
			    		<img src="<c:url value='/Theme/images/bbs/file.gif'/>" alt="파일첨부" hspace="3" align="middle" >
			    	</c:if>
	    		</c:when>
	    		<c:otherwise>
		    		<form name="subForm" method="post" action="<c:url value='/bbs${prefix}/selectBoardArticle.do'/>">
						<input type="hidden" name="companyCode" value="<c:out value='${result.companyCode}'/>" />
						<input type="hidden" name="bbsId" value="<c:out value='${result.bbsId}'/>" />
						<input type="hidden" name="nttId"  value="<c:out value="${result.nttId}"/>" />
						<input type="hidden" name="bbsTyCode" value="<c:out value='${brdMstrVO.bbsTyCode}'/>" />
						<input type="hidden" name="bbsAttrbCode" value="<c:out value='${brdMstrVO.bbsAttrbCode}'/>" />
						<input type="hidden" name="authFlag" value="<c:out value='${brdMstrVO.authFlag}'/>" />
						<input name="pageIndex" type="hidden" value="<c:out value='${searchVO.pageIndex}'/>"/>
				    	<c:if test="${result.replyLc!=0}">
				    		<c:forEach begin="0" end="${result.replyLc}" step="1">
				    			&nbsp;
				    		</c:forEach>
				    		<img src="<c:url value='/Theme/images/bbs/reply_arrow.gif'/>" alt="reply arrow">
				    	</c:if>
		    			<c:out value="${result.nttSj}"/>
				    	<c:if test="${now==result.frstRegisterPnttm}">
				    		<img src="<c:url value='/Theme/images/bbs/icon_new.gif'/>" alt="신규" align="middle" >
				    	</c:if>
				    	<c:if test="${fn:substring(result.atchFileId,0,4)=='FILE'}">
				    		<img src="<c:url value='/Theme/images/bbs/file.png'/>" alt="파일첨부" align="middle" >
				    	</c:if>
			    	</form>
	    		</c:otherwise>
	    	</c:choose>
	    </td>
    	<c:if test="${brdMstrVO.bbsAttrbCode == 'BBSA01'}">
		    <td class="listCenter" nowrap><c:out value="${result.ntceBgnde}"/></td>
		    <td class="listCenter" nowrap><c:out value="${result.ntceEndde}"/></td>
    	</c:if>
    	<c:if test="${anonymous != 'true'}">
	    	<td class="listCenter" nowrap><c:out value="${result.frstRegisterNm}"/></td>
	    </c:if>
		<c:if test="${brdMstrVO.bbsTyCode == 'BBST05'}">
		    <td class="listCenter" nowrap><c:out value="${result.ntceSelName}"/></td>
	    </c:if>
	    <td class="listCenter" nowrap><c:out value="${result.frstRegisterPnttm}"/></td>
	    <td class="lt_text3" nowrap><input type="button" value="선택" onClick="javascript:fn_egov_select_notice('<c:out value="${result.nttId}"/>','<c:out value="${result.nttSj}"/>');"/></td>
	  </tr>
	 </c:forEach>
	 <c:if test="${fn:length(resultList) == 0}">
	  <tr>
    	<c:choose>
    		<c:when test="${brdMstrVO.bbsAttrbCode == 'BBSA01'}">
    			<td class="listCenter" colspan="7" ><spring:message code="common.nodata.msg" /></td>
    		</c:when>
    		<c:otherwise>
    			<c:choose>
    				<c:when test="${anonymous == 'true'}">
		    			<td class="listCenter" colspan="6" ><spring:message code="common.nodata.msg" /></td>
		    		</c:when>
		    		<c:otherwise>
		    			<td class="listCenter" colspan="7" ><spring:message code="common.nodata.msg" /></td>
		    		</c:otherwise>
		    	</c:choose>
    		</c:otherwise>
    	</c:choose>
 		  </tr>
	 </c:if>
 </tbody>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="10"></td>
  </tr>
</table>

<div align="center" class="PageNumber">
	<ui:pagination paginationInfo="${paginationInfo}" type="image" jsFunction="fn_egov_select_noticeList" />
</div>

</div>
<script>
$('.search_date').datepicker( {
    changeMonth: true,
    changeYear: true,
    dayNames: ['월요일', '화요일', '수요일', '목요일', '금요일', '토요일', '일요일'],         
    dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],          
    monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'],         
    monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
    nextText: '다음 달',
    prevText: '이전 달',
    showButtonPanel: true,          
    currentText: '오늘 날짜',          
    closeText: '닫기',
    dateFormat: "yy-mm-dd",
    showMonthAfterYear: true,   
    yearSuffix: ' 년 ',   
    monthSuffix: ' 월',   
    showOtherMonths: true, // 나머지 날짜도 화면에 표시   
    selectOtherMonths: true
});
</script>
</body>
</html>

﻿<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<%@ page import="xgov.core.vo.XLoginVO" %>
<%@ page import="egovframework.rte.com.util.service.EgovUserDetailsHelper" %>
<%@ page import="egovframework.rte.com.util.service.EgovClntInfo" %>
<%@ page import="egovframework.rte.com.service.EgovLoginService" %>
<%@ page import="egovframework.rte.com.service.impl.EgovLoginServiceImpl" %>

<%
/**
  *<pre>
  * Statements
  *</pre>
  *
  * @Class Name : EgovLoginUsr.jsp
  * @Description : Login 인증 화면
  * @Modification Information
  * @
  * @  수정일        		 수정자                   수정내용
  * @ -------    --------    ---------------------------
  * @ 2013.03.03    박영찬        최초 생성
  * @
  */
%>
<%
	String userAgent = request.getHeader("user-agent");
	boolean mobile1 = userAgent.matches(".*(iPhone|iPod|Android|Windows CE|BlackBerry|Symbian|Windows Phone|webOS|Opera Mini|Opera Mobi|POLARIS|IEMobile|lgtelecom|nokia|SonyEricsson).*");
	boolean mobile2 = userAgent.matches(".*(LG|SAMSUNG|Samsung).*");
	
	String userId = "";  // = ibmsso.getID(request);
		
%>

<%@ include file="/Theme/jsp/login.jsp"%>
<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%
/**
  *<pre>
  * Statements
  *</pre>
  *
  * @Class Name : EgovLoginUsr.jsp
  * @Description : Login 인증 화면
  * @Modification Information
  * @
  * @  수정일        		 수정자                   수정내용
  * @ -------    --------    ---------------------------
  * @ 2013.03.03    박영찬        최초 생성
  * @
  */
%>
<%
	String userAgent = request.getHeader("user-agent");
	boolean mobile1 = userAgent.matches(".*(iPhone|iPod|Android|Windows CE|BlackBerry|Symbian|Windows Phone|webOS|Opera Mini|Opera Mobi|POLARIS|IEMobile|lgtelecom|nokia|SonyEricsson).*");
	boolean mobile2 = userAgent.matches(".*(LG|SAMSUNG|Samsung).*");
	if(mobile1 || mobile2) {
		//모바일은 접속 못하게(홈페이지 사이트로...) 2015.05.23 KYY
		response.sendRedirect("http://www.hec.co.kr"); 
		return;
	}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
<meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
<link rel="stylesheet" href="<c:url value='/css/login.css'/>" type="text/css">
<style type="text/css">
<!--
    #login_bg {position: absolute; top: 0; left: 0; width: 100%; height: 700px; background: url(<c:url value='/images/main/01fmst_back.jpg'/>) 0 0 repeat-x;}
    #login_area {position: relative; width: 1000px; height: auto; margin: -10px auto;}
    #login_img {width: 1000px; height: 700px; background: url(<c:url value='/images/main/01fmst_bg.jpg'/>) top center no-repeat;}
    #login_box {position: absolute; top: 487px; left: 184px; width: 630px; height: 40px;}
    #login_inp {position: absolute; top: -1px; left: 0px; width: 330px; height: 40px;}
    #login_btn {position: absolute; top: -1px; left: 320px; width: 115px; height: 40px;}
  	#login_opt {position: absolute; top: 2px; left: 456px; width: 200px; height: 40px;}
    #id, #password {width: 133px; height: 27px; margin: 0; padding: 0 10px; ime-mode: disabled; font-family : 맑은 고딕; font-size: 0.8em; color: #666; background-color: #EAF0FA; border: 1px solid #BBC0C8; line-height: 26px;}
    #saveid {vertical-align: middle;}
    .login_saveid, .login_changepw {font-family : 맑은 고딕; font-weight: bold; font-size: 0.7em; color: #666;}
    .login_changepw {margin-left: 19px;}
    @media screen and (-webkit-min-device-pixel-ratio:0) {
        #login_opt {top: 1px; left: 453px;}
      }
//-->
</style>
<title>현대엔지니어링 자산관리시스템(ePMS)</title>
<script type="text/javaScript" language="javascript">

function pf_actionLogin() {
    if ($("#companyCode").val() =="") {
        $("#companyCode").val("HD00");
    }
    if (document.loginForm.id.value =="") {
        alert("아이디를 입력하세요");
        document.loginForm.id.focus();
    } else if (document.loginForm.password.value =="") {
        alert("비밀번호를 입력하세요");
        document.loginForm.password.focus();
    } else {
    	pf_saveid(loginForm);
        document.loginForm.action="/com/actionLogin.do";
        //document.loginForm.j_username.value = document.loginForm.userSe.value + document.loginForm.username.value;
        //document.loginForm.action="<c:url value='/j_spring_security_check'/>";
        document.loginForm.submit();
    }
}

function pf_goHelp() {
	alert('도움말 찾기 준비 중입니다');
}

function pf_setCookie (name, value, expires) {
    document.cookie = name + "=" + escape (value) + "; path=/; expires=" + expires.toGMTString() + "; httpOnly";	//보안관련 설정추가 (2015.02.26 KYY)
}

function getCookie(Name) {
    var search = Name + "=";
    if (document.cookie.length > 0) { // 쿠키가 설정되어 있다면
        offset = document.cookie.indexOf(search);
        if (offset != -1) { // 쿠키가 존재하면
            offset += search.length;
            // set index of beginning of value
            end = document.cookie.indexOf(";", offset);
            // 쿠키 값의 마지막 위치 인덱스 번호 설정
            if (end == -1)
                end = document.cookie.length;
            return unescape(document.cookie.substring(offset, end));
        }
    }
    return "";
}

function pf_saveid(form) {
    var expdate = new Date();
    // 기본적으로 30일동안 기억하게 함. 일수를 조절하려면 * 30에서 숫자를 조절하면 됨
   expdate.setTime(expdate.getTime() + 1000 * 3600 * 24 * 7); // 30일
    pf_setCookie("pf_saveid", form.id.value, expdate);
}

function pf_getid(form) {
    form.id.value = getCookie("pf_saveid");
}

function pf_Init() {
    var message = document.loginForm.message.value;
    var account = document.loginForm.account.value;
    if (message != "") {
        if (account=="5") alert("비밀번호가 5회이상 틀리면 일시 사용중지가 됩니다!\r\n관리자에게 문의 하십시요!");
        	else alert(message);
    }

    pf_saveid(document.loginForm.id);
    pf_getid(document.loginForm.id);
    document.loginForm.id.focus();
}

</script>
</head>
<body onLoad="pf_Init();">
    <form name="loginForm" action="/com/actionLogin.do" method="post">
    <input type="hidden" name="message" value="${message}">
    <input type="hidden" name="account" value="${account}"/>
    <input type="hidden" id="companyCode" name="companyCode" value="${companyCode}"/>
    <div id="login_bg">
    </div>
    <div id="login_area">
        <div id="login_img">
            <div id="login_box">
                <div id="login_inp">
                    <input type="text" name="id" id="id" placeholder="아이디" value='' tabindex="1" maxlength="20"/>
                    <input type="password" name="password" id="password" placeholder="비밀번호" value='' tabindex="2" maxlength="20" onKeyDown="javascript:if (event.keyCode == 13) { pf_actionLogin(); }"/>
                </div>
                <div id="login_btn">
                    <a href="javascript:pf_actionLogin();" onFocus="this.blur()"><img src="<c:url value='/images/main/01fmst_btn.png'/>" border="0" width="109px"/></a>
                </div>
                <div id="login_opt">
                    <input type="checkbox" id="saveid" name="saveid" value=""/ onFocus="this.blur()"> <span class="login_saveid">아이디저장</span>
                    <!--<span class="login_changepw">비밀번호변경</span>-->
                </div>
            </div>
        </div>
    </div>
		</form>
</body>
</html>

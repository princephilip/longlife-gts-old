﻿<%@ page language="java" contentType="text/html; charset=utf-8"	session="true" pageEncoding="utf-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ page import ="xgov.core.vo.XLoginVO"%>
<%@ page import ="xgov.core.env.XConfiguration"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%
/**
 *<pre>
 * Statements
 *</pre>
 *
 * @Class Name : EgovMain.jsp
 * @Descriptio	  메인페이지
 * @Modification Information
 * @
 * @  수정일        		 수정자                   수정내용
 * @ -------    --------    ---------------------------
 * @ 2013.03.03    박영찬        최초 생성
 * @
 */
%>


<%
	// 세션 정보를 가져온다. LoginVO 
 	XLoginVO user = (XLoginVO)session.getAttribute("loginVO");
	
	StringBuffer sbSys = new StringBuffer();
	 
	sbSys.append("var _GlobalCache = new Array();");
	sbSys.append("var _SystemName = '세방TEC ERP';");
	sbSys.append("var _DBMS = 'Oracle';");
	sbSys.append("var _StringBuilder = '';");
	sbSys.append("var _MsgContent = '';");
	sbSys.append("var _Msg = '';");
	sbSys.append("var _SqlSecurityCheck = true;");
	sbSys.append("var _FooterHeight = 0;");
	sbSys.append("var _MyPgmList = null;");

	sbSys.append("var _rootDIR = '';");
	sbSys.append("var _rootFileUpload = '" + request.getContextPath() + "/FileUpload/';");
	sbSys.append("var _rootImageUpload = '" + request.getContextPath() + "/FileUpload/PHOTO/';");
	sbSys.append("var _CPATH ='" + request.getContextPath() + "';");

	sbSys.append("var _CompanyCode='" + user.getCompanyCode() + "';");
	sbSys.append("var _CompanyName='" + user.getCompanyName() + "';");
	sbSys.append("var _CompanyCodehm='100';");
	sbSys.append("var _UserID = '" + user.getUserId() + "';");
	sbSys.append("var _LoginID = '" + user.getLoginId() + "';");
	sbSys.append("var _DeptCode = '" + user.getDeptCode() + "';");
	sbSys.append("var _UserName = '" + user.getUserName() + "';");
	sbSys.append("var _DeptName = '" + user.getDeptName() + "';");
	sbSys.append("var _IdStatus = '" + user.getuseYesNo() + "';");
	sbSys.append("var _StdCode = '" + user.getstdCode() + "';");
	sbSys.append("var _Email = '" + user.getEMail() + "';");
	sbSys.append("var _MobileNo = '" + user.getMobileNo() + "';");
	sbSys.append("var _LoginTag = '" + user.getLoginLockYesNo() + "';");
	sbSys.append("var _ClientIP = '" + request.getRemoteAddr()  + "';");
	sbSys.append("var _CustCode = '" + user.getVendorCode()  + "';");
	sbSys.append("var _CustName = '" + user.getVendorName()  + "';");
	sbSys.append("var _UserTag = '" + user.getUserTag() + "';");
	sbSys.append("var _PasswordUpdate = '" + user.getPasswordUpdate() + "';");
	sbSys.append("var _PasswordExpire = " + user.getPasswordExpire() + ";");
	//sbSys.append("var _SePW = '" + user.getSePw() + "';");

	sbSys.append("var _SePW = '" + (String)session.getAttribute("sePw") + "';");

	//울트라 추가(2015.08.24 KYY)
	sbSys.append("var _AuthCode = '';");
	sbSys.append("var _UserRole = '';");
	sbSys.append("var _DataRole = '';");
	sbSys.append("var _MenuAuth = '';");
	sbSys.append("var _GradeCode = '"+ user.getGradeCode() +"';");
	sbSys.append("var _GradeName = '';");
	
	// 재가서비스용 마지막 센터
	sbSys.append("var _gsDeptCode = '';");
	
	String sysinfo = new StringBuffer( xgov.core.util.XBase64Coder.encodeString(sbSys.toString()) ).reverse().toString();

	String[] pops = xgov.core.dao.XDAO.XmlSelect(request, "array", "com", "POPUP", "popup", user.getCompanyCode(), "all", "˛", "¸").split("˛");
	String popids = "";
	
	StringBuffer sbPops = new StringBuffer();
	
	for(int i=0; i<pops.length; i++) {
		String[] popData = pops[i].split("¸");
		popids += (i==0 ? "" : "|") + popData[0];
	}
	sbPops.append("var _PopIds = '" + popids + "';");
	String popinfo = new StringBuffer( xgov.core.util.XBase64Coder.encodeString(sbPops.toString()) ).reverse().toString();

%>

<%@ include file="/Theme/jsp/egovmain.jsp"%>
<%@ page language="java" contentType="text/html; charset=utf-8"	session="true" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%
/**
  * @Class Name : main.jsp
  * @Description : 대쉬보드 메인 화면
  * @Modification Information
  * @
  * @  수정일        		 수정자                   수정내용
  * @ -------    --------    ---------------------------
  * @ 2015.04.14    김성한        최초 생성
  * @
  */
%>

<link rel="stylesheet" href="<c:url value='/css/main.css'/>" type="text/css">
<script type="text/javaScript" src="<c:url value='/js/main/main.js'/>"></script>

<div id="main_img">
	<!-- 업무보고 현황 위젯 -->
	<div id="main_report">
		<div id="report_head"></div>
		<div id="report_list" class="table_list">
			<table width="237" cellpadding="0" cellspacing="0" border="0">
				<thead>
					<tr>
						<td class="r_title" width="28%">대상사업장</td>
						<td class="r_title" width="18%">정상</td>
						<td class="r_title" width="18%">유의</td>
						<td class="r_title" width="18%">사고</td>
						<td class="r_title" width="18%">미보고</td>
					</tr>
				</thead>
				<tbody>
				<c:forEach var="i" begin="0" end="0" step="1">
					<tr>
						<td class="r_body" id="D0REPORT${i}">0</td>
						<td class="r_body" id="D1REPORT${i}">0</td>
						<td class="r_body" id="D2REPORT${i}">0</td>
						<td class="r_body" id="D3REPORT${i}">0</td>
						<td class="r_body" id="D4REPORT${i}">0</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</div>
	</div>

	<!-- 불편신고 접수 현황 위젯 -->
	<div id="main_voc">
		<div id="voc_head"></div>
		<div id="voc_list" class="table_list">
			<table width="541" cellpadding="0" cellspacing="0" border="0">
				<thead>
					<tr>
						<td class="v_title" width="20%">구분</td>
						<td class="v_title" width="20%">접수</td>
						<td class="v_title" width="20%">처리완료</td>
						<td class="v_title" width="20%">처리중</td>
						<td class="v_title" width="20%">처리지연</td>
					</tr>
				</thead>
				<tbody>
				<c:set var="itemlist" value="${fn:split('당일,당월,년누계', ',')}" />
				<c:forEach var="i" items="${itemlist}" varStatus="status">
					<tr>
						<td class="v_body t_color">${i}</td>
						<td class="v_body" id="D0VOC${status.index}">0</td>
						<td class="v_body" id="D1VOC${status.index}">0</td>
						<td class="v_body" id="D2VOC${status.index}">0</td>
						<td class="v_body" id="D3VOC${status.index}">0</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</div>
	</div>

	<!-- 순회관리 일일 점검계획 위젯 -->
	<div id="main_center">
		<div id="center_head"></div>
		<div id="center_list" class="table_list">
			<table width="284" cellpadding="0" cellspacing="0" border="0">
				<thead>
					<tr>
						<td class="c_title" width="20%">센터명</td>
						<td class="c_title" width="20%">수도</td>
						<td class="c_title" width="20%">중부</td>
						<td class="c_title" width="20%">호남</td>
						<td class="c_title" width="20%">영남</td>
					</tr>
				</thead>
				<tbody>
				<c:set var="itemlist" value="${fn:split('금일,명일', ',')}" />
				<c:forEach var="i" items="${itemlist}" varStatus="status">
					<tr>
						<td class="c_body t_color">${i}</td>
						<td class="c_body" id="CAPITAL${status.index}">0</td>
						<td class="c_body" id="MIDDLE${status.index}">0</td>
						<td class="c_body" id="HONAM${status.index}">0</td>
						<td class="c_body" id="YOUNGNAM${status.index}">0</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</div>
	</div>

	<!-- 공지사항 위젯 -->
	<div id="main_board">
		<div id="board_head"></div>
		<div id="board_more"></div>
		<div id="board_list" class="table_list">
			<table width="541" cellpadding="0" cellspacing="0" border="0">
				<thead>
					<tr>
						<td class="b_title" width="10%">No</td>
						<td class="b_title" width="50%">제목</td>
						<td class="b_title" width="20%">작성자</td>
						<td class="b_title" width="20%">작성일</td>
					</tr>
				</thead>
				<tbody>
				<c:forEach var="i" begin="0" end="4" step="1">
					<tr>
						<td class="b_body" id="NO${i}"></td>
						<td class="b_body"><div class="b_subject" id="SUBJECT${i}"></td>
						<td class="b_body" id="REGNAME${i}"></td>
						<td class="b_body" id="REGDATE${i}"></td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</div>
	</div>

	<!-- 오늘의 날씨 위젯 -->
	<div id="main_weather">
		<div id="site_head"></div>
		<div id="site_list" class="table_list">
			<table width="272" cellpadding="0" cellspacing="0" border="0">
				<thead>
					<tr>
						<td class="s_title" width="48%">구분</td>
						<td class="s_title" width="52%">사업장수(개)</td>
					</tr>
				</thead>
				<tbody>
				<c:set var="itemlist" value="${fn:split('국내,해외,합계', ',')}" />
				<c:forEach var="i" items="${itemlist}" varStatus="status">
					<tr>
						<td class="s_body t_color">${i}</td>
						<td class="s_body" id="SITECNT${status.index}">0</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</div>
		<div id="weather_today">
			<table width="207" cellpadding="0" cellspacing="0" border="0">
				<thead>
					<tr>
						<td class="w_title" width="100%" id="TODAY"></td>
					</tr>
				</thead>
				<tbody>
				<c:forEach var="i" begin="0" end="2" step="1">
					<tr>
						<td class="w_body" id="WEATHER${i}"></td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</div>
		<div id="weather_icon"></div>
	</div>

	<!-- 자산관리 현장 위젯 -->
	<div id="main_map">
		<div id="map_head"></div>
		<div id="map_more">
			<img src="/images/main/map.png" usemap="#map" />
			<map name="map" id="map">
				<area shape="rect" coords="140,48,253,60" onfocus="this.blur()" href="javascript:getMap(1);">
				<area shape="rect" coords="94,49,125,60" onfocus="this.blur()" href="javascript:getMap(2);">
				<area shape="rect" coords="128,67,157,76" onfocus="this.blur()" href="javascript:getMap(3);">
				<area shape="rect" coords="105,83,141,92" onfocus="this.blur()" href="javascript:getMap(4);">
				<area shape="rect" coords="146,80,170,89" onfocus="this.blur()" href="javascript:getMap(5);">
				<area shape="rect" coords="173,91,199,100" onfocus="this.blur()" href="javascript:getMap(6);">
				<area shape="rect" coords="141,98,168,108" onfocus="this.blur()" href="javascript:getMap(7);">
				<area shape="rect" coords="104,98,133,108" onfocus="this.blur()" href="javascript:getMap(8);">
				<area shape="rect" coords="73,110,104,122" onfocus="this.blur()" href="javascript:getMap(9);">
				<area shape="rect" coords="133,119,164,131" onfocus="this.blur()" href="javascript:getMap(10);">
				<area shape="rect" coords="183,127,214,139" onfocus="this.blur()" href="javascript:getMap(11);">
				<area shape="rect" coords="100,146,131,159" onfocus="this.blur()" href="javascript:getMap(12);">
				<area shape="rect" coords="131,212,161,224" onfocus="this.blur()" href="javascript:getMap(13);">
				<area shape="rect" coords="317,223,348,235" onfocus="this.blur()" href="javascript:getMap(14);">
				<area shape="rect" coords="307,259,337,271" onfocus="this.blur()" href="javascript:getMap(15);">
				<area shape="rect" coords="259,282,291,293" onfocus="this.blur()" href="javascript:getMap(16);">
				<area shape="rect" coords="108,273,140,287" onfocus="this.blur()" href="javascript:getMap(17);">
				<area shape="rect" coords="152,303,184,314" onfocus="this.blur()" href="javascript:getMap(18);">
			</map>
		</div>
		<div id="map_photo"></div>
		<div id="map_list">
			<table width="126" cellpadding="0" cellspacing="0" border="0">
				<tbody>
				<c:forEach var="i" begin="0" end="3" step="1">
					<tr>
						<td class="m_body m_dot" width="5%"><img src='/images/main/icon_date.png' /></td>
						<td class="m_body" width="95%" id="MAP${i}"></td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</div>
	</div>

	<!-- 퀵 메뉴 위젯 -->
	<div id="main_quick">
		<div id="quick_top"></div>
		<div id="quick_list">
			<table width="71" cellpadding="0" cellspacing="0" border="0">
				<tbody>
				<c:forEach var="i" begin="1" end="8" step="1">
					<tr>
						<td class="q_body" width="100%"><img src='/images/main/quick0${i}.png' id="QUICK${i}" onmouseover="this.src='/images/main/quick0${i}_on.png'" onmouseout="this.src='/images/main/quick0${i}.png'" /></td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</div>
		<div id="quick_bottom"></div>
	</div>
</div>

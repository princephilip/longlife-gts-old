<%--
  Class Name : EgovPopupRegist.jsp
  Description : 팝업창관리 등록 페이지
  Modification Information

      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2009.09.16    장동한          최초 생성

    author   : 공통서비스 개발팀 장동한
    since    : 2009.09.16

    Copyright (C) 2009 by MOPAS  All right reserved.
--%>
<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<title>팝업창관리 관리</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
<link type="text/css" charset="utf-8" href="/Mighty/3rd/jQuery/css/Aristo/Aristo.X-1.1.1.css" rel="stylesheet" media="all" />
<link href="<c:url value='/css/bbs.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='/css/button.css' />" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<c:url value="/com/validator.do"/>"></script>
<script type="text/javascript" src="<c:url value='/js/bbs/EgovBBSMng.js' />"></script>
<script type="text/javascript" src="<c:url value='/js/cal/EgovCalPopup.js' />"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/jQuery/js/jquery-1.11.2.min.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/jQuery/js/jquery.ui.datepicker-ko.min.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/js/mighty-1.2.7.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/js/mighty-win-1.2.7.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/js/mighty-mdi-1.2.7.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/jQuery/js/jquery-ui-1.11.3.custom/jquery-ui.min.js'/>"></script>
<validator:javascript formName="popupManageVO" staticJavascript="false" xhtml="true" cdata="false"/>
<script type="text/javaScript" language="javascript">
/* ********************************************************
 * 초기화
 ******************************************************** */
function fn_egov_init_PopupManage(){

}

function fn_SetDate(a_val,a_obj) {
	$("#"+a_obj.id).val(a_val.replace(/\-/g,''));
}

/* ********************************************************
 * 저장처리화면
 ******************************************************** */
function fn_egov_save_PopupManage(){
	var varFrom = document.popupManageVO;

	if(confirm("<spring:message code="common.save.msg" />")){
		varFrom.action =  "<c:url value='/pwm/registPopup.do' />";
		if(!validatePopupManageVO(varFrom)){
			return;
		}else{

			var ntceBgndeYYYMMDD = document.getElementById('ntceBgndeYYYMMDD').value;
			var ntceEnddeYYYMMDD = document.getElementById('ntceEnddeYYYMMDD').value;

			var iChkBeginDe = Number( ntceBgndeYYYMMDD.replaceAll("-","") );
			var iChkEndDe = Number( ntceEnddeYYYMMDD.replaceAll("-","") );

			if(iChkBeginDe > iChkEndDe || iChkEndDe < iChkBeginDe ){
				alert("게시시작일자는 게시종료일자 보다 클수 없고,\n게시종료일자는 게시시작일자 보다 작을수 없습니다. ");
				return;
			}

			varFrom.ntceBgnde.value = ntceBgndeYYYMMDD.replaceAll('-','') + fn_egov_SelectBoxValue('ntceBgndeHH') +  fn_egov_SelectBoxValue('ntceBgndeMM');
			varFrom.ntceEndde.value = ntceEnddeYYYMMDD.replaceAll('-','') + fn_egov_SelectBoxValue('ntceEnddeHH') +  fn_egov_SelectBoxValue('ntceEnddeMM');
			varFrom.submit();
		}
	}
}

function fn_egov_select_popupList() {
	document.popupManageVO.action = "<c:url value='/pwm/listPopup.do'/>";
	document.popupManageVO.submit();
}

/* ********************************************************
* RADIO BOX VALUE FUNCTION
******************************************************** */
function fn_egov_RadioBoxValue(sbName)
{
	var FLength = document.getElementsByName(sbName).length;
	var FValue = "";
	for(var i=0; i < FLength; i++)
	{
		if(document.getElementsByName(sbName)[i].checked == true){
			FValue = document.getElementsByName(sbName)[i].value;
		}
	}
	return FValue;
}
/* ********************************************************
* SELECT BOX VALUE FUNCTION
******************************************************** */
function fn_egov_SelectBoxValue(sbName)
{
	var FValue = "";
	for(var i=0; i < document.getElementById(sbName).length; i++)
	{
		if(document.getElementById(sbName).options[i].selected == true){

			FValue=document.getElementById(sbName).options[i].value;
		}
	}

	return  FValue;
}

function fn_egov_inqire_bbsInf(){
	var retVal;
	var url = "<c:url value='/bbs/openPopup.do' />?requestUrl=/bbs/selectBoardPop.do&width=850&height=520";
	var openParam = "dialogWidth: 900px; dialogHeight: 520px; resizable: 0, scroll: 1, center: 1";

	retVal = window.showModalDialog(url,"p_cmmntyInqire", openParam);
	if(retVal != null){
		var tmp = retVal.split("|");
		document.popupManageVO.nttId.value = tmp[0];
		document.popupManageVO.nttSj.value = tmp[1];
	}
}

/* ********************************************************
* PROTOTYPE JS FUNCTION
******************************************************** */
String.prototype.trim = function(){
	return this.replace(/^\s+|\s+$/g, "");
}

String.prototype.replaceAll = function(src, repl){
	 var str = this;
	 if(src == repl){return str;}
	 while(str.indexOf(src) != -1) {
	 	str = str.replace(src, repl);
	 }
	 return str;
}
</script>
</head>
<body onLoad="fn_egov_init_PopupManage();">
<DIV id="content" style="width:712px">
<!--  상단타이틀 -->
<form:form commandName="popupManageVO" name="popupManageVO" action="${pageContext.request.contextPath}/pwm/registPopup.do" method="post" >
<%-- noscript 테그 --%>
<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
<!-- ----------------- 상단 타이틀  영역 -->
<table width="100%" cellpadding="8" class="table-search" border="0" summary="상단 타이틀을 제공한다.">
 <tr>
  <td width="100%"class="title_left">
	  <img src="<c:url value='/images/bbs/tit_icon.gif' />" height="16" hspace="3" align="middle" alt="팝업창관리  등록">&nbsp;팝업창관리 등록</td>
 </tr>
</table>
<!--  줄간격조정  -->
<table width="100%" cellspacing="0" cellpadding="0" border="0" summary="화면 줄간격을 조정한다.">
<tr>
	<td height="3px"></td>
</tr>
</table>
<!--  등록  폼 영역  -->

<table width="100%" border="0" cellpadding="3" cellspacing="1" class="table-register" summary="팝업창관리  입력을 제공한다.">
  <tr>
    <th width="20%" height="23" class="required_text" nowrap ><label id="IdPopupTitleNm">*팝업창명</label></th>
    <td width="80%">
      <form:input path="popupTitleNm" size="73" cssClass="detail_input" maxlength="255"/>
    </td>
  </tr>

  <tr>
    <th width="20%" height="23" class="required_text" nowrap ><label id="IdFileUrl">*팝업창URL</label></th>
    <td width="80%">
      <form:input path="fileUrl" size="73" cssClass="detail_input" maxlength="255"/>
    </td>
  </tr>

  <tr>
    <th width="20%" height="23" nowrap ><label id="IdNttId">공지사항명</label></th>
    <td width="80%">
      <input name="nttSj" type="text" size="80"  maxlength="80" title="공지사항명" readonly />
      &nbsp;<a href="#LINK" onclick="fn_egov_inqire_bbsInf();" style="selector-dummy: expression(this.hideFocus=false);"><img src="<c:url value='/images/bbs/search.gif' />"
     			width="15" height="15" align="middle" alt="새창" /></a>
    </td>
  </tr>

  <tr>
    <th height="23" class="required_text" ><label id="IdPopupWlc">*팝업창위치</label></th>
    <td>
       가로 <form:input path="popupWlc" size="5" cssClass="detail_input" maxlength="10"/>  세로 <form:input path="popupHlc" size="5" cssClass="detail_input" maxlength="10"/>
    </td>
  </tr>

  <tr>
    <th height="23" class="required_text" ><label id="IdPopupWSize">*팝업창사이즈</label></th>
    <td>
  넓이 <form:input path="popupWSize" size="5" cssClass="detail_input" maxlength="5" />  높이<form:input path="popupHSize" cssClass="detail_input" size="5" maxlength="5"/>
    </td>
  </tr>

  <tr>
    <th width="20%" height="23" class="required_text" nowrap><label id="IdNtceEnddeHH">*게시 기간</label></th>
    <td width="80%">
	    <input type="text" name="ntceBgndeYYYMMDD" id="ntceBgndeYYYMMDD" size="10" maxlength="10" class="detail_date" readonly title="게시기간" onchange="javascript:fn_SetDate(this.value,ntceBgnde)" >
	    <form:select path="ntceBgndeHH">
	        <form:options items="${ntceBgndeHH}" itemValue="code" itemLabel="codeNm"/>
	    </form:select>H
	    <form:select path="ntceBgndeMM">
	        <form:options items="${ntceBgndeMM}" itemValue="code" itemLabel="codeNm"/>
	    </form:select>M
	    ~
	    <input type="text" name="ntceEnddeYYYMMDD" id="ntceEnddeYYYMMDD" size="10" maxlength="10" class="detail_date" readonly title="게시기간" onchange="javascript:fn_SetDate(this.value,ntceBgnde)" >
	    <form:select path="ntceEnddeHH">
	        <form:options items="${ntceEnddeHH}" itemValue="code" itemLabel="codeNm"/>
	    </form:select>H
	    <form:select path="ntceEnddeMM">
	        <form:options items="${ntceEnddeMM}" itemValue="code" itemLabel="codeNm"/>
	    </form:select>M
    </td>
  </tr>
  <tr>
    <th width="20%" height="23" class="required_text" nowrap ><label id="IdStopVewAt">*그만보기 설정 여부</label></th>
    <td width="80%">
		<input type="radio" name="stopVewAt" value="Y" checked>Y
		<input type="radio" name="stopVewAt" value="N">N
    </td>
  </tr>
  <tr>
    <th width="20%" height="23" class="required_text" nowrap ><label id="IdNtceAt">*계시 상태</label></th>
    <td width="80%">
		<input type="radio" name="ntceAt" value="Y" checked>Y
		<input type="radio" name="ntceAt" value="N">N
    </td>
  </tr>
</table>

<!--  줄간격조정  -->
<table width="100%" cellspacing="0" cellpadding="0" border="0" summary="화면 줄간격을 조정한다.">
<tr>
	<td height="3px"></td>
</tr>
</table>
<center>
<!--  목록/저장버튼  -->
<table border="0" cellspacing="0" cellpadding="0" align="center" summary="목록/저장 버튼을 제공한다.">
<tr>
	<td>
		<span class="bbsbutton">
			<input type="image" id="x_list" onclick="javascript:fn_egov_select_popupList(); return false;" src="<c:url value='/images/bbs/x_list.gif' />"  onmouseover="fn_set_imgover(this)" onmouseout="fn_set_imgout(this)">
		</span>
	</td>
	<td width="5px"></td>
	<td>
	  	<span class="bbsbutton">
    		<input type="image" id="x_save" onclick="JavaScript:fn_egov_save_PopupManage(); return false;" src="<c:url value='/images/bbs/x_save.gif' />"  onmouseover="fn_set_imgover(this)" onmouseout="fn_set_imgout(this)">
  		</span>
   </td>
</tr>
</table>
</center>
<form:hidden path="ntceBgnde" />
<form:hidden path="ntceEndde" />
<form:hidden path="bbsId" value="BBSMSTR_000000000001"/>
<form:hidden path="nttId" />

<input name="cmd" type="hidden" value="<c:out value='save'/>"/>
</form:form>
</DIV>
<script>
	$('.detail_date').datepicker( {
	    changeMonth: true,
	    changeYear: true,
	    dayNames: ['월요일', '화요일', '수요일', '목요일', '금요일', '토요일', '일요일'],         
	    dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],          
	    monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'],         
	    monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
	    nextText: '다음 달',
	    prevText: '이전 달',
	    showButtonPanel: true,          
	    currentText: '오늘 날짜',          
	    closeText: '닫기',
	    dateFormat: "yy-mm-dd",
	    showMonthAfterYear: true,   
	    yearSuffix: ' 년 ',   
	    monthSuffix: ' 월',   
	    showOtherMonths: true, // 나머지 날짜도 화면에 표시   
	    selectOtherMonths: true
	});
	</script>

</body>
</html>

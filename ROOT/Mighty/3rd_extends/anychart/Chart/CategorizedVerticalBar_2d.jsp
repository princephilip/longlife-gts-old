<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%
	//String ls_title = xgov.core.util.XUtility.E2K(request.getParameter("tt"));
	String ls_title = java.net.URLDecoder.decode(request.getParameter("tt").toString(),"utf-8");	
	ls_title = java.net.URLDecoder.decode(ls_title, "MS949");
	//String ls_title_x = xgov.core.util.XUtility.E2K(request.getParameter("tx"));
	String ls_title_x = java.net.URLDecoder.decode(request.getParameter("tx").toString(),"utf-8");	
	ls_title_x = java.net.URLDecoder.decode(ls_title_x, "MS949");
	//String ls_title_y = xgov.core.util.XUtility.E2K(request.getParameter("ty"));
	String ls_title_y = java.net.URLDecoder.decode(request.getParameter("ty").toString(),"utf-8");	
	ls_title_y = java.net.URLDecoder.decode(ls_title_y, "MS949");
	String ls_subsystem = request.getParameter("ss");
	String ls_param = request.getParameter("ps");
	String ls_xmlfile = request.getParameter("xf");
	String ls_xmlkey = request.getParameter("xk");
	String ls_data = "";
	String ls_chartData = "";
	
	if(ls_param!=null && !ls_param.equals("")){
		ls_data = xgov.core.dao.XDAO.XmlSelect(request,"array", ls_subsystem, ls_xmlfile, ls_xmlkey, ls_param.replace(",","¸"), null,null,null);
		if(ls_data!=null && !ls_data.equals("")){
			String 	 s_id = "";
			String[] rowData = ls_data.split("˛");
			
			for(int i=0; i<rowData.length; i++){
				String[] cellData = rowData[i].split("¸");
				if(!cellData[0].equals(s_id)) {
					ls_chartData += (i==0?"":"</series>") + "<series id='series" + cellData[0] + "' name='" + cellData[1] + "'>";
				}
				ls_chartData += "<point name='" + cellData[2] + "' y='" + cellData[3] + "' />";
				s_id = cellData[0];
			}
			ls_chartData += (ls_chartData.equals("")?"":"</series>");

		}
	}
	else{
		ls_chartData = "<series><point name='111' y='20' /></series>";
	}
	
%>

<anychart>
  <settings>
    <animation enabled="True" />
  </settings>
  <charts>
    <chart plot_type="CategorizedVertical">
      <data_plot_settings default_series_type="Bar">
        <bar_series point_padding="0.2" group_padding="1">
          <label_settings enabled="true">
            <background enabled="false" />
            <font color="DarkColor(%Color)" />
            
            <effects>
              <drop_shadow enabled="true" opacity="1" />
            </effects>
          </label_settings>
          <tooltip_settings enabled="True">
           
            <background>
              <border color="DarkColor(%Color)" />
            </background>
            <font color="DarkColor(%Color)" />
          </tooltip_settings>
        </bar_series>
      </data_plot_settings>
      <chart_settings>
        <title enabled="true">
          <text><%=ls_title%></text>
        </title>
        <axes>
          <x_axis>
            <title enabled="true">
              <text></text>
            </title>
          </x_axis>
          <y_axis>
            <title enabled="true">
              <text></text>
            </title>
          </y_axis>
        </axes>
      </chart_settings>
      <data>
<%=ls_chartData%>
      </data>
    </chart>
  </charts>
</anychart>

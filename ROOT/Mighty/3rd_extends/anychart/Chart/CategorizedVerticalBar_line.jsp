<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%
	//String ls_title = xgov.core.util.XUtility.E2K(request.getParameter("tt"));
	String ls_title = java.net.URLDecoder.decode(request.getParameter("tt").toString(),"utf-8");	
	ls_title = java.net.URLDecoder.decode(ls_title, "MS949");
	//String ls_title_x = xgov.core.util.XUtility.E2K(request.getParameter("tx"));
	String ls_title_x = java.net.URLDecoder.decode(request.getParameter("tx").toString(),"utf-8");	
	ls_title_x = java.net.URLDecoder.decode(ls_title_x, "MS949");
	//String ls_title_y = xgov.core.util.XUtility.E2K(request.getParameter("ty"));
	String ls_title_y = java.net.URLDecoder.decode(request.getParameter("ty").toString(),"utf-8");	
	ls_title_y = java.net.URLDecoder.decode(ls_title_y, "MS949");
	String ls_subsystem = request.getParameter("ss");
	String ls_param = request.getParameter("ps");
	String ls_xmlfile = request.getParameter("xf");
	String ls_xmlkey = request.getParameter("xk");
	String ls_data = "";
	String ls_chartData = "";
	
	if(ls_param!=null && !ls_param.equals("")){
		ls_data = xgov.core.dao.XDAO.XmlSelect(request,"array", ls_subsystem, ls_xmlfile, ls_xmlkey, ls_param.replace(",","¸"), null,null,null);
		if(ls_data!=null && !ls_data.equals("")){
			String 	 s_id = "";
			String[] rowData = ls_data.split("˛");
			
			for(int i=0; i<rowData.length; i++){
				String[] cellData = rowData[i].split("¸");
				if(!cellData[0].equals(s_id)) {
					ls_chartData += (i==0?"":"</series>") + "<series id='series" + cellData[0] + "' name='" + cellData[1] + "'>";
				}
				ls_chartData += "<point name='" + cellData[2] + "' y='" + cellData[3] + "' />";
				s_id = cellData[0];
			}
			ls_chartData += (ls_chartData.equals("")?"":"</series>");

		}
	}
	else{
		ls_chartData = "<series><point name='' y='' /></series>";
	}
	
%>

<anychart>
  <settings>
    <animation enabled="True" />
  </settings>
  <charts>
    <chart plot_type="CategorizedVertical">
      <chart_settings>
        <title enabled="true">
          <text><%=ls_title%></text>
        </title>
        <axes>
          <x_axis tickmarks_placement="Center">
            <title enabled="true">
              <text>Arguments</text>
            </title>
          </x_axis>
          <y_axis>
            <scale minimum="1" type="Logarithmic" log_base="5" />
            <title enabled="true">
              <text>Values:  {%AxisMin} - {%AxisMax}</text>
            </title>
          </y_axis>
        </axes>
      </chart_settings>
      <data_plot_settings default_series_type="Line">
        <line_series point_padding="0.2" group_padding="1">
          <label_settings enabled="true">
            <background enabled="false" />
            <font color="Rgb(45,45,45)" bold="true" size="9">
              <effects enabled="true">
                <glow enabled="true" color="White" opacity="1" blur_x="1.5" blur_y="1.5" strength="3" />
              </effects>
            </font>
            <format>{%YValue}{numDecimals:0}</format>
          </label_settings>
          <tooltip_settings enabled="true">
            <format>
Value: {%YValue}{numDecimals:2}
Argument: {%Name}
</format>
            <background>
              <border type="Solid" color="DarkColor(%Color)" />
            </background>
            <font color="DarkColor(%Color)" />
          </tooltip_settings>
          <marker_settings enabled="true" />
          <line_style>
            <line thickness="3" />
          </line_style>
        </line_series>
      </data_plot_settings>
      <data>
<%=ls_chartData%>
      </data>
    </chart>
  </charts>
</anychart>
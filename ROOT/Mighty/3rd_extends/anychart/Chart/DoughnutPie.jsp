<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%


	//String ls_title = xgov.core.util.XUtility.E2K(request.getParameter("tt"));
	String ls_title = java.net.URLDecoder.decode(request.getParameter("tt").toString(),"utf-8");	
	ls_title = java.net.URLDecoder.decode(ls_title, "MS949");
	//String ls_title_x = xgov.core.util.XUtility.E2K(request.getParameter("tx"));
	String ls_title_x = java.net.URLDecoder.decode(request.getParameter("tx").toString(),"utf-8");	
	ls_title_x = java.net.URLDecoder.decode(ls_title_x, "MS949");
	//String ls_title_y = xgov.core.util.XUtility.E2K(request.getParameter("ty"));
	String ls_title_y = java.net.URLDecoder.decode(request.getParameter("ty").toString(),"utf-8");	
	ls_title_y = java.net.URLDecoder.decode(ls_title_y, "MS949");
	
	// String _Row_Separate = "˛";
  // String _Col_Separate = "¸";
  // String _Field_Separate = "ª";
	// String _Field_Separate2 = "º";
			
	
	String ls_subsystem = request.getParameter("ss");
  //String ls_param = java.net.URLDecoder.decode(request.getParameter("ps").toString(),"utf-8");
  //ls_param = java.net.URLDecoder.decode(ls_param, "MS949");
	
	String ls_param = request.getParameter("ps");
	
	if(ls_param!=null && !ls_param.equals("")){
		ls_param = java.net.URLDecoder.decode(request.getParameter("ps").toString(),"utf-8");
    ls_param = java.net.URLDecoder.decode(ls_param, "MS949");
  }  
	
	String ls_xmlfile = request.getParameter("xf");
	String ls_xmlkey = request.getParameter("xk");
	String ls_data = "";
	String ls_chartData = "";
	
	if(ls_param!=null && !ls_param.equals("")){
		ls_data = xgov.core.dao.XDAO.XmlSelect(request,"array", ls_subsystem, ls_xmlfile, ls_xmlkey, ls_param.replace(",","¸"), null,null,null);
		if(ls_data!=null && !ls_data.equals("")){
			
			String[] rowData = ls_data.split("˛");
			
			ls_chartData +=  "<series name='산지' type='Pie'>";
			
			for(int i=0; i<rowData.length; i++){
				String[] cellData = rowData[i].split("¸");
				ls_chartData += " <point name='" + cellData[0] + "' y='" + cellData[1] + "' />";
			}
			ls_chartData += "</series>";

		}
	}
	else{
		ls_chartData = "<series><point name='' y='' /></series>";
	}
	
%>

<anychart>
  <settings>
    <animation enabled="True" />
  </settings>
  <charts>
    <chart plot_type="doughnut">
      <data_plot_settings enable_3d_mode="false">
        <pie_series>
          <tooltip_settings enabled="true">
            <position anchor="CenterTop" padding="10" />
          </tooltip_settings>
          <label_settings enabled="true">
            <position anchor="Center" valign="Center" halign="Center" padding="0" />
            <background enabled="false" />
            <position anchor="Center" valign="Center" halign="Center" padding="5%" />
            <format><![CDATA[{%Name}]]></format>	
            <font size="11" color="White">
              <effects>
                <drop_shadow enabled="true" distance="1" opacity="0.9" blur_x="2" blur_y="2" />
              </effects>
            </font>
            <format>{%YPercentOfSeries}{numDecimals:0}%</format>
          </label_settings>
          <marker_settings enabled="true">
            <marker type="None" />
            <states>
              <hover>
                <marker type="Circle" anchor="CenterTop" />
              </hover>
            </states>
          </marker_settings>
        </pie_series>
      </data_plot_settings>
      <data>
<%=ls_chartData%>
      </data>
      <chart_settings>
        <title enabled="true" padding="20">
          <text><%=ls_title%></text>
        </title>
      </chart_settings>
    </chart>
  </charts>
</anychart>
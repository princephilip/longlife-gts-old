/**
 * Object :  XG_GRID1_template.js
 * @Description : grid 1 개 type templete
 * @author :엑스인터넷정보 김양열
 * @since : 2013.02.10
 * @version : 1.1.0
 *
 * @Modification Information
 * <pre>
 *   since    	  author           Description
 *  ----------    --------    ---------------------------
 *   2013.02.10    김양열        최초생성
 *    
 */

var li_result = 100;
var _GridBorder=false;
var _DelTrigger=false;
var _PChild = true;
var _PFirst = false;
var _ibInserting = false;
var _ibDeleting = false;
var _ibRtrieveMsg = true;

/*==변경시작========================================================================================================================================================
	조회버튼 클릭 시
	Retrieve와 RowFocusChange에서 각각 예외 처리가 발생.
	총 두번의 예외처리가 발생.
	한번만 발생 하도록 처리 하기 위해 사용.
*/
var _ibRetriving = false;
//==변경끝==========================================================================================================================================================

/*==변경시작========================================================================================================================================================
	parent 그리드 row 삭제 시
	child 그리드의 삭제된 rowData를 담기 위해 사용.
	dataload() 함수에서 child 그리드 로드 시 담아둔 rowData를 dg_child._DeletedRows 속성안에 담는다.
	저장 시 parent 그리드의 rowData와 child 그리드의 rowData를 모두 삭제 처리 함.
*/
var _childDatas = null;
//==변경끝==========================================================================================================================================================

/*==변경시작========================================================================================================================================================
	복제 시 UpdateRow가 일어남과 동시에 InsertAfter 함수가 호출되어 InsertAfter에서 불필요한 로직을 수행하게 됨.
	_ibDuplicating = true; 시 InsertAfter 로직을 수행하지 않도록 함.
*/
var _ibDuplicating = false;
//==변경끝==========================================================================================================================================================

//Description : window onload 시 처리(grid setting)
//화면 디자인관련 요소들 초기화 작업
function x_InitForm(){
	if (typeof(x_InitForm2)!="undefined") {
		if (x_InitForm2()!=100) return;
	}
}

//grid layout를 체크한다(a_allcompleted 가 true 이면 전체 grid load 완료
function xe_GridLayoutComplete(a_dg, ab_allcompleted){
	if("SlickGrid"=="SlickGrid") { // global 변수 처리
		setTimeout( function() { a_dg._SlickGrid.resizeCanvas(); }, 0 );
	}
	
	if (typeof(xe_GridLayoutComplete2)!="undefined") {
		if (xe_GridLayoutComplete2(a_dg, ab_allcompleted)!=100) return;
	}
}

//데이타를 조회한다
function x_DAO_Retrieve(a_dg){
	/*==변경시작========================================================================================================================================================
		조회버튼 클릭 시
		Retrieve와 RowFocusChange에서 각각 예외 처리가 발생.
		총 두번의 예외처리가 발생.
		한번만 발생 하도록 처리
		확인 버튼 클릭 시 _ibRetriving = true
		아니오 버튼 클릭 시 _ibRetriving = false
		2015.03.10 : _ibRtrieveMsg = true; 일때만 변경데이타 체크 하도록 함.
	*/
	
	if (x_IsAllDataChanged()>0) {
		if(_ibRtrieveMsg) {
			if(_X.MsgBoxYesNo("변경된 자료가 있습니다 \r\n 계속 진행 하시겠습니까?")==2) {
				return;
			}
		}
		_ibRetriving = true;
	} else {
		_ibRetriving = false;
	}
	//==변경끝==========================================================================================================================================================
	
	if (typeof(x_DAO_Retrieve2)!="undefined") {
		if (x_DAO_Retrieve2(a_dg)!=100) return;
	}

	var param = new Array();
	a_dg.Retrieve(param);
}

//html input type 의 자료가 변경된 경우 발생처리
function xe_EditChanged(a_obj, a_val, a_oldVal){
	if (typeof(xe_EditChanged2)!="undefined") {
		if (xe_EditChanged2(a_obj, a_val, a_oldVal)!=100) return;
	}
}

//enter key event 발생시 호출
function xe_InputKeyEnter(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	if (typeof(xe_InputKeyEnter2)!="undefined") {
		if (xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey)!=100) return;
	}
}

//key down event 발생시 호출
function xe_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){
	if (typeof(xe_InputKeyDown2)!="undefined") {
		if (xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey)!=100) return;
	}
}

//데이타 저장
function x_DAO_Save(a_dg, childBtn){
	var li_result = 100;
	if (typeof(x_DAO_Save2)!="undefined") {
			if(x_DAO_Save2(a_dg)!=100) return;
	}

	if (x_IsAllDataChanged()>0&&x_DAO_ChkErr()==false) {
		li_result = -1;
		return;
	}

	if (li_result != 100) return;
	li_result = x_SaveGridData();
	if (li_result > 0) {
		x_DAO_Saved();
	}
}

//데이타 유효성 check
function x_DAO_ChkErr(){
	//필수입력항목 체크
	for(var i=0; i<window._Grids.length; i++){
		if (window._Grids[i]._Updatable) {
			if(window._Grids[i].MustInputCheck()>0){
				return false;
			}
		}			
	}
	
	//문자열 길이 체크
	for(var i=0; i<window._Grids.length; i++){
		if (window._Grids[i]._Updatable) {
			if(window._Grids[i].ByteInputCheck()>0){
				return false;
			}
		}
	}
	
	if (typeof(x_DAO_ChkErr2)!="undefined") {
		if (!x_DAO_ChkErr2()) return false;
	}
}

//자료를 저장한후 CALLBACK
function x_DAO_Saved(){
	/*==변경시작========================================================================================================================================================
		저장시 _childDatas 변수 값을 null 로 한다.
	*/
	_childDatas = null;
	//==변경끝==========================================================================================================================================================
	
	if (typeof(x_DAO_Saved2)!="undefined") {
		if (x_DAO_Saved2()!=100) return;
	}
}

//grid insert/add
function x_DAO_Insert(a_dg, row, childBtn){	
	/*==변경시작========================================================================================================================================================
		parent 그리드 Insert 시 
		child 그리드의 변경 데이타를 확인 하도록 함.
		변경 데이타가 있을경우 메세지 처리.
	*/
	if (dg_child.IsDataChanged()) {
		if(_childDatas!=null&&_childDatas.length<dg_child.GetChangedData().length) {
			if(_X.MsgBoxYesNo("변경된 자료가 있습니다 \r\n 계속 진행 하시겠습니까?")==2) {return;}
		}
	}
	//==변경끝==========================================================================================================================================================
	
	if (!childBtn && window._CurGrid && a_dg != window._CurGrid) {
		a_dg = window._CurGrid;
		if (row == null || row > 0) {
			row = a_dg.GetRow();
		}
	}
	else {
		row = row == null ? a_dg.GetRow() : row;
	}
	
	if (typeof(x_DAO_Insert2)!="undefined") {
		if (x_DAO_Insert2(a_dg, row)!=100) return;
	}
	//_ibInserting=false;//추가버튼 클릭 시 rowfocuschange에서 child예외처리 타도록  잠시 false 처리
	a_dg.InsertRow(row);
}

//grid insert 수행후 발생
//default 값 처리
function x_Insert_After(a_dg, rowIdx){
	if(_ibDuplicating) return;
	/*==변경시작========================================================================================================================================================
		child 그리드에서 InsertRow 시 mighty-dao-RealGrid에서 x_Insert_After를 호출.
		결과적으로 x_Insert_After와 x_Child_Insert_After를 각각 한번씩 총 두번 호출 함.
		child 에서 InsertRow를 하였을 경우 return 처리.
	*/
	if(a_dg==dg_child) return;
	//==변경끝==========================================================================================================================================================
	
	_ibInserting=false;
	if (typeof(x_Insert_After2)!="undefined") {
		if (x_Insert_After2(a_dg, rowIdx)!=100) return;
	}
}

//grid 복제
function x_DAO_Duplicate(a_dg, row, childBtn){
	if (!childBtn && window._CurGrid && a_dg != window._CurGrid) {
		a_dg = window._CurGrid;
	}
	var row = a_dg.GetRow();
	if(row<1) return;
	if (typeof(x_DAO_Duplicate2)!="undefined") {
		if (x_DAO_Duplicate2(a_dg, row)!=100) return;
	}
	_ibDuplicating = true;
	a_dg.DuplicateRow(row);
}

//grid 복제후
function x_Duplicate_After(a_dg, rowIdx){
	_ibDuplicating = false;
	if (typeof(x_Duplicate_After2)!="undefined") {
		if (x_Duplicate_After2(a_dg, rowIdx)!=100) return;
	}
}

//grid 삭제
function x_DAO_Delete(a_dg, childBtn){
	if (!childBtn && window._CurGrid && a_dg != window._CurGrid) {
		a_dg = window._CurGrid;
	}
	
	/*==변경시작========================================================================================================================================================
		parent 그리드에 데이터가 있는지 확인.
	*/
	if(a_dg.RowCount()<=0) {
		_X.MsgBox("삭제할 데이타가 없습니다.");
		return;
	}
	//==변경끝==========================================================================================================================================================
	
	_ibDeleting=true;
	if (typeof(x_DAO_Delete2)!="undefined") {
		if (x_DAO_Delete2(a_dg)!=100) {
			_ibDeleting=false;
			return;
		}
	}
	
	/*==변경시작========================================================================================================================================================
		parent 그리드 row 삭제 시
		child 그리드의 데이터가 있는지 확인 하도록 함.
		데이터가 있을 경우 메세지 처리.
		데이터가 없을 경우 commit 후 삭제된 child 그리드의 rowData를 _childDatas 변수에 대입.
		_DelTrigger = true 시 로직 변경.
	*/
	var result = false;
	
	if(_DelTrigger) {
		result = _X.MsgBoxYesNo("선택한 자료를 삭제 하시겠습니까?\r\n관련된 하위 자료들도 모두 삭제됩니다.")==1;
	} else {
		if(dg_child.RowCount() > 0) {
			_X.MsgBox("관련된 하위 자료를 모두 삭제한 후 삭제가 가능합니다.");
		} else {
			result = _X.MsgBoxYesNo("선택한 자료를 삭제 하시겠습니까?")==1;
		}
	}
	
	if(result) {
		if(!_DelTrigger && dg_child._DeletedRows.length > 0) {
			dg_child.GridCommit();
			_childDatas = dg_child._DeletedRows.slice();
		}
	} else {
		_ibDeleting=false;
		return;
	}
		
	a_dg.DeleteRow(a_dg.GetRow());
	_ibDeleting=false;
	
	if(_DelTrigger) {
		x_DAO_Save();
	}
	//==변경끝==========================================================================================================================================================
	if (typeof(x_DAO_Deleted)!="undefined") {
		if (x_DAO_Deleted(a_dg)!=100) return;
	}
}

//삭제 후
function x_DAO_Deleted(a_dg){
	if (typeof(x_DAO_Deleted2)!="undefined") {
		if (x_DAO_Deleted2(a_dg)!=100) return;
	}
}

//excel 저장
function x_DAO_Excel(a_dg, childBtn){
	if (!childBtn && window._CurGrid && a_dg != window._CurGrid) {
		a_dg = window._CurGrid;
	}
	
	if (typeof(x_DAO_Excel2)!="undefined") {
		if (x_DAO_Excel2(a_dg)!=100) return;
	}
	
	a_dg.ExcelExport();
}


//grid child insert/add
function x_DAO_Child_Insert(a_dg, row){
	if (typeof(x_DAO_Child_Insert2)!="undefined") {
		if (x_DAO_Child_Insert2(a_dg, row)!=100) return;
	}
	a_dg.InsertRow(row);
}

//grid child insert 수행후 발생
//default 값 처리
function x_Child_Insert_After(a_dg, rowIdx){
	if(_ibDuplicating) return;
	if (typeof(x_Child_Insert_After2)!="undefined") {
		if (x_Child_Insert_After2(a_dg, rowIdx)!=100) return;
	}
}

//Child_Retrieve
function x_DAO_Child_Retrieve(a_dg){
	if (typeof(x_DAO_Child_Retrieve2)!="undefined") {
		if (x_DAO_Child_Retrieve2(a_dg)!=100) return;
	}
}

//grid child 복제
function x_DAO_Child_Duplicate(a_dg, row){
	if (typeof(x_DAO_Child_Duplicate2)!="undefined") {
		if (x_DAO_Child_Duplicate2(a_dg, row)!=100) return;
	}
	_ibDuplicating = true;
	a_dg.DuplicateRow(row);
}

//grid child 복제후
function x_Duplicate_Child_After(a_dg, rowIdx){
	_ibDuplicating = false;
	if (typeof(x_Duplicate_Child_After2)!="undefined") {
		if (x_Duplicate_Child_After2(a_dg, rowIdx)!=100) return;
	}
}

//grid child 삭제
function x_DAO_Child_Delete(a_dg){
	/*==변경시작========================================================================================================================================================
		child 그리드에 데이터가 있는지 확인.
		child 그리드 row 삭제 시 메세지 추가.
	*/
	if(a_dg.RowCount()<=0) {
		_X.MsgBox("선택된 자료가 없습니다.");
		return;
	}
	//if(_X.MsgBoxYesNo("선택한 자료를 삭제 하시겠습니까?")==2) {return;}
	//==변경끝==========================================================================================================================================================
	
	if (typeof(x_DAO_Child_Delete2)!="undefined") {
		if (x_DAO_Child_Delete2(a_dg)!=100) return;
	}
	a_dg.DeleteRow(a_dg.GetRow());
}

//excel 저장
function x_DAO_Child_Excel(a_dg){
	if (typeof(x_DAO_Child_Excel2)!="undefined") {
		if (x_DAO_Child_Excel2(a_dg)!=100) return;
	}
	if(window._CurGrid==null) window._CurGrid=a_dg;
	window._CurGrid.ExcelExport();
}

//출력
function x_DAO_Print(){
	if (typeof(x_DAO_Print2)!="undefined") {
		if (x_DAO_Print2()!=100) return;
	}
}

//tab 변경시
function xe_TabChanging(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	if (typeof(xe_TabChanging2)!="undefined") {
		if (xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab)!=1) return 0;
	}
	
	/*==변경시작========================================================================================================================================================
		메세지 변경
		old : 변경된 데이터가 있습니다. 계속 진행 하시겠습니까? \n <확인>시 변경된 데이터는 저장되지 않습니다.
		new : 변경된 자료가 있습니다 \r\n 계속 진행 하시겠습니까?
	*/
	if (x_IsAllDataChanged()>0) {
    if(_X.MsgBoxYesNo("변경된 자료가 있습니다 \r\n 계속 진행 하시겠습니까?") == "2"){
       return 0;
    }
	}
	//==변경끝==========================================================================================================================================================
	
	return 1;
}

//tab 변경시
function xe_TabChanged(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	if (typeof(xe_TabChanged2)!="undefined") {
		if (xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab)!=100) return;
	}
}

//grid data 변경 시
function xe_GridDataChange(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	if (typeof(xe_GridDataChange2)!="undefined") {
		if (xe_GridDataChange2(a_dg, a_row, a_col, a_newvalue, a_oldvalue)!=100) return;
	}
}

//grid data 변경 후
function xe_GridDataChanged(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	if (typeof(xe_GridDataChanged2)!="undefined") {
		if (xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue)!=100) return;
	}
}

//grid row focus change
function xe_GridRowFocusChange(a_dg, a_newrow, a_oldrow){
	var lb_return = true;

	if (typeof(xe_GridRowFocusChange2)!="undefined") {
		lb_return =  xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow);
	}
	
	/*==변경시작========================================================================================================================================================
		조회버튼 클릭 시
		Retrieve와 RowFocusChange에서 각각 예외 처리가 발생.
		총 두번의 예외처리가 발생.
		한번만 발생 하도록 처리
		_ibRetriving = false 시 -> 예외 처리
		2015.03.10 : lb_return == 100 일때 변경자료를 체크 하지 않도록 함.
	*/
	if(lb_return || lb_return == 100) {
		if(a_dg==dg_parent){
			if(!_ibInserting&&!_ibDeleting) {
				if (!_ibRetriving&&dg_child.IsDataChanged()) {
					if(_childDatas!=null&&_childDatas.length==dg_child.GetChangedData().length) return true;
					if(lb_return != 100) {
						if(_X.MsgBoxYesNo("변경된 자료가 있습니다 \r\n 계속 진행 하시겠습니까?")==2) {
							return false;
						}
					}
				}
			}
		}
		return true;
	}
	//==변경끝==========================================================================================================================================================
	
	return false;
}

//grid row focus change
function xe_GridRowFocusChanged(a_dg, a_newrow){
	if (typeof(xe_GridRowFocusChanged2)!="undefined") {
		lb_return = xe_GridRowFocusChanged2(a_dg, a_newrow);
	} 
	if(a_dg==dg_parent&&a_newrow > 0){
		if (typeof(x_DAO_Child_Retrieve)!="undefined") {
			if (x_DAO_Child_Retrieve(dg_child)!=100) return lb_return;
		}
	}
}

//grid item focus change event
function xe_GridItemFocusChange(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
	if (typeof(xe_GridItemFocusChange2)!="undefined") {
		return xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol);
	}
}

//grid item focus changed event
function xe_GridItemFocusChanged(a_dg, a_newrow, a_newcol){
	if (typeof(xe_GridItemFocusChanged2)!="undefined") {
		xe_GridItemFocusChanged2(a_dg, a_newrow, a_newcol);
	}
}

//grid data loaded
function xe_GridDataLoad(a_dg){
	
	if (typeof(xe_GridDataLoad2)!="undefined") {
		if (xe_GridDataLoad2(a_dg)!=100) return;
	}
	
	if(a_dg==dg_parent){
		_ibRetriving = false;
		
		if(dg_parent.RowCount() <= 0) {             
//			xe_GridRowFocusChange(dg_parent, 1, 1);
//		} else {
//			/*==변경시작========================================================================================================================================================
//				parent data 없을때 _ibRetriving 값으로 false를 대입
//			*/
//			_ibRetriving = false;
//			//==변경끝==========================================================================================================================================================
			dg_child.Reset();
			//수정(2017.08.18 KYY)
			if(typeof(_X.Noty)!="undefined")
				_X.Noty("조회된 데이터가 없습니다.");
			else
				_X.MsgBox("조회된 데이터가 없습니다.");
		}
	} else {
		/*==변경시작========================================================================================================================================================
			child 그리드 dataload 시 _childDatas 변수에 값이 있는지 확인.
			값이 있을 경우 dg_child._DeletedRows 속성 값으로 _childDatas 의 값을 대입.
		*/
		if(_childDatas != null) {
			a_dg._DeletedRows = _childDatas.slice();
		}
		//==변경끝==========================================================================================================================================================
	}
}

//grid button click event
function xe_GridButtonClick(a_dg, a_row, a_col, a_colname){
	if (typeof(xe_GridButtonClick2)!="undefined") {
		if (xe_GridButtonClick2(a_dg, a_row, a_col, a_colname)!=100) return;
	}
}

//header click event
function xe_GridHeaderClick(a_dg, a_col, a_colname){
	if (typeof(xe_GridHeaderClick2)!="undefined") {
		if (xe_GridHeaderClick2(a_dg, a_col, a_colname)!=100) return;
	}
}

//item double click event
function xe_GridItemDoubleClick(a_dg, a_row, a_col, a_colname){
	if (typeof(xe_GridItemDoubleClick2)!="undefined") {
		if (xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname)!=100)  return;
	}
}

//item click event
function xe_GridItemClick(a_dg, a_row, a_col, a_colname){
	if (typeof(xe_GridItemClick2)!="undefined") {
		if (xe_GridItemClick2(a_dg, a_row, a_col, a_colname)!=100)  return;
	}
}

//tree item checked event
function xe_GridItemChecked(a_dg, a_itemIndex, a_checked){
	if (typeof(xe_GridItemChecked2)!="undefined") {
		if (xe_GridItemChecked2(a_dg, a_itemIndex, a_checked)!=100)  return;
	}
}

//tree item checked event
function xe_TreeItemExpanding(a_dg, a_itemIndex, a_rowId){
	if (typeof(xe_TreeItemExpanding2)!="undefined") {
		if (xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId)!=100)  return;
	}
}

// - anychart  point click event
function xe_ChartPointClick(a_dg, a_event){
	if (typeof(xe_ChartPointClick2)!="undefined") {
		if (xe_ChartPointClick2(a_dg, a_event)!=100)  return;
	}
}

//- anychart  point select event
function xe_ChartPointSelect(a_dg, a_event){
	if (typeof(xe_ChartPointSelect2)!="undefined") {
		if (xe_ChartPointSelect2(a_dg, a_event)!=100)  return;
	}
}

//- anychart  point mouse over event
function xe_ChartPointMouseOver(a_dg){
	if (typeof(xe_ChartPointMouseOver2)!="undefined") {
		if (xe_ChartPointMouseOver2(a_dg)!=100)  return;
	}
}

//- anychart  point mouse out  event
function xe_ChartPointMouseOut(a_dg, a_event){
	if (typeof(xe_ChartPointMouseOut2)!="undefined") {
		if (xe_ChartPointMouseOut2(a_dg, a_event)!=100)  return;
	}
}


//window close
function x_Close() {
	if (typeof(x_Close2)!="undefined") {
		if (x_Close2()!=100)  return;
	}

	if (x_IsAllDataChanged()>0) {
		if(_X.MsgBoxYesNo("변경된 자료가 있습니다 \r\n 프로그램을 종료 하시겠습니까?")==2) {return;}	
	}
	
	if(_IsModal || _IsPopup) {
		window.close();
	} else if(typeof(_X.CloseSheet)!="undefined") {
		_X.CloseSheet(window);	
	} else {
		_Caller.$("#findmodal").dialog("close");
	}
}

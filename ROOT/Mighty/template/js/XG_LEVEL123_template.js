/**
 * Object :  XG_LEVEL123_template.js
 * @Description : grid 3개 level 1, 2, 3 type templete
 * @author :엑스인터넷정보 이상규
 * @since : 2015.02.09
 * @version : 1.1.0
 *
 * @Modification Information
 * <pre>
 *   since    	  author           Description
 *  ----------    --------    ---------------------------
 *   2015.02.09    이상규        최초생성
 *    
 */

var li_result = 100;
var _GridBorder=false;
var _DelTrigger=false;
var _PChild = false;

var level_1_Name = null;//1번그리드이름
var level_2_Name = null;//2번그리드이름
var level_3_Name = null;//3번그리드이름
var _childDatas_1 = null;//2번 그리드 삭제 데이타
var _childDatas_2 = null;//3번그리드 삭제 데이타 
var _ibRetriving = false;//1번 그리드 focuschange 예외처리 사용
var _ibRetriving_2 = false;//2번 그리드 focuschange 예외처리 사용
var _ibInserting = false;//추가 시 focuschange 예외처리 사용
var _ibDuplicating = false;//복제 시 insertAfter 예외처리 사용
var _ibDeleting = false;//삭제 시 focuschange 예외처리 사용

//Description : window onload 시 처리(grid setting)
//화면 디자인관련 요소들 초기화 작업
function x_InitForm(){
	if (typeof(x_InitForm2)!="undefined") {
		if (x_InitForm2()!=100) return;
	}
}

//grid layout를 체크한다(a_allcompleted 가 true 이면 전체 grid load 완료
function xe_GridLayoutComplete(a_dg, ab_allcompleted){
	if("SlickGrid"=="SlickGrid") { // global 변수 처리
		setTimeout( function() { a_dg._SlickGrid.resizeCanvas(); }, 0 );
	}
	
	if (typeof(xe_GridLayoutComplete2)!="undefined") {
		if (xe_GridLayoutComplete2(a_dg, ab_allcompleted)!=100) return;
	}
}

//데이타를 조회한다
function x_DAO_Retrieve(a_dg){
	if(a_dg.id == "dg_1") {
		if (x_IsAllDataChanged()>0) {
			if(_X.MsgBoxYesNo("변경된 자료가 있습니다 \r\n 계속 진행 하시겠습니까?")==2) {return;}
			_ibRetriving = true;
			_ibRetriving_2 = true;
		} else {
			_ibRetriving = false;
			_ibRetriving_2 = true;
		}
	}
	
	if (typeof(x_DAO_Retrieve2)!="undefined") {
		if (x_DAO_Retrieve2(a_dg)!=100) return;
	}
	
	var param = new Array();
	a_dg.Retrieve(param);
}

//html input type 의 자료가 변경된 경우 발생처리
function xe_EditChanged(a_obj, a_val, a_oldVal){
	if (typeof(xe_EditChanged2)!="undefined") {
		if (xe_EditChanged2(a_obj, a_val, a_oldVal)!=100) return;
	}
}

//enter key event 발생시 호출
function xe_InputKeyEnter(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	if (typeof(xe_InputKeyEnter2)!="undefined") {
		if (xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey)!=100) return;
	}
}

//key down event 발생시 호출
function xe_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){
	if (typeof(xe_InputKeyDown2)!="undefined") {
		if (xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey)!=100) return;
	}
}

//데이타 저장
function x_DAO_Save(a_dg, childBtn){
	var li_result = 100;
	if (x_IsAllDataChanged()==0||x_DAO_ChkErr()==false) {
		li_result = -1;
		return;
	}
	if (typeof(x_DAO_Save2)!="undefined") {
			if(x_DAO_Save2(a_dg)!=100) return;
	}
	
	li_result = x_SaveGridData('Y');
	if (li_result > 0) x_DAO_Saved();
}

//데이타 유효성 check
function x_DAO_ChkErr(){
	//필수입력항목 체크
	for(var i=0; i<window._Grids.length; i++){
		if (window._Grids[i]._Updatable) {
			if(window._Grids[i].MustInputCheck()>0){
				return false;
			}
		}			
	}
	
	//문자열 길이 체크
	for(var i=0; i<window._Grids.length; i++){
		if (window._Grids[i]._Updatable) {
			if(window._Grids[i].ByteInputCheck()>0){
				return false;
			}
		}
	}
	
	if (typeof(x_DAO_ChkErr2)!="undefined")
		if (!x_DAO_ChkErr2()) return false;
}

//자료를 저장한후 CALLBACK
function x_DAO_Saved(){
	_childDatas_1 = null;
	_childDatas_2 = null;
	
	if (typeof(x_DAO_Saved2)!="undefined") {
		if (x_DAO_Saved2()!=100) return;
	}
}

//grid insert/add
function x_DAO_Insert(a_dg, row, childBtn){
	if (!childBtn && window._CurGrid && a_dg != window._CurGrid) {
		a_dg = window._CurGrid;
		if (row == null || row > 0) {
			row = a_dg.GetRow();
		}
	}
	else {
		row = row == null ? a_dg.GetRow() : row;
	}
	
	if(!window._IsRetrieving) {//조회된 상태에서 추가 가능
		_X.MsgBox("조회 후 추가 해 주십시오.");
		return;
	}
	
	if(a_dg.id == "dg_1") {
		if(dg_2.IsDataChanged()) {//2번그리드 변경데이타 체크
			if(_childDatas_1 == null || (_childDatas_1 != null && _childDatas_1.length < dg_2.GetChangedData().length)) {//하위 데이타 삭제 유무 확인
				if(_X.MsgBoxYesNo("변경된 자료가 있습니다 \r\n 계속 진행 하시겠습니까?") == "2") {
					return;
				}
			}
		}

		if(dg_3.IsDataChanged()) {//3번그리드 변경데이타 체크
			if(_childDatas_2 == null || (_childDatas_2 != null && _childDatas_2.length < dg_3.GetChangedData().length)) {//하위 데이타 삭제 유무 확인
				if(_X.MsgBoxYesNo("변경된 자료가 있습니다 \r\n 계속 진행 하시겠습니까?") == "2") {
					return;
				}
			}
		}
	}
	
	if(a_dg.id == "dg_2") {
		if(dg_1.RowCount() <= 0) {//상위 데이타 확인
			_X.MsgBox((level_1_Name == null ? "대분류" : level_1_Name) + " 선택 후 추가 해 주십시오.");
			return;
		}
		
		if(dg_1.GetRowState(dg_1.GetRow()) != "none") {//상위 데이타 변경상태 확인
			_X.MsgBox((level_1_Name == null ? "대분류" : level_1_Name) + "의 추가/변경 작업이 있습니다.\r\n완료(저장)한 후 추가 해 주십시오.");
			return;
		}
		
		if(dg_3.IsDataChanged()) {//하위 데이타 변경상태 확인
			if(_childDatas_2 == null || (_childDatas_2 != null && _childDatas_2.length < dg_3.GetChangedData().length)) {
				if(_X.MsgBoxYesNo("변경된 자료가 있습니다 \r\n 계속 진행 하시겠습니까?") == "2") {
					return;
				}
			}
		}
	}
	
	if(a_dg.id == "dg_3") {
		if(dg_2.RowCount() <= 0) {//상위 데이타 확인
			_X.MsgBox((level_2_Name == null ? "중분류" : level_2_Name) + " 선택 후 추가해 주십시오.");
			return;
		}
		
		if(dg_2.GetRowState(dg_2.GetRow()) != "none") {//상위 데이타 변경상태 확인
			_X.MsgBox((level_2_Name == null ? "중분류" : level_2_Name) + "의 추가/변경 작업이 있습니다.\r\n완료(저장)한 후 추가 해 주십시오.");
			return;
		}
	}
	
	if (typeof(x_DAO_Insert2)!="undefined") {
		if (x_DAO_Insert2(a_dg, row)!=100) return;
	}
	
	_ibInserting = true;
	a_dg.InsertRow(row);
}

//grid insert 수행후 발생
//default 값 처리
function x_Insert_After(a_dg, rowIdx){
	if(_ibDuplicating) return;
	_ibInserting = false;
	if (typeof(x_Insert_After2)!="undefined") {
		if (x_Insert_After2(a_dg, rowIdx)!=100) return;
	}
}

//grid 복제
function x_DAO_Duplicate(a_dg, row, childBtn){
	if (!childBtn && window._CurGrid && a_dg != window._CurGrid) {
		a_dg = window._CurGrid;
	}
	var row = a_dg.GetRow();
	if(row<1) return;
	if (typeof(x_DAO_Duplicate2)!="undefined") {
		if (x_DAO_Duplicate2(a_dg, row)!=100) return;
	}
	_ibDuplicating = true;
	a_dg.DuplicateRow(row);
}

//grid 복제후
function x_Duplicate_After(a_dg, rowIdx){
	_ibDuplicating = false;
	if (typeof(x_Duplicate_After2)!="undefined") {
		if (x_Duplicate_After2(a_dg, rowIdx)!=100) return;
	}
}
	
//grid 삭제
function x_DAO_Delete(a_dg, childBtn){
	if (!childBtn && window._CurGrid && a_dg != window._CurGrid) {
		a_dg = window._CurGrid;
	}
	
	if(a_dg.RowCount() <= 0) {//데이타 유무 확인
		_X.MsgBox("삭제할 데이타가 없습니다.");
		return;
	}
	
	_ibDeleting=true;
	
	if (typeof(x_DAO_Delete2)!="undefined") {
		if (x_DAO_Delete2(a_dg)!=100) {
			_ibDeleting=false;
			return;
		}
	}
	
	if(a_dg.id == "dg_1") {
		if(dg_2.RowCount() > 0) {//하위 데이타 확인
			_X.MsgBox("하위 항목이 존재 합니다.\r\n하위 항목을 모두 삭제한 후 삭제 해 주십시오.");
			return;
		}
	}
	
	if(a_dg.id == "dg_2") {
		if(dg_3.RowCount() > 0) {//하위 데이타 확인
			_X.MsgBox("하위 항목이 존재 합니다.\r\n하위 항목을 모두 삭제한 후 삭제 해 주십시오.");
			return;
		}
	}
	
	var result = false;
	var childDg = null;
	
	if(!_DelTrigger) {
		result = _X.MsgBoxYesNo("선택한 자료를 삭제 하시겠습니까?")==1;
	} else {
		result = _X.MsgBoxYesNo("선택한 자료를 삭제 하시겠습니까?\r\n관련된 하위 자료들도 모두 삭제됩니다.")==1;
	}
	
	if(result) {
		if(!_DelTrigger) {
			if(a_dg.id == "dg_1" && dg_2._DeletedRows.length > 0) {
				dg_2.GridCommit();
				_childDatas_1 = dg_2._DeletedRows.slice();//하위 그리드 삭제 데이타 저장
			} else if(a_dg.id == "dg_2" && dg_3._DeletedRows.length > 0) {
				dg_3.GridCommit();
				_childDatas_2 = dg_3._DeletedRows.slice();//하위 그리드 삭제 데이타 저장
			}
		}
	} else {
		_ibDeleting=false;
		return;
	}
	
	var deleteRow = a_dg.GetRow();
	a_dg.DeleteRow(a_dg.GetRow());
	_ibDeleting=false;
	
	if(_DelTrigger) {
		x_DAO_Save();
	}
	
	if(deleteRow == a_dg.GetRow())
		xe_GridRowFocusChanged(a_dg, a_dg.GetRow());
		
	if (typeof(x_DAO_Deleted)!="undefined") {
		if (x_DAO_Deleted(a_dg)!=100) return;
	}
}

//삭제 후
function x_DAO_Deleted(a_dg){
	if (typeof(x_DAO_Deleted2)!="undefined") {
		if (x_DAO_Deleted2(a_dg)!=100) return;
	}
}

//excel 저장
function x_DAO_Excel(a_dg, childBtn){
	if (!childBtn && window._CurGrid && a_dg != window._CurGrid) {
		a_dg = window._CurGrid;
	}
	
	if (typeof(x_DAO_Excel2)!="undefined") {
		if (x_DAO_Excel2(a_dg)!=100) return;
	}
	
	a_dg.ExcelExport();
}

//출력
function x_DAO_Print(){
	if (typeof(x_DAO_Print2)!="undefined") {
		if (x_DAO_Print2()!=100) return;
	}
}

//tab 변경시
function xe_TabChanging(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	if (typeof(xe_TabChanging2)!="undefined") {
		if (xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab)!=1) return 0;
	}
	if (x_IsAllDataChanged()>0) {
    if(_X.MsgBoxYesNo("변경된 자료가 있습니다 \r\n 계속 진행 하시겠습니까?") == "2"){
       return 0;
    }
	}
	return 1;
}
//tab 변경시
function xe_TabChanged(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	if (typeof(xe_TabChanged2)!="undefined") {
		if (xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab)!=100) return;
	}
}

//grid data 변경 시
function xe_GridDataChange(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	if (typeof(xe_GridDataChange2)!="undefined") {
		if (xe_GridDataChange2(a_dg, a_row, a_col, a_newvalue, a_oldvalue)!=100) return;
	}
}

//grid data 변경 후
function xe_GridDataChanged(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	if (typeof(xe_GridDataChanged2)!="undefined") {
		if (xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue)!=100) return;
	}
}

//grid row focus change
function xe_GridRowFocusChange(a_dg, a_newrow, a_oldrow){
	if(a_dg.id == "dg_1") {
		if(!_ibInserting && !_ibDeleting) {//추가, 삭제 여부 확인
			if(!_ibRetriving) {//조회 여부 확인
				if(dg_2.IsDataChanged()) {
					if(_childDatas_1 == null || (_childDatas_1 != null && _childDatas_1.length < dg_2.GetChangedData().length)) {//하위 데이타 삭제 상태 확인
						if(_X.MsgBoxYesNo("변경된 자료가 있습니다 \r\n 계속 진행 하시겠습니까?") == "2") {
							return false;
						}
					}
				}
				if(dg_3.IsDataChanged()) {
					if(_childDatas_2 == null || (_childDatas_2 != null && _childDatas_2.length < dg_3.GetChangedData().length)) {//하위 데이타 삭제 상태 확인
						if(_X.MsgBoxYesNo("변경된 자료가 있습니다 \r\n 계속 진행 하시겠습니까?") == "2") {
							return false;
						}
					}
				}
			} else {
				_ibRetriving = false;
			}
		}
	} else if(a_dg.id == "dg_2") {
		if(!_ibInserting && !_ibDeleting) {//추가, 삭제 여부 확인
			if(!_ibRetriving_2 && dg_3.IsDataChanged()) {//조회 여부 확인
				if(_childDatas_2 == null || (_childDatas_2 != null && _childDatas_2.length < dg_3.GetChangedData().length)) {//하위 데이타 삭제 상태 확인
					if(_X.MsgBoxYesNo("변경된 자료가 있습니다 \r\n 계속 진행 하시겠습니까?") == "2") {
						return false;
					}
				}
			} else {
				_ibRetriving_2 = false;
			}
		}
	}
	
	if (typeof(xe_GridRowFocusChange2)!="undefined") {
		var li_result = xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow);
		if(!li_result) return li_result;
	}
}

//grid row focus change
function xe_GridRowFocusChanged(a_dg, a_newrow){
	if (typeof(xe_GridRowFocusChanged2)!="undefined") {
		if(xe_GridRowFocusChanged2(a_dg, a_newrow)!=100) return;
	} 
	
	if(a_dg==dg_1&&a_newrow > 0){
		if (typeof(x_DAO_Retrieve)!="undefined") {
			x_DAO_Retrieve(dg_2);
		}
	}
	
	if(a_dg==dg_2&&a_newrow > 0){
		if (typeof(x_DAO_Retrieve)!="undefined") {
			x_DAO_Retrieve(dg_3);
		}
	}
}

//grid item focus change event
function xe_GridItemFocusChange(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
	if (typeof(xe_GridItemFocusChange2)!="undefined") {
		return xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol);
	}
}

//grid item focus changed event
function xe_GridItemFocusChanged(a_dg, a_newrow, a_newcol){
	if (typeof(xe_GridItemFocusChanged2)!="undefined") {
		xe_GridItemFocusChanged2(a_dg, a_newrow, a_newcol);
	}
}

//grid data loaded
function xe_GridDataLoad(a_dg){
	if(a_dg==dg_1) {
		if(a_dg.RowCount() <= 0) {//1번그리드 데이타 확인
			_ibRetriving = false;
			dg_2.Reset();
			dg_3.Reset();
			_X.MsgBox("조회된 데이터가 없습니다.");
		}
	} else if(a_dg==dg_2) {
		if(a_dg.RowCount() <= 0) {//2번그리드 데이타 확인
			_ibRetriving_2 = false;
			dg_3.Reset();
		}
		if(_childDatas_1 != null) {
			a_dg._DeletedRows = _childDatas_1.slice();//1번 데이타 삭제 시 2번 삭제 데이타 저장
		}
	} else {
		if(_childDatas_2 != null) {
			a_dg._DeletedRows = _childDatas_2.slice();//2번 데이타 삭제 시 3번 삭제 데이타 저장
		}
	}
	
	if (typeof(xe_GridDataLoad2)!="undefined") {
		if (xe_GridDataLoad2(a_dg)!=100) return;
	}
}

//grid button click event
function xe_GridButtonClick(a_dg, a_row, a_col, a_colname){
	if (typeof(xe_GridButtonClick2)!="undefined") {
		if (xe_GridButtonClick2(a_dg, a_row, a_col, a_colname)!=100) return;
	}
}

//header click event
function xe_GridHeaderClick(a_dg, a_col, a_colname){
	if (typeof(xe_GridHeaderClick2)!="undefined") {
		if (xe_GridHeaderClick2(a_dg, a_col, a_colname)!=100) return;
	}
}

//item double click event
function xe_GridItemDoubleClick(a_dg, a_row, a_col, a_colname){
	if (typeof(xe_GridItemDoubleClick2)!="undefined") {
		if (xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname)!=100)  return;
	}
}

//item click event
function xe_GridItemClick(a_dg, a_row, a_col, a_colname){
	if (typeof(xe_GridItemClick2)!="undefined") {
		if (xe_GridItemClick2(a_dg, a_row, a_col, a_colname)!=100)  return;
	}
}

//tree item checked event
function xe_GridItemChecked(a_dg, a_itemIndex, a_checked){
	if (typeof(xe_GridItemChecked2)!="undefined") {
		if (xe_GridItemChecked2(a_dg, a_itemIndex, a_checked)!=100)  return;
	}
}

//tree item checked event
function xe_TreeItemExpanding(a_dg, a_itemIndex, a_rowId){
	if (typeof(xe_TreeItemExpanding2)!="undefined") {
		if (xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId)!=100)  return;
	}
}

// - anychart  point click event
function xe_ChartPointClick(a_dg, a_event){
	if (typeof(xe_ChartPointClick2)!="undefined") {
		if (xe_ChartPointClick2(a_dg, a_event)!=100)  return;
	}
}

//- anychart  point select event
function xe_ChartPointSelect(a_dg, a_event){
	if (typeof(xe_ChartPointSelect2)!="undefined") {
		if (xe_ChartPointSelect2(a_dg, a_event)!=100)  return;
	}
}

//- anychart  point mouse over event
function xe_ChartPointMouseOver(a_dg){
	if (typeof(xe_ChartPointMouseOver2)!="undefined") {
		if (xe_ChartPointMouseOver2(a_dg)!=100)  return;
	}
}

//- anychart  point mouse out  event
function xe_ChartPointMouseOut(a_dg, a_event){
	if (typeof(xe_ChartPointMouseOut2)!="undefined") {
		if (xe_ChartPointMouseOut2(a_dg, a_event)!=100)  return;
	}
}


//window close
function x_Close() {
	if (typeof(x_Close2)!="undefined") {
		if (x_Close2()!=100)  return;
	}

	if (x_IsAllDataChanged()>0) {
		if(_X.MsgBoxYesNo("변경된 자료가 있습니다 \r\n 프로그램을 종료 하시겠습니까?")==2) {return;}	
	}
	
	if(_IsModal || _IsPopup) {
		window.close();
	} else if(typeof(_X.CloseSheet)!="undefined") {
		_X.CloseSheet(window);	
	} else {
		_Caller.$("#findmodal").dialog("close");
	}
}

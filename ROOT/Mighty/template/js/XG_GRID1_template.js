﻿/**
 * Object :  XG_GRID1_template.js
 * @Description : grid 1 개 type templete
 * @author :엑스인터넷정보 김양열
 * @since : 2013.02.10
 * @version : 1.1.0
 *
 * @Modification Information
 * <pre>
 *   since    	  author           Description
 *  ----------    --------    ---------------------------
 *   2013.02.10    김양열        최초생성
 *
 */
var li_result = 100;
var _GridBorder=false;
var _DelTrigger=false;
var _DelRowCountCheck=true; // 삭제시 그리드RowCount 체크여부. (2015.02.23 김영석)
var _RetChangeCheck = true; // 조회 시 그리드 변경사항 체크여부. (2015.03.23 이상규)
var _PChild = false;
var _ibDuplicating = false;//복제 시 insertAfter 실행하지 않도록 함.(2015.02.13 이상규)

//Description : window onload 시 처리(grid setting)
//화면 디자인관련 요소들 초기화 작업
function x_InitForm(){
	if (typeof(x_InitForm2)!="undefined") {
		res = x_InitForm2();
	}
}

//grid layout를 체크한다(a_allcompleted 가 true 이면 전체 grid load 완료
function xe_GridLayoutComplete(a_dg, ab_allcompleted){
	//if("SlickGrid"=="SlickGrid") { // global 변수 처리
		//setTimeout( function() { a_dg._SlickGrid.resizeCanvas(); }, 0 );
		a_dg._SlickGrid.resizeCanvas()
	//}

	if (typeof(xe_GridLayoutComplete2)!="undefined") {
		a_dg.ResizeCanvas();
		setTimeout( function() { xe_GridLayoutComplete2( a_dg, ab_allcompleted ); }, 100 );
		//if (xe_GridLayoutComplete2(a_dg, ab_allcompleted)!=100) return;
	}
}

//데이타를 조회한다
function x_DAO_Retrieve(a_dg, childBtn){
	if ( childBtn ) {
		if ( a_dg.IsDataChanged() ) {
			if(_X.MsgBoxYesNo("변경된 자료가 있습니다 \r\n 계속 진행 하시겠습니까?")==2) {return;}
		}
	}
	else if (_RetChangeCheck && x_IsAllDataChanged()>0) {
		if(_X.MsgBoxYesNo("변경된 자료가 있습니다 \r\n 계속 진행 하시겠습니까?")==2) {return;}
	}

	if ( a_dg && a_dg._ChildGrids ) {
		for ( var i = 0, ii = a_dg._ChildGrids.length; i < ii; i++ ) {
			a_dg._ChildGrids[i].Reset();
		}
	}

	if (typeof(x_DAO_Retrieve2)!="undefined") {
		if (x_DAO_Retrieve2(a_dg)!=100) return;
	}

	var param = new Array();
	a_dg.Retrieve(param);
}

//html input type 의 자료가 변경된 경우 발생처리
function xe_EditChanged(a_obj, a_val, a_oldVal){
	if (typeof(xe_EditChanged2)!="undefined") {
		if (xe_EditChanged2(a_obj, a_val, a_oldVal)!=100) return;
	}
}

//document key event 발생 시 호출
function xe_DocumentKeyDown(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	//저장 ctrl + s
	if ( event.keyCode == KEY_s && a_ctrlKey && !a_altKey && !a_shiftKey ) {
		var saveBtn = $("#xBtnSave")[0];
		if ( saveBtn ) {
			saveBtn.click();
		}

		return false;
	}
	//조회 ctrl + r
	else if ( event.keyCode == KEY_r && a_ctrlKey && !a_altKey && !a_shiftKey ) {
		var retrieveBtn = $("#xBtnRetrieve")[0];
		if ( retrieveBtn ) {
			retrieveBtn.click();
		}

		return false;
	}
	//삽입 ctrl + i
	else if ( event.keyCode == KEY_i && a_ctrlKey && !a_altKey && !a_shiftKey ) {
		var insertBtn = $("#xBtnInsert")[0];
		if ( insertBtn ) {
			insertBtn.click();
		}

		return false;
	}
	//추가 ctrl + a
	else if ( event.keyCode == KEY_a && a_ctrlKey && !a_altKey && !a_shiftKey ) {
		var appendBtn = $("#xBtnAppend")[0];
		if ( appendBtn ) {
			appendBtn.click();
		}

		return false;
	}
	//복제 ctrl + d
	else if ( event.keyCode == KEY_d && a_ctrlKey && !a_altKey && !a_shiftKey ) {
		var duplicateBtn = $("#xBtnDuplicate")[0];
		if ( duplicateBtn ) {
			duplicateBtn.click();
		}

		return false;
	}
	//삭제 ctrl + del
	else if ( event.keyCode == KEY_DELETE && a_ctrlKey && !a_altKey && !a_shiftKey ) {
		var deleteBtn = $("#xBtnDelete")[0];
		if ( deleteBtn ) {
			deleteBtn.click();
		}

		return false;
	}
	//출력 ctrl + p
	else if ( event.keyCode == KEY_p && a_ctrlKey && !a_altKey && !a_shiftKey ) {
		var printBtn = $("#xBtnPrint")[0];
		if ( printBtn ) {
			printBtn.click();
		}

		return false;
	}
	//엑셀 ctrl + e
	else if ( event.keyCode == KEY_e && a_ctrlKey && !a_altKey && !a_shiftKey ) {
		var excelBtn = $("#xBtnExcel")[0];
		if ( excelBtn ) {
			excelBtn.click();
		}

		return false;
	}
	//닫기 ctrl + F4
	else if ( event.keyCode == KEY_F4 && a_ctrlKey && !a_altKey && !a_shiftKey ) {
		var closeBtn = $("#xBtnClose")[0];
		if ( closeBtn ) {
			closeBtn.click();
		}

		return false;
	}
}

//enter key event 발생시 호출
function xe_InputKeyEnter(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	if (typeof(xe_InputKeyEnter2)!="undefined") {
		if (xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey)!=100) return;
	}
}

//key down event 발생시 호출
function xe_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){
	if (typeof(xe_InputKeyDown2)!="undefined") {
		if (xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey)!=100) return;
	}
}

//데이타 저장
function x_DAO_Save(a_dg, childBtn){
	if (typeof(x_DAO_Save2)!="undefined") {
			if(x_DAO_Save2(a_dg)!=100) return;
	}

	var li_result = 100;
//	if (x_IsAllDataChanged()==0||x_DAO_ChkErr()==false) {
//		li_result = -1;
//		return;
//	}
	if (x_DAO_ChkErr()==false) {
		li_result = -1;
		return;
	}
	li_result = x_SaveGridData('Y');
	if (li_result > 0) x_DAO_Saved();
}

//데이타 유효성 check
function x_DAO_ChkErr(){
	//필수입력항목 체크
	for(var i=0; i<window._Grids.length; i++){
		if (window._Grids[i]._Updatable) {
			if(window._Grids[i].MustInputCheck()>0){
				return false;
			}
		}
	}

	//문자열 길이 체크
	for(var i=0; i<window._Grids.length; i++){
		if (window._Grids[i]._Updatable) {
			if(window._Grids[i].ByteInputCheck()>0){
				return false;
			}
		}
	}

	if (typeof(x_DAO_ChkErr2)!="undefined")
		return x_DAO_ChkErr2();
}

//자료를 저장한후 CALLBACK
function x_DAO_Saved(){
	if (typeof(x_DAO_Saved2)!="undefined") {
		if (x_DAO_Saved2()!=100) return;
	}
}

//grid insert/add
function x_DAO_Insert(a_dg, row, childBtn){
	if (!childBtn && window._CurGrid && a_dg != window._CurGrid) {
		a_dg = window._CurGrid;
		if (row == null || row > 0) {
			row = a_dg.GetRow();
		}
	}
	else {
		if ( typeof row == "undefined" ) {
			row = a_dg.GetRow();
		}
	}

	if ( typeof x_DAO_Insert2 != "undefined" ) {
		var rtVal = x_DAO_Insert2(a_dg, row);

		if ( rtVal != 100 && rtVal != 30 && rtVal != 50 && rtVal != 55 && rtVal != 70 && rtVal != 75 ) {
			return;
		}

		if ( rtVal == 30 ) {
			if ( a_dg._ChildGrids ) {
				for ( var i = 0, ii = a_dg._ChildGrids.length; i < ii; i++ ) {
					if ( a_dg._ChildGrids[i].IsDataChanged() ) {
						if ( _X.MsgBoxYesNo("확인", "하위 정보에 변경된 내용이 있습니다.\n계속 진행 하실경우 변경된 내용이 초기화 됩니다.\r\n계속 진행 하시겠습니까?") === "1" ) {
							break;
						} else {
							return;
						}
					}
				}

				if ( row > 0 ) {
					for ( var j = 0, jj = a_dg._ChildGrids.length; j < jj; j++ ) {
						a_dg._ChildGrids[j].Reset();
					}
				}
			}
		}
		else if ( rtVal == 50 || rtVal == 55 ) {
			if ( a_dg._ParentGrid ) {
				if ( a_dg._ParentGrid.RowCount() <= 0 ) {
					_X.Noty("선택된 상위 정보가 없습니다.", "확인", null, 400);
					return;
				}

				if ( rtVal == 50 ) {
					var cData = a_dg._ParentGrid.GetChangedData();

					if ( cData != null && cData != "" ) {
						_X.Noty("상위 정보에 변경된 내용이 있습니다.<br>저장 또는 조회 후  다시 시도하여 주십시오.", "확인", null, 700);
						return;
					}
				}
			}

			if ( a_dg._ChildGrids ) {
				for ( var i = 0, ii = a_dg._ChildGrids.length; i < ii; i++ ) {
					if ( a_dg._ChildGrids[i].IsDataChanged() ) {
						if ( _X.MsgBoxYesNo("확인", "하위 정보에 변경된 내용이 있습니다.\n계속 진행 하실경우 변경된 내용이 초기화 됩니다.\r\n계속 진행 하시겠습니까?") === "1" ) {
							break;
						} else {
							return;
						}
					}
				}

				if ( row > 0 ) {
					for ( var j = 0, jj = a_dg._ChildGrids.length; j < jj; j++ ) {
						a_dg._ChildGrids[j].Reset();
					}
				}
			}
		}
		else if ( rtVal == 70 || rtVal == 75 ) {
			if ( a_dg._ParentGrid ) {
				if ( a_dg._ParentGrid.RowCount() <= 0 ) {
					_X.Noty("선택된 상위 정보가 없습니다.", "확인", null, 400);
					return;
				}

				if ( rtVal == 70 ) {
					var cData = a_dg._ParentGrid.GetChangedData();

					if ( cData != null && cData != "" ) {
						_X.Noty("상위 정보에 변경된 내용이 있습니다.<br>저장 또는 조회 후  다시 시도하여 주십시오.", "확인", null, 700);
						return;
					}
				}
			}
		}
	}

	a_dg.InsertRow(row);
}

//grid insert 수행후 발생
//default 값 처리
function x_Insert_After(a_dg, rowIdx){
	if(_ibDuplicating) return;
	if (typeof(x_Insert_After2)!="undefined") {
		if (x_Insert_After2(a_dg, rowIdx)==100) {
			_X.FormSetDisable( a_dg._SyncObject, false );
		}
	}
}

//grid 복제
function x_DAO_Duplicate(a_dg, childBtn){
	if (!childBtn && window._CurGrid && a_dg != window._CurGrid) {
		a_dg = window._CurGrid;
	}

	var row = a_dg.GetRow();

	if ( row < 1 ) {
		return;
	}

	if ( typeof x_DAO_Duplicate2 != "undefined" ) {
		var rtVal = x_DAO_Duplicate2(a_dg, row);

		if ( rtVal != 100 && rtVal != 30 && rtVal != 50 && rtVal != 55 && rtVal != 70 && rtVal != 75 ) {
			return;
		}

		if ( rtVal == 30 ) {
			if ( a_dg._ChildGrids ) {
				for ( var i = 0, ii = a_dg._ChildGrids.length; i < ii; i++ ) {
					if ( a_dg._ChildGrids[i].IsDataChanged() ) {
						if ( _X.MsgBoxYesNo("확인", "하위 정보에 변경된 내용이 있습니다.\n계속 진행 하실경우 변경된 내용이 초기화 됩니다.\r\n계속 진행 하시겠습니까?") === "1" ) {
							break;
						} else {
							return;
						}
					}
				}
			}
		}
		else if ( rtVal == 50 || rtVal == 55 ) {
			if ( a_dg._ParentGrid ) {
				if ( rtVal == 50 ) {
					var cData = a_dg._ParentGrid.GetChangedData();

					if ( cData != null && cData != "" ) {
						_X.Noty("상위 정보에 변경된 내용이 있습니다.<br>저장 또는 조회 후 다시 시도하여 주십시오.", "확인", null, 700);
						return;
					}
				}
			}

			if ( a_dg._ChildGrids ) {
				for ( var i = 0, ii = a_dg._ChildGrids.length; i < ii; i++ ) {
					if ( a_dg._ChildGrids[i].IsDataChanged() ) {
						if ( _X.MsgBoxYesNo("확인", "하위 정보에 변경된 내용이 있습니다.\n계속 진행 하실경우 변경된 내용이 초기화 됩니다.\r\n계속 진행 하시겠습니까?") === "1" ) {
							break;
						} else {
							return;
						}
					}
				}
			}
		}
		else if ( rtVal == 70 ) {
			if ( a_dg._ParentGrid ) {
				var cData = a_dg._ParentGrid.GetChangedData();

				if ( cData != null && cData != "" ) {
					_X.Noty("상위 정보에 변경된 내용이 있습니다.<br>저장 또는 조회 후 다시 시도하여 주십시오.", "확인", null, 700);
					return;
				}
			}
		}
	}

	_ibDuplicating = true;
	a_dg.DuplicateRow(row);
}

//grid 복제후
function x_Duplicate_After(a_dg, rowIdx){
	_ibDuplicating = false;
	if (typeof(x_Duplicate_After2)!="undefined") {
		if (x_Duplicate_After2(a_dg, rowIdx)!=100) return;
	}
}

//grid 삭제
function x_DAO_Delete(a_dg, childBtn){
	if (!childBtn && window._CurGrid && a_dg != window._CurGrid) {
		a_dg = window._CurGrid;
	}

	//그리드에 데이타가 있는지 확인(2015.02.12 이상규)
	if(_DelRowCountCheck && a_dg.RowCount()<=0) {
		_X.Noty("삭제할 데이타가 없습니다.");
		return;
	}

	var retVal;

	if (typeof(x_DAO_Delete2)!="undefined") {
		retVal = x_DAO_Delete2(a_dg);
	}

	if (retVal == 100) {
		//삭제시 저장처리
		if(_DelTrigger) {
			var li_rc = _X.MsgBoxYesNo("확인","자료를 삭제 하시겠습니까?\r\n삭제된 자료는 복구 되지 않습니다!");
			if(li_rc==2) return;
			a_dg.DeleteRow(a_dg.GetRow());
			x_DAO_Save();
			return;
		}
		a_dg.DeleteRow(a_dg.GetRow());

		if (typeof(x_DAO_Deleted)!="undefined") {
			if (x_DAO_Deleted(a_dg)!=100) return;
		}
	}
	else if (retVal == 50) {
		var rows = a_dg.GetCheckedRows();
		var row  = a_dg.GetRow();

		if ( rows.length > 0 ) {
			if ( _X.MsgBoxYesNo( "확인", "선택된 행들을 일괄 삭제 하시겠습니까?" ) === "1" ) {
				var datas = a_dg.GetData();

				for ( var i = rows.length - 1; i >= 0; i-- ) {
					a_dg._DeletedRows.push( datas.splice( rows[i] - 1, 1 )[0] );
				}

				var dRows = $.extend(true, [], a_dg._DeletedRows);

				a_dg.ClearRows();
				a_dg.SetData( datas );
				a_dg.SetRow( row );

				a_dg._DeletedRows = dRows;
			}
		}
		else {
			a_dg.DeleteRow(row);
		}

		if (typeof(x_DAO_Deleted)!="undefined") {
			if (x_DAO_Deleted(a_dg)!=100) return;
		}
	}
}

//삭제 후
function x_DAO_Deleted(a_dg){
	if (typeof(x_DAO_Deleted2)!="undefined") {
		if (x_DAO_Deleted2(a_dg)!=100) return;
	}
}

//excel 저장
function x_DAO_Excel(a_dg, childBtn){
	if (!childBtn && window._CurGrid && a_dg != window._CurGrid) {
		a_dg = window._CurGrid;
	}

	if (typeof(x_DAO_Excel2)!="undefined") {
		if (x_DAO_Excel2(a_dg)!=100) return;
	}

	a_dg.ExcelExport(true);
}

//출력
function x_DAO_Print(){
	if (typeof(x_DAO_Print2)!="undefined") {
		if (x_DAO_Print2()!=100) return;
	}
}

//tab 변경시
function xe_TabChanging(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab, a_new_tabno, a_old_tabno){
	if ( typeof xe_TabChanging2 != "undefined" ) {
		var rtVal = xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab, a_new_tabno, a_old_tabno);

		if ( rtVal == 100 ) {//return 100일 경우 하위로직 수행하지 않고 바로 changing (2015.03.03 이상규)
			return 1;
		}
		else if ( typeof rtVal === "object" ) {
			if ( rtVal ) {
				if ( rtVal.IsDataChanged() ) {
					if ( _X.MsgBoxYesNo("확인", "변경된 내용이 있습니다.\n계속 진행 하실경우 변경된 내용이 초기화 됩니다.\r\n계속 진행 하시겠습니까?") === "2" ) {
						return 0;
					}
				}

				if ( rtVal._ChildGrids ) {
					for ( var i = 0, ii = rtVal._ChildGrids.length; i < ii; i++ ) {
						if ( rtVal._ChildGrids[i].IsDataChanged() ) {
							if ( _X.MsgBoxYesNo("확인", "변경된 내용이 있습니다.\n계속 진행 하실경우 변경된 내용이 초기화 됩니다.\r\n계속 진행 하시겠습니까?") === "1" ) {
								break;
							} else {
								return 0;
							}
						}
					}

					for ( var j = 0, jj = rtVal._ChildGrids.length; j < jj; j++ ) {
						rtVal._ChildGrids[j].Reset();
					}
				}

				return 1;
			}
		}
		else if ( rtVal == 1 ) {
			if ( x_IsAllDataChanged() > 0 ) {
		    if ( _X.MsgBoxYesNo("확인", "변경된 내용이 있습니다.\n계속 진행 하실경우 변경된 내용이 초기화 됩니다.\r\n계속 진행 하시겠습니까?") == "2" ) {
		       return 0;
		    }
		  }

		  //tab변경 시 모든 그리드 Reset retreive 예외처리 x focuschange 예외처리 x (2015.03.03 이상규)
		  for ( var name in window._Grids ) {
				window._Grids[name].Reset();
			}

			return 1;
		}
	}

	return 0;
}

//tab 변경시
function xe_TabChanged(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab, a_nav, preNav, a_new_tabno, a_old_tabno){
	if (typeof(xe_TabChanged2)!="undefined") {
		if (xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab, a_new_tabno, a_old_tabno)!=100) return;

		var ctab    = $(a_nav).data("ctab");
		var preCtab = $(preNav).data("ctab");

		if ( preCtab != "" ) {
			$(preCtab).hide();
		}
		if ( ctab != "" ) {
			$(ctab).show();
		}

		xe_BodyResize();
	}
}

//grid data 변경 시
function xe_GridDataChange(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	if (typeof(xe_GridDataChange2)!="undefined") {
		if (xe_GridDataChange2(a_dg, a_row, a_col, a_newvalue, a_oldvalue)!=100) return;
	}
}

//grid data 변경 후
function xe_GridDataChanged(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	if (typeof(xe_GridDataChanged2)!="undefined") {
		if (xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue)!=100) return;
	}
}

//grid row focus change
function xe_GridRowFocusChange(a_dg, a_newrow, a_oldrow){
	if ( typeof xe_GridRowFocusChange2 != "undefined" ) {
		var rtVal = xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow);

		if ( rtVal == 50 ) {
			if ( a_dg._ChildGrids ) {
				for ( var i = 0, ii = a_dg._ChildGrids.length; i < ii; i++ ) {
					if ( a_dg._ChildGrids[i].IsDataChanged() ) {
						if ( _X.MsgBoxYesNo("확인", "변경된 내용이 있습니다.\n계속 진행 하실경우 변경된 내용이 초기화 됩니다.\r\n계속 진행 하시겠습니까?") === "1" ) {
							break;
						} else {
							return false;
						}
					}
				}

				for ( var j = 0, jj = a_dg._ChildGrids.length; j < jj; j++ ) {
					a_dg._ChildGrids[j].Reset();
				}

				return true;
			}
		}

		return rtVal;
	}
}

//grid row focus change
function xe_GridRowFocusChanged(a_dg, a_newrow){
	if (typeof(xe_GridRowFocusChanged2)!="undefined") {
		xe_GridRowFocusChanged2(a_dg, a_newrow);
	}
}

//grid item focus change event
function xe_GridItemFocusChange(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
	if (typeof(xe_GridItemFocusChange2)!="undefined") {
		return xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol);
	}
}

//grid item focus changed event
function xe_GridItemFocusChanged(a_dg, a_newrow, a_newcol){
	if (typeof(xe_GridItemFocusChanged2)!="undefined") {
		xe_GridItemFocusChanged2(a_dg, a_newrow, a_newcol);
	}
}

//grid sort changed event
function xe_GridSortChanged(a_dg, sortInfo) {
	if (typeof(xe_GridSortChanged2)!="undefined") {
		xe_GridSortChanged2(a_dg, sortInfo);
	}
}

//Before grid data loaded
function xe_BeforeGridDataLoad(a_dg){
	if (typeof(xe_BeforeGridDataLoad2)!="undefined") {
		if ( xe_BeforeGridDataLoad2(a_dg) == 100 ) {
			if ( a_dg.RowCount() ) {
				_X.FormSetDisable( a_dg._SyncObject, false );
			} else {
				_X.FormSetDisable( a_dg._SyncObject, true );
			}
		}
	}
}

//grid data loaded
function xe_GridDataLoad(a_dg){
	if (typeof(xe_GridDataLoad2)!="undefined") {
		var li_result = xe_GridDataLoad2(a_dg);
		if(li_result == 100) {
			if(a_dg.id == "dg_1") {
				if(a_dg.RowCount() <= 0) {
					//수정(2016.12.27 KYY)
					if(typeof(_X.Noty)!="undefined") {
						_X.Noty("조회된 데이터가 없습니다.");
					}
				}
			}

			if ( a_dg._ChildGrids ) {
				if ( a_dg.RowCount() <= 0 ) {
					var resetFunction = function(parentGrid) {
						for ( var i = 0, ii = parentGrid._ChildGrids.length; i < ii; i++ ) {
							if ( parentGrid._ChildGrids[i]._ChildGrids ) {
								resetFunction(parentGrid._ChildGrids[i]);
							}

							parentGrid._ChildGrids[i].Reset();
						}
					};

					resetFunction(a_dg);
				}
			}
		}
	}
}

//grid button click event
function xe_GridButtonClick(a_dg, a_row, a_colname, a_col){
	if (typeof(xe_GridButtonClick2)!="undefined") {
		//setTimeout("xe_GridButtonClick2(" + a_dg.id + "," + a_row + "," + a_col + ",'" + a_colname +"')",10);
		if (xe_GridButtonClick2(a_dg, a_row, a_colname, a_col)!=100) return;
	}
}

//header click event
function xe_GridHeaderClick(a_dg, a_colname){
	if (typeof(xe_GridHeaderClick2)!="undefined") {
		if (xe_GridHeaderClick2(a_dg, a_colname)!=100) return;
	}
}

//item click event
function xe_GridItemClick(a_dg, a_row, a_col, a_colname){
	if (typeof(xe_GridItemClick2)!="undefined") {
		if (xe_GridItemClick2(a_dg, a_row, a_col, a_colname)!=100)  return;
	}
}

//checkbar click event
function xe_GridCheckBarClick(a_dg, a_startRow, a_endRow, a_val, ctrlKey, altKey, shiftKey){
	if (typeof(xe_GridCheckBarClick2)!="undefined") {
		if (xe_GridCheckBarClick2(a_dg, a_startRow, a_endRow, a_val, ctrlKey, altKey, shiftKey)!=100)  return;
	}
}

//checkbar header click event
function xe_GridCheckBarHeaderClick(a_dg, a_val){
	if (typeof(xe_GridCheckBarHeaderClick2)!="undefined") {
		if (xe_GridCheckBarHeaderClick2(a_dg, a_val)!=100)  return;
	}
}

//item double click event
function xe_GridItemDoubleClick(a_dg, a_row, a_col, a_colname){
	if (typeof(xe_GridItemDoubleClick2)!="undefined") {
		if (xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname)!=100)  return;
	}
}

function xe_GridBeforeKeyDown(a_obj, event, a_key, a_ctrl, a_alt, a_shift) {
	if ( typeof xe_GridBeforeKeyDown2 !== "undefined" ) {
		var cell = a_obj._SlickGrid.getCellFromEvent( event );
		if ( cell ) {
			var row  = cell.row + 1;
			var colName = a_obj.GetColumnNameByIndex( cell.cell );
			var retVal = xe_GridBeforeKeyDown2(a_obj, row, colName, event, a_key, a_ctrl, a_alt, a_shift);

			if ( retVal !== 100 ) {
				return 0;
			}
		}
	}

	return 100;
}

//그리드 keydown 이벤트 호출
function xe_GridKeyDown(a_obj, event, a_key, a_ctrl, a_alt, a_shift) {
	if ( typeof xe_GridKeyDown2 !== "undefined" ) {
		var cell = a_obj._SlickGrid.getCellFromEvent( event );
		if ( cell ) {
			var row  = cell.row + 1;
			var colName = a_obj.GetColumnNameByIndex( cell.cell );

			xe_GridKeyDown2(a_obj, row, colName, event, a_key, a_ctrl, a_alt, a_shift);
		}
	}
}

//filebuttonclick
function xe_FileButtonClick(a_obj, rowIdx, col, div){
	var retVal = true;
	if (typeof(xe_FileButtonClick2) !== "undefined") {
		retVal = xe_FileButtonClick2(a_obj, rowIdx, col, div);
	}

	return retVal !== false && retVal !== 0;
}

//filechanged
function xe_FileChanged(a_dg, row, cell, fileName, fileSize, fileType, firstData){
	var retVal = true;
	if (typeof(xe_FileChanged2) !== "undefined") {
		retVal = xe_FileChanged2(a_dg, row, cell, fileName, fileSize, fileType, firstData);
	}

	return retVal !== false && retVal !== 0;
}

//fileuploaded
function xe_FileUploaded(a_dg, uploadInfo){
	var retVal = true;
	if (typeof(xe_FileUploaded2) !== "undefined") {
		retVal = xe_FileUploaded2(a_dg, uploadInfo);
	}

	return retVal !== false && retVal !== 0;
}

//filedeleted
function xe_FileDeleted(a_dg, row, cell){
	var retVal = true;
	if (typeof(xe_FileDeleted2) !== "undefined") {
		retVal = xe_FileDeleted2(a_dg, row, cell);
	}

	return retVal !== false && retVal !== 0;
}

//tree item CollapseExpand event
function xe_TreeBeforeCollapseExpand(a_dg, a_row, collapsed){
	if ( typeof xe_TreeBeforeCollapseExpand2 != "undefined" ) {
		var retVal = xe_TreeBeforeCollapseExpand2(a_dg, a_row, collapsed);
		if ( retVal != null && retVal != 100 ) {
			return false;
		}
	}

	return true;
}

//filtered event
function xe_FilterOnOff_After(a_dg, visible){
	if ( typeof xe_FilterOnOff_After2 != "undefined" ) {
		xe_FilterOnOff_After2();
	}
}


//tree item checked event
function xe_TreeItemExpanding(a_dg, a_itemIndex, a_rowId){
	if (typeof(xe_TreeItemExpanding2)!="undefined") {
		if (xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId)!=100)  return;
	}
}

// - anychart  point click event
function xe_ChartPointClick(a_dg, a_event){
	if (typeof(xe_ChartPointClick2)!="undefined") {
		if (xe_ChartPointClick2(a_dg, a_event)!=100)  return;
	}
}

//- anychart  point select event
function xe_ChartPointSelect(a_dg, a_event){
	if (typeof(xe_ChartPointSelect2)!="undefined") {
		if (xe_ChartPointSelect2(a_dg, a_event)!=100)  return;
	}
}

//- anychart  point mouse over event
function xe_ChartPointMouseOver(a_dg){
	if (typeof(xe_ChartPointMouseOver2)!="undefined") {
		if (xe_ChartPointMouseOver2(a_dg)!=100)  return;
	}
}

//- anychart  point mouse out  event
function xe_ChartPointMouseOut(a_dg, a_event){
	if (typeof(xe_ChartPointMouseOut2)!="undefined") {
		if (xe_ChartPointMouseOut2(a_dg, a_event)!=100)  return;
	}
}

// scroll event
function xe_Scroll(a_dg, a_event, scrollInfo){
	if (typeof(xe_Scroll2)!="undefined") {
		if (xe_Scroll2(a_dg, a_event, scrollInfo)!=100)  return;
	}
}

//추가(2017.05.25 KYY)
function xe_ViewportChanged(a_dg, a_event){
	if (typeof(xe_ViewportChanged2)!="undefined") {
		if (xe_ViewportChanged2(a_dg, a_event)!=100)  return;
	}
}

//window close
function x_Close() {
	if (typeof(x_Close2)!="undefined") {
		if (x_Close2()!=100)  return;
	}

	if (x_IsAllDataChanged()>0) {
		if(_X.MsgBoxYesNo("변경된 자료가 있습니다 \r\n 프로그램을 종료 하시겠습니까?")==2) {return;}
	}

	if(_IsModal || _IsPopup) {
		window.close();
	} else if(typeof(_X.CloseSheet)!="undefined") {
		_X.CloseSheet(window);
	} else {
		_Caller.$("#findmodal").dialog("close");
	}
}

function xe_BodyResize(a_init){
	if("SlickGrid"=="SlickGrid") { // global 변수 처리
		//SlickGrid 리사이징(2015.05.11 KYY)
		for ( var i = 0, ii = window._Grids.length; i < ii; i++ ) {
			setTimeout( window._Grids[i].id + "._SlickGrid.resizeCanvas()", 0);
		}
	}
}


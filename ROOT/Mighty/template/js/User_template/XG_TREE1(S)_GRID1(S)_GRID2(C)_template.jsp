﻿<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="<c:url value='/Mighty/template/XG_TREE1(S)_GRID1(S)_GRID2(C)_template.js'/>"></script>
<%@ page import ="xgov.core.vo.XLoginVO" %>

<div id='topsearch' class='option_box2'>
	<div id='xbtns' style='height:23px;'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_tree1' gridCall="true"></html:authbutton>
		</span>
	</div>	
</div>

<div id='grid_tree_box' class='grid_div mr5'>
	<div id='grid_tree_btn' style='float:right;'>
		<div style='float:right;'><html:button name='B_EXPAND' id='B_EXPAND' type='button' icon='ui-icon-zoomout' desc="펼치기" onClick="pf_expand();"></html:button></div>	
		<div style='float:right;'><html:button name='B_COLLAPSE' id='B_COLLAPSE' type='button' icon='ui-icon-zoomin' desc="접기" onClick="pf_collapse();"></html:button></div>
	</div>		
	<div id='grid_tree1' class='grid_div mr5'>
		<%= xgov.core.util.XHtmlUtil.RealTree("dg_tree1", request.getHeader("User-Agent"), "0", "N", "N") %>
	</div>
</div>	
		
<div id='grid_1' class='grid_div'>
	<%= xgov.core.util.XHtmlUtil.RealGrid("dg_1", request.getHeader("User-Agent"), "1", "N", "N") %>
</div>

<div id='grid_2' class='grid_div mt5'>
	<%= xgov.core.util.XHtmlUtil.RealGrid("dg_2", request.getHeader("User-Agent"), "2", "N", "Y") %>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:33, anchor:{x1:1,y1:1,x2:1,y2:0}};
	grid_tree_box.ResizeInfo = {init_width:300, init_height:662, anchor:{x1:1,y1:1,x2:0,y2:1}};
	grid_tree_btn.ResizeInfo = {init_width:300, init_height:25, anchor:{x1:1,y1:1,x2:0,y2:0}};
	grid_tree1.ResizeInfo = {init_width:300, init_height:637, anchor:{x1:1,y1:1,x2:0,y2:1}};
	grid_1.ResizeInfo = {init_width:700, init_height:331, anchor:{x1:1,y1:1,x2:1,y2:0.5}};
	grid_2.ResizeInfo = {init_width:700, init_height:331, anchor:{x1:1,y1:1,x2:1,y2:0.5}};
</script>


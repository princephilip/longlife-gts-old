﻿<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="<c:url value='/Mighty/template/XG_GRID1_template.js'/>"></script>

<div id='topsearch' class='option_box2'>
	<div id='xbtns' style='height:23px;'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true"></html:authbutton>
		</span>
	</div>
</div>

<div id='grid_1' class='grid_div'>
	<%= xgov.core.util.XHtmlUtil.RealGrid("dg_1", request.getHeader("User-Agent"), "0", "N", "Y") %>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:33, anchor:{x1:1,y1:1,x2:1,y2:0}};
	grid_1.ResizeInfo = {init_width:1000, init_height:662, anchor:{x1:1,y1:1,x2:1,y2:1}};
</script>
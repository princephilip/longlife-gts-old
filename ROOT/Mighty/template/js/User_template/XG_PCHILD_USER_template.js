//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 : @PGM_CODE|PGM_NAME
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
// 
// 
//===========================================================================================
//	
//===========================================================================================

function x_InitForm2(){
	_X.RealGrid(grid_parent, "dg_parent", "100%", "100%", "sm", "sm0101", "sm_query_file|sm_query_id_C01");
	_X.RealGrid(grid_child, "dg_child", "100%", "100%", "sm", "sm0101", "sm_query_file|sm_query_id_C02");
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	//if(ab_allcompleted) x_DAO_Retrieve2(dg_parent);
}

function x_DAO_Retrieve2(a_dg){
	if(a_dg.id == "dg_parent") {
		a_dg.Retrieve(new Array());
	}
}

function x_DAO_Child_Retrieve2(a_dg){
	a_dg.Retrieve(new Array());
}

function xe_EditChanged2(a_obj, a_val, a_oldVal){

}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){}
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

function x_DAO_Saved2(){
	
}

function x_DAO_Insert2(a_dg, row){
	return 100;
}

function x_Insert_After2(a_dg, rowIdx){

}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){

}

function x_DAO_Delete2(a_dg){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Child_Insert2(a_dg, row){
	return 100;
}

function x_Insert_Child_After2(a_dg, rowIdx){
	
}

function x_DAO_Child_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_Child_After2(a_dg, rowIdx){

}

function x_DAO_Child_Delete2(a_dg){
	return 100;
}

function x_DAO_Child_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){

}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 1;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){

}

function xe_GridDataChange2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){

}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
	
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
	return true;
}

function xe_GridItemFocusChanged(a_dg, a_newrow, a_newcol){
	
}

function xe_GridDataLoad2(a_dg){
	return 100;
}

function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){
}

function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){
}

function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){
}

function xe_TreeItemExpanding2(a_dg, a_event){

}

function xe_ChartPointSelect2(a_dg, a_event){
	
}

function xe_ChartPointMouseOver2(a_dg){
	
}

function xe_ChartPointMouseOut2(a_dg, a_event){

}

function x_Close() {
	return 100;
}

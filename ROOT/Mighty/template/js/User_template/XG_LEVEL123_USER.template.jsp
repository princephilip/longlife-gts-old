<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="<c:url value='/Mighty/template/XG_LEVEL123_template.js'/>"></script>
<%@ page import ="xgov.core.vo.XLoginVO" %>

<div id='topsearch' class='option_box2'>
	<div id='xbtns' style='height:23px;'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true"></html:authbutton>		
		</span>	
	</div>
</div>

<div id='grid_1' class='grid_div'>
	<div style="float: left; height: 20px; color: #4374D9; font-weight: bold; line-height: 23px;">
		<img src='${pageContext.request.contextPath}/images/btn/tit_icon_pop.gif'>&nbsp; 대분류
	</div>
	<span style="float:right;margin-top:3px; margin-bottom:1px;">
		<html:authchildbutton id='buttons' grid='dg_1' eventHead="" append='true' insert='false' duplicate='false' delete='true' excel='false' retrieve='false' ></html:authchildbutton>
	</span>

	<%= xgov.core.util.XHtmlUtil.RealGrid("dg_1", request.getHeader("User-Agent"),"0","N","Y") %>
</div>

<div id='grid_2' class='grid_div ml5'>
	<div style="float: left; height: 20px; color: #4374D9; font-weight: bold; line-height: 23px;">
		<img src='${pageContext.request.contextPath}/images/btn/tit_icon_pop.gif'>&nbsp; 중분류
	</div>
	<span style="float:right;margin-top:3px; margin-bottom:1px;">
		<html:authchildbutton id='buttons' grid='dg_2' eventHead="" append='true' insert='false' duplicate='false' delete='true' excel='false' retrieve='false' ></html:authchildbutton>
	</span>

	<%= xgov.core.util.XHtmlUtil.RealGrid("dg_2", request.getHeader("User-Agent"),"1","N","Y") %>
</div>

<div id='grid_3' class='grid_div ml5'>
	<div style="float: left; height: 20px; color: #4374D9; font-weight: bold; line-height: 23px;">
		<img src='${pageContext.request.contextPath}/images/btn/tit_icon_pop.gif'>&nbsp; 소분류
	</div>
	<span style="float:right;margin-top:3px; margin-bottom:1px;">
		<html:authchildbutton id='buttons' grid='dg_3' eventHead="" append='true' insert='false' duplicate='false' delete='true' excel='false' retrieve='false' ></html:authchildbutton>
	</span>

	<%= xgov.core.util.XHtmlUtil.RealGrid("dg_3", request.getHeader("User-Agent"),"2","N","Y") %>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:33, anchor:{x1:1,y1:1,x2:1,y2:0}};
	grid_1.ResizeInfo = {init_width:300, init_height:637, anchor:{x1:1,y1:1,x2:0.3,y2:1}};
	grid_2.ResizeInfo = {init_width:300, init_height:637, anchor:{x1:1,y1:1,x2:0.3,y2:1}};
	grid_3.ResizeInfo = {init_width:400, init_height:637, anchor:{x1:1,y1:1,x2:0.4,y2:1}};
</script>
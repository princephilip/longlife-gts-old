//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 : @PGM_CODE|PGM_NAME
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
// 
// 
//===========================================================================================
//	
//===========================================================================================

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		x_DAO_Retrieve(dg_1);
	}
}

function x_DAO_Retrieve2(a_dg){
	
	if(a_dg.id == "dg_1") {
		
		//조회조건 : 회사코드
		a_dg.Retrieve([top._CompanyCode, "1", "%"]);
		level_1_Cnt = 0;
		
	} else if(a_dg.id == "dg_2") {
		
		//조회조건 : 회사코드, 유형레벨, 상위유형코드
		a_dg.Retrieve([top._CompanyCode, "2", dg_1.GetItem(dg_1.GetRow(), "L_CODE")]);
		level_2_Cnt = 0;
		
	} else {
		
		//조회조건 : 회사코드, 유형레벨, 상위유형코드
		a_dg.Retrieve([top._CompanyCode, "3", dg_2.GetItem(dg_2.GetRow(), "L_CODE")]);
		level_3_Cnt = 0;
	}
}

function xe_EditChanged2(a_obj, a_val, a_oldVal){

}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){

}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){

}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	
	var cDatas = new Array();
	cDatas.push(dg_1.GetChangedData());
	cDatas.push(dg_2.GetChangedData());
	cDatas.push(dg_3.GetChangedData());
	
	if((cDatas[0] == null || cDatas[0] == "") && (cDatas[1] == null || cDatas[1] == "") && (cDatas[2] == null || cDatas[2] == "")) return 0;
	
	for(var i = 0; i < cDatas.length; i++) {
		if(cDatas[i] == null || cDatas[i] == "") continue;
		
		for(var j = 0; j < cDatas[i].length; j++) {//저장 시 코드부여
			if(cDatas[i][j].job != "I") continue;
			
			var row = cDatas[i][j].idx + 1;
			var lCode;//코드
			
			if(i == 0) {
				
				lCode = _X.LPad(_X.ToInt(_X.XmlSelect("sm", "FM_CODE_LAND", "S_SM0116_01", new Array(top._CompanyCode, "1", "%"), "array")[0][0]) + j, 2, "0");
				dg_1.SetItem(row, "L_CODE", lCode);//코드
				
			} else if(i == 1) {
				
				upperCode = dg_1.GetItem(dg_1.GetRow(), "L_CODE");
				lCode = _X.XmlSelect("sm", "FM_CODE_LAND", "S_SM0116_01", new Array(top._CompanyCode, "2", upperCode), "array")[0][0];
				lCode = _X.LPad(_X.ToInt(lCode == 1 ? 1 : lCode.substring(lCode.length - 2)) + j, 2, "0");
				dg_2.SetItem(row, "L_CODE", upperCode + lCode);//코드
				
			} else {
				
				upperCode = dg_2.GetItem(dg_2.GetRow(), "L_CODE");
				lCode = _X.XmlSelect("sm", "FM_CODE_LAND", "S_SM0116_01", new Array(top._CompanyCode, "3", upperCode), "array")[0][0];
				lCode = _X.LPad(_X.ToInt(lCode == 1 ? 1 : lCode.substring(lCode.length - 2)) + j, 2, "0");
				dg_3.SetItem(row, "L_CODE", upperCode + lCode);//코드
			}
		}
	}
	return true;
}

function x_DAO_Saved2(){
	level_1_Cnt = 0;
	level_2_Cnt = 0;
	level_3_Cnt = 0;
}

function x_DAO_Insert2(a_dg, row){
	return 100;
}

function x_Insert_After2(a_dg, rowIdx){
	
	var lCode;//코드
	var upperCode = 0;//상위코드
	
	if(a_dg.id == "dg_1") {
		
		lCode = _X.LPad(_X.ToInt(_X.XmlSelect("sm", "FM_CODE_LAND", "S_SM0116_01", new Array(top._CompanyCode, "1", "%"), "array")[0][0]) + (level_1_Cnt++), 2, "0");
		
	} else if(a_dg.id == "dg_2") {
		
		upperCode = dg_1.GetItem(dg_1.GetRow(), "L_CODE");
		lCode = _X.XmlSelect("sm", "FM_CODE_LAND", "S_SM0116_01", new Array(top._CompanyCode, "2", upperCode), "array")[0][0];
		lCode = _X.LPad(_X.ToInt(lCode == 1 ? 1 : lCode.substring(lCode.length - 2)) + (level_2_Cnt++), 2, "0");
		lCode = upperCode + lCode;
		
	} else if(a_dg.id == "dg_3") {
		
		upperCode = dg_2.GetItem(dg_2.GetRow(), "L_CODE");
		lCode = _X.XmlSelect("sm", "FM_CODE_LAND", "S_SM0116_01", new Array(top._CompanyCode, "3", upperCode), "array")[0][0];
		lCode = _X.LPad(_X.ToInt(lCode == 1 ? 1 : lCode.substring(lCode.length - 2)) + (level_3_Cnt++), 2, "0");
		lCode = upperCode + lCode;
		
	}
	
	a_dg.SetItem(rowIdx, "COMPANY_CODE", top._CompanyCode);//회사코드
	a_dg.SetItem(rowIdx, "USE_YESNO", "Y");//사용여부
	a_dg.SetItem(rowIdx, "L_LEVEL", a_dg.id.split("_")[1]);//레벨
	a_dg.SetItem(rowIdx, "L_CODE", lCode);//코드
	a_dg.SetItem(rowIdx, "UPPER_CODE", upperCode);//상위코드
	
	var maxSortOrder = _X.GetSummary(a_dg, "SORT_ORDER", "max");
	a_dg.SetItem(rowIdx, "SORT_ORDER", (isNaN(maxSortOrder) ? 0 : maxSortOrder) + 1);//정렬순서
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
	
}
	
function x_DAO_Delete2(a_dg){
	return 100;
}
	
function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
	
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 1;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){

}

function xe_GridDataChange2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	
	if(a_col == "SORT_ORDER") {
		if(!_X.IsNumber(a_newvalue)) {
			a_dg.SetItem(a_row, a_col, a_oldvalue);
			return;
		}
		
		if(isNaN(a_newvalue)) {
			a_dg.SetItem(a_row, a_col, a_oldvalue);
			return;
		}

		if(a_newvalue < -2147483648) {
				_X.MsgBox("최소 입력값 -2147483648 보다 작은 수가 입력 되었습니다.");
				a_dg.SetItem(a_row, a_col, a_oldvalue);
				
		} else if(a_newvalue > 2147483647) {
			_X.MsgBox("최대 입력값 2147483647 보다 큰 수가 입력 되었습니다.");
			a_dg.SetItem(a_row, a_col, a_oldvalue);
		}
	}
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
	return 100;
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
	return true;
}

function xe_GridItemFocusChanged(a_dg, a_newrow, a_newcol){
	
}

function xe_GridDataLoad2(a_dg){
	
}

function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){}
function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){}
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2() {
	return 100;
}

------------------
function x_InitForm2(){
	_X.RealGrid(grid_1, "dg_1", "100%", "100%", "sys", "pgmCode", "queryFile|selectId");
	_X.RealGrid(grid_2, "dg_2", "100%", "100%", "sys", "pgmCode", "queryFile|selectId");
	_X.RealGrid(grid_3, "dg_3", "100%", "100%", "sys", "pgmCode", "queryFile|selectId");
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		x_DAO_Retrieve(dg_1);
	}
}

function x_DAO_Retrieve2(a_dg){
	
	if(a_dg.id == "dg_1") {
		//조회조건 : 
		a_dg.Retrieve(new Array());
		
	} else if(a_dg.id == "dg_2") {
		//조회조건 : 
		a_dg.Retrieve(new Array());
		
	} else {
		//조회조건 : 
		a_dg.Retrieve(new Array());
	}
}

function xe_EditChanged2(a_obj, a_val){

}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){}
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

function x_DAO_Saved2(){
	
}

function x_DAO_Insert2(a_dg, row){
	return 100;
}

function x_Insert_After2(a_dg, rowIdx){
	
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
	
}
	
function x_DAO_Delete2(a_dg){
	return 100;
}
	
function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
	
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 1;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	
}

function xe_GridDataChange2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
	return 100;
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
	return true;
}

function xe_GridDataLoad2(a_dg){
	
}

function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){}
function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){}
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2() {
	return 100;
}

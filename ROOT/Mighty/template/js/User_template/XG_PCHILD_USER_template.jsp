﻿<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="<c:url value='/Mighty/template/XG_PCHILD_template.js'/>"></script>

<div id='topsearch' class='option_box2'>
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_parent'></html:authbutton>		
		</span>	
	</div>
</div>

<div id='grid_parent' class='grid_div'>
	<%= xgov.core.util.XHtmlUtil.RealGrid("dg_parent", request.getHeader("User-Agent"),"0","N","Y") %>
</div>

<div id='grid_cbutton' class='grid_div'>
	<span style="float:right;margin-top:5px;">
		<html:authchildbutton id='buttons' grid='dg_child' eventHead="_Child" append='true' insert='false' duplicate='false' delete='true' excel='false' retrieve='false' ></html:authchildbutton>
	</span>
</div>

<div id='grid_child' class='grid_div'>
	<%= xgov.core.util.XHtmlUtil.RealGrid("dg_child", request.getHeader("User-Agent"),"1","N","Y") %>
</div>

<script type="text/javascript">

	topsearch.ResizeInfo = {init_width:1000, init_height:33, anchor:{x1:1,y1:1,x2:1,y2:0}};
	grid_parent.ResizeInfo = {init_width:1000, init_height:300, anchor:{x1:1,y1:1,x2:1,y2:0}};
	grid_cbutton.ResizeInfo = {init_width:1000, init_height:25, anchor:{x1:1,y1:1,x2:1,y2:0}};
	grid_child.ResizeInfo = {init_width:1000, init_height:337, anchor:{x1:1,y1:1,x2:1,y2:1}};

</script>
<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%
 /**
  * @JSP Name : MG020.jsp
  * @Description : template 유형2
  * @Modification Information
  * 
  *   수정일     수정자          수정내용
  *  -------    --------    ---------------------------
  *  2012.12.01  김양열          최초 생성
  *
  * author (주)엑스인터넷정보 
  * Copyright (C) 2012 by X-Internet Info.  All right reserved.
  */
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Mighty eGov Framework</title>
	<div id="mightylib"><%@ include file="/Mighty/jsp/mightylib-1.0.1.jsp"%></div>

	<script type="text/javascript" src="/APPS/cm/js/CM020010.js"></script>
</head>

<body style="verticalAlign: top; text-align: left; margin: 0; display: inline; padding-top: 0px;">
	
	<div style='float:left;left:0;width:100%;height:400;' id='div_criteria'></div>
	
	<div style='float: left;'>시스템 </div><div style='float: left;' id='div_syscode'></div>
	<div style='float: left;'> 상위 프로그램 </div><div style='float: left;' id='ddlb_pgmcode1'></div>
	<div style='float: left;'> 실행 프로그램 </div><div style='float: left;' id='ddlb_pgmcode2'></div>
	
	<div style="font-size: 13px; font-family: Verdana;" id="selectionlog">
		<div id='xbtns' style='float:right;'></div>  
		</br>
	</div>
	
	<div style='float:none;left:0;width:100%;height:200px;' id='grid_1'></div>
	<div style='float:none;left:0;width:100%;height:200px;' id='grid_2'></div>
	
	
	<script type="text/javascript">
		if(typeof(x_InitForm)!="undefined"){x_InitForm();}
	</script>


</body>
</html>
<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
 /**
  * @JSP Name : XF020.jsp
  * @Description : 코드찾기 templete(com 에  있는 공통모듈활용 과 query,grid 각 시스템에 )
  * @Modification Information
  *
  *   수정일     수정자          수정내용
  *  -------    --------    ---------------------------
  *  2012.12.01  김양열          최초 생성
  *
  * author (주)엑스인터넷정보
  * Copyright (C) 2012 by X-Internet Info.  All right reserved.
  */
 %>

<%
	String sys = request.getParameter("sys");
	String fcd = request.getParameter("fcd");
	String fnm = request.getParameter("fnm");
	String fgrid = request.getParameter("fgrid");

	//String[] params  = egovframework.rte.com.service.EgovStringUtil.split(fgrid,"|");
	String _Separate = "¸";
	String[] params = egovframework.rte.com.service.EgovStringUtil.split(fgrid, _Separate);
	if(params.length==1) {
		params = egovframework.rte.com.service.EgovStringUtil.split(params[0],"|");
	}

	String subUrl = "/Mighty/jsp/FindCodeSlickGrid.jsp";

	String mustInput = request.getParameter("mustinput");
	String autoClose = request.getParameter("autoclose");

	//System.out.println(subUrl);
%>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
	<title></title>

	<script type="text/javascript">
		var _Compact = true;
	</script>

	<jsp:include page="/Mighty/include/COMM_JS.jsp"/>

	<link type="text/css" charset="utf-8" href="/Mighty/css/mighty-2.0.3.css?v=20180628"  rel="stylesheet" media="all" />
	<link type="text/css" charset="utf-8" href="/Theme/css/custom_sub.css"  rel="stylesheet" media="all" />

	<script type="text/javascript">
		var _AutoRetrieve = true;
		var _FIND_SYS = "<%=sys%>";
		var _FIND_CODE = "<%=fcd%>";
		var _FIND_NAME = "<%=fnm%>";
		var _FIND_GRID = "<%=params[0]%>";
		var _FIND_QUERY = "<%=params[1]%>";
		var _FIND_SELECT = "<%=params[2]%>";

		var _MustInput = <%=(mustInput==null || mustInput.equals("") || mustInput.equals("N") ? "false" : "true")%>;
		var _AutoClose = <%=(autoClose==null || autoClose.equals("") || autoClose.equals("Y") ? "true" : "false")%>;

		function xm_UnloadForm() {
			xm_ClearObject(window);
		}
	</script>

	<link type="text/css" charset="utf-8" href="/Mighty/3rd/jQuery/alert/themes/default/theme.css"  rel="stylesheet" media="all" />
	<link type="text/css" charset="utf-8" href="/Mighty/3rd/jQuery/alert/css/alert.min.css"  rel="stylesheet" media="all" />


	<script type="text/javaScript" src="${pageContext.request.contextPath}/APPS/com/js/Common.js?v=<%=xgov.core.util.XUtility.GetCurrentDate("yyyyMMddHHmmss")%>"></script>
	<script type="text/javaScript" src="${pageContext.request.contextPath}/APPS/com/js/<%=sys.toLowerCase()%>Common.js?v=<%=xgov.core.util.XUtility.GetCurrentDate("yyyyMMddHHmmss")%>"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/Mighty/js/FindCodeSlickGrid.js" charset="UTF-8"></script>

	<%
		String _Row_Separate = "˛";
		String _Col_Separate = "¸";
		String grid_schima = "";
 		try {
		 	String gridparams  = sys + _Col_Separate + fcd;
			grid_schima = xgov.core.dao.XDAO.XmlSelect3(request,"array", "com", "X5", "GRID_SCRIPT", gridparams, "all", _Row_Separate,_Col_Separate);
		} catch (Exception e) {
			e.printStackTrace();
		}
	%>

	<%if(grid_schima==null || grid_schima.equals("")){%>
		<script type="text/javascript" src="${pageContext.request.contextPath}/APPS/<%=sys%>/grid/<%=params[0]%>.Grid.js" charset="UTF-8"></script>
	<%}else{%>
		<script type="text/javaScript"><%=grid_schima%></script>
	<%}%>

	<script type=text/javascript src="/Mighty/3rd/jQuery/alert/js/alert.min.js"></script>

	<!-- <script type="text/javascript" src="${pageContext.request.contextPath}/APPS/<%=sys%>/js/<%=fcd%>.js" charset="UTF-8"></script> -->
	<base target=_self>
</HEAD>

<body style="verticalAlign: top; text-align: left; display: inline; " bgcolor="#ffffff">
	<div id="pageLayout" style='position:absolute;left:0px;top:0px;width:100%;height:100%;background-color:#ffffff;overflow:hidden;z-index:1;margin:5px; padding:5px 0px 0px 5px ;'>
		<jsp:include page="<%=subUrl%>" />
	</div>

	<script type="text/javascript">
		if(typeof(_Caller._FindCode.title)!="undefined") {
			document.title = _Caller._FindCode.title;
		}
		_X.Block();

		function xm_InitForm_After() {
			xm_InputInitial();
			dg_1._SlickGrid.resizeCanvas();
			//입력필드 한글입력 활성화(2015.04.07 KYY)
			$("input[type=text]").addClass("ko");
			$(":input:visible:enabled:first").focus();
		}

		$(window.document).ready(function() {

		});

		$(window).load(function() {
			pageLayout.ResizeInfo = {init_width:1010, init_height:720};

			var rtValue = 0;
			if(typeof(x_InitForm)!="undefined"){rtValue = x_InitForm();}
			if(rtValue!=null && rtValue<0)
				return;

			$(window).bind("resize", xm_BodyResize);
			$(window).bind("unload", xm_UnloadForm);

			xm_BodyResize(true);
			pageLayout.style.display = "inline";
			_X.UnBlock();
			setTimeout("xm_InitForm_After()",0);
		});

		$(document).keydown(function(e) {
			switch(e.which) {
				case KEY_F2: //F2
					setFindFocus();
					break;
				case KEY_DELETE: //DEL
					$("#FIND_CODE").val("");
					setFindFocus();
					break;
				case KEY_ESC:
				case KEY_F4:
					closeModal();
					break;
				//case KEY_ENTER:
				case KEY_SPACE_BAR:
					if(_XSerachMode) {
						setFindFocus();
					}
					break;
			}
		});

		function closeModal() {
			if(isIE) {
				window.close();
			} else {
				_Caller.$("#findmodal").dialog("close");
			}
		}
	</script>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
 /**
  * @JSP Name : XF020.jsp
  * @Description : 코드찾기 templete(com 에  있는 공통모듈활용 과 query,grid 각 시스템에 )
  * @Modification Information
  * 
  *   수정일     수정자          수정내용
  *  -------    --------    ---------------------------
  *  2012.12.01  김양열          최초 생성
  *  2015.06.08  SKLEE			SLICKGRID, DATASET 버전으로 변경
  *
  * author (주)엑스인터넷정보 
  * Copyright (C) 2012 by X-Internet Info.  All right reserved.
  */
 %>

<%
	String sys = request.getParameter("sys");
	String fcd = request.getParameter("fcd");
	String fnm = request.getParameter("fnm");
	String fgrid = request.getParameter("fgrid");
	
	//String[] params  = egovframework.rte.com.service.EgovStringUtil.split(fgrid,"|");
	String _Separate = "¸";
	String[] params = egovframework.rte.com.service.EgovStringUtil.split(fgrid, _Separate);
	if(params.length==1) {
		params = egovframework.rte.com.service.EgovStringUtil.split(params[0],"|");
	}
	
	/*String subUrl = "/APPS/sm/jsp/SM070701.jsp";테스트버전으로 테스트 jsp 페이지를 설정(2015.06.08 이상규)*/
	String subUrl = "/APPS/"+sys+"/jsp/"+fcd+".jsp";
	//System.out.println(subUrl);
%>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
	<title>/title>

	<script type="text/javascript">
		var _Compact = true;
	</script>
	
	<jsp:include page="/Mighty/include/COMM_JS.jsp"/>

	<script type="text/javascript">
		var _AutoRetrieve = false;
		var _FIND_SYS = "<%=sys%>";
		var _FIND_CODE = "<%=fcd%>";
		var _FIND_NAME = "<%=fnm%>";
		var _FIND_GRID = "<%=params[0]%>";
		var _FIND_QUERY = "<%=params[1]%>";
		var _FIND_SELECT = "<%=params[2]%>";
	
		function xm_UnloadForm() {
			//Flash관련 오브젝트 정리(2013.04.15 김양열)
			xm_ClearObject(window);
		}		
	</script>

	<link type="text/css" charset="utf-8" href="/Mighty/jQuery/alert/themes/default/theme.css"  rel="stylesheet" media="all" />
	<link type="text/css" charset="utf-8" href="/Mighty/jQuery/alert/css/alert.min.css"  rel="stylesheet" media="all" />
	
	<script type="text/javaScript" src="${pageContext.request.contextPath}/Mighty/js/crypto/base64.js"></script><!-- _X.GSI함수에서 base64 사용으로 인해 base64 스크립트 로드(2015.06.08 이상규) -->
	<script type="text/javaScript" src="${pageContext.request.contextPath}/APPS/com/js/Common.js"></script>
	<script type="text/javaScript" src="${pageContext.request.contextPath}/APPS/com/js/<%=sys.toLowerCase()%>Common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/APPS/<%=sys%>/js/<%=params[0]%>.js" charset="UTF-8"></script>
	<!-- <script type="text/javascript" src="${pageContext.request.contextPath}/APPS/<%=sys%>/grid/<%=params[0]%>.RealGrid.js" charset="UTF-8"></script> dataset 은 RealGrid.js 파일 필요없음(2015.06.08 이상규)-->
	
	<script type=text/javascript src="/Mighty/jQuery/alert/js/alert.min.js"></script> 
	
	<!-- <script type="text/javascript" src="${pageContext.request.contextPath}/APPS/<%=sys%>/js/<%=fcd%>.js" charset="UTF-8"></script> -->
	<base target=_self>
</HEAD>

<body style="verticalAlign: top; text-align: left; display: inline; " bgcolor="#ffffff">
	<div id="pageLayout" style='position:absolute;left:0px;top:0px;width:100%;height:100%;background-color:#ffffff;overflow:hidden;z-index:1;margin:5px; padding:5px 0px 0px 5px ;'>
		<jsp:include page="<%=subUrl%>" />
	</div>
	
	<script type="text/javascript">
		if(typeof(_Caller._FindCode.title)!="undefined") {
			document.title = _Caller._FindCode.title;
		}
		_X.Block();

		function xm_InitForm_After() {
			xm_InputInitial();
			//dg_1._SlickGrid.resizeCanvas();dataset 은 resize 필요없음 (2015.06.08 이상규)
			//입력필드 한글입력 활성화(2015.04.07 KYY)			
			$("input[type=text]").addClass("ko");			
			$(":input:visible:enabled:first").focus();
		}

		$(window.document).ready(function() {
		});
			
		$(window).load(function() {
			pageLayout.ResizeInfo = {init_width:1000, init_height:720};

			var rtValue = 0;
			if(typeof(x_InitForm)!="undefined"){rtValue = x_InitForm();}
			if(rtValue!=null && rtValue<0)
				return;
			
			$(window).bind("resize", xm_BodyResize);
			$(window).bind("unload", xm_UnloadForm);	//Flash관련 오브젝트 정리(2013.04.15 김양열)

			xm_BodyResize(true);
			pageLayout.style.display = "inline";
			_X.UnBlock();
			setTimeout("xm_InitForm_After()",0);
		});	
		
	</script>
</body>
</html>
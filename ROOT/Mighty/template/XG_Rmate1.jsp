<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%
 /**
  * @JSP Name : MG010.jsp
  * @Description : template 유형1
  * @Modification Information
  * 
  *   수정일     수정자          수정내용
  *  -------    --------    ---------------------------
  *  2012.12.01  김양열          최초 생성
  *
  * author (주)엑스인터넷정보 
  * Copyright (C) 2012 by X-Internet Info.  All right reserved.
  */
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >

<%
	String pcd = request.getParameter("pcd");
	String params = request.getParameter("params");
	String subUrl = "/APPS/" + pcd.substring(0,2).toLowerCase() + "/jsp/" + pcd + ".jsp?params="+params;
%>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Mighty eGov Framework</title>

	<script type="text/javascript">
		var _PGM_CODE = "<%=pcd%>";
	</script>

	<div id="mightylib"><%@ include file="/Mighty/jsp/mightylib-1.0.1.jsp"%></div>

	<script type="text/javascript" src="/APPS/com/js/CommonVariable.js" charset="UTF-8"></script>
	<script type="text/javascript" src="/APPS/<%=pcd.substring(0,2).toLowerCase()%>/js/<%=pcd%>.js" charset="UTF-8"></script>
</head>

<body style="verticalAlign: top; text-align: left; margin: 0; display: inline; padding-top: 0px;" bgcolor="#ffffff">

	<div id="pageLayout" class="pageLayout_MG010">
		<jsp:include page="<%=subUrl%>" />
	</div>

	<script type="text/javascript">
		function xm_InitForm() {
			var rtValue = 0;
			if(typeof(x_InitForm)!="undefined"){rtValue = x_InitForm();}
			if(rtValue!=null && rtValue<0)
				return;

			pageLayout.ResizeInfo = {init_width:1000, init_height:710};
			$(window).bind("resize", xm_BodyResize);
			xm_BodyResize();
			pageLayout.style.display = "inline";
			setTimeout("xm_InputInitial()",0);
		}

		$(window).bind("load", xm_InitForm);	//변경(2013.04.16 김양열)
		//$(document).ready(xm_InitForm);
		//xm_InitForm();
	</script>

</body>

</html>
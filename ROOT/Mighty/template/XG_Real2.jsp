<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<%@ page import ="xgov.core.vo.XLoginVO" %>
<%@ page import ="org.springframework.util.StopWatch" %>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%
 /**
  * @JSP Name : XG010.jsp
  * @Description : 기본 페이지 구조
  * @Modification Information
  * 
  *   수정일     수정자         수정내용
  *  ----------  --------       ---------------------------
  *  2012.12.01  김양열         최초 생성
  *  2015.06.08  SKLEE          SLICKGRID 버전으로 변경
  *
  * author (주)엑스인터넷정보 
  * Copyright (C) 2015 by X-Internet Info.  All right reserved.
  */
%>

<%
	//StopWatch stopWatch = new StopWatch();
	//stopWatch.start();

	String pcd = request.getParameter("pcd");
	String params = request.getParameter("params");
	String subUrl = "/APPS/" + pcd.substring(0,2).toLowerCase() + "/jsp/" + pcd + ".jsp?params="+params;
	XLoginVO user = (XLoginVO)session.getAttribute("loginVO");
	if(session.getAttribute("userId")==null) {
		 session.setAttribute("userId",user.getUserId());
	}
	if(session.getAttribute("deptCode")==null) {
		 session.setAttribute("deptCode",user.getDeptCode());
	}
	if(session.getAttribute("companyCode")==null) {
		 session.setAttribute("companyCode",user.getCompanyCode());
	}
	if(session.getAttribute("userTag")==null) {
		 session.setAttribute("userTag",user.getUserTag());
	}

	//프로그램 실행권한 체크(2015.04.20 KYY)
	String _Row_Separate = "˛";
	String _Col_Separate = "¸";
 	String authparams = user.getCompanyCode() + _Col_Separate+user.getUserId() + _Col_Separate + pcd;

 	try {
		String auth_yn = xgov.core.dao.XDAO.XmlSelect3(request,"array", "com", "AUTH", "PgmAuth", authparams, "all", _Row_Separate,_Col_Separate);
		if(auth_yn.equals("N")) {
			response.sendRedirect("/Mighty/jsp/AuthError.jsp");
			return;
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
	
	//stopWatch.stop();
	//String processTime1 = Long.toString(stopWatch.getTotalTimeMillis());	
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
	<title>X-Internet eGov Framework</title>

	<script type="text/javascript">
		var _Compact = false;
		var _PGM_CODE = "<%=pcd%>";
		var _WIN_OPEN_DATE = "<%=xgov.core.util.XUtility.GetCurrentDate("yyyy-MM-dd HH:mm:ss")%>";		
	</script>
	
	<!-- INCLUDE 변경 (2015.06.08 이상규) -->
	<jsp:include page="/Mighty/include/COMM_JS_Core.jsp"/>
	<jsp:include page="/Mighty/include/COMM_JS_SlickGrid.jsp"/>
	
	<script type="text/javaScript" src="${pageContext.request.contextPath}/APPS/com/js/Common.js"></script>
	<script type="text/javaScript" src="${pageContext.request.contextPath}/APPS/com/js/<%=pcd.substring(0,2).toLowerCase()%>Common.js"></script>
	<script type="text/javaScript" src="${pageContext.request.contextPath}/APPS/<%=pcd.substring(0,2).toLowerCase()%>/grid/<%=pcd%>.SlickGrid.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/APPS/<%=pcd.substring(0,2).toLowerCase()%>/js/<%=pcd%>.js"></script>
</head>

<body style="verticalAlign: top; text-align: left; display: inline;" bgcolor="#ffffff" topmargin="10px">
	<div id="element_to_pop_up" ></div>
	<div id='formLoadImg' style='position:absolute; left:50%; top:40%; z-index:10000;'>
		<img src='/Theme/images/viewLoading.gif'/>
	</div>
	<div id="pageLayout">
		<jsp:include page="<%=subUrl%>"/>
	</div>

	<script type="text/javascript">

		//x_InitForm 호출위치 변경(2014.06.10 김양열)
		if(typeof(x_InitForm)!="undefined"){x_InitForm();}
		
		_X.Block();
		function xm_InitForm() {		
			pageLayout.ResizeInfo = {init_width:1000, init_height:710};
			$(window).bind("resize", xm_BodyResize);
			xm_BodyResize(true);
			
			pageLayout.style.display = 'inline';
			if(window._GridRequestCnt==0){
				_X.UnBlock();
			}
			setTimeout("xm_InitForm_After()",0);
		}

		function xm_InitForm_After() {
			//input style 과 event attach
			xm_InputInitial();
			$(":input:visible:enabled:first").focus();
		}
		$(window).bind("load", xm_InitForm);
		
	

	</script>

</body>

</html>
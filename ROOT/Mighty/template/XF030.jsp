<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<%@ page import ="xgov.core.vo.XLoginVO" %>
<%@ page import ="org.springframework.util.StopWatch" %>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<%
 /**
  * @JSP Name : XF030.jsp
  * @Description : 개별시스템의 popup 창 처리
  * @Modification Information
  *
  *   수정일     수정자          수정내용
  *  -------    --------    ---------------------------
  *  2012.12.01  김양열          최초 생성
  *
  * author (주)엑스인터넷정보
  * Copyright (C) 2012 by X-Internet Info.  All right reserved.
  */
 %>

<%
	String sys = request.getParameter("sys");
	String fcd = request.getParameter("fcd");
	String fnm = request.getParameter("fnm");
	String fgrid = request.getParameter("fgrid");

	//String[] params  = egovframework.rte.com.service.EgovStringUtil.split(fgrid,"|");
	String _Separate = "¸";
	String[] params = egovframework.rte.com.service.EgovStringUtil.split(fgrid, _Separate);
	if(params.length==1) {
		params = egovframework.rte.com.service.EgovStringUtil.split(params[0],"|");
	}
	
	String subUrl = "/APPS/"+sys+"/jsp/"+fcd+".jsp";
%>

<html >
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
	<title></title>

	<script type="text/javascript">
		var _Compact = true;
		var _PGM_CODE = "<%=fcd%>";
	</script>

	<jsp:include page="/Mighty/include/COMM_JS.jsp"/>

	<link type="text/css" charset="utf-8" href="/Mighty/css/mighty-2.0.3.css?v=20180628"  rel="stylesheet" media="all" />
	<link type="text/css" charset="utf-8" href="/Theme/css/custom_sub.css"  rel="stylesheet" media="all" />

	<!-- 추가(2017.11.11 KYY) -->
	<link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap-theme.min.css">

	<script type="text/javascript">
		var _AutoRetrieve = false;
		var _FIND_GRID = "<%=params[0]%>";
		var _FIND_QUERY = "<%=params[1]%>";
		var _FIND_SELECT = "<%=params[2]%>";
		var _FIND_SYS = "<%=sys%>";

		function xm_UnloadForm() {
			//Flash관련 오브젝트 정리(2013.04.15 김양열)
			xm_ClearObject(window);
		}
	</script>

	<link type="text/css" charset="utf-8" href="/Mighty/3rd/jQuery/alert/themes/default/theme.css"  rel="stylesheet" media="all" />
	<link type="text/css" charset="utf-8" href="/Mighty/3rd/jQuery/alert/css/alert.min.css"  rel="stylesheet" media="all" />

	<script type="text/javaScript" src="${pageContext.request.contextPath}/APPS/com/js/Common.js?v=<%=xgov.core.util.XUtility.GetCurrentDate("yyyyMMddHHmmss")%>"></script>
	<script type="text/javaScript" src="${pageContext.request.contextPath}/APPS/com/js/<%=sys.substring(0,2).toLowerCase()%>Common.js?v=<%=xgov.core.util.XUtility.GetCurrentDate("yyyyMMddHHmmss")%>"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/APPS/<%=sys%>/js/<%=params[0]%>.js?v=<%=xgov.core.util.XUtility.GetCurrentDate("yyyyMMddHHmmss")%>" charset="UTF-8"></script>

	<script type=text/javascript src="/Mighty/3rd/jQuery/alert/js/alert.min.js"></script>
	
	<%
		String _Row_Separate = "˛";
		String _Col_Separate = "¸";
		String grid_schima = "";
 		try {
		 	String gridparams  = sys + _Col_Separate + fcd;
			grid_schima = xgov.core.dao.XDAO.XmlSelect3(request,"array", "com", "X5", "GRID_SCRIPT", gridparams, "all", _Row_Separate, _Col_Separate);
		} catch (Exception e) {
			e.printStackTrace();
		}
	%>
	<%if(grid_schima==null || grid_schima.equals("")){%>
	  <script type="text/javaScript" src="/APPS/<%=sys%>/grid/<%=fcd%>.Grid.js?v=<%=xgov.core.util.XUtility.GetCurrentDate("yyyyMMddHHmmss")%>"></script>
	<%}else{%>
	  <script type="text/javaScript"><%=grid_schima%>
	  </script>
	<%}%>

	<base target=_self>
</head>

<body style="verticalAlign: top; text-align: left; display: inline; " bgcolor="#ffffff">

	<div id="pageLayout" class="pageLayout_MG010" style='position:absolute;left:0px;top:0px;width:100%;height:100%;background-color:#ffffff;overflow:hidden;z-index:1;margin:3px; padding:0px;'>
		<jsp:include page="<%=subUrl%>" />
	</div>

	<script type="text/javascript">
		if(typeof(_Caller._FindCode.title)!="undefined") {
			document.title = _Caller._FindCode.title;
		}
		_X.Block();

		function xm_InitForm_After() {
			xm_InputInitial();
			//입력필드 한글입력 활성화(2015.04.07 KYY)
			$("input[type=text]").addClass("ko");
			$(":input:visible:enabled:first").focus();

			//window.dialogWidth  = ($(document).width()+1) + 'px';
			//window.dialogHeight = ($(document).height()+1) + 'px';

		}

	$(window).load(function() {
			pageLayout.ResizeInfo = {init_width:1000, init_height:700};

			var rtValue = 0;
			if(typeof(x_InitForm)!="undefined"){rtValue = x_InitForm();}
			if(rtValue!=null && rtValue<0)
				return;

			$(window).bind("resize", xm_BodyResize);
			$(window).bind("unload", xm_UnloadForm);	//Flash관련 오브젝트 정리(2013.04.15 김양열)

			xm_BodyResize(true);
			pageLayout.style.display = "inline";
			_X.UnBlock();
			setTimeout("xm_InitForm_After()",0);
		});




		$(window).bind("unload", function() {
			//if(_Caller) {
			//	_Caller._X.UnBlock();
			//}
		});

		//$(document).ready(xm_InitForm);
		//xm_InitForm();
	</script>

</body>

</html>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<%@ page import ="xgov.core.vo.XLoginVO" %>
<%@ page import ="org.springframework.util.StopWatch" %>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%
 /**
  * @JSP Name : XG_page.jsp
  * @Description : 기본 페이지 구조
  * @Modification Information
  *
  *   수정일     수정자         수정내용
  *  ----------  --------       ---------------------------
  *  2012.12.01  김양열         최초 생성
  *  2015.06.08  SKLEE          SLICKGRID 버전으로 변경
  *  2017.09.29  JH             SIMPLE 버전으로 변경
  *
  * author (주)엑스인터넷정보
  * Copyright (C) 2015 by X-Internet Info.  All right reserved.
  */
%>

<%
  //StopWatch stopWatch = new StopWatch();
  //stopWatch.start();

  String pcd = request.getParameter("pcd");
  String params = request.getParameter("params");

  String subUrl = "/APPS/" + pcd.substring(0,2).toLowerCase() + "/jsp/" + pcd + ".jsp?params="+params;
  XLoginVO user = (XLoginVO)session.getAttribute("loginVO");
  if(session.getAttribute("userId")==null) {
     session.setAttribute("userId",user.getUserId());
  }
  if(session.getAttribute("deptCode")==null) {
     session.setAttribute("deptCode",user.getDeptCode());
  }
  if(session.getAttribute("companyCode")==null) {
     session.setAttribute("companyCode",user.getCompanyCode());
  }
  if(session.getAttribute("userTag")==null) {
     session.setAttribute("userTag",user.getUserTag());
  }

  //프로그램 실행권한 체크(2015.04.20 KYY)
  String _Row_Separate = "˛";
  String _Col_Separate = "¸";
  String authparams = user.getCompanyCode() + _Col_Separate+user.getUserId() + _Col_Separate + pcd;

  try {
    String auth_yn = xgov.core.dao.XDAO.XmlSelect3(request,"array", "com", "AUTH", "PgmAuth", authparams, "all", _Row_Separate,_Col_Separate);
    if(auth_yn.equals("N")) {
      response.sendRedirect("/Mighty/jsp/AuthError.jsp");
      return;
    }
  } catch (Exception e) {
    e.printStackTrace();
  }

  //stopWatch.stop();
  //String processTime1 = Long.toString(stopWatch.getTotalTimeMillis());
%>

<%@ include file="/Theme/jsp/xg_page.jsp"%>

(function ($) {
  // register namespace
  $.extend(true, window, {
    "Slick": {
      "CheckboxSelectColumn": CheckboxSelectColumn
    }
  });


  function CheckboxSelectColumn(options, header) {
    var _grid;
    var _self = this;
    var _handler = new Slick.EventHandler();
    var _selectedRowsLookup = {};
    var _defaults = {
      columnId: "_checkbox_selector",
      cssClass: null,
      toolTip: "Select/Deselect All",
      width: 30
    };

    var _options = $.extend(true, {}, _defaults, options);
		
		/* init 변경 (2015.11.04 이상규) */
		function init(grid) {
      _grid = grid;
      
      if ( header ) {
      	_handler.subscribe(_grid.onHeaderClick, handleHeaderClick);
      }
      
      _handler.subscribe(_grid.onClick, handleClick)
    }
    /*
    function init(grid) {
      _grid = grid;
      
      if ( header ) {
      	_handler.subscribe(_grid.onHeaderClick, handleHeaderClick);
      }
      
      _handler
        .subscribe(_grid.onSelectedRowsChanged, handleSelectedRowsChanged)
        .subscribe(_grid.onClick, handleClick)
        .subscribe(_grid.onHeaderClick, handleHeaderClick)
        .subscribe(_grid.onKeyDown, handleKeyDown);
    }
    */

    function destroy() {
      _handler.unsubscribeAll();
    }

    function handleSelectedRowsChanged(e, args) {
      var selectedRows = _grid.getSelectedRows();
      var lookup = {}, row, i;
      for (i = 0; i < selectedRows.length; i++) {
        row = selectedRows[i];
        lookup[row] = true;
        if (lookup[row] !== _selectedRowsLookup[row]) {
          _grid.invalidateRow(row);
          delete _selectedRowsLookup[row];
        }
      }
      for (i in _selectedRowsLookup) {
        _grid.invalidateRow(i);
      }
      _selectedRowsLookup = lookup;
      _grid.render();

      if (selectedRows.length && selectedRows.length == _grid.getDataLength()) {
        _grid.updateColumnHeader(_options.columnId, "<input type='checkbox' checked='checked'>", _options.toolTip);
      } else {
        _grid.updateColumnHeader(_options.columnId, "<input type='checkbox'>", _options.toolTip);
      }
    }

    function handleKeyDown(e, args) {
      if (e.which == 32) {
        if (_grid.getColumns()[args.cell].id === _options.columnId) {
          // if editing, try to commit
          if (!_grid.getEditorLock().isActive() || _grid.getEditorLock().commitCurrentEdit()) {
            toggleRowSelection(args.row);
          }
          e.preventDefault();
          e.stopImmediatePropagation();
        }
      }
    }


		/* click 이벤트 변경 (2015.11.04 이상규) */
		function handleClick(e, args) {
			if (_grid.getColumns()[args.cell].id === _options.columnId && $(e.target).is(":checkbox")) {
	      var item = args.grid.getDataItem( args.row ),
	      		rowNode = args.grid.getCellNode( args.row, args.cell ).parentElement,
						gridId  = _grid.getContainerNode().id;
						
				if (e.target.checked) {
					item.check = "Y";
					$(rowNode).addClass( "slick-checkbox-checked" );
				} else {
					item.check = "N";
					$(rowNode).removeClass( "slick-checkbox-checked" );
				}
			}
    }
    /*
    function handleClick(e, args) {console.log(args)
      // clicking on a row select checkbox
      if (_grid.getColumns()[args.cell].id === _options.columnId && $(e.target).is(":checkbox")) {
        // if editing, try to commit
        if (_grid.getEditorLock().isActive() && !_grid.getEditorLock().commitCurrentEdit()) {
          e.preventDefault();
          e.stopImmediatePropagation();
          return;
        }

        toggleRowSelection(args.row);
        e.stopPropagation();
        e.stopImmediatePropagation();
      }
    }
		*/
		
    function toggleRowSelection(row) {
      if (_selectedRowsLookup[row]) {
        _grid.setSelectedRows($.grep(_grid.getSelectedRows(), function (n) {
          return n != row
        }));
      } else {
        _grid.setSelectedRows(_grid.getSelectedRows().concat(row));
      }
    }
		
		/* 로직변경 (2015.09.21 이상규) */
    function handleHeaderClick(e, args) {
      if (args.column.id == _options.columnId && $(e.target).is(":checkbox")) {
        // if editing, try to commit
        if (_grid.getEditorLock().isActive() && !_grid.getEditorLock().commitCurrentEdit()) {
          e.preventDefault();
          e.stopImmediatePropagation();
          return;
        }
				
				var gridId   = _grid.getContainerNode().id,
						gridRows = $("#" + gridId + " .ui-widget-content.slick-row"),
						items  	 = _grid.getData().getItems();
				
				var checked = $(e.target).is( ":checked" ) ? "checked" : "";
				
				var $checkBoxCell = gridRows.find(".slick-cell-checkbox");
				$checkBoxCell.prop( "checked", checked );
				
        if ( checked ) {
        	gridRows.addClass( "slick-checkbox-checked" );
        	
        	for ( var i = 0, ii = items.length; i < ii; i++ ) {
        		items[i].check = "Y";
        	}
        }
        else {
        	gridRows.removeClass( "slick-checkbox-checked" );
        	
        	for ( var i = 0, ii = items.length; i < ii; i++ ) {
        		items[i].check = "N";
        	}
        }
        e.stopPropagation();
        e.stopImmediatePropagation();
      }
    }
    /*
		function handleHeaderClick(e, args) {
      if (args.column.id == _options.columnId && $(e.target).is(":checkbox")) {
        // if editing, try to commit
        if (_grid.getEditorLock().isActive() && !_grid.getEditorLock().commitCurrentEdit()) {
          e.preventDefault();
          e.stopImmediatePropagation();
          return;
        }

        if ($(e.target).is(":checked")) {
          var rows = [];
          for (var i = 0; i < _grid.getDataLength(); i++) {
            rows.push(i);
          }
          _grid.setSelectedRows(rows);
        } else {
          _grid.setSelectedRows([]);
        }
        e.stopPropagation();
        e.stopImmediatePropagation();
      }
    }
    */

    function getColumnDefinition() {
      return {
        id: _options.columnId,
        name: header ? "<input type='checkbox'>" : "",
        toolTip: _options.toolTip,
        /* field: sel -> check 변경 (2015.09.22 이상규) */
        field: "check",
        width: _options.width,
        resizable: false,
        sortable: false,
        cssClass: _options.cssClass,
        formatter: checkboxSelectionFormatter
      };
    }
		
		/* 포멧형식변경 (2015.09.21 이상규) */
    function checkboxSelectionFormatter(row, cell, value, columnDef, dataContext) {
      if (dataContext) {
      	if ( value === "Y" ) {
      		return "<p><input type='checkbox' class='slick-cell-checkbox' checked='checked'></p>";
      	}
      	else {
      		return "<p><input type='checkbox' class='slick-cell-checkbox'></p>";
      	}
      }
      return null;
    }
    /*
    function checkboxSelectionFormatter(row, cell, value, columnDef, dataContext) {
      if (dataContext) {
        return  
        		_selectedRowsLookup[row]
        		? "<input type='checkbox' checked='checked'>"
        		: "<input type='checkbox'>";
      }
      return null;
    }
    */

    $.extend(this, {
      "init": init,
      "destroy": destroy,

      "getColumnDefinition": getColumnDefinition
    });
  }
})(jQuery);
/***
 * Contains basic SlickGrid formatters.
 * 
 * NOTE:  These are merely examples.  You will most likely need to implement something more
 *        robust/extensible/localizable/etc. for your use!
 * 
 * @module Formatters
 * @namespace Slick
 */

	


(function ($) {
  // register namespace
  /*=== Formatters bind (2015.06.08 이상규) ===*/
  $.extend(true, window, {
    "Slick": {
      "Formatters": {
      	"Seq"             	: SeqFormatter,
      	
        "PercentComplete"		: PercentCompleteFormatter,
        "PercentCompleteBar": PercentCompleteBarFormatter,
        "YesNo"							: YesNoFormatter,
        
        "Text"							: TextFormatter,
        
        "Number"						: NumberFormatter,
        
        "NumberF1"					: NumberF1_Formatter,
        "NumberF2"					: NumberF2_Formatter,
        "NumberF3"					: NumberF3_Formatter,
        "NumberF4"					: NumberF4_Formatter,
        
        "Date"							: DateFormatter,
        "DateMonth"					: DateMonthFormatter,
        "DateTime"					: DateTimeFormatter,
        
        "ComboBox"					:	ComboBoxFormatter,
        "ComboBoxCenter"		:	ComboBoxCenterFormatter,
        "ComboBoxRight"			:	ComboBoxRightFormatter,
        "ComboBoxTree"			:	ComboBoxTreeFormatter,
        
        "Checkbox"					: CheckmarkFormatter,
        
        "Tree"              : TreeFormatter,
        
        "GroupText"					: GroupTextFormatter,
        "GroupTotal"				: GroupTotalFormatter
      }
    }
  });
	
  function PercentCompleteFormatter(row, cell, value, columnDef, dataContext) {
    if (value == null || value === "") {
      return "-";
    } else if (value < 50) {
      return "<span style='color:red;font-weight:bold;'>" + value + "%</span>";
    } else {
      return "<span style='color:green'>" + value + "%</span>";
    }
  }

  function PercentCompleteBarFormatter(row, cell, value, columnDef, dataContext) {
    if (value == null || value === "") {
      return "";
    }

    var color;

    if (value < 30) {
      color = "red";
    } else if (value < 70) {
      color = "silver";
    } else {
      color = "green";
    }

    return "<span class='percent-complete-bar' style='background:" + color + ";width:" + value + "%'></span>";
  }

  function YesNoFormatter(row, cell, value, columnDef, dataContext) {
    return value ? "Yes" : "No";
  }

//============================================================== formatter start (2015.06.08 이상규)
	function makeFormatObject(columnDef) {
		var formatObj = {
			getStyleBold: function() {
				return columnDef.styleBold ? " style-text-bold" : "";
			},
			getAutoClass: function(args) {
				return columnDef.getAutoClass && columnDef.getAutoClass( args ) || "";
			},
			getAutoValue: function(args) {
				return columnDef.getAutoValue && columnDef.getAutoValue( args ) || args.value;
			},
			getMaskValue: function(args) {
				return columnDef.getMaskValue && columnDef.getMaskValue( args ) || args.value;
			},
			getGroupMaskValue: function(args) {
				return columnDef.getGroupMaskValue && columnDef.getGroupMaskValue( args ) || args.value;
			}
		};
		
		return formatObj;
	}
	
	function formatProcessing(row, cell, value, columnDef, dataContext, formatType) {
		var formatObj = makeFormatObject( columnDef ),
				args			= { 
					rowIdx : row + 1,
					colName: columnDef.id,
					value  : value,
					oriVal : value,
					dataContext : dataContext
				};
		
		switch( formatType ) {
			case "text":
				value = args.value = formatObj.getAutoValue( args );
				break;

			case "number":
				value = args.value = formatObj.getAutoValue( args );
				value = args.value = _X.FormatCommaPoint2( value, 0, columnDef.numberOnly );
				break;
			
			case "numberf1":
			case "numberf2":
			case "numberf3":
			case "numberf4":
				value = args.value = formatObj.getAutoValue( args );
				value = args.value = _X.FormatCommaPoint2( value, Number( formatType.substring( formatType.length - 1 ) ), columnDef.numberOnly );
				break;

			case "date":
				value = args.value = _X.ToString( value, "yyyy-mm-dd" );
				break;
			
			case "datemonth":
				value = args.value = _X.ToString( value, "yyyy-mm" );
				break;
				
			case "datetime":
				value = args.value = _X.ToString( value, "yyyy-mm-dd hh:mi:ss" );
				break;
		}
		
		value = args.value = formatObj.getMaskValue( args );
		
		if ( value == null || value === "" ) {
			if ( columnDef.button && columnDef.alwaysShowButton ) {
				if ( columnDef.textButton ) {
					return returnVal = "<p class='" + columnDef.align + boldClass + AutoClass + "'><span class='cell-button2' onClick='xe_M_SlickGridButtonClick( " + columnDef.gridId + ", " + row + ", " + cell + " );'>" + columnDef.textButton + "</span></p>";
				} else {
					return returnVal = "<p class='" + columnDef.align + boldClass + AutoClass + "'><span class='cell-button' onClick='xe_M_SlickGridButtonClick( " + columnDef.gridId + ", " + row + ", " + cell + " );'></span></p>";
				}
			}
			return "";
		}
		
		var boldClass  = formatObj.getStyleBold(),
				AutoClass  = formatObj.getAutoClass( args ),
				returnVal  = "";
		
		switch( formatType ) {
			case "text":
			case "number":
			case "numberf1":
			case "numberf2":
			case "numberf3":
			case "numberf4":
			case "date":
			case "datemonth":
			case "datetime":
				returnVal = columnDef.button && "<span class='cell-left-value mr17'>" + value + "</span>" || "<span>" + value + "</span>";
				break;
				
			case "comboBox":
				if ( columnDef.lookupDisplay ) {
		  		value = columnDef.comboData && columnDef.comboData.syncLabel[ value ] || value;
		  	}
		  	
		  	if ( columnDef.dataType === "tree" ) {
		  		return value;
		  	} else {
		  		returnVal = columnDef.button && "<span class='cell-left-value mr17'>" + value + "</span>" || "<span>" + value + "</span>";
		  	}
		  	break;
		}
		
		//Cell버튼 처리(2016.01.22 KYY)
		if ( columnDef.button && columnDef.alwaysShowButton) {
			//커스텀 버튼처리(2016.03.31 LSK)
			if ( columnDef.textButton ) {
				returnVal += "<span class='cell-button2' onClick='xe_M_SlickGridButtonClick( " + columnDef.gridId + ", " + row + ", " + cell + " );'>" + columnDef.textButton + "</span>";
			} else {
				returnVal += "<span class='cell-button' onClick='xe_M_SlickGridButtonClick( " + columnDef.gridId + ", " + row + ", " + cell + " );'></span>";
			}
		}
		
		var bgColor = "";
		if ( columnDef.comboData && columnDef.comboData.dropDownColor.length ) {
			bgColor = value;
		}
		
		return returnVal = "<p class='" + columnDef.align + boldClass + AutoClass + "' style='background-color: " + bgColor + ";'>" + returnVal + "</p>";
	}
	
	function groupFormatProcessing(value, columnDef, formatType) {
		var formatObj = makeFormatObject( columnDef ),
				args			= { 
					colName: columnDef.id,
					value  : value,
					oriVal : value
				};
				
		var	getMaskValue = function () {
				if ( columnDef.getGroupMaskValue ) {
					var val = formatObj.getGroupMaskValue( args );
					return val == 0 ? 0 : val || "";
				}
				else {
					try {
						var val = formatObj.getMaskValue( args );
						return val == 0 ? 0 : val || "";
					} catch (err) {
						return args.value == 0 ? 0 : args.value || "";
					}
				}
		};
		
		try {
				switch( formatType ) {
					case "text":
						break;
		
					case "number":
						value = args.value = _X.FormatCommaPoint2( value, 0, columnDef.numberOnly );
						break;
						
					case "numberf1":
					case "numberf2":
					case "numberf3":
					case "numberf4":
						value = args.value = _X.FormatCommaPoint2( value, Number( formatType.substring( formatType.length - 1 ) ), columnDef.numberOnly );
						break;
		
					case "date":
						value = args.value = _X.ToString( value, "yyyy-mm-dd" );
						break;
					
					case "datemonth":
						value = args.value = _X.ToString( value, "yyyy-mm" );
						break;
					
					case "datetime":
						value = args.value = _X.ToString( value, "yyyy-mm-dd hh:mi:ss" );
						break;
				}
		} catch (err) {
			return getMaskValue();
		}
		
		return getMaskValue();
	}
	
	/*=== Seq Area ===*/
	function SeqFormatter(row, cell, value, columnDef, dataContext) {
		if ( columnDef.groupSeq && dataContext.seq ) {
			return "<p>" + dataContext.seq + "</p>";
		}
		return "<p>" + ( row + 1 ) + "</p>";
	}
	
  /*=== Text Area ===*/
  function TextFormatter(row, cell, value, columnDef, dataContext) {
    return formatProcessing(row, cell, value, columnDef, dataContext, columnDef.dataType);
  }
  
  /*=== Number Area ===*/
  function NumberFormatter(row, cell, value, columnDef, dataContext) {
  	return formatProcessing(row, cell, value, columnDef, dataContext, columnDef.dataType);
  }
  
  /*=== Number F Area ===*/
  function NumberF1_Formatter(row, cell, value, columnDef, dataContext) {
  	return formatProcessing(row, cell, value, columnDef, dataContext, columnDef.dataType);
  }
  function NumberF2_Formatter(row, cell, value, columnDef, dataContext) {
  	return formatProcessing(row, cell, value, columnDef, dataContext, columnDef.dataType);
  }
  function NumberF3_Formatter(row, cell, value, columnDef, dataContext) {
  	return formatProcessing(row, cell, value, columnDef, dataContext, columnDef.dataType);
  }
  function NumberF4_Formatter(row, cell, value, columnDef, dataContext) {
  	return formatProcessing(row, cell, value, columnDef, dataContext, columnDef.dataType);
  }
  
  /*=== date Area ===*/
  function DateFormatter(row, cell, value, columnDef, dataContext) {
  	return formatProcessing(row, cell, value, columnDef, dataContext, columnDef.dataType);
  }
  function DateMonthFormatter(row, cell, value, columnDef, dataContext) {
  	return formatProcessing(row, cell, value, columnDef, dataContext, columnDef.dataType);
  }
  function DateTimeFormatter(row, cell, value, columnDef, dataContext) {
  	return formatProcessing(row, cell, value, columnDef, dataContext, columnDef.dataType);
  }
  
  /*=== ComboBox Area ===*/
  function ComboBoxFormatter(row, cell, value, columnDef, dataContext) {
  	return formatProcessing(row, cell, value, columnDef, dataContext, "comboBox");
  }
  function ComboBoxCenterFormatter(row, cell, value, columnDef, dataContext) {
  	return formatProcessing(row, cell, value, columnDef, dataContext, "comboBox");
  }
  function ComboBoxRightFormatter(row, cell, value, columnDef, dataContext) {
  	return formatProcessing(row, cell, value, columnDef, dataContext, "comboBox");
  }
  function ComboBoxTreeFormatter(row, cell, value, columnDef, dataContext) {
  	value = formatProcessing(row, cell, value, columnDef, dataContext, "comboBox");
  	return TreeFormatter(row, cell, value, columnDef, dataContext);
  }
  
  /*=== CheckBox Area ===*/
  function CheckmarkFormatter(row, cell, value, columnDef, dataContext) {
  	//수정(2015.12.14 KYY)
    return "<p class='align-center'>" + (value === "Y" ? "<img src='/Mighty/3rd/v2.2/SlickGrid/images/tick.png' class='align-center-check'>" : 
    											 "<img src='/Mighty/3rd/SlickGrid/v2.2/images/tick-no.png' class='align-center-check'>") + "</p>";
  }
  
  /*=== Tree Area ===*/
  function TreeFormatter(row, cell, value, columnDef, dataContext) {
		//수정(2016.01.20 KYY)
	  value = value.replace( /&/g,"&amp;" )
	  						 .replace( /</g,"&lt;" )
	  						 .replace( />/g,"&gt;" );
	  					 
	  var spacer 		= "<div class='tree-space'></div>",
	  		startLine = dataContext.treeData.treeLineInfo[ dataContext.currentTreeData.lineParent ] || dataContext.LEV;
	  
	  /* spacer setting */
		if ( startLine > dataContext.LEV ) {
			for ( var i = 0, ii = ( dataContext.LEV - 2 ); i < ii; i++ ) {
				spacer += "<div class='tree-space'></div>";
			}
		}
		else {
			for ( var i = 0, ii = startLine - 2; i < ii; i++ ) {
				spacer += "<div class='tree-space'></div>";
			}

			var cutLine = [],
					parent = dataContext.currentTreeData.parent;
			
			for ( var i = 0, ii = dataContext.LEV - startLine; i < ii; i++ ) {
				if ( parent.parent && parent.parent.child[ parent.parent.child.length - 1 ].id === parent.id ) {
					cutLine[i] = true;
				} else {
					cutLine[i] = false;
				}
				
				parent = parent.parent;
			}
			
			var addSpacer = "";
			
			for ( var i = 0, ii = dataContext.LEV - startLine; i < ii; i++ ) {
				if ( cutLine[i] ) {
					addSpacer = "<div class='tree-space'></div>" + addSpacer;
				} else {
					addSpacer = "<div class='tree-line1'></div>" + addSpacer;
				}
			}
			
			spacer += addSpacer;
		}
		
		/* line setting */
		if ( dataContext.LEV == 1 ) {
			/* level 1 */
			if ( dataContext._collapsed ) {
	      return "<div class='tree-space'></div> <div class='toggle tree-expand'></div>" + "<div class='tree-text'>" + value + "</div>";
	    } else {
	      return "<div class='tree-space'></div> <div class='toggle tree-collapse'></div>" + "<div class='tree-text'>" + value + "</div>";
	    }
		}
		else if ( startLine > dataContext.LEV ) {
			/* line 없음 */
			if ( dataContext._collapsed ) {
	      return spacer + "<div class='tree-line3'></div>"
	      							+ "<div class='toggle tree-expand'></div>" + "<div class='tree-text'>" + value + "</div>";
	    } else {
	      return spacer + "<div class='tree-line3'></div>"
	      							+ "<div class='toggle tree-collapse'></div>" + "<div class='tree-text'>" + value + "</div>";
	    }
		}
		else if ( startLine <= dataContext.LEV ) {
			var parent = dataContext.currentTreeData.parent;
			
			if ( parent.child[ parent.child.length - 1 ].id === dataContext.currentTreeData.id ) {
				/* lastLine child 있음  */
				if ( dataContext.currentTreeData.child.length ) {
			    if ( dataContext._collapsed ) {
			      return spacer + "<div class='tree-line3'></div>"
	      							+ "<div class='toggle tree-expand'></div>" + "<div class='tree-text'>" + value + "</div>";
			    } else {
			      return spacer + "<div class='tree-line3'></div>"
	      							+ "<div class='toggle tree-collapse'></div>" + "<div class='tree-text'>" + value + "</div>";
			    }
			  }
			  else {
			  	/* lastLine child 없음 */
			  	return spacer + "<div class='tree-line3'></div>"
	      							+"<div class='toggle'></div>" + "<div class='tree-text'>" + value + "</div>";			  	 
			  }
			}
			else {
				/* crossLine child 있음 */
				if ( dataContext.currentTreeData.child.length ) {
			    if ( dataContext._collapsed ) {
			      return spacer + "<div class='tree-line2'></div>"
	      							+ "<div class='toggle tree-expand'></div>" + "<div class='tree-text'>" + value + "</div>";
			    } else {
			      return spacer + "<div class='tree-line2'></div>"
	      							+ "<div class='toggle tree-collapse'></div>" + "<div class='tree-text'>" + value + "</div>";
			    }
			  }
			  else {
			  	/* crossLine child 없음 */
			  	return spacer + "<div class='tree-line2'></div>"
	      							+ "<div class='toggle'></div>" + "<div class='tree-text'>" + value + "</div>";
			  }
			}
		}
	};


	/* GroupTotal Area */
	function GroupTextFormatter(totals, columnDef) {
  	var align = "";
  	if ( columnDef.group && columnDef.group.align ) {
  		align = "align-" + columnDef.group.align;
  	}
  	else {
  		align = columnDef.align;
  	}
  	
  	var preText = "";
  	if ( columnDef.group && columnDef.group.preText ) {
  		preText = columnDef.group.preText;
  	}
  	
  	var text = "";
  	if ( columnDef.group && columnDef.group.text ) {
  		text = columnDef.group.text;
  	}
  	
  	var postText = "";
  	if ( columnDef.group && columnDef.group.postText ) {
  		postText = columnDef.group.postText;
  	}
  	
    return "<p class='" + align + "'>" + preText + text + postText + "</p>";;
	}
	
	function GroupTotalFormatter(totals, columnDef) {
	  var val = totals.expression && totals.expression[ columnDef.field ];
	  if (val != null) {
	  	var align = "";
	  	if ( columnDef.group && columnDef.group.align ) {
	  		align = "align-" + columnDef.group.align;
	  	}
	  	else {
	  		align = columnDef.align;
	  	}
	  	
	  	var preText = "";
	  	if ( columnDef.group && columnDef.group.preText ) {
	  		preText = columnDef.group.preText;
	  	}
	  	
	  	var postText = "";
	  	if ( columnDef.group && columnDef.group.postText ) {
	  		postText = columnDef.group.postText;
	  	}
	  	
	  	val = groupFormatProcessing( val, columnDef, columnDef.dataType );

	  	return "<p class='" + align + "'>" + preText + val + postText + "</p>";
	  }

	  return "";
	}

//============================================================== formatter end (2015.06.08 이상규)
})(jQuery);

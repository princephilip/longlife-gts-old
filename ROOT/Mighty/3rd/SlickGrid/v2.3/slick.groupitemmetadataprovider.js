(function ($) {
  $.extend(true, window, {
    Slick: {
      Data: {
        GroupItemMetadataProvider: GroupItemMetadataProvider
      }
    }
  });


  /***
   * Provides item metadata for group (Slick.Group) and totals (Slick.Totals) rows produced by the DataView.
   *dataview로 부터 그룹 (slick.group) 과 합계에 대한 (Slick.Totals) 행을 생산하여 항목의 메타 데이터를 제공합니다.
   * This metadata overrides the default behavior and formatting of those rows so that they appear and function
   *메타데이터의 재정의, 기본동작과 해당행의 서식 및 표시 와 기능
   * correctly when processed by the grid.
   *정확하게 그리드에 의해 처리
   *
   * This class also acts as a grid plugin providing event handlers to expand & collapse groups.
   *또한 이 클래스의 역할은 그리드 이벤트 핸들러를 제공하는 플로그인에 확장 및 그룹을 축소 합니다.
   * If "grid.registerPlugin(...)" is not called, expand & collapse will not work.
   *"grid.registerPlugin (...)가"호출되지 않은 경우, 확장 및 축소가 작동하지 않습니다.
   *
   * @class GroupItemMetadataProvider
   * @module Data
   * @namespace Slick.Data
   * @constructor
   * @param options
   */
  function GroupItemMetadataProvider(options) {
    var _grid;
    var _defaults = {
      groupCssClass: "slick-group",
      groupKey: "",
      groupTitleCssClass: "slick-group-title",
      totalsCssClass: "slick-group-totals",
      groupFocusable: false,
      totalsFocusable: false,
      toggleCssClass: "slick-group-toggle",
      toggleExpandedCssClass: "expanded",
      toggleCollapsedCssClass: "collapsed",
      enableExpandCollapse: true,
      groupFormatter: defaultGroupCellFormatter,
      totalsFormatter: defaultTotalsCellFormatter
    };

    options = $.extend(true, {}, _defaults, options);

		/* defaultGroupCellFormatter 재정의 (2015.11.11 이상규) */
		
    function defaultGroupCellFormatter(row, cell, value, columnDef, item) {
      if (!options.enableExpandCollapse) {
        return item.title;
      }
			
      var indentation = item.level * 15 + "px";
      
      //클릭 이벤트
      var _groupKey	 = "group-" + row;
      item.id			   = "groupId_" + _groupKey;
     	item.groupKey  = _groupKey;
     	
	    if ( item.totals ) {
     		item.totals.id = "groupTotalId_" + _groupKey;
     		item.totals.groupKey = _groupKey;
     	}
			
     	for ( var i = 0, ii = item.rows.length; i < ii; i++ ) {
     		//formatter 순번
     		item.rows[i].seq = i + 1;
     		item.rows[i].groupKey = _groupKey;
     	}
     	
      return "<span class='" + options.toggleCssClass + " group-level-" + item.level + " " +
          (item.collapsed ? options.toggleCollapsedCssClass : options.toggleExpandedCssClass) +
          "' style='margin-left:" + indentation +"'>" +
          "</span>" +
          "<span class='" + options.groupTitleCssClass + "' level='" + item.level + "'>" +
            item.title +
          "</span>";
    }
    /*
		function defaultGroupCellFormatter(row, cell, value, columnDef, item) {
      if (!options.enableExpandCollapse) {
        return item.title;
      }

      var indentation = item.level * 15 + "px";
     	
      return "<span class='" + options.toggleCssClass + " " +
          (item.collapsed ? options.toggleCollapsedCssClass : options.toggleExpandedCssClass) +
          "' style='margin-left:" + indentation +"'>" +
          "</span>" +
          "<span class='" + options.groupTitleCssClass + "' level='" + item.level + "'>" +
            item.title +
          "</span>";
    }
    */
    
    function defaultTotalsCellFormatter(row, cell, value, columnDef, item) {
    	if (columnDef.id === "sel") {
    		return row + 1;
    	}
      return (columnDef.groupTotalsFormatter && columnDef.groupTotalsFormatter(item, columnDef)) || "";
    }


    function init(grid) {
      _grid = grid;
      _grid.onClick.subscribe(handleGridClick);
      _grid.onKeyDown.subscribe(handleGridKeyDown);

    }

    function destroy() {
      if (_grid) {
        _grid.onClick.unsubscribe(handleGridClick);
        _grid.onKeyDown.unsubscribe(handleGridKeyDown);
      }
    }
		
		/* handleGridClick 재정의 (2015.11.19 이상규) */
    function handleGridClick(e, args) {
      var item = this.getDataItem(args.row);
      if (item && item instanceof Slick.Group && $(e.target).hasClass(options.toggleCssClass)) {
        var range = _grid.getRenderedRange();
        this.getData().setRefreshHints({
          ignoreDiffsBefore: range.top,
          ignoreDiffsAfter: range.bottom
        });

        if (item.collapsed) {
          this.getData().expandGroup(item.groupingKey);
        } else {
          this.getData().collapseGroup(item.groupingKey);
        }

        e.stopImmediatePropagation();
        e.preventDefault();
      }
      else if ( item.__group || item.__groupTotals ) {
      	if ( item.groupKey ) {
      		var dataView = _grid.getData();
      		dataView.refresh();
      		
      		var addGroupClass = function (groupItem, groupKey, firtsLevel) {
      			var rowNode  = null;
      			
	      		if ( firtsLevel >= groupItem.level ) {
	      			for ( var i = 0, ii = groupItem.rows.length; i < ii; i++ ) {
	      				rowNode = _grid.getCellNode( dataView.getRowById( groupItem.rows[i].id ), 0 );
		      			if( rowNode ) {
		      				rowNode.parentElement.classList.add( groupKey );
		      			}
		      		}
	      		}
	      		else {
	      			rowNode = _grid.getCellNode( dataView.getRowById( groupItem.id ), 0 );
	      			if( rowNode ) {
	      				rowNode.parentElement.classList.add( groupKey );
	      			}
	      		}
	      		
	      		if ( groupItem.totals ) {
      				rowNode = _grid.getCellNode( dataView.getRowById( groupItem.totals.id ), 0 );
      				if( rowNode ) {
	      				rowNode.parentElement.classList.add( groupKey );
	      			}
      			}
      			
	      		if ( groupItem.groups ) {
	      			for ( var i = 0, ii = groupItem.groups.length; i < ii; i++ ) {
	      				addGroupClass( groupItem.groups[i], groupKey, firtsLevel );
		      		}
	      		}
      		};
      		
      		var groupItem_ = item.__groupTotals ? item.group : item;
      		addGroupClass( groupItem_, groupItem_.groupKey, groupItem_.level );
      		
	      	var $groups = $("." + item.groupKey);
	      	$groups.removeClass( "group-effect" ).addClass( "group-effect" );
	      	setTimeout( function () { $groups.removeClass( "group-effect" ); }, 300 );	
	      }
      }
    }
		/*
		function handleGridClick(e, args) {
      var item = this.getDataItem(args.row);
      if (item && item instanceof Slick.Group && $(e.target).hasClass(options.toggleCssClass)) {
        var range = _grid.getRenderedRange();
        this.getData().setRefreshHints({
          ignoreDiffsBefore: range.top,
          ignoreDiffsAfter: range.bottom
        });

        if (item.collapsed) {
          this.getData().expandGroup(item.groupingKey);
        } else {
          this.getData().collapseGroup(item.groupingKey);
        }

        e.stopImmediatePropagation();
        e.preventDefault();
      }
    }
		*/
		
    // TODO:  add -/+ handling
    function handleGridKeyDown(e, args) {
      if (options.enableExpandCollapse && (e.which == $.ui.keyCode.SPACE)) {
        var activeCell = this.getActiveCell();
        if (activeCell) {
          var item = this.getDataItem(activeCell.row);
          if (item && item instanceof Slick.Group) {
            var range = _grid.getRenderedRange();
            this.getData().setRefreshHints({
              ignoreDiffsBefore: range.top,
              ignoreDiffsAfter: range.bottom
            });

            if (item.collapsed) {
              this.getData().expandGroup(item.groupingKey);
            } else {
              this.getData().collapseGroup(item.groupingKey);
            }

            e.stopImmediatePropagation();
            e.preventDefault();
          }
        }
      }
    }
		
		function getGroupRowMetadata(item) {
      return {
        selectable: false,
        focusable: options.groupFocusable,
        cssClasses: options.groupCssClass,
        columns: {
          0: {
            colspan: "*",
            formatter: options.groupFormatter,
            editor: null
          }
        }
      };
    }
		
		function getTotalsRowMetadata(item) {
      return {
        selectable: false,
        focusable: options.totalsFocusable,
        cssClasses: options.totalsCssClass,
        formatter: options.totalsFormatter,
        editor: null
      };
    }


    return {
      "init": init,
      "destroy": destroy,
      "getGroupRowMetadata": getGroupRowMetadata,
      "getTotalsRowMetadata": getTotalsRowMetadata
    };
  }
})(jQuery);

﻿/**
 * Object 			: slick.footerexpression.js
 * @Description : slickgrid 합계
 * @author 			: 엑스인터넷정보 이상규
 * @since 			: 2016.12.23
 * @version 		:
 *
 * @Modification Information
 * <pre>
 *   since    	     author      Description
 *  ------------    --------    ----------------------------
 *   2016.12.23      이상규      최초생성
 */
(function ($) {
  // register namespace
  $.extend(true, window, {
    "Slick": {
    	"Data": {
    		"FooterExpression": FooterExpression
    	}
    }
  });

  function FooterExpression(options) {
    var _grid;
    var _data;
    var _self = this;
    var _handler = new Slick.EventHandler();
    var _options;
    var _defaults = {
    	footerRowCount: 0,
    	formatter: defaultFormatter
    };

    function init(grid) {
      _options = $.extend(true, {}, _defaults, options);
      _grid = grid;
      _data = grid.getData();
      _handler.subscribe(_grid.onCellChange, setCellFooterData);
      _handler.subscribe(_data.onRowCountChanged, setAllFooterData);
      _handler.subscribe(_grid.onCreatedColumnHeaders, setAllFooterData);
    }

    function destroy() {
      _handler.unsubscribeAll();
    }

		function setCellFooterData(e, args) {
			if (args.grid.getInitialized() && _options.footerRowCount) {
				var columns = _grid.getColumns();
				var column  = columns[args.cell];

				if (column && column.footerExpr != null && column.footerExpr != "") {
					var total = 0;
					var exprs = column.footerExpr.split("|");

					for (var i = 0, ii = _options.footerRowCount; i < ii; i++) {
						var footerEle   = $("#" + _grid.getUid() + column.id + "_" + i + "_f");
						var footerAlign = column.footerAlign ? "align-" + column.footerAlign : column.align;
						footerEle.empty();

						if ( exprs[i] ) {
							var expr = exprs[i].toUpperCase();
							var filteredItems = _data.getFilteredItems(_data.getItems());

							switch(expr) {
								case "SUM":
								case "AVG":
								case "MIN":
								case "MAX":
								case "CNT":
								case "COUNT":
									total = getTotalValue(column.id, expr, filteredItems.rows);
									break;
							}

							footerEle.append("<p class='" + footerAlign + "'><span class='slick-footer'>"
							                + ( column.footerPre || "" )
															+ ( expr == "CNT" || expr == "COUNT"
															  ? formatCommaPoint(total, 0, false) + (column.footerPost == "" ? "건" : "")
															  : expr == "SUM" || expr == "AVG" || expr == "MIN" || expr == "MAX"
															  ? _options.formatter(column.dataType, total, column)
															  : expr == "AUTO" && column.getAutoFooterValue
																? column.getAutoFooterValue(i + 1, footerEle)
																: expr )
															+ ( column.footerPost || "" )
															+ "</span></p>");

				      if (column && column.footerStyle) {
				      	var footerStyle = column.footerStyle;
				      	for (var prop in footerStyle) {
				      		if (footerStyle.hasOwnProperty(prop)) {
				      			footerEle.css(prop, footerStyle[prop]);
				      		}
				      	}
				      }
						}
					}
				}
			}
    }

		function setAllFooterData(e, args) {
			if (_options.footerRowCount) {
	    	var columns = _grid.getColumns();
				var column;

				for (var i = 0, ii = columns.length; i < ii; i++) {
					column = columns[i];

					if (column && column.footerExpr != null && column.footerExpr != "") {
						var total = 0;
						var exprs = column.footerExpr.split("|");

						for (var j = 0, jj = _options.footerRowCount; j < jj; j++) {
							var footerEle   = $("#" + _grid.getUid() + column.id + "_" + j + "_f");
							var footerAlign = column.footerAlign ? "align-" + column.footerAlign : column.align;
							footerEle.empty();

							if (exprs[j]) {
								var expr = exprs[j].toUpperCase();
								var filteredItems = _data.getFilteredItems(_data.getItems());

								switch(expr) {
									case "SUM":
									case "AVG":
									case "MIN":
									case "MAX":
									case "CNT":
									case "COUNT":
										total = getTotalValue(column.id, expr, filteredItems.rows);
										break;
								}

								footerEle.append("<p class='" + footerAlign + "'><span class='slick-footer'>"
																+ ( column.footerPre || "" )
																+ ( expr == "CNT" || expr == "COUNT"
																  ? formatCommaPoint(total, 0, false) + (column.footerPost == "" ? "건" : "")
																  : expr == "SUM" || expr == "AVG" || expr == "MIN" || expr == "MAX"
																  ? _options.formatter && _options.formatter(column.dataType, total, column) || total
																	: expr == "AUTO" && column.getAutoFooterValue
																	? column.getAutoFooterValue(j + 1, footerEle)
																	: expr )
																+ ( column.footerPost || "" )
																+ "</span></p>");

							  if (column && column.footerStyle) {
					      	var footerStyle = column.footerStyle;
					      	for (var prop in footerStyle) {
					      		if (footerStyle.hasOwnProperty(prop)) {
					      			footerEle.css(prop, footerStyle[prop]);
					      		}
					      	}
					      }
							}
						}
					}
				}
			}
    }

	  function getTotalValue(colName, expr, items) {
	  	if (items == null || items.length <= 0) {
	  		return 0;
	  	}

	  	if (expr.toUpperCase() == "CNT" || expr.toUpperCase() == "COUNT") {
	  		return items.length;
	  	}

	  	var minVal = isNaN( parseFloat(items[0][colName]) ) ? 0 : parseFloat(items[0][colName]);
	  	var maxVal = minVal;
	  	var totVal = minVal;

  		//Footer라인 소숫점 오류로 수정(2018.11.06 YSKIM)
	  	var fixPoint = 0;
  		if(expr.toUpperCase() == "SUM") {
  			var y = (totVal%1).toString();
  			var p = y.length - 2;
  			if(p > fixPoint) fixPoint = (p > 2) ? 2 : p;
  		}

	  	for (var i = 1, ii = items.length; i < ii; i++) {
	  		var curVal = isNaN( parseFloat(items[i][colName]) ) ? 0 : parseFloat(items[i][colName]);

	  		//Footer라인 소숫점 오류로 수정(2018.11.06 YSKIM)
	  		if(expr.toUpperCase() == "SUM") {
	  			var y = (curVal%1).toString();
	  			var p = y.length - 2;
	  			if(p > fixPoint) fixPoint = (p > 2) ? 2 : p;
	  		}

	  		//Footer라인 소숫점 오류로 수정(2017.06.14 KYY)
	  		//totVal = (parseFloat(totVal) + curVal ).toFixed(items[i][colName].decimalPoint);
	  		totVal += curVal;

	  		if (curVal > maxVal) {
	  			maxVal = curVal;
	  		}

	  		if (curVal < minVal) {
	  			minVal = curVal;
	  		}
	    }

	    switch (expr.toUpperCase()) {
	    	case "MIN":
	    		return minVal;

	    	case "MAX":
	    		return maxVal;

	    	case "AVG":
	    		return totVal / items.length;

	    	case "SUM":
	    		//return totVal;
	    		//Footer라인 소숫점 오류로 수정(2018.11.06 YSKIM)
	    		return totVal.toFixed(fixPoint);
	    }
	  }

	  function formatCommaPoint(num, len, comma) {
			if (num === "" || isNaN(num)) {
				return "";
			}

			var currency = "",
					snum = num + "",
					l, r;

		  if (snum.indexOf(".") !== -1) {
		  	l = snum.split(".")[0] || "0";
		  	r = snum.split(".")[1];
		  } else {
		  	l = parseInt(num, 10) + "";
		  	r = "";
		  }

		  if (len > 0) {
				if (r.length > len) {
					r = r.substring(0, len);
				} else {
					r = _X.RPad(r, len, "0");
				}
				r = "." + r;
			}
			else {
				r = "";
			}

		  while (!comma && l.length > 3) {
				currency = "," + l.substr(l.length - 3, 3) + currency;
				l = l.substring(0, l.length - 3);
		  }

		  var retVal = l + currency + r;

		  return retVal.replace("-,", "-");
		}

		function defaultFormatter(formatType, value, columnDef) {
			switch ( formatType ) {
				case "amt":
				case "AMT":
				case "cnt":
				case "CNT":
				case "count":
				case "COUNT":
					value = formatCommaPoint( value, 0, false );
					break;
			}
			return value;
		}

    return {
      "init": init,
      "destroy": destroy,

      "getTotalValue": getTotalValue,
      "setAllFooterData": setAllFooterData
    };
  }
})(jQuery);

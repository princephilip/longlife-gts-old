﻿<!--
 * Object 			: FileRowDelete.jsp
 * @Description : slickgrid 행 파일삭제
 * @author 			: 엑스인터넷정보 이상규
 * @since 			: 2017.01.19
 * @version 		:
 *
 * @Modification Information
 * <pre>
 *   since    	     author      Description
 *  ------------    --------    ----------------------------
 *   2017.01.19      이상규      최초생성
-->

<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.File" %>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="java.lang.StringBuffer" %>

<%@ page import="xgov.core.dao.XSQLRun" %>
<%@ page import="xgov.core.env.XConfiguration" %>

<%@ page import="org.json.simple.JSONObject" %>

<%
	response.setContentType("text/html; charset=UTF-8");
	PrintWriter pOut = response.getWriter();

	String subdir           = request.getParameter("subdir");                    // 저장 디렉토리 (시스템명)
	String atchFileIds      = request.getParameter("atchids");                   // 삭제 파일 ID
	String [] atchFileIdArr = atchFileIds.split(",");                            // 삭제 파일 ID 배열
	String sRootPath        = XConfiguration.getString("Globals.fileStorePath"); // 저장 경로 첫 번째 (globals.properties 의 저장 경로)
	String sqlResult        = null;                                              // 쿼리 결과 값
	XSQLRun xsql            = null;                                              // 쿼리 실행 객체
	String strSql           = null;
	String [] resultArr     = null;                                              // 결과 값 배열

	try {
		// 쿼리 실행 객체 생성
		xsql = new XSQLRun(subdir);

		// System.out.println(atchFileIds);

		for (int i = 0; i < atchFileIdArr.length; i++) {
			if (atchFileIdArr[i] != null && !atchFileIdArr[i].equals("")) {
				strSql =  "SELECT ATCH_FILE_ID" +
								",	FILE_SN" +
								",	FILE_STRE_COURS" +
								",	STRE_FILE_NM" +
								",	ORIGNL_FILE_NM" +
								",	FILE_EXTSN" +
								",	FILE_CN" +
								",	FILE_SIZE " +
						"FROM		SM_COMM_FILE_DETAIL " +
						"WHERE	ATCH_FILE_ID = '" + atchFileIdArr[i] + "' " +
						"AND		FILE_SN      = 1";

				// 파일 정보 조회
				// System.out.println("52: " + strSql);
				xsql.executeSQL(strSql);

				sqlResult = xsql.getResult();
				// System.out.println("56: " + sqlResult);

				resultArr = sqlResult.split("\t");

				// 파일경로 조회
				String deleteFileName = resultArr[3];
				String deleteFilePath = sRootPath + resultArr[2] + "/" + deleteFileName;
				deleteFilePath = deleteFilePath.replaceAll("\\\\", "/");
				// System.out.println("64: " + deleteFilePath);

				// 파일 삭제
				File df = new File(deleteFilePath);
				df.delete();

				// svg 파일 삭제
				if ( deleteFileName.substring( deleteFileName.lastIndexOf(".") + 1 ).toUpperCase().equals("DWG") ) {
					deleteFilePath = deleteFilePath.substring(0, deleteFilePath.lastIndexOf(".")) + ".svg";
					//System.out.println(deleteFilePath);
					df = new File(deleteFilePath);
					df.delete();
				}

				// DELETE 구문 생성
				strSql = "DELETE SM_COMM_FILE_DETAIL WHERE ATCH_FILE_ID = '" + atchFileIdArr[i] + "'";

				// 테이블 데이타 삭제
				// System.out.println("74: " + strSql);
				xsql.executeSQLs(strSql);
			}
		}

		String deleteinfo = new StringBuffer( xgov.core.util.XBase64Coder.encodeString("[{}]") ).reverse().toString();

		JSONObject jsonObj  = new JSONObject();
		jsonObj.put("data", deleteinfo);

		String jsonStr = jsonObj.toJSONString();

		pOut.println(jsonStr);
		pOut.flush();
		pOut.close();

	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		if (xsql != null) {xsql.close();}
		xsql = null;
	}
%>

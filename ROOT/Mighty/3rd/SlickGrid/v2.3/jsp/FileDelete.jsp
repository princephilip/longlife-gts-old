<!--
 * Object 			: FileDelete.jsp
 * @Description : slickgrid 파일삭제
 * @author 			: 엑스인터넷정보 이상규
 * @since 			: 2017.01.11
 * @version 		:
 *
 * @Modification Information
 * <pre>
 *   since    	     author      Description
 *  ------------    --------    ----------------------------
 *   2017.01.11      이상규      최초생성
-->

<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.File" %>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="java.lang.StringBuffer" %>

<%@ page import="xgov.core.dao.XSQLRun" %>
<%@ page import="xgov.core.env.XConfiguration" %>

<%@ page import="org.json.simple.JSONObject" %>

<%
	response.setContentType("text/html; charset=UTF-8");
	PrintWriter pOut = response.getWriter();

	String dbType        = XConfiguration.getString("Globals.DbType").toUpperCase();     // 사용 DB 타입
	String subdir        = request.getParameter("subdir");                               // 저장 디렉토리 (시스템명)
	String atchFileId    = request.getParameter("atchid");                               // 파일 ID
	String upTable       = request.getParameter("uptable");                              // 저장 테이블 명
	String keyColumnStr  = request.getParameter("keycolumns");                           // 키 컬럼 문자 (구분자 ,)
	String [] keyColumns = keyColumnStr.split(",");                                      // 키 컬럼 배열
	String [] keyValues  = request.getParameterValues("upkey[]");                        // key 값
	String delColumns    = request.getParameter("delColumns");                           // 동시 삭제 컬럼
	String userId        = request.getParameter("userid");                               // 접속자 ID
	String userIp        = request.getRemoteAddr();                                      // 접속자 IP
	String sRootPath     = XConfiguration.getString("Globals.fileStorePath");            // 저장 경로 첫 번째 (globals.properties 의 저장 경로)
	String sqlResult     = null;                                                         // 쿼리 결과 값
	String [] resultArr  = null;                                                         // 결과 값 배열
	String returnData    = null;                                                         // 반환 값
	XSQLRun xsql         = null;                                                         // 쿼리 실행 객체
	String strSql        = null;
	String [] strSqlArr  = new String[2];

	// System.out.println("76: " + dbType);

	try {
		// 쿼리 실행 객체 생성
		xsql = new XSQLRun(subdir);

		if (atchFileId != null && !atchFileId.equals("")) {
			strSql =  "SELECT ATCH_FILE_ID" +
							",	FILE_SN" +
							",	FILE_STRE_COURS" +
							",	STRE_FILE_NM" +
							",	ORIGNL_FILE_NM" +
							",	FILE_EXTSN" +
							",	FILE_CN" +
							",	FILE_SIZE " +
					"FROM		SM_COMM_FILE_DETAIL " +
					"WHERE	ATCH_FILE_ID = '" + atchFileId + "' " +
					"AND		FILE_SN      = 1";

			// 파일 정보 조회
			// System.out.println("63: " + strSql);
			xsql.executeSQL(strSql);

			sqlResult = xsql.getResult();
			// System.out.println("67: " + sqlResult);

			resultArr = sqlResult.split("\t");

			// 파일경로 조회
			String deleteFileName = resultArr[3];
			String deleteFilePath = sRootPath + resultArr[2] + "/" + deleteFileName;
			deleteFilePath = deleteFilePath.replaceAll("\\\\", "/");
			// System.out.println("74: " + deleteFilePath);

			// 파일 삭제
			File df = new File(deleteFilePath);
			df.delete();

			// svg 파일 삭제
			if ( deleteFileName.substring( deleteFileName.lastIndexOf(".") + 1 ).toUpperCase().equals("DWG") ) {
				deleteFilePath = deleteFilePath.substring(0, deleteFilePath.lastIndexOf(".")) + ".svg";
				System.out.println(deleteFilePath);
				df = new File(deleteFilePath);
				df.delete();
			}

			// DELETE 구문 생성
			strSqlArr[0] = "DELETE SM_COMM_FILE_DETAIL WHERE ATCH_FILE_ID = '" + atchFileId + "'";

			if (keyValues != null) {
				int index = -1;
				String whereStr = "";

				// WHERE 절 생성
				for (String keyValue : keyValues) {
					if (++index == 0) {
						whereStr += " WHERE " + keyColumns[index] + " = '" + keyValue + "'";
					} else {
						whereStr += " AND " + keyColumns[index] + " = '" + keyValue + "'";
					}
				}

				// UPDATE 구문 생성
				if (dbType.equals("ORACLE") || dbType.equals("ORA")) {
					strSqlArr[1] = "UPDATE " + upTable + " SET ROW_UPDATE_DT = SYSDATE, ROW_UPDATE_EMP_NO = '" + userId + "', ROW_UPDATE_IP = '" + userIp + "', " + delColumns + whereStr;
				}
				else if (dbType.equals("MSSQL") || dbType.equals("MSS")) {
					strSqlArr[1] = "UPDATE " + upTable + " SET ROW_UPDATE_DT = GETDATE(), ROW_UPDATE_EMP_NO = '" + userId + "', ROW_UPDATE_IP = '" + userIp + "', " + delColumns + whereStr;
				}

			}
			else {
				strSqlArr[1] = "";
			}

			// 테이블 데이타 삭제 및 변경
			// System.out.println("104: " + strSqlArr[0]);
			// System.out.println("105: " + strSqlArr[1]);
			xsql.executeSQLs(strSqlArr);

			returnData = "[{" +
					"'ATCH_FILE_ID'   : '" + resultArr[0] + "'," +
					"'FILE_SN'        : "  + resultArr[1] + ","  +
					"'FILE_STRE_COURS': '" + resultArr[2] + "'," +
					"'STRE_FILE_NM'   : '" + resultArr[3] + "'," +
					"'ORIGNL_FILE_NM' : '" + resultArr[4] + "'," +
					"'FILE_EXTSN'     : '" + resultArr[5] + "'," +
					"'FILE_SIZE'      : "  + resultArr[7] +
			"}]";

			// System.out.println("118: " + returnData);

			String deleteinfo = new StringBuffer( xgov.core.util.XBase64Coder.encodeString(returnData) ).reverse().toString();

		  JSONObject jsonObj  = new JSONObject();
		  jsonObj.put("data", deleteinfo);

		  String jsonStr = jsonObj.toJSONString();

		  pOut.println(jsonStr);
		  pOut.flush();
		  pOut.close();
		}

	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		if (xsql != null) {xsql.close();}
		xsql = null;
	}
%>

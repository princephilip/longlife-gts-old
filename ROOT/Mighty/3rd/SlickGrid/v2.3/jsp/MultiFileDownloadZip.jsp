<!--
 * Object 			: MultiFileDownloadZip.jsp
 * @Description : slickgrid 파일다운로드
 * @author 			: 엑스인터넷정보 이상규
 * @since 			: 2017.02.09
 * @version 		:
 *
 * @Modification Information
 * <pre>
 *   since    	     author      Description
 *  ------------    --------    ----------------------------
 *   2017.02.09      이상규      최초생성
-->

<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.*" %>
<%@ page import="java.util.zip.ZipEntry" %>
<%@ page import="java.util.zip.ZipOutputStream" %>

<%@ page import="java.io.File" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.io.FileInputStream" %>
<%@ page import="java.io.BufferedInputStream" %>
<%@ page import="java.io.BufferedOutputStream" %>

<%@ page import="xgov.core.dao.XSQLRun" %>
<%@ page import="xgov.core.env.XConfiguration" %>

<%!
void addFile( ZipOutputStream outZip, File f, String name ) {
	BufferedInputStream in = null;

	try {
		// Add ZIP entry to output stream.
		outZip.putNextEntry( new ZipEntry( name ) ) ;

		in = new BufferedInputStream( new FileInputStream(f) );

		// Transfer bytes from the file to the ZIP file
		byte[] buf = new byte[ 4096 ];
		int len;
		while( ( len = in.read( buf ) ) > 0 ) {
			outZip.write( buf, 0, len );
		}
	} catch( IOException ex ) {
		ex.printStackTrace();
	} finally {
		// Complete the entry
		try{ outZip.closeEntry(); } catch( IOException ex ) { }
		try{ in.close(); } catch( IOException ex ) { }
	}
}
%>
<%
	out.clear();
	out.clearBuffer();

	String dbType        = XConfiguration.getString("Globals.DbType").toUpperCase();  // 사용 DB 타입
	String subdir        = request.getParameter("subdir");                            // 시스템명
	String zipFileName   = request.getParameter("filename");                          // 파일명
	String [] fileIdArr  = request.getParameterValues("fileIds");                     // 파일 ID
	String sRootPath     = XConfiguration.getString("Globals.fileStorePath");         // 저장 경로 첫 번째 (globals.properties 의 저장 경로)
	String returnData    = null;                                                      // 반환 값
	XSQLRun xsql         = new XSQLRun(subdir);                                       // 쿼리 실행 객체

	String filePath               = null;                           // 파일경로
	String fileName               = null;                           // 파일명
	HashMap<String, Integer> hMap = new HashMap<String, Integer>(); // 파일명 중복 체크 map

	try {
		// set the content type and the filename
		response.setContentType( "application/zip;" );
		response.addHeader( "Content-Disposition", "attachment; filename=" + new String(zipFileName.getBytes("KSC5601"), "8859_1") + ".zip" );

		// get a ZipOutputStream, so we can zip our files together
		ZipOutputStream outZip = new ZipOutputStream( new BufferedOutputStream(response.getOutputStream(), 4096) );

		// add some files to the zip...
		if ( fileIdArr != null ) {
			for ( int i = 0; i < fileIdArr.length; i++ ) {
				//System.out.println("fileIdArr[i]: " + fileIdArr[i]);

				xsql.executeSQL("SELECT FILE_STRE_COURS || '\\' || STRE_FILE_NM FROM SM_COMM_FILE_DETAIL WHERE ATCH_FILE_ID = '" + fileIdArr[i] + "' AND FILE_SN = 1");
				filePath = xsql.getResult();

				xsql.executeSQL("SELECT ORIGNL_FILE_NM FROM SM_COMM_FILE_DETAIL WHERE ATCH_FILE_ID = '" + fileIdArr[i] + "' AND FILE_SN = 1");
				fileName = xsql.getResult();

				if ( hMap.containsKey(fileName) ) {
					hMap.put(fileName, hMap.get(fileName) + 1);
					fileName = fileName.substring(0, fileName.lastIndexOf(".")) + "(" + hMap.get(fileName) + ")" + fileName.substring(fileName.lastIndexOf("."));
				} else {
					hMap.put(fileName, 0);
				}

				addFile( outZip, new File(sRootPath + filePath), fileName );

				//System.out.println("FILE_PATH: " + sRootPath + filePath);
				//System.out.println("ORIGNL_FILE_NM: " + fileName);
			}
		}

		// flush the stream, and close it
		outZip.flush();
		outZip.close();
	} catch(Exception e) {
		e.printStackTrace();
	} finally {
		if (xsql != null) {xsql.close();}
		xsql = null;
	}
%>
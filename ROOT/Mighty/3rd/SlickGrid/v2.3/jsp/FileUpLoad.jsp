<!--
 * Object 			: FileUpLoad.jsp
 * @Description : slickgrid 파일업로드
 * @author 			: 엑스인터넷정보 이상규
 * @since 			: 2017.01.10
 * @version 		:
 *
 * @Modification Information
 * <pre>
 *   since    	     author      Description
 *  ------------    --------    ----------------------------
 *   2017.01.10      이상규      최초생성
-->

<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.File" %>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="java.lang.StringBuffer" %>

<%@ page import="xgov.core.dao.XSQLRun" %>
<%@ page import="xgov.core.env.XConfiguration" %>
<%@ page import="xgov.core.lib.fileupload.XUploadedFileUtil" %>
<%@ page import="com.oreilly.servlet.MultipartRequest" %>
<%@ page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy" %>

<%!
	public String fileTypeConverter(String type, String name) {
		String returnType = type;

		if ( type.equals("application/vnd.ms-excel") ) {
			returnType = "xls";
		} else if ( type.equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") ) {
			returnType = "xlsx";
		} else if ( type.equals("application/vnd.ms-powerpoint") ) {
			returnType = "ppt";
		} else if ( type.equals("application/vnd.openxmlformats-officedocument.presentationml.presentation") ) {
			returnType = "pptx";
		} else if ( type.equals("application/x-msdownload") ) {
			returnType = "application";
		} else if ( type.equals("application/octet-stream") ) {
			returnType = "octet-stream";
		} else if ( type.equals("application/haansofthwp") ) {
			returnType = "application/hwp";
		} else if ( type.equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document") ) {
			returnType = "application/docx";
		}

		return returnType;
	}
%>

<%
	response.setContentType("text/html; charset=UTF-8");
	PrintWriter pOut = response.getWriter();

	String dbType        = XConfiguration.getString("Globals.DbType").toUpperCase();     // 사용 DB 타입
	String subdir        = request.getParameter("subdir");                               // 시스템명
	String fileSaveDir   = request.getParameter("fileSaveDir");                          // 저장 디렉토리
	String upTable       = request.getParameter("uptable");                              // 저장 테이블 명
	String keyColumnStr  = request.getParameter("keycolumns");                           // 키 컬럼 문자 (구분자 ,)
	String [] keyColumns = keyColumnStr.split(",");                                      // 키 컬럼 배열
	String userId        = request.getParameter("userid");                               // 접속자 ID
	String userIp        = request.getRemoteAddr();                                      // 접속자 IP
	int    sizeLimit     = 1024 * 1024 * 20;                                             // 최대 저장 크기
	String returnData    = null;                                                         // 반환 값
	XSQLRun xsql         = null;                                                         // 쿼리 실행 객체

	String svrFilePathYn = request.getParameter("svrFilePathYn");                        // 파일 저장 시 년월 폴더 추가 여부
	String convDwgToSvg  = XConfiguration.getString("Globals.fileDwgToSvg");             // dwg 파일 svg 변환 여부
	String sRootPath     = XConfiguration.getString("Globals.fileStorePath");            // 저장 경로 첫 번째 (globals.properties 의 저장 경로)
	String sSvrFilePath  = null;                                                         // 저장 경로 두 번째 (subdir + 년 + 월)
	String sRealPath     = null;                                                         // 실제 저장 경로
	String isSvgYn       = "N";                                                          // svg 파일 존재 여부

	// System.out.println("76: " + dbType);

	if ( fileSaveDir == null || fileSaveDir.equals("") ) {
		fileSaveDir = subdir;
	}

	if ( svrFilePathYn != null && svrFilePathYn.equals("N") ) {
		sSvrFilePath = fileSaveDir;

		StringBuffer sbImagePath = new StringBuffer();

		File filePath = null;

		sbImagePath.append(sSvrFilePath);
		filePath = new File(sRootPath + sbImagePath.toString());

		if ( !filePath.exists() ) {
			filePath.mkdir();
		}
	}
	else {
		sSvrFilePath = XUploadedFileUtil.getFilePath(sRootPath, fileSaveDir);
	}

	sRealPath = sRootPath + sSvrFilePath;

	try {
		// 파일 저장
		MultipartRequest multi = new MultipartRequest(request, sRealPath, sizeLimit, "UTF-8", new DefaultFileRenamePolicy());
		Enumeration      files = multi.getFileNames();

		// 쿼리 실행 객체 생성
		xsql = new XSQLRun(subdir);

		returnData = "[";

		while (files.hasMoreElements()) {
			String name          = (String)files.nextElement();                                          // 엘레멘트 name 값
			String oriName       = multi.getOriginalFileName(name);                                      // 원본 파일 명
			String fileName      = multi.getFilesystemName(name);                                        // 저장 파일 명
			String fileType      = multi.getContentType(name);                                           // 파일 확장자
			String [] rowCell    = name.split("_");                                                      // 그리드 상의 행, 셀 번호
			String [] strSqls3   = new String[3];                                                        // 저장 쿼리 배열
			String [] svgSql     = new String[1];                                                        // SVG  쿼리 배열
			String fileIdField   = multi.getParameter("idfield_"       + rowCell[1] + "_" + rowCell[2]); // 파일 ID 컬럼
			String atchFileId    = multi.getParameter("atchid_"        + rowCell[1] + "_" + rowCell[2]); // 파일 ID
			String saveNameField = multi.getParameter("savenamefield_" + rowCell[1] + "_" + rowCell[2]); // 저장파일명 컬럼
			String [] keyValues  = multi.getParameterValues("upkey_" + rowCell[1] + "_" + rowCell[2]);   // key 값
			String nextVal       = null;                                                                 // 파일 시퀀스번호

			File   f        = multi.getFile(name);
			Long   fileSize = f.length();
			String dateFunc = null;


			// 신규 파일 데이타 생성
			if (atchFileId == null || atchFileId.equals("")) {
				if (dbType.equals("ORACLE") || dbType.equals("ORA")) {
					xsql.executeSQL("SELECT SEQ_SM_COMM_FILE.NEXTVAL FROM DUAL");
					dateFunc = "SYSDATE";
				}
				else if (dbType.equals("MSSQL") || dbType.equals("MSS")) {
					xsql.executeSQL("SELECT NEXT VALUE FOR SEQ_SM_COMM_FILE");
					dateFunc = "GETDATE()";
				}

				// 시퀀스 조회
				nextVal = xsql.getResult();

				// 파일 ID 생성 (FILE_ + LPAD 15 자리 시퀀스)
				atchFileId = "FILE_" + String.format("%15s", nextVal).replace(" ", "0");


				// INSERT 구문 생성
				strSqls3[0] = "INSERT INTO SM_COMM_FILE (ATCH_FILE_ID, CREAT_DT, USE_AT, SYS_ID, USER_ID, CLIENT_IP) " +
												  							"VALUES (" +
													  								"'" + atchFileId + "'," +
													 	 								dateFunc + "," +
													 		 							"'Y'," +
													 			 						"'" + subdir + "'," +
													 				 					"'" + userId + "'," +
													 					 				"'" + userIp + "'" +
													 						 	")";
				strSqls3[1] = "INSERT INTO SM_COMM_FILE_DETAIL (ATCH_FILE_ID, FILE_SN, FILE_STRE_COURS, STRE_FILE_NM, ORIGNL_FILE_NM, FILE_EXTSN, FILE_CN, FILE_SIZE) " +
																							 "VALUES (" +
																								 	"'" + atchFileId + "'," +
																									"1," +
																									"'" + sSvrFilePath.replaceAll("\\\\", "/") + "'," +
																									"'" + fileName + "'," +
																									"'" + oriName  + "'," +
																									"'" + fileTypeConverter( fileType, oriName ) + "'," +
																									"NULL," +
																									fileSize +
																							 ")";
			}
			// 기존 파일 데이타 변경
			else {
				if (dbType.equals("ORACLE") || dbType.equals("ORA")) {
					xsql.executeSQL("SELECT FILE_STRE_COURS || '/' || STRE_FILE_NM FROM SM_COMM_FILE_DETAIL WHERE ATCH_FILE_ID = '" + atchFileId + "' AND FILE_SN = 1");
					dateFunc = "SYSDATE";
				}
				else if (dbType.equals("MSSQL") || dbType.equals("MSS")) {
					xsql.executeSQL("SELECT FILE_STRE_COURS +  '/' +  STRE_FILE_NM FROM SM_COMM_FILE_DETAIL WHERE ATCH_FILE_ID = '" + atchFileId + "' AND FILE_SN = 1");
					dateFunc = "GETDATE()";
				}

				// 파일경로 조회
				String deleteFileName = xsql.getResult();
				String deleteFilePath = sRootPath + deleteFileName;
				deleteFilePath = deleteFilePath.replaceAll("\\\\", "/");
				// System.out.println("161: " + deleteFilePath);

				// 기존 파일 삭제
				File df = new File(deleteFilePath);
				df.delete();

				// svg 파일 삭제
				if ( deleteFileName.substring( deleteFileName.lastIndexOf(".") + 1 ).toUpperCase().equals("DWG") ) {
					deleteFilePath = deleteFilePath.substring(0, deleteFilePath.lastIndexOf(".")) + ".svg";
					//System.out.println(deleteFilePath);
					df = new File(deleteFilePath);
					df.delete();
				}

				// 기존 SVG DB 정보 삭제
				svgSql[0] = "DELETE FROM SM_COMM_FILE_DETAIL WHERE ATCH_FILE_ID = '" + atchFileId + "' AND FILE_SN = 1001";
				xsql.executeSQLs(svgSql);

				// UPDATE 구문 생성
				strSqls3[0] = "UPDATE SM_COMM_FILE " +
												 "SET CREAT_DT  = " + dateFunc + "," +
														 "SYS_ID		= '" + subdir + "'," +
														 "USER_ID   = '" + userId + "'," +
														 "CLIENT_IP = '" + userIp + "' " +
											 "WHERE ATCH_FILE_ID = '" + atchFileId + "'";
				strSqls3[1] = "UPDATE SM_COMM_FILE_DETAIL " +
												 "SET FILE_STRE_COURS = '" + sSvrFilePath.replaceAll("\\\\", "/") + "'," +
													 	 "STRE_FILE_NM    = '" + fileName + "'," +
														 "ORIGNL_FILE_NM  = '" + oriName  + "'," +
														 "FILE_EXTSN      = '" + fileTypeConverter( fileType, oriName ) + "'," +
														 "FILE_SIZE       = "  + fileSize + " "  +
											 "WHERE ATCH_FILE_ID = '" + atchFileId + "' " +
												 "AND FILE_SN      = 1";
			}



			if (keyValues != null) {
				int index = -1;
				String updateFieldStr = "";
				String whereStr       = "";

				// 추가 UPDATE 구문 생성
				if ( saveNameField != null ) {
					updateFieldStr += ", " + saveNameField + "='" + fileName + "'";
				}

				// WHERE 절 생성
				for (String keyValue : keyValues) {
					if (++index == 0) {
						whereStr += " WHERE " + keyColumns[index] + " = '" + keyValue + "'";
					} else {
						whereStr += " AND " + keyColumns[index] + " = '" + keyValue + "'";
					}
				}

				// UPDATE 구문 생성
				strSqls3[2] = "UPDATE " + upTable + " SET " + fileIdField + " = '" + atchFileId + "'" + updateFieldStr + whereStr;
			}
			else {
				strSqls3[2] = "";
			}

			// SM_COMM_FILE, SM_COMM_FILE_DETAIL 테이블에 데이타 생성 및 그리드 테이블 데이타 변경
			// System.out.println("192: " + strSqls3[0]);
			// System.out.println("193: " + strSqls3[1]);
			// System.out.println("194: " + strSqls3[2]);
			xsql.executeSQLs(strSqls3);


 			// System.out.println("convDwgToSvg: " + convDwgToSvg);

			// dwg 파일 svg 변환
			if ( convDwgToSvg != null && convDwgToSvg.equals("true") ) {
				if ( oriName.substring( oriName.lastIndexOf(".") + 1 ).toUpperCase().equals("DWG") ) {
					String dwgFilePath = sRootPath + sSvrFilePath.replaceAll("\\\\", "/") + "/" + fileName;
					Process proc = Runtime.getRuntime().exec("AcmeCADConverter /r /ad /f 101 \"" + dwgFilePath + "\"");
					proc.waitFor();

					// SVG DB 정보 생성
					svgSql[0] = "INSERT INTO SM_COMM_FILE_DETAIL (ATCH_FILE_ID, FILE_SN, FILE_STRE_COURS, STRE_FILE_NM, ORIGNL_FILE_NM, FILE_EXTSN) " +
																							 "VALUES (" +
																								 	"'" + atchFileId + "'," +
																									"1001," +
																									"'" + sSvrFilePath.replaceAll("\\\\", "/") + "'," +
																									"'" + fileName.substring(0, fileName.lastIndexOf(".")) + ".svg'," +
																									"'" + oriName.substring(0, oriName.lastIndexOf(".")) + ".svg'," +
																									"'svg'" +
																							 ")";
					xsql.executeSQLs(svgSql);

					isSvgYn = "Y";
					// System.out.println("AcmeCADConverter /r /f 101 \"" + dwgFilePath + "\"");
				}
			}



			// 반환 값 생성
			returnData += "{" +
												"'row'            : "  + rowCell[1]                             + ","  +
												"'cell'           : "  + rowCell[2]                             + ","  +
												"'SYS_ID'         : '" + subdir                                 + "'," +
												"'FILE_STRE_COURS': '" + sSvrFilePath.replaceAll("\\\\", "/")   + "'," +
												"'STRE_FILE_NM'   : '" + fileName                               + "'," +
												"'ORIGNL_FILE_NM' : '" + oriName                                + "'," +
												"'FILE_EXTSN'     : '" + fileTypeConverter( fileType, oriName ) + "'," +
												"'FILE_SIZE'      : "  + fileSize                               + ","  +
												"'REAL_PATH'      : '" + sRealPath.replaceAll("\\\\", "/")      + "'," +
												"'ATCH_FILE_ID'   : '" + atchFileId                             + "'," +
												"'FILE_ID_FIELD'  : '" + fileIdField                            + "'," +
												"'SVG_YN'         : '" + isSvgYn                                + "'";

			if ( files.hasMoreElements() ) {
				returnData += "},";
			} else {
				returnData += "}]";
			}
		}

		// System.out.println("213: " + returnData);

		String uploadinfo = new StringBuffer( xgov.core.util.XBase64Coder.encodeString(returnData) ).reverse().toString();

		pOut.println(uploadinfo);
		pOut.flush();
		pOut.close();

	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		if (xsql != null) {xsql.close();}
		xsql = null;
	}
%>

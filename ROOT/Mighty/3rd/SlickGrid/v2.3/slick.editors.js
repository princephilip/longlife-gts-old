/***
 * Contains basic SlickGrid editors.
 * @module Editors
 * @namespace Slick
 */

(function ($) {
	// register namespace
	$.extend(true, window, {
		"Slick": {
		"Editors": {
			"Text"				: TextEditor,
			"Integer"			: IntegerEditor,
			"CharDate"			: CharDateEditor,
			"Date"				: DateEditor,
			"HhMm"				: HhMmEditor, //시분 편집기 추가(20161207 이상규)
			"MmDd"				: MmDdEditor, //월일 편집기 추가(20180702 KYY)
			"YesNoSelect"		: YesNoSelectEditor,
			"Checkbox"			: CheckboxEditor2,
			"PercentComplete"	: PercentCompleteEditor,
			"LongText"			: LongTextEditor,
			"Combo"				: ComboBoxEditor /* ComboBox bind (2015.06.09 이상규) */
			}
		}
	});

  function TextEditor(args) {
	var $input;
	var defaultValue;
	var oldValue; // oldValue field 추가 (2015.08.20 이상규)
	var scope = this;
		var num = 1;
	this.init = function () {
		var editorWidth = args.position.width - 13 - (args.column.button ? 20: 0);
	  $input = $("<INPUT type=text class='editor-text' style='width: " + editorWidth + "px;'/>")
		  .appendTo(args.container)
		  .bind("keydown.nav", function (e) {
			if (e.keyCode === $.ui.keyCode.LEFT || e.keyCode === $.ui.keyCode.RIGHT) {
			  e.stopImmediatePropagation();
			}
		  })
		  /* doubleClick event 추가 (2015.10.21 이상규) */
		  .bind("dblclick", function(e) {
			var cell = args.grid.getCellFromEvent(e);
			e = e || new Slick.EventData();
			return args.grid.onDblClick.notify( { row: cell.row, cell: cell.cell, grid: args.grid } , e, args.grid );
			})
		  .focus()
		  .select();
	};

	this.destroy = function () {
	  $input.remove();
	};

	this.focus = function () {
	  $input.focus();
	};

	this.getValue = function () {
	  return $input.val();
	};

		/* oldValue get 메소드 추가 (2015.08.20 이상규) */
		this.getOldValue = function() {
			return oldValue;
		};

	this.setValue = function (val) {
	  $input.val(val);
	};

	this.loadValue = function (item) {
	  defaultValue = item[args.column.field] || "";
	  $input.val(defaultValue);
	  $input[0].defaultValue = defaultValue;
	  $input.select();
	};

	this.serializeValue = function () {
	  return $input.val();
	};

	this.applyValue = function (item, state) {
		oldValue = item[args.column.field];//oldValue 셋팅 추가 (2015.08.20 이상규)
	  item[args.column.field] = state;
	};

	this.isValueChanged = function () {
	  return (!($input.val() == "" && defaultValue == null)) && ($input.val() != defaultValue);
	};

	this.validate = function () {
	  if (args.column.validator) {
		var validationResults = args.column.validator($input.val());
		if (validationResults && !validationResults.valid) {
		  return validationResults;
		}
	  }

	  return {
		valid: true,
		msg: null
	  };
	};

	this.init();
  }

  function IntegerEditor(args) {
	var $input;
	var defaultValue;
	var oldValue; // oldValue field 추가 (2015.08.20 이상규)
	var scope = this;
	var dataType = "number";

	this.init = function () {
		var editorWidth = args.position.width - 13;
		dataType = args.column.dataType;

	  $input = $("<INPUT type=text class='editor-integer' style='width: " + editorWidth + "px;'/>");

	  $input.bind("keydown.nav", function (e) {
		if (e.keyCode === $.ui.keyCode.LEFT || e.keyCode === $.ui.keyCode.RIGHT) {
		  e.stopImmediatePropagation();
		}
	  })
	  /* doubleClick event 추가 (2015.10.21 이상규) */
	  .bind("dblclick", function(e) {
		var cell = args.grid.getCellFromEvent(e);
		e = e || new Slick.EventData();
		return args.grid.onDblClick.notify( { row: cell.row, cell: cell.cell, grid: args.grid } , e, args.grid );
		});

	  $input.appendTo(args.container);
	  $input.focus().select();
	};

	this.destroy = function () {
	  $input.remove();
	};

	this.focus = function () {
	  $input.focus();
	};

		/* oldValue get 메소드 추가 (2015.08.20 이상규) */
		this.getOldValue = function() {
			return oldValue;
		};

		this.setValue = function(val) {
			$input.val(val);
		};

	this.loadValue = function (item) {
	  defaultValue = item[args.column.field];
	  $input.val(defaultValue);
	  $input[0].defaultValue = defaultValue;
	  $input.select();
	};

	this.serializeValue = function () {
		/* parseInt 삭제 (2015.08.17 이상규) */
	  return $input.val() || 0;
	};

	this.applyValue = function (item, state) {
		oldValue = item[args.column.field];//oldValue 셋팅 추가 (2015.08.20 이상규)
		state = _X.FormatCommaPoint2( state, parseInt( dataType.replace("numberf", ""), 10 ) );

	  item[args.column.field] = state.split( "," ).join( "" );
	};

	this.isValueChanged = function () {
	  return (!($input.val() == "" && defaultValue == null)) && ($input.val() != defaultValue);
	};

	this.validate = function () {
	  if (isNaN($input.val())) {
		return {
		  valid: false,
		  msg: "Please enter a valid integer"
		};
	  }

			if (args.column.validator) {
		var validationResults = args.column.validator($input.val());
		if (validationResults && !validationResults.valid) {
		  return validationResults;
		}
	  }

	  return {
		valid: true,
		msg: null
	  };
	};

	this.init();
  }


function CharDateEditor(args) {
	var $input;
	var defaultValue;
	var oldValue; // oldValue field 추가 (2015.08.20 이상규)
	var scope = this;
	var calendarOpen = false;

	this.init = function () {
		$input = $("<INPUT type=text class='editor-text' />");
		$input.appendTo(args.container);
		$input.focus().select();
		$input.datepicker({
			changeMonth: true,
			changeYear: true,
			dayNames: ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'],
			dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
			monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'],
			monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
			nextText: '다음 달',
			prevText: '이전 달',
			showButtonPanel: true,
			currentText: '오늘 날짜',
			closeText: '닫기',
			dateFormat: "yy-mm-dd",
			showMonthAfterYear: true,
			yearSuffix: ' 년 ',
			monthSuffix: ' 월',
			showOtherMonths: true,
			selectOtherMonths: true,
			showOn: "button",
			buttonImageOnly: true,
			yearRange: 'c-50:c+50', //년도범위 추가(2017.09.18 KYY)
			/* 이미지 경로 수정 (2015.08.18 이상규) */
			buttonImage: "/Mighty/3rd/SlickGrid/v2.3/images/calendar.gif",
			beforeShow: function () {
				calendarOpen = true
			},
			onClose: function () {
				calendarOpen = false
			}
		});
		$input.width($input.width() - 18);
		/* doubleClick event 추가 (2015.10.21 이상규) */
		$input.bind("dblclick", function(e) {
			var cell = args.grid.getCellFromEvent(e);
			e = e || new Slick.EventData();
			return args.grid.onDblClick.notify( { row: cell.row, cell: cell.cell, grid: args.grid } , e, args.grid );
		});
	};

	this.destroy = function () {
		$.datepicker.dpDiv.stop(true, true);
		$input.datepicker("hide");
		$input.datepicker("destroy");
		$input.remove();
	};

	this.show = function () {
		if (calendarOpen) {
			$.datepicker.dpDiv.stop(true, true).show();
		}
	};

	this.hide = function () {
		if (calendarOpen) {
			$.datepicker.dpDiv.stop(true, true).hide();
		}
	};

	this.position = function (position) {
		if (!calendarOpen) {
			return;
		}
		$.datepicker.dpDiv
			.css("top", position.top + 30)
			.css("left", position.left);
	};

	this.focus = function () {
		$input.focus();
	};

		/* oldValue get 메소드 추가 (2015.08.20 이상규) */
		this.getOldValue = function() {
			return oldValue;
		};

		this.setValue = function (val) {
		$input.val(val);
	};

	this.loadValue = function (item) {
		defaultValue = item[args.column.field];
		$input.val( _X.ToString(defaultValue, "yyyy-MM-dd") );
		$input[0].defaultValue = defaultValue;
		$input.select();
	};

	this.serializeValue = function () {
		return _X.ToString($input.val(), "yyyyMMdd");
	};

	this.applyValue = function (item, state) {
		oldValue = item[args.column.field];//oldValue 셋팅 추가 (2015.08.20 이상규)
		item[args.column.field] = _X.ToString(state, "yyyyMMdd");
	};

	this.isValueChanged = function () {
		return (!($input.val() == "" && defaultValue == null)) && (_X.ToString($input.val(), "yyyyMMdd") != defaultValue);
	};

	this.validate = function () {
		if (args.column.validator) {
			var validationResults = args.column.validator( _X.ToString($input.val(), "yyyyMMdd") );

			if (validationResults && !validationResults.valid) {
				return validationResults;
			}
		}

		return {
			valid: true,
			msg: null
		};
	};

	this.init();
  }


  function DateEditor(args) {
	var $input;
	var defaultValue;
	var oldValue; // oldValue field 추가 (2015.08.20 이상규)
	var scope = this;
	var calendarOpen = false;

	this.init = function () {
	  $input = $("<INPUT type=text class='editor-text' />");
	  $input.appendTo(args.container);
	  $input.focus().select();
	  $input.datepicker({
		changeMonth: true,
			changeYear: true,
			dayNames: ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'],
			dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
			monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'],
			monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
			nextText: '다음 달',
			prevText: '이전 달',
			showButtonPanel: true,
			currentText: '오늘 날짜',
			closeText: '닫기',
			dateFormat: "yy-mm-dd",
			showMonthAfterYear: true,
			yearSuffix: ' 년 ',
			monthSuffix: ' 월',
			showOtherMonths: true,
			selectOtherMonths: true,
		showOn: "button",
		buttonImageOnly: true,
		yearRange: 'c-50:c+50', //년도범위 추가(2017.09.18 KYY)
		/* 이미지 경로 수정 (2015.08.18 이상규) */
		buttonImage: "/Mighty/3rd/SlickGrid/v2.3/images/calendar.gif",
		beforeShow: function () {
		  calendarOpen = true
		},
		onClose: function () {
		  calendarOpen = false
		}
	  });
	  $input.width($input.width() - 18);
	  /* doubleClick event 추가 (2015.10.21 이상규) */
	  $input.bind("dblclick", function(e) {
		var cell = args.grid.getCellFromEvent(e);
		e = e || new Slick.EventData();
		return args.grid.onDblClick.notify( { row: cell.row, cell: cell.cell, grid: args.grid } , e, args.grid );
		});
	};

	this.destroy = function () {
	  $.datepicker.dpDiv.stop(true, true);
	  $input.datepicker("hide");
	  $input.datepicker("destroy");
	  $input.remove();
	};

	this.show = function () {
	  if (calendarOpen) {
		$.datepicker.dpDiv.stop(true, true).show();
	  }
	};

	this.hide = function () {
	  if (calendarOpen) {
		$.datepicker.dpDiv.stop(true, true).hide();
	  }
	};

	this.position = function (position) {
	  if (!calendarOpen) {
		return;
	  }
	  $.datepicker.dpDiv
		  .css("top", position.top + 30)
		  .css("left", position.left);
	};

	this.focus = function () {
	  $input.focus();
	};

		/* oldValue get 메소드 추가 (2015.08.20 이상규) */
		this.getOldValue = function() {
			return oldValue;
		};

		this.setValue = function (val) {
	  $input.val(val);
	};

	this.loadValue = function (item) {
	  defaultValue = item[args.column.field];
	  $input.val(defaultValue);
	  $input[0].defaultValue = defaultValue;
	  $input.select();
	};

	this.serializeValue = function () {
		/* 직접 입력시 mask 처리 (2015.08.28 이상규) */
		if ( args.column.dataType === "date" ) {
			return _X.ToString($input.val(), "yyyy-MM-dd");
		}
		else if ( args.column.dataType === "datemonth" ) {
		return _X.ToString($input.val() + '01', "yyyyMM");
	  }
		else if ( args.column.dataType == "datatime" ) {
			return _X.ToString($input.val(), "yyyy-MM-dd HH:mm:ss");
		}
	};

	this.applyValue = function (item, state) {
		oldValue = item[args.column.field];//oldValue 셋팅 추가 (2015.08.20 이상규)
	  item[args.column.field] = state;
	};

	this.isValueChanged = function () {
	  return (!($input.val() == "" && defaultValue == null)) && ($input.val() != defaultValue);
	};

	this.validate = function () {
		if (args.column.validator) {
		var validationResults = args.column.validator( _X.ToString($input.val(), "yyyy-MM-dd") );
		if (validationResults && !validationResults.valid) {
		  return validationResults;
		}
	  }

	  return {
		valid: true,
		msg: null
	  };
	};

	this.init();
  }

  //시분 편집기 추가(20161207 이상규)
  function HhMmEditor(args) {
	var $input;
	var defaultValue;
	var oldValue;
	var scope = this;
	var calendarOpen = false;

	this.init = function () {
	  $input = $("<INPUT type=text class='editor-text style-text-center' />");
	  $input.inputmask("hh:mm", {placeholder: "00:00"})
				.bind("dblclick", function(e) {
						var cell = args.grid.getCellFromEvent(e);
						e = e || new Slick.EventData();
						return args.grid.onDblClick.notify( { row: cell.row, cell: cell.cell, grid: args.grid } , e, args.grid );
					})
				.appendTo(args.container)
				.focus()
				.select();
	};

	this.destroy = function () {
	  $input.remove();
	};

	this.focus = function () {
	  $input.focus();
	};

	this.getValue = function () {
	  return $input.val().replace(/:/, "");
	};

		/* oldValue get 메소드 추가 (2015.08.20 이상규) */
		this.getOldValue = function() {
			return oldValue;
		};

	this.setValue = function (val) {
	  $input.val(val);
	};

	this.loadValue = function (item) {
	  defaultValue = item[args.column.field] || "";
	  $input.val(defaultValue);
	  $input[0].defaultValue = defaultValue;
	  $input.select();
	};

	this.serializeValue = function () {
	  return $input.val().replace(/:/, "");
	};

	this.applyValue = function (item, state) {
		oldValue = item[args.column.field];
	  item[args.column.field] = state;
	};

	this.isValueChanged = function () {
	  return (!($input.val() == "" && defaultValue == null)) && ($input.val().replace(/:/, "") != defaultValue);
	};

	this.validate = function () {
	  if (args.column.validator) {
		var validationResults = args.column.validator($input.val());
		if (validationResults && !validationResults.valid) {
		  return validationResults;
		}
	  }

	  return {
		valid: true,
		msg: null
	  };
	};

	this.init();
  }

  //월일 편집기 추가(20180702 KYY)
  function MmDdEditor(args) {
	var $input;
	var defaultValue;
	var oldValue;
	var scope = this;
	var calendarOpen = false;

	this.init = function () {
	  $input = $("<INPUT type=text class='editor-text style-text-center' />");
	  $input.appendTo(args.container)
			.focus()
			.select();				
	};

	this.destroy = function () {
	  $input.remove();
	};

	this.focus = function () {
	  $input.focus();
	};

	this.getValue = function () {
	  return $input.val().replace(/-/, "");
	};

	this.getOldValue = function() {
		return oldValue;
	};

	this.setValue = function (val) {
	  $input.val(val);
	};

	this.loadValue = function (item) {
	  defaultValue = item[args.column.field] || "";
	  $input.val(defaultValue);
	  $input[0].defaultValue = defaultValue;
	  $input.select();
	};

	this.serializeValue = function () {
	  return $input.val().replace(/-/, "");
	};

	this.applyValue = function (item, state) {
		oldValue = item[args.column.field];
		item[args.column.field] = state;
	};

	this.isValueChanged = function () {
	  return (!($input.val() == "" && defaultValue == null)) && ($input.val().replace(/-/, "") != defaultValue);
	};

	this.validate = function () {
	  if (args.column.validator) {
		var validationResults = args.column.validator($input.val());
		if (validationResults && !validationResults.valid) {
		  return validationResults;
		}
	  }

	  return {
		valid: true,
		msg: null
	  };
	};

	this.init();
  }

  function YesNoSelectEditor(args) {
	var $select;
	var defaultValue;
	var oldValue; // oldValue field 추가 (2015.08.20 이상규)
	var scope = this;

	this.init = function () {
	  $select = $("<SELECT tabIndex='0' class='editor-yesno'><OPTION value='yes'>Yes</OPTION><OPTION value='no'>No</OPTION></SELECT>");
	  $select.appendTo(args.container);
	  $select.focus();
	};

	this.destroy = function () {
	  $select.remove();
	};

	this.focus = function () {
	  $select.focus();
	};

		/* oldValue get 메소드 추가 (2015.08.20 이상규) */
		this.getOldValue = function() {
			return oldValue;
		};

	this.loadValue = function (item) {
	  $select.val((defaultValue = item[args.column.field]) ? "yes" : "no");
	  $select.select();
	};

	this.serializeValue = function () {
	  return ($select.val() == "yes");
	};

	this.applyValue = function (item, state) {
		oldValue = item[args.column.field];//oldValue 셋팅 추가 (2015.08.20 이상규)
	  item[args.column.field] = state;
	};

	this.isValueChanged = function () {
	  return ($select.val() != defaultValue);
	};

	this.validate = function () {
	  return {
		valid: true,
		msg: null
	  };
	};

	this.init();
  }

  function CheckboxEditor(args) {
	var $select;
	var defaultValue;
	var oldValue; // oldValue field 추가 (2015.08.20 이상규)
	var scope = this;

	this.init = function () {
	  $select = $("<INPUT type=checkbox value='true' class='editor-checkbox' hideFocus>");
	  $select.appendTo(args.container);
	  $select.focus();
	};

	this.destroy = function () {
	  $select.remove();
	};

	this.focus = function () {
	  $select.focus();
	};

		/* oldValue get 메소드 추가 (2015.08.20 이상규) */
		this.getOldValue = function() {
			return oldValue;
		};

	this.loadValue = function (item) {
	  defaultValue = !!item[args.column.field];
	  if (defaultValue) {
		$select.prop('checked', true);
	  } else {
		$select.prop('checked', false);
	  }
	};

	this.serializeValue = function () {
	  return $select.prop('checked');
	};

	this.applyValue = function (item, state) {
		oldValue = item[args.column.field];//oldValue 셋팅 추가 (2015.08.20 이상규)
	  item[args.column.field] = state;
	};

	this.isValueChanged = function () {
	  return (this.serializeValue() !== defaultValue);
	};

	this.validate = function () {
	  return {
		valid: true,
		msg: null
	  };
	};

	this.init();
  }

	/*=== Checkbox Editor (2015.08.18 이상규) ===*/
	function CheckboxEditor2(args) {
	var $select;
	var defaultValue;
	var oldValue; // oldValue field 추가 (2015.08.20 이상규)
	var scope = this;

	this.init = function () {
		$select = $("<INPUT type=checkbox value='true' class='editor-checkbox' hideFocus>");
	  $select.appendTo(args.container);

	  $select.click(function (e) {
		var container = args.grid.getContainerNode();
		var cell      = args.grid.getCellFromEvent(e);
		var value     = this.checked ? "Y" : "N";
		var oldValue  = this.checked ? "N" : "Y";
		if ( typeof xe_GridDataChanged !== "undefined" ) {
					xe_GridDataChanged(container, cell.row + 1, args.column.field, value, oldValue);
				}
	  });

	  $select.focus();
	};

	this.destroy = function () {
	  $select.remove();
	};

	this.focus = function () {
	  $select.focus();
	};

		/* oldValue get 메소드 추가 (2015.08.20 이상규) */
		this.getOldValue = function() {
			return oldValue;
		};

	this.loadValue = function (item) {
	  defaultValue = item[args.column.field];
	  if (defaultValue === "Y") {
		$select.prop('checked', true);
	  } else {
		$select.prop('checked', false);
	  }
	};

	this.serializeValue = function () {
	  return $select.prop('checked') ? "Y" : "N";
	};

	this.applyValue = function (item, state) {
		oldValue = item[args.column.field];//oldValue 셋팅 추가 (2015.08.20 이상규)
	  item[args.column.field] = state;
	};

	this.isValueChanged = function () {
	  return (this.serializeValue() !== defaultValue);
	};

	this.validate = function () {
	  return {
		valid: true,
		msg: null
	  };
	};

	this.init();
  }

  function PercentCompleteEditor(args) {
	var $input, $picker;
	var defaultValue;
	var oldValue; // oldValue field 추가 (2015.08.20 이상규)
	var scope = this;

	this.init = function () {
	  $input = $("<INPUT type=text class='editor-percentcomplete' />");
	  $input.width($(args.container).innerWidth() - 25);
	  $input.appendTo(args.container);

	  $picker = $("<div class='editor-percentcomplete-picker' />").appendTo(args.container);
	  $picker.append("<div class='editor-percentcomplete-helper'><div class='editor-percentcomplete-wrapper'><div class='editor-percentcomplete-slider' /><div class='editor-percentcomplete-buttons' /></div></div>");

	  $picker.find(".editor-percentcomplete-buttons").append("<button val=0>Not started</button><br/><button val=50>In Progress</button><br/><button val=100>Complete</button>");

	  $input.focus().select();

	  $picker.find(".editor-percentcomplete-slider").slider({
		orientation: "vertical",
		range: "min",
		value: defaultValue,
		slide: function (event, ui) {
		  $input.val(ui.value)
		}
	  });

	  $picker.find(".editor-percentcomplete-buttons button").bind("click", function (e) {
		$input.val($(this).attr("val"));
		$picker.find(".editor-percentcomplete-slider").slider("value", $(this).attr("val"));
	  })
	};

	this.destroy = function () {
	  $input.remove();
	  $picker.remove();
	};

	this.focus = function () {
	  $input.focus();
	};

		/* oldValue get 메소드 추가 (2015.08.20 이상규) */
		this.getOldValue = function() {
			return oldValue;
		};

	this.loadValue = function (item) {
	  $input.val(defaultValue = item[args.column.field]);
	  $input.select();
	};

	this.serializeValue = function () {
	  return parseInt($input.val(), 10) || 0;
	};

	this.applyValue = function (item, state) {
		oldValue = item[args.column.field];//oldValue 셋팅 추가 (2015.08.20 이상규)
	  item[args.column.field] = state;
	};

	this.isValueChanged = function () {
	  return (!($input.val() == "" && defaultValue == null)) && ((parseInt($input.val(), 10) || 0) != defaultValue);
	};

	this.validate = function () {
	  if (isNaN(parseInt($input.val(), 10))) {
		return {
		  valid: false,
		  msg: "Please enter a valid positive number"
		};
	  }

	  return {
		valid: true,
		msg: null
	  };
	};

	this.init();
  }

  /*
   * An example of a "detached" editor.
   * The UI is added onto document BODY and .position(), .show() and .hide() are implemented.
   * KeyDown events are also handled to provide handling for Tab, Shift-Tab, Esc and Ctrl-Enter.
   */
  function LongTextEditor(args) {
	var $input, $wrapper;
	var defaultValue;
	var oldValue; // oldValue field 추가 (2015.08.20 이상규)
	var scope = this;

	this.init = function () {
	  var $container = $("body");

	  $wrapper = $("<DIV style='z-index:10000;position:absolute;background:white;padding:5px;border:3px solid gray; -moz-border-radius:10px; border-radius:10px;'/>")
		  .appendTo($container);

	  $input = $("<TEXTAREA hidefocus rows=5 style='backround:white;width:250px;height:80px;border:0;outline:0'>")
		  .appendTo($wrapper);

	  $("<DIV style='text-align:right'><BUTTON>Save</BUTTON><BUTTON>Cancel</BUTTON></DIV>")
		  .appendTo($wrapper);

	  $wrapper.find("button:first").bind("click", this.save);
	  $wrapper.find("button:last").bind("click", this.cancel);
	  $input.bind("keydown", this.handleKeyDown);

	  scope.position(args.position);
	  $input.focus().select();
	};

	this.handleKeyDown = function (e) {
	  if (e.which == $.ui.keyCode.ENTER && e.ctrlKey) {
		scope.save();
	  } else if (e.which == $.ui.keyCode.ESCAPE) {
		e.preventDefault();
		scope.cancel();
	  } else if (e.which == $.ui.keyCode.TAB && e.shiftKey) {
		e.preventDefault();
		args.grid.navigatePrev();
	  } else if (e.which == $.ui.keyCode.TAB) {
		e.preventDefault();
		args.grid.navigateNext();
	  }
	};

	this.save = function () {
	  args.commitChanges();
	};

	this.cancel = function () {
	  $input.val(defaultValue);
	  args.cancelChanges();
	};

	this.hide = function () {
	  $wrapper.hide();
	};

	this.show = function () {
	  $wrapper.show();
	};

	this.position = function (position) {
	  $wrapper
		  .css("top", position.top - 5)
		  .css("left", position.left - 5)
	};

	this.destroy = function () {
	  $wrapper.remove();
	};

	this.focus = function () {
	  $input.focus();
	};

		/* oldValue get 메소드 추가 (2015.08.20 이상규) */
		this.getOldValue = function() {
			return oldValue;
		};

	this.loadValue = function (item) {
	  $input.val(defaultValue = item[args.column.field]);
	  $input.select();
	};

	this.serializeValue = function () {
	  return $input.val();
	};

	this.applyValue = function (item, state) {
		oldValue = item[args.column.field];//oldValue 셋팅 추가 (2015.08.20 이상규)
	  item[args.column.field] = state;
	};

	this.isValueChanged = function () {
	  return (!($input.val() == "" && defaultValue == null)) && ($input.val() != defaultValue);
	};

	this.validate = function () {
	  return {
		valid: true,
		msg: null
	  };
	};

	this.init();
  }

  /*=== ComboBox Editor (2015.06.09 이상규) ===*/
  function ComboBoxEditor(args) {
	var $select;
	var defaultValue;
	var oldValue; // oldValue field 추가 (2015.08.20 이상규)
	var scope = this;

	this.init = function () {
		var option_str = "",
				autoComboOptions,
				v,
				d;

	  if ( args.column.getAutoComboOptions ) {
			autoComboOptions = args.column.getAutoComboOptions( args.item );
		}

		// autoComboOption (2015.12.08 이상규)
		if ( autoComboOptions && autoComboOptions.values.length == autoComboOptions.labels.length ) {
			for ( var i = 0, ii = autoComboOptions.values.length; i < ii; i++ ) {
				v = autoComboOptions.values[i];
			d = autoComboOptions.labels[i];

			option_str += "<OPTION value='" + v + "'>" + d + "</OPTION>";
			}
		}
	  else if (args.column.comboData !== undefined && args.column.comboData.values.length == args.column.comboData.labels.length) {
		  for ( var i = 0, ii = args.column.comboData.values.length; i < ii; i++ ) {
			v = args.column.comboData.values[i];
			d = args.column.comboData.labels[i];
			c = args.column.comboData.dropDownColor[i];

			option_str += "<OPTION value='" + v + "' style='background-color:" + c + ";'>" + d + "</OPTION>";
		  }
		}

		var editorWidth = args.position.width - 9;
	  $select = $("<SELECT tabIndex='0' class='editor-select' style='width: " + editorWidth + "px;'>" + option_str + "</SELECT>");
	  $select.appendTo(args.container);
	  $select.focus();
	};

	this.destroy = function () {
	  $select.remove();
	};

	this.focus = function () {
	  $select.focus();
	};

		/* oldValue get 메소드 추가 (2015.08.20 이상규) */
		this.getOldValue = function() {
			return oldValue;
		};

		this.setValue = function(value) {
			return $select.val(value);
		};

	this.loadValue = function (item) {
	  $select.val((defaultValue = item[args.column.field]) ? defaultValue : "");
	  $select.select();
	};

	this.serializeValue = function () {
	  return $select.val();
	};

	this.applyValue = function (item, state) {
		oldValue = item[args.column.field];//oldValue 셋팅 추가 (2015.08.20 이상규)
	  item[args.column.field] = state;
	};

	this.isValueChanged = function () {
	  return ($select.val() != null && $select.val() != defaultValue);
	};

	this.validate = function () {
		if (args.column.validator) {
		var validationResults = args.column.validator($select.val());
		if (validationResults && !validationResults.valid) {
		  return validationResults;
		}
	  }

	  return {
		valid: true,
		msg: null
	  };
	};

	this.init();
  }
})(jQuery);

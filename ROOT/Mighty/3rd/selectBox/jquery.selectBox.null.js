;(function ($) {
    var SelectBox = this.SelectBox = function (select, options) {
    };

    SelectBox.prototype.version = '1.2.0';
    SelectBox.prototype.init = function (options) {
    };
    SelectBox.prototype.getOptions = function (type) {
    };
    SelectBox.prototype.getLabelClass = function () {
    };
    SelectBox.prototype.getLabelText = function () {
    };
    SelectBox.prototype.setLabel = function () {
    };
    SelectBox.prototype.destroy = function () {
    };
    SelectBox.prototype.refresh = function () {
    };
    SelectBox.prototype.showMenu = function () {
    };
    SelectBox.prototype.hideMenus = function () {
    };
    SelectBox.prototype.selectOption = function (li, event) {
    };
    SelectBox.prototype.addHover = function (li) {
    };
    SelectBox.prototype.getSelectElement = function () {
    };
    SelectBox.prototype.removeHover = function (li) {
    };
    SelectBox.prototype.keepOptionInView = function (li, center) {
    };
    SelectBox.prototype.handleKeyDown = function (event) {
    };
    SelectBox.prototype.handleKeyPress = function (event) {
    };
    SelectBox.prototype.enable = function () {
    };
    SelectBox.prototype.disable = function () {
    };
    SelectBox.prototype.setValue = function (value) {
    };
    SelectBox.prototype.setOptions = function (options) {
    };
    SelectBox.prototype.disableSelection = function (selector) {
     };
    SelectBox.prototype.generateOptions = function (self, options) {
    };
    $.extend($.fn, {
        selectBox: function (method, options) {
        }
    });
})(jQuery);

<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@page import="java.util.*" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>simple</title>
<script type="text/javascript" src="./rexscript/getscript.jsp?f=rexpert.min"></script>
<script type="text/javascript" src="./rexscript/getscript.jsp?f=rexpert.properties"></script>
<script type="text/javaScript">
function fnopen() {
	var oReport = GetfnParamSet("0");

	//oReport.rptname = "testserver";

	<%
		Enumeration params = request.getParameterNames(); 
	
		while (params.hasMoreElements()) { 
			String name = (String)params.nextElement(); 
			String value = request.getParameter(name); 
			
			if( name.equals("rex_rptname") ){
	
	%>
			oReport.rptname = "<%=value%>";
	//alert("rebname:" + "<%=value%>");
	<%
			} 
			if(  name.equals("rex_db") ){
	%>
			oReport.connectname= "<%=value%>";
	//alert("db:" + "<%=value%>");

	<%
			}
	%>

		oReport.param("<%=name%>").value = "<%=value%>";

	<%		
		}	
	%>

	//oReport.param("t1").value = "bbb";
	//oReport.param("t2").value = "ccc";

	oReport.iframe(ifrmRexPreview1);
	//oReport.open();

	//rptname=test&t1=bbb&t2=ccc
}
</script>
</head>
<body onload="fnopen()" leftmargin=0 topmargin=0 scroll=no>
	<iframe id="ifrmRexPreview1" name="ifrmRexPreview1" src="" width="100%" height="100%"></iframe>
</body>
</html>
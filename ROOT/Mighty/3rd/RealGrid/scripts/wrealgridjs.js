﻿RealGridJS.LocalDataProvider.prototype.loadData = function (options, onSuccess, onFailed, onComplete, onXhr) {
    var provider = this;
    var xhrcallback = undefined;
    if (onXhr) { 
        xhrcallback = function () {
            if (onXhr)
                return onXhr();
        }
    }

    $.ajax({
        type: options.method ? options.method : "GET",
        url: options.url,
        dataType: "text",
        data: options.params,
        success: function (data) {
            if (options.filters) {
                provider.setFilters(provider.filters);
            };

            if (options.type == "json") {
                provider.fillJsonData(data, options);
            } else if (options.type == "xml") {
                provider.fillXmlData(data, options);
            } else if (options.type == "csv") {
                provider.fillCsvData(data, options);
            };

            if (onSuccess)
                onSuccess(provider);
        },
        error: function (xhr, status, error) {
            if (onFailed)
                onFailed(provider, xhr + ', ' + status + ', ' + error);
        },
        complete: function (data) {
            if (onComplete)
                onComplete();
        },
        xhr: xhrcallback
    });
}

RealGridJS.LocalTreeDataProvider.prototype.loadData = function (options, onSuccess, onFailed, onComplete, onXhr) {
    var provider = this;
    var xhrcallback = undefined;
    if (onXhr) {
        xhrcallback = function () {
            if (onXhr)
                return onXhr();
        }
    }
    $.ajax({
        type: options.method ? opt.method : "GET",
        url: options.url,
        data: options.params,
        success: function (data) {
            if (options.filters) {
                provider.setFilters(provider.filters);
            };

            if (options.type == "json") {
                provider.fillJsonData(data, options);
            } else if (options.type == "xml") {
                provider.fillXmlData(data, options);
            } else if (options.type == "csv") {
                provider.fillCsvData(data, options);
            };

            if (onComplete)
                onComplete(provider);
        },
        error: function (xhr, status, error) {
            if (onFailed)
                onFailed(provider, xhr + ', ' + status + ', ' + error);
        },
        complete: function (data) {
            if (onComplete)
                onComplete();
        },
        xhr: xhrcallback
    });
}

RealGridJS.GridView.prototype.addImageList = function (images) {
    var imgs = new RealGridJS.ImageList(images.name, images.rootUrl);
    imgs.addUrls(images.images);
    this.registerImageList(imgs);
}

RealGridJS.TreeView.prototype.addImageList = function (images) {
    var imgs = new RealGridJS.ImageList(images.name, images.rootUrl);
    imgs.addUrls(images.images);
    this.registerImageList(imgs);
}
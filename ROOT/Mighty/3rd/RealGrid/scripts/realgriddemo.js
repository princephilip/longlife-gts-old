﻿/*
    SKINS
*/
function clearAllStyles(grid) {
    if (grid) {
        grid.clearStyles("grid");
        grid.clearStyles("panel");
        grid.clearStyles("body");
        grid.clearStyles("body.empty");
        grid.clearStyles("fixed");
        grid.clearStyles("fixed.colBar");
        grid.clearStyles("fixed.rowBar");
        grid.clearStyles("header");
        grid.clearStyles("header.group");
        grid.clearStyles("footer");
        grid.clearStyles("rowGroup.header");
        grid.clearStyles("rowGroup.footer");
        grid.clearStyles("rowGroup.head");
        grid.clearStyles("rowGroup.foot");
        grid.clearStyles("rowGroup.headerBar");
        grid.clearStyles("rowGroup.footerBar");
        grid.clearStyles("rowGroup.bar");
        grid.clearStyles("rowGroup.panel");
        grid.clearStyles("indicator");
        grid.clearStyles("indicator.head");
        grid.clearStyles("indicator.foot");
        grid.clearStyles("checkBar");
        grid.clearStyles("checkBar.head");
        grid.clearStyles("checkBar.foot");
        grid.clearStyles("stateBar");
        grid.clearStyles("stateBar.head");
        grid.clearStyles("stateBar.foot");
        grid.clearStyles("selection");
        grid.clearStyles("tree.expander");
    };
};

function getSkin(grid, skinId) {
    if (grid == null) {
        if (window['grdMain'] == undefined)
            grid = treeMain;
        else
            grid = grdMain;
    }

  
    $.ajaxSetup({ cache: false });
            
    var params = {};
    params.SkinId = skinId;

    $.getJSON("/Manager/GetSkin", params, function (data) {
        if (data) {
            var styles = JSON.parse(data.SkinSource);
            clearAllStyles(grid);
            grid.setStyles(styles);
        };
    })
    .done(function () {
        console.log("getSkin success");
    })
    .fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        console && console.log("jQuery getJSON() Failed: " + err);
        alert("jQuery getJSON() Failed: " + err);
    })
    .always(function () {
        console.log("complete");
    });
};

function setSkin(grid, skinId, completed) {
    $.ajaxSetup({ cache: false });
            
    var params = {};
    params.SkinId = skinId;

    $.getJSON("/Manager/GetSkin", params, function (data) {
        if (data) {
            var style = JSON.parse(data.SkinSource);
            clearAllStyles(grid);
            grid.setStyles(style);
        };
    })
    .done(function () {
        console.log("getSkin success");
    })
    .fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        console && console.log("jQuery getJSON() Failed: " + err);
        alert("jQuery getJSON() Failed: " + err);
    })
    .always(function () {
        console.log("complete");
        if (completed)
            completed();
    });
}

/*-------------------------------------------------------------------------------------*/

function SetupGrid(tagid, width, height, onload, params) {
    setupGrid(tagid, width, height, onload, params);
}

// Remote Call, fiddle.net등
function setupRemoteGrid(tagid, width, height, url, onload, params) {
    var flashvars = { 
        id: tagid 
    };

    if (params) {
        for (var p in params) {
            flashvars[p] = params[p];
        }
    }

    if (onload){
        flashvars.onload = typeof(onload) === "function" ? onload.name : onload;
        console && console.log(flashvars);
    }

    var pars = {
        quality: "high",
        wmode: "opaque",
        allowscriptaccess: "always",
        allowfullscreen: "true"
    };

    var attrs = {
        id: tagid,
        name: tagid,
        align: "middle"
    };

    /* SWFObject v2.2 <http://code.google.com/p/swfobject/> */
    var swfUrl = url + "/Objects/RealGridWeb.swf";
    swfobject.embedSWF(swfUrl, tagid, width, height, "11.1.0", url + "/Objects/expressInstall.swf", flashvars, pars, attrs);
};

function formatInt(v) {
    return v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

//ADD DATA FIELD
function addDataField(dataProvider) {
    var field = {
        fieldName: "Field1",
        dataType: "text",
        length: 50,
        defaultValue: null
    };

    dataProvider.setFields([field]);
};

//ADD DATA FIELDS
function addDataFields(grid, dataProvider) {

    var fields = [];
    var field = new RealGrids.DataField("Field1");

    fields.push(field);

    field = {
        fieldName: "Field2",
        dataType: "text",
        length: 50,
        defaultValue: null
    };

    fields.push(field);
    fields.push(new RealGrids.DataField("Field3"));
    fields.push(new RealGrids.DataField("Field4"));
    fields.push(new RealGrids.DataField("Field5"));

    dataProvider.setFields(fields);

    console && console.log("==> Set DataField: ");
    console && console.log(fields);
};

//ADD COLUMN
function addColumn(grid) {
    var column = {
        type: "data",
        name: "column1",
        fieldName: "field1",
        width: 150,
        header: {
            text: "COLUMN"
        }
    };
    grid.setColumns([column]);
};

//ADD COLUMNS
function addColumns(grid) {
    var columns = [];
    var column;

    column = new RealGrids.DataColumn();
    column.name = "column1";
    column.fieldName = "Field1";
    column.header = { text: "Column 1" }
    column.width = 150;
    columns.push(column);

    column = new RealGrids.DataColumn();
    column.name = "column2";
    column.fieldName = "Field2";
    column.header = { text: "Column 2" }
    column.width = 100;
    columns.push(column);

    column = new RealGrids.DataColumn();
    column.name = "column3";
    column.fieldName = "Field3";
    column.header = { text: "Column 3" }
    column.width = 100;
    columns.push(column);

    column = new RealGrids.DataColumn();
    column.name = "column4";
    column.fieldName = "Field4";
    column.header = { text: "Column 4" }
    column.width = 100;
    columns.push(column);

    column = new RealGrids.DataColumn();
    column.name = "column5";
    column.fieldName = "Field5";
    column.header = { text: "Column 5" }
    column.width = 100;
    columns.push(column);

    grid.setColumns(columns);
    console.log("==> Set Data Columns; ");
};

//setDefaultStyles
function setDefaultStyles(grid) {
    var style = {};
    style.grid = { font: "맑은 고딕,12" };
    style.panel = {};
    style.body = {};
    style.empth = {};
    style.header = { };
    style.footer = {};

    grid.setStyles(style);
    console && console.log("==> Set default grid styles; ");
};

//setDefaultStyles
function setDefaultOptions(grid) {
    grid.setPanel({
        visible: false
    });

    grid.setDisplayOptions({
        rowHeight: 24
    });

    grid.setFooter({
        visible: false
    });

    grid.setCheckBar({
        visible: false
    });

    grid.setIndicator({
        visible: false
    });

    grid.setStateBar({
        visible: false
    });

    grid.setHeader({
        minHeight: 32
    });
    console && console.log("==> Set default grid options; ");
};

//setGridStyle
function setGridStyle(grid) {
    var style = {};
    style.grid = {};
    style.panel = {};
    style.body = {};
    style.empth = {};
    style.header = { background: "linear,#FFD3D3D3,#FF3E3E3E,90" };
    style.footer = {};

    console && console.log("==> Set GridStyle: ");
    console && console.log(style);
    grid.setStyles(style);
};

//addUserFields
function addUserFields(grid, dataProvider) {
    console && console.log(RealGrids);

    var fields = [];
    var field;

    fields.push(new RealGrids.DataField("UserId"));
    fields.push(new RealGrids.DataField("UserName"));
    fields.push(new RealGrids.DataField("Age"));
    fields.push(new RealGrids.DataField("Gender"));
    fields.push(new RealGrids.DataField("MobilePhone"));
    fields.push(new RealGrids.DataField("Email"));

    dataProvider.setFields(fields);

    console && console.log("==> addUserFields()");
    console && console.log(fields);
};

//addUserColumns
function addUserColumns(grid) {
    var columns = [];
    var column;

    column = new RealGrids.DataColumn();
    column.fieldName = "UserId";
    column.header = { text: "아이디" }
    column.width = 150;
    columns.push(column);

    column = new RealGrids.DataColumn();
    column.fieldName = "UserName";
    column.header = { text: "이름" }
    column.width = 150;
    columns.push(column);

    column = new RealGrids.DataColumn();
    column.fieldName = "Age";
    column.header = { text: "나이" }
    column.width = 60;
    columns.push(column);

    column = new RealGrids.DataColumn();
    column.fieldName = "Gender";
    column.header = { text: "성별" }
    column.width = 60;
    columns.push(column);

    column = new RealGrids.DataColumn();
    column.fieldName = "MobilePhone";
    column.header = { text: "휴대폰" }
    column.width = 150;
    columns.push(column);

    column = new RealGrids.DataColumn();
    column.fieldName = "Email";
    column.header = { text: "이메일" }
    column.width = 200;
    columns.push(column);

    grid.setColumns(columns);

    console && console.log("==> addUserColumns()");
    console && console.log(columns);
};

//addNewUser
function addNewUser(grid, dataProvider, userId, userName, age, sex, phone, mail) {
    var row = {};
    row.UserId = userId;
    row.UserName = userName;
    row.Age = age;
    row.Gender = sex;
    row.MobilePhone = phone;
    row.Email = mail;

    dataProvider.addRow(row);

    console && console.log("==> addNewUser()");
    console && console.log(row);
};

//setUserDataRows
function setUserDataRows(grid, dataProvider) {
    if (grid) {
        var rows = [];
        rows.push(["user1", "홍길동", "28", "남", "001-0002-0001", "user1@naver.com"]);
        rows.push(["user3", "임꺽정", "25", "남", "001-0002-0003", "user3@naver.com"]);
        rows.push(["user6", "구미호", "21", "여", "001-0002-0006", "user6@naver.com"]);
        rows.push(["user2", "전우치", "25", "남", "001-0002-0002", "user2@naver.com"]);
        rows.push(["user4", "일지매", "38", "남", "001-0002-0004", "user4@naver.com"]);
        rows.push(["user5", "장길산", "55", "남", "001-0002-0005", "user5@naver.com"]);
        rows.push(["user7", "김미려", "44", "여", "001-0002-0007", "user7@naver.com"]);
        rows.push(["user8", "김화수", "33", "여", "001-0002-0008", "user8@naver.com"]);
        rows.push(["user9", "이태수", "55", "남", "001-0002-0009", "user9@naver.com"]);
        rows.push(["user10", "박양신", "65", "남", "001-0002-0010", "user10@naver.com"]);
        rows.push(["user11", "장영시", "29", "여", "001-0002-0011", "user11@naver.com"]);
        rows.push(["user12", "황혜리", "18", "여", "001-0002-0012", "user12@naver.com"]);
        rows.push(["user13", "오규식", "52", "남", "001-0002-0013", "user13@naver.com"]);
        rows.push(["user14", "임태철", "61", "남", "001-0002-0014", "user14@naver.com"]);
        rows.push(["user15", "이여월", "26", "여", "001-0002-0015", "user15@naver.com"]);
        rows.push(["user16", "최창호", "46", "남", "001-0002-0016", "user16@naver.com"]);
        rows.push(["user17", "육이오", "64", "남", "001-0002-0017", "user17@naver.com"]);
        dataProvider.setRows(rows);

        console && console.log("==> setUserDataRows()");
        console && console.log(rows);
    };
};

//setDisplayOptions
function setDisplayOptions(grid) {
    // options
    var options = new RealGrids.DisplayOptions();
    options.rowResizable = true;
    grid.setDisplayOptions(options);

    console && console.log("==> setDisplayOptions()");
    console && console.log(options);
};

//setFixedOptions
function setFixedOptions(grid) {
    // options
    var options = new RealGrids.FixedOptions();
    options.colCount = 2;
    options.rowCount = 2;
    grid.setFixedOptions(options);

    console && console.log("==> setFixedOptions()");
    console && console.log(options);
};

//setPanelOptions
function setPanelOptions(grid) {
    // options
    var options = new RealGrids.PanelOptions();
    options.visible = !options.visible;
    grid.setPanel(options);

    console && console.log("==> setPanel()");
    console && console.log(options);
};

function addLargeDataFields(grid, dataProvider) {
    var fields = [
        {
            fieldName: "ItemId",
            dataType: "text",
            length: 50,
            defaultValue: null
        },
        {
            fieldName: "ItemName",
            dataType: "text",
            length: 100,
            defaultValue: null
        },
        {
            fieldName: "ServiceName",
            dataType: "text",
            length: 200,
            defaultValue: null
        },
        {
            fieldName: "RequestType",
            dataType: "text",
            length: 50,
            defaultValue: null
        },
        {
            fieldName: "LowBounds",
            dataType: "text",
            length: 50,
            defaultValue: null
        },
        {
            fieldName: "LowSign",
            dataType: "text",
            length: 50,
            defaultValue: null
        },
        {
            fieldName: "HighSign",
            dataType: "text",
            length: 50,
            defaultValue: null
        },
        {
            fieldName: "HighBounds",
            dataType: "text",
            length: 50,
            defaultValue: null
        },
        {
            fieldName: "CheckPrice",
            dataType: "text",
            length: 100,
            defaultValue: null
        },
        {
            fieldName: "CheckUnit",
            dataType: "text",
            length: 50,
            defaultValue: null
        }
    ];

    dataProvider.setFields(fields);
    console && console.log("==> addLargeDataFields()");
};

function addLargeDataColumns(grid) {
    var columns = [];
    var column;

    column = new RealGrids.DataColumn();
    column.fieldName = "ItemId";
    column.header = { text: "ItemId" }
    column.width = 80;
    columns.push(column);

    column = new RealGrids.DataColumn();
    column.fieldName = "ItemName";
    column.header = { text: "항목명" }
    column.width = 100;
    columns.push(column);

    column = new RealGrids.DataColumn();
    column.fieldName = "RequestType";
    column.header = { text: "분야" }
    column.width = 80;
    columns.push(column);

    column = new RealGrids.DataColumn();
    column.fieldName = "ServiceName";
    column.header = { text: "유형명" }
    column.width = 200;
    columns.push(column);

    /*
    column = new RealGrids.DataColumn();
    column.fieldName = "LowBounds";
    column.header = { text: "하위값" }
    column.width = 70;
    columns.push(column);

    column = new RealGrids.DataColumn();
    column.fieldName = "LowSign";
    column.header = { text: "하위값 기호" }
    column.width = 70;
    columns.push(column);

    column = new RealGrids.DataColumn();
    column.fieldName = "HighSign";
    column.header = { text: "상위값 기호" }
    column.width = 70;
    columns.push(column);

    column = new RealGrids.DataColumn();
    column.fieldName = "HighBounds";
    column.header = { text: "상위값" }
    column.width = 70;
    columns.push(column);
    */

    var group = {
        type: "group",
        header: {
            text: "기준 범위"
        },
        width: 200,
        columns: [
            {
                type: "group",
                orientation: "vertical",
                header: {
                    text: "하위 조건",
                    visible: false
                },
                columns: [
                    {
                        type: "data",
                        fieldName: "LowBounds",
                        width: 80,
                        header: { text: "하위 값" }
                    }, 
                    {
                        type: "data",
                        fieldName: "LowSign",
                        width: 80,
                        header: { text: "기호" }
                    }
                ],
            },
            {
                type: "group",
                orientation: "vertical",
                header: {
                    text: "상위 조건",
                    visible: false
                },
                columns: [
                    {
                        type: "data",
                        fieldName: "HighBounds",
                        width: 80,
                        header: { text: "상위 값" }
                    }, {
                        type: "data",
                        fieldName: "HighSign",
                        width: 80,
                        header: { text: "기호" }
                    }
                ],
            }
        ]
    };
    columns.push(group);

    column = new RealGrids.DataColumn();
    column.fieldName = "CheckUnit";
    column.header = { text: "단위" }
    column.width = 80;
    columns.push(column);

    column = new RealGrids.DataColumn();
    column.fieldName = "CheckPrice";
    column.header = { text: "수수료" }
    column.width = 90;
    column.styles = { textAlignment: "far" };
    columns.push(column);

    grid.setColumns(columns);

    console && console.log("==> addLargeDataColumns()");
    console && console.log(columns);
};

function setLargeDataRows(grid, provider, itemName) {
    var params = {};
    params.ItemName = itemName;

    provider.clearRows();

    //$.getJSON("/Demo/GetServiceItemArray", params, function (data) {
    $.getJSON("/Demo/GetServiceItemModels", params, function (data) {
        provider.setRows(data);
        
        /*add row 방식
        $.each(data, function (index, value) {
            dataProvider.addRow(value);
        });*/
    })
    .done(function () {
        console && console.log("success");
    })
    .fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        console && console.log("Request Failed: " + err);
        alert("Request Failed: " + err);
    })
    .always(function () {
        console && console.log("complete");
    });
    //.always(function () { $("#Loading").hide(); });
};

function setLargeDataRowsFromFile(grid, provider) {
    provider.clearRows();

    $.getJSON("/DemoData/LargeDataSet.json", null, function (data) {
        grid.showBusyIndicator();
        provider.setRows(data);

        /*add row 방식
        $.each(data, function (index, value) {
        dataProvider.addRow(value);
        });*/
    })
    .done(function () {
        console && console.log("success");
    })
    .fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        console && console.log("Request Failed: " + err);
        alert("Request Failed: " + err);
    })
    .always(function () {
        console && console.log("complete");
        grid.hideBusyIndicator();
    });
    //.always(function () { $("#Loading").hide(); });
};



/*------------------------------------------------------------------------------------
    CUSTOMER ORDERS DEMO
------------------------------------------------------------------------------------*/
function _setHilightColumn(grid, cols) {
    $(cols).each( function (idx, v) {
        var _col = grid.columnByName(v);
        if (_col) {
            col.header = {
                styles: {
                    background: "linear,#22ffd500,#ffffd500,90"
                }
            };
            grid.setColumn(col);
        };
    });
};

function setCustomerOrdersColumns(grid, provider, url) {
    setCustomerOrdersColumns(grid, provider, url, null);
};

function setCustomerOrdersColumns(grid, provider, url, hiCols) {
    $.getJSON(url, null, function (data) {
        console && console.log(data);

        provider.setFields(data.fields);
        grid.setColumns(data.columnSet1);
    })
    .done(function () {
        if (hiCols){
            _setHilightColumn(grid, hiCols);
        };
        console && console.log("success");
    })
    .fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        console && console.log("Request Failed: " + err);
        alert("Request Failed: " + err);
    })
    .always(function () {
        console && console.log("complete");
    });
};

function setCustomerOrdersData(grid, provider, customerId, succCallback) {
    var params = {};
    params.CustomerId = customerId;

    $.getJSON("/Demo/GetCustomOrders", params, function (data) {
        provider.setRows(data);
    })
    .done(function () {
        console && console.log("success");
    })
    .fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        console && console.log("Request Failed: " + err);
        alert("Request Failed: " + err);
    })
    .always(function () {
        if (succCallback && typeof(succCallback === "function")){
            succCallback();
        }
        console && console.log("complete");
    });
};

function getCustomerOrdersData() {
    var result;
    var params = {};
    params.CustomerId = "";

    $.getJSON("/Demo/GetCustomOrders", params, function (data) {
        result = data;
    })
    .done(function () {
        return result;
    })
    .fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        console && console.log("Request Failed: " + err);
        alert("Request Failed: " + err);
    })
    .always(function () {
        console && console.log("complete");
    });
};

function setCustomerOrdersByCountryData(provider, customerId, country, msgLabel) {
    var params = {}
    params.CustomerId = customerId;
    params.Country = country;

    $.getJSON("/Demo/GetCustomerOrdersByCountry", params, function (data) {
        provider.setRows(data);
    })
    .done(function () {
        console && console.log("GetCustomerOrdersByCountry success");
        $(msgLabel).text("Customer orders by '"+ country +"' is loaded!");
    })
    .fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        console && console.log("Request Failed: " + err);
        alert("Request Failed: " + err);
    })
    .always(function () {
        console.log("GetCustomerOrdersByCountry complete");
    });
};

function setGridColumns(grid, url) {
    $.getJSON(url, null, function (data) {
        grid.setColumns(data.columnSet1);
    })
    .done(function () {
        console && console.log("success");
    })
    .fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        console && console.log("Request Failed: " + err);
        alert("Request Failed: " + err);
    })
    .always(function () {
        console && console.log("complete");
    });
};

function setProviderFields(provider, url) {
    $.getJSON(url, null, function (data) {
        provider.setFields(data.fields);
    })
    .done(function () {
        console && console.log("success");
    })
    .fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        console && console.log("Request Failed: " + err);
        alert("Request Failed: " + err);
    })
    .always(function () {
        console && console.log("complete");
    });
};

function addColumnContinue(grid, count) {
    var columns = [];

    for (var i = 0; i < count; i++){
        columns.push({
            type: "data",
            name: "column" + i,
            fieldName: "field" + i,
            width: 100,
            headder: {
                text : "COLUMN"
            }
        });
    }
    grid.setColumns(columns);
    console && console.log("==> Set Data Columns; ");
};

/*------------------------------------------------------------------------------------*/

function setTreeViewColumns(grid, provider, url, hiCols, type) {
    $.ajaxSetup({ cache: false });
    $.getJSON(url, null, function (data) {
        console && console.log(data);

        provider.setFields(data.fields);
        grid.setColumns(data.columnSet1);
    })
    .done(function () {
        if (hiCols){
            _setHilightColumn(grid, hiCols);
        };
        if (type == "Array"){
            setArrayTypeData(provider);
        } else if (type == "Xml"){
            setXmlTypeData(provider);
        }
    })
    .fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        console && console.log("Request Failed: " + err);
        alert("Request Failed: " + err);
    })
    .always(function () {
        console && console.log("complete");
    });
};

function setJsonTypeData(provider, url) {
    console & console.log("setJsonTypeData");

   $.ajaxSetup({ cache: false });
   $.getJSON(url, null, function (data) {
        console && console.log(data);

        provider.setJsonRows(data, "rows", "", "icon");
    })
    .done(function () {
        console & console.log("setJsonTypeData succeed");
    })
    .fail(function (jqxhr, textStatus, error) {
        var err = textStatus + ', ' + error;
        console && console.log("Request Failed: " + err);
        alert("Request Failed: " + err);
    })
    .always(function () {
        console && console.log("complete");
    });   
};

function setArrayTypeData(provider) {
    console && console.log("setArrayTypeData");

    var rows = [    
        [0, "1", "Argentina", "CACTU", "(1) 135-5555", "28", "Argentina", "Head", "001-0000-0000", "user1@naver.com"],
        [0, "1.1", "Buenos Aires", "OCEAN", "(1) 135-5333", "25", "Argentina", "Middle", "001-0001-0000", "user3@googlei.com"],
        [0, "1.1.1", "Rancho grande", "RANCH", "(1) 123-5555", "21", "Argentina", "Local", "001-0001-0001", "user6@yahoo.com"],
        [0, "1.1.2", "Atheha grande", "PTNCH", "(1) 123-4578", "28", "Argentina", "Local", "001-0001-0002", "user6@korea.com"],
        [1, "2", "Brazil", "HANAR", "(21) 555-0091", "25", "Brazil", "Head", "002-0000-0000", "user2@naver.com"],
        [1, "2.1", "Rio de Janeiro", "QUEDE", "(21) 555-4252", "38", "Brazil", "Middle", "002-0001-0000", "user4@google.com"],
        [1, "2.2", "Sao Paulo", "FAMIA", "(11) 555-9857", "38", "Brazil", "Middle", "002-0002-0001", "user4@change.com"],
        [1, "2.2.1", "Queen Cozinha", "QUEEN", "(11) 555-1189", "38", "Brazil", "Local", "001-0002-0002", "user4@paulo.com"],
        [2, "3", "France", "DUMON", "40.67.88.88", "38", "France", "Head", "003-0000-0000", "user4@naver.com"],
        [2, "3.1", "Nantes", "FRANR", "40.32.21.21", "38", "France", "Middle", "003-0001-0000", "user4@pvick.com"],
        [2, "3.2", "Paris", "PARIS", "(1) 42.34.22.66", "38", "France", "Middle", "003-0002-0000", "user64@naver.com"],
        [2, "3.2.1", "du monde", "SPECD", "(1) 47.55.60.10", "38", "France", "Local", "003-0002-0001", "user4@google.com"],
        [3, "4", "Mexico", "ANTON", "(5) 555-3932", "38", "Mexico", "Head", "004-0000-0000", "user24@naver.com"],
        [3, "4.1", "México D.F", "CENTC", "(5) 555-3392", "38", "Mexico", "Middle", "004-0001-0000", "user14@naver.com"],
        [3, "4.1.1", "Pericles Comidas", "PERIC", "(5) 552-3745", "38", "Mexico", "Local", "004-0001-0001", "user334@ekpas.com"],
        [4, "5", "Portugal", "FURIB", "(1) 354-2534", "38", "Portugal", "Head", "005-0000-0000", "user4@naver.com"],
        [4, "5.1", "Lisboa", "PRINI", "(11) (1) 356-5634", "38", "Portugal", "Middle", "005-0001-0000", "ersuser@naver.com"],
        [5, "6", "Spain", "BOLID", "(91) 555 22 82", "38", "Spain", "Head", "006-0000-0000", "987user4@google.com"],
        [5, "6.1", "Madrid", "FISSA", "(91) 555 94 44", "38", "Spain", "Middle", "006-0001-0000", "user4dy4@naver.com"],
        [5, "6.1.1", "Romero tomillo", "ROMEY", "(91) 745 6200", "38", "Spain", "Local", "006-0001-0001", "user4587@korea.com"],
        [6, "7", "UK", "AROUT", "(171) 555-7788", "38", "UK", "Head", "007-0000-0000", "user664@naver.com"],
        [6, "7.1", "London", "BSBEV", "(171) 555-1212", "38", "UK", "Middle", "007-0001-0000", "user789@gmail.com"],
        [6, "7.1.1", "Eastern Connection", "EASTC", "(171) 555-0297", "38", "UK", "Local", "007-0001-0001", "udfeser4@naver.com"],
        [7, "8", "USA", "LONEP", "(503) 555-9573", "38", "USA", "Head", "008-0000-0000", "user4@naver.com"],
        [7, "8.1", "Portland", "THEBI", "(503) 555-3612", "38", "USA", "Middle", "008-0001-0000", "usfdaer4@naver.com"],
        [8, "9", "Venezuela", "HILAA", "(5) 555-1340", "38", "Venezuela", "Head", "009-0000-0000", "usgderer4@naver.com"],
        [8, "9.1", "I.de Margarita", "LINOD", "(8) 34-56-12", "38", "Venezuela", "Middle", "009-0001-0000", "usedsafr4@naver.com"]
    ];

    provider.setRows(rows, "tree", true, "", "icon");                
};

function setXmlTypeData(provider) {
    console && console.log("setXmlTypeData");

    var rows = "<rows>"
                + "<row icon='0' Col0='남자' Col1='6787' Col2='5699' Col3='2601' Col4='4309' Col5='5804' Col6='11120' Col7='5204' Col8='3066'>"
                + "<row icon='-1'Col0='유럽' Col1='3864' Col2='3668' Col3='1682' Col4='1767' Col5='864' Col6='6484' Col7='2983' Col8='1784'>"
                + "<row icon='2' Col0='독일' Col1='3285' Col2='3254' Col3='1464' Col4='1188' Col5='491' Col6='5185' Col7='2324' Col8='1524'></row>"
                + "<row icon='3' Col0='그리스' Col1='332' Col2='201' Col3='199' Col4='287' Col5='292' Col6='630' Col7='302' Col8='128'></row>"
                + "<row icon='4' Col0='헝가리' Col1='232' Col2='202' Col3='20' Col4='278' Col5='75' Col6='650' Col7='348' Col8='124'></row>"
                + "<row icon='5' Col0='아이슬란드' Col1='15' Col2='11' Col3='2' Col4='14' Col5='6' Col6='19' Col7='9' Col8='8'></row></row>"
                + "<row icon='-1' Col0='아프리카' Col1='1868' Col2='1296' Col3='478' Col4='1858' Col5='4592' Col6='3219' Col7='1482' Col8='693'>"
                + "<row icon='6' Col0='이집트' Col1='1868' Col2='1296' Col3='478' Col4='1858' Col5='4592' Col6='3219' Col7='1482' Col8='693'></row></row>"
                + "<row icon='-1' Col0='오세아니아' Col1='1055' Col2='735' Col3='441' Col4='684' Col5='348' Col6='1417' Col7='739' Col8='589'>"
                + "<row icon='7' Col0='오스트레일리아' Col1='891' Col2='621' Col3='382' Col4='574' Col5='240' Col6='1204' Col7='589' Col8='516'></row>"
                + "<row icon='8' Col0='뉴질랜드' Col1='164' Col2='114' Col3='59' Col4='110' Col5='108' Col6='213' Col7='150' Col8='73'></row></row></row>"
                + "<row icon='1' Col0='여자' Col1='23131' Col2='5512' Col3='21523' Col4='35267' Col5='10938' Col6='9184' Col7='2499' Col8='14253'>"
                + "<row icon='-1' Col0='아시아' Col1='2474' Col2='1831' Col3='2715' Col4='5644' Col5='9076' Col6='2242' Col7='1424' Col8='7613'>"
                + "<row icon='9' Col0='필리핀' Col1='1007' Col2='465' Col3='1043' Col4='1665' Col5='930' Col6='690' Col7='241' Col8='4677'></row>"
                + "<row icon='10' Col0='싱가포르' Col1='112' Col2='116' Col3='192' Col4='95' Col5='0' Col6='10' Col7='35' Col8='74'></row>"
                + "<row icon='11' Col0='태국' Col1='840' Col2='787' Col3='923' Col4='3360' Col5='6359' Col6='1258' Col7='918' Col8='1973'></row>"
                + "<row icon='12' Col0='터키' Col1='515' Col2='413' Col3='557' Col4='524' Col5='1787' Col6='284' Col7='230' Col8='889'></row></row>"
                + "<row icon='-1' Col0='북아메리카' Col1='19862' Col2='3155' Col3='18231' Col4='27803' Col5='961' Col6='6395' Col7='988' Col8='4376'>"
                + "<row icon='13' Col0='캐나다' Col1='1530' Col2='1562' Col3='1749' Col4='1556' Col5='96' Col6='143' Col7='297' Col8='470'></row>"
                + "<row icon='14' Col0='멕시코' Col1='1340' Col2='1593' Col3='1809' Col4='4172' Col5='664' Col6='1605' Col7='691' Col8='3906'></row>"
                + "<row icon='15' Col0='미국' Col1='16992' Col2='-' Col3='14673' Col4='22075' Col5='201' Col6='4647' Col7='-' Col8='-'></row></row>"
                + "<row icon='-1' Col0='남아메리카' Col1='795' Col2='526' Col3='577' Col4='1820' Col5='901' Col6='547' Col7='87' Col8='2264'>"
                + "<row icon='16' Col0='볼리비아' Col1='143' Col2='93' Col3='86' Col4='473' Col5='785' Col6='167' Col7='2' Col8='321'></row>"
                + "<row icon='17' Col0='코스타리카' Col1='101' Col2='79' Col3='93' Col4='153' Col5='8' Col6='30' Col7='23' Col8='199'></row>"
                + "<row icon='18' Col0='페루' Col1='463' Col2='320' Col3='289' Col4='1060' Col5='96' Col6='314' Col7='49' Col8='1577'></row>"
                + "<row icon='19' Col0='우루과이' Col1='88' Col2='34' Col3='109' Col4='134' Col5='12' Col6='36' Col7='13' Col8='167'></row></row></row>"
                + "</rows>"

    provider.setXmlRows(rows, "row", "", "icon");
};

function appendListOptions(options, list){
    for (var i = 0; i < options.length; ++i) {
        $("<option />", { value: options[i], text: options[i] }).appendTo(list);
    }
};

// setup grid
function initGrid(options) {
    if (options && options.type) {
        if (options.type == "js") {
            if (options.width) {
                options.width = (options.width.indexOf("%") > 0) ? options.width : options.width + "px";
                document.getElementById(options.objId).style.width = options.width;
            }
            if (options.height) {
                options.height = (options.height.indexOf("%") > 0) ? options.height : options.height + "px";
                document.getElementById(options.objId).style.height = options.height;
            }
            RealGridJS.setTrace(false);
            RealGridJS.setRootContext("/img/realgridjs")
            options.jsInit(options.objId);
        } else {
            RealGrids.onload = options.flashInit;

            if (options.viewType == "tree")
                setupTree(options.objId, options.width, options.height);
            else
                setupGrid(options.objId, options.width, options.height);
        };
    };
};

function showJsToast(grid) {
    if (realgridType == "js")
        grid.showToast("Load data...",true);
}

function hideJsToast(grid) {
    if (realgridType == "js")
        grid.hideToast();
}

function loadGridConfig(grid, provider, gridUrl, onDone, loadOptions, onLoadCompleted, onLoadFailed) {
    if (!gridUrl) {
        if (language == "ko")
            gridUrl = "/DemoGrids/defaultgrid_ko.grid";
        else
            gridUrl = "/DemoGrids/defaultgrid_en.grid";
    }

    $.ajaxSetup({ cache: false });

    $.getJSON(gridUrl, {}, function (config) {
        grid.loadGrid(config);

        if (!config.data.source && loadOptions) {
            provider.loadData(loadOptions, onLoadCompleted, onLoadFailed);
        };

        if (onDone)
            onDone(grid);
    });
}

function loadJsGridConfig(grid, provider, gridUrl, onDone, loadOptions, onLoadCompleted, onLoadFailed) {
    if (!gridUrl) {
        if (language == "ko")
            gridUrl = "/DemoGrids/defaultgrid_ko.grid";
        else
            gridUrl = "/DemoGrids/defaultgrid_en.grid";
    };

    $.ajaxSetup({ cache: false });

    $.getJSON(gridUrl, {}, function (config) {
        provider.setFields(config.data.fields);
        grid.setColumns(config.columns);
        if (config.grid) // options
            grid.setOptions(config.grid);

        if (!loadOptions)
            loadOptions = config.data.source;
        
        if (loadOptions) {
            
            provider.loadData(loadOptions, onLoadCompleted, onLoadFailed);
        };
        if (onDone)
            onDone(grid);
    });
}


//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 : @main_cfms.js|대쉬보드 메인 화면(협력업체) JavaScript
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
// 김성한       	 x-internetinfo      2014.04.14 
//
//===========================================================================================
//	
//===========================================================================================

// 상단로고 변경
function getLogo() {
	$('#complogo').children('img').attr("src", "/Theme/images/menu/logo_cfms.jpg");
}

// 공지사항 데이터 호출
function getBoard() {
	var ls_data = _X.XmlSelect("si", "XG_COMTN_BBS", "R_WIDGET_01", new Array(mytop._CompanyCode, "BBSMSTR_000000000001", mytop._UserID), "array");
	if(ls_data[0][0]!=null && ls_data[0][0]!="") {
		for(var i=0; i<ls_data.length; i++) {
			$('#NO'+i).html(ls_data[i][1]);
			$('#SUBJECT'+i).html("<a href='javascript:popNotice("+ls_data[i][2]+")'>"+ls_data[i][3]+"</a>");
			$('#REGNAME'+i).html(ls_data[i][5]);
			$('#REGDATE'+i).html(ls_data[i][6]);
		}
	}
}

// 오늘 날짜 요일 구하기
function getToday() {
	var dt = new Date();
	var year = dt.getFullYear();
	var month = dt.getMonth()+1;
	var day = dt.getDate();
	var week = new Array('일', '월', '화', '수', '목', '금', '토');
	$('#TODAY').html(month + "월 " + day + "일&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;" + week[dt.getDay()] + "요일");
}

// 오늘의 날씨 데이터 호출
function getWeather(zone_no) {
	var ls_url = "/APPS/si/jsp/WeatherAPI.jsp";
	var ls_param = "zone=" + zone_no; 
	$.ajax({
		type: 'POST',
		url : ls_url,
		data: ls_param,
		dataType: 'text',
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		async: true,
		cache: false,
		success: function(data) {
			var dataArr = data.replace(/\r/g, "").replace(/\n/g, "").split("\t");
			var dataIcon = "";
			if(dataArr[0]!=null && dataArr[0]!="") {
				$('#WEATHER0').html(dataArr[0] + "&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;기온 : " + dataArr[1] + "℃");
				$('#WEATHER1').html("습도 : " + dataArr[2] + "%&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;강수확률 : " + dataArr[3] + "%");
				if(dataArr[5]=='0') dataIcon = "0" + dataArr[4];
				else dataIcon = "1" + dataArr[5];
				$('#weather_icon').css("background", "url('/images/weather/DB" + dataIcon + ".png') 0 0 no-repeat");
			}
		},
		error: function(data, status, err) {
			// alert('오늘의 날씨 조회가 실패하였습니다.');
		}
	});
}

// 공지사항 팝업
function popNotice(ntt_id) {
	var ls_data = _X.XmlSelect("si", "XG_COMTN_BBS", "R_WIDGET_05", new Array(mytop._CompanyCode, "BBSMSTR_000000000001", ntt_id), "array");
	var option = 'width=700,height=400';
	popwin = window.open("", "popNotice", "scrollbars=yes,toolbar=no,resizable=yes," + option);
	popwin.document.open();
	popwin.document.write('<head><title>공지사항</title><link rel="stylesheet" href="/css/egovbbsTemplate.css" type="text/css"><link rel="stylesheet" href="/css/bbs.css" type="text/css"></head><body style="margin:10px;padding:0"><table width="100%" cellpadding="8" class="table-search" border="0"><tbody><tr><td width="100%" class="title_left"><img src="/images/bbs/tit_icon.gif" width="3" height="16" hspace="3" style="vertical-align: middle" alt=""> &nbsp;공지사항 - 글조회</td></tr></tbody></table><table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="ffffff" class="generalTable"><tbody><tr><th width="15%" height="23" nowrap="" class="title_left">제목</th><td width="85%" colspan="5" nowrap="">'+ls_data[0][3]+'</td></tr><tr><th width="15%" height="23" nowrap="" class="title_left">작성자</th><td width="15%" class="lt_text3" nowrap="">'+ls_data[0][6]+'</td><th width="15%" height="23" nowrap="" class="title_left">작성시간</th><td width="15%" class="listCenter" nowrap="">'+ls_data[0][7]+'</td><th width="15%" height="23" nowrap="" class="title_left">조회수</th><td width="15%" class="listCenter" nowrap="">'+ls_data[0][8]+'</td></tr><tr><th height="23" class="title_left">글내용</th><td colspan="5"><div id="bbs_cn">'+ls_data[0][4].replace(/&lt;/g, "<").replace(/&gt;/g, ">")+'</div></td></tr></tbody></table></body>');
	popwin.document.close();
}

$(document).ready(function() {
	getLogo();
	getBoard();
	getToday();
	getWeather();
});

//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 : @main.js|대쉬보드 메인 화면 JavaScript
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
// 김성한       	 x-internetinfo      2014.04.14 
//
//===========================================================================================
//	
//===========================================================================================

// 업무보고 현황 데이터 호출
function getReprt() {
	var ls_data = _X.XmlSelect("si", "XG_COMTN_BBS", "R_WIDGET_02", new Array(mytop._CompanyCode), "array");
	if(ls_data[0][0]!=null && ls_data[0][0]!="") {
		// $('#D1REPORT0').html("<img src='/images/main/ico_blue.png'/>");
		// $('#D2REPORT0').html("<img src='/images/main/ico_yellow.png'/>");
		// $('#D3REPORT0').html("<img src='/images/main/ico_red.png'/>");
		// $('#D4REPORT0').html("<img src='/images/main/ico_light.png'/>");
		$('#D0REPORT0').html(ls_data[0][0]);
		$('#D1REPORT0').html(ls_data[0][1]);
		$('#D2REPORT0').html(ls_data[0][2]);
		$('#D3REPORT0').html(ls_data[0][3]);
		$('#D4REPORT0').html(ls_data[0][4]);
	}
	var ls_data = _X.XmlSelect("si", "XG_COMTN_BBS", "R_WIDGET_11", new Array(mytop._CompanyCode, mytop._UserID, "SI0204"), "array");
	if(ls_data[0][0]!=null && ls_data[0][0]!="") {
		for(var i=0; i<1; i++) {
			$("#D1REPORT"+i).css("cursor", "pointer").click(function(){getReprtLink('1');});
			$("#D2REPORT"+i).css("cursor", "pointer").click(function(){getReprtLink('2');});
			$("#D3REPORT"+i).css("cursor", "pointer").click(function(){getReprtLink('3');});
			$("#D4REPORT"+i).css("cursor", "pointer").click(function(){popNoReprt();});
		}
	}
}

// 불편신고 접수 현황 데이터 호출
function getVOC() {
	var ls_data = _X.XmlSelect("si", "XG_COMTN_BBS", "R_WIDGET_03", new Array(mytop._CompanyCode), "array");
	if(ls_data[0][0]!=null && ls_data[0][0]!="") {
		for(var i=0; i<ls_data.length; i++) {
			$('#D'+i+'VOC0').html(ls_data[i][0]);
			$('#D'+i+'VOC1').html(ls_data[i][1]);
			$('#D'+i+'VOC2').html(ls_data[i][2]);
		}
	}
	var ls_data = _X.XmlSelect("si", "XG_COMTN_BBS", "R_WIDGET_11", new Array(mytop._CompanyCode, mytop._UserID, "FM0401"), "array");
	if(ls_data[0][0]!=null && ls_data[0][0]!="") {
		for(var i=0; i<4; i++) {
			$('#D'+i+'VOC0').css("cursor", "pointer").click(getVOCLink('1',i+1));
			$('#D'+i+'VOC1').css("cursor", "pointer").click(getVOCLink('2',i+1));
			$('#D'+i+'VOC2').css("cursor", "pointer").click(getVOCLink('3',i+1));
		}
	}
}

// 순회관리 일일 점검계획 데이터 호출
function getCenter() {
	var ls_cnt = 0;
	var ls_data = _X.XmlSelect("si", "XG_COMTN_BBS", "R_WIDGET_07", new Array(mytop._CompanyCode), "array");
	if(ls_data[0][0]!=null && ls_data[0][0]!="") {
		for(var i=0; i<ls_data.length; i++) {
			var ls_capital = "-";
			var ls_middle = "-";
			var ls_honam = "-";
			var ls_youngnam = "-";
			if(ls_data[i][0] > 0) {
				ls_capital = ls_data[i][0]+"개소";
				ls_cnt++;
			}
			if(ls_data[i][1] > 0) {
				ls_middle = ls_data[i][1]+"개소";
				ls_cnt++;
			}
			if(ls_data[i][2] > 0) {
				ls_honam = ls_data[i][2]+"개소";
				ls_cnt++;
			}
			if(ls_data[i][3] > 0) {
				ls_youngnam = ls_data[i][3]+"개소";
				ls_cnt++;
			}
			$('#CAPITAL'+i).html(ls_capital);
			$('#MIDDLE'+i).html(ls_middle);
			$('#HONAM'+i).html(ls_honam);
			$('#YOUNGNAM'+i).html(ls_youngnam);
		}
	}
	if(ls_cnt > 0) {
		for(var i=0; i<2; i++) {
			$("#CAPITAL"+i).css("cursor", "pointer").click(function(){popCenter();});
			$("#MIDDLE"+i).css("cursor", "pointer").click(function(){popCenter();});
			$("#HONAM"+i).css("cursor", "pointer").click(function(){popCenter();});
			$("#YOUNGNAM"+i).css("cursor", "pointer").click(function(){popCenter();});
		}
	}
}

// 사업장 현황 데이터 호출
function getSite() {
	var ls_data = _X.XmlSelect("si", "XG_COMTN_BBS", "R_WIDGET_04", new Array(mytop._CompanyCode), "array");
	if(ls_data[0][0]!=null && ls_data[0][0]!="") {
		$('#SITECNT0').html(ls_data[0][0]);
		$('#SITECNT1').html(ls_data[0][1]);
		$('#SITECNT2').html(ls_data[0][2]);
	}
	var ls_data = _X.XmlSelect("si", "XG_COMTN_BBS", "R_WIDGET_11", new Array(mytop._CompanyCode, mytop._UserID, "SM0214"), "array");
	if(ls_data[0][0]!=null && ls_data[0][0]!="") {
		$("#SITECNT0").css("cursor", "pointer").click(function(){getSiteLink('1');});
		$("#SITECNT1").css("cursor", "pointer").click(function(){getSiteLink('2');});
		$("#SITECNT2").css("cursor", "pointer").click(function(){getSiteLink('3');});
	}
}

// 자산관리 현장 데이터 호출
function getMap(map_no) {
	var ls_map = new Array();
	if(map_no=="2") ls_map = ["인천","현대제철(인천공장)","878,068㎡","시설/미화/보안/조경/통근","인천광역시 동구 중봉대로 63","2814000000"];
	else if(map_no=="3") ls_map = ["의왕","의왕연구소","90,561㎡","시설/미화/보안/조경/통근","경기도 의왕시 철도박물관로 37 (삼동)","4143000000"];
	else if(map_no=="4") ls_map = ["소하리","기아자동차(소하리공장)","346,177㎡","시설/미화/보안/조경/통근","경기도 광명시 기아로 113 (소하동)","4121000000"];
	else if(map_no=="5") ls_map = ["안양","안양블루몬테","4,242㎡","시설/미화/조경/임대","경기도 안양시 만안구 예술공원로 213 (석수동)","4117100000"];
	else if(map_no=="6") ls_map = ["용인","현대모비스(마북연구소)","128,325㎡","시설/미화/보안/통근","경기도 용인시 기흥구 마북로240번길 17-2 (마북동)","4146300000"];
	else if(map_no=="7") ls_map = ["남양","현대자동차(남양연구소)","531,305㎡","시설/미화/보안/조경/통근","경기도 화성시 남양읍 현대연구소로 150","4159000000"];
	else if(map_no=="8") ls_map = ["화성","기아자동차(화성공장)","2,652,990㎡","시설/미화/보안/조경/통근","경기도 화성시 우정읍 기아자동차로 95","4159000000"];
	else if(map_no=="9") ls_map = ["당진","현대제철(당진공장)","7,404,959㎡","시설/미화/보안/조경/통근","충청남도 당진시 송악읍 북부산업로 1480","4427000000"];
	else if(map_no=="10") ls_map = ["아산","현대자동차(아산공장)","412,916㎡","시설/미화/보안/조경/통근","충청남도 아산시 인주면 현대로 1077","4420000000"];
	else if(map_no=="11") ls_map = ["진천","현대모비스(진천공장)","52,992㎡","시설/미화/보안/조경","충청북도 진천군 문백면 사양2길 95","4375000000"];
	else if(map_no=="12") ls_map = ["서산","현대파워텍(서산공장)","307,438㎡","미화/보안/조경","충청남도 서산시 지곡면 충의로 958","4421000000"];
	else if(map_no=="13") ls_map = ["전주","현대자동차(전주공장)","430,705㎡","미화/보안/조경/통근","전라북도 완주군 봉동읍 완주산단5로 163","4571000000"];
	else if(map_no=="14") ls_map = ["포항","현대제철(포항공장)","607,060㎡","시설/미화/보안/조경/통근","경상북도 포항시 남구 동해안로 6363 (송내동)","4711100000"];
	else if(map_no=="15") ls_map = ["울산","현대자동차(울산공장)","2,688,474㎡","미화/보안/조경/통근","울산광역시 북구 염포로 700 (양정동)","3120000000"];
	else if(map_no=="16") ls_map = ["창원","현대로템(창원공장)","266,252㎡","미화/보안/조경/통근","경상남도 창원시 의창구 창원대로 488 (대원동)","4812100000"];
	else if(map_no=="17") ls_map = ["광주","기아자동차(광주공장)","1,544,142㎡","미화/보안/조경/통근","광주광역시 서구 화운로 277 (내방동)","2914000000"];
	else if(map_no=="18") ls_map = ["순천","현대제철(순천공장)","20,820㎡","미화/보안/통근","전라남도 순천시 해룡면 인덕로 300","4615000000"];
	else ls_map = ["서울","현대자동차(양재사옥)","149,246㎡","시설/미화/보안/통근/임대","서울특별시 서초구 헌릉로 12 (양재동)","1100000000"];
	$('#MAP0').html(ls_map[1]);
	$('#MAP1').html(ls_map[2]);
	$('#MAP2').html(ls_map[3]);
	$('#MAP3').html(ls_map[4]);
	$('#map_photo').css("background", "url('Theme/images/photo/photo_site_" + map_no + ".jpg') 0 0 no-repeat").css("background-size", "cover");
	getWeather(ls_map[5], ls_map[0], ls_map[1]);
}

// 공지사항 데이터 호출
function getBoard() {
	var ls_data = _X.XmlSelect("im", "XG_COMTN_BBS", "R_WIDGET_06", new Array(mytop._CompanyCode, "BBSMSTR_000000000001", mytop._UserID), "array");
	if(ls_data[0][0]!=null && ls_data[0][0]!="") {
		for(var i=0; i<ls_data.length; i++) {
			$('#NO'+i).html(ls_data[i][1]);
			$('#SUBJECT'+i).html("<a href='javascript:popNotice("+ls_data[i][2]+")'>"+ls_data[i][3]+"</a>");
			$('#REGNAME'+i).html(ls_data[i][5]);
			$('#REGDATE'+i).html(ls_data[i][6]);
		}
	}
	$('#board_more').css("cursor", "pointer").click(function(){_X.MDI_Open('SI0101','공지사항','/bbs/selectBoardList.do?pcd=SI0101&bbsid=BBSMSTR_000000000001');});
}

// 업무보고 현황 링크
function getReprtLink(a_para) {
	var ls_data = Number(_X.XmlSelect("sm", "SM_AUTH_GROUP_USERS", "S_MAIN_01", new Array(mytop._CompanyCode, mytop._UserID), "array"));
	if(ls_data > 0) {
		_X.MDI_Open('SI0204','일지작성조회','/XG010.do?pcd=SI0204');
	}
	else {
		if(mytop._UserID=='ADMIN' || mytop._UserID=='admin' || mytop._UserID=='0973516' || mytop._UserID=='0434765' || mytop._UserID=='PM00' || mytop._UserID=='PM01' || mytop._UserID=='pm00' || mytop._UserID=='pm01') {
			_X.MDI_Open('SI0204','일지작성조회','/XG010.do?pcd=SI0204');
		}
		else {
			_X.MDI_Open('SI0202','일지작성조회','/XG010.do?pcd=SI0202&params='+a_para);
		}
	}
}

// 불편신고 접수 현황 링크
function getVOCLink(a_div,a_para) {
	return function() {
		_X.MDI_Link('FM0401','불편접수현황','/XG010.do?pcd=FM0401&params='+a_div+'@0'+a_para);
	}
}

// 사업장 현황 링크
function getSiteLink(a_para) {
	_X.MDI_Open('SM0214','사업장현황','/XG010.do?pcd=SM0214&params='+a_para);
}

// 퀵 메뉴 링크
function getQuick() {
	$("#QUICK1").css("cursor", "pointer").click(function(){window.open('http://www.autobus.co.kr', '_blank');});
	$("#QUICK2").css("cursor", "pointer").click(function(){_X.MDI_Open('FM0231','원격감시상황조회','/XG010.do?pcd=FM0231');});
	$("#QUICK3").css("cursor", "pointer").click(function(){_X.MDI_Open('SI0105','비상연락망','/XG010.do?pcd=SI0105');});
	$("#QUICK4").css("cursor", "pointer").click(function(){_X.MDI_Open('SI0105','비상연락망','/XG010.do?pcd=SI0105');});
	$("#QUICK5").css("cursor", "pointer").click(function(){_X.MDI_Open('SI0104','자산관리소식지','/bbs/selectGalleryList.do?pcd=SI0104&bbsid=BBSMSTR_000000000004');});
	$("#QUICK6").css("cursor", "pointer").click(function(){window.open('http://blumonte.amco.co.kr', '_blank');});
	$("#QUICK7").css("cursor", "pointer").click(function(){window.open('http://kdgym.hec.co.kr', '_blank');});
	$("#QUICK8").css("cursor", "pointer").click(function(){window.open('http://uwgym.hec.co.kr', '_blank');});
	$("#QUICK2").parent("td").parent("tr").css("display", "none");
	$("#QUICK4").parent("td").parent("tr").css("display", "none");
}

// 오늘 날짜 요일 구하기
function getToday() {
	var dt = new Date();
	var year = dt.getFullYear();
	var month = dt.getMonth()+1;
	var day = dt.getDate();
	var week = new Array('일', '월', '화', '수', '목', '금', '토');
	$('#TODAY').html("<img src='Theme/images/main/icon_date.png'/>&nbsp;&nbsp;" + month + "월 " + day + "일&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;" + week[dt.getDay()] + "요일");
}

// 오늘의 날씨 데이터 호출
function getWeather(zone_no, zone_area, zone_name) {
	var ls_url = "/APPS/si/jsp/WeatherAPI.jsp";
	var ls_param = "zone=" + zone_no; 
	$.ajax({
		type: 'POST',
		url : ls_url,
		data: ls_param,
		dataType: 'text',
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		async: true,
		cache: false,
		success: function(data) {
			var dataArr = data.replace(/\r/g, "").replace(/\n/g, "").split("\t");
			var dataIcon = "";
			if(dataArr[0]!=null && dataArr[0]!="") {
				$('#WEATHER0').html(zone_area + "&nbsp;&nbsp;|&nbsp;&nbsp;" + zone_name).addClass('w_bod2');
				$('#WEATHER1').html(dataArr[0] + "&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;기온 : " + dataArr[1] + "℃");
				$('#WEATHER2').html("습도 : " + dataArr[2] + "%&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;강수확률 : " + dataArr[3] + "%");
				if(dataArr[5]=='0') dataIcon = "0" + dataArr[4];
				else dataIcon = "1" + dataArr[5];
				$('#weather_icon').css("background", "url('Theme/images/weather/DBS" + dataIcon + ".png') 0 0 no-repeat").css("background-size", "cover");
			}
		},
		error: function(data, status, err) {
			// alert('오늘의 날씨 조회가 실패하였습니다.');
		}
	});
}

// 이미지 팝업
function popImage(imagesrc, winwidth, winheight) {
	var option = 'width=' + winwidth + ',height='+winheight;
	popwin = window.open("", "popImage", "scrollbars=no,toolbar=no,resizable=no," + option);
	popwin.document.open();
	popwin.document.write('<head><title>사업장 현황</title></head><body style="margin:0;padding:0"><img src="'+imagesrc+'" width="100%" height="100%" border="0"></body>');
	popwin.document.close();
}

// 공지사항 팝업
function popNotice(ntt_id) {
	var ls_data = _X.XmlSelect("si", "XG_COMTN_BBS", "R_WIDGET_05", new Array(mytop._CompanyCode, "BBSMSTR_000000000001", ntt_id), "array");
	var option = 'width=700,height=400';
	popwin = window.open("", "popNotice", "scrollbars=yes,toolbar=no,resizable=yes," + option);
	popwin.document.open();
	popwin.document.write('<head><title>공지사항</title><link rel="stylesheet" href="Theme/css/egovbbsTemplate.css" type="text/css"><link rel="stylesheet" href="Theme/css/bbs.css" type="text/css"></head><body style="margin:10px;padding:0"><table width="100%" cellpadding="8" class="table-search" border="0"><tbody><tr><td width="100%" class="title_left"><img src="/images/bbs/tit_icon.gif" width="3" height="16" hspace="3" style="vertical-align: middle" alt=""> &nbsp;공지사항 - 글조회</td></tr></tbody></table><table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="ffffff" class="generalTable"><tbody><tr><th width="15%" height="23" nowrap="" class="title_left">제목</th><td width="85%" colspan="5" nowrap="">'+ls_data[0][3]+'</td></tr><tr><th width="15%" height="23" nowrap="" class="title_left">작성자</th><td width="15%" class="lt_text3" nowrap="">'+ls_data[0][6]+'</td><th width="15%" height="23" nowrap="" class="title_left">작성시간</th><td width="15%" class="listCenter" nowrap="">'+ls_data[0][7]+'</td><th width="15%" height="23" nowrap="" class="title_left">조회수</th><td width="15%" class="listCenter" nowrap="">'+ls_data[0][8]+'</td></tr><tr><th height="23" class="title_left">글내용</th><td colspan="5"><div id="bbs_cn">'+ls_data[0][4].replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&amp;/g, "&")+'</div></td></tr></tbody></table></body>');
	popwin.document.close();
}

// 업무보고 팝업
function popNoReprt() {
	var ls_data = _X.XmlSelect("si", "XG_COMTN_BBS", "R_WIDGET_09", new Array(mytop._CompanyCode), "array");
	var option = 'width=500,height=300';
	popwin = window.open("", "popNoReprt", "scrollbars=yes,toolbar=no,resizable=yes," + option);
	popwin.document.open();
	popwin.document.write('<head><title>미보고 사업장 현황</title><link rel="stylesheet" href="/css/egovbbsTemplate.css" type="text/css"><link rel="stylesheet" href="/css/bbs.css" type="text/css"></head><body style="margin:10px;padding:0"><table width="100%" cellpadding="8" class="table-search" border="0"><tbody><tr><td width="100%" class="title_left"><img src="/images/bbs/tit_icon.gif" width="3" height="16" hspace="3" style="vertical-align: middle" alt=""> &nbsp;미보고 사업장 현황</td></tr></tbody></table><table width="100%" border="1" bordercolor="#c6c6c6" cellpadding="0" cellspacing="0" class="generalTable"><tbody><tr><th width="20%" height="23" nowrap="" class="title_left" style="text-align:center;">No</th><th width="80%" height="23" nowrap="" class="title_left" style="text-align:center;">사업장명</th></tr>');
	for(var i=0; i<ls_data.length; i++) {
		popwin.document.write('<tr><td height="23" nowrap="" style="text-align:center;">'+(i+1)+'</td><td height="23" nowrap="" style="padding:0 15px;">'+ls_data[i][0]+'</td></tr>');
	}
	popwin.document.write('</tbody></table></body>');
	popwin.document.close();
}

// 순회관리 팝업
function popCenter() {
	var url = '/APPS/si/jsp/Popup.jsp';
	var option = 'width=600,height=300';
	popwin = window.open(url, "popCenter", "scrollbars=yes,toolbar=no,resizable=yes," + option);
}

$(document).ready(function() {
	getReprt();
	getVOC();
	getCenter();
	getSite();
	getMap(1);
	getBoard();
	getQuick();
	getToday();
});

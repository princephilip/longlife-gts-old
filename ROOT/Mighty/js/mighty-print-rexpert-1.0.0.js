﻿function includeJavaScript(file) {     
  //var script   = document.createElement('script');   
  //script.src   = file;   
  //script.type  = 'text/javascript';   
  //script.defer = true;   
  //document.getElementsByTagName('head').item(0).appendChild(script);  
  document.write("<script type='text/javascript'  src='" + _web_context.path + file + "' charset='utf-8'></script>");
}  


/**
 * fn_name :  _X.Rexpert
 * Description : Rexpert report 를 설정하고 인쇄한다...
 * param : a_div:grid parent div id, a_id:grid id, a_width:width, a_height:height, 
 * 		   a_subSystem:단위시스템코드, a_layoutFile:gird column 속성파일, a_sqlFile:query 파일, 
 * 		   a_share:form html share 유무, a_tree:tree 유무, a_key:key column
 * Statements : 
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */

_X.SetRexpert = function (a_rptname, a_params, a_div, a_gbn, a_title) {	
	if(a_div == "" || a_div == null) a_div = '1';
	
	if(a_div == '1'){
		var oReport = GetfnParamSet();
		oReport.rptname = a_rptname;
		
		//oReport.con	= "Server1";		
		oReport.connectname= "sql1";		
		//oReport.dataset = "SQLDS1";
		
		// 레포트 파라메터
		
		for(var i=0;i<a_params.length; i++){
			oReport.param("asValue"+(i+1)).value = a_params[i];
		}
	
		oReport.open();
  }else{
  	
  	var ls_asValues = "";
  	
  	for(var i=0;i<a_params.length; i++){
			ls_asValues += "&asValue"+(i+1)+"=" + a_params[i];
		}
  
    // 무조건고정
    var ls_parttype = Base64.encode('7') ;
    ls_parttype = escape(Base64._utf8_encode(ls_parttype));
    
    // 사번
    
    if(mytop._UserID == 'ADMIN'){
    	var ls_empno = Base64.encode('0689763') ;
    }else{
    	var ls_empno = Base64.encode(mytop._UserID) ;
    }	
    ls_empno = escape(Base64._utf8_encode(ls_empno));
    
    // 결재번호
  	// 자재전자결재 구분 : 4000
  	//var ls_tag = '4000';
  	
  	var ls_tag = a_gbn;
  	var ls_date = _X.XmlSelect("com", "COMMON", "COMM_GETDATE", new Array(), "array")[0][0];
		var ls_num = _X.XmlSelect("com", "COMMON", "COMM_GETNUM", new Array(), "array")[0][0];
		var ls_filename = ls_date + _X.LPad(ls_num, 5, '0') +'_' +ls_tag;
		
		//키값만들기 ->base64 ->url인코딩 
		var ls_dockey = Base64.encode(ls_filename);
		ls_dockey = escape(Base64._utf8_encode(ls_dockey));
		
		var ls_parapath = Base64.encode(ls_filename+'.xml');
		ls_parapath = escape(Base64._utf8_encode(ls_parapath));
		
		var ls_wfdoctypeid = Base64.encode(ls_tag);
		ls_wfdoctypeid = escape(Base64._utf8_encode(ls_wfdoctypeid));
		
		//개발:1, 테스트 :2, 운영:3
		if(window.location.host == "epms.xinternet.co.kr"){
			var ls_gbn = '1';
		}else if(window.location.host == "epmsdev.hec.co.kr"){
			var ls_gbn = '2';
		}else if(window.location.host == "epms.hec.co.kr"){
			var ls_gbn = '3';
		}else{
			var ls_gbn = '1';
		}	

    //pdf파일생성
	  var dataPDF = "rebURL="+rex_gsReportURL+a_rptname+".reb"+"&fileName="+ls_filename+"&pdfgbn="+ls_gbn+ls_asValues;
		$.ajax({
	    type: "POST",
	    url : mytop._CPATH+"/APPS/com/jsp/RexESP.jsp",
	    data: dataPDF,
	    async: false
	  });
	
  	alert((a_title && "&xmlTitle="+a_title))
  	//xml 파일 생성
  	var ls_xmlName = "xmlName="+ls_filename+".xml&pdfName="+ls_filename+".pdf"+"&xmlgbn="+ls_gbn+ (a_title != null ? "&xmlTitle="+a_title : "");
  	$.ajax({
	    type: "POST",
	    url : mytop._CPATH+"/APPS/com/jsp/xmlCreate.jsp",
	    data: ls_xmlName,
	    async: false 
	  });
	  
	  //운영반영시 운영주소변경		
	  var ls_url = "http://wfdev.hec.co.kr/Application/Client/ClientMain.application?"+"parttype="+ls_parttype+"&empno="+ls_empno+"&dockey="+ls_dockey+"&parapath="+ls_parapath+"&wfdoctypeid="+ls_wfdoctypeid;
		w=window.open(ls_url,"approval","width=1px,height=1px, scrollbars=yes,left=1,top=1");
	  w.focus();
		
		return ls_filename;
  }	
}
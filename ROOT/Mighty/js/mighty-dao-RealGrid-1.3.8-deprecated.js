	function RealGrid_Deprecated(a_obj) {
		a_obj.save = a_obj.Save;
		a_obj.retrieve = a_obj.Retrieve;
		a_obj.reset = a_obj.Reset;
		a_obj.duplicateRow = a_obj.DuplicateRow;
		a_obj.deleteRow = a_obj.DeleteRow;
		a_obj.excelExport = a_obj.ExcelExport;
		a_obj.excelImport = a_obj.ExcelImport;
	  a_obj.setColumnProperty2 = a_obj.SetColumn;
	  a_obj.SetColumnProperty = a_obj.SetColumn;
		a_obj.rowCount = a_obj.RowCount;
		//a_obj.getRowCount = a_obj.rowCount;
		//a_obj.GetRowCount = a_obj.rowCount;
		
		a_obj.setRow = a_obj.SetRow;
		a_obj.getItem = a_obj.GetItem;
		a_obj.getItemString = a_obj.GetItem;
		a_obj.GetItemString = a_obj.GetItem;
		a_obj.getItemNumber = a_obj.GetItem;
		a_obj.GetItemNumber = a_obj.GetItem;
		a_obj.getFieldValues2 = a_obj.GetFieldValues;
		a_obj.ColumnNames = GetColumnNamesForConfig;
		a_obj.getColumnName = a_obj.GetColumnNameByIndex;
		a_obj.GetColumnName = a_obj.GetColumnNameByIndex;
		a_obj.getColName = a_obj.GetColumnNameByIndex;
		a_obj.GetColName = a_obj.GetColumnNameByIndex;
		a_obj.getColumnIndex = a_obj.GetColumnIndexByName;
		a_obj.GetColumnIndex = a_obj.GetColumnIndexByName;
		a_obj.getColIndex = a_obj.GetColumnIndexByName;
		a_obj.GetColIndex = a_obj.GetColumnIndexByName;
		a_obj.getColumnDataType = a_obj.GetColumnTypeByName;
		a_obj.getSQLData = a_obj.GetSQLData;
		
		a_obj.getChangedData = function(){
			a_obj.GridRoot().commit();
			var rows = a_obj.DataGrid().getAllStateRows();
			if(a_obj._DeletedRows.length<=0 && rows.updated.length<=0 && rows.created.length<=0)
				return null;
			
			var cData = [];
			//Delete
			for(var i=0; i<a_obj._DeletedRows.length; i++)
			{
				var rowData = {};
				rowData.job = "D";
				rowData.idx	= 0;
				rowData.data = a_obj._DeletedRows[i];
				cData.push(rowData);
			}
			//Update
			for(var i=0; i<rows.updated.length; i++)
			{
				var rowData = {};
				rowData.job = "U";
				rowData.idx	= (a_obj._IsTree?a_obj.GetRowIndex(rows.updated[i])-1:rows.updated[i]);
				rowData.data = a_obj.GridRoot().getRowData(rows.updated[i]);
				cData.push(rowData);
			}
			//Insert���� ����
			for(var i=0; i<rows.created.length; i++)
			{
				var rowData = {};
				rowData.job  = "I";
				rowData.idx	= (a_obj._IsTree?a_obj.GetRowIndex(rows.created[i])-1:rows.created[i]);
				rowData.data = a_obj.GridRoot().getRowData(rows.created[i]);
				cData.push(rowData);
			}
			return cData;
		};
		a_obj.GetChangedData = a_obj.getChangedData;
		
		a_obj.getRowState2 = function(row) {
			return a_obj.DataGrid().getRowState(row -1);
		};
		a_obj.GetRowState = a_obj.getRowState2;
		
		a_obj.setRowStates3 = function(a_rows, a_newstate, a_force, a_rowEvents) {
			if(a_newstate==null || a_newstate=="" || a_newstate=="undefined") a_newstate = "none";
			if(a_force==null || a_force=="" || a_force=="undefined") a_force = false;
			if(a_rowEvents==null || a_rowEvents=="" || a_rowEvents=="undefined") a_rowEvents = false;
		  
		  a_obj._DataGrid.setRowStates(a_rows, a_newstate, a_force, a_rowEvents);	
		};
		a_obj.SetRowStates3 = a_obj.setRowStates3;
		
		a_obj.setRowStates2 = function(a_oldstate, a_newstate, a_force) {
			if(a_newstate==null || a_newstate=="" || a_newstate=="undefined") a_newstate = "none";
			if(a_force==null || a_force=="" || a_force=="undefined") a_force = true;
			
			for(var i = 0; i < (typeof(a_oldstate) == "string" ? 1 : a_oldstate.length); i++) {
				var rows = a_obj._DataGrid.getStateRows(typeof(a_oldstate) == "string" ? a_oldstate : a_oldstate[i]);
		  		a_obj._DataGrid.setRowStates(rows, a_newstate, a_force);	
			}
		};
		a_obj.SetRowStates = a_obj.setRowStates2;
		
		a_obj.getStateRowsDiv = function(a_obj, a_div) {
			return a_obj.GetLengthByState(a_div);
		};
		a_obj.GetStateRowsDiv = a_obj.getStateRowsDiv;
		
		a_obj.ResetRowStates = a_obj.ResetRowsStates;
		a_obj.setCombo = a_obj.SetCombo;
		a_obj.treeChildRetrieve = a_obj.TreeChildRetrieve;
		a_obj.getRowId = a_obj.GetRowId;
		a_obj.getRowIndex = a_obj.GetRowIndex;
		a_obj.getParent = a_obj.GetParent;
		a_obj.getParentRowId = a_obj.GetParentRowId;
		a_obj.getChildren = a_obj.GetChildren;
		a_obj.getChildrenRowId = a_obj.GetChildrenRowId;
		a_obj.getDescendants = a_obj.GetDescendants;
		a_obj.getDescendantsRowId = a_obj.GetDescendantsRowId;
		a_obj.getAncestors  = a_obj.GetAncestors ;
		a_obj.getAncestorsRowId  = a_obj.GetAncestorsRowId;
		a_obj.treeExpand = a_obj.TreeExpand;
		a_obj.treeCollapse=a_obj.TreeCollapse;
		a_obj.treeExpandAll  = a_obj.TreeExpandAll;
		a_obj.treeCollapseAll  = a_obj.TreeCollapseAll;
		a_obj.getChildCount = a_obj.GetChildCount;
		a_obj.getDescendantCount = a_obj.GetDescendantCount;
		a_obj.addChildRow = a_obj.AddChildLevel;
		
		
		a_obj.setStyles2=function (styles) {
			a_obj.GridRoot().setStyles(styles);
	  };
	  a_obj.SetStyles= a_obj.setStyles2;
	    
	  a_obj.getStyles = function (region, all) {
	      all == (arguments.length > 1) ? all : true;
	      obj = a_obj.GridRoot().getStyles(region, all);
	      return obj;
	  };
	    
	  a_obj.clearStyles = function (region) {
	  	a_obj.GridRoot().clearStyles(region);
	  };
	  a_obj.ClearStyles = a_obj.clearStyles;
	    
	  a_obj.hasCellStyle =  function (id) {
	      return a_obj.GridRoot().hasCellStyle(id);
	  };
	  a_obj.HasCellStyle = a_obj.hasCellStyle;
	    
	  a_obj.addCellStyle2 = function (id, cellStyle, overwrite) {
	      overwrite = arguments.length > 2 ? overwrite : false;
	      a_obj.GridRoot().addCellStyle(id, cellStyle, overwrite);
	  };
	  a_obj.AddCellStyle = a_obj.addCellStyle2;
	
	  a_obj.addCellStyles2 = function (cellStyles, overwrite) {
	      overwrite = arguments.length > 1 ? overwrite : false;
	      a_obj.GridRoot().addCellStyles(cellStyles, overwrite);
	  };
	  a_obj.AddCellStyles = a_obj.addCellStyles2;
	    
	  a_obj.removeCellStyles = function (ids) {
	  	a_obj.GridRoot().removeCellStyles(ids);
	  };
	  a_obj.RemoveCellStyles  = a_obj.removeCellStyles;
	    
	  a_obj.removeAllCellStyles = function () {
	  	a_obj.GridRoot().removeAllCellStyles();
	  };
	  a_obj.RemoveAllCellStyles  = a_obj.removeAllCellStyles ;
			
	  a_obj.setCellStyle2 = function (dataRow, field, styleId) {
	  	a_obj.GridRoot().setCellStyle(dataRow, field, styleId);
	  };
	  a_obj.SetCellStyle  = a_obj.setCellStyle2;
	    
	  a_obj.setCellStyles2 = function (dataRows, fields, styleId) {
	  	a_obj.GridRoot().setCellStyles(dataRows, fields, styleId);
	  };
	  a_obj.SetCellStyles = a_obj.setCellStyles2;
	    
	  a_obj.setCellStyleRows = function (rows, fieldMap) {
	  	a_obj.GridRoot().setCellStyleRows(rows, fieldMap);
	  };
	  a_obj.SetCellStyleRows = a_obj.setCellStyleRows;
	  
	  a_obj.clearCellStyles = function () {
	  	a_obj.GridRoot().clearCellStyles();
	  };
	  a_obj.ClearCellStyles = a_obj.clearCellStyles;
	
		a_obj.getCells = function() {
			var sArr = a_obj.DataGrid().getSelectedCells();
		  return sArr;
		};
		a_obj.GetCells = a_obj.getCells;
	
		a_obj.setCells = function(rArr) {
		  a_obj.DataGrid().setSelectedCells(rArr);
		};
		a_obj.SetCells = a_obj.setCells;		
	}


	_X.RowCommit = function (a_obj) {
		return a_obj.GridCommit();
	};
	
	_X.RowCancel = function (a_obj) {
		return a_obj.GridCancel();
	};
		
	_X.GridInsertDatas2 = function(a_dg, a_datas, a_flag){
		a_dg.AppendRowsData( a_datas, a_flag );
	};
	
	_X.GridInsertDatas = function(a_dg, a_datas){
		a_dg._DataGrid.addRow(a_datas);
	};
	
	_X.SetColumnVisible = function (a_obj, a_column, a_flag, a_div) {
		a_obj.SetColumnVisible(a_column, a_flag, a_div);
	};	

	_X.CheckVisibleHandler = function (a_obj, a_flag, a_exclusive, a_head, a_showGroup, a_checkColor) {
		a_obj.SetCheckBar(a_flag, a_exclusive, a_head, a_showGroup, a_checkColor);
	};
	
	_X.GetCheckedRows = function (a_obj, a_flag) {
		return a_flag ? a_obj._GridRoot.getCheckedItems() : a_obj._GridRoot.getCheckedRows();
	};
	
	_X.SetCheckRows = function (a_obj, a_rowArr, a_flag) {
		return a_obj.GridRoot().checkRows(a_rowArr, a_flag);
	};
	
	_X.SetCheckRow = function (a_obj, a_row, a_flag, a_exclusive) {
		if(a_exclusive==null) a_exclusive=false;
		return a_obj.GridRoot().checkRow(a_row-1, a_flag, a_exclusive);
	};

	_X.CheckChildren =  function (a_obj, a_row, a_checked, a_recursive, a_visibleOnly) {
		if(a_checked==null) a_checked=true;
		if(a_recursive==null) a_recursive=false;
		if(a_visibleOnly==null) a_visibleOnly=false;
		a_obj.GridRoot().checkChildren(a_row-1, a_checked, a_recursive, a_visibleOnly);
	};
	
	_X.SetCheckAll = function (a_obj, a_flag) {
		a_obj.SetCheckAll(a_flag);
	};

	_X.IsCheckedRow = function (a_obj, a_row) {
		a_obj.IsCheckedRow(a_row);
	};

	_X.FootVisibleHandler = function (a_obj, a_flag) {
		a_obj.SetFooter(a_flag);
	};

	_X.SetColumnHeaderName = function (a_obj,a_column, a_name, a_div) {
		a_obj.SetColumnHeaderName(a_column, a_name, a_div);
	};	

	_X.SetColumnHeaderColor = function (a_obj,a_column, a_color, a_div) {
		a_obj.SetColumnHeaderColor(a_column, a_color, a_div);
	};

	_X.DgFindItem = function (ele, a_obj, a_fields) {
		a_obj.SetFocusByElement(ele, a_fields);
	}
	_X.DgFindItemValue = function (a_obj, a_fields_arr, a_values_arr) {
		a_obj.SetFocusByValue(a_fields_arr, a_values_arr);
	}
	
	_X.GridReadOnly = function (a_obj, a_flag) {
		a_obj.GridReadOnly(a_flag);
	};

	_X.GridAppendable = function (a_obj, a_flag) {
		a_obj.GridAppendable(a_flag);
	};

	_X.DgIsChanged = _X.IsDataChangedAll;

	_X.SetRowColor =   function(a_dg, a_col, a_val, a_color, a_comparison){
		a_dg.SetStyles({body: {dynamicStyles: [{criteria: ["values['" + a_col + "'] " + (a_comparison == null ? "=" : a_comparison) + " '" + a_val + "'"],styles: ["background=" + a_color]}]}});
	};

	_X.SetRowColors =   function(a_dg, a_col_arr, a_val_arr, a_color_arr, a_comparison_arr){
		var lo_criteria_arr = new Array();
		var lo_styles_arr = new Array();
		for(var i = 0; i < a_col_arr.length; i++) {
			lo_criteria_arr.push("values['" + a_col_arr[i] + "'] " + a_comparison_arr[i] + " '" + a_val_arr[i] + "'");
			lo_styles_arr.push("background=" + a_color_arr[i]);
		}
		a_dg.SetStyles({body: {dynamicStyles: [{criteria: lo_criteria_arr, styles: lo_styles_arr}]}});	
	};

	_X.SetOptions = function(a_dg, a_options){
		a_dg._GridRoot.setOptions(a_options);
	};
	
	_X.SetGridGroup = function(a_obj, a_columns, ab_foot) {
		a_obj.SetGroup(a_columns, ab_foot);
	};
/**
 * Object :  mighty-dao-1.2.0.js
 * @Description : Grid 자료저장 처리  JavaScript
 * @author :엑스인터넷정보 김양열
 * @since : 2013.02.10
 * @version : 1.2.6
 *
 * @Modification Information
 * <pre>
 *   since    	  author           Description
 *  ----------    --------    ---------------------------
 *   2013.02.10    김양열        최초생성
 */

/**
 * fn_name :  x_GridRowSync
 * Description : 동기화 칼럼 데이타 처리
 * param : a_obj:grid, a_row:row, a_col:column, a_val:동기화값
 * 		   (컬럼이 지정되지 않은 경우 전체....sync)
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
var x_GridRowSync = function (a_obj, a_row, a_col, a_val){
	_EventLog += "RS_" + a_row + " ";
	if(a_row==null){a_row = a_obj.GetRow();}
	//동기화 칼럼설정-성능 향상을 위해 최초로 필요한 경우에 설정 (2015.03.02 KYY)
	x_GridSetSyncInfo(a_obj);
	if(typeof(a_obj._SyncObject)!="undefined" && a_obj._SyncObject.length>0){
		if(a_col!=null){ // 칼럼하나씩
			//셀 동기화
			if(typeof(a_col)=="number")
				a_col = a_obj.GetColumnNameByIndex(a_col);
			for(var i=0; i<a_obj._SyncObject.length; i++){
				//성능이슈로 선택자를 변수로 처리(2015.03.01 KYY)
				var jSyncObj = $("#" + a_obj._SyncObject[i].id);
				if(a_obj._SyncObject[i].SyncColumn==a_col){
					//a_obj._SyncObject[i].value = a_val;
					//Mask관련 수정(2013.03.03 김양열)
					if (a_obj._SyncObject[i].SyncDataType=="datetime")
						jSyncObj.val(toDateString(a_val));
					else
						jSyncObj.val(a_val);
					//jSyncObj.val(a_val);
					if(typeof(a_obj._SyncObject[i].formatString)!="undefined"){
						a_obj._SyncObject[i].SetMaskValue();
					}
					// class 명으로 포멧 변경 - 박종현
					if(jSyncObj.hasClass('detail_amt')){
						jSyncObj.val(_X.FormatComma(a_val == 0 ? 0 : a_val,2));
					}else if(jSyncObj.hasClass('detail_date') || jSyncObj.hasClass('option_date') || jSyncObj.hasClass('option_date2') || jSyncObj.hasClass('search_date') || jSyncObj.hasClass('search_date2')){
						//프리폼에 NaN으로 나오지 않게 수정 (2014.09.18 이상규)
						var Cvalue = _X.Left(typeof(a_val)=="string" ? a_val : toDateString(a_val),10);
						jSyncObj.val(_X.ToString(Cvalue, "yyyy-mm-dd"));
					}else if(jSyncObj.hasClass('detail_float1')){
						jSyncObj.val(_X.FormatCommaPoint2(a_val == 0 ? 0 : a_val,1,2));
					}else if(jSyncObj.hasClass('detail_float2')){
						jSyncObj.val(_X.FormatCommaPoint2(a_val == 0 ? 0 : a_val,2,2));
					}else if(jSyncObj.hasClass('detail_float3')){
						jSyncObj.val(_X.FormatCommaPoint2(a_val == 0 ? 0 : a_val,3,2));
					}else if(jSyncObj.hasClass('detail_float4')){
						jSyncObj.val(_X.FormatCommaPoint2(a_val == 0 ? 0 : a_val,4,2));
					}else if(jSyncObj.hasClass('detail_cust')){
						jSyncObj.val(_X.CustMask(a_val == null ? '' : a_val));
					}else if(jSyncObj.hasClass('detail_register')){
						jSyncObj.val(_X.CustMask(a_val == null ? '' : a_val));
					//}else if(jSyncObj.hasClass('detail_zipcode')){
					//	jSyncObj.val(_X.ZipcodeMask(a_val == null ? '' : a_val));
					}

					//2014.06.10 refresh 안돼서 추가 ojkim1219
					if(jSyncObj.prop('type') == 'select-one'){
						//성능이슈로 확인요함(2015.03.10 KYY)
						//jSyncObj.selectBox('refresh');
						//jSyncObj.selectBox('destroy');
						//jSyncObj.selectBox();
					} else if(jSyncObj.attr('type') == "checkbox"){
						//jSyncObj.prettyCheckable(a_val=='Y' ? 'check' : 'uncheck');
						jSyncObj.prop("checked", a_val=='Y');
					}
				}
			}
		}
		else{
			var rowData = (a_row<=0 ? "" : a_obj.GetRowData(a_row));

			if(rowData == 'null' || rowData == null) return;

			for(var i=0; i<a_obj._SyncObject.length; i++){
				var jSyncObj = a_obj._SyncObjectJ[i];
				var colData  = rowData[a_obj._SyncObject[i].SyncColumn];
        switch (a_obj._SyncObject[i].SyncDataType) {
            case 'datetime':
            	//jSyncObj.val(a_row<=0 ? "":toDateString(colData));
            	a_obj._SyncObject[i].value = (a_row<=0 ? "":toDateString(colData));
              break;
            default :
            	//콤보 데이타의 경우 라벨값으로 싱크 (2015.10.23 이상규)
            	var column = a_obj.GetColumn( a_obj._SyncObject[i].id );
            	if ( column && column.comboData && a_obj._SyncObjectJ[i].prop('tagName') !== "SELECT" ) {
            		jSyncObj.val( column.comboData.syncLabel[ colData ] );
            	}
            	else {
            		jSyncObj.val(a_row<=0 ? "" :colData);
            	}
              break;
        }
				if(typeof(a_obj._SyncObject[i].formatString)!="undefined"){
					a_obj._SyncObject[i].SetMaskValue();
				}
				// class 명으로 포멧 변경 - 박종현
				if(jSyncObj.hasClass('detail_amt')){
					//컬럼 값이 undefined 일때 공백 처리 - (2014.09.17 이상규)
					var ls_val = (colData == null || isNaN(colData)) ? '' : _X.FormatComma(colData,2);
					jSyncObj.val(ls_val);
				}else if(jSyncObj.hasClass('detail_date') || jSyncObj.hasClass('option_date') || jSyncObj.hasClass('option_date2') || jSyncObj.hasClass('search_date') || jSyncObj.hasClass('search_date2')){
					var Cvalue = _X.Left(toDateString(colData),10);
					jSyncObj.val(_X.ToString(Cvalue, "yyyy-mm-dd"));
				}else if(jSyncObj.hasClass('detail_float1')){
					//컬럼 값이 undifined 일때 indexOf 에러 방지 처리 및 프리폼에서 공백 처리 - (2014.09.18 이상규)
					var ls_val = (colData == null || isNaN(colData)) ? '' : _X.FormatCommaPoint2(colData,1,2);
					jSyncObj.val(ls_val);
				}else if(jSyncObj.hasClass('detail_float2')){
					var ls_val = (colData == null || isNaN(colData)) ? '' : _X.FormatCommaPoint2(colData,2,2);
					jSyncObj.val(ls_val);
				}else if(jSyncObj.hasClass('detail_float3')){
					var ls_val = (colData == null || isNaN(colData)) ? '' : _X.FormatCommaPoint2(colData,3,2);
					jSyncObj.val(ls_val);
				}else if(jSyncObj.hasClass('detail_float4')){
					var ls_val = (colData == null || isNaN(colData)) ? '' : _X.FormatCommaPoint2(colData,4,2);
					jSyncObj.val(ls_val);
				}else if(jSyncObj.hasClass('detail_cust')){
					jSyncObj.val(_X.CustMask(colData == null ? '' : colData));
				}else if(jSyncObj.hasClass('detail_register')){
					jSyncObj.val(_X.CustMask(colData == null ? '' : colData));
				}
				//alert(jSyncObj.length + " : " + jSyncObj.val());
				//2014.06.10 refresh 안돼서 추가 ojkim1219
				if(jSyncObj.prop('type') == 'select-one'){
					//성능이슈로 확인요함(2015.03.10 KYY)
					//jSyncObj.selectBox('refresh');
					//jSyncObj.selectBox('destroy');
					//jSyncObj.selectBox();
				} else if(jSyncObj.attr('type') == "checkbox"){
					//jSyncObj.prettyCheckable(colData=='Y' ? 'check' : 'uncheck');
					jSyncObj.prop("checked", colData =='Y');
				}
			}
		}

		//성능이슈로 막아둠(2015.03.10)
		//$('select').selectBox('refresh');
	}
};

/**
 * fn_name :  x_IsAllDataChanged
 * Description :전체grid 변경유무
 * param : id : grid id
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
//
function x_IsAllDataChanged(){
	var sqlData = "";
	var rowCount = 0;
	var fileCnt = 0;

	for(var i=0; i<window._Grids.length; i++){
		if(window._Grids[i]!=null&&window._IsRetrieving) {
			if (window._Grids[i]._Updatable) {
				var cData = window._Grids[i].GetSQLData();
				if(cData!=null && cData!="")
					sqlData += (sqlData=="" ? "" : _Row_Separate) + cData;

				rowCount = rowCount + cData.length;
				fileCnt += Object.keys(window._Grids[i]._FileUploadInfo).length || 0;
			}
		}
	}

	return rowCount > 0 ? rowCount : fileCnt;
}

/**
 * fn_name :  x_SaveGridData
 * Description :Grid데이타 저장
 * param : MsgYn:저장후 메시지 유무, beforeSQL:xx, afterSQL:xx
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	   김양열      최초 생성
 *   2017.01.19      이상규      파일업로드 관련 로직 추가
 */
function x_SaveGridData(MsgYn, beforeSQL, afterSQL, beforeSqlData, afterSqlData){
	var sqlData = "";
	if(MsgYn == null || MsgYn == "") MsgYn = 'Y';

	var fileCnt     = 0;
	for(var i=0; i<window._Grids.length; i++){
		window._Grids[i].GridCommit();
		if (window._Grids[i]._Updatable) {
			var cData = window._Grids[i].GetSQLData();
			if(cData!=null && cData!="")
				sqlData += (sqlData=="" ? "" : _Row_Separate) + cData;

			fileCnt += Object.keys(window._Grids[i]._FileUploadInfo).length || 0;
		}
	}

	if((sqlData==null || sqlData=="") && fileCnt==0){
		if(MsgYn == 'Y'){
			_X.Noty("변경된 데이타가 존재하지 않습니다.",null,'danger',500);
			//_X.MsgBox("변경된 데이타가 존재하지 않습니다.");
		}
		return 0;
	}

	//beforeSqlData, afterSqlData 추가 (2017.01.20 이상규)
	if (beforeSqlData!=null) {
		sqlData = beforeSqlData + _Row_Separate + sqlData;
	}
	if (afterSqlData!=null) {
		sqlData = sqlData + _Row_Separate + afterSqlData;
	}

	//필수입력항목 체크
	for(var i=0; i<window._Grids.length; i++){
		if (window._Grids[i]._Updatable) {
			if(window._Grids[i].MustInputCheck()>0){
				return -1;
			}
		}
	}

	//문자열 길이 체크
	for(var i=0; i<window._Grids.length; i++){
		if (window._Grids[i]._Updatable) {
			if(window._Grids[i].ByteInputCheck()>0){
				return -1;
			}
		}
	}

	for(var i=0; i<window._Grids.length; i++){
		if (window._Grids[i]._Updatable) {
			window._Grids[i].ShowLoadingBar();
		}
	}

	//Session Timeout 처리(2015.03.27)
	//SQLInfo 관련 수정(2017.01.05 KYY)
	//var rowCnt = _X.SXD(window._Grids[0].subSystem, sqlData, beforeSQL, afterSQL);
	var rowCnt = 0;
	if(sqlData!=null && sqlData!=""){
		rowCnt = _X.SXD(window._Grids[0].subSystem + _Col_Separate + _PGM_CODE, sqlData, beforeSQL, afterSQL);
	}

	var fileCnt = 0;
	if (rowCnt != -1) {
		//filesave 추가, 하위로직 메시지도 변경 (2017.01.09 이상규)
		for(var i=0; i<window._Grids.length; i++){
			window._Grids[i].FileSave(function(cnt) {
				fileCnt += cnt;
			});

			window._Grids[i].RowFileDelete();
		}
	}

	for(var i=0; i<window._Grids.length; i++){
		if (window._Grids[i]._Updatable) {
			window._Grids[i].HideLoadingBar();
			if(rowCnt != -1) {
				window._Grids[i].ResetRowsStates();
			}
		}
	}

	if(rowCnt!=null && rowCnt>0){
		if(MsgYn == 'Y'){
			if(fileCnt){
				_X.Noty(rowCnt + "건의 데이터 변경작업이<br>정상적으로 이루어졌습니다.<br><br>" + fileCnt + "건의 첨부파일 변경작업이<br>정상적으로 이루어졌습니다.", null, "success", 500);
			} else {
				//_X.MsgBox(rowCnt + "건의 데이터 변경작업이 \r\n 정상적으로 이루어졌습니다.");
				//(message, title, alertType, delay)
				_X.Noty(rowCnt + "건의 데이터 변경작업이<br>정상적으로 이루어졌습니다.", null, "success", 500);
			}
		}
		return rowCnt;
	}

	if(fileCnt) {
		_X.Noty(fileCnt + "건의 첨부파일 변경작업이<br>정상적으로 이루어졌습니다.", null, "success", 500);
	}

	return rowCnt;
}

/**
 * fn_name :  x_GetGridPureData
 * Description : 주석, XML선언, 태그 사이 공백 등의 의미 없는 코드를 삭제
 * param : MsgYn:저장후 메시지 유무, beforeSQL:xx, afterSQL:xx
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
function x_GetGridPureData(a_obj, xmlData) {
	if(xmlData==null)
		return null;
	if(typeof(xmlData)=="object"){
		var colNames = a_obj.GetColumnNamesForConfig();
		var rtValue="";
		var cnt=0;
		for(var i=0; i<colNames.length; i++){
			if(colNames[i]!="No"&&colNames[i]!="NO"&&colNames[i]!="no"&&colNames[i]!="TREE") {
				if (typeof(xmlData[colNames[i]])=="object") {
					rtValue += (cnt==0?"":_Field_Separate) + toDateString(xmlData[colNames[i]]);
				} else if(typeof(xmlData[colNames[i]])=="undefined") {
					rtValue += (cnt==0?"":_Field_Separate) + "";
				} else {
					rtValue += (cnt==0?"":_Field_Separate) + xmlData[colNames[i]];
				}
//				rtValue += (cnt==0?"":_Field_Separate) + (typeof(xmlData[colNames[i]])=="object"?toDateString(xmlData[colNames[i]]):xmlData[colNames[i]]);
				cnt++;
			}
		}
		return rtValue;
	}else
		return xmlData.replace(/<(\?|\!-)[^>]*>/g,'').replace(/>\s+</g, '><').replace(/<\/[^>]*>|<[^<]*\/>/gi, _Field_Separate).replace(/<[^>]*>|/gi, '');
}

/**
 * fn_name :  toDateString
 * Description : javascript Date object convert to date string
 * param : a_date: 날짜객체
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
function toDateString (a_date) {
	if(a_date==null||a_date=="") return "";
	now = _X.ToDate(a_date);
	if (!now) return "";
	year = "" + now.getFullYear();
	month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
	day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
	hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
	minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
	second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
	return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
}


/**
 * fn_name :  x_GetGridCheckedKeys
 * Description : Grid ,체크박스 선택된 Row들의 키값을 DB에 저장하고 그 값을 배열로 리턴한다.
 * param :
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2017.03.08  	김양열          최초 생성
 */
function x_GetGridCheckedKeys (a_obj, a_keycols, a_save) {
	var checkedRows = a_obj.GetCheckedRows();
	if(checkedRows.length == 0) {
		return null;
	}
    //Row_Separate varchar2(2) :=  '。';
    //Col_Separate  varchar2(2) :=  ˚';

	var keydata = "";
	var dataArray = [];
	var paramNum = 0;
	if(a_save==null || a_save) {
		paramNum = _X.ToInt(_X.XmlSelect('sm', 'XG_GRID', 'PARAM_NUM', [], 'Array')[0][0]);
	 }
	$(checkedRows).each(function(idx,row){
		dataArray.push([]);
		if(keydata!="") {keydata += "。";}
		for(var i=0; i<a_keycols.length; i++){
			dataArray[dataArray.length -1].push(a_obj.GetItem(row, a_keycols[i]));
			keydata += a_obj.GetItem(row, a_keycols[i]) + (i!=0 ? "˚" : "");
		}
		if	(keydata.length >= 1000) {
	 		_X.EP("sm", "PROC_XM_PARAM_DATA", [paramNum, _PGM_CODE, mytop._UserID, keydata]);
	 		keydata = "";
	 	}
	});
	if((a_save==null || a_save) && keydata!="") {
	 	_X.EP("sm", "PROC_XM_PARAM_DATA", [paramNum, _PGM_CODE, mytop._UserID, keydata]);
	 }
 	return {NUM : paramNum, COUNT : checkedRows.length, DATA : dataArray };
}

_X.GetGridCheckedKeys = x_GetGridCheckedKeys;
/**
 * Object :  mighty-win-bootstrap-1.0.0.js
 * @Description : Mighty-X WIINDOW 관련 RESIZE 및 window event 제어
 * @author :엑스인터넷정보 김양열
 * @since : 2013.02.10
 * @version : 1.2.0
 *
 * @Modification Information
 * <pre>
 *   since        author           Description
 *  ----------    --------    ---------------------------
 *   2013.02.10    김양열        최초생성
 */


/**
 * fn_name :  xm_BodyResize
 * Description : page resize 처리
 * param : a_init : resize 유무(true:false)
 * Statements :
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
 */
var xm_BodyResize = function(a_init){
  if( $("#main-mdi-content", parent.document.body).width() != null ) {
    var win_width   = $("#main-mdi-content", parent.document.body).width(); //$(window).width();
    var win_height  = $("#main-mdi-content", parent.document.body).height(); //$(window).height();
    win_width = win_width - ( $(".x-mdi-wrap", parent.document.body).css("padding-left").replace("px", "") * 2 );
    win_height = win_height - ( $(".x-mdi-wrap", parent.document.body).css("padding-top").replace("px", "") * 2 );

    if(a_init==null && win_width == _Pre_Win_Width && win_height == _Pre_Win_Height) {
      return;
    }

    _Pre_Win_Width  = win_width;
    _Pre_Win_Height = win_height;

    $("#pageLayout").width(win_width).height(win_height);

    xm_DivResize(win_width, win_height);
    //xm_DivResize($("#pageLayout")[0], win_width, win_height, a_init);

    //Input 요소들 처리
    //var cInputs = $(".detail_input,.option_input");
    //for(var i=0; i<cInputs.length; i++) {
    //  var pWidth = $(cInputs[i]).parent().innerWidth();
    //  $(cInputs[i]).outerWidth(pWidth -0, true);
    //}

    if(typeof(xe_BodyResize)!="undefined") {
      xe_BodyResize(a_init);
    }
  } else {
    $("#pageLayout").css("overflow-y", "auto");
  }
};

/**
 * fn_name :  xm_DivResize
 * Description : page divs object resize
 * param : a_pobj : page div, a_width : page width
 *       a_height : page height, a_init : 사이즈변경유무
 * Statements :
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
 */
//var xm_DivResize = function(a_pobj, a_width, a_height, a_init){
var xm_DivResize = function(a_width, a_height){
  // height 계산 : x5-h-fill ie8 호환 때문에 계산로직 처리
  // dom level 순으로 정렬
  var hobjFill = $(".x5-h-fill").sort(function(a, b) {
    var x = $(a).parents().length; var y = $(b).parents().length;
    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
  });

  hobjFill.each(function(idx) {
    xm_ResizePanelHeight( $(this).parent() );
  });

  var wobjFill = $(".x5-w-fill").sort(function(a, b) {
    var x = $(a).parents().length; var y = $(b).parents().length;
    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
  });

  wobjFill.each(function(idx) {
    xm_ResizePanelWidth( $(this).parent() );
  });
};


/**
 * fn_name :  xm_ResizePanel
 * Description : x5-panel child elements resize
 * param : a_div : target div
 * Statements :
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2015.09.17   배장훈          최초 생성
 */
var xm_ResizePanelHeight = function(a_obj) {
  var parent_h = $(a_obj).height();
  var fix_h = 0;
  //var patt = /x5-h-(fix|rate)/;
  var patt = /x5-h-(\d+|rate)/;
  var fillObj = [];

  $(a_obj).children("div,h3").each(function( index ) {
    if( patt.test($(this).attr("class")) ) {
      fix_h = fix_h + $(this).height();
    } else if($(this).hasClass("x5-h-fill")) {
      fillObj.push($(this));
    } else {
      //아무것도 없을 때는 fix 로 동작
      fix_h = fix_h + $(this).height();
    }
  });

  if(fillObj.length>0) {
    var fill_h = parseInt((parent_h - fix_h)/fillObj.length);

    $(fillObj).each(function(idx) {
      $(this).height(fill_h);
    });
  }
}

var xm_ResizePanelWidth = function(a_obj) {
  var parent_w = $(a_obj).width();
  var fix_w = 0;
  var patt = /x5-w-(\d+|rate)/;
  var fillObj = [];

  $(a_obj).children("div").each(function(index) {
    if( patt.test($(this).attr("class")) ) {
      fix_w = fix_w + $(this).width();
    } else if($(this).hasClass("x5-w-fill")) {
      fillObj.push($(this));
    } else {
      fix_w = fix_w + $(this).height();
    }
  });

  if(fillObj.length>0) {
    var fill_w = parseInt((parent_w - fix_w)/fillObj.length);

    $(fillObj).each(function(idx) {
      $(this).width(fill_w);
    });
  }
}

/**
 * fn_name :  xm_InputInitial
 * Description :object style  적용
 * param :
 *
 * Statements :
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
 */
var xm_InputInitial = function(){
  //date style  적용
  $(".detail_box,.option_input_bg").find(".detail_date,.option_date,.option_date2,.search_date,.search_date2").datepicker({
         changeMonth: true,
         changeYear: true,
         dayNames: ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'],
         dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
         monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'],
         monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
         nextText: '다음 달',
         prevText: '이전 달',
         showButtonPanel: true,
         currentText: '오늘 날짜',
         closeText: '닫기',
         dateFormat: "yy-mm-dd",

         showMonthAfterYear: true,
         yearSuffix: ' 년 ',
         monthSuffix: ' 월',
         showOtherMonths: true, // 나머지 날짜도 화면에 표시
         selectOtherMonths: true,
         yearRange: 'c-50:c+50' //년도범위 추가(2017.09.18 KYY)

  });

  $(".detail_box,.option_input_bg").find(".detail_date,.option_date,.option_date2,.search_date,.search_date2").each(function (idx, elem) {
        if ($(elem).is(":input"))
          this.value = (this.value.length==8?this.value=_X.ToString(this.value, "yyyy-mm-dd"):this.value);
          $(elem).change(function(event) {;$(elem).val(_X.ToString(this.value, "yyyy-mm-dd"));if(!_X.ChkDate(this.value))$(elem).val("");});
     });

  $(".detail_box,.option_input_bg").find(".detail_yymm,.option_yymm,.option_yymm2,.search_yymm,.search_yymm2").each(function (idx, elem) {
        if ($(elem).is(":input"))
          this.value = (this.value.length==6?this.value=_X.ToString(this.value+'01', "yyyy-mm"):this.value);
          $(elem).change(function(event) {;$(elem).val(_X.ToString(this.value+'01', "yyyy-mm"));});
     });

  //버튼 jQueryUI 작업
  //$( "input[type=button], a, button" )
  $(".ui-button").not(".ui-button-icon-only")
    //.css({'font-size': '9pt','padding':'0px 0px 0px 0px','height':'21px','font-family':'맑은 고딕, Malgun Gothic' })
    .css({'font-size': '12px','padding':'0px 0px 0px 0px','height':'22px'})
    .removeClass('ui-corner-all')
    .hover(
            function () {
                $(this).addClass('ui-state-hover');
            },
            function () {
                $(this).removeClass('ui-state-hover');
            }
        )
    .click(function( event ) {
      event.preventDefault();
  });

  //Checkbox 스타일 적용
/*  var chkObjs = $(".detail_box,.option_input_bg").find('input:checkbox,input:radio');
  for(var i=0; i<chkObjs.length; i++) {
    $(chkObjs[i]).prettyCheckable();
    $(chkObjs[i])[0].check = function () {$(this).prop('checked', true).attr('checked', true).parent().find('a:first').addClass('checked'); xm_InputChanged(this);};
    $(chkObjs[i])[0].uncheck = function () {$(this).prop('checked', false).attr('checked', false).parent().find('a:first').removeClass('checked'); xm_InputChanged(this);};
    $(chkObjs[i])[0].enable = function () {$(this).removeAttr('disabled').parent().find('a:first').removeClass('disabled');};
    $(chkObjs[i])[0].disable = function () {$(this).attr('disabled', 'disabled').parent().find('a:first').addClass('disabled');};
  };*/

  //Select 스타일 적용
  //속도가 느림 다른걸로 변경해야 함(2015.03.10 KYY)
  //$('select').selectBox();

  // mask - 박종현
  $(".detail_box,.option_input_bg").find(".detail_date .option_date .option_date2 .search_date .search_date2").inputmask("date", {yearrange: { minyear: 1900, maxyear: 9999 }});
  //$(".option_date").inputmask("date", {yearrange: { minyear: 1900, maxyear: 9999 }});
  //$(".option_date2").inputmask("date", {yearrange: { minyear: 1900, maxyear: 9999 }});
  //$(".search_date").inputmask("date", {yearrange: { minyear: 1900, maxyear: 9999 }});
  //$(".search_date2").inputmask("date", {yearrange: { minyear: 1900, maxyear: 9999 }});
  $(".detail_box,.option_input_bg").find(".detail_hhmm").inputmask("hh:mm");//(2015.01.27 이상규)

  $(".detail_box,.option_input_bg").find(".detail_amt").inputmask("decimal", { radixPoint: ".",  autoGroup: true, groupSeparator: ",", digits: 0, repeat: 16});
  $(".detail_box,.option_input_bg").find(".detail_float1").inputmask("decimal", { radixPoint: ".", autoGroup: true, groupSeparator: ",", digits: 1, repeat: 15 });
  $(".detail_box,.option_input_bg").find(".detail_float2").inputmask("decimal", { radixPoint: ".", autoGroup: true, groupSeparator: ",", digits: 2, repeat: 14 });
  $(".detail_box,.option_input_bg").find(".detail_float3").inputmask("decimal", { radixPoint: ".", autoGroup: true, groupSeparator: ",", digits: 3, repeat: 13 });
  $(".detail_box,.option_input_bg").find(".detail_float4").inputmask("decimal", { radixPoint: ".", autoGroup: true, groupSeparator: ",", digits: 4, repeat: 12 });

  $(".detail_box,.option_input_bg").find(".detail_cust").inputmask("999-99-99999",{placeholder:" ", clearMaskOnLostFocus: true });
  $(".detail_box,.option_input_bg").find(".detail_register").inputmask("999999-9999999",{placeholder:" ", clearMaskOnLostFocus: true });
  $(".detail_box,.option_input_bg").find(".detail_zipcode").inputmask("999-999",{placeholder:" ", clearMaskOnLostFocus: true });

  //이벤트 처리
  $(".form-group").delegate('input,textarea,select','keydown', xm_InputKeyDown);
  $(".form-group").delegate('input,textarea,select','change', xm_InputChanged);
  $(".form-group").delegate('input,textarea,select','click', xm_InputClicked);
  $(".form-group").delegate('input,textarea,select','focus', xm_InputFocused);

};



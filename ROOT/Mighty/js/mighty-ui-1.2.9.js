/**
 * Object :  mighty-ui-1.1.0.js
 * @Description : Mighty-X UI 관련 javascript libaray
 * @author :엑스인터넷정보 김양열
 * @since : 2013.02.10
 * @version : 1.2.7
 *
 * @Modification Information
 * <pre>
 *   since        author           Description
 *  ----------    --------    ---------------------------
 *   2013.02.10    김양열        최초생성
 *   2014.11.20    박영찬        IE8 오루수정 및 일부 기능 taglib 로 변경
 */

var isIE  = (navigator.appVersion.indexOf("MSIE") != -1||navigator.appVersion.indexOf("Trident") != -1) ? true : false;
var isWin = (navigator.appVersion.toLowerCase().indexOf("win") != -1) ? true : false;
var isOpera = (navigator.userAgent.indexOf("Opera") != -1) ? true : false;

if (!isIE) {
  window.showModalDialog = function (showModalDialog) {
    return function (url, name, features) {
      // some browsers dont support the call() method so call the method directly in such cases
      return open.call ?
        open.call(window, url, name, features)
        : open( url, name, features);
      };
    }(window.showModalDialog);
}

/**
 * fn_name :  _X.SetHTML
 * Description : html object 에 tag script 을 대입
 * param : a_obj : html object, a_html = String
 * Statements :
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
 */
_X.SetHTML = function(a_obj, a_html) {
  a_obj.innerHTML = a_html;
  if(_Eval!="")
  {
    eval(_Eval);
    _Eval = "";
  }
};

/**
 * fn_name :  InitChart
 * Description : chart 초기화
 * param : chartInfo : object(chart 의 설정정보)
 * Statements :
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2015.11.16   배장훈          최초 생성
 */
_X.DrawChart = function (chartInfo) {
  var chartObj = new Object();
  var chartCfg = new Object();

  if(chartInfo.vendor=="morris") {
    if(chartInfo.chartType=="donut") {
      chartCfg.element = chartInfo.element;
      chartCfg.data = chartInfo.data;
      //도넛 차트에서 센터 텍스트에 포맷터 적용하기 위해 속성 - (2016.03.15 박찬호)
      chartCfg.formatter = chartInfo.formatter;
      chartCfg.resize = (!chartInfo.resize?true:chartInfo.resize);
      if(chartInfo.Colors) chartCfg.colors = chartInfo.Colors;

      chartObj = Morris.Donut(chartCfg);
    } else {
      chartCfg.element = chartInfo.element;
      chartCfg.data = chartInfo.data;
      chartCfg.xkey = chartInfo.key_x;
      chartCfg.ykeys = chartInfo.keys_y;
      chartCfg.labels = chartInfo.titles_y;
      chartCfg.resize = (!chartInfo.resize?true:chartInfo.resize);
      chartCfg.fillOpacity = (!chartInfo.fillOpacity?0.6:chartInfo.fillOpacity);
      chartCfg.hideHover = (!chartInfo.hideHover?'auto':chartInfo.hideHover);
      chartCfg.behaveLikeLine = (!chartInfo.behaveLikeLine?true:chartInfo.behaveLikeLine);
      chartCfg.pointFillColors = (!chartInfo.pointFillColors?['#ffffff']:chartInfo.pointFillColors);
      chartCfg.pointStrokeColors = (!chartInfo.pointStrokeColors?['#000000']:chartInfo.pointStrokeColors);
      //morris 차트 속성추가(x축텍스트크기,바너비,바사이 갭(빈공간) 조정속성)-(2016.03.02 박찬호)
      chartCfg.gridTextSize = (!chartInfo.gridTextSize?'10px':chartInfo.gridTextSize);
      chartCfg.barSizeRatio = (!chartInfo.barSizeRatio?0.75:chartInfo.barSizeRatio);
      chartCfg.barGap = (!chartInfo.barGap?3:chartInfo.barGap);
      chartCfg.xLabelMargin = (!chartInfo.xLabelMargin?1:chartInfo.xLabelMargin);



      switch(chartInfo.chartType) {
        case "bar" :
          if(chartInfo.Colors) chartCfg.barColors = chartInfo.Colors;
          chartObj = Morris.Bar(chartCfg);
          break;
        case "line" :
          chartCfg.parseTime = (!chartInfo.parseTime?false:chartInfo.parseTime);
          if(chartInfo.Colors) chartCfg.lineColors = chartInfo.Colors;
          chartObj = Morris.Line(chartCfg);
          break;
        case "area" :
          chartCfg.parseTime = (!chartInfo.parseTime?false:chartInfo.parseTime);
          if(chartInfo.Colors) chartCfg.areaColors = chartInfo.Colors;
          chartObj = Morris.Area(chartCfg);
          break;
        case "stacked" :
          chartCfg.stacked = true;
          if(chartInfo.Colors) chartCfg.barColors = chartInfo.Colors;
          chartObj = Morris.Bar(chartCfg);
          break;
      }
    }
  }

  return chartObj;
};

/**
 * fn_name :  Chart
 * Description : any chart 초기값을 설정한다.
 * param : chartInfo : object(chart 의 설정정보)
 * Statements :
 *
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
 */
_X.Chart = function (chartInfo) {
  var chartObj = new AnyChart('/Mighty/swf/AnyChart.swf', '/Mighty/swf/Preloader.swf');
  chartObj.id = chartInfo.id;
  chartObj.width = chartInfo.width;
  chartObj.height = chartInfo.height;
  chartObj.wMode = "opaque";
  chartObj._Title = chartInfo.title;
  chartObj._Title_X = chartInfo.title_x;
  chartObj._Title_Y = chartInfo.title_y;
  chartObj._ChartURL = '/Mighty/Xml/Chart/' + chartInfo.layoutfile + '.jsp';

  var xe_P_ChartPointClick = function(event){xe_M_ChartPointClick(a_obj, event);};
  var xe_P_ChartPointSelect = function(event){xe_M_ChartPointSelect(a_obj, event);};
  var xe_P_ChartPointMouseOver = function(event){xe_M_ChartPointMouseOver(a_obj, event);};
  var xe_P_ChartPointMouseOut = function(event){xe_M_ChartPointMouseOut(a_obj, event);};

  chartObj.addEventListener('pointClick', xe_P_ChartPointClick);
  chartObj.addEventListener('pointSelect',xe_P_ChartPointSelect);
  chartObj.addEventListener('pointMouseOver',xe_P_ChartPointMouseOver);
  chartObj.addEventListener('pointMouseOut',xe_P_ChartPointMouseOut);

  chartObj.setXMLFile(chartObj._ChartURL + '?tt=' + encodeURIComponent(encodeURIComponent(chartInfo.title)) + '&tx=' + encodeURIComponent(encodeURIComponent(chartInfo.title_x)) + '&ty=' + encodeURIComponent(encodeURIComponent(chartInfo.title_y)));
  chartObj.write(chartInfo.div);
  chartObj.refresh();

  chartInfo.sqlfiles = chartInfo.sqlfile.split('|');

  $("#" + chartInfo.id)[0].subSystem = chartInfo.subsystem;
  $("#" + chartInfo.id)[0].sqlFile = chartInfo.sqlfiles[0];
  $("#" + chartInfo.id)[0].sqlKey = chartInfo.sqlfiles[1];

  _X.ChartInit($("#" + chartInfo.id)[0], chartInfo.div, chartObj);
};

/**
 * fn_name :  ChartInit
 * Description : chart 객체의 기능을 부여(reset, 데이타 로드 등)
 * param : a_obj : chart 객체, a_div : chart div id, a_chart : chart type
 * Statements :
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
 */
_X.ChartInit = function (a_obj, a_div, a_chart) {
  var _ChartIndex = window._Charts.length;
  window._Charts[_ChartIndex] = a_obj;

  a_obj._Div  = a_div;
  a_obj.Chart = a_chart;
  a_obj._Info = null;
  a_obj.Refresh = a_obj.Chart.refresh;
  a_obj.Info = function() {if(a_obj._Info==null){a_obj._Info=a_obj.Chart.getInformation();} return a_obj._Info;};

  a_obj.reset = function (a_refresh){
    var info = a_obj.Info();
    for(var i=info.Series.length -1; i>=0; i--){
      a_obj.RemoveSeries(info.Series[i].ID);
    }
    if(a_refresh==null || a_refresh)
      a_obj.Refresh();

  };

  a_obj.retrieve = function (args) {
    a_obj.Chart.setXMLFile(a_obj.Chart._ChartURL + '?tt=' + encodeURIComponent(encodeURIComponent(a_obj.Chart._Title)) + '&tx=' + encodeURIComponent(encodeURIComponent(a_obj.Chart._Title_X)) + '&ty=' + encodeURIComponent(encodeURIComponent(a_obj.Chart._Title_Y)) + '&ss=' + a_obj.subSystem + '&xf=' + a_obj.sqlFile + '&xk=' + a_obj.sqlKey + '&ps=' + _X.ArrayToStr(args,','));
  };
  a_obj.Retrieve = a_obj.retrieve;

  //a_obj.chart = a_obj.Chart
  a_obj.info = a_obj.Info;
  a_obj.refresh = a_obj.Refresh;
};

  //(시작)울트라버전과의 호한성을 위해 추가(2015.08.24 KYY) ===============================================================
  _X.Buttons = function(a_pgm_cd)
  {
    if(mytop._MyPgmList==null || mytop._MyPgmList[a_pgm_cd]==null){
      return;
    }
    var pgm = mytop._MyPgmList[a_pgm_cd];
    return (pgm.UPDATE_YN=="Y" && pgm.UPDATE_AUTH=="Y" ? _X.Button("xBtnSave", "x_DAO_Save()", "/images/btn/x_save") : "")
         + (pgm.QUERY_YN=="Y" ? _X.Button("xBtnRetrieve", "x_DAO_Retrieve()", "/images/btn/x_retrieve") : "")
         + (pgm.UPDATE_YN=="Y" && pgm.INSERT_YN=="Y" && pgm.UPDATE_AUTH=="Y" && pgm.INSERT_AUTH=="Y" ? _X.Button("xBtnInsert", "x_DAO_Insert()", "/images/btn/x_insert") : "")
         + (pgm.UPDATE_YN=="Y" && pgm.APPEND_YN=="Y" && pgm.UPDATE_AUTH=="Y" && pgm.INSERT_AUTH=="Y" ? _X.Button("xBtnAppend", "x_DAO_Insert(0)", "/images/btn/x_append") : "")
         + (pgm.UPDATE_YN=="Y" && pgm.MULTI_YN=="Y" && pgm.UPDATE_AUTH=="Y" ? _X.Button("xBtnDuplicate", "x_DAO_Duplicate()", "/images/btn/x_duplicate") : "")
         + (pgm.UPDATE_YN=="Y" && pgm.DELETE_YN=="Y" && pgm.UPDATE_AUTH=="Y" && pgm.DELETE_AUTH=="Y" ? _X.Button("xBtnDelete", "x_DAO_Delete()", "/images/btn/x_delete") : "")
         + (pgm.EXCEL_YN=="Y" && pgm.EXCEL_AUTH=="Y" ? _X.Button("xBtnExcel", "x_DAO_Excel()", "/images/btn/x_excel") : "")
         + (pgm.PRINT_YN=="Y" && pgm.PRINT_AUTH=="Y" ? _X.Button("xBtnPrint", "x_DAO_Print()", "/images/btn/x_print") : "")
         + _X.Button("xBtnClose", "x_Close()", "/images/btn/x_close")
         ;
  };

  _X.CssButton = function(btnClass, btnId, btnAction, btnTitle)
  {
    return '<li><a class="' + btnClass + '" id="' + btnId + '" href="javascript:;" onclick="' + btnAction + ';" title="' + btnTitle + '"></a></li>';
  }

  _X.MainButtons = function(a_pgm_cd, a_dg)
  {
    if(mytop._MyPgmList==null || mytop._MyPgmList[a_pgm_cd]==null){
      return;
    }
    var pgm = mytop._MyPgmList[a_pgm_cd];
    var btnHtml = '<ul id="buttons">';

    btnHtml += (pgm.UPDATE_YN=="Y" && pgm.UPDATE_AUTH=="Y" ? _X.CssButton("save", "xBtnSave", "x_DAO_Save(" + a_dg + ")", "[Ctrl+S] 변경된 데이타를 저장합니다.") : "")
         + (pgm.QUERY_YN=="Y" ? _X.CssButton("retrieve", "xBtnRetrieve", "x_DAO_Retrieve(" + a_dg + ")", "[Ctrl+R] 데이타를 다시 조회합니다.") : "")
         + (pgm.UPDATE_YN=="Y" && pgm.INSERT_YN=="Y" && pgm.UPDATE_AUTH=="Y" && pgm.INSERT_AUTH=="Y" ? _X.CssButton("insert", "xBtnInsert", "x_DAO_Insert(" + a_dg + ")", "[Ctrl+I] 신규 데이타를 삽입합니다.") : "")
         + (pgm.UPDATE_YN=="Y" && pgm.APPEND_YN=="Y" && pgm.UPDATE_AUTH=="Y" && pgm.INSERT_AUTH=="Y" ? _X.CssButton("append", "xBtnAppend", "x_DAO_Insert(" + a_dg + ",0)", "[Ctrl+A] 신규 데이타를 추가합니다.") : "")
         + (pgm.UPDATE_YN=="Y" && pgm.MULTI_YN=="Y" && pgm.UPDATE_AUTH=="Y" ? _X.CssButton("duplicate", "xBtnDuplicate", "x_DAO_Duplicate(" + a_dg + ")", "[Ctrl+D] 선택된 데이타를 복제합니다.") : "")
         + (pgm.UPDATE_YN=="Y" && pgm.DELETE_YN=="Y" && pgm.UPDATE_AUTH=="Y" && pgm.DELETE_AUTH=="Y" ? _X.CssButton("delete", "xBtnDelete", "x_DAO_Delete(" + a_dg + ")", "[Ctrl+Del] 선택된 데이타를 삭제합니다.") : "")
         + (pgm.EXCEL_YN=="Y" && pgm.EXCEL_AUTH=="Y" ? _X.CssButton("excel", "xBtnExcel", "x_DAO_Excel(" + a_dg + ")", "[Ctrl+E] 엑셀로 데이타를 Export합니다.") : "")
         + (pgm.PRINT_YN=="Y" && pgm.PRINT_AUTH=="Y" ? _X.CssButton("print", "xBtnPrint", "x_DAO_Print(" + a_dg + ")", "[Ctrl+P] 출력물을 미리보기합니다.") : "")
         ;
    btnHtml += _X.CssButton("close", "xBtnClose", "x_Close()", "[Ctrl+F4] 윈도우 화면을 닫습니다.");
    btnHtml += "</ul>";
    return btnHtml;
  };

  _X.ChildButtons = function(a_pgm_cd, a_dg, a_save, a_retrieve, a_insert, a_append, a_duplicate, a_delete, a_excel, a_print)
  {
    if(mytop._MyPgmList==null || mytop._MyPgmList[a_pgm_cd]==null){
      return;
    }
    var pgm = mytop._MyPgmList[a_pgm_cd];
    return (pgm.UPDATE_YN=="Y" && pgm.UPDATE_AUTH=="Y" && a_save ? _X.Button("xBtnSave_" + a_dg, "x_DAO_Save(" + a_dg + ")", "/images/btn/x_save") : "")
         + (a_retrieve  ? _X.Button("xBtnRetrieve_" + a_dg, "x_DAO_Retrieve(" + a_dg + ")", "/images/btn/x_retrieve") : "")
         + (a_insert && pgm.UPDATE_AUTH=="Y" && pgm.INSERT_AUTH=="Y" ? _X.Button("xBtnInsert_" + a_dg, "x_DAO_Insert(" + a_dg + ")", "/images/btn/x_insert") : "")
         + ( a_append && pgm.UPDATE_AUTH=="Y" && pgm.INSERT_AUTH=="Y" ? _X.Button("xBtnAppend_" + a_dg, "x_DAO_Insert(" + a_dg + ",0)", "/images/btn/x_append") : "")
         + (a_duplicate && pgm.UPDATE_AUTH=="Y" ? _X.Button("xBtnDuplicate_" + a_dg, "x_DAO_Duplicate(" + a_dg + ")", "/images/btn/x_duplicate") : "")
         + (a_delete && pgm.UPDATE_AUTH=="Y" && pgm.DELETE_AUTH=="Y" ? _X.Button("xBtnDelete_" + a_dg, "x_DAO_Delete(" + a_dg + ")", "/images/btn/x_delete") : "")
         + (a_excel && pgm.EXCEL_AUTH=="Y" ? _X.Button("xBtnExcel", "x_DAO_Excel(" + a_dg + ")", "/images/btn/x_excel") : "")
         + (a_print && pgm.PRINT_AUTH=="Y" ? _X.Button("xBtnPrint", "x_DAO_Print(" + a_dg + ")", "/images/btn/x_print") : "")
         ;
  };


  _X.Button = function(btnId, btnAction, imgName, imgDir, a_imgwidth, a_imgheight, a_str, a_bullet)
  {
    if(a_bullet==null) a_bullet=false;

    return "<img src=\"" + imgName + ".gif\" border=0 align=\"absmiddle\" vspace=0 hspace=0 name=\"" + btnId + "\""
           + " OnMouseOver=\"if(this.style.visibility!='hidden'){this.src='" + imgName + "_over.gif';}\""
           + " OnMouseOut =\"if(this.style.visibility!='hidden'){this.src='" + imgName + ".gif';}\""
           + " OnMouseUp=\"if(this.style.visibility!='hidden'){this.src='" + imgName + "_over.gif';" + btnAction + ";}\""
           + " OnMouseDown=\"if(this.style.visibility!='hidden'){this.src='" + imgName + "_down.gif';}\""
           + (a_imgwidth!=null?" width=" + a_imgwidth:"")
           + (a_imgheight!=null?" height=" + a_imgheight:"")
           + " style=\"cursor:pointer;margin-left:2\" onfocus=\"this.blur()\" tabIndex=-1>";
  };

  function x_SetImg(a_obj, a_src)
  {

  }
  //(끝)울트라버전과의 호한성을 위해 추가(2015.08.24 KYY) ===============================================================

/**
 * fn_name :  _X.DropDownList
 * Description : selectbox html 생성
 * param : selectbox id, a_width : width
 * Statements :
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
 */
_X.DropDownList = function (a_id, a_width)  {
  if(a_width==null)
    a_width=150;

  _Eval += a_id + ".SetData=function(a_datas, a_default, ab_code, ab_all, a_alltext){_X.DDLB_SetData(a_id, a_datas, a_default, ab_code, ab_all, a_alltext);};";

  return '<SELECT id="' + a_id + '" language="JavaScript" onchange="xe_EditChanged(this, this.value);" style="padding:0; font-family:맑은 고딕; font-size:9pt; width:' + a_width + 'px; height:20px;">'
    + '<OPTION value="%">선택</OPTION>'
    + '</SELECT>'
    + '</div>';
};

/**
 * fn_name :  _X.DropDownList_SetData
 * Description : select box option value 값 생성 또는 reload
 * param a_obj : selectbox object, a_datas : json data, ab_code: 코드표시유무
 * param ab_all : 전체표시유무, a_alltext : 전체표시 타이틀(없는경우 전체가 default)
 * Statements :
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
 */
_X.DropDownList_SetData = function(a_obj, a_datas, a_default, ab_code, ab_all, a_alltext) {
  if(typeof(a_obj.options)=="undefined") return;
  if(a_obj.options.length>0)
    a_obj.options.length = 0;

  //if(a_datas==null||a_datas=='') return '';

  //(2014.09.03 이상규)
  //$('#'+a_obj.id).selectBox('destroy');
  if(ab_all) {
    if(a_alltext==null) {
      a_alltext = '전체';
    }
    a_obj.options[a_obj.options.length] = new Option(a_alltext, a_alltext == "전체" ? '%' : "", false, false);
  }

  if(a_datas==null||a_datas=='') return '';

  for(var i=0; i<a_datas.length; i++){
    if(typeof(a_datas[0].code)!="undefined")
      a_obj.options[a_obj.options.length] = new Option((ab_code!=null&&ab_code?'['+a_datas[i].code+'] ':'') + a_datas[i].label, a_datas[i].code, false, a_datas[i].code==a_default||a_datas[i].label==a_default);
    else if(typeof(a_datas[0].CODE)!="undefined")
      a_obj.options[a_obj.options.length] = new Option((ab_code!=null&&ab_code?'['+a_datas[i].CODE+'] ':'') + a_datas[i].LABEL, a_datas[i].CODE, false, a_datas[i].CODE==a_default||a_datas[i].LABEL==a_default);
    else
      a_obj.options[a_obj.options.length] = new Option((ab_code!=null&&ab_code?'['+a_datas[i][0]+'] ':'') + a_datas[i][1], a_datas[i][0], false, a_datas[i][0]==a_default||a_datas[i][1]==a_default);
  }
  //$('#'+a_obj.id).selectBox();
};
_X.DDLB_SetData = _X.DropDownList_SetData;

//checkbox radio tag lib 로 전환
_X.CheckBox = function(a_objname, a_text, a_checked, a_win) {
  if(a_win==null) a_win=window;
  if(a_win._isModal)
    return '<input type="checkbox" id="' + a_objname + '" name="' + a_objname + '" ' //+ "value="' + a_value + '" '
      + (a_checked?"checked":"") + ' onclick="xe_EditChanged(this,this.value)" style="vertical-align:middle;">'
      + '<label style="vertical-align:middle;font-weight:bold;"  for="' + a_objname + '" style="vertical-align:middle;">'+a_text+'</label>';
  else
    return '<a href="javascript:if(!' + a_objname + '.disabled){' + a_objname + '.checked=!' + a_objname + '.checked;xe_EditChanged('
      + a_objname + ',' + a_objname + '.value)}" target="_self"><input type="checkbox" id="'
      + a_objname + '" name="' + a_objname + '" ' //+ "value="' + a_value + '" '
      + (a_checked?"checked":"") + ' onclick="xe_EditChanged(this,this.value)" style="vertical-align:middle;">'
      + '<label style="vertical-align:middle;font-weight:bold;" for="' + a_objname + '">'+a_text+'</label>' + '</a>';
};


/**
 * fn_name :  _X.MsgBox
 * Description : alert type Messagebox popup
 * param a_title : 제목, a_content : 메세지 내용, a_icon: icon type
 * param a_button : button type,  a_default : default button 값
 * param a_script : html script  a_win : window object
 * Statements :
 *  icon_Information= 1;  icon_StopSign = 2;
 *  icon_Exclamation= 3;  icon_Question = 4;
 *  icon_None   = 5;  icon_Ok     = 6;
 *
 *  btn_OK      = 1;  btn_OKCancel  = 2;
 *  btn_YesNo   = 3;  btn_YesNoCancel = 4;
 *  btn_RetryCancel = 5;  btn_AbortRetryIgnore =6;
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
 */
_X.MsgBox = function(a_title, a_content, a_icon, a_button, a_default, a_script, a_win) {
  if (a_content==null){
    a_content = a_title;
    a_title = "확인";
  }

  if (a_icon==null) a_icon  = 1;
  if (a_button==null) a_button = btn_OK;
  if (a_default==null) a_default = 1;

  if (a_script!=null) {
    a_script = "&script=" + encodeURIComponent(a_script);
  } else {a_script = "";}

  return alert(a_content);
 };

 /**
  * fn_name :  _X.MsgBox
  * Description : confirmtype type Messagebox popup
  * param a_title : 제목, a_content : 메세지 내용, a_icon: icon type
  * param a_button : button type,  a_default : default button 값
  * param a_script : html script  a_win : window object
  * Statements :
  *  icon_Information= 1; icon_StopSign = 2;
  * icon_Exclamation= 3;  icon_Question = 4;
  * icon_None   = 5;  icon_Ok     = 6;
  *
  * btn_OK      = 1;  btn_OKCancel  = 2;
  * btn_YesNo   = 3;  btn_YesNoCancel = 4;
  * btn_RetryCancel = 5;  btn_AbortRetryIgnore =6;
  *
  *   since        author           Description
  *  ------------    --------    ---------------------------
  *   2013.02.10    김양열          최초 생성
  */
_X.MsgBoxYesNo = function(a_title, a_content, a_icon, a_button, a_default, a_script, a_win) {

  if (a_content==null||a_content==""){
    a_content = a_title;
    a_title = "확인";
  }

  if (a_icon==null) a_icon  = 1;
  if (a_button==null) a_button = btn_YesNo;
  if (a_default==null) a_default = 1;

  if (a_script!=null) {
    a_script = "&script=" + encodeURIComponent(a_script);
  } else {a_script = "";}
  result = confirm(a_content);
  return (result?"1":"2");
 };

//show modal 문제로 막음
_X.MsgBoxYesNoCancel = function(a_title, a_content, a_default) {
    //(a_title, a_content, a_icon, a_button, a_default, a_script, a_win)
  //return _X.MsgBox(a_title, a_content, icon_Question, btn_YesNoCancel, a_default);
};
//show modal 문제로 막음
_X.MsgBoxOKCancel = function(a_title, a_content, a_default) {
  //(a_title, a_content, a_icon, a_button, a_default, a_script, a_win)
  //return _X.MsgBox(a_title, a_content, icon_Question, btn_OKCancel, a_default);
};
//show modal 문제로 막음
_X.MsgBoxRetryCancel = function(a_title, a_content, a_default) {
    //(a_title, a_content, a_icon, a_button, a_default, a_script, a_win)
  //return _X.MsgBox(a_title, a_content, icon_Question, btn_RetryCancel, a_default);
};
//show modal 문제로 막음
_X.MsgBoxAbortRetryIgnore = function(a_title, a_content, a_default) {
  //(a_title, a_content, a_icon, a_button, a_default, a_script, a_win)
  //return _X.MsgBox(a_title, a_content, icon_Question, btn_AbortRetryIgnore, a_default)
};

/**
 * fn_name :  _X.Tabs
 * Description : Tab Control UI 처리
 * param a_div :  div object, a_inittab : 초기 focus tab index
 * param a_skin : css tab skin,  a_NavHeight : tab height
 * Statements : _X.Tabs(tabs_1, 0);
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
*/
_X.Tabs = function(a_div, a_inittab, a_skin, a_NavHeight) {
  if(a_inittab==null)
    a_inittab = 0;
  if(a_skin==null)
    a_skin = "ctab_o";
  if(a_NavHeight==null)
    a_NavHeight = 25;
  var rInfo = null;
  var curTab = null;

  if(typeof(a_div.ResizeInfo)!=""){
    rInfo = a_div.ResizeInfo;
    rInfo.init_height = rInfo.init_height - a_NavHeight;
  }

  var cDivs = $(a_div).children("div");
  for (var i=0; i<cDivs.length; i++) {
    var cDiv = cDivs[i];
    if(i==0) {
      //Navigation Div
      cDiv.ResizeInfo = {init_width:rInfo.init_width, init_height:a_NavHeight, anchor:{x1:1,y1:1,x2:1,y2:0}};
      var tabNavHtml = "";
      var cLis = $(cDiv).children("ul").children("li");
      tabNavHtml += "<div class='" + a_skin + "_line'></div>";
      for(var j=0; j<cLis.length; j++) {
        var navIdx  = j;
        var navId   = a_div.id + "_nav_" + navIdx;
        var tabHref = $(cLis[j].children).attr("href");
				var ctab    = $(cLis[j].children).data("ctab");
				var tabno   = $(cLis[j].children).data("tabno");
				
        tabNavHtml += "<div id='" + navId + "' class='" + (navIdx==a_inittab ? a_skin + "_a" : a_skin)
                    + "' data-ctab='" + (ctab || "")
                    + "' data-tabno='" + (tabno || "")
                    + "' onclick='_X.TabChange(" + a_div.id + ",this," + navIdx + ',' + (tabHref==""||tabHref=="#" ? "null" : tabHref.replace('#','')) + ',"' + a_skin + '", ' + tabno + ')' + "'" + ">"
                    + $(cLis[j].children).text()
                    + "</div>";
        if(tabHref!="" && tabHref!="#" && navIdx==a_inittab) {
          curTab = $(tabHref)[0];
        }
      }
      //tabNavHtml += "<div class='" + a_skin + "_line'></div>";
      $(cDiv).html(tabNavHtml);
    } else {
      //Tab Div
      //$(cDiv).css("display", "none");
      cDiv.ResizeInfo = rInfo;
    }
  }

  a_div.GetTabIndex = function() {
    return a_div._PreIdx;
  };
  a_div.GetNewTabIndex = a_div.GetTabIndex;
	
	a_div.GetTabNo = function() {
    return a_div._PreTabNo;
  };
  
  a_div._PreNav   = $("#" + a_div.id + "_nav_" + a_inittab)[0];
  a_div._PreTab   = curTab;
  a_div._PreIdx   = a_inittab;
  a_div._PreTabNo = $(a_div._PreNav).data("tabno") || 0;
  $(curTab).show();
};

/**
 * fn_name :  _X.GetTabIndex
 * Description : 현재의 Tab index 번호 리턴
 * param a_div :  div object
 * Statements : _X.GetTabIndex(tabs_1)
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
*/
_X.GetTabIndex = function(a_div) {
  return a_div._PreIdx;
};

// 20170817 이상규 추가
_X.GetTabNo = function(a_div) {
  return a_div._PreTabNo;
};

/**
 * fn_name :  _X.TabChange
 * Description : Tab 선택을 변경된 후 호출되는 함수
 * param a_div :  div object, a_nav : tab navigator object.
 * param a_navidx : ab navigator index,  a_curtab : 선택 tab index, a_skin : css tab skin
 * Statements : 내부 호출 사용자 호출 못함
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
*/
_X.TabChange = function(a_div, a_nav, a_navidx, a_curtab, a_skin, a_tabNo) {
  if(a_skin==null)
    a_skin = "ctab_o";

  if(typeof(xe_TabChanging)!="undefined") {
    if (xe_TabChanging(a_div, a_navidx, a_div._PreIdx, a_curtab, a_div._PreTab, a_tabNo, a_div._PreTabNo)!=1) return;
  }

  if(a_div._PreIdx != a_navidx) {
    //Style변경
    $(a_div._PreNav).removeClass(a_skin + "_a");
    $(a_div._PreNav).addClass(a_skin);

    $(a_nav).removeClass(a_skin);
    $(a_nav).addClass(a_skin + "_a");
  }

  if(typeof(a_div._PreTab)!="undefined" && a_div._PreTab != a_curtab) {
    $(a_div._PreTab).hide();
  }
  $(a_curtab).show();

	var preTab   = a_div._PreTab;
	var preIdx   = a_div._PreIdx;
	var preNav   = a_div._PreNav;
	var preTabNo = a_div._PreTabNo;
	
  a_div._PreNav   = a_nav;
  a_div._PreTab   = a_curtab;
  a_div._PreIdx   = a_navidx;
  a_div._PreTabNo = a_tabNo;
	
	if(typeof(xe_TabChanged)!="undefined") {
    xe_TabChanged(a_div, a_navidx, preIdx, a_curtab, preTab, a_nav, preNav, a_tabNo, preTabNo);
  }
};

/**
 * fn_name :  _X.SetTabChange
 * Description : Tab의 선택 처리
 * param a_div :  div object
 * Statements : _X.SetTabChange(tabs_1,2)
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
*/
_X.SetTabChange = function(a_div, a_tab, a_href) {
  if(a_div._PreIdx == a_tab) return;
  var navId = eval(a_div.id + "_nav_" + a_tab);
  _X.TabChange(a_div, navId, a_tab, a_href);
};

//수정(2015.07.26 KYY)
_X.Block = function(a_screen, a_img) {
  if(a_screen==null) a_screen = true;
  if(a_img==null) a_img = true;
  if(a_img)
    $("#pageLayout").append("<div id='formLoadImg' style='position:absolute; left:50%; top:40%; z-index:10000;'><img src='/Theme/images/viewLoading.gif'/></div>");
  if(a_screen)
    $.blockUI();
};
//수정(2015.07.26 KYY)
_X.UnBlock = function(a_screen, a_img) {
  if(a_screen==null) a_screen = true;
  if(a_img==null) a_img = true;
  if(a_img)
    $('#formLoadImg').remove();
  if(a_screen)
    $.unblockUI();
};

//로딩이미지추가-박종현
_X.Block2 = function(){$.blockUI({ message: '<img src="/images/processing.gif"/>', css : { border : 'none', width: 250, height: 250, 'cursor': 'auto'}  });};

/**
 * fn_name :  _X.FormClearValue
 * Description : div 내의 input/select/checbox value 초기화
 * param a_div :  div object
 * Statements : _X.FormClearValue(freeform);
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
*/
_X.FormClearValue = function(ele) {
  $(ele).find(':input').each(function() {
        switch(this.type) {
            case 'select-multiple':
            case 'select-one':
              //$(this).selectBox('value','');
              $(this).val('');
              break;
            case 'select':
            case 'password':
            case 'text':
            case 'textarea':
                $(this).val('');
                break;
            case 'checkbox':
            case 'radio':
              $(this).prop("checked", false);
                break;
        }
    });
};

/**
 * fn_name :  _X.FormSetDisable
 * Description : object disable/enable
 * param ele :  array object , flag : true(enable) false(disable)
 * Statements  :_X.EleToggleStatus(new Array(EANNUAL_SDATE), false);

 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
*/
_X.FormSetDisable =  function(ele, flag) {
  $.each(ele, function (i, item) {
    switch($(item).prop('type')) {
          case 'select-multiple':
          case 'select-one':
          case 'select':
          case 'password':
          case 'text':
          case 'textarea':
          case 'radio':
            $(item).prop( "disabled", flag );
            break;
            
          case 'checkbox':
            if(flag){
              $(this).attr('disabled', 'disabled').parent().find('a:first').addClass('disabled');
            } else {
              $(this).removeAttr('disabled').parent().find('a:first').removeClass('disabled');
            }
            break;
    }
  });
};

/**
 * fn_name :  _X.FormSetAllDisable
 * Description : div 전체 disable/enable toggle
 * param div :  div object , flag : true(enable) false(disable)
 * Statements  :_X.FormSetAllDisable(freeform, false);
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
*/
_X.FormSetAllDisable =  function(div, flag) {
  //수정(2015.06.08 KYY)
  var ele = (div.indexOf("#")>=0 || div.indexOf(".")>=0 ? div : '#'+ div);

  $(ele).find('*').each(function(index,item) {
    switch($(item).prop('type')) {
        case 'select-multiple':
        case 'select-one':
        case 'select':
        case 'password':
        case 'text':
        case 'textarea':
        case 'radio':
          $(item).prop( "disabled", flag );
          break;
           
        case 'checkbox':
          if(flag){
            $(this).attr('disabled', 'disabled').parent().find('a:first').addClass('disabled');
          } else {
            $(this).removeAttr('disabled').parent().find('a:first').removeClass('disabled');
          }
          //(flag?this.disable():this.enable());
          break;
    }
  });
};

/**
 * fn_name :  _X.FromSetDisplay
 * Description : 원하는 요소의 display를 끄고 킴 (id, true=show : false=hide)
 * param eles :  div object , flag : true(enable) false(disable)
 * Statements  :_X.FromSetDisplay(new Array("D_DEPT_LABEL", "S_DEPT_CODE_NAME"), new Array(false, false));
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
*/
_X.FromSetDisplay = function (eles, flags){

  for(var i = 0; i < eles.length; i++){
    if(flags[i]){
      $('#'+eles[i]).show();
    }else{
      $('#'+eles[i]).hide();
    }
  }
}

/**
 * fn_name :  _X.FormSetReadOnlyColor
 * Description : 원하는 요소의 background-color 를 readonly의 색으로 toggle (아이디배열, true=readonly : false=none)
 * param a_objid_arr :  array div object , flag : true(enable) false(disable)
 * Statements  :_X.FormSetReadOnlyColor(new Array("D_DEPT_LABEL", "S_DEPT_CODE_NAME"), new Array(false, false));
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
*/
_X.FormSetReadOnlyColor =  function(a_objid_arr, a_flag) {
  if(a_objid_arr == null || a_objid_arr == "" || typeof(a_objid_arr) == "undefined") return;

  for(var i = 0; i < a_objid_arr.length; i++) {
    $('#' + a_objid_arr[i]).css('background-color', a_flag ? "#f0f4f7" : "#ffffff");
  }
};

/**
 * fn_name :  _X.fullCalendar
 * Description : 원하는 요소의 background-color 를 readonly의 색으로 toggle (아이디배열, true=readonly : false=none)
 * param a_div :  div object, a_tag : calendar type (1:월간 2:주간),
 * param a_data : 일정별내역데이타(JSON), a_width : div width, a_height : div height
 * Statements  :_X.fullCalendar('fullcalendar', '1', ls_data, 1000, 450);
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
*/
_X.fullCalendar = function(a_div, a_tag, a_data, a_width, a_height){
  var ls_header_left = '';
  var ls_header_center = '';
  var ls_header_right = '';

  if(a_div == null || a_div == "") a_div = 'fullcalendar';
  if(a_data == null || a_data == "") a_data = '';
  if(a_width == null || a_width == "") a_width = 1000;
  if(a_height == null || a_height == "") a_height = 665;

  if(a_tag == '1'){
    ls_header_left = 'prev,next today';
    ls_header_center = 'title';
    ls_header_right = '';
  }else{
    ls_header_left = 'prev,next today';
    ls_header_center = 'title';
    ls_header_right = 'month,agendaWeek,agendaDay';
  }

  $('#'+a_div).fullCalendar({
    header: {left: ls_header_left,center: ls_header_center,right: ls_header_right},
    lang: 'ko',
    editable: false,
    eventLimit: true,
    width: a_width,
    height: a_height,
    eventClick: function(calEvent, jsEvent, view) {
      x_fullcalendar_click('eventClick', 'Y', view.name);
    },
    dayClick: function(date, jsEvent, view) {
      x_fullcalendar_click('dayClick', date.format(), view.name);
    },
    events: a_data
  });
};

//div 전체 disable/enable toggle
_X.DivToggleStatus =  function(div, flag) {
  $('#'+ div).find('*').each(function(index,item) {
    switch($(item).prop('type')) {
        case 'select-multiple':
        case 'select-one':
          (flag?$(item).selectBox('disable'):$(item).selectBox('enable'));
          break;
        case 'password':
        case 'text':
        case 'textarea':
          (flag?$(item).prop( "disabled", true ):$(item).removeAttr("disabled"));
            break;
        case 'checkbox':
        case 'radio':
          (flag?this.disable():this.enable());
            break;
    }
  });
};


//추가(2018.03.14 KYY - 기능 보강해야함)
_X.SetAmChart = function(a_chartdiv, a_info, a_data) {
  for(var i=0; i<a_data.length; i++) {
    // if(typeof a_data[i].USE_A != "undefined" && a_data[i].USE_A == null || a_data[i].USE_A == '')
    //  a_data[i].USE_A = 'NaN';
    // if(typeof a_data[i].USE_B != "undefined" && a_data[i].USE_B == null || a_data[i].USE_B == '')
    //  a_data[i].USE_B = 'NaN';
    // if(typeof a_data[i].USE_C != "undefined" && a_data[i].USE_C == null || a_data[i].USE_C == '')
    //  a_data[i].USE_C = 'NaN';
    for (var j=0; j<a_info.cols.length; j++) {
      if(typeof a_data[i][a_info.cols[j].field] != "undefined" && a_data[i][a_info.cols[j].field] == null || a_data[i][a_info.cols[j].field] == '')
        a_data[i][a_info.cols[j].field] = 'NaN';
    }

  }

  if($("#"+a_chartdiv).length>0 && typeof $("#"+a_chartdiv)[0].Chart != "undefined") {
    if($("#"+a_chartdiv)[0].Chart.titles[0].text != a_info.title)
      $("#"+a_chartdiv)[0].Chart.titles[0].text = a_info.title;
    //$("#"+a_chartdiv)[0].Chart.clear();
    $("#"+a_chartdiv)[0].Chart.dataProvider = a_data;
    $("#"+a_chartdiv)[0].Chart.validateData();
    $("#"+a_chartdiv)[0].Chart.validateNow();
    return; 
  }

  var colors = ["#20acd4","#dd5a14","#B0DE09"];
  var colInfo = [];

  for (var i=0; i<a_info.cols.length; i++) {
    colInfo[i] = {
        "id": "g" + (i+1),
        "valueAxis": "v1",
        "bullet": "round",
        "bulletBorderAlpha": 1,
        "bulletColor": colors[i],
        "bulletSize": 5,
        "hideBulletsCount": 50,
        "lineThickness": (a_info.cols[i].type.toLowerCase().indexOf("line")>=0 ? 1.5 : 0.5),
        "lineColor": colors[i],
        "fillColors": colors[i],
        "fillAlphas": (a_info.cols[i].type.toLowerCase().indexOf("line")>=0 ? 0.03 : 0.5),
        "title": a_info.cols[i].title,
        "type": a_info.cols[i].type,
        "valueField": a_info.cols[i].field,
        "clustered": true,
        "legendValueText": "[[value]]",
        "balloonText": "[[title]]<br/><b style='font-size: 130%'> [[value]]</b>"
    };
  }


  var charInfo = {
    "type": "serial",
    "theme": "default",
    "precision": 2,
    "valueAxes": [{
      "id": "v1",
      //"title": "(단위:천만원)",
      "position": "left",
      "autoGridCount": true,
      "labelFunction": (typeof(a_info.valueFunction)!="undefined" ? a_info.valueFunction : function(value) {return _X.FormatComma(parseInt(Math.round(value))) + "";})
    }],
    "graphs": colInfo,
    "chartCursor": {
      "pan": true,
      "valueLineEnabled": true,
      "valueLineBalloonEnabled": true,
      "cursorAlpha": 0,
      "valueLineAlpha": 0.2
    },
    "categoryField": a_info.categoryField,
    "categoryAxis": {
      //"parseDates": true,
      "dashLength": 1,
      "minorGridEnabled": true,
      "labelFunction": (typeof(a_info.categoryFunction)!="undefined" ? a_info.categoryFunction : function(value) {return value;})
    },
    "legend": {
      "enabled": a_info.legend,
      "marginLeft": 10,
      "marginRight": 10,
      "useGraphSettings": true,
      "valueWidth": 0
    },
    "balloon": {
      "borderThickness": 1,
      "shadowAlpha": 0
    },
    "export": {
     "enabled": true
    },
    "titles": [
      {
        "id": "c_title",
        "size": 16,
        "text": a_info.title
      }
    ],
    //"depth3D": 5,
    //"angle": 5,
    "dataProvider": a_data
  }

  var chart = AmCharts.makeChart(a_chartdiv, charInfo);   
  $("#"+a_chartdiv)[0].Chart = chart;
};

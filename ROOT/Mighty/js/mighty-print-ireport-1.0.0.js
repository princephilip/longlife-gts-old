﻿
/**
 * fn_name :  _X.iReport
 * Description : iReport report 를 설정하고 인쇄한다...
 * param : a_system:단위시스템코드, a_rptname: report 파일명, a_params : 파라미터(a_comp=100&a_userid=ADMIN)
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2015.10.26  	HEM          최초 생성
 */

_X.SetiReport = function (a_system, a_rptname, a_params, width, height) {

		var ar_param = a_params.split("&");
		var enc_param = "";
		for(var i=0;i<ar_param.length;i++){
			var ar = ar_param[i].split("=");
			enc_param = enc_param + "&" + ar[0] + "=" + encodeURIComponent(encodeURI(ar[1],"UTF-8"));
		}

	  var param = "rptURL=/APPS/" + a_system + "/report/"+encodeURIComponent(encodeURI(a_rptname,"UTF-8"))+enc_param;
	  //param = encodeURI(param, "UTF-8");  //encodeURLComponent()
	  var irptView = "/Mighty/jsp/iReport.jsp";
		window.open(irptView+"?"+param, "", "center=yes,scrollbars=no,status=no,toolbar=no,resizable=0,location=no,menu=no, left=100, top=100, width="+(width || 1000)+",height="+(height || 800));

}
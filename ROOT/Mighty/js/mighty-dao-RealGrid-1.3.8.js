/**
 * Object :  mighty-dao-RealGrid-1.2.0.js
 * @Description : Grid 함수처리  JavaScript
 * @author :엑스인터넷정보 김양열
 * @since : 2013.02.10
 * @version : 1.2.0
 *
 * @Modification Information
 * <pre>
 *   since    	  author           Description
 *  ----------    --------    ---------------------------
 *   2013.02.10    김양열        최초생성
 *   2014.12.20    박영찬        share 기능 개선, updatable 지정
 *   2015.09.04    이상규			   인터페이스 변경
 */

//IME모드 관련 옵션 추가(2015.03.24 KYY);
RealGrids.enableImeOnExit(true);

//error callback
RealGrids.onerror = function (id, error) {
	if(_X.IsAdmin() && error.indexOf("#1034")<0) {
		//RealGrid 에러 메시지 표시하지 않음(2015.03.24 KYY)
  	alert("REALGRIDS ERROR(관리자용) !\n\nGrid ID:" + id + "\r\n" + error);
	}
};

//리얼그리드 스타일 옵션
var _RealGrid_styles = {
	"grid":{"background":"#ffffffff","paddingRight":"2","iconLocation":"left","border":"#ff84a3c0,1","selectedForeground":"#ffffffff","iconAlignment":"center","foreground":"#ff000000","inactiveForeground":"#ff808080","iconOffset":"0","fontFamily":"맑은 고딕","textAlignment":"near","selectedBackground":"#ff696969","lineAlignment":"center","inactiveBackground":"#ffd3d3d3","selectionDisplay":"mask","hoveredMaskBorder":"#335292f7,1","iconPadding":"2","hoveredMaskBackground":"#1f5292f7","fontSize":"12","contentFit":"auto","paddingTop":"2","figureBackground":"#ff000000","paddingBottom":"2","iconIndex":"0"},
			"panel":{"background":"#ffbfc5c9","iconLocation":"left","border":"#88888888,0","iconAlignment":"center","borderBottom":"#ff778896,1","fontFamily":"맑은 고딕","iconPadding":"2","selectedBackground":"#ff696969","contentFit":"auto","inactiveBackground":"#ffd3d3d3","iconOffset":"0","selectionDisplay":"mask","hoveredMaskBackground":"#1f5292f7","fontSize":"12","hoveredMaskBorder":"#335292f7,1","figureBackground":"#ff000000","paddingBottom":"5","paddingTop":"4","paddingRight":"2","selectedForeground":"#ffffffff","foreground":"#ff74797d","inactiveForeground":"#ff808080","textAlignment":"near","lineAlignment":"center","borderRight":"#ff777777,0","iconIndex":"0"},
			"body":{"background":"#ffffffff","iconLocation":"left","border":"#88888888,0","iconAlignment":"center","borderBottom":"#ffe5e5e5,1","fontFamily":"맑은 고딕","iconPadding":"2","selectedBackground":"#ff696969","contentFit":"auto","inactiveBackground":"#ffd3d3d3","iconOffset":"0","selectionDisplay":"mask","hoveredMaskBackground":"#1f5292f7","fontSize":"12","hoveredMaskBorder":"#335292f7,1","figureBackground":"#ff000000","paddingBottom":"2","paddingTop":"2","paddingRight":"2","selectedForeground":"#ffffffff","foreground":"#ff5d5d5d","inactiveForeground":"#ff808080","line":"#ff696969,1","textAlignment":"near","lineAlignment":"center","borderRight":"#ffe5e5e5,1","iconIndex":"0",
			"empty":{"background":"#fff8f8f8","iconLocation":"left","border":"#88888888,1","iconAlignment":"center","borderBottom":"#ff999999,1","fontFamily":"맑은 고딕","iconPadding":"2","selectedBackground":"#ff696969","contentFit":"auto","inactiveBackground":"#ffd3d3d3","iconOffset":"0","selectionDisplay":"mask","hoveredMaskBackground":"#1f5292f7","fontSize":"12","hoveredMaskBorder":"#335292f7,1","figureBackground":"#ff000000","paddingBottom":"2","paddingTop":"2","paddingRight":"2","selectedForeground":"#ffffffff","foreground":"#ff000000","inactiveForeground":"#ff808080","textAlignment":"near","lineAlignment":"near","borderRight":"#ff999999,1","iconIndex":"0"}},
	"fixed":{"background":"#fff9f9f9","iconLocation":"left","border":"#88888888,1","iconAlignment":"center","borderBottom":"#ffe5e5e5,1","fontFamily":"맑은 고딕","iconPadding":"2","selectedBackground":"#ff696969","contentFit":"auto","inactiveBackground":"#ffd3d3d3","iconOffset":"0","selectionDisplay":"mask","hoveredMaskBackground":"#1f5292f7","fontSize":"12","hoveredMaskBorder":"#335292f7,1","figureBackground":"#ff000000","paddingBottom":"2","paddingTop":"2","paddingRight":"2","selectedForeground":"#ffffffff","foreground":"#ff000000","inactiveForeground":"#ff808080","textAlignment":"near","lineAlignment":"center","borderRight":"#ff999999,1","iconIndex":"0",
			"colBar":{"background":"#ffd3d3d3","iconLocation":"left","border":"#88888888,1","iconAlignment":"center","borderBottom":"#ff999999,1","fontFamily":"맑은 고딕","iconPadding":"2","selectedBackground":"#ff696969","contentFit":"auto","inactiveBackground":"#ffd3d3d3","iconOffset":"0","selectionDisplay":"mask","hoveredMaskBackground":"#1f5292f7","fontSize":"12","hoveredMaskBorder":"#335292f7,1","figureBackground":"#ff000000","paddingBottom":"2","paddingTop":"2","paddingRight":"2","selectedForeground":"#ffffffff","foreground":"#ff000000","inactiveForeground":"#ff808080","textAlignment":"near","lineAlignment":"center","borderRight":"#ff999999,1","iconIndex":"0"},
			"rowBar":{"background":"#ffd3d3d3","iconLocation":"left","border":"#88888888,1","iconAlignment":"center","borderBottom":"#ff999999,1","fontFamily":"맑은 고딕","iconPadding":"2","selectedBackground":"#ff696969","contentFit":"auto","inactiveBackground":"#ffd3d3d3","iconOffset":"0","selectionDisplay":"mask","hoveredMaskBackground":"#1f5292f7","fontSize":"12","hoveredMaskBorder":"#335292f7,1","figureBackground":"#ff000000","paddingBottom":"2","paddingTop":"2","paddingRight":"2","selectedForeground":"#ffffffff","foreground":"#ff000000","inactiveForeground":"#ff808080","textAlignment":"near","lineAlignment":"center","borderRight":"#ff999999,1","iconIndex":"0"}},	
		"header":{"background":"linear,#ffffffff,#ffd2dbe9,90","iconLocation":"left","border":"#88888888,0","iconAlignment":"center","borderBottom":"#aa565f67,1","fontFamily":"맑은 고딕","borderTop":"#ffffffff,1","selectedBackground":"linear,#aaf9d99f,#aaf1c15f,90","contentFit":"auto","inactiveBackground":"#ffd3d3d3","iconOffset":"0","selectionDisplay":"mask","iconPadding":"2","hoveredMaskBackground":"linear,#fffdffd1,#fff4e392,90","fontSize":"12","paddingTop":"2","fontBold":"false","figureBackground":"#ff556169","paddingBottom":"2","hoveredMaskBorder":"#335292f7,1","paddingRight":"2","selectedForeground":"#ff47565f","foreground":"#ff47565f","inactiveForeground":"#ff808080","textAlignment":"center","borderLeft":"#ffe5f0f6,1","lineAlignment":"center","borderRight":"#ff879095,1","textWrap":"normal","iconIndex":"0",
		"group":{"background":"linear,#ffffffff,#ffd2dbe9,90","iconLocation":"left","border":"#88888888,0","iconAlignment":"center","borderBottom":"#aa565f67,1","fontFamily":"맑은 고딕","borderTop":"#ffffffff,1","selectedBackground":"linear,#aaf9d99f,#aaf1c15f,90","contentFit":"auto","inactiveBackground":"#ffd3d3d3","iconOffset":"0","selectionDisplay":"mask","iconPadding":"2","hoveredMaskBackground":"linear,#fffdffd1,#fff4e392,90","fontSize":"12","paddingTop":"2","fontBold":"false","figureBackground":"#ff556169","paddingBottom":"2","hoveredMaskBorder":"#335292f7,1","paddingRight":"2","selectedForeground":"#ff47565f","foreground":"#ff47565f","inactiveForeground":"#ff808080","textAlignment":"center","borderLeft":"#ffe5f0f6,1","lineAlignment":"center","borderRight":"#ff879095,1","textWrap":"normal","iconIndex":"0"}},
	"footer":{"hoveredMaskBorder":"#335292f7,1","iconLocation":"left","figureBackground":"#ff000000","borderRight":"#ffc6c6c6,1","iconOffset":"0","borderTop":"#ffc6c6c6,1","iconAlignment":"center","background":"#ffeeeeee","foreground":"#ff000000","border":"#88888888,1","iconPadding":"0","textAlignment":"near","inactiveBackground":"#ffd3d3d3","paddingRight":"2","paddingTop":"2","selectedBackground":"#ff696969","paddingBottom":"1","lineAlignment":"center","selectedForeground":"#ffffffff","inactiveForeground":"#ff808080","font":"맑은 고딕,12,bold","selectionDisplay":"mask","contentFit":"auto","hoveredMaskBackground":"#1f5292f7","iconIndex":"0"},
	"rowGroup":{"header":{"background":"#ffdfe5e9","iconLocation":"left","border":"#88888888,0","iconAlignment":"center","borderBottom":"#ffbac5cc,1","fontFamily":"맑은 고딕","borderTop":"#ffffffff,1","selectedBackground":"#ff696969","contentFit":"auto","inactiveBackground":"#ffd3d3d3","iconOffset":"0","selectionDisplay":"mask","iconPadding":"2","hoveredMaskBackground":"#1f5292f7","fontSize":"12","paddingTop":"2","figureBackground":"#ff556169","paddingBottom":"2","hoveredMaskBorder":"#335292f7,1","paddingRight":"2","selectedForeground":"#ffffffff","foreground":"#ff636363","inactiveForeground":"#ff808080","textAlignment":"near","borderLeft":"#ffffffff,1","lineAlignment":"center","borderRight":"#ff696969,0","iconIndex":"0"},
				"footer":{"hoveredMaskBorder":"#335292f7,1","iconLocation":"left","figureBackground":"#ff808080","borderRight":"#ffc7c4b0,1","borderBottom":"#ffc7c4b0,1","iconOffset":"0","borderTop":"#ffffffff,0","iconAlignment":"center","background":"#fffff9d4","foreground":"#ff535353","border":"#ffffffff,0","selectedBackground":"#ff696969","textAlignment":"far","inactiveBackground":"#ffd3d3d3","paddingRight":"4","paddingTop":"2","iconPadding":"0","paddingBottom":"2","lineAlignment":"center","selectedForeground":"#ffffffff","inactiveForeground":"#ff808080","borderLeft":"#ffffffff,0","font":"맑은 고딕,12,bold","selectionDisplay":"mask","contentFit":"auto","hoveredMaskBackground":"#1f5292f7","iconIndex":"0"},
				"head":{"hoveredMaskBorder":"#335292f7,1","iconLocation":"left","figureBackground":"#ff000000","borderRight":"#ffbedef3,1","borderBottom":"#ff349cde,1","iconOffset":"0","borderTop":"#ffffffff,0","iconAlignment":"center","background":"#ffdcf1ff","foreground":"#ff000000","border":"#ffffffff,0","selectedBackground":"#ff696969","textAlignment":"center","inactiveBackground":"#ffd3d3d3","paddingRight":"2","paddingTop":"2","iconPadding":"0","paddingBottom":"2","lineAlignment":"center","selectedForeground":"#ffffffff","inactiveForeground":"#ff808080","borderLeft":"#ffffffff,0","font":",12","selectionDisplay":"mask","contentFit":"auto","hoveredMaskBackground":"#1f5292f7","iconIndex":"0"},
				"foot":{"background":"linear,#ffd5dbdf,#ffb5bfc5,90","iconLocation":"left","border":"#88888888,0","iconAlignment":"center","iconOffset":"0","fontFamily":"맑은 고딕","borderTop":"#ff9da4a9,1","selectedBackground":"#ff696969","contentFit":"auto","inactiveBackground":"#ffd3d3d3","selectionDisplay":"mask","iconPadding":"2","hoveredMaskBackground":"#1f5292f7","fontSize":"12","hoveredMaskBorder":"#335292f7,1","figureBackground":"#ff516674","paddingBottom":"1","paddingTop":"2","paddingRight":"2","selectedForeground":"#ffffffff","foreground":"#ff585858","inactiveForeground":"#ff808080","textAlignment":"far","lineAlignment":"center","borderRight":"#ffa8b0b5,1","iconIndex":"0"},
				"headerBar":{"background":"#ffdfe5e9","iconLocation":"left","border":"#88888888,0","iconAlignment":"center","borderBottom":"#ffbac5cc,0","fontFamily":"맑은 고딕","iconPadding":"2","selectedBackground":"#ff696969","contentFit":"auto","inactiveBackground":"#ffd3d3d3","iconOffset":"0","selectionDisplay":"mask","hoveredMaskBackground":"#1f5292f7","fontSize":"12","hoveredMaskBorder":"#335292f7,1","paddingRight":"2","paddingBottom":"2","paddingTop":"2","figureBackground":"#ff556169","selectedForeground":"#ffffffff","foreground":"#ffffffff","inactiveForeground":"#ff808080","textAlignment":"near","lineAlignment":"center","iconIndex":"0"},
				"footerBar":{"background":"#ffeff5fa","iconLocation":"left","border":"#88888888,0","iconAlignment":"center","borderBottom":"#ffc1d2e0,1","fontFamily":"맑은 고딕","iconPadding":"2","selectedBackground":"#ff696969","contentFit":"auto","inactiveBackground":"#ffd3d3d3","iconOffset":"0","selectionDisplay":"mask","hoveredMaskBackground":"#1f5292f7","fontSize":"12","hoveredMaskBorder":"#335292f7,1","figureBackground":"#ff808080","paddingBottom":"2","paddingTop":"2","paddingRight":"2","selectedForeground":"#ffffffff","foreground":"#ff000000","inactiveForeground":"#ff808080","textAlignment":"far","borderLeft":"#ffffffff,1","lineAlignment":"center","borderRight":"#ffc1d2e0,0","iconIndex":"0"},
				"bar":{"background":"#ffdfe5e9","iconLocation":"left","border":"#88888888,0","iconAlignment":"center","borderBottom":"#ffc1d2e0,1","fontFamily":"맑은 고딕","borderTop":"#ffffffff,0","selectedBackground":"#ff696969","contentFit":"auto","inactiveBackground":"#ffd3d3d3","iconOffset":"0","selectionDisplay":"mask","iconPadding":"2","hoveredMaskBackground":"#1f5292f7","fontSize":"12","hoveredMaskBorder":"#335292f7,1","figureBackground":"#ffffffff","paddingBottom":"2","paddingTop":"2","paddingRight":"2","selectedForeground":"#ffffffff","foreground":"#ffffffff","inactiveForeground":"#ff808080","textAlignment":"near","lineAlignment":"center","borderRight":"#ffbac5cc,1","iconIndex":"0"},
				"panel":{"background":"linear,#ffffffff,#ffd2dbe9,90","iconLocation":"left","border":"#ff7e8a93,1","iconAlignment":"center","borderBottom":"#aa565f67,1","fontFamily":"맑은 고딕","iconPadding":"2","selectedBackground":"linear,#aaf9d99f,#aaf1c15f,90","contentFit":"auto","inactiveBackground":"#ffd3d3d3","iconOffset":"0","selectionDisplay":"mask","hoveredMaskBackground":"linear,#fffdffd1,#fff4e392,90","fontSize":"12","hoveredMaskBorder":"#335292f7,1","figureBackground":"#ff556169","paddingBottom":"1","paddingTop":"1","paddingRight":"1","selectedForeground":"#ff47565f","foreground":"#ff47565f","inactiveForeground":"#ff808080","line":"#ff7e8a93,1","textAlignment":"center","lineAlignment":"center","borderRight":"#ff879095,1","textWrap":"normal","iconIndex":"0"}},
	"indicator":{"background":"linear,#ffffffff,#ffd2dbe9,0","iconLocation":"left","border":"#88888888,0","iconAlignment":"center","borderBottom":"#ff97a4ad,1","fontFamily":"맑은 고딕","borderTop":"#ffffffff,1","selectedBackground":"linear,#aaf9d99f,#aaf1c15f,0","contentFit":"auto","inactiveBackground":"#ffd3d3d3","iconOffset":"0","selectionDisplay":"mask","iconPadding":"2","hoveredMaskBackground":"linear,#fffeffe3,#fff7e4ab,0","fontSize":"12","hoveredMaskBorder":"#335292f7,1","figureBackground":"#ff556169","paddingBottom":"2","paddingTop":"2","paddingRight":"2","selectedForeground":"#ff47565f","foreground":"#ff47565f","inactiveForeground":"#ff808080","textAlignment":"center","lineAlignment":"center","borderRight":"#ffa1abb5,1","iconIndex":"0",
				"head":{"background":"linear,#ffffffff,#ffd2dbe9,90","iconLocation":"left","border":"#88888888,0","iconAlignment":"center","borderBottom":"#aa565f67,1","fontFamily":"맑은 고딕","iconPadding":"2","selectedBackground":"linear,#aaf9d99f,#aaf1c15f,90","contentFit":"auto","inactiveBackground":"#ffd3d3d3","iconOffset":"0","selectionDisplay":"mask","hoveredMaskBackground":"linear,#fffdffd1,#fff4e392,90","fontSize":"12","hoveredMaskBorder":"#335292f7,1","figureBackground":"#ff556169","paddingBottom":"2","paddingTop":"2","paddingRight":"2","selectedForeground":"#ff47565f","foreground":"#ff47565f","inactiveForeground":"#ff808080","textAlignment":"center","lineAlignment":"center","borderRight":"#ff879095,1","textWrap":"normal","iconIndex":"0"},
				"foot":{"background":"linear,#ffd5dbdf,#ffb5bfc5,90","iconLocation":"left","border":"#88888888,0","iconAlignment":"center","iconOffset":"0","fontFamily":"맑은 고딕","borderTop":"#ff9da4a9,1","selectedBackground":"#ff696969","contentFit":"auto","inactiveBackground":"#ffd3d3d3","selectionDisplay":"mask","iconPadding":"2","hoveredMaskBackground":"#1f5292f7","fontSize":"12","hoveredMaskBorder":"#335292f7,1","figureBackground":"#ff516674","paddingBottom":"1","paddingTop":"2","paddingRight":"2","selectedForeground":"#ffffffff","foreground":"#ff585858","inactiveForeground":"#ff808080","textAlignment":"far","lineAlignment":"center","borderRight":"#ffa8b0b5,1","iconIndex":"0"}},
	"checkBar":{"background":"#fffafafa","iconLocation":"left","border":"#88888888,0","iconAlignment":"center","borderBottom":"#ffcacaca,1","fontFamily":"맑은 고딕","borderTop":"#ffffffff,1","selectedBackground":"#ff696969","contentFit":"auto","inactiveBackground":"#ffd3d3d3","iconOffset":"0","selectionDisplay":"mask","iconPadding":"2","hoveredMaskBackground":"#1f5292f7","fontSize":"12","paddingTop":"2","figureBackground":"#ff007ab9","paddingBottom":"2","hoveredMaskBorder":"#335292f7,1","paddingRight":"2","figureSize":"12","selectedForeground":"#ffffffff","foreground":"#ff000000","inactiveForeground":"#ff808080","textAlignment":"center","borderLeft":"#ffffffff,1","lineAlignment":"center","borderRight":"#ffbcbcbc,1","iconIndex":"0",
		"head":{"background":"linear,#ffffffff,#ffd2dbe9,90","iconLocation":"left","border":"#88888888,0","iconAlignment":"center","borderBottom":"#aa565f67,1","fontFamily":"맑은 고딕","iconPadding":"2","selectedBackground":"linear,#aaf9d99f,#aaf1c15f,90","contentFit":"auto","inactiveBackground":"#ffd3d3d3","iconOffset":"0","selectionDisplay":"mask","hoveredMaskBackground":"linear,#fffdffd1,#fff4e392,90","fontSize":"12","paddingTop":"2","figureBackground":"#ff007ab9","paddingBottom":"2","hoveredMaskBorder":"#335292f7,1","paddingRight":"2","figureSize":"12","selectedForeground":"#ff47565f","foreground":"#ff47565f","inactiveForeground":"#ff808080","textAlignment":"center","lineAlignment":"center","borderRight":"#ff879095,1","textWrap":"normal","iconIndex":"0"},
		"foot":{"background":"linear,#ffd5dbdf,#ffb5bfc5,90","iconLocation":"left","border":"#88888888,0","iconAlignment":"center","iconOffset":"0","fontFamily":"맑은 고딕","borderTop":"#ff9da4a9,1","selectedBackground":"#ff696969","contentFit":"auto","inactiveBackground":"#ffd3d3d3","selectionDisplay":"mask","iconPadding":"2","hoveredMaskBackground":"#1f5292f7","fontSize":"12","hoveredMaskBorder":"#335292f7,1","figureBackground":"#ff516674","paddingBottom":"1","paddingTop":"2","paddingRight":"2","selectedForeground":"#ffffffff","foreground":"#ff585858","inactiveForeground":"#ff808080","textAlignment":"far","lineAlignment":"center","borderRight":"#ffa8b0b5,1","iconIndex":"0"}},
	"statusBar":{"background":"#fffafafa","iconLocation":"left","border":"#88888888,0","iconAlignment":"center","borderBottom":"#ffcacaca,1","fontFamily":"맑은 고딕","borderTop":"#ffffffff,1","selectedBackground":"#ff696969","contentFit":"auto","inactiveBackground":"#ffd3d3d3","iconOffset":"0","selectionDisplay":"mask","iconPadding":"2","hoveredMaskBackground":"#1f5292f7","fontSize":"12","paddingTop":"2","figureBackground":"#ff556169","paddingBottom":"2","hoveredMaskBorder":"#335292f7,1","paddingRight":"2","selectedForeground":"#ffffffff","foreground":"#ff000000","inactiveForeground":"#ff808080","textAlignment":"center","borderLeft":"#ffffffff,1","lineAlignment":"center","borderRight":"#ffcacaca,1","iconIndex":"0",
		"head":{"background":"linear,#ffffffff,#ffd2dbe9,90","iconLocation":"left","border":"#88888888,0","iconAlignment":"center","borderBottom":"#aa565f67,1","fontFamily":"맑은 고딕","borderTop":"#ffffffff,1","selectedBackground":"linear,#aaf9d99f,#aaf1c15f,90","contentFit":"auto","inactiveBackground":"#ffd3d3d3","iconOffset":"0","selectionDisplay":"mask","iconPadding":"2","hoveredMaskBackground":"linear,#fffdffd1,#fff4e392,90","fontSize":"12","paddingTop":"2","figureBackground":"#ff556169","paddingBottom":"2","hoveredMaskBorder":"#335292f7,1","paddingRight":"2","selectedForeground":"#ff47565f","foreground":"#ff47565f","inactiveForeground":"#ff808080","textAlignment":"center","borderLeft":"#ffe5f0f6,1","lineAlignment":"center","borderRight":"#ff879095,1","textWrap":"normal","iconIndex":"0"},
		"foot":{"background":"linear,#ffd5dbdf,#ffb5bfc5,90","iconLocation":"left","border":"#88888888,0","iconAlignment":"center","iconOffset":"0","fontFamily":"맑은 고딕","borderTop":"#ff9da4a9,1","selectedBackground":"#ff696969","contentFit":"auto","inactiveBackground":"#ffd3d3d3","selectionDisplay":"mask","iconPadding":"2","hoveredMaskBackground":"#1f5292f7","fontSize":"12","hoveredMaskBorder":"#335292f7,1","figureBackground":"#ff516674","paddingBottom":"1","paddingTop":"2","paddingRight":"2","selectedForeground":"#ffffffff","foreground":"#ff585858","inactiveForeground":"#ff808080","textAlignment":"far","lineAlignment":"center","borderRight":"#ffa8b0b5,1","iconIndex":"0"}},
	"selection":{"background":"#19f9992d","paddingRight":"0","iconLocation":"left","border":"#fff9992d,2","selectedForeground":"#ffffffff","iconAlignment":"center","foreground":"#ff000000","inactiveForeground":"#ff808080","iconOffset":"0","fontFamily":"맑은 고딕","textAlignment":"center","selectedBackground":"#ff696969","lineAlignment":"center","inactiveBackground":"#ffd3d3d3","selectionDisplay":"mask","hoveredMaskBorder":"#335292f7,1","iconPadding":"2","hoveredMaskBackground":"#1f5292f7","fontSize":"12","contentFit":"auto","paddingTop":"0","figureBackground":"#ff008800","paddingBottom":"0","iconIndex":"0"}}
//	"selection":{"background":"#1f1e90ff","paddingRight":"0","iconLocation":"left","border":"#5f1e90ff,1","selectedForeground":"#ffffffff","iconAlignment":"center","foreground":"#ff000000","inactiveForeground":"#ff808080","iconOffset":"0","fontFamily":"맑은 고딕","textAlignment":"center","selectedBackground":"#ff696969","lineAlignment":"center","inactiveBackground":"#ffd3d3d3","selectionDisplay":"mask","hoveredMaskBorder":"#335292f7,1","iconPadding":"2","hoveredMaskBackground":"#1f5292f7","fontSize":"12","contentFit":"auto","paddingTop":"0","figureBackground":"#ff008800","paddingBottom":"0","iconIndex":"0"}}
;

/***
 * Style Format, Editor 
 */
var _Styles_text     = {fontBold: false, textAlignment: "near"};
var _Styles_textc    = {fontBold: false, textAlignment: "center"};
var _Styles_textf    = {fontBold: false, textAlignment: "far"};
var _Styles_textb    = {fontBold:  true, textAlignment: "near"};
var _Styles_textcb   = {fontBold:  true, textAlignment: "center"};
var _Styles_textfb   = {fontBold:  true, textAlignment: "far"};
var _Styles_number   = {fontBold: false, textAlignment: "far",    numberFormat: "#,##0"};
var _Styles_numberc  = {fontBold: false, textAlignment: "center", numberFormat: "#,##0"};
var _Styles_numberf  = {fontBold: false, textAlignment: "far",    numberFormat: "#,##0.00"};
var _Styles_numberf1 = {fontBold: false, textAlignment: "far",    numberFormat: "#,##0.0"};
var _Styles_numberf2 = {fontBold: false, textAlignment: "far",    numberFormat: "#,##0.00"};
var _Styles_numberf3 = {fontBold: false, textAlignment: "far",    numberFormat: "#,##0.000"};
var _Styles_numberf4 = {fontBold: false, textAlignment: "far",    numberFormat: "#,##0.0000"};

var _Styles_checkbox = {figureSize: "90%", fontBold: false, textAlignment: "center", figureBackground: "#ff007ab9", figureInactiveBackground: "#33000088"};
var _Styles_date	   = {fontBold: false, textAlignment: "center", datetimeFormat: "yyyy-MM-dd"};
var _Styles_datetime = {fontBold: false, textAlignment: "center", datetimeFormat: "yyyy-MM-dd HH:mm:ss"};

var _Editor_text      = {type: "line", enterToTab: false, enterToEdit: true };
var _Editor_multiline = {type: "multiLine"};
var _Editor_number    = {type: "number", editFormat: "#,##0",      integerOnly: true};
var _Editor_numberf   = {type: "number", editFormat: "#,##0.00",   integerOnly: false};
var _Editor_numberf1  = {type: "number", editFormat: "#,##0.0",    integerOnly: false};
var _Editor_numberf2  = {type: "number", editFormat: "#,##0.00",   integerOnly: false};
var _Editor_numberf3  = {type: "number", editFormat: "#,##0.000",  integerOnly: false};
var _Editor_numberf4  = {type: "number", editFormat: "#,##0.0000", integerOnly: false};

var _Editor_dropdown = {type: "dropDown", dropDownCount: 10, textReadOnly: true };
var _Editor_date	   = {type: "date", editFormat: "yyyyMMdd", yearNavigation:true};
var _Editor_datetime = {type: "date", editFormat: "yyyy-MM-dd HH:mm:ss", yearNavigation:true};

var _Renderer_check_YN = {type: "check", editable: true, startEditOnClick: true, trueValues: "Y", falseValues: "N", labelPosition: "hidden"};
var _Renderer_check_edit_YN = {type: "check", editable: false, startEditOnClick: true, trueValues: "Y", falseValues: "N", labelPosition: "hidden"};


/***
 * RealGrid option
 */
var _RealGrid_options = {
		panel: {visible: false},
		footer: {visible: false},
		checkBar: {visible: false},
		statusBar: {visible: false},
		select: {style: RealGrids.SelectionStyle.SINGLE_ROW},
    edit: {
			insertable: true,
			appendable: false,
			updatable: true,
			deletable: false,
			deleteRowsConfirm: true,
			deleteRowsMessage: "선택된 행(들)을 삭제하시겠습니까?"
    },
    "display": {"rowResizable": true, "heightMeasurer": "fixed", "rowHeight": 21, "focusColor": 0x5292f7}
};

/***
 * RealGrid tree option
 */
var _RealTree_options = {
		panel: {visible: false},
		footer: {visible: false},
		checkBar: {visible: false},
		statusBar: {visible: true},
		select: {style: RealGrids.SelectionStyle.SINGLE_ROW},
        summaryMode: RealGrids.SummaryMode.AGGREGATE,
    edit: {
			insertable: true,
			appendable: true,
			updatable: true,
			deletable: true
    },
    "display": {"rowResizable": true, "heightMeasurer": "fixed", "rowHeight": 21}
};

/***
 * RealGrid data option
 */
var _RealGrid_dataoptions = {
	softDeleting: false
};

/***
 * sync info
 * 그리드와 프리폼의 sync 작업을 위한 sync object 캐시화
 */
var x_GridSetSyncInfo = function (a_dg){
	//동기화 칼럼설정		
	if(a_dg._Share && !a_dg._SyncInfoSet) {
		a_dg._SyncInfoSet = true;
		for(var i=0; i<a_dg._ColumnNames.length; i++){
			var columnName = a_dg._ColumnNames[i];
			var dataField  = a_dg._DataField[i];
			var colObjs = $("#"+columnName);
			
			if(colObjs.length>0){
				for(var j=0; j<colObjs.length; j++){
					colObjs[j].SyncGrid  = a_dg;
					colObjs[j].SyncColumn = columnName;
					colObjs[j].SyncColumnIndex = i +1;
					colObjs[j].SyncDataType = dataField.dataType;
					a_dg._SyncObject[a_dg._SyncObject.length] = colObjs[j];
					// 성능향상을 위해 추가(2015.03.02 KYY)
					a_dg._SyncObjectJ[a_dg._SyncObjectJ.length] = $(colObjs[j]);
				}
			}
		}
	}
}

/**
 * fn_name     :  RealGrids.onload
 * Description : grid onload event 발생시 호출
 * param       : id : grid id
 * Statements  : 
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
RealGrids.onload = function (id) {
	var gridObj = $("#" +id)[0];
	var idx = gridObj.GetVariable("index");
	var share = gridObj.GetVariable("share");
	var updatable = gridObj.GetVariable("updatable");
	
	if(idx==null || typeof(idx)=="undefined" || idx=="undefined") idx="99";
	
_StopWatch.start(2);	

	var value = gridObj.innerHTML;
	_X.RealGridInit(gridObj, $(gridObj).parent()[0],(value.indexOf("TreeGridWeb")>0?true:false),idx,share,updatable);
	
_StopWatch.stop(2);
//alert(id + " - " + _StopWatch.getTime(2));	
};

/**
 * fn_name      :  _X.ResetGrid 
 * Description  : grid 의 컬럼속성과 query 정보설정
 * param        : a_dg : grid, a_column_config : 컬럼변수, a_field_config:필드변수, 
 * 		   					a_option:grid option 값, a_sqlFile:sql 파일, a_key:sql key id
 * Statements 	: 
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.ResetGrid = function (a_dg, a_column_config, a_field_config, a_option, a_sqlFile, a_share, a_updatable) {
	if ( a_dg.Reset ) {
		a_dg.Reset();
	}
	
	if(a_sqlFile!=null) {
		var a_sqlFiles = a_sqlFile.split('|');	
		a_dg.sqlFile = a_sqlFiles[0];
		a_dg.sqlKey = a_sqlFiles[1];
	}
	if(a_share==null) a_share=false;
	if(a_updatable==null) a_updatable=true;
	
	a_dg._ColumnConfig = a_column_config;		
	a_dg._FieldConfig = a_field_config;		
	a_dg._DataGrid.setFields(a_field_config);		
	a_dg._GridRoot.setColumns(a_dg._ColumnConfig);
	a_dg._KeyCols = new Array();
	
	if(a_option!=null) {
		a_dg._GridRoot.setOptions(a_option);
	}

	a_dg._ColumnNames=a_dg.getFieldNames();
	a_dg._DataField=a_dg.getDataFields();
	
	if(a_share==null||a_share=="N") {
		a_dg._Share = false;
	} else {
		a_dg._Share = true;
	}
	
	a_dg._Updatable=a_updatable;
	a_dg._Share=a_share;

	if(a_dg._Share) {
		a_dg._SyncInfoSet = false;
	}

	//데이타 변경이 가능한 경우
	if(!a_dg._ReadOnly) {
		for(var i=0; i<a_dg._ColumnNames.length; i++){
			var columnName = a_dg._ColumnNames[i];
			if(a_dg._FirstColName==null) {
				if(columnName.toUpperCase()!=="TREE"){
					var colObj = a_dg.GridRoot().columnByField(columnName); 
					if (colObj && colObj.visible) {
						a_dg._FirstColName = columnName;
					}
				}
			}		
		}
	
		//필수 항목일때 그리드 글자색변경,입력창이 아닌것은 색상변경('*' 추가)
		var keyCount=0;
		for(var j=0; j<a_column_config.length; j++) {
			if(a_dg._ColumnConfig[j].must_input=="Y") {
				var colObj = a_dg._GridRoot.columnByField(a_dg._ColumnConfig[j].fieldName); 
				if (colObj && colObj.visible) {
					var aColumn = a_dg._ColumnConfig[j].header.text;
					a_dg._GridRoot.setColumnProperty(a_dg.columnByField(a_dg._ColumnConfig[j].fieldName), "header", "*"+aColumn);
					//a_obj._GridRoot.setColumnProperty(aColumn, 'header',{styles:{foreground:"#FF4848"}});
				}
			}
			
			//grid readonly 인 경우 배경색 변경
			if(a_dg._ColumnConfig[j].readonly==true && a_dg._ColumnConfig[j].editable==false) {
				var aColumn = a_dg._GridRoot.columnByField(a_dg._ColumnConfig[j].fieldName); 
				a_dg._GridRoot.setColumnProperty(aColumn, 'styles', {background:"#F6F6F6"});
			}
			
			//key 값 체크(입력모드만 수정 가능하게)
			if(a_dg._ColumnConfig[j].isKey=="Y") {
				a_dg._KeyCols[keyCount]=a_dg._ColumnConfig[j].fieldName;
				keyCount++;
			}
			
			//maxLength 적용(2015.04.27 이상규)
			if(typeof(a_dg._ColumnConfig[j].maxLength) != "undefined") {
				var colObj = a_dg._GridRoot.columnByField(a_dg._ColumnConfig[j].fieldName);
				if(colObj.editor != "undefined" && colObj.editor != null) {
					colObj.editor = a_dg._ColumnConfig[j].editor;
					colObj.editor.maxLength = a_dg._ColumnConfig[j].maxLength.maxLength;
				} else {
					colObj.editor	=	a_dg._ColumnConfig[j].maxLength;
				}
				a_dg._GridRoot.setColumn(colObj);
			}
		}
	}
};

/**
 * fn_name 		 :  _X.RealGrid
 * Description : realgrid 의 관련속성과 datasource 를 설정한다.
 * param 			 : a_div:grid parent div id, a_id:grid id, a_width:width, a_height:height, 
 * 		   				 a_subSystem:단위시스템코드, a_layoutFile:gird column 속성파일, a_sqlFile:query 파일, 
 * 		   				 a_share:share div id, a_updatable:데이타 저장, a_key:key column
 * Statements  : 
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.RealGrid = function (a_div, a_id, a_width, a_height, a_subSystem, a_layoutFile, a_sqlFile) {
  window._GridRequestCnt++;
 	var gridObj = document.getElementById(a_id);
	var a_sqlFiles = a_sqlFile.split('|');
	gridObj.subSystem = a_subSystem;
	gridObj.sqlFile 	= a_sqlFiles[0];
	gridObj.sqlKey 	  = a_sqlFiles[1];
};

/**
 * fn_name       :  _X.RealGridInit
 * Description   : realgrid dataprovider 와 root , 컬럼, 스타일, 이벤트를 초기화 한다.
 * param 				 : a_obj : grid object, a_div:grid div, ab_tree:tree유무, ab_index:그리드순서 
 * Statements 	 : 
 *
 *   since   		     author      Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	   김양열      최초 생성
 *   2015.09.04      이상규      인터페이스변경으로 인한 제공함수, 함수명, 파라메터 변경
 */
_X.RealGridInit = function (a_obj, a_div, ab_tree, ai_idx, as_share, as_updatable) {
//============================================================== RealGrid set start
	var _GridIndex = window._Grids.length;
	if(ai_idx=="99"||ai_idx=="") {
		_GridIndex = window._Grids.length;
	} else _GridIndex = parseInt(ai_idx);

	if(as_share==null||as_share=="99") as_share="N";
	if(as_updatable==null||as_updatable=="99") as_updatable="Y";
	
	window._Grids[_GridIndex] = a_obj;

	a_obj._IsTree = ab_tree;
	a_obj._CurrentRow = -1;
	a_obj._IsLayoutCompleted = false;
	a_obj._IsRetrieving = false;
	a_obj._OldRow = -1;
	a_obj._Div  = a_div;
	a_obj._ReadOnly = (typeof(_Compact)=="undefined"||!_Compact ? false : true);
	a_obj._Updatable = false;
	
	a_obj._GridApp  = null;
	a_obj._GridRoot = (ab_tree?new RealGrids.TreeView(a_obj.id):new RealGrids.GridView(a_obj.id));
	a_obj._DataGrid = (ab_tree?new RealGrids.TreeDataProvider():new RealGrids.LocalDataProvider());
	a_obj._FirstColName = null;

	a_obj._DataGridColumn = null;
	a_obj._DataGridSelectorColumn = null;
	a_obj._ContextMenu = null;
	a_obj._ContextMenuItem = null;
	a_obj._SyncObject  = null;
	a_obj._DeletedRows = null;
	a_obj._SyncObjectJ = null;
	a_obj._SyncObject  = new Array();
	a_obj._DeletedRows = new Array();
	a_obj._SyncObjectJ = new Array();
	a_obj._SyncInfoSet = false;

	a_obj._NewItem = null;

	a_obj.GridRoot = function(){return a_obj._GridRoot;};
	a_obj.DataGrid = function(){return a_obj._DataGrid;};

	//RealGrid 디자인 초기화
	a_obj._GridRoot.setDataProvider(a_obj._DataGrid);
	if(!ab_tree) a_obj._DataGrid.setOptions(_RealGrid_dataoptions);
	
	eval("var a_column_config = columns_" + a_obj.id + ";");
	eval("var a_field_config = fields_" + a_obj.id + ";");
	eval("var a_option = (typeof(options_" + a_obj.id + ")!='undefined' ? options_" + a_obj.id + (ab_tree?" : _RealTree_options);":" : _RealGrid_options);"));
	
	a_obj._Share = (as_share=="Y"?true:false);
	a_obj._Updatable = (as_updatable=="Y"?true:false);


	if(typeof(a_obj._ColumnConfig)=="undefined") {
		_X.ResetGrid(a_obj, a_column_config, a_field_config, a_option, null, a_obj._Share, a_obj._Updatable);
	} else {
		_X.ResetGrid(a_obj, a_obj._ColumnConfig, a_obj._FieldConfig, a_option, null, a_obj._Share, a_obj._Updatable);
	}

	a_obj._GridRoot.setStyles(_RealGrid_styles);

	//클립보드에 복사한 값을 그리드에 붙여넣기 CTRL+V 키 입력을 막는 기능
	a_obj._GridRoot.setPasteOptions({enabled : false});
	
	a_obj.DataField = function(){if(a_obj._DataField==null){a_obj._DataField=a_obj.getDataFields();}; return a_obj._DataField;};
//============================================================== RealGrid set end

//============================================================== RealGrid event start
	var xe_P_GridDataLoad = function(provider){xe_M_RealGridDataLoad(a_obj);};
	//var xe_P_GridKeyDown = function(event){xe_M_RealGridKeyDown(a_obj, event);};
	
	a_obj._GridRoot.onCurrentChanging = function(grid, oldIndex, newIndex){return xe_M_RealGridRowFocusChange(a_obj, newIndex, oldIndex);};
	a_obj._GridRoot.onCellEdited = function(grid, itemIndex, dataRow, field){xe_M_RealGridDataChange(a_obj, itemIndex, dataRow, field, ls_oldValue);};
	a_obj._GridRoot.onDataCellClicked = function(grid, cellIndex){xe_M_RealGridItemClick(a_obj, cellIndex);};
	a_obj._GridRoot.onDataCellDblClicked = function(grid, cellIndex){xe_M_RealGridItemDoubleClick(a_obj, cellIndex);};
	a_obj._GridRoot.onCellButtonClicked = function(grid, itemIndex, column){xe_M_RealGridButtonClick(a_obj, itemIndex, column);};
	a_obj._GridRoot.onColumnHeaderClicked = function(grid, column){xe_M_RealGridHeaderClick(a_obj, column);};
	a_obj._GridRoot.onContextMenuItemClicked = function(grid, label, cellIndex){xe_M_RealGridContextMenuItemSelect(a_obj, label, cellIndex);};
	a_obj._GridRoot.onMenuItemClicked = function(grid, data){xe_M_RealGridMenuItemSelect(a_obj, data);};
	a_obj._DataGrid.onRowsDeleted = function(provider, rows){xe_M_RealGridRowsDeleted(a_obj, rows);};
	a_obj._GridRoot.onItemChecked = function(grid,itemIndex, checked ){xe_M_RealGridItemChecked(a_obj, itemIndex, checked);};
	a_obj._GridRoot.onTreeItemExpanding = function (grid, itemIndex, rowId) {xe_M_RealTreeItemExpanding(a_obj, itemIndex, rowId);};
	//a_obj._GridRoot.onScrollToBottom = function(grid){xe_M_RealGridScrollToBottom(a_obj);};
	//a_obj._GridRoot.onEditCommit = function(grid, index, oldValue, newValue){xe_M_RealGridDataChange(a_obj, index, oldValue, newValue);};
	
	a_obj._GridRoot.onCurrentRowChanged = function(grid, oldRow, newRow){
		//epms프로젝트는 newRow가 0일때 focusChanged 이벤트를 실행하지 않는다
		if(_Epms && newRow < 0) return;
		xe_M_RealGridRowFocusChanged(a_obj, {itemIndex:newRow}, {itemIndex:oldRow});
	};
	
	var ls_oldValue=null;
	//xe_M_RealGridDataChange oldValue
	a_obj._GridRoot.onEditCommit = function (id, index, oldValue, newValue){
		if(typeof(oldValue) == "object"){
			ls_oldValue = toDateString(oldValue);
		}else{
			ls_oldValue = oldValue;
		}
	};

	//이벤트 제거 함수 추가(2013.03.31 김양열)
	a_obj.removeEvents = function() {
			a_obj._GridRoot.onCellEdited = null;
			a_obj._GridRoot.onCurrentChanged = null;
			a_obj._GridRoot.onDataCellClicked = null;
			a_obj._GridRoot.onDataCellDblClicked = null;
			a_obj._GridRoot.onCellButtonClicked = null;
			a_obj._GridRoot.onColumnHeaderClicked = null;
			a_obj._GridRoot.onContextMenuItemClicked = null;
			a_obj._GridRoot.onMenuItemClicked = null;
			a_obj._GridRoot.onScrollToBottom = null;
			a_obj._DataGrid.onRowsDeleted = null;
			a_obj._GridRoot.onItemChecked = null;
			a_obj._GridRoot.onTreeItemExpanding = null;	
	
			a_obj._IsTree = null;
			a_obj._CurrentRow = null;
			a_obj._IsLayoutCompleted = null;
			a_obj._IsRetrieving = null;
			a_obj._OldRow = null;
			a_obj._Div = null;
			a_obj._ReadOnly = null;
			a_obj._Share = null;
			a_obj._Updatable = null;
			
			a_obj._GridApp  = null;
			a_obj._GridRoot = null;
			a_obj._DataGrid = null;
			a_obj._FirstColName = null;
			
			a_obj._DataGridColumn = null;
			a_obj._DataGridSelectorColumn = null;
			a_obj._ContextMenu = null;
			a_obj._ContextMenuItem = null;
			a_obj._SyncObject = null;
			a_obj._DeletedRows = null;
			a_obj._SyncObjectJ = null;
			a_obj._SyncInfoSet = null;
	};
//============================================================== RealGrid event end

//============================================================== Mighty-X5 Basic Method start
	//LoadingBar 생성
	a_obj.ShowLoadingBar = function() {
		return;
	};
	
	//LoadingBar 삭제
	a_obj.HideLoadingBar = function() {
		return;
	};
	
	//그리드의 변경사항을 적용
	a_obj.GridCommit = function () {
		return a_obj.GridRoot().commit(true);
	};
	
	//그리드의 변경사항을 취소
	a_obj.GridCancel = function () {
		return a_obj.GridRoot().cancel();
	};
	
	//그리드를 초기 상태로 돌린다
	a_obj.Reset = function (){
		a_obj.GridRoot().cancel();
		
		a_obj._CurrentRow = -1;
		a_obj.DataGrid().clearRows();
		for(var i=0; i<a_obj._SyncObject.length; i++)
			a_obj._SyncObject[i].value = "";
	};
	
	//데이타 저장
	a_obj.Save = function (beforeSQL, afterSQL, MsgYn) {
		var sqlData = a_obj.GetSQLData();
		if(MsgYn == null || MsgYn == "") MsgYn = 'Y';
		if(sqlData==null || sqlData.length==0){
			_X.MsgBox("변경된 데이타가 존재하지 않습니다.");
			return 0;
		}
		//필수입력항목 체크
		if(a_obj.MustInputCheck()>0){
			return 0;
		}
		
		// 문자열 길이 체크
		if(a_obj.ByteInputCheck()>0){
			return 0;
		}
		
		a_obj.ShowLoadingBar();

		//Session Timeout 처리
		var rowCnt = _X.SaveXmlData(a_obj.subSystem, sqlData, beforeSQL, afterSQL);
		
		if(rowCnt!=null) {
			a_obj.ResetRowsStates();
		}		
		a_obj.HideLoadingBar();
		
		if(rowCnt!=null && rowCnt>0){
			if(MsgYn == 'Y'){
				_X.MsgBox(rowCnt + "건의 데이타 변경작업이 \r\n 정상적으로 이루어졌습니다.");
			}
		
			if(typeof(x_DAO_Saved)!="undefined") {x_DAO_Saved();}		
			return rowCnt;
		}	
	};
	
	//데이타 조회
	a_obj.Retrieve = function (args) {
		_X.Block();
		
		var realRetrieve = function () {
			a_obj.GridRoot().commit();
			a_obj.ShowLoadingBar();
			a_obj._CurrentRow  = -2;
			a_obj._IsRetrieving = true;
			window._IsRetrieving = true;
			a_obj._OldRow = a_obj.GetRow();
			a_obj.DataGrid().clearRows();
			a_obj._DeletedRows.length = 0;
			
			var rowData = _X.XmlSelect(a_obj.subSystem, a_obj.sqlFile, a_obj.sqlKey, args, "csvstream", "csvstream");
			if(a_obj._IsTree) {
				if(rowData!=null && rowData!="" && rowData!="NO_DATA"){
					rowData = top._CPATH+"/GetCSVResult.do?fkey="+ rowData;
					a_obj.DataGrid().loadData({type: "csv", url: rowData,  start: 0, tree: "tree", progress: true, delimiter: _Col_Separate}, xe_P_GridDataLoad);
				} else {
					//데이타 없는 경우 DataLoad 이벤트 호출(2015.03.12 KYY); <- 트리도 적용되도록 추가(2015.04.24 이상규)
					xe_M_RealGridDataLoad(a_obj);
				}
			} else {
				if(rowData!=null && rowData!="" && rowData!="NO_DATA"){
					rowData = top._CPATH+"/GetCSVResult.do?fkey="+ rowData;
					a_obj.DataGrid().loadData({type: "csv", url: rowData,  progress: true,  delimiter: _Col_Separate},xe_P_GridDataLoad);				
				} else {
					//데이타 없는 경우 DataLoad 이벤트 호출(2015.03.12 KYY);
					xe_M_RealGridDataLoad(a_obj);
				}
			}
			rowData = null;
			a_obj.HideLoadingBar();
		}
		realRetrieve();
	};
	
	//행 삽입, 추가
	a_obj.InsertRow = function (rowIdx) {
		var newRow = a_obj.NewItem();
		a_obj.GridRoot().commit();
		
		//Default값 처리
		for(var i=0; i<a_obj._ColumnConfig.length; i++){
			if(a_obj._ColumnConfig[i].default_value!=null && a_obj._ColumnConfig[i].default_value!="") {
				newRow[a_obj._ColumnConfig[i].fieldName] = a_obj._ColumnConfig[i].default_value;
			}
		}
		
		//row가 없는경우 강제로 insert mode 로 변경
		if(rowIdx==0&&a_obj.RowCount()==0) rowIdx=null;	
		
		if(rowIdx==null || rowIdx>0) {
			if(rowIdx==null)
				rowIdx=(a_obj.GetRow()==0 ? 1 : a_obj.GetRow());
				
			try {
				a_obj.DataGrid().insertRow(rowIdx -1, newRow);
	    } catch (err) {
	   		_X.MsgBox(err);
	    }
		} else if (rowIdx==0) {
			try {
				a_obj.DataGrid().addRow(newRow);
			} catch (err) {
				_X.MsgBox(err);
			}
			rowIdx = a_obj.RowCount();
		}
	  	
  	a_obj.SetRow(rowIdx, a_obj._FirstColName);

  	if(typeof(x_Insert_After)!="undefined") {
				x_Insert_After(a_obj, rowIdx);
		}
		
		if(typeof(_PChild)!="undefined") {
		  if (_PChild&&a_obj==dg_child) {
				if(typeof(x_Child_Insert_After)!="undefined") {
					x_Child_Insert_After(a_obj, rowIdx);
				}
			}
		}
		return rowIdx;
	};
	
	//행 복제
	a_obj.DuplicateRow = function (rowIdx) {
		if(rowIdx==null){rowIdx=0;};
		if(rowIdx==0)
			rowIdx = a_obj.GetRow();
		var rowData = a_obj.GetRowData(rowIdx -1);
		if(rowData!=null)
		{
			a_obj.InsertRow(rowIdx +1);
			a_obj.DataGrid().updateRow(rowIdx, rowData);

			if(typeof(x_Duplicate_After)!="undefined")
				x_Duplicate_After(a_obj, rowIdx +1);

			if(typeof(_PChild)!="undefined") {
				if(typeof(x_Duplicate_Child_After)!="undefined")
					x_Duplicate_Child_After(a_obj, rowIdx +1);
			}
		}
	};
	
	//행 삭제
	a_obj.DeleteRow = function (rowIdx) {
		a_obj.GridRoot().commit(true);
		if(a_obj._IsTree) {
			if(rowIdx==null){rowIdx=(a_obj.GetRow()==0 ? 2 : a_obj.GetRowId(a_obj.GetRow()));};
			var index = a_obj.GetRowIndex(rowIdx);
			if(a_obj.GetChildCount(index)>0) {
				var descendants = a_obj.GetDescendants(index);
				for(var j=0; j<descendants.length; j++){
					a_obj._DeletedRows.push(a_obj.GridRoot().getRowData(descendants[j]-1));						
				}	
			} else{
				a_obj._DeletedRows.push(a_obj.GridRoot().getRowData(index -1));
			} 
			a_obj.DataGrid().removeRow(rowIdx);
		} else {
			if(rowIdx==null){rowIdx=(a_obj.GetRow()==0 ? 1 : a_obj.GetRow());};
			//신규 추가후 삭제되는 데이타는 저장하지 않음
			if(a_obj.DataGrid().getRowState(rowIdx -1)!="created") {
				a_obj._DeletedRows.push(a_obj.GridRoot().getRowData(rowIdx -1));
			}
			if(a_obj.GetRow()!=0) a_obj.DataGrid().removeRow(rowIdx -1);
		}
		
		if(a_obj.GetRow() != 0){
			if(xe_M_RealGridRowFocusChange(a_obj, { itemIndex:rowIdx }, a_obj.RowCount() >= rowIdx ? { itemIndex:rowIdx -1} : { itemIndex:rowIdx -2}, 'D') != false){
				xe_M_RealGridRowFocusChanged(a_obj, a_obj.RowCount() >= rowIdx ? { itemIndex:rowIdx -1} : { itemIndex:rowIdx -2 }, { itemIndex:rowIdx -1 });
			}
		}
		
		a_obj.focus();
	};

	//엑셀 보내기
	a_obj.ExcelExport = function () {
		a_obj.GridRoot().exportGrid({
	        type: "excel",
	        target: "local",
	        url: "realgrid.xls",
	        linear: false,
	        lookupDisplay : true
		});
	  //a_obj.GridRoot().excelExport();
	};

	//엑셀 가져오기
	a_obj.ExcelImport = function () {
	  a_obj.GridRoot().excelCSVImport();
	};
//============================================================== Mighty-X5 Basic Method end
 	
//============================================================== Mighty-X5 Data Control Method start
	//전체 컬럼의 옵션 반환
	a_obj.GetColumns = function () {
  };
  
  //전체 컬럼의 옵션 변경
  a_obj.SetColumns = function (columnsConfig) {
  };
  
  //컬럼의 옵션 반환
  a_obj.GetColumn = function (colName) {
  };
  
  //컬럼의 옵션 변경
  a_obj.SetColumn = function (colName, prop, value) {
  	a_obj._GridRoot.setColumnProperty( a_obj.columnByField( column ), prop, value );
  };
  
  //전체 행의 수 반환 (a_flag 가 true 일 때 행 수를 gridview 에서 가져온다.)
	a_obj.RowCount = function(a_flag) {
		return a_flag ? dg_1._GridRoot.getItemCount() : a_obj._DataGrid.getRowCount();
	};
	
	//전체 데이타 반환
	a_obj.GetData = function() {
	};
	
	//전체 데이타 갱신
	a_obj.SetData = function(items) {
	}
	
	//전체 데이타 삭제
	a_obj.ClearRows = function () {
 	};
 	
 	//행의 데이타를 반환
	a_obj.GetRowData = function(rowIdx){
		return a_obj.GridRoot().getRowData(rowIdx -1);
	};
	
	//행의 데이타를 변경
	a_obj.SetRowData = function(rowIdx, newItem) {
	};
	
	//셀의 값을 변경
	a_obj.SetCellData = function (rowIdx, colName, value, update) {
  };
  
  //그리드에 데이터 행들을 추가 (a_flag true 일 때 기존자료를 삭제 후 추가)
	a_obj.AppendRowsData = function(a_datas, a_flag){
		if(a_flag) a_obj.Reset();
		a_obj._DataGrid.addRows(JSON.parse(JSON.stringify( a_datas )), 0, -1, false);
	};
	
  //선택된 행의 번호 반환
	a_obj.GetRow = function() {
		return a_obj.GridRoot().getCurrent().itemIndex + 1;
	};
  
  //행을 선택
	a_obj.SetRow = function(rowIdx, colName, setfocus) {
		var rowCnt = a_obj.RowCount();

		if(rowIdx>rowCnt) {
			rowIdx = rowCnt;
		}
		
		a_obj._CurrentRow = rowIdx -1;

		a_obj.GridRoot().setCurrent({
    			itemIndex: rowIdx-1,
      		fieldName: colName
		});

		if(setfocus==null || setfocus) {
			a_obj.GridRoot().setFocus();
		  	a_obj.focus();
		}
	};
	
	//행 데이타의 기본형태를 반환
	a_obj.NewItem = function(){
		if(a_obj._NewItem==null){
			a_obj._NewItem = "{";
			for(var i=0; i<a_obj.GetColumnNamesForConfig().length; i++){
				a_obj._NewItem += (i==0?"":",") + '"' + a_obj.GetColumnNamesForConfig()[i] + '":""';
			}
			a_obj._NewItem += "}";
			
			eval("a_obj._NewItem=" + a_obj._NewItem);
		};
		return a_obj._NewItem;
	};
	
	//셀 값을 반환
	a_obj.GetItem = function (row,col) {
		if(typeof(col)=="string")
			col = a_obj.GetColumnIndexByName(col);
		if(col<=0)
			return "";
		return (a_obj.GetColumnTypeByName(a_obj.GetColumnNameByIndex(col))=="datetime")?toDateString(a_obj.GridRoot().getValue(row -1, col -1)):a_obj.GridRoot().getValue(row -1, col -1);
	};

	//셀의 값을 변경
	a_obj.SetItem = function (row,col,val) {
		if(typeof(col)=="string")
			col = a_obj.GetColumnIndexByName(col);
		a_obj.GridRoot().setValue(row -1, col -1, val);
		
		a_obj.GridRoot().commit();
		
		if(a_obj.DataGrid().getRowState(row -1).toLowerCase()=="none") {
			//데이타 변경 Tag 설정
			values={};
			a_obj.DataGrid().updateRow(row -1, values);
			a_obj.GridRoot().commit();
		}
		
		//데이타 동기화 처리
		if($("#"+a_obj.GetColumnNameByIndex(col)).prop('type') != "undefined" && $("#"+a_obj.GetColumnNameByIndex(col)).prop('type') != "" && $("#"+a_obj.GetColumnNameByIndex(col)).prop('type') != null){
	  	if(a_obj._Share) {
	  		xe_M_RealGridDataChange(a_obj, row-1, row, col-1, val);
	  	}
		}  	
		return true;
	};
	
	//지정된 범위의 모든 행에서 해당 컬럼의 데이타를 배열로 반환
	a_obj.GetFieldValues = function(colName, startRow, endRow){
		if(startRow==null) startRow = 0; else startRow--;
		if(endRow==null) endRow = -1; else endRow--;
		return a_obj._DataGrid.getFieldValues(colName,  startRow, endRow);
	};
	
	//지정된 범위의 모든 행에서 해당 컬럼의 데이타를 변경
	a_obj.SetFieldValues = function(colName, value, startRow, endRow) {
		if ( !_X.ArrayContains( colName, this.GetColumnNamesForConfig() ).isEle ) {
			return null;
		}
		
		var rowCnt 	 = a_obj.RowCount();
				startRow = ( startRow && startRow > 0 && startRow ) || 1;
				endRow   = ( endRow && endRow <= rowCnt && endRow ) || rowCnt;
				
		for ( var i = startRow, ii = endRow; i < ii; i++ ) {
			a_obj.SetItem( i, conName, value );
		}
	};
	
	//전체 컬럼의 이름을 반환 (대상: grid)
	a_obj.GetColumnNames = function() {
		var colNames = [];
		
		for ( var i = 0, ii = a_obj._ColumnConfig.length; i < ii; i++ ) {
	    if ( a_obj._ColumnConfig[i].visible ) {
	    	colNames.push( a_obj._ColumnConfig[i].fieldName );
	    }
		}
		return colNames;
	};
	
	//전체 컬럼의 이름을 반환 (대상: _ColumnConfig)
	a_obj.GetColumnNamesForConfig = function () {
		if ( a_obj._ColumnNames == null ) {
			a_obj._ColumnNames = a_obj.getFieldNames();
		}
		return a_obj._ColumnNames;
	};
	
	//컬럼의 이름 반환 (대상: Grid Columns)
	a_obj.GetColumnNameByIndex = function(colIdx) {
		if(colIdx<=0)
			return "";
		return a_obj.DataGrid().getFieldName(colIdx -1);
	};
	
	//컬럼의 이름 반환 (대상: _ColumnConfig)
	a_obj.GetColumnNameByIndexForConfig = function(colIdx) {
		return this._ColumnConfig[ colIdx ].fieldName;
	};
	
	//컬럼의 번호를 반환 (대상: Grid Columns)
	a_obj.GetColumnIndexByName = function(colName) {
		var fIdx = a_obj.DataGrid().getFieldIndex(colName);
		if(fIdx<0) {
			if(_X.IsAdmin()) {
  			alert("REALGRIDS ERROR(관리자용) !\n\n" + colName + " 칼럼이 존재하지 않습니다.");
			}
			return "";
		}
		
		return a_obj.DataGrid().getFieldIndex(colName) + 1;
	};
	
	//컬럼의 번호 반환 (대상: _ColumnConfig)
	a_obj.GetColumnIndexByNameForConfig = function(colName) {
		for ( var i = 0, ii = this._ColumnConfig.length; i < ii; i++ ) {
			if ( this._ColumnConfig[i].fieldName === colName ) {
				return i + 1;
			}
		}
	};
	
	//컬럼의 유형 반환
	a_obj.GetColumnTypeByName = function(colName) {
		if(colName==null || colName=="")
			return null;

		var dataField = a_obj.getDataFields();
		var columnIndex = a_obj.GetColumnIndexByName(colName);
		return dataField[columnIndex-1].dataType;
	};
	
	//변경된 데이타 반환 (sql 변환)
	a_obj.GetSQLData = function () {
		a_obj.GridRoot().commit();
		var rows = a_obj.GetAllStateRows();
		var row = 0;

		if(a_obj._DeletedRows.length<=0 && rows.updated.length<=0 && rows.created.length<=0)
			return "";
		var sqlDatas = a_obj.subSystem + _Field_Separate + a_obj.sqlFile + _Field_Separate + a_obj.sqlKey;
		//삭제구문 생성
		for(var i=0; i<a_obj._DeletedRows.length; i++)
		{
			sqlDatas += _Col_Separate + "D" + _Field_Separate + x_GetGridPureData(a_obj, a_obj._DeletedRows[i]) ;
		}

		//Update구문 생성
		for(var i=0; i<rows.updated.length; i++)
		{
			row = (a_obj._IsTree?a_obj.GetRowIndex(rows.updated[i])-1:rows.updated[i]);			
			sqlDatas += _Col_Separate + "U" + _Field_Separate + (a_obj._IsTree?x_GetGridPureData(a_obj, a_obj.GridRoot().getRowData(row)):x_GetGridPureData(a_obj, a_obj.GridRoot().getRowData(row))) ;
		}
		//Insert구문 생성
		for(var i=0; i<rows.created.length; i++)
		{
			row = (a_obj._IsTree?a_obj.GetRowIndex(rows.created[i])-1:rows.created[i]);			
			sqlDatas += _Col_Separate + "I" + _Field_Separate + (a_obj._IsTree?x_GetGridPureData(a_obj, a_obj.GridRoot().getRowData(row)):x_GetGridPureData(a_obj, a_obj.GridRoot().getRowData(row)));
			
		}
		return sqlDatas;
	};
	
	//변경된 행을 반환 (I, U)
	a_obj.GetAllStateRows = function() {
		return a_obj.DataGrid().getAllStateRows();
	};
	
	//변경된 행을 반환 (I, U, D)
	a_obj.GetChangedData	= function(){
		a_obj.GridRoot().commit();
		var rows = a_obj.GetAllStateRows();
		if(a_obj._DeletedRows.length<=0 && rows.updated.length<=0 && rows.created.length<=0)
			return null;
		
		var cData = [];
		//Delete
		for(var i=0; i<a_obj._DeletedRows.length; i++)
		{
			var rowData = {};
			rowData.job = "D";
			rowData.idx	= 0;
			rowData.data = a_obj._DeletedRows[i];
			cData.push(rowData);
		}
		//Update
		for(var i=0; i<rows.updated.length; i++)
		{
			var rowData = {};
			rowData.job = "U";
			rowData.idx	= (a_obj._IsTree?a_obj.GetRowIndex(rows.updated[i])-1:rows.updated[i]) + 1;
			rowData.data = a_obj.GridRoot().getRowData(rows.updated[i]);
			cData.push(rowData);
		}
		//Insert구문 생성
		for(var i=0; i<rows.created.length; i++)
		{
			var rowData = {};
			rowData.job  = "I";
			rowData.idx	= (a_obj._IsTree?a_obj.GetRowIndex(rows.created[i])-1:rows.created[i]) + 1;
			rowData.data = a_obj.GridRoot().getRowData(rows.created[i]);
			cData.push(rowData);
		}
		return cData;
	};
	
	//행의 상태값 반환
	a_obj.GetRowState = function(row) {
		var state = a_obj._DataGrid.getRowState( row - 1 );
		return a_obj.GetMightyState( state );
	};
	
	//행의 상태값 변경 (지정값)
	a_obj.SetRowsState = function(a_rows, a_newstate, a_force, a_rowEvents) {
		a_newstate  = this.GetGridState( a_newstate );
		a_force     = typeof a_force === "boolean" ? a_force : true;
		a_rowEvents = typeof a_rowEvents === "boolean" ? a_rowEvents : false;
		
		for ( var i = 0, ii = a_rows.length; i < ii; i++ ) {
	  	a_rows[i] = a_rows[i] - 1;
	  }
	  
		a_obj._DataGrid.setRowStates( a_rows, a_newstate, a_force, a_rowEvents ); 
	};
	
	//행의 상태값 변경 (old값 => new 값)
	a_obj.SetRowsStateOldNew = function(a_oldstate, a_newstate, a_force) {
		a_newstate = this.GetGridState( a_newstate );
		a_force    = typeof a_force === "boolean" ? a_force : true;
		
		for ( var i = 0, ii = typeof a_oldstate === "string" ? 1 : a_oldstate.length; i < ii; i++ ) {
			var oldState;
			
			if ( typeof a_oldstate === "string" ) {
				oldState = this.GetGridState( a_oldstate );
			}
			else {
				oldState = this.GetGridState( a_oldstate[i] );
			}
			
			var rows = a_obj._DataGrid.getStateRows( oldState );
			a_obj._DataGrid.setRowStates( rows, a_newstate, a_force ); 
		}
	};
	
	//지정한 상태의 행의 수 반환
	a_obj.GetLengthByState = function(a_div) {
		if(a_div == null || a_div == "") a_div = 'A';
		var rows = a_obj.GetAllStateRows();
		
		if(a_div == 'C') return rows.created.length;
		if(a_div == 'U') return rows.updated.length;
		if(a_div == 'D') return a_obj._DeletedRows.length;
		if(a_div == 'A') return rows.created.length+rows.updated.length+a_obj._DeletedRows.length;	
	};
	
	//현재 적용중인 그리드의 상태값 표기명을 가져온다.
  a_obj.GetGridState = function(a_state) {
		return a_state === "I" ? "created" :
         	 a_state === "U" ? "updated" :
         	 a_state === "D" ? "deleted" : "none";
  };

  //현재 적용중인 그리드의 상태값을 mighty의 표기명으로 가져온다.
  a_obj.GetMightyState = function(a_state) {
		return a_state === "created" ? "I" :
					 a_state === "updated" ? "U" :
					 a_state === "deleted" ? "D" : "N";
  };
  
  //전체 행의 상태값 초기화
	a_obj.ResetRowsStates = function() {
		a_obj._DeletedRows.length = 0;
		//treeview 적용시 오류 발생...
    if(a_obj._IsTree)a_obj.DataGrid().clearRowStates(true,false);
    else  a_obj.DataGrid().clearRowStates(false,false);
	};
	
	//데이타 변경여부 체크
	a_obj.IsDataChanged = function() {
		var sqlData = a_obj.GetSQLData();
		if((sqlData==null || sqlData.length==0)&&(a_obj._DeletedRows.length==0)) return false;
		return true;
	};
	
	//셀의 콤보박스를 설정
	a_obj.SetCombo = function(colName, datas, abNull){
		if(abNull==null) abNull=false;
		var colObj = a_obj.GridRoot().columnByField(colName);
		var c_values = [];
		var c_labels = [];
		if(abNull) {
				c_values.push('');
				c_labels.push('');
		}
		
		for(var i=0; i<datas.length; i++) {
			if(typeof(datas[0].code)!="undefined") {
				c_values.push(datas[i].code);
				c_labels.push(datas[i].label);
			} else if(typeof(datas[0].CODE)!="undefined") {
				c_values.push(datas[i].CODE);
				c_labels.push(datas[i].LABEL);
			} else {
				c_values.push(datas[i][0]);
				c_labels.push(datas[i][1]);
			}
		}
		
	    if (colObj) {
			colObj.values = c_values;
	        colObj.labels = c_labels;
	        a_obj.GridRoot().setColumn(colObj);
	    }
	};
	
	//필수 입력 항목 체크
	a_obj.MustInputCheck = function() {
		var rows = a_obj.DataGrid().getStateRows("created");
		for(var i=0; i<rows.length; i++) {
			for(var j=0; j<a_obj._ColumnConfig.length; j++) {
				if(a_obj._ColumnConfig[j].must_input=="Y") {
					var fieldVal = a_obj.GetItem(rows[i]+1, a_obj._ColumnConfig[j].fieldName);
					if(fieldVal==null || fieldVal=="") {
						_X.MsgBox("[" + a_obj._ColumnConfig[j].header.text + "] 항목은 필수입력 항목입니다.");
						if(a_obj._Share) {
								$("#"+a_obj._ColumnConfig[j].fieldName).focus();
						} else {
							if(a_obj._ColumnConfig[j].visible==true) {
								a_obj.SetRow(rows[i]+1, a_obj._ColumnConfig[j].fieldName);
							}	
						}
						return rows[i]+1;
					}
				}
			}			
		}		

		var rows = a_obj.DataGrid().getStateRows("updated");
		for(var i=0; i<rows.length; i++) {
			for(var j=0; j<a_obj._ColumnConfig.length; j++) {
				if(a_obj._ColumnConfig[j].must_input=="Y") {
					var fieldVal = a_obj.GetItem(rows[i]+1, a_obj._ColumnConfig[j].fieldName);
					if(fieldVal==null || fieldVal=="") {
						_X.MsgBox("[" + a_obj._ColumnConfig[j].header.text + "] 항목은 필수입력 항목입니다.");
						if(a_obj._Share) {
							if(a_obj._ColumnConfig[j].visible==true) {
								a_obj.SetRow(rows[i]+1, a_obj._ColumnConfig[j].fieldName);
							}	
							$("#"+a_obj._ColumnConfig[j].fieldName).focus();
						} else {
							if(a_obj._ColumnConfig[j].visible==true) {
								a_obj.SetRow(rows[i]+1, a_obj._ColumnConfig[j].fieldName);
							}	
						}
						return rows[i]+1;
					}
				}
			}			
		}	
		return 0;
	};
	
	//입력 길이 체크
	a_obj.ByteInputCheck = function() {
		var rows = a_obj.DataGrid().getStateRows("created");
		for(var i=0; i<rows.length; i++) {
			for(var j=0; j<a_obj._ColumnConfig.length; j++) {
		  		if(a_obj._ColumnConfig[j].byte_input=="Y") {
		  			var fieldVal = a_obj.GetItem(rows[i]+1, a_obj._ColumnConfig[j].fieldName);
		  			var byte_len = a_obj._ColumnConfig[j].byte_len;
		  			
		  			if(!_X.ChkLen(String(fieldVal), byte_len)) {
							_X.MsgBox("[" + a_obj._ColumnConfig[j].header.text + "] 항목의 문자열 길이가 허용된 길이보다 깁니다.["+ byte_len+"]");
							if(a_obj._Share) {
								if(a_obj._ColumnConfig[j].visible==true) {
									a_obj.SetRow(rows[i]+1, a_obj._ColumnConfig[j].fieldName);
								}	
								$("#"+a_obj._ColumnConfig[j].fieldName).focus();
							} else {
								if(a_obj._ColumnConfig[j].visible==true) {
									a_obj.SetRow(rows[i]+1, a_obj._ColumnConfig[j].fieldName);
								}	
							}
							return rows[i]+1;
						}
		  		}
		  	}
		}
	
		var rows = a_obj.DataGrid().getStateRows("updated");
		for(var i=0; i<rows.length; i++) {
			for(var j=0; j<a_obj._ColumnConfig.length; j++) {
		  		if(a_obj._ColumnConfig[j].byte_input=="Y") {
		  			var fieldVal = a_obj.GetItem(rows[i]+1, a_obj._ColumnConfig[j].fieldName);
		  			var byte_len = a_obj._ColumnConfig[j].byte_len;
		  			
		  			if(!_X.ChkLen(String(fieldVal), byte_len)) {
							_X.MsgBox("[" + a_obj._ColumnConfig[j].header.text + "] 항목의 문자열 길이가 허용된길이보다 깁니다.["+ byte_len+"]");
							if(a_obj._Share) {
								if(a_obj._ColumnConfig[j].visible==true) {
									a_obj.SetRow(rows[i]+1, a_obj._ColumnConfig[j].fieldName);
								}	
								$("#"+a_obj._ColumnConfig[j].fieldName).focus();
							} else {
								if(a_obj._ColumnConfig[j].visible==true) {
									a_obj.SetRow(rows[i]+1, a_obj._ColumnConfig[j].fieldName);
								}	
							}
							return rows[i]+1;
						}
		  		}
		  	}
	  	}	
		return 0;
	};
	
	//정렬 비교 함수 생성
	a_obj.SetSortComparer = function (colName, a_div) {
	};
	
	//정렬 해제
	a_obj.ResetSort = function () {
	};
//============================================================== Mighty-X5 Data Control Method end

//============================================================== Mighty-X5 tree Method start
	//TreeView.Expand
	a_obj.TreeExpand = function(a_row, a_recursive) {
	    a_obj.GridRoot().expand(a_row-1, a_recursive);
	};
	 
	//TreeView.Collapse
	a_obj.TreeCollapse= function(a_row, a_recursive) {
	    a_obj.GridRoot().collapse(a_row-1, a_recursive);
	};
	
	//트리 확장 (선택레벨까지)
	a_obj.TreeExpandLevel = function(collapseLevel) {
	};
	
	//TreeView.ExpandAll
	a_obj.TreeExpandAll = function(a_row, a_recursive) {
	    a_obj.GridRoot().expandAll();
	};
	 
	//TreeView.CollapseAll
	a_obj.TreeCollapseAll= function(a_row, a_recursive) {
	    a_obj.GridRoot().collapseAll();
	};
	
	//(삭제예정!) tree 데이타 조회
	a_obj.TreeChildRetrieve = function (args, a_row) {
		a_obj.GridRoot().commit();
		a_obj.ShowLoadingBar();
		a_obj._IsRetrieving = true;

		var rowData = _X.XmlSelect(a_obj.subSystem, a_obj.sqlFile, a_obj.sqlKey, args, "jsontreestream", "jsontreestream");
		if(rowData!=null && rowData!=""){
			//a_obj.DataGrid().setRows(rowData);
			//a_obj.DataGrid().setJsonRows(rowData, "rows");
			rowData = "/GetJSONResult.do?fkey="+ rowData;
			var rowId = a_obj.GetRowId(a_row);
			a_obj.TreeExpand(a_row);
			var childrows = a_obj.GetChildren(a_row);
			for(var i=childrows.length-1; i>=0; i--){
				a_obj.DataGrid().removeRow(a_obj.GetRowId(childrows[i]));
			}
			a_obj.DataGrid().loadData({type: "json", url: rowData,  start: 1, tree: "tree",  fillMode: "append", progress: true, parent: rowId});
			//var rowDatas = JSON.stringify(a_obj.GetRowData(index+1));
		  rowData = null;
		}
		a_obj.HideLoadingBar();
	};
	
	//(삭제예정!) TreeView 현재의 itemindex 의  rowId 값을 가져온다.
	a_obj.GetRowId = function (a_row) {
		return a_obj.GridRoot().getRowId(a_row-1); 
	};
	
	//(삭제예정!) Row Index return
	a_obj.GetRowIndex= function(a_rowId) {
	    return a_obj.GridRoot().getItemIndex(a_rowId)+1;
	};

	//부모행 번호를 반환
	a_obj.GetParent = function (a_row) {
		var rowId = a_obj.GetRowId(a_row);
		return a_obj.GetRowIndex(a_obj.DataGrid().getParent(rowId));
	};
	
	//(삭제예정!) TreeView.GetParent(rowId) 함수로 자식 행들의 rowId를 가져옵니다.
	a_obj.GetParentRowId = function (a_row) {
		var rowId = a_obj.GetRowId(a_row);
		returna_obj.DataGrid().getParent(rowId);
	};

	//자식행들의 행번호를 반환
	a_obj.GetChildren = function (a_row) {
		var rowId = a_obj.GetRowId(a_row);
	    var child=a_obj.DataGrid().getChildren(rowId);
	    var newchild = child;
		for(var i=0; i<child.length; i++){
			newchild[i] = a_obj.GetRowIndex(child[i]);
		}
	    return newchild;
	};

	//(삭제예정!) 
	a_obj.GetChildrenRowId = function (a_row) {
		var rowId = a_obj.GetRowId(a_row);
	    return a_obj.DataGrid().getChildren(rowId);
	};

	//모든 자식행들의 행번호를 반환
	a_obj.GetDescendants = function(a_row) {
		var rowId = a_obj.GetRowId(a_row);
		var descendants = a_obj.DataGrid().getDescendants(rowId);
		var newdescendants = descendants;
		
		if(newdescendants == null) return newdescendants; 
		
		for(var i=0; i<descendants.length; i++){
			newdescendants[i] = a_obj.GetRowIndex(descendants[i]);
		}
	    return newdescendants;
	};
	 
	a_obj.GetDescendantsRowId= function(a_row) {
		var rowId = a_obj.GetRowId(a_row);
		return a_obj.DataGrid().getDescendants(rowId);
	};
	
	//(삭제예정!) TreeView.GetAncestors(rowId) 함수로 모든 부모 행들의 rowId를 가져옵니다.
	a_obj.GetAncestors = function(a_row) {
		var rowId = a_obj.GetRowId(a_row);
		var ancestors = a_obj.DataGrid().getAncestors(rowId);
		var newancestors = ancestors;
		for(var i=0; i<ancestors.length; i++){
			newancestors[i] = a_obj.GetRowIndex(ancestors[i]);
		}
	    return newancestors;
	};
	
	//(삭제예정!) TreeView.GetAncestors(rowId) 함수로 모든 부모 행들의 rowId를 가져옵니다.
	a_obj.GetAncestorsRowId = function(a_row) {
		var rowId = a_obj.GetRowId(a_row);
		return a_obj.DataGrid().GetAncestors(rowId);
	};

	//자식의 행수를 반환
	a_obj.GetChildCount= function(a_row) {
		var rowId = a_obj.GetRowId(a_row);
	    return a_obj.DataGrid().getChildCount(rowId);
	};

	//(삭제예정!) Parent Count return
	a_obj.GetDescendantCount= function(a_row) {
		var rowId = a_obj.GetRowId(a_row);
	    return a_obj.DataGrid().getDescendantCount(rowId);
	};

	//현재 레벨 추가
	a_obj.AddCurrentLevel = function (a_row) {
	};
	
	//자식 레벨의 행을 추가
	a_obj.AddChildLevel = function (a_row) {
		var current = a_obj.GridRoot().getCurrent();
		var newRow = a_obj.NewItem();
		var rowId = a_obj.GetRowId(a_row);
		//Default값 처리
		for(var i=0; i<a_obj._ColumnConfig.length; i++){
			if(a_obj._ColumnConfig[i].default_value!=null && a_obj._ColumnConfig[i].default_value!="") {
				newRow[a_obj._ColumnConfig[i].fieldName] = a_obj._ColumnConfig[i].default_value;
			}
		}
		var child = a_obj.DataGrid().addChildRow(rowId, newRow, 0);
		a_obj.TreeExpand(a_row);
        current.itemIndex = a_obj.GridRoot().getItemIndex(child);
		a_obj.GridRoot().setCurrent(current);
		a_obj.GridRoot().setFocus();
		return current.itemIndex + 1;
	};
	
	//(삭제예정!) 지정한 트리 행의 자식 행을 추가한다.
	a_obj.InsertChildRow = function (a_row, ab_child) {
		var current = a_obj.GridRoot().getCurrent();
		var newRow = a_obj.NewItem();
		var rowId = a_obj.GetRowId(a_row);
		var child = null; 
		if(ab_child==null) ab_child=false;

		//Default값 처리
		for(var i=0; i<a_obj._ColumnConfig.length; i++){
			if(a_obj._ColumnConfig[i].default_value!=null && a_obj._ColumnConfig[i].default_value!="") {
				newRow[a_obj._ColumnConfig[i].fieldName] = a_obj._ColumnConfig[i].default_value;
			}
		}
		
		if (a_row>=1) {
			if(ab_child) {
				child = a_obj.DataGrid().insertChildRow(rowId, 0, newRow, 0, true);
				a_obj.TreeExpand(a_row);
			} else {
				var parent = a_obj.DataGrid().getParent(rowId);
		    	var childrows=a_obj.DataGrid().getChildren(parent);
				for(var i=0; i<childrows.length; i++){
					if(rowId==childrows[i]) break;
				}
				child = a_obj.DataGrid().insertChildRow(parent, i, newRow, 0, true);
			}
	    } else {
	    	child = a_obj.DataGrid().insertChildRow(-1, 0, NewRow,0);
	    }
        current.itemIndex = a_obj.GridRoot().getItemIndex(child);
		a_obj.GridRoot().setCurrent(current);
		a_obj.GridRoot().setFocus();
		return current.itemIndex + 1;
	};
//============================================================== Mighty-X5 tree Method end	
	
//============================================================== Mighty-X5 cssStyle Method start
	//셀의 좌측부터 지정한 수만큼의 컬럼을 스크롤바의 이벤트로 부터 고정
	a_obj.SetFixed = function(fixCount) {
	    var options = new RealGrids.FixedOptions();
	    options.colCount = fixCount;
	    options.colBarWidth = 2;
	    options.resizable = true;
	    options.ignoreColumnStyles = false;
	    //options.editable = false; 
	    a_obj.GridRoot().fixedOptions(options);
	};
	
	//cssRules 생성
  a_obj.MakeCssRules = function (a_cssKey, a_ruls) {
  };
  
  //전체 스타일시트 삭제
  a_obj.RemoveAllStyle = function () {
  };
  
  //스타일시트 삭제
  a_obj.RemoveStyles = function (cssKey) {
	};
	
	//스타일시트 생성 및 cssRules 추가
	a_obj.CreateStylesheet = function (a_cssKey, a_cssRules) {
	};
  
	//스타일변경 전처리 (처리 객체 반환)
	a_obj.SetStylePreprocessing = function (cssKey, a_ruls, isColNames) {;
	};
	
	//행의 스타일 변경
	a_obj.SetRowStyle = function (rowIdx, a_styles) {
		var id = a_obj.id + "-r" + rowIdx + "-rowstyle";
		
		a_obj._GridRoot.addCellStyle( id, a_styles, true );
		a_obj._GridRoot.setCellStyle( rowIdx - 1, -1, id );
	};
	
	//컬럼의 스타일 변경
	a_obj.SetColumnStyle = function (colName, a_styles) {
		var id = a_obj.id + "-" + colName + "-columnstyle";
		
		a_obj._GridRoot.addCellStyle( id, a_styles, true );
		
		var rows = [];
		
		for ( var i = 0, ii = a_obj.RowCount(); i < ii; i++ ) {
			rows.push(i);
		}
		
		a_obj._GridRoot.setCellStyles( rows, colName, id );
	};
	
	//셀의 스타일 변경
	a_obj.SetCellStyle = function (rowIdx, colName, a_styles) {
		var id = a_obj.id + "-r" + rowIdx + "-" + colName + "-cellstyle";
		
		a_obj._GridRoot.addCellStyle( id, a_styles, true );
		a_obj._GridRoot.setCellStyle( rowIdx - 1, colName, id );
	};
	
	//컬럼 visible 설정
	a_obj.SetColumnVisible = function (a_column, a_flag, a_div) {
		var columnName="";
		if(a_div==null||typeof(a_div)=="undefined"){
			a_div = 'F';
		}
		if(a_div == 'F'){
			// 필드로(field)..
			columnName = a_obj.columnByField(a_column);
		}else{
			// name
			columnName = a_obj.columnByName(a_column);
		}
		//없으면 해당 칼럼의 상태값을 가져옴
		if(a_flag==null||typeof(a_flag)=="undefined"){
			a_flag = !a_obj._GridRoot.getColumn(columnName, "visible");	
		}
		a_obj.GridRoot().setColumnProperty(columnName, "visible", a_flag);
	};	
//============================================================== Mighty-X5 cssStyle Method end

//============================================================== Mighty-X5 auto cell start
	//동적 행 스타일 변경
	a_obj.SetAutoRowStyle = function (colNames, comps, values, a_styles) {
		if ( typeof colNames === "string" ) {
			if ( comps === "==") {
				comps = "=";
			}
			
			var styles = "",
					cnt = 0;
					
			for ( var prop in a_styles ) {
				if ( !a_styles.hasOwnProperty( prop ) ) {
					continue;
				}
				
				var appProp  = prop;
						appStyle = a_styles[ prop ];
						
				if ( cnt ) {
					styles += ";";
				}
				
				if ( appProp == "font-weight" ) {
					appProp = "fontBold";
					
					if ( appStyle == "bold" ) {
						appStyle = "true";
					}
				}
				
				styles += appProp + "=" + appStyle;
				cnt++;
			}

			a_obj._GridRoot.setStyles({
			    body: {
			        dynamicStyles: [{
		            criteria: "values['" + colNames + "'] " + comps + " '" + values + "'",
					      styles: styles
			        }]
			    }
			});
		}
		else {
			var criteria = [],
					styles   = [];
					
			for ( var i = 0, ii = colNames.length; i < ii; i++ ) {
				if ( comps[i] === "==" ) {
					comps[i] = "=";
				}
				
				criteria[ criteria.length ] = "values['" + colNames[i] + "'] " + comps[i] + " '" + values[i] + "'";
			}
			
			for ( var i = 0, ii = a_styles.length; i < ii; i++ ) {
				var index = styles.length;
				styles[ index ] = "";
				
				for ( var prop in a_styles[i] ) {
					if ( !a_styles[i].hasOwnProperty( prop ) ) {
						continue;
					}

					if ( styles[ index ] ) {
						styles[ index ] += ";";
					}
					
					var appProp  = prop;
							appStyle = a_styles[i][ prop ];
					
					if ( appProp == "font-weight" ) {
						appProp = "fontBold";
						
						if ( appStyle == "bold" ) {
							appStyle = "true";
						}
					}
					
					styles[ index ] += appProp + "=" + appStyle;
				}
			}

			a_obj._GridRoot.setStyles({
	        body: {
	            dynamicStyles: [{
	                criteria: criteria,
						      styles: styles
	            }]
	        }
	    });
	  }
	};
	
	//동적 셀 스타일 변경
	a_obj.SetAutoCellStyle = function (colName, comps, values, a_styles) {
	}
	
	//동적 셀값 설정
	a_obj.SetAutoValue = function (colName, a_callbackFunc, isUpdate) {
	};
	
	//동적 readonly 설정
	a_obj.SetAutoReadOnly = function (colName, a_collbackFunc, appStyle) {
	};
	
	//셀 마스크 설정
	a_obj.SetMaskValue = function (colName, a_callbackFunc) {
	};
//============================================================== Mighty-X5 auto cell end

//============================================================== other start
	/**
	 * fn_name     : SetCheckBar
	 * Description : 체크바 설정
	 * param       : a_flag      : display 여부
	 *							 a_exclusive : true 체크박스 false 라디오박스
	 *							 a_head      : 헤더 체크박스 표시여부
	 *               a_showGroup : 그룹된 row에 체크박스 표시여부
	 *							 a_checkColor: 체크된 행의 색
	 * Statements  : 
	 *
	 *   since   		     author          Description
	 *  ------------    --------    ---------------------------
	 *   2015.05.27  	   이상규          최초 생성
	 */
	a_obj.SetCheckBar = function (a_flag, a_exclusive, a_head, a_showGroup, a_checkColor) {
		if(a_flag==null) a_flag=true;
		if(a_exclusive==null) a_exclusive=false;
		if(a_head==null) a_head=true;
		if(a_showGroup==null) a_showGroup=false;
		a_obj.GridRoot().checkBar({
	        visible: a_flag,
	        exclusive: a_exclusive,
	        showAll: a_head,
	        showGroup: a_showGroup
	  });
	  if(a_checkColor != null && a_checkColor != "") {
	  	a_obj._GridRoot.setStyles({body: {dynamicStyles: [{criteria: "checked", styles: "background=" + a_checkColor}]}});
	  }
	};
	
	//체크된 행번호를 반환 (a_flag가 true 일 때 화면에 보이는 순서대로 row번호를 가져온다)
	a_obj.GetCheckedRows = function (a_flag) {   
		var arr = a_flag ? a_obj._GridRoot.getCheckedItems() : a_obj._GridRoot.getCheckedRows();
		for ( var i = 0, ii = arr.length; i < ii; i++ ) {
			arr[i] = arr[i] + 1;
		}
		return arr;
	};
	
	//지정한 행을 체크한다
	a_obj.SetCheckRows = function (a_rows, a_flag) {
		if ( a_flag == null ) {
			a_flag = true;
		}
		
		if ( typeof a_rows === "number" ) {
			a_rows = [a_rows - 1];
		}
		else {
			for ( var i = 0, ii = a_rows.length; i < ii; i++ ) {
				a_rows[i] = a_rows[i] - 1;
			}
		}
		return a_obj._GridRoot.checkRows(a_rows, a_flag);
	};
	
	//자식행을 체크 또는 해제
	a_obj.SetCheckChild =  function (a_row, a_checked, a_recursive, a_visibleOnly) {
		if(a_checked==null) a_checked=true;
		if(a_recursive==null) a_recursive=false;
		if(a_visibleOnly==null) a_visibleOnly=false;
		a_obj.GridRoot().checkChildren(a_row-1, a_checked, a_recursive, a_visibleOnly);
	};
	
	//전체 행을 체크 또는 해제
	a_obj.SetCheckAll = function (a_flag) {
		if(a_flag == null) a_flag = true;
		a_obj._GridRoot.checkAll(a_flag, false);
	};
	
	//체크된 행 인지 확인 (tree 인경 rowid 값 반환)
	a_obj.IsCheckedRow = function (a_row) {
		if(a_obj._IsTree) a_row = a_obj.GetRowId(a_row); else  a_row=a_row-1;
		return a_obj._GridRoot.isCheckedRow(a_row);
	};
	
	//footer 설정
	a_obj.SetFooter = function (a_flag) {
		if (a_flag == null) {
			a_flag = true;
		}
		a_obj._GridRoot.setFooter({ visible: a_flag });
	};
	
	//컬럼 header 명칭 변경	
	a_obj.SetColumnHeaderName = function (a_column, a_name, a_div) {
		var columnName = "";
		if(a_div==null||typeof(a_div)=="undefined"){
			a_div = 'F';
		}
		
		if(a_div == 'F'){
			// 필드로(field)..
			columnName = a_obj.columnByField(a_column);
		}else{
			// name
			columnName = a_obj.columnByName(a_column);
		}
		a_obj.GridRoot().setColumnProperty(columnName, "header", a_name);
	};	
	
	//컬럼 header 글자색 변경
	a_obj.SetColumnHeaderColor = function (a_column, a_color, a_div) {
		var columnName = "";
		if(a_div==null||typeof(a_div)=="undefined"){
			a_div = 'F';
		}
		
		if(a_div == 'F'){
			columnName = a_obj.columnByField(a_column);
		}else{
			columnName = a_obj.columnByName(a_column);
		}
		
		if(a_color==null||typeof(a_color)=="undefined"){
			a_color = "#FF4848";
		}
		a_obj.GridRoot().setColumnProperty(columnName, "header", a_name);
		a_obj.GridRoot().setColumnProperty(aColumn, 'header', {styles:{foreground:a_color}});
	};

	//지정된 요소의 값과 동일한 값을가진 셀로 포커스를 이동시킨 후 그 결과값을 반환
	a_obj.SetFocusByElement = function (a_ele, a_fields){
		var valuesArr = new Array();
		var srcValue;

		if ( typeof a_ele === "string" ) {
			srcValue = $('#' + a_ele).get(0).value;
		}
		else {
			srcValue = a_ele.value;
		}
		
		for(var i=0; i<a_fields.length; i++){
			valuesArr.push(srcValue);
		}
		if(srcValue == null || srcValue == ""){
				return;	
		}
		var caseSensitive = "false";	//대소문자 구분
		var partialMatch = "partial";	//부분일치/전체일치 구분
		var wrap = "wrap";				    //row의 끝까지 검색시 반복/미반복 구분
		
	
		var options = {fields:  a_fields,
								values : valuesArr,
								caseSensitive: caseSensitive,
								partialMatch: partialMatch,
								wrap: wrap,
								select: false,
								allFields : false };
		
		options.startIndex = (a_obj.RowCount() == a_obj.GetRow() ? 1 : a_obj.GetRow());
		var index = a_obj.GridRoot().searchItem(options);
		if (index < 0) {
		  return;
		}
		
		var rowDatas = JSON.stringify(a_obj.GetRowData(index+1));
		//후처리 작업을 있을 때 return 100
		var result = 0;
		$.each($.parseJSON(rowDatas), function(idx, obj) {
			for(var i=0; i<a_fields.length; i++){
				if(idx==a_fields[i]&&obj.toString().indexOf(srcValue)>=0)
				{	
					var column = a_obj.columnByField(a_fields[i]);
					if(column.visible) {
						a_obj.SetRow(index+1, a_fields[i]);	
					} else {
						a_obj.SetRow(index+1);
					}
					a_ele.focus();
					result = 100;
					break;
				};		
			};
		});
		return result;
	};
	
	//지정된 값과 동일한 값을가진 셀로 포커스를 이동시킨 후 그 결과값을 반환 (flag = 존재여부, rowIdx = 행번호)
	a_obj.SetFocusByValue = function (a_fields_arr, a_values_arr){
		var options = new Object();
		
		options.fields = JSON.parse(JSON.stringify( a_fields_arr )); 
		options.values = JSON.parse(JSON.stringify( a_values_arr )); 
		
		var ls_item = -1;
		
		try {
			ls_item = a_obj._GridRoot.search(options);
		} catch(err) {
		}
		
		var lo_obj = new Object();
		lo_obj.flag = ls_item == -1 ? false : true;
		lo_obj.rowIdx = ls_item + 1;
		return lo_obj;
	};
	
	//중복된 값이 있는 행번호를 반환 (excludeRows: 제외행번호)
	a_obj.GetDuplicateRowByValue = function (values, colNames, excludeRows, flash){
	};
	
	//중복된 키값이 있는 행번호를 반환
	a_obj.GetDuplicateRowByKey = function (keyValues, keyColNames) {
	};
	
	//그리드 전체 readOnly
	a_obj.GridReadOnly = function (a_flag) {
		if (a_flag == null) {
			a_flag = true;
		}
		a_obj._GridRoot.setEditOptions({ readOnly: a_flag });
	};
	
	//방향키 누를때 행 추가
	a_obj.GridAppendable = function (a_flag) {
		if (a_flag == null) {
			a_flag = true;
		}
		a_obj._GridRoot.setEditOptions({ appendable: a_flag });
	};
	
	//summary group 정의
	a_obj.SetGroup = function (a_columns, ab_foot) {
		if(ab_foot=null) ab_foot=true;
		a_obj.SetFooter(ab_foot);
		a_obj.GridRoot().setOptions({stateBar: {visible: false},
							        sorting: {
							            enabled: false,
							            style: RealGrids.SortStyle.INCLUSIVE,
							            handleVisibility: "hidden"
							        },
							        display: {
							        },
							        header: {
							        },
							        footer: {
							            visible: true
							        },
								    rowGroup: {mergeMode: true,
														 expandedAdornments: "footer",
														 collapsedAdornments: "footer",
														 mergeExpander: ""
									}
	  });
		a_obj.GridRoot().groupBy(JSON.parse(JSON.stringify(a_columns)));
	};
//============================================================== other end
	/*=== GirdLayoutComplete ===*/
	if(typeof(xe_GridLayoutComplete)!="undefined") {
		a_obj._IsLayoutCompleted = true;
		
		var allGridLoaded = true;
		if(window._GridRequestCnt>window._Grids.length) {
			allGridLoaded = false;
		} else {
			for(var i=0; i<window._Grids.length; i++){
				if(window._Grids[i]==null || typeof(window._Grids[i]._IsLayoutCompleted)=="undefined" || !window._Grids[i]._IsLayoutCompleted) {
					allGridLoaded = false;
					break;
				}
			}
		}

		xe_GridLayoutComplete(a_obj, allGridLoaded);
		//load 데이타시 횟수만큼count 만큼 증가하여 일단 막아둠
		//if(a_obj._IsTree) a_obj.GridRoot().setIndicator({displayValue:"row"}); else a_obj.GridRoot().setIndicator({displayValue:"index"});

		if(allGridLoaded) {
			_X.UnBlock();
		}
	}
	
	//이전버전 호환용(2015.09.14 KYY)
	if(typeof(RealGrid_Deprecated)!="undefined") {
		RealGrid_Deprecated(a_obj);
	}
};

//============================================================== _X  start
//Grid Layout 파일 생성
_X.CreateRealGridFile = function(subSystem, pgmCode){	
	_X.AjaxCall(subSystem.toLowerCase(), "ReadGridFile", "array", pgmCode);

	if(_X_AjaxResult==null)
		return "";
		
	if(_X_AjaxResult.result=="ERROR"){
		_X.MsgBox(_X_AjaxResult.datas);
		return "";
	} else if(_X_AjaxResult.result=="OK"){			
		_X.MsgBox("Grid정보 파일 생성이 완료되었습니다.");
		if(_X_AjaxResult.datas==null)
			return "";
		else
		{
			return _X_AjaxResult.datas;
		}
	}
};

//전체 그리드 변경유무 확인
_X.IsDataChangedAll = function(ab_msg){
	var sqlData = "";
	if(ab_msg==null) ab_msg==true;
	for(var i=0; i<window._Grids.length; i++){
		if (window._Grids[i]._Updatable) {
			var cData = window._Grids[i].GetSQLData();
			if(cData!=null && cData!="")
				sqlData += (sqlData=="" ? "" : _Row_Separate) + cData;
		}
	}
	if(sqlData==null || sqlData==""){
		if(ab_msg) _X.MsgBox("변경된 데이타가 존재하지 않습니다.");
		return 0;
	}
    return sqlData.length;
};
//============================================================== _X  end

//============================================================== 정리되지 않은 함수

//그리드상에 체크박스의 체크값이 유일해야 할 때 사용(xe_GridItemClick()에서 사용한다. xe_GridDataChange()에서 사용시 루프됨.) (그리드, 컬럼명, 컬럼값)
//dao 로 변경--->a_obj.SetCheckbox(a_fields_arr,a_values_arr)
_X.SetCheckOneOnly = function(a_dg, a_field, a_value){
	var ls_field_idx = a_dg.GetColumnIndexByName(a_field);
	var ls_row_datas = a_dg._DataGrid.getFieldValues(ls_field_idx - 1, 0, -1);

	for(var i = 0; i < ls_row_datas.length; i++) {
		if(ls_row_datas[i] == "Y") {
			a_dg.SetItem(i + 1, a_field, "N");
			return;
		}
	}
};

//특정 컬럼의 최대값/최소값/총합/개수를 구할 때 사용(number형식 컬럼에서만 작동)(그리드, 컬럼, 조건)
_X.GetSummary = function(a_dg, a_col, a_div){
	if(a_div == "max" || a_div == "sum" || a_div == "min" || a_div == "count"){
		return a_dg.GridRoot().getSummary(a_col, a_div);
	}else{
		return;	
	}
};

//그리드의 컬럼그룹을 만든다.(2015.05.07 이상규)
//a_dg = 그리드, a_cols = 그룹할 컬럼 배열, a_fields = 리셋그리드에 사용 할 field, a_width = 가로길이, a_header = 그룹헤더, a_orientation = 가로, 세로
//사용예 : _X.SetGridColumnGroup(dg_1, new Array(["M01", "MC01"], ["M02", "MC02"]), fields_dg_1, 50, "1월그룹", "vertical");
_X.SetGridColumnGroup = function(a_dg, a_cols, a_fields, a_width, a_headers, a_orientation, a_showChild){
	if(a_width		 	 				== null) a_width 							= "100";
	if(a_orientation 				== null) a_orientation 				= "vertical";
	
	var colConfig 			  = JSON.parse(JSON.stringify( a_dg._ColumnConfig ));
	var startColumnConfig = new Array();
	var endColumnConfig   = new Array();
	var colArrs					  = new Array();
	var groupConfig				= null;
	var isFind						= false;
	var isArr							= a_cols[0] instanceof Array;
	
	if(isArr) {
		while(colConfig.length > 0) {
			if(_X.ArrayContains(colConfig[0].fieldName, a_cols).isEle) break;
			startColumnConfig.push(colConfig.splice(0, 1)[0]);
		}
		
		groupConfig	=	new Array();
		var cnt = -1;
		var res = true;
		
		while(a_cols.length > 0) {
			var makeGroupArr = new Array();
			while(res) {
				cnt = ++cnt >= colConfig.length ? 0 : cnt;
				if(a_cols[0][0] == colConfig[cnt].fieldName) {
					makeGroupArr.push(colConfig.splice(cnt, 1)[0]);
					a_cols[0].shift();
					cnt = -1;
				}
				
				if(a_cols[0] == null || a_cols[0] == "") {
					groupConfig.push({type: "group", name: "", orientation: a_orientation, width: a_width, header:{"visible": (a_headers != null && a_headers != ""), text: a_headers.shift() }, hideChildHeaders: a_showChild, columns:makeGroupArr});
					a_cols.shift();
					cnt = -1;
					makeGroupArr = new Array();
				}
				
				if(a_cols.length <= 0) {
					res = false;
				}
			}
		}
		
		for(var i = 0; i < colConfig.length; i++) {
			endColumnConfig.push(colConfig[i]);
		}
	} else if(typeof(a_cols[0]) == "string") {
		for(var i = 0; i < colConfig.length; i++) {
			if(a_cols.indexOf(colConfig[i].fieldName) != -1) {
				isFind = true;
				colArrs.push(colConfig[i]);
			} else {
				if(!isFind) {
					startColumnConfig.push(colConfig[i]);
				} else {
					endColumnConfig.push(colConfig[i]);
				}
			}
		}
		groupConfig = {type: "group", name: "", orientation: a_orientation, width: a_width, header:{"visible": (a_headers != null && a_headers != ""), text: a_headers.shift() }, hideChildHeaders: a_showChild, columns:colArrs};
	}
	
	if(isArr) {
		for(var i = 0; i < groupConfig.length; i++) {
			startColumnConfig.push(groupConfig[i]);
		}
	} else {
		startColumnConfig.push(groupConfig);
	}
	for(var i = 0; i < endColumnConfig.length; i++) {
		startColumnConfig.push(endColumnConfig[i]);
	}
	
	var realGridOptions = JSON.parse(JSON.stringify( _RealGrid_options ));
	if(!a_dg._IsTree && a_orientation == "vertical") {
		realGridOptions.display.rowHeight = _RealGrid_options.display.rowHeight * 2;
	}
	
	a_dg._GridRoot.setOptions(realGridOptions);
	_X.ResetGrid(a_dg, startColumnConfig, a_fields, null, a_dg.sqlFile + "|" + a_dg.sqlKey);
}

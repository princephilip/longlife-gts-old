/**
 * Object 			: mighty-dao-SlickGrid-1.4.1.js
 * @Description : Grid 함수처리 JavaScript
 * @author 			: 엑스인터넷정보 이상규
 * @since 			: 2016.12.23
 * @version 		: 1.4.1
 *
 * @Modification Information
 * <pre>
 *   since    	     author      Description
 *  ------------    --------    ---------------------------
 *   2013.02.10      김양열      RealGrid      ver 최초생성
 *   2014.12.20      박영찬      share 기능 개선, updatable 지정
 *	 2015.06.11		   이상규      SlickGrid-2.2 ver 생성
 *   2016.12.23      이상규      SlickGrid-2.3 ver 생성
 *   2017.11.03			 jhb				 handsontable-0.34.5 ver 생성
 */

/***
 * Style Format, Editor
 */
var _Styles_seq       = {},
    _Styles_text      = {},
    _Styles_textc     = {},
    _Styles_textr     = {},
    _Styles_number    = {},
    _Styles_numberc   = {},
    _Styles_numberl   = {},
    _Styles_numberf1  = {},
    _Styles_numberf2  = {},
    _Styles_numberf3  = {},
    _Styles_numberf4  = {},
    _Styles_chardate  = {},
    _Styles_date      = {},
    _Styles_datemonth = {},
    _Styles_datetime  = {},
    _Styles_hhmm      = {},
    _Styles_checkbox  = {},
    _Styles_tree      = {},
    _Styles_file      = {};

var _Editor_text      = null,
    _Editor_multiline = null,
    _Editor_number    = null,
    _Editor_numberf1  = null,
    _Editor_numberf2  = null,
    _Editor_numberf3  = null,
    _Editor_numberf4  = null,
    _Editor_chardate  = null,
    _Editor_date      = null,
    _Editor_datemonth = null,
    _Editor_datetime  = null,
    _Editor_hhmm      = null,
    _Editor_checkbox  = null,
    _Editor_dropdown  = null,
    _Editor_file      = null;

/***
 * Handsontable create
 * Handsontable 초기화 함수 호출
 */
_X.InitGrid = function (a_div) {
	//_X.InitGrid(grid_1, "dg_1", "100%", "100%", "cm", "cm251010", "CM251010|CM251010_01",false,true);
	window._GridRequestCnt++;

	var gridParam = {};

	if(typeof a_div.grid !== "undefined") {
		gridParam = a_div;

		gridParam.grid = (typeof gridParam.grid === "object" ? gridParam.grid : $("#" + gridParam.grid)[0]);;

		if(typeof gridParam.div === "undefined" || gridParam.div === null) {
			gridParam.div  = $(gridParam.grid).parents("div")[0];
		}
		else {
			gridParam.div  = (typeof gridParam.div === "object" ? gridParam.div : $("#" + gridParam.div)[0]);
		}

		gridParam.tree = false;
		gridParam.share = (typeof gridParam.share === "undefined" || gridParam.share === null ? false : gridParam.share);
		gridParam.updatable = (typeof gridParam.updatable === "undefined" || gridParam.updatable === null ? true : gridParam.updatable);
		gridParam.grid.subSystem = _PGM_CODE.substr(0,2).toLowerCase();
		gridParam.sqlFile = (typeof gridParam.sqlFile === "undefined" || gridParam.sqlFile === null ? _PGM_CODE : gridParam.sqlFile);
		gridParam.sqlId	  = (typeof gridParam.sqlId === "undefined" || gridParam.sqlId === null ? null : gridParam.sqlId);
		gridParam.option = gridParam.option; //a_option;
	}

	_X.HandsontableInit( gridParam );
};

/**
 * fn_name 				: _X.HandsontableInit
 * Description 		: Grid 기본 변수, DataView 와 Slickgrid, 컬럼, 스타일, 이벤트, Grid 메서드를 초기화 한다.
 * param 					: a_obj: grid object, a_div: grid div, ab_tree: tree유무, a_share: share 여부, a_updatable: update 여부
 * Statements 		:
 *
 *   since   		     author      Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	   김양열      최초 생성
 *   2015.06.11      이상규      SlickGrid ver 으로 변경 및 기능 추가
 *   2018.09.07      김영석      첨부파일 다운로드 관련 수정
 */
_X.HandsontableInit = function (gridParam) {
	//변수 셋팅
	var a_obj = gridParam.grid;
	window._Grids[ window._Grids.length ] = a_obj;

	a_obj.sqlFile = gridParam.sqlFile;
	a_obj.sqlKey  = gridParam.sqlId;

	if(typeof gridParam.config === "undefined" || gridParam.config === null) {
		eval("gridParam.config = columns_" + $(gridParam.grid).attr("id") + ";" );
	}

	var tHeaders = [];
	var tConfigIds = [];
	var tColumns = [];
	var tSumIds = [];
	var tSumFields = [];
	var tShowGridParam = [];
	var tIdx = 0;
	for(var i=0;i<gridParam.config.length;i++) {
		if(gridParam.config[i].visible) {
			tConfigIds.push(i);
			if(gridParam.config[i].header.text=="선택") {
				//tHeaders.push("<span><button id='select-all' onclick='$(this).toggleClass(\"selected\");_XU.check_all("+$(gridParam.grid).attr("id")+");' ><i class='fa fa-check'></i></button> "+gridParam.config[i].header.text+"</span>");
				tHeaders.push("<span>"+gridParam.config[i].header.text+"</span>");
			} else {
				tHeaders.push(gridParam.config[i].header.text);
			}

			// 2018.09.07 그리드 첨부파일 다운로드 관련 수정
			//tColumns.push({data: i});
			if(gridParam.config[i].alwaysShowButton) {
	    	tColumns.push({data: i, renderer: "html"});
	    } else {
	    	tColumns.push({data: i});
	    }

			tShowGridParam.push(gridParam.config[i]);
			if(gridParam.config[i].footerExpr=="SUM") {
				tSumIds.push(tIdx);
				tSumFields.push(gridParam.config[i].fieldName);
			}
			tIdx++;
		}
	}

	var rowsCount; // = 5;

	a_obj._DataView = [];
	a_obj._Status = [];
	a_obj._SumIds = tSumIds;
	a_obj._SumFields = tSumFields;
	a_obj._Grid = Handsontable(gridParam.grid, {
	  data: a_obj._DataView,
	  height: gridParam.option.height,
	  colWidths: 100,
	  /* minCols: 26, */
	  /* minRows: 100, */
	  rowHeaders: true,
	  colHeaders: tHeaders,
	  columns: tColumns,
	  columnSorting: true,
	  sortIndicator: true,
	  filters: true,
	  dropdownMenu: true,
	  /* contextMenu: true, */
	  autoRowSize: true,
	  /* autoColumnSize: true, */
	  manualColumnResize: true,
	  manualColumnMove: false,
	  manualRowMove: false,
	  fillHandle: {
	    autoInsertRow: false,
	  },
	  cells: function(row, column, prop) {
	    var cellMeta = {};

	    // if (row > rowsCount) {
	    //   return cellMeta;
	    // }

	    if(column>=0&&column<=tConfigIds.length) {
	    	//var tConfig = gridParam.config[tConfigIds[column]];
	    	var tConfig = tShowGridParam[column];
	    	var tClass = [];

	    	cellMeta.readOnly = (typeof(tConfig.readonlyfn)=="undefined"?tConfig.readonly:tConfig.readonlyfn(row, prop)) ;
	    	cellMeta.width    = tConfig.width;

	    	if(tConfig.readonly) { // ||row==rowsCount) {
	    		tClass.push("hst-bg-readonly");
	    	}

	    	if(tConfig.styles==_Styles_textc) {
	    		tClass.push('htCenter');
	    	} else if(tConfig.styles==_Styles_number ||
	    						tConfig.styles==_Styles_numberf1 ||
	    						tConfig.styles==_Styles_numberf2 ||
	    						tConfig.styles==_Styles_numberf3 ||
	    						tConfig.styles==_Styles_numberf4) {
	    		tClass.push('htRight');
	    		cellMeta.type = 'numeric';
	    		if(tConfig.styles==_Styles_number) {
	    			cellMeta.format = '0,0';
	    		} else if(tConfig.styles==_Styles_numberf1) {
	    			cellMeta.format = '0,0.0';
	    		} else if(tConfig.styles==_Styles_numberf2) {
	    			cellMeta.format = '0,0.00';
	    		} else if(tConfig.styles==_Styles_numberf3) {
	    			cellMeta.format = '0,0.000';
	    		} else if(tConfig.styles==_Styles_numberf4) {
	    			cellMeta.format = '0,0.0000';
	    		}
	    	} else if(tConfig.styles==_Styles_checkbox) {
	    		cellMeta.type = 'checkbox';
	    		tClass.push('htCenter');
	    	}

	    	// if(row==rowsCount) {
	    	// 	if(column==0) tClass.push('htCenter');
	    	// 	tClass.push('fBold');
	    	// 	tClass.push('cDark');
	    	// }

	    	cellMeta.className = tClass.join(" ");
	    }
	    /*
	    if (column === 1) {
	      cellMeta.type = 'dropdown';
	      cellMeta.source = [
	        'Ben',
	        'Chris',
	        'Jessica',
	        'Kate',
	        'Michael',
	        'Monica',
	        'Omar',
	        'Paul',
	        'Samuel',
	      ];

	    } else if (column === 2) {
	      cellMeta.readOnly = true;
	      cellMeta.type = 'text';
	      cellMeta.renderer = function(hotInstance, TD, row, col, prop, value) {
	        var colors = {
	          Red: '#e87677',
	          Green: '#66e100',
	          Blue: '#00a7fe',
	          Purple: '#6623e2',
	          Orange: '#ffad24',
	          Yellow: '#ffe300',
	        };

	        TD.style.color = colors[value];
	        TD.textContent = value;
	      };

	    } else if (column === 3) {
	    	cellMeta.width = 150;
	      cellMeta.type = 'dropdown';
	      cellMeta.source = [
	        'New',
	        'Accepted',
	        'Rejected',
	        'In progress',
	        'Completed',
	      ];

	    } else if (column === 4 || column === 5) {
	    	cellMeta.width = 130;
	      cellMeta.type = 'date';
	      cellMeta.dateFormat = 'YYYY-MM-DD';

	    } else if (column === 6) {
	      var isChecked = this.instance.getDataAtCell(this.instance.toVisualRow(row), column);

	      cellMeta.type = 'checkbox';
	      cellMeta.className = 'htCenter' + (isChecked ? ' at-risk-checked' : '');

	    // } else if (column === 7) {
	    //   cellMeta.width = 110;
	    //   cellMeta.renderer = function(hotInstance, TD, row, col, prop, value, cellProperties) {
	    //     var progressBar = document.createElement('progress');

	    //     value = parseInt(value, 10);

	    //     progressBar.max = 100;
	    //     progressBar.value = isNaN(value) ? 0 : value;

	    //     TD.textContent = '';
	    //     TD.appendChild(progressBar);
	    //   };
	    }
			*/
	    return cellMeta;
	  },
	  // Create virtual column data ("Team" column)
	  // modifyData: function(row, column, valueHolder, ioMode) {
	  //   if (this.toPhysicalColumn(column) === 2 && ioMode === 'get') {
	  //     valueHolder.value = getOwnerTeam(this.getDataAtCell(this.toVisualRow(row), this.toVisualColumn(1)));
	  //   }
	  // },
	  afterLoadData: function() {
	  	a_obj._Status = new Array(this.countRows());
	  },
	  afterChange: function(changes) {
			var rowIndex = 0, columnID = 1, oldVal = 2, newVal = 3, ntv = '', nv = '';
			$(changes).each(function () {
	      if(this[newVal]!=this[oldVal]) a_obj._Status[this[rowIndex]] = 'U';
	    });
	  }
	});


	a_obj.RowCount = function() {
		return a_obj._Grid.countRows();
	}

	a_obj.IsChanged = function() {
		var rtnVal = false;
		for(var i=0;i<a_obj._Status.length;i++) {
			if(a_obj._Status[i]=="I"||a_obj._Status[i]=="U"||a_obj._Status[i]=="D") rtnVal = true;
		}

		return rtnVal;
	}


	//데이타 조회
	a_obj.Retrieve = function (args) {
		var rowData = _X.XS( this.subSystem, this.sqlFile, this.sqlKey, args, "array" );

		if(rowData.length<=1&&rowData[0]==0) {
			a_obj._Grid.loadData([]);
		} else {
			a_obj._Grid.loadData(rowData);
		}

		//if(rowData.length>0) {
		//	a_obj._Grid.setDataAtCell(rowData.length, 0, "합 계")
		//	// var tFooter = new Array(rowData[0].length);
		//	// tFooter[tConfigIds[0]] = "합 계";

		//	// rowData.push(tFooter);
		//}
	}

	//데이터 저장
	a_obj.Save = function() {
		var sqlDatas = "";
		sqlDatas += this.subSystem + _Field_Separate + this.sqlFile + _Field_Separate + this.sqlKey;

		var rCnt = this._Status.length;
		var jCnt = 0;

		//console.log(a_obj._Grid.getData());
		for ( var i=0; i < rCnt; i++ ) {
			if(this._Status[i]=="U"||this._Status[i]=="I") {
				jCnt++;
				var tRow = this._Grid.getSourceDataAtRow(i);
				sqlDatas += _Col_Separate + this._Status[i];
				for(var j=0;j<tRow.length;j++) {
					sqlDatas += _Field_Separate + tRow[j];
				}
			}
		}

		if(jCnt>0) {
			var rowCnt = _X.SXD( this.subSystem + _Col_Separate + this.sqlFile, sqlDatas, null, null );
			//console.log(sqlDatas);

			if( rowCnt > 0) {
			  //_X.MsgBox("확인", rowCnt + "건의 데이터 변경작업이 정상적으로 이루어졌습니다.");
			  _X.Noty(rowCnt + "건의 데이터 변경작업이<br>정상적으로 이루어졌습니다.", null, "success", 500);

			  if( _XU.Saved ) _XU.Saved();
			}
		} else {
			_X.Noty("변경된 내용이 없습니다.", null, "danger", 500);
		}

	}
};


/**
 * fn_name 				: _X.AddFooter
 * Description 		: XmlSelect 결과 Array 에서 Footer 열을 추가
 * param 					: a_result: array object, a_object: footer 칼럼명
 * Statements 		:
 *
 *   since   		     author      Description
 *  ------------    --------    ---------------------------
 *   2017.11.10  	   JHBAE       최초 생성
 */
_X.AddFooter = function (a_results, a_obj) {
  var vObj = {x_row_type:"footer"};

  // 합계 추가
  $.each(a_results, function(ix, el) {
  	$.each(a_obj, function(idx, val) {
  		if(val=="SUM") {
  			if(!vObj[idx]) vObj[idx] = 0;
  			vObj[idx] += Number(el[idx]);
  		}
  	});
  });

  a_results.push(vObj);
  return a_results;
}

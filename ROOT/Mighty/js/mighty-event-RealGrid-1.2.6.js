/**
 * Object :  mighty-event-1.2.0.js
 * @Description : Grid Event 처리  JavaScript
 * @author :엑스인터넷정보 김양열
 * @since : 2013.02.10
 * @version : 1.2.6
 *
 * @Modification Information
 * <pre>
 *   since    	  author           Description
 *  ----------    --------    ----------------------------
 *   2013.02.10    김양열        최초생성
 */

//데이타로딩후 RowFocusChange이벤트 호출
var xe_M_RealGridDataLoad = function(a_obj) {
	if(a_obj.RowCount()>0){
		//RealGrid에서는 필요 없음(2015.03.02 KYY);
		//if(a_obj._OldRow<=1 && a_obj.GetRow()!=0) {
		//	xe_M_RealGridRowFocusChange(a_obj, {itemIndex:-1, dataRow:-1, fieldName:"RETRIEVE후 호출"}, {itemIndex:0, dataRow:0, fieldName:""});
		//}
		a_obj.SetRow(1, null, false);
	}
	_X.UnBlock();
	if(typeof(xe_GridDataLoad)!="undefined") {xe_GridDataLoad(a_obj);}
	a_obj._IsRetrieving = false;
};

//item click event
var xe_M_RealGridItemClick = function(a_obj, cellIndex) {
	if(typeof(xe_GridItemClick)!="undefined") {
		xe_GridItemClick(a_obj, cellIndex.itemIndex +1, a_obj.GetColumnIndexByName(cellIndex.fieldName), cellIndex.fieldName);
	}
};

//rowfocus 변경시 발생 event
var xe_M_RealGridRowFocusChange = function(a_obj, newIndex, oldIndex, action) {
	//_EventLog += a_obj.id + " RFC" + newIndex.itemIndex + " "; 
	var rtValue = true;
	var editable = false;
	var oldItemIndex = (oldIndex==null ? -1 : oldIndex.itemIndex);
	var oldFiledName = (oldIndex==null ? "" : oldIndex.fieldName);

	// 수정(action 추가, 삭제 deleteRow에서 호출 - 박종현
	action = (action== null ? "R" : action);
	if(newIndex!=null) {
		//Row Focus가 변경된 경우
		
		if((newIndex.itemIndex != oldItemIndex) || action == 'D') {
			if(typeof(xe_GridRowFocusChange)!="undefined") {
				//a_obj._CurrentRow
				//_X.MsgBox('xe_M_RealGridRowFocusChange ' + a_obj._CurrentRow + " , " + a_obj.GetRow() + " , " + a_obj._OldRow);
				if(action == 'D'){
					rtValue = xe_GridRowFocusChange(a_obj, a_obj.GetRow(), a_obj.GetRow());
				}else{
					rtValue = xe_GridRowFocusChange(a_obj, newIndex.itemIndex +1, oldItemIndex +1);
				}
				if(rtValue!=null && rtValue==false) {
					return false;
				}
				if(a_obj._KeyCols!=null){
          //소계 부분 GetRowState 에러때문에 추가 -- 박종현 
					if(newIndex.dataRow != -1){
						editable = (a_obj.DataGrid().getRowState(newIndex.dataRow)=="created" ? true : false);
					}		
						
					for(var i=0;i<a_obj._KeyCols.length; i++){
						a_obj.GridRoot().setColumnProperty(a_obj.columnByField(a_obj._KeyCols[i]), "editable", editable);
						if(a_obj._Share) {
							_X.FormSetDisable($("#"+a_obj._KeyCols[i]),!editable);
						}
					}
				}			
				if(a_obj._Share){
					x_GridRowSync(a_obj, newIndex.itemIndex+1);
				}
			}
		}

		//Item Focus가 변경된 경우
		if(typeof(xe_GridItemFocusChange)!="undefined") {
			rtValue = xe_GridItemFocusChange(a_obj, newIndex.itemIndex +1, newIndex.fieldName, oldItemIndex +1, oldFiledName);
			if(rtValue!=null && rtValue==false) {
				return false;
			}
		}
	}
	a_obj._CurrentRow = (newIndex==null ? -1 : newIndex.itemIndex);
if(a_obj.id == 'dg_1')
	return rtValue;
};

//rowfocus 변경시 발생 event
//oldIndex 파라메타 추가(2015.05.23 KYY)
var xe_M_RealGridRowFocusChanged = function(a_obj, newIndex, oldIndex) {
		if(typeof(xe_GridRowFocusChanged)!="undefined") {
		return xe_GridRowFocusChanged(a_obj, newIndex.itemIndex +1, oldIndex.itemIndex +1);
	}
};

var xe_M_RealGridDataChange = function(a_obj, itemIndex, dataRow, field, onEditCommitoldValue) {
	var rowIndex = itemIndex +1;			// 변경된 행번호
	//var columnIndex = field +1;			// 변경된 열번호
	var columnName = a_obj.GetColumnNameByIndex(field+1);
	var newValue = a_obj.GetItem(rowIndex,columnName);	// 변경후 값
	var oldValue = onEditCommitoldValue;						// 변경전 값

	// 숫자 타입일때 체크 - 박종현
	if(a_obj.GetColumnTypeByName(columnName) == "number"){
		/*2015.02.06 이상규
			문제발생시점: column type 이 number 이고 변경된 값이 0일 경우
			문제내용: newValue == "" 체크 시 타입이  number 이기 때문에 값이 0 일 경우 newValue == "" 의 결과값이 true 가 됨.
			          때문에 아래 SetItem() 함수를 호출하게 되고 SetItem() 함수의 내부로직에서 _Share == true 상태일 경우 현재 함수를 다시 호출 하게 됨.
			          SetItem() 호출 당시 값을 0 으로 줬기 때문에 위와 같은 과정이 무한루프 됨.
			해결방법: newValue == "" 를 String(newValue) == "" 로 타입을 String 으로 변경하여 체크 하도록 함.
			}*/
		if(newValue == "undefined" || String(newValue) == "" || newValue == null){
			a_obj.GridRoot().commit();
			a_obj.SetItem(rowIndex,columnName,0);
			newValue = 0;
		}
	}
	
	if(a_obj._Share){
		x_GridRowSync(a_obj, rowIndex, columnName, newValue, oldValue);
	}
	
	if(typeof(xe_GridDataChange)!="undefined") {
		xe_GridDataChange(a_obj, rowIndex, columnName, newValue, oldValue);
	}
	/*2015.02.03 이상규
		문제발생시점: _Share = true; 상태일 때 그리드의 데이타 변경 시.
									xe_GridDataChange() 함수에서 SetItem() 함수를 호출 할 때
									
		문제내용:	SetItem() 함수에서 x_GridRowSync() 함수를 호출하여 sync 작업이 이루어진 후
						  xe_M_RealGridDataChange() 의 x_GridRowSync() 함수가 나중에 호출되어
						  SetItem() 함수를 호출하여 변경한 값을 다시 바꿔버리는 문제가 발생
		
		해결방법: mighty-event-RealGrid-1.2.0.js 의 xe_M_RealGridDataChange() 함수의 내부로직 중
							x_GridRowSync() 의 호출 시점을 xe_GridDataChange() 함수 호출 전으로 이동.
							
				 			mighty-dao-RealGrid-1.3.0.js 의 SetItem() 함수 내부로직 중
				 			x_GridRowSync() 호출에서 xe_M_RealGridDataChange() 호출로 변경.
		
		if(a_obj._Share){
			x_GridRowSync(a_obj, rowIndex, columnName, newValue, oldValue);
		}*/
};

//item double click event
var xe_M_RealGridItemDoubleClick = function(a_obj, cellIndex) {
	if(typeof(xe_GridItemDoubleClick)!="undefined") {
		xe_GridItemDoubleClick(a_obj, cellIndex.itemIndex +1, a_obj.GetColumnIndexByName(cellIndex.fieldName), cellIndex.fieldName);
	}
};

//header click event
var xe_M_RealGridHeaderClick = function(a_obj, column) {
	if(typeof(xe_GridHeaderClick)!="undefined") {
		xe_GridHeaderClick(a_obj, a_obj.GetColumnIndexByName(column.fieldName), column.fieldName);
//			var dept_code = dg_1.GetItem(dg_1.GetRow(),'dept_name');
//	_FindCode.grid_row = dg_1.GetRow();
//	_X.FindCode(window,"부서찾기","com","DEPT_CODE", dept_code,new Array(top._CompanyCode), "FindDeptCode|FindCode|FindDeptCode",500, 500);

	}
};

//grid button click event
var xe_M_RealGridButtonClick = function(a_obj, itemIndex, column) {
	if(typeof(xe_GridButtonClick)!="undefined") {
		xe_GridButtonClick(a_obj, itemIndex +1, a_obj.GetColumnIndexByName(column.fieldName), column.fieldName);
	}
	if(typeof(x_FindCode)!="undefined") {
		_FindCode.grid_row = a_obj.GetRow();
		_FindCode.grid = a_obj;
		x_FindCode(a_obj, a_obj.GetRow(),column.fieldName, a_obj.GetItem(a_obj.GetRow(),column.fieldName),window);
	}
};

//setContextMenu를 통해 추가한 Flash 컨텍스트 메뉴 항목을 오른쪽 마우스로 클릭했을 때 호출된다.
var xe_M_RealGridContextMenuItemSelect = function(a_obj, label, cellIndex) {
	if(typeof(xe_GridContextMenuItemSelect)!="undefined") {
		xe_GridContextMenuItemSelect(a_obj, label, cellIndex.itemIndex +1, a_obj.GetColumnIndexByName(cellIndex.fieldName), cellIndex.fieldName);
	}
};

//addPopupMenu로 추가한 팝업메뉴 항목이 클릭됐을 때 호출된다.
var xe_M_RealGridMenuItemSelect = function(a_obj, data) {
	if(typeof(xe_GridMenuItemSelect)!="undefined") {
		xe_GridMenuItemSelect(a_obj, data.label, data.checked, data.tag);
	}
};

//TreeDataProvider에 데이터행들이 삭제된 후 호출된다.
var xe_M_RealGridRowsDeleted = function(a_obj, rows) {
	//for(var i=0; i<rows.length; i++) {
	//	a_obj._DeletedRows.push(a_obj.GridRoot().getRowData(rows[i]));
	//}
};

//TreeGrid 에서 checkbox를 check할때 발생하는 이벤트.
var xe_M_RealGridItemChecked = function(a_obj, itemIndex, checked) {
	if(typeof(xe_GridItemChecked)!="undefined") {
		xe_GridItemChecked(a_obj, itemIndex + 1, checked);
	}
};

//TreeGrid 에서 expanding 시 발생하는 event
var xe_M_RealTreeItemExpanding = function(a_obj, itemIndex, rowId) {
	if(typeof(xe_TreeItemExpanding)!="undefined") {
		xe_TreeItemExpanding(a_obj, itemIndex + 1, rowId);
	}
};

//추후 chart 관련 event  추가 처리
//차트관련 이벤트 추가(2013.03.27 김양열)
var xe_M_ChartPointClick = function(a_obj, event) {
	_X.MsgBox('xe_M_ChartPointClick');
	if(typeof(xe_ChartPointClick)!="undefined") {xe_ChartPointClick(a_obj, event);}
};

////사용자가 키보드나 스크롤 바 등을 통해 그리드에 마지막 행이 표시되는 시점에 호출된다.
//var xe_M_RealGridScrollToBottom = function(a_obj) {
//	if(typeof(xe_GridScrollToBottom)!="undefined") {
//		xe_GridScrollToBottom(a_obj);
//	}
//};
/**
 * Object :  mighty-mdi-1.2.0.js
 * @Description : Mighty-X MDI  관련 javascript libaray
 * @author :엑스인터넷정보 김양열
 * @since : 2013.02.10
 * @version : 1.2.8
 *
 * @Modification Information
 * <pre>
 *   since        author           Description
 *  ----------    --------    ---------------------------
 *   2013.02.10    김양열        최초생성
 */
var _MAX_MDI_CNT = 20;
var _MDI_CNT = 0;
var _MDI_FRAME = new Array();
var _MDI_MENU  = new Array();
var _CurMenuIndex = null;
var _PreMenuIndex = null;

/**
 * fn_name :  _X.MDI_Open
 * Description : 새로운 MDI화면을 연다.
 * param :a_pgmname : 메뉴코드 , a_pgmname : 프로그램명, a_url : 경로
 * Statements :
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
 */
_X.MDI_Open = function(a_pgmcode, a_pgmname, a_url) {
  if(_MDI_CNT >= _MAX_MDI_CNT) {
    _X.MsgBox('창의 갯수가 ' + _MAX_MDI_CNT + '개로 제한되어 있습니다.');
    return;
  }
  a_url = top._CPATH + a_url;
  //이미 있으면
  var isIndex = _X.MDI_IsMenuIndex(a_url);
  if(isIndex>=0) {
    _X.MDI_Select(isIndex,false);
    return;
  }
  var mdiIdx = _X.MDI_NewIndex();
  //IFRAME 생성
  var ndiv = document.createElement('div');
  ndiv.style.position = "absolute";
  ndiv.style.left = "10px";
  ndiv.style.right = "0px";
  ndiv.style.top = _X.MDI_Top() + "px";
  //화면 리사이징을 위한 조정 김억주
  ndiv.style.width = _X.MDI_Width() + "px";
  ndiv.style.height = _X.MDI_Height() + "px";
  ndiv.style.bottom = "0px";
  ndiv.style.zIndex = -500;
  ndiv.style.backgroundColor = "#ffffff";
  ndiv.style.overflow = "hidden";
  ndiv.style.verticalAlign = "top";
  $(ndiv).html('<iframe id="mdi_' + mdiIdx + '" src="' + a_url + '" scrolling="auto" width="100%" height="100%" align="top" frameborder="0" marginheight="0" marginwidth="0"/>');
  document.body.appendChild(ndiv);
  _MDI_FRAME[mdiIdx] = ndiv;
  //MDI 탭 생성
  _X.MDI_DeSelect();
  var ndivtab = document.createElement('div');
  //ndivtab.style.margin = '0 0 0 -2000px';
  ndivtab.style.float = 'left';
  $(ndivtab).html(_X.MDI_GetHtml(mdiIdx, a_pgmcode, a_pgmname, true));
  ndivtab.tabIndex = mdiIdx;
  ndivtab.pgmCode = a_pgmcode;
  ndivtab.pgmName = a_pgmname;
  ndivtab.selected = true;
  ndivtab.preMenuIndex = _CurMenuIndex;
  mdimenu.appendChild(ndivtab);

  _MDI_MENU[mdiIdx] = ndivtab;
  _MDI_MENU[mdiIdx]._OpenUrl = a_url;

  _CurMenuIndex = mdiIdx;
  _MDI_CNT++;

  if(mdi_main.style.display!="none")
    mdi_main.style.display="none";
};

/**
 * fn_name :  _X.MDI_Select
 * Description : 선택된 탭을 활성화 시킨다.
 * param :a_idx : tab index , ab_keepPreIndex : 이전 tab 활성화 유무
 * Statements :
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
 */
_X.MDI_Select = function(a_idx, ab_keepPreIndex)
{
  _X.MDI_DeSelect(a_idx);
  if(_MDI_MENU[a_idx]!=null){
    if(!_MDI_MENU[a_idx].selected) {
      $(_MDI_MENU[a_idx]).html(_X.MDI_GetHtml(a_idx, _MDI_MENU[a_idx].pgmCode, _MDI_MENU[a_idx].pgmName, true));
      //_MDI_MENU[a_idx].innerHTML = _X.MDI_GetHtml(a_idx, _MDI_MENU[a_idx].pgmCode, _MDI_MENU[a_idx].pgmName, true);
      _MDI_MENU[a_idx].selected = true;
      if(ab_keepPreIndex==null || !ab_keepPreIndex)
        _MDI_MENU[a_idx].preMenuIndex = _CurMenuIndex;
      _MDI_FRAME[a_idx].style.display = 'inline';
    }
  }
  _CurMenuIndex = a_idx;

  if(mdi_main.style.display!="none")
    mdi_main.style.display="none";

};

/**
 * fn_name :  _X.MDI_DeSelect
 * Description : 선택된 탭을 비활성화 시킨다.
 * param :a_newidx : tab index
 * Statements :
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
 */
_X.MDI_DeSelect = function(a_newidx)
{
  for(var i=0; i<_MDI_MENU.length; i++) {
    if(_MDI_MENU[i]!=null && i!=a_newidx){
      if(_MDI_MENU[i].selected) {
        $(_MDI_MENU[i]).html(_X.MDI_GetHtml(i, _MDI_MENU[i].pgmCode, _MDI_MENU[i].pgmName, false));
        //_MDI_MENU[i].innerHTML = _X.MDI_GetHtml(i, _MDI_MENU[i].pgmCode, _MDI_MENU[i].pgmName, false);
        _MDI_MENU[i].selected = false;
        //_MDI_FRAME[i].style.zIndex = 1;
        _MDI_FRAME[i].style.display = 'none';
      }
    }
  }
};

_X.MDI_CloseAll = function() {
  var mdilen = _MDI_FRAME.length;
  for(var i=mdilen-1; i>=0; i--) {
    if(_MDI_FRAME[i] != null) {
      _X.MDI_Close(i);
    }
  }
}

/**
 * fn_name :  _X.MDI_Close
 * Description : MDI 화면을 닫는다.
 * param :a_idx : tab index
 * Statements :
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
 */
_X.MDI_Close = function(a_idx) {
  //if(a_idx<=0)  //pyc
  //  return;
  //try{
    if(_MDI_CNT==1)
      mdi_main.style.display="inline";

    _MDI_FRAME[a_idx].style.display = 'none';
    var preMenuIndex = _X.MDI_PreMenuIndex(a_idx);
    if(_MDI_MENU[a_idx].selected && preMenuIndex!=null && _MDI_FRAME[preMenuIndex]!=null){
      _X.MDI_Select(preMenuIndex, true);
    }

    var mdiObj = $('#mdi_' + a_idx);
    if(mdiObj.length >0) {
      var mdi_frame = $('#mdi_' + a_idx)[0];
      var mdi_win = mdi_frame.contentWindow;
      //화면에 로딩된 Flash 오브젝트 등을 Clear한다. (2013.04.07 김양열)
      xm_ClearObject(mdi_win);

      //메모리 Clear를 위해 추가(2015.06.30)
      mdi_frame.src = "about:blank";
      mdi_frame.contentWindow.document.write('');

      //chrome 일때 parent.close 로 (2015.10.06 이상규)
      var agt = navigator.userAgent.toLowerCase();
      if (agt.indexOf("chrome") != -1) {
        mdi_frame.contentWindow.parent.close();
      }
      else {
        mdi_frame.contentWindow.close();
      }

      //$target.remove();
      if( typeof CollectGarbage == "function") {
        CollectGarbage();
      }

      mdi_win = null;
    }

    _X.purge(_MDI_MENU[a_idx]);
    _X.purge(_MDI_FRAME[a_idx]);

    //$(_MDI_MENU[a_idx]).html("");
    //$(_MDI_FRAME[a_idx]).html("");

    //mdimenu.removeChild(_MDI_MENU[a_idx]);
    //document.body.removeChild(_MDI_FRAME[a_idx]);

    _MDI_FRAME[a_idx] = null;
    _MDI_MENU[a_idx] = null;

    _MDI_CNT--;

    if(_MDI_CNT<=0)
      _X.MDI_ShowMain();

  //}
  //catch(e)
  //{e=null;}
};

/**
 * fn_name :  _X.MDI_PreMenuIndex
 * Description : 선택이전 tab index
 * param :a_idx : tab index
 * Statements :
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
 */
_X.MDI_PreMenuIndex = function(a_idx)
{
  //이전에 선택된 탭 활성화
  var preMenuIndex = _MDI_MENU[a_idx].preMenuIndex;
  if(preMenuIndex==a_idx || preMenuIndex==null || _MDI_FRAME[preMenuIndex]==null){
    for(var i=a_idx +1; i<_MDI_MENU.length; i++){
      if(_MDI_FRAME[i]!=null)
        return i;
    }
    for(var i=a_idx -1; i>=0; i--){
      if(_MDI_FRAME[i]!=null)
        return i;
    }
  }

  return preMenuIndex;
};

/**
 * fn_name :  _X.MDI_IsMenuIndex
 * Description : 현재의 tab index
 * param :a_idx : tab index
 * Statements :
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
 */
_X.MDI_IsMenuIndex = function(a_url)
{
  for(var i=0; i<_MDI_MENU.length; i++){
    if(_MDI_MENU[i]!=null) {
      if(_MDI_MENU[i]._OpenUrl==a_url) {
        return i;
      };
    }
  }
  return -1;
};

/**
* fn_name :  _X.MDI_IsMenuIndex
* Description : 새로운 MDI화면 인덱스를 구한다.
*
* Statements :
*
*   since        author           Description
*  ------------    --------    ---------------------------
*   2013.02.10    김양열          최초 생성
*/
_X.MDI_NewIndex = function(){
  for(var i=0; i<_MDI_FRAME.length; i++) {
    if(_MDI_FRAME[i]==null)
      return i;
  }
  return _MDI_FRAME.length;
};

/**
* fn_name :  _X.MDI_Resize
* Description : 열린 MDI 프레임의 사이즈를 재조정한다.
*
* Statements :
*
*   since        author           Description
*  ------------    --------    ---------------------------
*   2013.02.10    김양열          최초 생성
*/
_X.MDI_Resize = function(){
  var newWidth  = _X.MDI_Width();
  var newHeight = _X.MDI_Height();

  for(var i=0; i<_MDI_FRAME.length; i++) {
    if(_MDI_FRAME[i]!=null) {
      //auto를 적용해 보자
      _MDI_FRAME[i].style.width  = newWidth + "px";
      _MDI_FRAME[i].style.height = newHeight + "px";
      //_MDI_FRAME[i].style.min-width = newWidth + "px";
      //_MDI_FRAME[i].style.min-height = newHeight + "px";

      //_MDI_FRAME[i].style.width  = "auto";
      //_MDI_FRAME[i].style.height = "auto";

      //var mdi_win = $('#mdi_' + i)[0].contentWindow;

      //if(typeof(mdi_win.pageLayout)!="undefined")
      //  mdi_win.pageLayout.style.height = newHeight + "px";
      //X.MsgBox(newHeight + "px");
    }
  }
};

/**
* fn_name :  _X.MDI_Width
* Description : MDI 화면 넓이를 구한다.
* param :
* Statements :
*
*   since        author           Description
*  ------------    --------    ---------------------------
*   2013.02.10    김양열          최초 생성
*/
_X.MDI_Width = function() {
  return Math.max((_X.InnerWidth() - 16), 600);
};

/**
* fn_name :  _X.MDI_Width
* Description : MDI 화면 높이를 구한다.
* param :
* Statements :
*
*   since        author           Description
*  ------------    --------    ---------------------------
*   2013.02.10    김양열          최초 생성
*/
_X.MDI_Height = function() {
  //var menuHeight = window.outerHeight - window.innerHeight;
  var newHeight = (_X.InnerHeight() - _X.toInt(mdi_border.offsetTop) - _FooterHeight - 5);
  return Math.max(newHeight,300);
};

/**
* fn_name :  _X.MDI_Top
* Description : MDI 화면 Top위치를 구한다.
* param :
* Statements :
*
*   since        author           Description
*  ------------    --------    ---------------------------
*   2013.02.10    김양열          최초 생성
*/
_X.MDI_Top = function() {
  return $("#mdi_border").offset().top + 5;
};

/**
* fn_name :  _X.InnerWidth
* Description : 브라우저 화면 넓이를 구한다.
* param :
* Statements :
*
*   since        author           Description
*  ------------    --------    ---------------------------
*   2013.02.10    김양열          최초 생성
*/
_X.InnerWidth = function()
{
  var iWidth = $(window).innerWidth(); //_X.toInt(document.all?document.body.clientWidth:window.innerWidth);
  return iWidth;
};
_X.innerWidth = _X.InnerWidth;

/**
* fn_name :  _X.InnerHeight
* Description : 브라우저 화면 높이를 구한다.
* param :
* Statements :
*
*   since        author           Description
*  ------------    --------    ---------------------------
*   2013.02.10    김양열          최초 생성
*/
_X.InnerHeight = function()
{
  var iHeight = $(window).innerHeight(); //_X.toInt(document.all?(window.outerHeight - window.screenTop + window.screenY - 8):window.innerHeight);
  //_X.MsgBox(iHeight);
  return iHeight;
};
_X.innerHeight = _X.InnerHeight;

/**
* fn_name :  _X.MDI_GetHtml
* Description : MDI탭의 HTML구문을 생성한다.
* param :
* Statements :
*
*   since        author           Description
*  ------------    --------    ---------------------------
*   2013.02.10    김양열          최초 생성
*/
_X.MDI_GetHtml = function (a_idx, a_pgmcode, a_pgmname, a_selected) {
  var ls_html;
  //var li_width = (a_pgmname.length>10 ? 140 : 40 + a_pgmname.length * 11);

  if (a_selected) {
    ls_html = '<table id=tbl_mdimenu_' +a_idx +' height=20 valign="bottom" border="0" cellspacing="0" cellpadding="0">'
        + ' <tr>'
        + '   <td width="3" rowspan="2"><img src="'+_web_context.themePath+'/images/mdi/bg_front_a.gif"></td>'
        + '   <td height="3" background="'+_web_context.themePath+'/images/mdi/bg_top_a.gif"></td>'
        + '   <td valign="top" background="'+_web_context.themePath+'/images/mdi/bg_left_top_a.gif"></td>'
        + '   <td width="5" rowspan="2"><div align="right"><img src="'+_web_context.themePath+'/images/mdi/bg_end_a.gif"></div></td>'
        + ' </tr>'
        + ' <tr ' + ' title="[' + a_pgmcode + '] ' + a_pgmname + '"' + '>'
        + '   <td height="18" background="'+_web_context.themePath+'/images/mdi/bg_main_a.gif" style="cursor:pointer;"><B><font color="#ffffff">' + (a_pgmname.length>20?a_pgmname.substr(0,19) + "..":a_pgmname) + '</font></B></span></td>'
        + '   <td width="10" valign="top" background="'+_web_context.themePath+'/images/mdi/bg_left_bottom_a.gif">' + '<img src="'+_web_context.themePath+'/images/mdi/close2.gif" alt="닫기(' + a_pgmname + ')" border="0" name="img_close_' + a_idx + '" id="img_close_' + a_idx + '" onMouseOut="_X.SetImg(this,' + "'"+_web_context.themePath+"/images/mdi/close2.gif')" + ';" onMouseOver="_X.SetImg(this,' + "'"+_web_context.themePath+"/images/mdi/close2_1.gif');" + '" onClick="_X.MDI_Close(' + a_idx + ');"></td>'
        + ' </tr>'
        + '</table>';
  } else {
    ls_html = '<table id=tbl_mdimenu_' +a_idx +' height=20 valign="bottom" border="0" cellspacing="0" cellpadding="0">'
        + ' <tr>'
        + '   <td width="3" rowspan="2"><img src="'+_web_context.themePath+'/images/mdi/bg_front_b.gif"></td>'
        + '   <td height="3" background="'+_web_context.themePath+'/images/mdi/bg_top_b.gif"></td>'
        + '   <td valign="top" background="'+_web_context.themePath+'/images/mdi/bg_left_top_b.gif"></td>'
        + '   <td width="5" rowspan="2"><div align="right"><img src="'+_web_context.themePath+'/images/mdi/bg_end_b.gif"></div></td>'
        + ' </tr>'
        + ' <tr ' + ' title="[' + a_pgmcode + '] ' + a_pgmname + '"' + ' onClick="_X.MDI_Select(' + a_idx + ')">'
        + '   <td height="18" background="'+_web_context.themePath+'/images/mdi/bg_main_b.gif" style="cursor:pointer;"><font color="#536370">' + (a_pgmname.length>20?a_pgmname.substr(0,19) + "..":a_pgmname) + '</font></span></td>'
        + '   <td width="10" valign="top" background="'+_web_context.themePath+'/images/mdi/bg_left_bottom_b.gif">' + '<img src="'+_web_context.themePath+'/images/mdi/close1.gif" alt="닫기(' + a_pgmname + ')" border="0" name="img_close_' + a_idx + '" id="img_close_' + a_idx + '" onMouseOut="_X.SetImg(this,' + "'"+_web_context.themePath+"/images/mdi/close1.gif')" + ';" onMouseOver="_X.SetImg(this,' + "'"+_web_context.themePath+"/images/mdi/close1_1.gif');" + '" onClick="_X.MDI_Close(' + a_idx + ');"></td>'
        + ' </tr>'
        + '</table>';
  }

  //스크롤 제어 김억주
  return '<div style="float:left; height:22px; overflow:hidden;">'
        + ls_html
        + '</div>';

};

_X.MDI_ShowMain = function() {
  // if(mdi_main.style.display!="inline")
  //   mdi_main.style.display="inline";

  // _X.MDI_DeSelect(-1);

  alert('MDI_ShowMain');
  $("#mdi_main").show();
  _X.MDI_DeSelect(-1);
  $("#mdi_main").focus();
}

_X.MDI_Link = function(a_pgmcode, a_pgmname, a_url) {
  if(_MDI_CNT >= _MAX_MDI_CNT) {
    _X.MsgBox('창의 갯수가 ' + _MAX_MDI_CNT + '개로 제한되어 있습니다.');
    return;
  }

  a_url = top._CPATH + a_url;

  //이미 있으면
  var isIndex = _X.MDI_IsMenuIndexNoParam(a_url);
  if(isIndex < 0) {
    _X.MDI_Open(a_pgmcode, a_pgmname, a_url);
  } else {
    _MDI_MENU[isIndex]._OpenUrl = a_url;
    _X.MDI_Select(isIndex, false);

    var mdi_frame = $('#mdi_' + isIndex)[0];
    var mdi_win = mdi_frame.contentWindow;

    if (mdi_win.x_MDI_Link_After) {
      mdi_win.x_MDI_Link_After(a_url);
    }
  }
}

_X.MDI_IsMenuIndexNoParam = function(a_url) {
  var a_mdi = a_url.indexOf("&") != -1;
  var a_mdi_url = a_mdi ? a_url.substring(0, a_url.indexOf("&")) : a_url;

  for(var i=0; i<_MDI_MENU.length; i++){
    if(_MDI_MENU[i]!=null) {
      var b_mdi = _MDI_MENU[i]._OpenUrl.indexOf("&") != -1;
      var b_mdi_url = b_mdi ? _MDI_MENU[i]._OpenUrl.substring(0, a_url.indexOf("&")) : _MDI_MENU[i]._OpenUrl;

      if(a_mdi_url == b_mdi_url) {
        return i;
      };
    }
  }
  return -1;
};

/**
 * Object :  mighty-ui-bootstrap-1.0.0.js
 * @Description : Mighty-X UI 관련 javascript libaray
 * @author :엑스인터넷정보 김양열
 * @since : 2013.02.10
 * @version : 1.2.7
 *
 * @Modification Information
 * <pre>
 *   since        author           Description
 *  ----------    --------    ---------------------------
 *   2013.02.10    김양열        최초생성
 *   2014.11.20    박영찬        IE8 오루수정 및 일부 기능 taglib 로 변경
 */


//수정(2015.07.26 KYY)
_X.Block = function(a_screen, a_img) {
  if(a_screen==null) a_screen = true;
  if(a_img==null) a_img = true;
  if(a_img)
    $("#pageLayout").append("<div id='formLoadImg'><img src='"+_web_context.themePath+"/images/viewLoading.gif'/></div>");
  if(a_screen)
    $.blockUI();
};
//수정(2015.07.26 KYY)
_X.UnBlock = function(a_screen, a_img) {
  if(a_screen==null) a_screen = true;
  if(a_img==null) a_img = true;
  if(a_img)
    $('#formLoadImg').remove();
  if(a_screen)
    $.unblockUI();
};

// 메뉴를 그립니다. 1단 버전
_X.SetTopMenu = function() {
  var thtml = "";
  for(var i=0; i<_MyPgmList.length; i++) {
    thtml = "";
    if(_MyPgmList[i].MENU_LEVEL=="1") {
      if( $("#main-menu").length == 0 ) {
        thtml = "<ul id=\"main-menu\" class=\"nav navbar-nav\"></ul>";

        $(".navbar .navbar-collapse").append(thtml);
      }

      thtml = "<li class=\"dropdown-mainmenu\">";
      if(_MyPgmList[i].CHILD_MENU_CNT!="0") {
        thtml += "<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">"+_MyPgmList[i].PGM_NAME+"</a>";
      } else {
        thtml += "<a href=\"#\" data-pgmcode=\""+_MyPgmList[i].PGM_CODE+"\" data-pgmname=\""+_MyPgmList[i].PGM_NAME+"\" data-openurl=\""+_MyPgmList[i].OPEN_URL+"\">"+_MyPgmList[i].PGM_NAME+"</a>";
      }
      thtml += "<ul id=\"main-ddmenu-"+_MyPgmList[i].SYS_ID.toLowerCase()+"\" class=\"dropdown-menu\"></ul>";
      thtml += "</li>";
      
      $("#main-menu").append( thtml );
    } else {
      var tObjNm = "";
      if(_MyPgmList[i].MENU_LEVEL=="2") {
        tObjNm = "#main-ddmenu-"+_MyPgmList[i].SYS_ID.toLowerCase();
      } else {
        tObjNm = "#main-ddmenu-"+_MyPgmList[i].PARENT_SORT_CODE.toLowerCase();
      }

      if(_MyPgmList[i].CHILD_MENU_CNT>0) {
        thtml = "<li class=\"dropdown-submenu\">";
        thtml += "<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">"+_MyPgmList[i].PGM_NAME+"</a>";
        thtml += "<ul id=\"main-ddmenu-"+_MyPgmList[i].SORT_CODE.toLowerCase()+"\" class=\"dropdown-menu\"></ul>";
        thtml += "</li>";
      } else {
        thtml = "<li>";
        thtml += "<a href=\"#\" data-pgmcode=\""+_MyPgmList[i].PGM_CODE+"\" data-pgmname=\""+_MyPgmList[i].PGM_NAME+"\" data-openurl=\""+_MyPgmList[i].OPEN_URL+"\">"+_MyPgmList[i].PGM_NAME+"</a>";
        thtml += "</li>";
      }

      $(tObjNm).append(thtml);
    }
  }
}

// 메뉴를 그립시다. depth-2 버전
_X.SetTopMenuD2 = function() {
  var thtml = "";
  for(var i=0; i<_MyPgmList.length; i++) {
    thtml = "";
		if(typeof(_MyPgmList[i].PARENT_SORT_CODE)=="undefined") {
			_MyPgmList[i].PARENT_SORT_CODE = _MyPgmList[i].PARENT_MENU_CODE;
		}
		
    if(_MyPgmList[i].MENU_LEVEL=="1") {
      $("#main-menu").append("<li><a href=\"#\" data-pgmcode=\""+_MyPgmList[i].PGM_CODE+"\" data-sidepanel=\"" + _MyPgmList[i].SIDEPANEL_YN + "\" >"+_MyPgmList[i].PGM_NAME+"</a></li>");
    } else {
      if(_MyPgmList[i].MENU_LEVEL=="2") {
        if ($("#main-menu-"+_MyPgmList[i].SYS_ID.toLowerCase()).length == 0) {
          thtml = "<ul id=\"main-menu-"+_MyPgmList[i].SYS_ID.toLowerCase()+"\" class=\"nav navbar-nav hide\">";
          thtml += "</ul>";

          $(".menu-depth-2").append(thtml);
        }

        thtml = "<li id=\"main-menu-"+_MyPgmList[i].SORT_CODE.toLowerCase()+"\" class=\"dropdown-mainmenu\">";
        thtml += "<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">"+_MyPgmList[i].PGM_NAME+"</a>";
        thtml += "</li>";

        $("#main-menu-"+_MyPgmList[i].SYS_ID.toLowerCase()).append(thtml);
      } else {
        // child 있을때 처리는? 
        if( $("#main-menu-"+_MyPgmList[i].PARENT_SORT_CODE.toLowerCase()+">.dropdown-menu").length == 0 ) {
          thtml = "<ul id=\"main-menu-pu-"+_MyPgmList[i].SORT_CODE.toLowerCase()+"\" class=\"dropdown-menu\">";
          thtml += "</ul>";

          $("#main-menu-"+_MyPgmList[i].PARENT_SORT_CODE.toLowerCase()).append(thtml);
        }

        if(_MyPgmList[i].CHILD_MENU_CNT>0) {
          thtml = "<li id=\"main-menu-"+_MyPgmList[i].SORT_CODE.toLowerCase()+"\" class=\"dropdown-submenu\"><a href=\"#\"  class=\"dropdown-toggle\" data-toggle=\"dropdown\">"+_MyPgmList[i].PGM_NAME+"</a></li>";
        } else {
          thtml = "<li id=\"main-menu-"+_MyPgmList[i].SORT_CODE.toLowerCase()+"\" ><a href=\"#\" data-pgmcode=\""+_MyPgmList[i].PGM_CODE+"\" data-pgmname=\""+_MyPgmList[i].PGM_NAME+"\" data-openurl=\""+_MyPgmList[i].OPEN_URL+"\">"+_MyPgmList[i].PGM_NAME+"</a></li>";
        }

        $("#main-menu-"+_MyPgmList[i].PARENT_SORT_CODE.toLowerCase()+">.dropdown-menu").append(thtml);
      }
    }
  }
}

// 메뉴를 그립시다. Top & Side 메뉴
_X.SetTopMenuTS = function() {
  var thtml = "";
  for(var i=0; i<_MyPgmList.length; i++) {
    thtml = "";

    if(_MyPgmList[i].MENU_LEVEL=="1") {
      //$("#main-menu").append("<li class=\"sys-"+_MyPgmList[i].PGM_CODE.toLowerCase()+"\"><a href=\"#\" data-pgmcode=\""+_MyPgmList[i].PGM_CODE+"\" data-sidepanel=\"" + _MyPgmList[i].SIDEPANEL_YN + "\" >"+_MyPgmList[i].PGM_NAME+"</a></li>");
    } else {
      
      if(_MyPgmList[i].MENU_LEVEL=="2") {
        if ($("#sub-menu-"+_MyPgmList[i].SYS_ID.toLowerCase()).length == 0) {
          thtml = "<div id=\"sub-menu-"+_MyPgmList[i].SYS_ID.toLowerCase()+"\" class=\"sub-menu-panel hide\" >";
          thtml += "<ul class=\"nav navbar-nav\"></ul>";
          thtml += "</div>"; // #sub-menu-(sys_id)

          $(".x5-side-panel").append(thtml);
        }

        thtml = "<li class=\"panel panel-default\" id=\"sub-menu-"+_MyPgmList[i].PGM_CODE.toLowerCase()+"\"></li>";
        $("#sub-menu-"+_MyPgmList[i].SYS_ID.toLowerCase()+">ul.navbar-nav").append(thtml);

        thtml = "<a data-toggle=\"collapse\" href=\"#sub-menu-dd-"+_MyPgmList[i].PGM_CODE.toLowerCase()+"\"><span class=\"fa fa-folder\"></span> "+_MyPgmList[i].PGM_NAME+" <span class=\"fa fa-caret-down\"></span></a>";
        thtml += "<div id=\"sub-menu-dd-"+_MyPgmList[i].PGM_CODE.toLowerCase()+"\" class=\"panel-collapse collapse in\">";
        thtml += "<div class=\"panel-body\">";
        thtml += "<ul id=\"sub-menu-dl-"+_MyPgmList[i].PGM_CODE.toLowerCase()+"\" class=\"nav navbar-nav sub-menu-dl\">";
        thtml += "</ul>";  // .navbar-nav
        thtml += "</div>"; // .panel-body
        thtml += "</div>"; // .panel-collapse

        $("#sub-menu-"+_MyPgmList[i].PGM_CODE.toLowerCase()).html(thtml);

      } else {
        thtml = "<li><a href=\"#\" data-pgmcode=\""+_MyPgmList[i].PGM_CODE+"\" data-pgmname=\""+_MyPgmList[i].PGM_NAME+"\" data-openurl=\""+_MyPgmList[i].OPEN_URL+"\"><span class=\"fa fa-chevron-right\"></span> "+_MyPgmList[i].PGM_NAME+"</a></li>";
        $("#sub-menu-dl-"+_MyPgmList[i].PARENT_SORT_CODE.toLowerCase()).append(thtml);
      }
      
    }
  }
}
﻿/**
 * Object :  mighty-mdi-1.2.0.js
 * @Description : 공통처리 javascript
 * @author :엑스인터넷정보 김양열
 * @since : 2013.02.10
 * @version : 1.2.8
 *
 * @Modification Information
 * <pre>
 *   since    	  author           Description
 *  ----------    --------    ---------------------------
 *   2013.02.10    김양열        최초생성
 */

var _SubFormIndex	= 0;

// 윈도우 타입
var	WINTYPE_NORMAL 	= 1;   // NEW WINDOW
var	WINTYPE_MODAL  	= 2;   // MODAL DIALOG
var	WINTYPE_MODELESS= 3;   // MODELESS DIALOG

var	icon_Information= 1;
var	icon_StopSign	= 2;
var	icon_Exclamation= 3;
var	icon_Question	= 4;
var	icon_None		= 5;
var	icon_Ok			= 6;

var	btn_OK			= 1;
var	btn_OKCancel	= 2;
var	btn_YesNo		= 3;
var	btn_YesNoCancel	= 4;
var	btn_RetryCancel	= 5;
var	btn_AbortRetryIgnore =6;

// 키
var	KEY_DELETE = 46;
var	KEY_BACKSPACE = 8;
var	KEY_TAB = 9;
var	KEY_ESC = 27;
var	KEY_SPACE_BAR = 32;
var	KEY_F1   = 112;
var	KEY_F2   = 113;
var	KEY_F3   = 114;
var	KEY_F4   = 115;
var	KEY_F5   = 116;
var	KEY_F6   = 117;
var	KEY_F7   = 118;
var	KEY_F8   = 119;
var	KEY_F9   = 120;
var	KEY_F10  = 121;
var	KEY_F11  = 122;
var	KEY_F12  = 123;
var	KEY_PAGEUP  = 33;
var	KEY_PAGEDOWN  = 34;
var	KEY_LEFT = 37;
var	KEY_UP   = 38;
var	KEY_RIGHT = 39;
var	KEY_DOWN  = 40;
var	KEY_ENTER = 13;
var	KEY_SHIFT = 16;
var	KEY_CONTROL = 17;
var	KEY_ALT 	= 18;
var	KEY_A = 97;
var	KEY_a = 65;
var	KEY_b = 66;
var	KEY_c = 67;
var	KEY_d = 68;
var	KEY_e = 69;
var	KEY_f = 70;
var	KEY_g = 71;
var	KEY_h = 72;
var	KEY_i = 73;
var	KEY_j = 74;
var	KEY_k = 75;
var	KEY_l = 76;
var	KEY_m = 77;
var	KEY_n = 78;
var	KEY_o = 79;
var	KEY_p = 80;
var	KEY_q = 81;
var	KEY_r = 82;
var	KEY_s = 83;
var	KEY_t = 84;
var	KEY_u = 85;
var	KEY_v = 86;
var	KEY_w = 87;
var	KEY_x = 88;
var	KEY_y = 89;
var	KEY_z = 90;

var	is_version = navigator.appVersion.split(";");
var	ii_rtvalue = 0;

if(mytop._StringBuilder!="undefined" || mytop._StringBuilder==null)
{
	mytop._StringBuilder = "";
}
//var _StringBuilder = "";
var _IE   = (navigator.appVersion.indexOf("MSIE")>=0);
var _IE8  = (navigator.appVersion.indexOf("MSIE 11")>=0 || navigator.appVersion.indexOf("MSIE 10")>=0 || navigator.appVersion.indexOf("MSIE 9")>=0 || navigator.appVersion.indexOf("MSIE 8")>=0);
var _IE9  = (navigator.appVersion.indexOf("MSIE 11")>=0 || navigator.appVersion.indexOf("MSIE 10")>=0 || navigator.appVersion.indexOf("MSIE 9")>=0);
var _IE10 = (navigator.appVersion.indexOf("MSIE 11")>=0 || navigator.appVersion.indexOf("MSIE 10")>=0);
var _IE11 = (navigator.appVersion.indexOf("MSIE 11")>=0);

/**
 * fn_name :  _X.Xml2Json
 * Description : xml to json 변환
 * param :xmlData : xml data
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.Xml2Json = function(xmlData){
	var jsonData="";
	eval("jsonData=" + _X.xml2json(parseXml(xmlData)));
	return jsonData;
};

_X.xml2json = function(xml, tab) {
   var X = {
      toObj: function(xml) {
         var o = {};
         if (xml.nodeType==1) {   // element node ..
            if (xml.attributes.length)   // element with attributes  ..
               for (var i=0; i<xml.attributes.length; i++)
                  o["@"+xml.attributes[i].nodeName] = (xml.attributes[i].nodeValue||"").toString();
            if (xml.firstChild) { // element has child nodes ..
               var textChild=0, cdataChild=0, hasElementChild=false;
               for (var n=xml.firstChild; n; n=n.nextSibling) {
                  if (n.nodeType==1) hasElementChild = true;
                  else if (n.nodeType==3 && n.nodeValue.match(/[^ \f\n\r\t\v]/)) textChild++; // non-whitespace text
                  else if (n.nodeType==4) cdataChild++; // cdata section node
               }
               if (hasElementChild) {
                  if (textChild < 2 && cdataChild < 2) { // structured element with evtl. a single text or/and cdata node ..
                     X.removeWhite(xml);
                     for (var n=xml.firstChild; n; n=n.nextSibling) {
                        if (n.nodeType == 3)  // text node
                           o["#text"] = X.escape(n.nodeValue);
                        else if (n.nodeType == 4)  // cdata node
                           o["#cdata"] = X.escape(n.nodeValue);
                        else if (o[n.nodeName]) {  // multiple occurence of element ..
                           if (o[n.nodeName] instanceof Array)
                              o[n.nodeName][o[n.nodeName].length] = X.toObj(n);
                           else
                              o[n.nodeName] = [o[n.nodeName], X.toObj(n)];
                        }
                        else  // first occurence of element..
                           o[n.nodeName] = X.toObj(n);
                     }
                  }
                  else { // mixed content
                     if (!xml.attributes.length)
                        o = X.escape(X.innerXml(xml));
                     else
                        o["#text"] = X.escape(X.innerXml(xml));
                  }
               }
               else if (textChild) { // pure text
                  if (!xml.attributes.length)
                     o = X.escape(X.innerXml(xml));
                  else
                     o["#text"] = X.escape(X.innerXml(xml));
               }
               else if (cdataChild) { // cdata
                  if (cdataChild > 1)
                     o = X.escape(X.innerXml(xml));
                  else
                     for (var n=xml.firstChild; n; n=n.nextSibling)
                        o["#cdata"] = X.escape(n.nodeValue);
               }
            }
            if (!xml.attributes.length && !xml.firstChild) o = null;
         }
         else if (xml.nodeType==9) { // document.node
            o = X.toObj(xml.documentElement);
         }
         else {
            //alert("unhandled node type: " + xml.nodeType);
		}
         return o;
      },
      toJson: function(o, name, ind) {
         var json = name ? ("\""+name+"\"") : "";
         if (o instanceof Array) {
            for (var i=0,n=o.length; i<n; i++)
               o[i] = X.toJson(o[i], "", ind+"\t");
            json += (name?":[":"[") + (o.length > 1 ? ("\n"+ind+"\t"+o.join(",\n"+ind+"\t")+"\n"+ind) : o.join("")) + "]";
         }
         else if (o == null)
            json += (name&&":") + "null";
         else if (typeof(o) == "object") {
            var arr = [];
            for (var m in o)
               arr[arr.length] = X.toJson(o[m], m, ind+"\t");
            json += (name?":{":"{") + (arr.length > 1 ? ("\n"+ind+"\t"+arr.join(",\n"+ind+"\t")+"\n"+ind) : arr.join("")) + "}";
         }
         else if (typeof(o) == "string")
            json += (name&&":") + "\"" + o.toString() + "\"";
         else
            json += (name&&":") + o.toString();
         return json;
      },
      innerXml: function(node) {
         var s = ""
         if ("innerHTML" in node)
            s = node.innerHTML;
         else {
            var asXml = function(n) {
               var s = "";
               if (n.nodeType == 1) {
                  s += "<" + n.nodeName;
                  for (var i=0; i<n.attributes.length;i++)
                     s += " " + n.attributes[i].nodeName + "=\"" + (n.attributes[i].nodeValue||"").toString() + "\"";
                  if (n.firstChild) {
                     s += ">";
                     for (var c=n.firstChild; c; c=c.nextSibling)
                        s += asXml(c);
                     s += "</"+n.nodeName+">";
                  }
                  else
                     s += "/>";
               }
               else if (n.nodeType == 3)
                  s += n.nodeValue;
               else if (n.nodeType == 4)
                  s += "<![CDATA[" + n.nodeValue + "]]>";
               return s;
            };
            for (var c=node.firstChild; c; c=c.nextSibling)
               s += asXml(c);
         }
         return s;
      },
      escape: function(txt) {
         return txt.replace(/[\\]/g, "\\\\")
                   .replace(/[\"]/g, '\\"')
                   .replace(/[\n]/g, '\\n')
                   .replace(/[\r]/g, '\\r');
      },
      removeWhite: function(e) {
         e.normalize();
         for (var n = e.firstChild; n; ) {
            if (n.nodeType == 3) {  // text node
               if (!n.nodeValue.match(/[^ \f\n\r\t\v]/)) { // pure whitespace text node
                  var nxt = n.nextSibling;
                  e.removeChild(n);
                  n = nxt;
               }
               else
                  n = n.nextSibling;
            }
            else if (n.nodeType == 1) {  // element node
               X.removeWhite(n);
               n = n.nextSibling;
            }
            else                      // any other node
               n = n.nextSibling;
         }
         return e;
      }
   };
   if (xml.nodeType == 9) // document node
      xml = xml.documentElement;
   var json = X.toJson(X.toObj(X.removeWhite(xml)), xml.nodeName, "\t");
   return "{\n" + (tab ? json.replace(/\t/g, tab) : json.replace(/\t|\n/g, "")) + "\n}";
};

_X.parseXml = function(xml) {
  var dom = null;
  if (window.DOMParser) {
   try
   {
   dom = (new DOMParser()).parseFromString(xml, "text/xml");
  }
  catch (e)
   {
    dom = null;
   }
  }
  else if (window.ActiveXObject) {
   try
   {
    dom = new ActiveXObject('Microsoft.XMLDOM');
    dom.async = false;
    if (!dom.loadXML(xml)) // parse error ..
    {
    //window.alert(dom.parseError.reason + dom.parseError.srcText);
    }
   }
   catch (e)
   {
    dom = null;
   }
  }
  else
  {
   //alert("cannot parse xml string!");
  }
  return dom;
};

/**
 * fn_name :  _X.getCookieVal
 * Description : offset 으로 cookie 값읽기
 * param : offset : offset value
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.getCookieVal = function(offset) {
   var endstr = document.cookie.indexOf (";", offset);
   if (endstr == -1) endstr = document.cookie.length;
   return unescape (document.cookie.substring(offset, endstr));
};
_X.GetCookieVal = _X.getCookieVal;

/**
 * fn_name :  _X.GetCookie
 * Description : 쿠키몀 으로 cookie 값읽기
 * param : name : 쿠키명
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.GetCookie = function (name) {
   var arg = name+"=";
   var alen = arg.length;
   var clen = document.cookie.length;
   var i = 0;
   while (i < clen) {
      var j = i + alen;
      //수정(2016.08.15 KYY)
      if (document.cookie.substring(i, j) == arg) return _X.getCookieVal(j);
      i = document.cookie.indexOf(" ", i) + 1;
      if (i == 0) break;
   }
   return null;
};

/**
 * fn_name :  _X.setCookieVal
 * Description : 쿠키몀 값 설정
 * param : name:쿠키명, value:쿠키값
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.SetCookie = function(name, value) {
	//수정(2016.08.15 KYY)
	//var argv = SetCookie.arguments;
	//var argc = SetCookie.arguments.length;
	var argv = arguments;
	var argc = arguments.length;
	var expires;
	if (argc > 2) {
		expires = argv[2];
	} else {
		expires = new Date();
		expires.setTime(expires.getTime() + (30 * 24 * 60 * 60 * 1000));
	}
	var path = (argc > 3) ? argv[3] : null;
	var domain = (argc > 4) ? argv[4] : null;
	var secure = (argc > 5) ? argv[5] : false;
	document.cookie = name + "=" + escape (value) +
	((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
	((path == null) ? "" : ("; path=" + path)) +
	((domain == null) ? "" : ("; domain=" + domain)) +
	((secure == true) ? "; secure" : "")	+
	//"; httpOnly";	//보안관련 설정추가 (2015.02.26 KYY)
	";";	//보안관련 설정삭제 (2016.08.15 KYY)
};

/**
 * fn_name :  _X.DelCookie
 * Description : 쿠키몀 값 삭제
 * param : name : 쿠키명
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.DelCookie  = function(name){
	exp=new Date();
	exp.setTime (exp.getTime() - 1);
	var cval = GetCookie ("name");
	document.cookie = name + "=" + cval +"; expires=" + exp.toGMTString()+ "; httpOnly";
};

_X.RestoreImg = function(name, nsdoc){
  var img = eval((navigator.appName.indexOf('Netscape', 0) != -1) ? nsdoc+'.'+name : 'document.all.'+name);
  if (name == '')
    return;
  if (img && img.altsrc) {
    img.src    = img.altsrc;
    img.altsrc = null;
  }
};

_X.PreLoadImg = function(){
  var img_list = jf_preload_img.arguments;
  if (document.preloadlist == null)
    document.preloadlist = new Array();
  var top = document.preloadlist.length;
  for (var i=0; i < img_list.length; i++) {
    document.preloadlist[top+i]     = new Image;
    document.preloadlist[top+i].src = img_list[i+1];
  }
};

_X.SetImg = function(a_obj, a_img) {
	if (typeof(a_obj)!="undefined"&&!a_obj.disabled)
		a_obj.src = a_img;
};

_X.ChangeImg = function(name, nsdoc, rpath, preload) {
  var img = eval((navigator.appName.indexOf('Netscape', 0) != -1) ? nsdoc+'.'+name : 'document.all.'+name);
  if (name == '')
    return;
  if (img) {
    img.altsrc = img.src;
    img.src    = rpath;
  }
};

MM_swapImgRestore = function() { //v3.0
	var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
};

MM_preloadImages = function() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
};

MM_findObj = function(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
};

MM_swapImage = function() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
};

_X.RootDir = function () {
	return mytop._rootDIR;
};

_X.OpenPopup = function (a_url, a_name, a_width, a_height, a_left, a_top, a_win) {
  var str = "status=0,location=0,height=" + a_height + ",innerHeight=" + a_height;
  str += ",width=" + a_width + ",innerWidth=" + a_width;
	if(a_win==null)
		a_win=window;

  if (a_name == "resizable" || a_name == "") {str = "resizable=1," + str;}

  if (a_win.screen) {
    var ah = screen.availHeight - 30;
    var aw = screen.availWidth - 10;

    var xc = (a_left==null?(aw - a_width) / 2: a_left);
    var yc = (a_top==null?(ah - a_height) / 2: a_top);

    str += ",left=" + xc + ",screenX=" + xc;
    str += ",top=" + yc + ",screenY=" + yc;
  }
  return a_win.open(a_url, a_name, str);
};

_X.OpenModal = function (a_url, a_width, a_height, a_left, a_top, a_win, a_resizable) {
		if(a_resizable==null) {a_resizable=true}
		if(a_win==null||typeof(a_win)=="undefined")
			a_win=window;
		var rtValue	= null;
		//alert(a_win.document.location);
		if (a_width==null || a_width<0) {
			rtValue = a_win.showModalDialog(a_url,a_win,"dialogWidth:1px;dialogHeight:1px;dialogLeft:2000px;dialogTop:2000px;center:0;scroll:0;help:0;status:0;resizable:" + (a_resizable?1:0) + ";");
		}
		else {
			rtValue = a_win.showModalDialog(a_url,a_win,"dialogWidth:" + (a_width +8) + "px;dialogHeight:" + (a_height +30) + "px;scroll:0;help:0;status:0;edge:sunken;resizable:" + (a_resizable?1:0) + ";" + (a_left==null||a_left<=0?"center:1;":"dialogLeft:" + a_left + ";dialogTop:" + a_top + ";center:0;"));
		}
		return (typeof(rtValue)!="undefined"?rtValue:null);
}

_X.OpenModal_OLD = function (a_url, a_width, a_height, a_left, a_top, a_win, a_resizable) {
	var is_returnValue=null;
	if(a_resizable==null) {a_resizable=true;}
	if(a_win==null||typeof(a_win)=="undefined") {a_win=window;}
	a_win.ii_rtvalue = 1999;
	if(a_win==null)	{a_win=window;}
	if (a_width==null || a_width<0) {
		is_returnValue = a_win.showModalDialog(a_url,a_win,"dialogWidth:1px;dialogHeight:1px;dialogLeft:2000px;dialogTop:2000px;center:0;scroll:0;help:0;status:0;resizable:" + (a_resizable?1:0) + ";");
	}
	else {
		//is_returnValue = a_win.showModalDialog(a_url,a_win,"dialogWidth:" + (a_width + 8) + "px;dialogHeight:" + (a_height + 30) + "px;scroll:0;help:1;status:1;edge:sunken;resizable:" + (a_resizable?1:0) + ";" + (a_left==null||a_left<=0?"center:1;":"dialogLeft:" + a_left + ";dialogTop:" + a_top + ";center:0;"));
		is_returnValue = a_win.showModalDialog(a_url,a_win);
	}
	if(typeof(a_win.ii_rtvalue)=='object' || a_win.ii_rtvalue!=1999) {
		return a_win.ii_rtvalue;
	}	else {
		return (typeof(is_returnValue)!="undefined"?is_returnValue:null);
	}
};

_X.OpenModalless = function (a_url, a_width, a_height, a_left, a_top, a_win, a_resizable) {
	var dialogWin;
	if(a_win==null)
		a_win=window;
	if (a_width==null || a_width<0) {
		dialogWin = a_win.showModelessDialog(a_url,a_win,"dialogWidth:1px;dialogHeight:1px;dialogLeft:2000px;dialogTop:2000px;center:0;scroll:0;help:0;status:0;resizable:1;");
	}
	else {
		dialogWin = a_win.showModelessDialog(a_url,a_win,"dialogWidth:" + (a_width +8) + "px;dialogHeight:" + (a_height +30) + "px;scroll:0;help:0;status:0;edge:sunken;resizable:1" + (a_left==null||a_left<=0?"center:1;":"dialogLeft:" + a_left + ";dialogTop:" + a_top + ";center:0;"));
	}
	return dialogWin;
};

/**
 * fn_name :  _X.StringBuilder
 * Description : 문자열 관련 함수
 * param : value : string value
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.StringBuilder = function (value){
    this.strings = new Array("");
    this.append(value);
};

// Appends the given value to the end of this instance.
_X.StringBuilder.prototype.append = function (value){
    if (value)
    {
        this.strings.push(value);
    }
};

// Clears the string buffer
_X.StringBuilder.prototype.clear = function (){
    this.strings.length = 1;
};

// Converts this instance to a String.
_X.StringBuilder.prototype.toString = function (){
    return this.strings.join("");
};

/**
 * fn_name :  _X.strPurify
 * Description : '.','/','-' 문자제거
 * param : a_str : 제거하고자 하는 문자(default:./-, a_ch:변경값(default:"")
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.strPurify = function (a_str, a_ch) {
    if ( a_ch == "" || a_ch == null ) {
        return _X.StrPurify(_X.StrPurify(_X.StrPurify(String(a_str), "."), "/"), "-");
    }

    var v_ret = a_str;
	 var v_i = 0;

    while ( v_ret.indexOf(a_ch) != -1 ) {
        v_i = v_ret.indexOf(a_ch);
        v_ret = v_ret.substr(0,v_i) + v_ret.substr(v_i+1,v_ret.length-v_i) ;
    }

    return v_ret;
};

_X.StrPurify = _X.strPurify;

/**
 * fn_name :  _X.ArryToString
 * Description : array 를 string으로 변환
 * param : a_array:문자형배열,a_separator:구분자
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.ArryToString = function (a_array,a_separator)
{
	if(a_separator==null) a_separator=",";
	var ls_return="";

	for(var i=0;i<a_array.length;i++)
		ls_return+=(i==0?"":a_separator)+a_array[i];
	return ls_return;
};

/**
 * fn_name :  _X.ByteLen
 * Description : 문자열의 Byte 수를 구한다.
 * param : bstr : 문자열
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.ByteLen = function (bstr) {
	len = bstr.length;
	for (var i=0; i<bstr.length; i++){
	 xx = bstr.substr(i,1).charCodeAt(0);
	 if (xx > 127) { len++; }
	}
	return len;
};

_X.ByteLength = function (bstr) {
	return _X.ByteLen(bstr);
};

/**
 * fn_name :  _X.GetToken
 * Description : 문자열의 구분자에 의한 배열값을 반환한다.
 * param : str:문자열, delimiter:구분자, nth:max array index
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.GetToken = function (str, delimiter, nth) {
  var tokens = str.split(delimiter);

  if ((nth <= 0) || (nth > tokens.length)) return "";
  else return _X.Trim(tokens[nth-1]);
};

/**
 * fn_name :  _X.Trim
 * Description : 문자열 양쪽의 whitespace를 모두 제거한다.
 * param : str:문자열
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.Trim = function(str) {
	if (!str||typeof(str)!="string")
		return str; //입력값 없는 경우는 Pass
	else
		return str.replace(/^\s*/ ,"").replace(/\s*$/ ,"");
};

/**
 * fn_name :  _X.LTrim
 * Description : 문자열 왼쪽의 whitespace를 모두 제거한다.
 * param : str:문자열
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.LTrim = function (str) {
	if (!str||typeof(str)!="string")
		return str; //입력값 없는 경우는 Pass
	else
		return str.replace(/^\s*/,"");
};

/**
 * fn_name : _X.RTrim
 * Description : 문자열 오른쪽의 whitespace를 모두 제거한다.
 * param : str:문자열
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.RTrim = function (str) {
	if (!str||typeof(str)!="string")
		return str; //입력값 없는 경우는 Pass
	else
		return str.replace(/\s*$/,"");
};

/**
 * fn_name : _X.LPad
 * Description : 문자열의 정해진 길이만큼 나머지부분을 특정한 문자로 채워서 반환하는 함수
 * param : psStr:문지열, piLen:전체길이, psSeed:치환값
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.LPad = function (psStr, piLen, psSeed) {
	if(psSeed==null) psSeed='0';
	return _X.Pad(psStr, piLen, psSeed, 'left');
};

/**
 * fn_name : _X.RPad
 * Description : 문자열의 정해진 길이만큼 나머지부분을 특정한 문자로 채워서 반환하는 함수
 * param : psStr:문지열, piLen:전체길이, psSeed:치환값
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.RPad = function (psStr, piLen, psSeed) {
	if(psSeed==null) psSeed='0';
	return _X.Pad(psStr, piLen, psSeed, 'right');
};

_X.Pad = function (psStr, piLen, psSeed, psTag) {
	var sRtnStr = String(psStr);

	psSeed = String(psSeed);
	if (_X.ByteLen(psSeed) != 1) return sRtnStr;

	for (var i = 0; i < (piLen - _X.ByteLen(String(psStr))); i++) {
	  sRtnStr = (psTag=='left' ? psSeed + sRtnStr : sRtnStr + psSeed);
	}

	return sRtnStr;
};

/**
 * fn_name : _X.Left
 * Description : 왼쪽 문자열 자르기
 * param : str:문자열, n:길이
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.Left = function(str, n){
	if (n <= 0)
	  return "";
	else if (n > String(str).length)
	  return str;
	else
	  return String(str).substring(0,n);
};

/**
 * fn_name : _X.Right
 * Description : 오른쪽 문자열 자르기
 * param : str:문자열, n:길이
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.Right = function(str, n){
  if (n <= 0)
     return "";
  else if (n > String(str).length)
     return str;
  else {
     var iLen = String(str).length;
     return String(str).substring(iLen, iLen - n);
  }
};

/**
 * fn_name : _X.ChkLen
 * Description : 문자열의 Byte 길이가 제한길이를 초과했는지 체크한다.
 * param : str:문자열, limit:제한길이
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.ChkLen = function (str, limit) {
	if (_X.ByteLen(str) > limit )
	 return false;
	else
	 return true;
};

/**
 * fn_name : _X.ChkInputLen
 * Description :  TextArea,TextField의 입력 문자열이 길이제한을 초과했는지 체크한다.
 * param : str:문자열, limit:제한길이,inputNm:text object id
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.ChkInputLen = function (input, limit, inputNm) {
  var itemLength = _X.ByteLen(input.value);
  if ( itemLength > limit ) {
    input.focus();
    return false;
  }
  return true;
};

/**
 * fn_name : _X.ClearSpaces
 * Description :  문자열 중간의 공백을 없애는 함수
 * param : str:문자열
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.ClearSpaces = function (str) {
	var newStr = "";
	for (var i=0; i< str.length; i++) {
		 if ( (str.charAt(i) != " ") && (str.charAt(i) != " ") ) {
			newStr = newStr + str.charAt(i);
		 }
	}
	return newStr;
};

/**
 * fn_name : _X.ClearZero
 * Description :  문자열 앞에 0을 제거 한다.
 * param : str:문자열
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.ClearZero = function (a_str) {
  a_str = String(a_str);
  if (a_str.charAt(0)!="0") return a_str;
  var pos = 0;
  for (var i = 1; i< a_str.length; i++) {
    if (a_str.charAt(i) != "0") {
      pos = i;
      break;
    }
  }
  return a_str.substring(pos, a_str.length);
};

/**
 * fn_name :  _X.StrReplace
 * Description : 스트링의 문자열을 주어진 문자열로 변경
 * param :   a_str:문자열,a_old:이전문자열,a_new:변경문자열
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.StrReplace = function(a_str,a_old,a_new) {
    if (!a_str) return "";
    if (!a_old) return a_str;
    if (!a_new) a_new = "";

		var temp = a_str.split(a_old);
		return temp.join(a_new);
};

/**
 * fn_name :  _X.StrRegReplace
 * Description : 스트링의 문자열을 주어진 문자로 변경
 * param :   a_str:문자열,a_old:이전문자열,a_new:변경문자열
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.StrRegReplace = function(a_str,a_old,a_new) {
    if (!a_str) return "";
    if (!a_old) return a_str;
    if (!a_new) a_new = "";

		var r = new RegExp(a_old, 'g');
		return a_str.replace(r, a_new);
};

/**
 * fn_name :  _X.ArrayToStr
 * Description : 배열을 문자열로 변경
 * param :   a_arr:문자열배열,colSepa:컬럼구분자
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.ArrayToStr = function(a_arr,colSepa) {
	if(colSepa==null) colSepa=_Col_Separate;

	if(a_arr==null) return;

	var rtVal = "";
	for(var i=0; i<a_arr.length; i++)
	{
		rtVal += (i==0 ? "" : colSepa) + a_arr[i] + "";
	}
	return rtVal;
};

/**
 * fn_name :  _X.IsNumber
 * Description : 숫자인지 체크
 * param :   psNum:문자열
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.IsNumber = function (psNum) {
	if(typeof(psNum)) return true;//number type 일 경우 _X.StrReplace()함수 호출 시 에러 발생하여 바로 return true 처리. (2015.02.09 이상규)
	psNum = _X.StrReplace(_X.StrReplace(_X.StrReplace(_X.StrReplace(psNum,',',''),'.',''),'-',''),'+','');
	fmt = /^\d+\d*$/;
	return fmt.test(psNum);
};
_X.isNumeric = _X.IsNumber;

/**
 * fn_name :  _X.ChkKey
 * Description : 입력값 체크
 * param :   a_keytype: keytype(한글,영문,숫자,,,)
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.ChkKey = function (a_keytype) {
	if(event==null)
		return;
	switch (a_keytype) {
		case 'K':	//한글만
			break;
		case 'E':	//영문만
			break;
		case 'N':	//숫자만
			if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;
			break;
		case 'NB':	//숫자만, Del, backspace - 박종현
			if ((event.keyCode < 48 || event.keyCode > 57) && event.keyCode != 8 && event.keyCode != 46 && (event.keyCode < 96 || event.keyCode > 105)){
				event.returnValue = false;
			}
			break;
		case 'EN':	//숫자,영문만

			break;
		default:
			event.returnValue = false;
			break;
	}
	return
};

/**
 * fn_name :  _X.DateMask
 * Description : 문자에 마스크 삽입
 * param :  as_date:날짜형의문자열, a_char:구분자
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.DateMask = function (as_date, a_char) {
	if (a_char==null) a_char='.';
	return as_date.substr(0,4) + a_char + as_date.substr(4,2) + a_char + as_date.substr(6,2);
};


/**
 * fn_name :  _X.ToInt
 * Description : 문자열을 숫자로.
 * param :  a_str:문자열
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.ToInt = function(a_str) {
  if (a_str==null || a_str=="") return 0;
	if (typeof(a_str)=="string"){
		a_str = _X.StrReplace(_X.StrReplace(_X.StrReplace(_X.StrReplace(a_str,' ', ''),'!',''),'px',''),',','');
		if (isNaN(a_str)) return 0;
		if (a_str.charAt(0)!="0") return parseInt(a_str);
	}
  var rtValue = parseInt(_X.ClearZero(a_str));
  return isNaN(rtValue) ? 0 : rtValue;
};
_X.toInt = _X.ToInt;

/**
 * fn_name :  _X.CustMask
 * Description : 사업자번호 마스크 삽입
 * param :  a_str:문자열
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.CustMask = function (a_str) {
  if (a_str.length==10)
  		return 	a_str.substr(0,3) + '-' + a_str.substr(3,2) + '-' + a_str.substr(5,5);
  else if (a_str.length==13)
  		return 	a_str.substr(0,6) + '-' + a_str.substr(6,7);
  else
  		return a_str;
};

/**
 * fn_name :  _X.GetWildCode
 * Description : '%'문자열 앞뒤로 join 하여 ㅁ반환
 * param :  a_str:문자열
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.GetWildCode = function (a_str) {
	if(a_str=='%') return '%';
	var	li_len = a_str.length;

	for (var i=li_len;i>=1;i--) {
		if(a_str.substr(i-1,1)!='0') {
			return a_str.substr(0,i) + '%';
		}
	}
	return a_str;
};

/**
 * fn_name :  _X.Split
 * Description : 문자열을 배열로 리턴한다.
 * param :  a_str:문자열
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.Split = function (a_str){
	if(a_str.indexOf(_Row_Separate)>=0)
		return a_str.split(_Row_Separate);
	else if(a_str.indexOf(_Col_Separate)>=0)
		return a_str.split(_Col_Separate);
	else if(a_str.indexOf(_Field_Separate)>=0)
		return a_str.split(_Field_Separate);
	else if(a_str.indexOf('|s|')>=0)
		return a_str.split('|s|');
	else if(a_str.indexOf('|')>=0)
		return a_str.split('|');
	else if(a_str.indexOf('@')>=0)
		return a_str.split('@');
	else
		return a_str.split(',');
};
_X.split = _X.Split;

/**
 * fn_name :  _X.FormatComma
 * Description : 1234567 ->1,234,567 로 변경한다.
 * param :  num:숫자형 문자열, div:구분자
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.FormatComma = function (num, div) {
	if(div==null || div=='') div = 1;
	var currency = '';

	if(div == 1){
		num = String(parseInt(_X.ClearZero(num), 10));
	}else{
		num = String(num);
	}

	while(num.substr(num.length - 3, 3).length == 3){
		currency = ',' + num.substr(num.length - 3, 3) + currency;
		num = num.substr(0, num.length - 3);
	}
	if ( num.length == 0 )
		currency = currency.substr(1, currency.length);
	else
		currency = num + currency;

	return currency;
};

_X.FormatComma2 = function (strValue) {
	if(typeof(strValue) != "string") strValue = strValue + "";
	var objRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
  while(objRegExp.test(strValue)) {
  	strValue = strValue.replace(objRegExp, '$1,$2');
  }
	return strValue;
}

/**
 * fn_name :  _X.FormatCommaPoint
 * Description : 1234567 ->1,234,567 로 변경한다.
 * param :  num:숫자형 문자열, len:길이, div:구분자
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.FormatCommaPoint = function (num, len, div) {
  if(len==null || len=='') len = -1;
  if(div==null || div=='') div = 1;
  var currency = '';
  var numPoint = '';
  var isMinus = false;
  if(num<0) {
    isMinus = true;
    num = num * -1;
  }
  /*
    mighty-win-1.2.0.js에서 mask 적용 시
    mighty-dao-1.2.0.js에서 x_GridRowSync 시
    인자 num이 숫자 타입으로 넘어올 때 indexOf() 에러 수정 (2015.01.23 이상규)
  */
  num = num + "";

  if(num.indexOf('.') != -1){
    //0.x 인 경우 오류남(2017.11.21 KYY)
    if(len!=-1) {
      numPoint = num.substring(num.indexOf('.'), num.length);      
    }
    num = num.substring(0, num.indexOf('.'));
  }
  if(num==""){num="0"}
  if(div == 1){
    if(num!="0") {
     num = String(parseInt(_X.ClearZero(num), 10)); 
    }
  }else{
    num = String(num);
  }

  while(num.substr(num.length - 3, 3).length == 3){
    currency = ',' + num.substr(num.length - 3, 3) + currency;
    num = num.substr(0, num.length - 3);
  }
  if ( num.length == 0 )
    currency = currency.substr(1, currency.length);
  else
    currency = num + currency;

  return (isMinus ? "-" : "") + currency + numPoint;
};

//2015.08.17 이상규
//수정(2017.05.24 KYY)
//수정(2017.12.04 KYY) - 소수점이하가 0일때 표시안함
_X.FormatCommaPoint2 = function (num, len, comma, zerotonull, removezero) {
  if (num === "" || isNaN(num)) {
    return "";
  }
 
  var isMinus = false;
  if(num<0) {
    isMinus = true;
    num = num * -1;
  } 
  if (zerotonull !==null && zerotonull && num==0) {
    return "";
  }

  var currency = "",
      snum = num + "",
      l, r;
  
  if (snum.indexOf(".") !== -1) {
    l = snum.split(".")[0] || "0";
    r = snum.split(".")[1];
  } else {
    l = parseInt(num, 10) + "";
    r = "";
  }
  
  if (len > 0) {
    if (r.length > len) {
      r = r.substring(0, len);
    } else {
      r = _X.RPad(r, len, "0");
    }
    r = "." + r;
  }
  else {
    r = "";
  }
  
  while (!comma && l.length > 3) {
    currency = "," + l.substr(l.length - 3, 3) + currency;
    l = l.substring(0, l.length - 3);
  }
  
  var retVal = l + currency + r;
  
  if ( retVal.substring(0, 1) == "." ) {
  	retVal = "0" + retVal;
  }
  
  retVal = retVal.replace("-,", "-").replace("-.", "-0.");

  if(removezero !==null && removezero){
    //수정(2017.12.04 KYY) - 소수점이하가 0일때 표시안함
    if(retVal.indexOf(".")>0 && parseInt(retVal)==parseFloat(retVal)){
     retVal = retVal.substr(0,retVal.indexOf("."));
    }
  }
  return (isMinus ? "-" : "") + retVal;
}

/**
 * fn_name :  _X.Round
 * Description : num를 소수 po자리에서 반올림 한다.
 * param :  num:문자열, po:반올림 소수점자리수
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.Round = function (num, po) {
	return Math.round(num * Math.pow(10,po))/Math.pow(10,po);
};
_X.round = _X.Round;

/**
 * fn_name :  _X.KoreanNumber
 * Description : 수치금액을 한글로 풀어서 돌려준다. (단위사이에 Space를 둔다)
 * param :  ad_num:문자열
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.KoreanNumber = function (ad_num) {
	if(typeof(ad_num)!="number" || ad_num==null || ad_num=='') return '';
	var i, li_len, li_mod;
	var ls_num, ls_return = "", ls_temp = "";
	var ls_str  = new Array();
	var ls_UNIT = new Array('천','백','십','조','천','백','십','억','천','백','십','만','천','백','십','');
	var NUM     = new Array('','일','이','삼','사','오','육','칠','팔','구');

	if (ad_num < 0) {
		ad_num = - ad_num;
		ls_return = '-';
	}

	ls_num = _X.Trim(String(Math.round(ad_num)));

	if (ls_num=="") return "";

	li_len = ls_num.length;
	if (li_len>16) {
		_X.MsgBox("금액범위 초과",'[' + ls_num +"]<BR>금액이 너무 큽니다. 확인 바랍니다." );
		return ls_num;
	}

	for (i=0;i<li_len;i++) ls_str[i] = ls_num.substr(i,1);

	for (i=0;i<li_len;i++) {
		if (ls_str[i]=="0")
			ls_str[i] = "";
		else
			ls_str[i] = NUM[_X.ToInt(ls_str[i])];
	}

	for (i=0;i<li_len;i++) {
		li_mod = 16 - li_len + i;
		if (li_mod==16 || li_mod==12 || li_mod==8 || li_mod==4 || li_mod==0) {
			 if ((ls_str[i]) != "" || li_mod==3 || li_mod==7 || li_mod==11 || li_mod==15) {
				 ls_return += ls_str[i] + ls_UNIT[li_mod];
		    }
			 ls_temp = "";
		}
		 else {
			ls_temp += ls_str[i];
			if (ls_str[i]!="" || li_mod==3 || li_mod==7 || li_mod==11 || li_mod==15) {
		 	 	ls_return += ls_str[i] + ls_UNIT[li_mod];
		 	}
		}
	}

	return ls_return;
};

/**
 * fn_name :  _X.Divide
 * Description : 나누기 값을 반환한다.
 * param :  ad_amt1:피젯수, ad_amt2:젯수
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.Divide = function (ad_amt1, ad_amt2) {
	if (ad_amt2==null || ad_amt2==0)
		return 0;
	else
		return ad_amt1 / ad_amt2;
};

/**
 * fn_name :  _X.PointCheck
 * Description : 숫자길이와 소숫점길이 체크
 * param :  a_value:값, a_len:숫자길이, a_po:소숫점길이
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2015.01.27  	박종현          최초 생성
*/

_X.PointCheck = function(a_value, a_len, a_po){

  if(typeof(a_value) == "number"){
  	a_value = _X.Trim(String(a_value));
  }else{
  	a_value = _X.Trim(a_value);
  }

  /*2015.02.04 이상규
  	문제발생시점: -값 입력 시
  	문제내용: 문자열로 체크하므로 (-) 부호도 길이체크에 적용이 되어
  						a_len - 1 로 체크한 결과가 나옴.
  	해결방법: (-) 부호 확인 하여 제거한 후 체크 하도록 함.
  */
  if(a_value.substring(0, 1) == "-") a_value = a_value.substring(1);

	if(a_po == null) a_po = 0;

	if(a_po == 0){

		if(a_value.indexOf('.') != -1){
			_X.MsgBox('확인','소숫점은 입력할 수 없습니다.');
			return false;
		}else{
			if(a_len < a_value.length){
				_X.MsgBox('확인','숫자열 길이가 '+ a_len+' 보다 같거나 작아야 합니다.');
				return false;
			}
		}
	}else{
		if(a_value.indexOf('.') == -1){
			var num = a_value;
		  var numPoint = "";
		}else{
			var num = a_value.substring(0, a_value.indexOf('.'));
		  var numPoint = a_value.substring(a_value.indexOf('.')+1, a_value.length);
		}

	  if(a_len < num.length){
			_X.MsgBox('확인','숫자열 길이가 '+ a_len+' 보다 같거나 작아야 합니다.');
			return false;
		}
		if(a_po < numPoint.length){
			_X.MsgBox('확인','소숫점 길이가 '+ a_po+' 보다 같거나 작아야 합니다.');
			return false;
		}
	}

	return true;
}

/**
 * fn_name :  _X.Calendar
 * Description : 달력호출(popup)
 * param :  a_curdate:선택일자,a_timetag:time 유무,a_format:일자format
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.Calendar = function (a_curdate,a_timetag,a_format)
{
	if(a_timetag==null) {a_timetag = false;}
	if(a_format==null) {a_format = "yyyymmdd";}
	if(a_curdate==null||a_curdate=="") a_curdate = _X.ToString(_X.GetSysDate(),'yyyymmdd');
	var l_curdate = _X.StrPurify(_X.Trim((typeof(a_curdate)=="string"?a_curdate:a_curdate.value)));
	if(l_curdate==null||l_curdate=="") l_curdate = _X.ToString(_X.GetSysDate(),'yyyymmdd');
	if(!_X.ChkDate(l_curdate,"선택된 날짜")) return ;


	var ls_url = "/APPS/com/jsp/calendar.jsp"
				+ "?year=" + l_curdate.substr(0,4)
				+ "&month=" + l_curdate.substr(4,2)
				+ "&day=" + l_curdate.substr(6,2)
				+ (!a_timetag ? "" : (l_curdate.length>8? "&hour=" + l_curdate.substr(9,2) + "&minute=" + l_curdate.substr(11,2) : "&hour=00&minute=00"));

	var ls_rtvalue = _X.OpenModal(ls_url, 260, 260);

	if(ls_rtvalue!=null&&typeof(ls_rtvalue)=="string"&&typeof(a_curdate)=="object"){
		if(!_X.ChkDate(ls_rtvalue)) return;
		ls_rtvalue = _X.ToString(ls_rtvalue, a_format);
		if(a_curdate.value!=ls_rtvalue){
			a_curdate.value=ls_rtvalue;
			$("#"+a_curdate.id).context.parentWindow.xe_EditChanged(a_curdate, a_curdate.value);
		}
		$("#"+a_curdate.id).context.parentWindow.focus();
		a_curdate.focus();
		return;
	}else{
		if(ls_rtvalue==null||!_X.ChkDate(ls_rtvalue))
			if(typeof(a_curdate)=="object")
				return;
			else
				return ls_rtvalue;
		else
			return ls_rtvalue;
	}
};

/**
 * fn_name :  _X.ToString
 * Description :  Date변수를 String변수로 변환
 * param :  a_date: any object, a_format:일자포맷
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.ToString = function (a_date, a_format) {
	if (a_date==null)	return null;
	if (typeof(a_date)=="number") return String(a_date);
	if (a_format==null) a_format = 'yyyymmdd';
	if (typeof(a_date)=="date") a_date=_X.dateToDate(a_date, a_format);
	if (typeof(a_date)=="string" && a_format == 'yyyy-mm' ) a_date=_X.ToDate( String(a_date) + '01');
	if (typeof(a_date)=="string" && a_format != 'yyyy-mm') a_date=_X.ToDate(a_date);
	if(a_date==null) return null;
	//var ls_yyyy	= "" + (a_date.getYear() + (a_date.getYear() < 1000 ? 1900 : 0));
  var ls_yyyy = "" + (a_date.getYear() + (a_date.getYear() < 1100 ? 1900 : 0));

	var ls_yy   = ls_yyyy.substr(2,2);
	var ls_mm	= ((a_date.getMonth()+1)<10 ? "0" : "") + (a_date.getMonth()+1);
	var ls_dd	= (a_date.getDate()<10 ? "0" : "") + a_date.getDate();
	var ls_hh	= (a_date.getHours()<10 ? "0" : "") + a_date.getHours();
	var ls_mi	= (a_date.getMinutes()<10 ? "0" : "") + a_date.getMinutes();
	var ls_ss	= (a_date.getSeconds()<10 ? "0" : "") + a_date.getSeconds();
	return a_format.toLowerCase().replace("yyyy",ls_yyyy).replace("yy",ls_yy).replace("mm",ls_mm).replace("dd",ls_dd).replace("hh",ls_hh).replace("mi",ls_mi).replace("ss",ls_ss);
};
_X.toString = _X.ToDate;

/**
 * fn_name :  _X.ToString
 * Description : YYYYMMDD 형식의 String을 Date Object로 변환
 * param :  dateStr: 날자형문자열
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.ToDate = function (dateStr) {
	if (dateStr==null) return null;
	var	li_yyyy, li_mm, li_dd, li_hh=0, li_mi=0, li_ss=0;

	if(dateStr.length==8) {
		li_yyyy = _X.ToInt(dateStr.substr(0,4));
		li_mm	= _X.ToInt(dateStr.substr(4,2));
		li_dd	= _X.ToInt(dateStr.substr(6,2));
	} else if(dateStr.length==10) {
		li_yyyy = _X.ToInt(dateStr.substr(0,4));
		li_mm	= _X.ToInt(dateStr.substr(5,2));
		li_dd	= _X.ToInt(dateStr.substr(8,2));
	} else if(dateStr.length>10) {
		li_yyyy = _X.ToInt(dateStr.substr(0,4));
		li_mm	= _X.ToInt(dateStr.substr(5,2));
		li_dd	= _X.ToInt(dateStr.substr(8,2));
		li_hh	= _X.ToInt(dateStr.substr(11,2));
		li_mi	= _X.ToInt(dateStr.substr(14,2));
		li_ss	= _X.ToInt(dateStr.substr(17,2));
	} else {
		return null;
	}

	return (new Date(li_yyyy, li_mm -1, li_dd, li_hh, li_mi, li_ss));
};
_X.toDate = _X.ToDate;

/**
 * fn_name :  _X.ChkDate
 * Description : date 유효성 검사
 * param :  as_date:날짜형문자열, as_msg:오류메시지 보기유무
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.ChkDate = function (as_date, as_msg) {
	if(as_date==null||_X.Trim(as_date)=="") return true;
	as_date = _X.StrPurify(as_date);
	var lb_result=true;
	var ls_lastdate = as_date;

	if(as_date.length==10) as_date=_X.ToString(as_date,'yyyymmdd');
	if (as_date.length!=8) lb_result=false;
	else if (_X.ToInt(as_date.substr(0,4))<1 || _X.ToInt(as_date.substr(0,4)) > 9999) 	lb_result=false;
	else if (_X.ToInt(as_date.substr(4,2))<1 || _X.ToInt(as_date.substr(4,2)) > 12) 	lb_result=false;
	else if (_X.ToInt(as_date.substr(6,2))<1 || _X.ToInt(as_date.substr(6,2)) > 31) 	lb_result=false;
	else {
		ls_lastdate = _X.LastDate(as_date.substr(0,6)+'01');
		if (_X.ToInt(as_date.substr(6,2))>_X.ToInt(ls_lastdate.substr(6,2))) lb_result=false;
	}
	if (!lb_result && as_msg!=null) _X.MsgBox("날짜 유효성 체크", "[" + as_msg + "](" + as_date + ")가 유효하지 않습니다.");
	return lb_result;
};

/**
 * fn_name :  _X.RelativeDate
 * Description : 지정일자부터 n일자 이후의 날짜를 구한다.
 * param :  a_date:날짜, n:일수
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.RelativeDate = function (a_date, n) {
	var newdate =(typeof(a_date)=="string" ? _X.ToDate(a_date): _X.NewDate(a_date));
	newdate.setDate(newdate.getDate() + n);

	return (typeof(a_date)=="string" ? _X.ToString(newdate): newdate);
};

/**
 * fn_name :  _X.RelativeMonth
 * Description : 지정일자부터 n월 이후의 날짜를 구한다.
 * param :  a_date:날짜, n:일수
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.RelativeMonth = function (a_date, n) {
	var newdate =(typeof(a_date)=="string" ? _X.ToDate(a_date): _X.NewDate(a_date));
	newdate.setMonth(newdate.getMonth() + n);

	return (typeof(a_date)=="string" ? _X.ToString(newdate): newdate);
};

/**
 * fn_name :  _X.NewDate
 * Description : date object 반환
 * param :  a_date:날짜, n:일수
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.NewDate = function (a_date) {
	var newDate = new Date(a_date.getFullYear(), a_date.getMonth(), a_date.getDate(), a_date.getHours(), a_date.getMinutes(), a_date.getSeconds());
	return newDate;
};

/**
 * fn_name :  _X.PreviousDate
 * Description : 지정일자의 이전일자를 구한다.
 * param :  a_date:날짜
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.PreviousDate = function (a_date) {
	return _X.RelativeDate(a_date,-1);
};
_X.PrevDate = _X.PreviousDate;

/**
 * fn_name :  _X.MovePreDate
 * Description : 일자이동(이전)
 * param :  a_obj:날짜
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.MovePreDate = function (a_obj) {
	ls_value = _X.StrReplace(a_obj.value,"-");
	if (ls_value.length==6) {
		 //var ld_date = Date(a_obj.value + '01');
		 var ld_date = ls_value + '01';
		 if(a_obj.value.indexOf("-")==-1)
			 a_obj.value = _X.ToString(_X.PreviousDate(ld_date, 20),'yyyymmdd').substr(0,6);
		 else a_obj.value = _X.ToString(_X.PreviousDate(ld_date, 20),'yyyy-mm-dd').substr(0,7);
	}
	else if (ls_value.length==8) {
		 //var ld_date = Date(a_obj.value);
		 var ld_date = ls_value;
		 if(a_obj.value.indexOf("-")==-1)
			 a_obj.value = _X.ToString(_X.PreviousDate(ld_date, 1),'yyyymmdd');
		 else a_obj.value = _X.ToString(_X.PreviousDate(ld_date, 1),'yyyy-mm-dd');
	}
	else if (ls_value.length==4) {
		 a_obj.value = Number(ls_value) - 1;
	}
	//수정(2015.07.15 KYY)
	if(typeof(xe_EditChanged) != "undefined")
		xe_EditChanged(a_obj, a_obj.value)
	else
		parent.$("#"+a_obj.id).trigger(xe_EditChanged(a_obj, a_obj.value));
};

/**
 * fn_name :  _X.MovePreDate
 * Description : 일자이동(이후)
 * param :  a_obj:날짜
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.MoveNextDate = function (a_obj) {
	ls_value = _X.StrReplace(a_obj.value,"-");
	if (ls_value.length==6) {
		//var ld_date = Data(a_obj.value + '15');
		var ld_date = ls_value + '15';
	    if(a_obj.value.indexOf("-")==-1)
	    	a_obj.value = _X.ToString(_X.RelativeDate(ld_date, 20),'yyyymmdd').substr(0,6);
	    else a_obj.value = _X.ToString(_X.RelativeDate(ld_date, 20),'yyyy-mm-dd').substr(0,7);
	}
	else if (ls_value.length==8) {
		//var ld_date = Date(a_obj.value);
		var ld_date = ls_value;
	    if(a_obj.value.indexOf("-")==-1)
	    	a_obj.value = _X.ToString(_X.RelativeDate(ld_date, 1),'yyyymmdd');
	    else a_obj.value = _X.ToString(_X.RelativeDate(ld_date, 1),'yyyy-mm-dd');
	}
	else if (ls_value.length==4) {
		a_obj.value = Number(a_obj.value) + 1;
	}
  // 크롬에서 작동을 안해서 변경 - 박종현
	//수정(2015.07.15 KYY)
	if(typeof(xe_EditChanged) != "undefined")
		xe_EditChanged(a_obj, a_obj.value)
	else
	  parent.$("#"+a_obj.id).trigger(xe_EditChanged(a_obj, a_obj.value));
};

/**
 * fn_name :  _X.NextDate
 * Description : 지정일자의 다음일자를 구한다.
 * param :  a_date:날짜
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.NextDate = function (a_date) {
	return _X.RelativeDate(a_date,1);
};

/**
 * fn_name :  _X.DaysAfter
 * Description : 날짜사이의 간격을 얻는다.
 * param :  a_date1:시작날짜,a_date2:종료날짜
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.DaysAfter = function (a_date1, a_date2) {
	var newdate1 = (typeof(a_date1)=="string" ? _X.ToDate(a_date1) : a_date1);
	var newdate2 = (typeof(a_date2)=="string" ? _X.ToDate(a_date2) : a_date2);

	return parseInt((newdate2 - newdate1) / 86400000, 10);
};

/**
 * fn_name :  _X.LastDate
 * Description : 지정일자가 속한달의 마지막일자를 구한다.
 * param :  as_date:날짜형문자열, as_msg:오류메시지 보기유무
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.LastDate = function (a_date) {
	var ls_date = (typeof(a_date)=="string" ? a_date : _X.ToString(a_date));
  var ls_yyyy = ls_date.substr(0,4);
  var ls_mm   = ls_date.substr(4,2);
	var ls_dd;

  var li_yyyy = _X.ToInt(ls_yyyy);
  var li_mm   = _X.ToInt(ls_mm);

  switch (li_mm) {
        case 2:
        	  ls_dd=(_X.IsLeap(li_yyyy) ? '29' : '28');
	        break;
        case 4:
        case 6:
        case 9:
        case 11: ls_dd='30'; break;
        default: ls_dd='31';
    }
	return (typeof(a_date)=="string" ? ls_yyyy + ls_mm + ls_dd : _X.ToDate(ls_yyyy + ls_mm + ls_dd));
};

/**
 * fn_name :  _X.IsLeap
 * Description : 윤년체크
 * param :  as_date:날짜형문자열, as_msg:오류메시지 보기유무
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.IsLeap = function (year) {
    return ((year%4 == 0 && year%100 != 0) || year%400 == 0);
};


/**
 * fn_name :  _X.GetSysDate
 * Description : 서버의 현재일자를 반환한다.
 * param :
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.GetSysDate = function () {
	var ldt_sysdate;
	//수정(2016.12.26 KYY)
	if(typeof(mytop._DBMS)!="undefined" && (mytop._DBMS == "Oracle" || mytop._DBMS == "ORA")){
		ldt_sysdate = _X.XmlSelect("com", "Comm", "SysDate_Ora", new Array('%'), "array");
	} else if(typeof(mytop._DBMS)!="undefined" && mytop._DBMS == "MSS"){
		ldt_sysdate = _X.XmlSelect("com", "Comm", "SysDate_Mss", new Array('%'), "array");
	}
	return ldt_sysdate[0][0];
};

/**
 * fn_name :  _X.ToYYYYMM
 * Description : yyyymm 포맷으로 반환
 * param :  al_yyyymm: 문자열
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.ToYYYYMM = function (al_yyyymm) {
	var	li_mm, li_yyyy;

	li_yyyy = _X.ToInt(String(al_yyyymm).substr(0,4));
	li_mm   = _X.ToInt(String(al_yyyymm).substr(4,2));

	li_yyyy += Math.floor(li_mm / 12.01);
	li_mm   = (li_mm % 12);
	if (li_mm==0) li_mm = 12;

	return String(li_yyyy) + _X.LPad(li_mm,2,'0');
};

/**
 * fn_name :  _X.SecondAfter
 * Description : 일자간의 일수
 * param :  StartDate:시작일자, curDate:현재일자
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.SecondAfter = function (StartDate, curDate) {
	var secondAfter = (curDate.getHours() * 3600 + curDate.getMinutes() * 60 + curDate.getSeconds()) - (StartDate.getHours()*3600 + StartDate.getMinutes()*60 + StartDate.getSeconds());
	var milliAfter  = curDate.getMilliseconds() - StartDate.getMilliseconds();
	if(milliAfter < 0)
	{
		secondAfter--;
		milliAfter+=10000;
	}
	return secondAfter + "." + _X.LPad(milliAfter,4,'0');
};

/**
 * fn_name :  _X.DayName
 * Description : 선택일자의 요일값 반환
 * param :  dateStr:일자문자열
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.DayName = function (dateStr) {
	var dayIndex = (typeof(dateStr)=="string"?_X.ToDate(dateStr).getDay():dateStr.getDay());
	switch(dayIndex){
		case 0: return '일';
		case 1: return '월';
		case 2: return '화';
		case 3: return '수';
		case 4: return '목';
		case 5: return '금';
		case 6: return '토';
	}
	return '';
};

/**
 * fn_name :  _X.KoreanTime
 * Description : 선택일자의 요일값 반환
 * param :  a_time:date object
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.KoreanTime = function (a_time){
	return a_time.getHours() + ' 시 ' + a_time.getMinutes() + ' 분 ' + a_time.getSeconds() + ' 초 ';
};
_X.KorTime = _X.KoreanTime;

//Select Element의 text읽어 오기
_X.SelectGetFullText = function (a_obj) {
	return a_obj.options[a_obj.selectedIndex].text;
};

/**
 * fn_name :  _X.SelectGetText
 * Description : 선택된 select text 값 반환
 * param :  a_obj:select object
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.SelectGetText = function (a_obj) {
	var ls_fulltext = _X.SelectGetFullText(a_obj);
	var pos1 = ls_fulltext.indexOf('[');
	var pos2 = ls_fulltext.indexOf(']');
	if(pos1>=0&&pos2>pos1)
		return _X.Trim(ls_fulltext.substr(pos2+1,ls_fulltext.length-pos2));
	else
		return ls_fulltext;
};

/**
 * fn_name :  _X.SelectGetText
 * Description : NAME값으로 라디오 체크된 값 가져오기 (체크된 값이 없을때 null)
 * param :  a_elename:radio object
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.RadioGetValue = function(a_elename){
	var ls_radioEles = document.getElementsByName(a_elename);
	for(var i = 0; i < ls_radioEles.length; i++) {
		if(ls_radioEles[i].checked == true) return ls_radioEles[i].value;
	}
	return null;
};

/**
 * fn_name :  _X.IsValid
 * Description : a_obj is valid check
 * param :  a_obj:object
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.IsValid = function (a_obj){
	if(typeof(a_obj)=="unknown"||typeof(a_obj)=="undefined"||a_obj==null){
		return false;
	}else{
		return true;
	}
};
_X.isValid = _X.IsValid;

/**
 * fn_name :  _X.IsTrue
 * Description : a_obj is true
 * param :  a_obj:object
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.IsTrue = function (a_obj){
	if(_X.IsValid(a_obj)&&a_obj){
		return true;
	}else{
		return false;
	}
};
_X.isTrue = _X.IsTrue;

_X.IsNull = function(val1, val2){
	if(val1==null)
		return val2;
	else
		return val1;
};

/**
 * fn_name :  _X.FileUpload
 * Description : 이미지 UPLOAD
 * param :  a_id:저장이름, a_subdir:단위시스템
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
_X.ImageUpload = function(a_obj, a_id, a_subdir, a_fileid ) {
	var sUrl = mytop._CPATH+"/mighty/imageupload.do?obj="+a_obj+"&id="+a_id+"&subdir=FM";
	var modal = "<div id='uploadmodal'><iframe id='if_uploadmodal' src='" + sUrl + "' class='iframe_popup' frameborder=0 marginwidh=0 marginheight=0></iframe></div>";
	$(modal).dialog({
	title:"이미지 올리기",
	show:false,
	width:450,
	height:460,
	modal:true,
	resizable:false,
	buttons: {
		"업로드": function() { $("#if_uploadmodal").get(0).contentWindow.pf_submit(); },
		"취소": function() { $("#if_uploadmodal").get(0).contentWindow.pf_cancel(); }
	},
	close:function(){try { $(this).dialog('destroy');} catch(e) {}}
	});
};

/**
 * fn_name :  _X.FileUpload
 * Description : file upload popup 호출
 * param :  limitFiles:첨부파일갯수, subsystem:단위시스템
 * 			obj : upload 구분, atchFileId: file id(변경할때)
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
*/
//fileReadOnly 추가 (2015.06.22 이상규)
_X.FileUpload = function (limitFiles, subsystem, obj, atchFileId, fileReadOnly) {
	var sUrl = mytop._CPATH+"/mighty/fileupload.do?upcnt="+limitFiles+"&subdir="+subsystem+"&obj="+obj+"&fileid="+atchFileId;

  var _UPLOAD_BTN_PARAM;

	if(fileReadOnly) {
		window.fileReadOnly = true;
    _UPLOAD_BTN_PARAM = [
      {text: "취소", class : 'btn btn-default btn-sm', click: function() { $("#if_uploadmodal").get(0).contentWindow.pf_cancel(); }, autofocus: false}
    ];
	} else {
		window.fileReadOnly = false;

    _UPLOAD_BTN_PARAM = [
      {text: "업로드", class : 'btn btn-success', click: function() { $("#if_uploadmodal").get(0).contentWindow.pf_submit();}, autofocus: false},
      {text: "취소", class : 'btn btn-default', click: function() { $("#if_uploadmodal").get(0).contentWindow.pf_cancel(); }, autofocus: false}
    ];

	}

	var modal = "<div id='uploadmodal'><iframe id='if_uploadmodal' src='" + sUrl + "' class='iframe_popup' frameborder=0 marginwidth=0 marginheight=0></iframe></div>";
	$(modal).dialog({
  	title:"파일 올리기",
  	show:false,
  	width:540,
  	height:380,
  	modal:true,
  	resizable:false,
  	buttons: _UPLOAD_BTN_PARAM,
  	close:function(){
  					try {
  						x_FileUploaded(obj,null);  // BM020101 Reload 문제로 인해 추가 (2015.02.11 이요한)
  						$(this).dialog("destroy").remove();
  					}
  					catch(e) {}
  					}
	});
};

/*
* fn_name :  _X.FileDownload
* Description : file download
* param :  atchFileId:파일아이디, fileSn:첨부파일seq
* Statements :
*
*   since   		 author           Description
*  ------------    --------    ---------------------------
*   2013.02.10  	김양열          최초 생성
*/
_X.FileDownload = function (atchFileId, fileSn) {
	_X.AjaxDownFile(mytop._CPATH+'/file/download.do',{'atchFileId': atchFileId, 'fileSn': fileSn});
};

/*
* fn_name     : _X.FileDownload2
* Description : file download
* param       : filePath: 파일경로, fileName: 파일명
* Statements  :
*
*   since   		 author           Description
*  ------------    --------    ---------------------------
*   2017.05.15  	  이상규        최초 생성
*/
_X.FileDownload2 = function (filePath, fileName) {
	if ( !fileName ) {
		fileName = filePath.substr(filePath.lastIndexOf("/") + 1);
	}
	
	_X.AjaxDownFile("/Mighty/jsp/FileDownLoad.jsp", "filePath=" + filePath + "&fileName=" + fileName);
};


_X.FUE = function (obj,uinfo) {
	eval("var infoObj=" + _X.SD(uinfo));
	x_FileUploaded(obj, infoObj);
	$("#uploadmodal").dialog("close");
  $("#uploadmodal").remove();
}

/*
* fn_name :  _X.FileDelete
* Description : file delete
* param :  atchFileId:파일아이디, fileSn:첨부파일seq
* Statements :
*
*   since   		 author           Description
*  ------------    --------    ---------------------------
*   2013.02.10  	김양열          최초 생성
*/
_X.FileDelete = function (atchFileId, fileSn, callback) {
	_X.AjaxDeleteFile('/file/delete.do', {'atchFileId': atchFileId, 'fileSn': fileSn}, callback || x_FileDeleted);
};

_X.StrReverse = function (s){
	return s.split("").reverse().join("");
}


/* Cross-browser SWF removal
- Especially needed to safely and completely remove a SWF in Internet Explorer
*/
_X.removeSWF = function removeSWF(obj) {
	//var obj = getElementById(id);
	if (obj && obj.nodeName == "OBJECT") {
		if (_IE) {
	    obj.style.display = "none";
	    (function () {
	        if (obj.readyState == 4) {
						_X.removeObject(obj);
	        }
	        else {
						setTimeout(arguments.callee, 10);
	        }
	    })();
		} else {
			obj.parentNode.removeChild(obj);
		}
	}
}
_X.RemoveSWF = _X.removeSWF;

_X.removeObject = function(obj) {
	if (obj) {
		a = obj.childNodes;
		if (a) {
		    l = a.length;
		    for (var i = l -1; i >=0; i--) {
		        _X.removeObject(a[i]);
		    }
		}
		for (var i in obj) {
			if (typeof obj[i] == "function") {
				obj[i] = null;
			}
			i = null;
		}
		if(obj.parentNode!=null) {
			obj.parentNode.removeChild(obj);
			//obj = null;
		}
	}
}
_X.RemoveObject = _X.removeObject;

/**
 * fn_name :  _X.purge
 * Description : tab,menu object 내용 clear
 * param : d : tab,menu object 내용 clear
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	김양열          최초 생성
 */
_X.purge = _X.removeObject;

_X.IsAdmin = function () {
	if(typeof(top._UserID)!="undefined" && (top._UserID.toUpperCase()=="ADMIN" || top._UserID.toUpperCase()=="XINTER"))
		return true;
	if(typeof(mytop)!="undefined" && typeof(mytop._UserID)!="undefined" && (mytop._UserID.toUpperCase()=="ADMIN" || mytop._UserID.toUpperCase()=="XINTER"))
		return true;

	return false;
}

_X.IsHtml = function (src) {
	var htmlRegex = new RegExp("<([A-Za-z][A-Za-z0-9]*)\b[^>]*>(.*?)</\1>");
	return htmlRegex.test(src);
}

_X.SendSMS = function (tran_phone, tran_callback, tran_msg) {
	//(subsystem, xmlFile, xmlKey, params, returnType, colSepa)
	var rtValue = _X.ES("sms","SendSMS","SEND_SMS",new Array(tran_phone, tran_callback, tran_msg));
	return (rtValue=="1" ? "OK" : rtValue);
}

_X.SendMMS = function (tran_phone, tran_callback, tran_msg) {
	//(subsystem, xmlFile, xmlKey, params, returnType, colSepa)
	var rtValue = _X.ES("sms","SendSMS","SEND_MMS",new Array(tran_phone, tran_callback, tran_msg));
	return (rtValue=="1" ? "OK" : rtValue);
}

/**
 * fn_name :  _X.TextAreaEnterE
 * Description : textarea enter 값 변환
 * param : a_value,a_tag
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2015.05.01  	박종현          최초 생성
 */

_X.TextAreaEnterE = function (a_value, a_tag) {
 	if(a_tag==null || a_tag=='') a_tag = '<br>';
 	if(a_value == null || a_value == "") return a_value;
	var Regex = new RegExp('\r\n','g');
	a_value = a_value.replace(Regex, a_tag);
	return a_value.replace(new RegExp('\n','g'), a_tag);
}

/**
 * fn_name :  _X.TextAreaEnterD
 * Description : 디비값 변환 textarea 적용 -
 * 기존 데이터 변환된값도 변경  %0A => enter, %20 => space

 * param : a_value
 * Statements :
 *
 *   since   		 author           Description
 *  ------------    --------    ---------------------------
 *   2015.05.01  	박종현          최초 생성
 */

_X.TextAreaEnterD = function (a_value) {
 	if(a_value == null || a_value == "") return a_value;
	return a_value.replace(/&nbsp;/g, ' ').replace(/<br.*?>/g, '\n').replace(/%0A/g, '\n').replace(/%20/g, ' ');
}

//span 에서 사용하기 위해 추가(2015.05.31 이상규)
_X.TextAreaEnterDS = function (a_value) {
 	if(a_value == null || a_value == "") return a_value;
	return a_value.replace(/&nbsp;/g, ' ').replace(/%0A/g, '<br>').replace(/%20/g, ' ');
}

/*(2015.03.05 이상규)
* fn_name     : _X.ArrayContains
* Description : 배열에 같은 값이 있는지 확인
* param       : a_ele -> 비교할 값,
*               a_arr -> 비교 대상이 되는 배열
* return      : isEle -> 같은 값이 있으면 true 없으면 false,
*               arr   -> 같은 값이 있을때 값을 가지고 있는 배열,
*               index -> 값을 가지고 있는 배열의 index
*/
_X.ArrayContains = function(a_ele, a_arr) {
	function contains(a_element, a_array, a_index) {
		if(!a_index) a_index = [];
		var result = {isEle: false};

		for(var i = 0; i < a_array.length; i++) {
			if(a_array[i] instanceof Array) {
				a_index.push(i);
				result = contains(a_element, a_array[i], a_index);
				if(result.isEle) {
					return result;
				}
			} else if(a_element == a_array[i]) {
				return {isEle: true, arr: a_array, index: i};
			}
		}
		a_index.pop();
		return result;
	}
	return a_arr instanceof Array ? contains(a_ele, a_arr) : null;
}

_X.MakeExcel = function (a_param) {
	//alert(JSON.stringify(a_param));
	_X.Block(false, true);
	var form = "<form action='/mighty/makeExcel.do' method='post'>"
					 + "<input type='hidden' name='params' value='" + JSON.stringify(a_param) +"' />"
					 + "</form>"
	//$(form).appendTo("body").submit(function(event) {_X.UnBlock();}).remove(); 
	$(form).appendTo("body").submit().remove();
	setTimeout("_X.UnBlock(false, true)", 3000);
}

//주민번호 체크(2016.06.09 이상규)
_X.CheckJumin = function(jumin) { 
	jumin = jumin.replace(/-/gi,'');
	var fmt = /^\d{6}[1234]\d{6}$/;

	if(!fmt.test(jumin))
		return false;

	var birthYear = (jumin.charAt(6) <= "2") ? "19" : "20";
	birthYear += jumin.substr(0, 2);
	var birthMonth = jumin.substr(2, 2) - 1;
	var birthDate = jumin.substr(4, 2);
	var birth = new Date(birthYear, birthMonth, birthDate);

	if(birth.getYear() % 100 != jumin.substr(0, 2) || birth.getMonth() != birthMonth || birth.getDate() != birthDate)
		return false;

	var buf = new Array(13);

	for(var i = 0; i < 13; i++) 
		buf[i] = parseInt(jumin.charAt(i));

	multipliers = [2,3,4,5,6,7,8,9,2,3,4,5];

	for(var sum = 0, i = 0; i < 12; i++) 
		sum += (buf[i] *= multipliers[i]);

	if((11 - (sum % 11)) % 10 != buf[12])
		return false;

	return true;
}

//주민번호 체크(2017.06.26 박현석)
_X.CheckJumin = function (jumin) {
 var strJumin = jumin.replace(/-/gi,'');
 var checkBit = new Array(2,3,4,5,6,7,8,9,2,3,4,5);
 var num7  = strJumin.charAt(6);
 var num13 = strJumin.charAt(12);
 var total = 0;
 if (strJumin.length == 13 ) {
  for (i=0; i<checkBit.length; i++) {
    total += strJumin.charAt(i)*checkBit[i];
  }
  // 외국인 구분 체크
  if (num7 == 0 || num7 == 9) { // 내국인 ( 1800년대 9: 남자, 0:여자)
   total = (11-(total%11)) % 10;
  }
  else if (num7 > 4) {  // 외국인 ( 1900년대 5:남자 6:여자  2000년대 7:남자, 8:여자)
   total = (13-(total%11)) % 10;
  }
  else { // 내국인 ( 1900년대 1:남자 2:여자  2000년대 3:남자, 4:여자)
   total = (11-(total%11)) % 10;
  }

  if(total != num13) {
   return false;
  }
  return true;
 } else{
  return false;
 }
}

//사업자번호 체크(2016.06.09 이상규)
_X.CheckBiz = function(bizID) {
	bizID = bizID.replace(/-/gi,'');
	var checkID = new Array(1, 3, 7, 1, 3, 7, 1, 3, 5, 1);
	var tmpBizID, i, chkSum=0, c2, remander;

	for(i=0; i<=7; i++) 
		chkSum += checkID[i] * bizID.charAt(i);

	c2 = "0" + (checkID[8] * bizID.charAt(8));
	c2 = c2.substring(c2.length - 2, c2.length);
	chkSum += Math.floor(c2.charAt(0)) + Math.floor(c2.charAt(1));
	remander = (10 - (chkSum % 10)) % 10 ;

	if(Math.floor(bizID.charAt(9)) == remander) 
		return true;

	return false;
}

_X.Noty = function (message, title, alertType, delay) {
	if(title==null) {title="알림"}
	if(alertType==null) {alertType="info";}
  if(delay==null) {delay=200;}
  mytop.$.notify({
    // options
    icon: 'glyphicon glyphicon-warning-sign',
    title: title,
    message: message,
    //url: 'https://github.com/mouse0270/bootstrap-notify',
    url: '',
    target: '_blank'
  },{
    // settings
    element: 'body',
    position: null,
    type: alertType,
    allow_dismiss: true,
    newest_on_top: false,
    showProgressbar: false,
    placement: {
      from: "top",
      align: "center"
    },
    offset: 300,
    spacing: 10,
    z_index: 9999,
    delay: delay,
    timer: 10,
    url_target: '_blank',
    mouse_over: null,
    animate: {
      enter: 'animated fadeInDown',
      exit: 'animated fadeOutUp'
    },
    onShow: null,
    onShown: null,
    onClose: null,
    onClosed: null,
    icon_type: 'class',
    template: '<div data-notify="container" class="col-xs-11 col-sm-2 alert alert-{0}" role="alert">' +
      '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
      '<span data-notify="icon"></span> ' +
      '<span data-notify="title">{1}<br><br></span> ' +
      '<span data-notify="message"><b>{2}</b></span>' +
      '<div class="progress" data-notify="progressbar">' +
        '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
      '</div>' +
      '<a href="{3}" target="{4}" data-notify="url"></a>' +
    '</div>' 
  });
}  

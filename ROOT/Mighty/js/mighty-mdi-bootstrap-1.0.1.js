/**
 * Object :  mighty-mdi-bootstrap-1.0.1.js
 * @Description : Mighty-X MDI 관련 javascript libaray - override
 * @author :엑스인터넷정보 김양열
 * @since : 2013.02.10
 * @version : 1.0.0
 *
 * @Modification Information
 * <pre>
 *   since        author           Description
 *  ----------    --------    ---------------------------
 *   2013.02.10    김양열        최초생성
 */

/**
 * fn_name :  _X.MDI_Open
 * Description : 새로운 MDI화면을 연다.
 * param :a_pgmname : 메뉴코드 , a_pgmname : 프로그램명, a_url : 경로
 * Statements :
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
 */

_X.MDI_Open = function(a_pgmcode, a_pgmname, a_url) {
  if(_MDI_CNT >= _MAX_MDI_CNT) {
    _X.MsgBox('창의 갯수가 ' + _MAX_MDI_CNT + '개로 제한되어 있습니다.');
    return;
  }
  a_url = mytop._CPATH + a_url;
  //이미 있으면
  var isIndex = _X.MDI_IsMenuIndex(a_url);
  if(isIndex>=0) {
    _X.MDI_Select(isIndex,false);
    return;
  }
  var mdiIdx = _X.MDI_NewIndex();
  //IFRAME 생성
  var ndiv = document.createElement('div');
  ndiv.id = "x-mdi-"+mdiIdx;
  ndiv.className = "tab-pane x-mdi-wrap wrap-"+a_pgmcode.substring(0, 2)+" wrap-"+a_pgmcode;
  ndiv.setAttribute("role", "tabpanel");
  $(ndiv).html('<iframe id="mdi_' + mdiIdx + '" src="' + a_url + '" scrolling="auto" align="top" frameborder="0" />');
  //document.body.appendChild(ndiv);
  $("#main-mdi-content").append(ndiv);
  _MDI_FRAME[mdiIdx] = ndiv;
  //MDI 탭 생성
  var ndivtab = document.createElement('li');
  ndivtab.setAttribute("role", "presentation");
  ndivtab.tabIndex = mdiIdx;
  ndivtab.pgmcode = a_pgmcode;
  ndivtab.pgmname = a_pgmname;
  ndivtab.premenuindex = _CurMenuIndex;
  //ndivtab.setAttribute("pgmCode", a_pgmcode);
  //ndivtab.setAttribute("pgmName", a_pgmname);
  //ndivtab.setAttribute("preMenuIndex", _CurMenuIndex);
  $(ndivtab).html('<a href="#x-mdi-' + mdiIdx + '" aria-controls="x-mdi-' + mdiIdx + '" role="tab" pgm-code="'+a_pgmcode+'" data-toggle="tab" title="'+"["+a_pgmcode+"] "+a_pgmname+'">' + a_pgmname + ' <button class="btn btn-xs btn-mdiclose"><i class="fa fa-close"></i></button></a>');
  $("#mdimenu ul").append(ndivtab);

  //var ndivtab =
  //var ndivtab = document.createElement('li');

  //ndivtab.style.margin = '0 0 0 -2000px';
  //ndivtab.style.float = 'left';
  //$(ndivtab).html(_X.MDI_GetHtml(mdiIdx, a_pgmcode, a_pgmname, true));

  //ndivtab.selected = true;
  //mdimenu.appendChild(ndivtab);

  _MDI_MENU[mdiIdx] = ndivtab;
  _MDI_MENU[mdiIdx]._OpenUrl = a_url;

  _CurMenuIndex = mdiIdx;
  _MDI_CNT++;

  _X.MDI_Select(mdiIdx, true);

  $('#mdimenu ul li a[data-toggle="tab"]').off('shown.bs.tab').on('shown.bs.tab', function(e) {
     var tId = parseInt( $(this).attr("href").replace("#x-mdi-", "") );
    // _MDI_MENU[tId].preMenuIndex = _CurMenuIndex;
    // _CurMenuIndex = tId;
    _X.MDI_Select(tId, true);
  });
};

/**
 * fn_name :  _X.MDI_Select
 * Description : 선택된 탭을 활성화 시킨다.
 * param :a_idx : tab index , ab_keepPreIndex : 이전 tab 활성화 유무
 * Statements :
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
 */
_X.MDI_Select = function(a_idx, ab_keepPreIndex)
{
  if(_MDI_MENU[a_idx]!=null){
    $('#mdimenu ul a[href="#x-mdi-' + a_idx + '"]').tab('show');

    _X.MDI_ShowTab();

    if(ab_keepPreIndex==null || !ab_keepPreIndex)
      _MDI_MENU[a_idx].preMenuIndex = _CurMenuIndex;
  }
  _CurMenuIndex = a_idx;

  if(_MDI_CNT==0) {
    _X.MDI_ShowMain();
  }

  if (typeof(pf_showtab)!="undefined") {
    pf_showtab(_CurMenuIndex);
  }
};

/**
 * fn_name :  _X.MDI_DeSelect
 * Description : 선택된 탭을 비활성화 시킨다.
 * param :a_newidx : tab index
 * Statements :
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
 */
_X.MDI_DeSelect = function(a_newidx)
{
  $("#mdimenu ul li").removeClass("active");
};



/**
 * fn_name :  _X.MDI_Close
 * Description : MDI 화면을 닫는다.
 * param :a_idx : tab index
 * Statements :
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
 */
_X.MDI_Close = function(a_idx) {
    if(_MDI_CNT==1) {
      _X.MDI_ShowMain();
    }

    var preMenuIndex = _X.MDI_PreMenuIndex(a_idx);
    _X.MDI_Select(preMenuIndex, true);
    $("#" + _MDI_FRAME[a_idx].id).remove();
    $(_MDI_MENU[a_idx]).remove();

    var mdiObj = $('#mdi_' + a_idx);
    if(mdiObj.length >0) {
      var mdi_win = $('#mdi_' + a_idx)[0].contentWindow;
      //화면에 로딩된 Flash 오브젝트 등을 Clear한다. (2013.04.07 김양열)
      xm_ClearObject(mdi_win);
      mdi_win = null;
    }

    //_X.purge(_MDI_MENU[a_idx]);
    //_X.purge(_MDI_FRAME[a_idx]);
    _MDI_FRAME[a_idx] = null;
    _MDI_MENU[a_idx] = null;

    _MDI_CNT--;

    if(_MDI_CNT<=0)
      _X.MDI_ShowMain();
};

/**
 * fn_name :  _X.MDI_PreMenuIndex
 * Description : 선택이전 tab index
 * param :a_idx : tab index
 * Statements :
 *
 *   since       author           Description
 *  ------------    --------    ---------------------------
 *   2013.02.10   김양열          최초 생성
 */
_X.MDI_PreMenuIndex = function(a_idx)
{
  //이전에 선택된 탭 활성화
  var preMenuIndex = $(_MDI_MENU[a_idx]).attr("preMenuIndex");
  if(preMenuIndex==a_idx || preMenuIndex==null || _MDI_FRAME[preMenuIndex]==null){
    for(var i=a_idx +1; i<_MDI_MENU.length; i++){
      if(_MDI_FRAME[i]!=null)
        return i;
    }
    for(var i=a_idx -1; i>=0; i--){
      if(_MDI_FRAME[i]!=null)
        return i;
    }
  }

  return preMenuIndex;
};

/**
* fn_name :  _X.MDI_Resize
* Description : 열린 MDI 프레임의 사이즈를 재조정한다.
*
* Statements :
*
*   since        author           Description
*  ------------    --------    ---------------------------
*   2013.02.10    김양열          최초 생성
*/
_X.MDI_Resize = function(){
  var newHeight = _X.MDI_Height();
  //$("#main-container").height(newHeight - 55);
  $("#main-container").height(newHeight);

  return;

  var newWidth  = _X.MDI_Width();

  for(var i=0; i<_MDI_FRAME.length; i++) {
    if(_MDI_FRAME[i]!=null) {
      //auto를 적용해 보자
      //_MDI_FRAME[i].style.width  = newWidth + "px";
      _MDI_FRAME[i].style.height = (newHeight) + "px";
      //_MDI_FRAME[i].style.height = (newHeight - 55) + "px";
      //_MDI_FRAME[i].style.min-width = newWidth + "px";
      //_MDI_FRAME[i].style.min-height = newHeight + "px";

      //_MDI_FRAME[i].style.width  = "auto";
      //_MDI_FRAME[i].style.height = "auto";

      //var mdi_win = $('#mdi_' + i)[0].contentWindow;

      //if(typeof(mdi_win.pageLayout)!="undefined")
      //  mdi_win.pageLayout.style.height = newHeight + "px";
      //X.MsgBox(newHeight + "px");
    }
  }
};

/**
* fn_name :  _X.MDI_Width
* Description : MDI 화면 높이를 구한다.
* param :
* Statements :
*
*   since        author           Description
*  ------------    --------    ---------------------------
*   2013.02.10    김양열          최초 생성
*/
_X.MDI_Height = function() {
  var newHeight = $("body").height() - $(".menu-header").height() - $("#mdimenu").height();

  return Math.max(newHeight,300);
};



_X.MDI_ShowMain = function() {
  $("#main-mdi-content").addClass("hide");
  $("#mdi_main").removeClass("hide");
  if($("#mdimenu>ul li").length==0) $("#mdimenu>ul").addClass("hide");

  _X.MDI_DeSelect(-1);
  $("#mdi_main").focus();
  $("#if_main").focus();
}

_X.MDI_ShowTab = function() {
  $("#main-mdi-content").removeClass("hide");
  $("#mdi_main").addClass("hide");
  $("#mdimenu>ul").removeClass("hide");

  _X.MDI_Resize();
}


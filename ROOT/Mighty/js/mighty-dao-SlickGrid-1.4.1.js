/**
 * Object 			: mighty-dao-SlickGrid-1.4.1.js
 * @Description : Grid 함수처리 JavaScript
 * @author 			: 엑스인터넷정보 이상규
 * @since 			: 2016.12.23
 * @version 		: 1.4.1
 *
 * @Modification Information
 * <pre>
 *   since    	     author      Description
 *  ------------    --------    ---------------------------
 *   2013.02.10      김양열      RealGrid      ver 최초생성
 *   2014.12.20      박영찬      share 기능 개선, updatable 지정
 *	 2015.06.11		   이상규      SlickGrid-2.2 ver 생성
 *   2016.12.23      이상규      SlickGrid-2.3 ver 생성
 */

/***
 * Style Format, Editor
 */
var _Styles_seq       = { "dataType": "number"	 , "decimalPoint": 0, "align": "align-center", "formatter": Slick.Formatters.Seq };
var _Styles_text      = { "dataType": "text"	 , "decimalPoint": 0, "align": "align-left"	 , "formatter": Slick.Formatters.Text };
var _Styles_textc     = { "dataType": "text"	 , "decimalPoint": 0, "align": "align-center", "formatter": Slick.Formatters.Text };
var _Styles_textr     = { "dataType": "text"	 , "decimalPoint": 0, "align": "align-right" , "formatter": Slick.Formatters.Text };
var _Styles_number    = { "dataType": "number"	 , "decimalPoint": 0, "align": "align-right" , "formatter": Slick.Formatters.Number };
var _Styles_numberc   = { "dataType": "number"	 , "decimalPoint": 0, "align": "align-center", "formatter": Slick.Formatters.Number };
var _Styles_numberl   = { "dataType": "number"	 , "decimalPoint": 0, "align": "align-left"	 , "formatter": Slick.Formatters.Number };
var _Styles_numberf1  = { "dataType": "numberf1" , "decimalPoint": 1, "align": "align-right" , "formatter": Slick.Formatters.NumberF1 };
var _Styles_numberf2  = { "dataType": "numberf2" , "decimalPoint": 2, "align": "align-right" , "formatter": Slick.Formatters.NumberF2 };
var _Styles_numberf3  = { "dataType": "numberf3" , "decimalPoint": 3, "align": "align-right" , "formatter": Slick.Formatters.NumberF3 };
var _Styles_numberf4  = { "dataType": "numberf4" , "decimalPoint": 4, "align": "align-right" , "formatter": Slick.Formatters.NumberF4 };
var _Styles_chardate  = { "dataType": "chardate" , "decimalPoint": 0, "align": "align-center", "formatter": Slick.Formatters.CharDate };
var _Styles_date      = { "dataType": "date"	 , "decimalPoint": 0, "align": "align-center", "formatter": Slick.Formatters.Date };
var _Styles_datemonth = { "dataType": "datemonth", "decimalPoint": 0, "align": "align-center", "formatter": Slick.Formatters.DateMonth };
var _Styles_datetime  = { "dataType": "datetime" , "decimalPoint": 0, "align": "align-center", "formatter": Slick.Formatters.DateTime };
var _Styles_hhmm      = { "dataType": "hhmm"     , "decimalPoint": 0, "align": "align-center", "formatter": Slick.Formatters.HhMm };
var _Styles_checkbox  = { "dataType": "checkbox" , "decimalPoint": 0, "align": "align-center", "formatter": Slick.Formatters.Checkbox };
var _Styles_tree      = { "dataType": "tree"	 , "decimalPoint": 0, "align": "align-left"	 , "formatter": Slick.Formatters.Tree };
var _Styles_file      = { "dataType": "text"	 , "decimalPoint": 0, "align": "align-left"	 , "formatter": Slick.Formatters.File };
var _Styles_mmdd      = { "dataType": "mmdd"     , "decimalPoint": 0, "align": "align-center", "formatter": Slick.Formatters.MmDd };

var _Editor_text      = Slick.Editors.Text;
var _Editor_multiline = Slick.Editors.LongText;
var _Editor_number    = Slick.Editors.Integer;
var _Editor_numberf1  = Slick.Editors.Integer;
var _Editor_numberf2  = Slick.Editors.Integer;
var _Editor_numberf3  = Slick.Editors.Integer;
var _Editor_numberf4  = Slick.Editors.Integer;
var _Editor_chardate  = Slick.Editors.CharDate;
var _Editor_date      = Slick.Editors.Date;
var _Editor_datemonth = Slick.Editors.Date;
var _Editor_datetime  = Slick.Editors.Date;
var _Editor_hhmm      = Slick.Editors.HhMm;
var _Editor_checkbox  = Slick.Editors.Checkbox;
var _Editor_dropdown  = Slick.Editors.Combo;
var _Editor_file      = Slick.Editors.File;
var _Editor_mmdd      = Slick.Editors.MmDd;

/***
 * SlickGrid option
 */
var _SlickGrid_options = {
  "editable"             : true,
  "enableAddRow"         : false,
  "enableCellNavigation" : true,
  "asyncEditorLoading"   : true,
  "enableAsyncPostRender": true,  //after render
  "asyncPostRenderDelay" : 0,     //after render delay
  "forceFitColumns"      : false, //화면 크기에 맞춰 컬럼의 길이를 가득 채움
  "topPanelHeight"       : 25,
  "inlineFilters"        : false, // dataview.setFilter
  "editCommandHandler"   : null,  // function(item, column, editCommand) {/*code*/}
  "multiColumnSort"      : true,  //다중 정렬 가능여부
  "multiSelect"          : false, //다중 행 선택
  "syncColumnCellResize" : true,  //컬럼 사이즈 변경 시 변경중일 때 렌더링 적용 여부
  "enableColumnReorder"  : true,  //컬림 순서 변경가능 여부
  "showHeaderRow"        : false, //헤더 행 표시여부
  "showTopPanel"         : false, //판넬 표시여부
  "showFooterRow"        : false, //합계 표시여부
  "footerRowCount"       : 0,     //합계 행 수
  "multiHeaderCount"     : 1,     //멀티헤더 행 수
  "autoEdit"             : true   //셀 포커스 시 에디트를 자동으로 열어줌
};

/***
 * SlickGrid columns headerMenu
 */
var x_GetColumnHeaderMenu = function (div) {
	var headerItems = [];

	/* sort */
	if ( div === "sort" ) {
		headerItems.push({
        iconImage: "/Mighty/3rd/SlickGrid/v2.3/images/sort-asc.gif",
        title		 : "오름차순 정렬",
        command	 : "sort-asc",
        disabled : false,
        tooltip	 : ""
    });
    headerItems.push({
        iconImage: "/Mighty/3rd/SlickGrid/v2.3/images/sort-desc.gif",
        title		 : "내림차순 정렬",
        command	 : "sort-desc",
        disabled : false,
        tooltip	 : ""
    });
    headerItems.push({
        iconImage: "/Mighty/3rd/SlickGrid/v2.3/images/sort-none.png",
        title		 : "정렬 해제",
        command	 : "sort-none",
        disabled : false,
        tooltip	 : ""
    });
	}
	/* filter */
	else if ( div === "filter" ) {
		headerItems.push({
      	iconImage: "/Mighty/3rd/SlickGrid/v2.3/images/filter-on.png",
        title		 : "필터 보이기",
        command	 : "filter-on",
        disabled : false,
        tooltip	 : ""
    });
    headerItems.push({
      	iconImage: "/Mighty/3rd/SlickGrid/v2.3/images/filter-off.png",
        title		 : "필터 숨기기",
        command	 : "filter-off",
        disabled : false,
        tooltip	 : ""
    });
	}

	return headerItems;
};

/***
 * SlickGrid first column
 */
var x_SetFirstColumn = function (a_obj) {
	var columns = a_obj._SlickGrid.getColumns();

	for ( var i = 1, ii = columns.length; i < ii; i++ ) {
		if ( !a_obj._FirstColName ) {
			a_obj._FirstColName = columns[i].field;
		}

		if ( !a_obj._FirstEditColName && !columns[i].readonly ) {
			a_obj._FirstEditColName = columns[i].field;
		}

		if ( a_obj._FirstColName && a_obj._FirstEditColName ) {
			break;
		}
	}
};

/***
 * SlickGrid columns config
 */
var x_ConvGridColumns = function(a_columnConfig, a_obj) {
	var slickCols = [];
	var mergedColumns = [];
	var maxFixedIdx = 1;

	a_obj._HeaderLineCnt = 0;
	a_obj._FooterLineCnt = 0;

	/* 행번호 컬럼 */
	slickCols[0] = {
		"id"                 : "sel",
		"headerNames"        : ["No."],
		"name"               : "No.",
		"toolTip"            : "순번",
		"field"              : "num",
		"behavior"           : "select",
		"cssClass"           : "cell-selection",
		"width"              : 41,	/*width(45->41)조정 (2017.10.10 KYY)*/
		"cannotTriggerInsert": true,
		"resizable"          : false,
		"selectable"         : false,
		"headerCssClass"     : null,
		"formatter"          : _Styles_seq.formatter,
		"groupSeq"           : false,
		"merge"              : false,
		"maxLength"          : 0,
		"columns"            : null,
		"button"             : "",
		"alwaysShowButton"   : false,
		"footerExpr"         : "합계",
		"reorderable"        : false,
		"fixed"              : true,
		"zerotonull"         : false,
		"removezero"         : false,
		"decimalPoint"       : 0
	};

	for ( var i = 0, ii = a_columnConfig.length; i < ii; i++ ) {
		/* tree 확인 */
		if ( a_columnConfig[i].styles.dataType === "tree" ) {
  			a_obj._IsTree = true;
			$.extend( _SlickGrid_options, { enableColumnReorder: false } );
  		}

		if ( a_columnConfig[i].visible ) {
			var headerNames    = (a_columnConfig[i].headerText || a_columnConfig[i].header.text).split("|"),
			    name 	 	   = headerNames[headerNames.length - 1],
				sortable	   = a_columnConfig[i].sortable,
				headerCssClass = null,
				cssClass 	   = null,
				headerMenu     = null,
				buttonObjId    = null,
				headerItems    = [],
				groupFooter    = {},
				headerLen      = 0,
				footerLen      = 0;

			/* 헤더명 */
			if ( a_columnConfig[i].mustInput || a_columnConfig[i].must_input === "Y" ) {
				name = "*" + name;
				headerNames[ headerNames.length - 1 ] = name;
			}

			/* 필수항목
			if ( a_columnConfig[i].mustInput || a_columnConfig[i].must_input === "Y" ) {
				headerCssClass += " must-input";
			}
			*/

			/* 편집불가 css */
			if ( a_columnConfig[i].readonly && !a_columnConfig[i].editable ) {
				cssClass = "readonly-cell"
			}

			/* 정렬 */
			if ( a_obj._IsTree ) {
				sortable = false;
			}

			/* 헤더 행 수 확인 */
		  	if ( a_columnConfig[i].headerText ) {
		  		headerLen = a_columnConfig[i].headerText.split("|").length;
		  	} else {
		  		headerLen = a_columnConfig[i].header.text.split("|").length;
		  	}

		  	a_obj._HeaderLineCnt = Math.max(a_obj._HeaderLineCnt, headerLen)

		  	/* 합계 행 수 확인 */
		  	if ( a_columnConfig[i].footerExpr ) {
		  		footerLen = a_columnConfig[i].footerExpr.split("|").length;
		  		a_obj._FooterLineCnt = Math.max(a_obj._FooterLineCnt, footerLen);
		  	}

			/* 헤더메뉴 sort */
			if ( sortable ) {
				headerItems = x_GetColumnHeaderMenu( "sort" );
			}

			/* 헤더메뉴 filter */
			if ( !a_obj._IsTree ) {
				headerItems = headerItems.concat( x_GetColumnHeaderMenu( "filter" ) );
				headerMenu  = { menu: { items: headerItems	}	};
			}

			/* 셀 버튼 */
			if ( a_columnConfig[i].button ) {
				buttonObjId = a_obj.id;
			}

			/* 체크박스 스타일의 경우 Editor 자동 설정 */
			if ( a_columnConfig[i].styles === _Styles_checkbox ) {
		  		a_columnConfig[i].editor = _Editor_checkbox;
		  	}

		  	/* merged columns */
		  	if ( a_columnConfig[i].merge ) {
		  		mergedColumns.push(a_columnConfig[i].fieldName);
		  	}

		  	/* fixed index */
		  	if ( a_columnConfig[i].fixed ) {
		  		maxFixedIdx = slickCols.length;
		  	}

		  	/* file info */
		  	if ( a_columnConfig[i].fileInfo ) {
		  		if ( a_columnConfig[i].fileInfo.fileMaxCnt ) {
		  			a_obj._FileMaxCnt = Math.max( a_obj._FileMaxCnt, a_columnConfig[i].fileInfo.fileMaxCnt );
		  		}

	  			if ( a_columnConfig[i].fileInfo.fileMaxSize ) {
	  				a_obj._FileMaxSize = Math.max( a_obj._FileMaxSize, a_columnConfig[i].fileInfo.fileMaxSize );
	  			}

	  			if ( a_columnConfig[i].fileInfo.fileSvrPath === false ) {
	  				a_obj._FileSvrPath = "N";
	  			}
		  	}

			slickCols[ slickCols.length ] = {
				"id" 				 : a_columnConfig[i].fieldName,
				"gridId" 			 : a_obj.id,
				"headerNames"        : headerNames,
				"name" 				 : name,
				"field" 			 : a_columnConfig[i].fieldName,
				"toolTip"			 : a_columnConfig[i].headerToolTip || "",
				"visible" 		 	 : a_columnConfig[i].visible,
				"width" 			 : a_columnConfig[i].width,
				"formatter"		 	 : a_columnConfig[i].styles.formatter,
				"editor"			 : a_columnConfig[i].editor,
				"readonly"			 : a_columnConfig[i].readonly,
				"editable"			 : a_columnConfig[i].editable,
				"sortable"		 	 : sortable,
				"dataType"		 	 : a_columnConfig[i].styles.dataType,
				"decimalPoint"       : a_columnConfig[i].styles.decimalPoint,
				"align"				 : a_columnConfig[i].styles.align,
				"lookupDisplay"		 : a_columnConfig[i].lookupDisplay,
				"headerCssClass"  	 : headerCssClass,
				"cssClass"			 : cssClass, 													//컬럼에 추가할 cssClass
				"numberOnly"		 : a_columnConfig[i].numberOnly,
				"styleBold"			 : a_columnConfig[i].styleBold,
				"focusable"        	 : true,
				"validator"        	 : a_columnConfig[i].validator, 			//컬럼의 예외처리 함수 		 ex) function(value) {return {valid: true, msg: null};}
				"asyncPostRender"  	 : a_columnConfig[i].asyncPostRender, //비동기 포스트렌더링 함수 ex) function( cellNode, rowIdx, item, columnInfo ) {}
				"header"			 : headerMenu,
				"dataview"			 : a_obj._DataView,
				"_collapsed"		 : true,
				"group"				 : a_columnConfig[i].group,
				"merge"				 : (a_columnConfig[i].merge ? a_columnConfig[i].merge : false),
				"mergedColumns"      : a_columnConfig[i].merge ? $.extend([], mergedColumns) : null,
				"maxLength"			 : (a_columnConfig[i].maxLength ? a_columnConfig[i].maxLength : 0),
	  	"button"			 : (a_columnConfig[i].button ? a_columnConfig[i].button : ""),
  		"alwaysShowButton" 	 : (a_columnConfig[i].alwaysShowButton ? a_columnConfig[i].alwaysShowButton : false),
				"textButton"		 : a_columnConfig[i].textButton,
				"fileInfo"           : a_columnConfig[i].fileInfo,
				"footerExpr"		 : a_columnConfig[i].footerExpr,
				"footerAlign"        : a_columnConfig[i].footerAlign,
				"footerStyle"        : a_columnConfig[i].footerStyle,
				"footerPre"          : a_columnConfig[i].footerPre,
				"footerPost"         : a_columnConfig[i].footerPost,
				"resizable"          : headerNames.length > 1 ? false : a_columnConfig[i].resizable,
				"reorderable"        : a_columnConfig[i].reorderable,
				"zerotonull"		: (a_columnConfig[i].zerotonull ? a_columnConfig[i].zerotonull : false),	//추가(2017.05.24 KYY)
				"removezero"		: (a_columnConfig[i].removezero ? a_columnConfig[i].removezero : false)	//추가(2017.12.04 KYY)
			};

			/* key 값 체크(입력모드만 수정 가능) */
			if ( a_columnConfig[i].isKey === "Y" ) {
				a_obj._KeyCols[ a_columnConfig[i].fieldName ] = a_columnConfig[i].editor;
			}
		}
	}

	if ( maxFixedIdx > 1 ) {
		for ( var i = 1, ii = maxFixedIdx; i <= ii; i++ ) {
			slickCols[i].fixed = true;
		}
	}

	$.extend( _SlickGrid_options, { multiHeaderCount: a_obj._HeaderLineCnt, showFooterRow: a_obj._FooterLineCnt > 0, footerRowCount: a_obj._FooterLineCnt } );

	return slickCols;
};

/***
 * sync info
 * 그리드와 프리폼의 sync 작업을 위한 sync object 캐시화
 */
var x_GridSetSyncInfo = function(a_obj) {
	if ( a_obj._Share && !a_obj._SyncInfoSet ) {
		a_obj._SyncInfoSet = true;

		for ( var i = 0, ii = a_obj._ColumnConfig.length; i < ii; i++ ){

			var $colObj = $("#" + a_obj._ColumnConfig[i].fieldName);
			if ( $colObj.length ) {
				$colObj[0].SyncGrid   		 = a_obj;
				$colObj[0].SyncColumn 		 = a_obj._ColumnConfig[i].fieldName;
				$colObj[0].SyncColumnIndex = i + 1;
				$colObj[0].SyncDataType 	 = a_obj._ColumnConfig[i].styles && a_obj._ColumnConfig[i].styles.dataType || "text";
				a_obj._SyncObject[ a_obj._SyncObject.length ] 	= $colObj[0];
				a_obj._SyncObjectJ[ a_obj._SyncObjectJ.length ] = $($colObj[0]);

				if ( a_obj._ColumnConfig[i].must_input === "Y" ) {

					var $inputBgObj = $($colObj[0]).closest(".detail_input_bg");
					if ( $inputBgObj.length ) {

						var $labelObj = $inputBgObj.prev(".detail_label,.detail_label_check,.detail_label_left,.detail_label_left_check");
						if ( $labelObj.length ) {

							var html = $labelObj.html();
							if ( html.length > 0 && html.substr(0, 1) !== "*" ) {
								$labelObj.html( "*" + html );
							}
						}
					}
				}
			}
		}
	}
};

/***
 * SlickGrid reset
 * dataview, slickgird 생성 | a_obj 변수 셋팅
 */
_X.ResetGrid = function(a_obj, a_column_config, a_field_config, a_option, a_sqlFile, a_share, a_updatable) {
	if(typeof a_obj.grid !== "undefined") {
		//gridParam.grid, gridParam.config, null, gridParam.option, gridParam.sqlFile + "|" + gridParam.sqlId, gridParam.share, gridParam.updatable
		a_obj.config	= (typeof a_obj.config === "undefined" || a_obj.config === null ? null : a_obj.config);
		a_obj.option	= (typeof a_obj.option === "undefined" || a_obj.option === null ? null : a_obj.option);
		a_obj.sqlFile 	= (typeof a_obj.sqlFile === "undefined" || a_obj.sqlFile === null ? _PGM_CODE : a_obj.sqlFile);
		a_obj.sqlId	  	= (typeof a_obj.sqlId === "undefined" || a_obj.sqlId === null ? null : a_obj.sqlId);
		a_obj.share 	= (typeof a_obj.share === "undefined" || a_obj.share === null ? false : a_obj.share);
		a_obj.updatable = (typeof a_obj.updatable === "undefined" || a_obj.updatable === null ? true : a_obj.updatable);

		a_column_config = a_obj.config;
		a_field_config  = null;
		a_option 		= a_obj.option;
		a_sqlFile 		= a_obj.sqlFile + "|" + a_obj.sqlId;
		a_share 		= a_obj.share;
		a_updatable 	= a_obj.updatable;
		a_obj 			= a_obj.grid;
	}

	if ( a_obj._SlickGrid ) {
		a_obj._SyncObject  = [];
		a_obj._SyncObjectJ = [];
		a_obj._FirstColName		= null;
		a_obj._FirstEditColName = null;
		a_obj._HeaderLineCnt	= 1;
		a_obj._FooterLineCnt	= 0;
		a_obj.Reset();
	}

	if ( a_sqlFile ) {
		var a_sqlFiles = a_sqlFile.split("|");
		a_obj.sqlFile = a_sqlFiles[0];
		a_obj.sqlKey  = a_sqlFiles[1];
	}

	if ( a_share == null ) {
		a_share = false;
	}

	if ( a_updatable == null ) {
		a_updatable = true;
	}

	if ( a_updatable ) {
		a_obj._UpdateInfo    = eval( _X.GSI( a_obj.subSystem, a_obj.sqlFile, a_obj.sqlKey ) );
		a_obj._UpDateKeyCols = [];

		if ( a_obj._UpdateInfo ) {
			for ( var i = 0, ii = a_obj._UpdateInfo.length; i < ii; i++ ) {
				if ( a_obj._UpdateInfo[i].iskeycol ) {
					a_obj._UpDateKeyCols.push( a_obj._UpdateInfo[i].name );
				}
			}
		}
	}

	a_obj._Share				   = a_share;
	a_obj._Updatable       = a_updatable;
	a_obj._ColumnConfig    = a_column_config;
	a_obj._KeyCols 		     = {};
	a_obj._Options         = a_option;
	a_obj._ConvGridColumns = x_ConvGridColumns( a_obj._ColumnConfig, a_obj );
	//추가(2017.07.06 KYY)
	a_obj._CheckBar		= false;

	if ( a_obj._Share ) {
		a_obj._SyncInfoSet = false;
		x_GridSetSyncInfo( a_obj );
	}

	/* plugins 생성 */
	a_obj._Plugins[ "RowSelectionModel" ] 		  = new Slick.RowSelectionModel();
	a_obj._Plugins[ "HeaderMenu" ] 			  	  = new Slick.Plugins.HeaderMenu( {} );
	a_obj._Plugins[ "groupItemMetadataProvider" ] = new Slick.Data.GroupItemMetadataProvider();
	a_obj._Plugins[ "FooterExpression" ] 		  = new Slick.Data.FooterExpression( { footerRowCount: a_obj._FooterLineCnt, formatter: Slick.Formatters.Footer } );

	a_obj._DataView  = new Slick.Data.DataView( { "groupItemMetadataProvider": a_obj._Plugins[ "groupItemMetadataProvider" ], "inlineFilters": false } );
	a_obj._SlickGrid = new Slick.Grid( "#" + a_obj.id, a_obj._DataView, a_obj._ConvGridColumns, $.extend(true, _SlickGrid_options, a_option) );

	/* plugins 적용 */
	a_obj._SlickGrid.setSelectionModel( a_obj._Plugins[ "RowSelectionModel" ] );
	a_obj._SlickGrid.registerPlugin		( a_obj._Plugins[ "HeaderMenu" ] );
	a_obj._SlickGrid.registerPlugin		( a_obj._Plugins[ "groupItemMetadataProvider" ] );
	a_obj._SlickGrid.registerPlugin		( a_obj._Plugins[ "FooterExpression" ] );

	/* Plugins event */
	a_obj._Plugins[ "HeaderMenu" ].onCommand.subscribe(function(e, args) { xe_M_PluginsHeaderMenuClicked( a_obj, e, args ); });

	/* SlickGrid event */
	a_obj._SlickGrid.onScroll.subscribe					(function (e, args) { a_obj.StartSlickEvent( "onScroll"					); xe_M_SlickGridScroll					 ( a_obj, e, args ); a_obj.EndSlickEvent( "onScroll"				); });
	a_obj._SlickGrid.onValidationError.subscribe		(function (e, args) { a_obj.StartSlickEvent( "onValidationError"		); xe_M_SlickValidationError 			 ( a_obj, e, args ); a_obj.EndSlickEvent( "onValidationError"		); });
	a_obj._SlickGrid.onSort.subscribe					(function (e, args) { a_obj.StartSlickEvent( "onSort"					); xe_M_SlickGridSort					 ( a_obj, e, args ); a_obj.EndSlickEvent( "onSort"					); });
	a_obj._SlickGrid.onHeaderClick.subscribe			(function (e, args) { a_obj.StartSlickEvent( "onHeaderClick"			); xe_M_SlickGridHeaderClick			 ( a_obj, e, args ); a_obj.EndSlickEvent( "onHeaderClick"			); });
	a_obj._SlickGrid.onClick.subscribe					(function (e, args) { a_obj.StartSlickEvent( "onClick"					); xe_M_SlickGridItemClick				 ( a_obj, e, args ); a_obj.EndSlickEvent( "onClick"					); });
	a_obj._SlickGrid.onDblClick.subscribe				(function (e, args) { a_obj.StartSlickEvent( "onDblClick"				); xe_M_SlickGridItemDoubleClick		 ( a_obj, e, args ); a_obj.EndSlickEvent( "onDblClick"				); });
	a_obj._SlickGrid.onKeyDown.subscribe				(function (e, args) { a_obj.StartSlickEvent( "onKeyDown"				); xe_M_SlickGridKeyDown				 ( a_obj, e, args ); a_obj.EndSlickEvent( "onKeyDown"				); });
	a_obj._SlickGrid.onCellChange.subscribe				(function (e, args) { a_obj.StartSlickEvent( "onCellChange"				); xe_M_SlickGridDataChanged			 ( a_obj, e, args ); a_obj.EndSlickEvent( "onCellChange"			); });
	a_obj._SlickGrid.onActiveCellChanged.subscribe	    (function (e, args) { a_obj.StartSlickEvent( "onActiveCellChanged"		); xe_M_SlickGridItemFocusChanged		 ( a_obj, e, args ); a_obj.EndSlickEvent( "onActiveCellChanged"		); });
	a_obj._SlickGrid.onSelectedRowsChanged.subscribe    (function (e, args) { a_obj.StartSlickEvent( "onSelectedRowsChanged"	); xe_M_SlickGridRowFocusChanged 		 ( a_obj, e, args ); a_obj.EndSlickEvent( "onSelectedRowsChanged"	); });
	a_obj._SlickGrid.onHeaderRowCellRendered.subscribe	(function (e, args) { a_obj.StartSlickEvent( "onHeaderRowCellRendered"  ); xe_M_SlickGridHeaderRowCellRendered 	 ( a_obj, e, args ); a_obj.EndSlickEvent( "onHeaderRowCellRendered" ); });
	a_obj._SlickGrid.onColumnsReordered.subscribe		(function (e, args) { a_obj.StartSlickEvent( "onColumnsReordered"		); xe_M_SlickGridColumnsReordered 		 ( a_obj, e, args ); a_obj.EndSlickEvent( "onColumnsReordered"		); });
	a_obj._SlickGrid.onBeforeEditCell.subscribe			(function (e, args) { a_obj.StartSlickEvent( "onBeforeEditCell"			); var res = xe_M_SlickGridBeforeEditCell ( a_obj, e, args ); a_obj.EndSlickEvent( "onBeforeEditCell"		); return res; });

	//추가(2017.05.25 KYY)
	a_obj._SlickGrid.onViewportChanged.subscribe		(function (e, args) { a_obj.StartSlickEvent( "onViewportChanged"		);  xe_M_SlickGridViewportChanged ( a_obj, e, args ); a_obj.EndSlickEvent( "onViewportChanged"		); });

	/* DataView event */
	a_obj._DataView.onRowCountChanged.subscribe(function (e, args) { a_obj._Commited = false; xe_M_SlickGridRowCountChanged( a_obj, e, args ); });
	a_obj._DataView.onRowsChanged.subscribe	   (function (e, args) { a_obj._Commited = false; xe_M_SlickGridRowsChanged	   ( a_obj, e, args ); });

	/* Slick 이벤트 시작 */
	a_obj.StartSlickEvent = function (eventName) {
  	a_obj._Commited = false;
  	a_obj._SlickEventObj.push( eventName, 5 );
  	a_obj.StartMightyEvent( eventName );
  	//console.log( a_obj._SlickEventObj );
  };

  /* Slick 이벤트 종료 */
  a_obj.EndSlickEvent = function (eventName, returnVal) {
  	if ( a_obj._SlickEventObj.indexOf( eventName ) == 0 ) {
  		a_obj._SlickEventObj = [];
  		a_obj.EndMightyEvent( eventName );
  	}
  	return returnVal;
  };

  /* Mighty 이벤트 시작 */
	a_obj.StartMightyEvent = function (eventName, eventLevel) {
		eventLevel = eventLevel || 10;
		if ( this._MightyEventObj != null && this._MightyEventObj.order != null ) {
			this._MightyEventObj.order.push( eventName );

			if (eventLevel > a_obj._EventLevel) {
				console.log( a_obj._MightyEventObj.order );
			}
		}
	};

	/* Mighty 이벤트 종료 */
	a_obj.EndMightyEvent = function (eventName, returnVal) {
		if (this._MightyEventObj!=null &&  a_obj._MightyEventObj.order.indexOf( eventName ) == 0 ) {
  		a_obj._MightyEventObj = { "order": [], "div": [] };
  	}
  	return returnVal;
	};

  /* 첫번재 컬럼 등록 */
  if ( !a_obj._ReadOnly ) {
		x_SetFirstColumn( a_obj );
	}
};

/***
 * SlickGrid create
 * slickgrid 초기화 함수 호출
 */
_X.InitGrid = function (a_div, a_id, a_width, a_height, a_subSystem, a_layoutFile, a_sqlFile, a_share, a_updatable, a_option) {
	//_X.InitGrid(grid_1, "dg_1", "100%", "100%", "cm", "cm251010", "CM251010|CM251010_01",false,true);
	window._GridRequestCnt++;

	var gridParam = {};

	if(typeof a_div.grid !== "undefined") {
		gridParam = a_div;

		gridParam.grid = (typeof gridParam.grid === "object" ? gridParam.grid : $("#" + gridParam.grid)[0]);;

		if(typeof gridParam.div === "undefined" || gridParam.div === null) {
			gridParam.div  = $(gridParam.grid).parents("div")[0];
		}
		else {
			gridParam.div  = (typeof gridParam.div === "object" ? gridParam.div : $("#" + gridParam.div)[0]);
		}

		gridParam.tree = false;
		gridParam.share = (typeof gridParam.share === "undefined" || gridParam.share === null ? false : gridParam.share);
		gridParam.updatable = (typeof gridParam.updatable === "undefined" || gridParam.updatable === null ? true : gridParam.updatable);
		gridParam.grid.subSystem = _PGM_CODE.substr(0,2).toLowerCase();
		gridParam.sqlFile = (typeof gridParam.sqlFile === "undefined" || gridParam.sqlFile === null ? _PGM_CODE : gridParam.sqlFile);
		gridParam.sqlId	  = (typeof gridParam.sqlId === "undefined" || gridParam.sqlId === null ? null : gridParam.sqlId);
		gridParam.option = a_option;
	} else {
		gridParam.div = (typeof a_div === "object" ? a_div : $("#" + a_div)[0]);;
		gridParam.grid = (typeof a_id === "object" ? a_id : $("#" + a_id)[0]);
		gridParam.tree = false;
		gridParam.share = (a_share!==null ? a_share : false);
		gridParam.updatable = (a_updatable!==null ? a_updatable : true);
		gridParam.grid.subSystem = a_subSystem;
		gridParam.sqlFile = a_sqlFile.split('|')[0];
		gridParam.sqlId	  = a_sqlFile.split('|')[1];
		gridParam.option  = a_option;
	}

	//_X.SlickGridInit( gridObj, a_div, false, a_share, a_updatable, a_sqlFile, a_option );
	_X.SlickGridInit( gridParam );
};

/**
 * fn_name 				: _X.slickGridInit
 * Description 		: Grid 기본 변수, DataView 와 Slickgrid, 컬럼, 스타일, 이벤트, Grid 메서드를 초기화 한다.
 * param 					: a_obj: grid object, a_div: grid div, ab_tree: tree유무, a_share: share 여부, a_updatable: update 여부
 * Statements 		:
 *
 *   since   		     author      Description
 *  ------------    --------    ---------------------------
 *   2013.02.10  	   김양열      최초 생성
 *   2015.06.11      이상규      SlickGrid ver 으로 변경 및 기능 추가
 */
//_X.SlickGridInit = function (a_obj, a_div, ab_tree, a_share, a_updatable, a_sqlFile, a_option) {
_X.SlickGridInit = function (gridParam) {
	//변수 셋팅
	var a_obj = gridParam.grid;
	window._Grids[ window._Grids.length ] = a_obj;
	a_obj._IsTree     		 = gridParam.tree;
	a_obj._IsLayoutCompleted = false;
	a_obj._CanvasResized	 = false;
	a_obj._FilesSize		 = 0;
	a_obj._FileUploadInfo    = {};
	a_obj._FileMaxCnt        = null;
	a_obj._FileMaxSize       = null;
	a_obj._FileSvrPath       = "Y";
	a_obj._CurrentRow 		 = -1;
	a_obj._CurrentCell		 = -1;
	a_obj._OldRow 			 = -1;
	a_obj._OldCell           = -1;
	a_obj._LastRowNumber     = 0;
	a_obj._IsRetrieving 	 = false;
	a_obj._Div  			 = gridParam.div;
	a_obj._ReadOnly 		 = !_Compact ? false : true;
	a_obj._ReadOnlyColor     = "#EAEAEA";
	a_obj._Updatable 		 = false;
	a_obj._UpdateInfo        = null;
	a_obj._UpDateKeyCols     = [];
	a_obj._GridReadonly		 = false;
	a_obj._FirstColName 	 = null;
	a_obj._FirstEditColName  = null;
	a_obj._MaskColumns		 = [];
	a_obj._DeletedRows 		 = [];
	a_obj._SyncObject  		 = [];
	a_obj._SyncObjectJ 		 = [];
	a_obj._SyncInfoSet 		 = false;
	a_obj._SetTimeId		 = null;
	a_obj._MightyEventObj    = { "order": [], "div": [] };
	a_obj._SlickEventObj     = [];
	a_obj._EventLevel      	 = 10;
	a_obj._Stylesheet		 = {};
	a_obj._SortInfo		     = {};
	a_obj._Plugins 			 = {};
	a_obj._ColumnFilters 	 = {};
	a_obj._Filter            = null;
	a_obj._Commited			 = false;
	a_obj._TreeData			 = null;
	a_obj._GroupIndicator = true;
	a_obj._GroupData		 = {};
	a_obj._HeaderLineCnt	 = 1;
	a_obj._FooterLineCnt	 = 0;
	a_obj._Focusable       = false;

	/* CheckBox 클릭시 값 바로변경 */
	a_obj._ClickedColumn     = "";
	a_obj._ClickedTime       = 0;

	if(typeof gridParam.config === "undefined" || gridParam.config === null) {
		eval("gridParam.config = columns_" + gridParam.grid.id + ";" );
	}

	//_X.ResetGrid( gridParam.grid, gridParam.config, null, gridParam.option, gridParam.sqlFile + "|" + gridParam.sqlId, gridParam.share, gridParam.updatable );
	_X.ResetGrid( gridParam);

	/* 화면에 로딩된 a_obj 의 참조값 삭제 */
	a_obj.removeEvents = function() {
		a_obj.Reset();
		a_obj._IsTree 			 = null;
		a_obj._IsLayoutCompleted = null;
		a_obj._FilesSize         = null;
		a_obj._FileUploadInfo    = null;
		a_obj._CurrentRow 		 = null;
		a_obj._CurrentCell		 = null;
		a_obj._OldRow 			 = null;
		a_obj._OldCell           = null;
		a_obj._LastRowNumber     = null;
		a_obj._IsRetrieving 	 = null;
		a_obj._Div 				 = null;
		a_obj._ReadOnly 		 = null;
		a_obj._ReadOnlyColor	 = null;
		a_obj._Share 			 = null;
		a_obj._Updatable 		 = null;
		a_obj._UpdateInfo        = null;
		a_obj._UpDateKeyCols     = null;
		a_obj._GridReadonly		 = null;
		a_obj._FirstColName 	 = null;
		a_obj._FirstEditColName  = null;
		a_obj._SyncObject 		 = null;
		a_obj._MaskColumns		 = null;
		a_obj._DeletedRows 		 = null;
		a_obj._SyncObjectJ 		 = null;
		a_obj._SyncInfoSet 		 = null;
		a_obj._SetTimeId         = null;
		a_obj._MightyEventObj    = null;
		a_obj._SlickEventObj     = null;
		a_obj._EventLevel      	 = null;
		a_obj._ColumnConfig      = null;
		a_obj._ConvGridColumns   = null;
		a_obj._KeyCols           = null;
		a_obj._SortInfo		     = null;
		a_obj._Plugins 			 = null;
		a_obj._ColumnFilters 	 = null;
		a_obj._Filter            = null;
		a_obj._Commited			 = null;
		a_obj._TreeData			 = null;
		a_obj._GroupIndicator = null;
		a_obj._GroupData		 = null;
		a_obj._HeaderLineCnt	 = null;
		a_obj._FooterLineCnt	 = null;
		a_obj._Focusable     	 = null;
		a_obj.RemoveAllStyle();
		a_obj.Destroy();
		a_obj._Stylesheet		 = null;
		a_obj._SlickGrid 		 = null;
		a_obj._DataView 		 = null;
	};

//============================================================== SlickGrid Method start
  a_obj.UpdateColumnHeader = function (colName, title, toolTip) {
  	a_obj._SlickGrid.updateColumnHeader( colName, title, toolTip );
  };

  a_obj.SetFocus = function () {
  	a_obj._SlickGrid.setFocus();
  };

  a_obj.ResizeCanvas = function () {
		/* 숨김 Grid의 경우 Grid ResizeCanvas 호출 */
  	if ( !a_obj._CanvasResized ) {
	  	a_obj._SlickGrid.resizeCanvas()
	  	a_obj._CanvasResized = true;
	  }
  }

//============================================================== SlickGrid Method end

//============================================================== Mighty-X5 Basic Method start
	//그리드 제거
  a_obj.Destroy = function () {
  	this.StartMightyEvent( "Destroy", 1 );
  	this._SlickGrid.destroy();
  	this.EndMightyEvent( "Destroy" );
  };

	//LoadingBar 생성
	a_obj.ShowLoadingBar = function() {
		this.StartMightyEvent( "ShowLoadingBar", 1 );
		this.EndMightyEvent( "ShowLoadingBar" );
	};

	//LoadingBar 삭제
	a_obj.HideLoadingBar = function() {
		this.StartMightyEvent( "HideLoadingBar", 1 );
		this.EndMightyEvent( "HideLoadingBar" );
	};

	//그리드의 변경사항을 적용
	a_obj.GridCommit = function (compulsion) {
		this.StartMightyEvent( "GridCommit", 4 );

		var res = false;

		/* 이벤트 단위로 commit 은 한번만 수행*/
		if ( compulsion || !this._Commited ) {
			var editor = this._SlickGrid.getEditController(),
			    res    = editor && editor.commitCurrentEdit();

			if ( this._Share ) {
				var activeCell = this._SlickGrid.getActiveCell();

				if ( activeCell ) {
					var colName = this.GetColumnNameByIndex ( activeCell.cell );

					x_GridRowSync( this, this.GetRow(), colName, this.GetItem( activeCell.row + 1, colName, false ) );
				}
			}

			this._Commited = true;
		}

		return this.EndMightyEvent( "GridCommit", res );
	};

	//그리드의 변경사항을 취소
	a_obj.GridCancel = function () {
		this.StartMightyEvent( "GridCancel", 4 );

		var editor = this._SlickGrid.getEditController();
		return this.EndMightyEvent( "GridCancel", editor && editor.cancelCurrentEdit() );
	};

	//그리드를 초기 상태로 돌린다
	a_obj.Reset = function (filter, sort) {
		this.StartMightyEvent( "Reset", 4 );
		this.GridCancel();
		this.ClearRows();
		this.SyncObjectReset();

		/* filter */
		if ( !(filter === false) ) {
			$("." + this.id + "-filter-input").val("");
			this._ColumnFilters = {};
		}

		/* sort */
		if ( !(sort === false) && !this.IsGroup() ) {
			this._SortInfo = {};
			this._SlickGrid.setSortColumns( [] );
			this.ResetSort( true );
		}

		/* checkbar */
		$(".ui-state-default.slick-header-column input[type=checkbox]").prop( "checked", false );

		/* file form */
		$("#" + this.id + "_fileUploadForm").remove();
		this._FilesSize      = 0;
		this._FileUploadInfo = {};

		this._CurrentRow  	 = -1;
		this._OldRow  			 = -1;
		this._CurrentCell 	 = -1;
		this._OldCell  	  	 = -1;
		this._LastRowNumber  = 0;
		this._DeletedRows 	 = [];
		this.EndMightyEvent( "Reset" );
	};

	//sync 관련된 컬럼의 값을 초기화
	a_obj.SyncObjectReset = function () {
		this.StartMightyEvent( "SyncObjectReset", 4 );

		for ( var i = 0, ii = this._SyncObject.length; i < ii; i++ ) {
			this._SyncObject[i].value = "";
		}

		this.EndMightyEvent( "SyncObjectReset" );
	};

	//데이타 재생성
	a_obj.Refresh = function() {
		dg_1._DataView.refresh();
	};

	//렌더링 처리
	a_obj.RenderProcess = function(rowIdx, scroll, active) {
		this.StartMightyEvent( "RenderProcess", 5 );

	  	/* 스크롤 변경 */
	  	if ( scroll ) {
			this._SlickGrid.scrollRowToTop( rowIdx - 1 );
	  	}
	  	else {
			this._SlickGrid.render();
	  	}

	  	/* SetRow 호출 */
	  	if ( rowIdx ) {
			var colIdx = null;

			if ( active ) {
				if ( typeof active === "number" ) {
			  		colIdx = active;
			  	} else if ( typeof active === "string" ) {
					colIdx = this.GetColumnIndexByName( active );
				} else {
					var activeCell = this._SlickGrid.getActiveCell();
			  		colIdx = activeCell && activeCell.cell || 1;
		  		}
			}

			this.SetRow( rowIdx, colIdx );
		}
		this.EndMightyEvent( "RenderProcess" );
	};

	//데이타 저장
	a_obj.Save = function (beforeSQL, afterSQL, MsgYn, beforeSqlData, afterSqlData) {
		this.StartMightyEvent( "Save" );

		for ( var i = 0, ii = window._Grids.length; i < ii; i++ ) {
			window._Grids[i].GridCommit();
		}

		var sqlData = this.GetSQLData();

		MsgYn = MsgYn || "Y";

		if ( (sqlData == null || sqlData.length === 0) && Object.keys(this._FileUploadInfo).length === 0 ) {
			if ( MsgYn == "Y" ) {
				_X.Noty("변경된 데이타가 존재하지 않습니다.", null, 'danger', 500);
			}
			return this.EndMightyEvent( "Save", 0 );
		}

		if (beforeSqlData!=null) {
			sqlData = beforeSqlData + _Row_Separate + sqlData;
		}
		if (afterSqlData!=null) {
			sqlData = sqlData + _Row_Separate + afterSqlData;
		}

		//필수 입력 항목 체크
		if ( this.MustInputCheck() > 0 ){
			return this.EndMightyEvent( "Save", -1 );
		}

		//문자열 길이 체크
		if ( this.ByteInputCheck() > 0 ){
			return this.EndMightyEvent( "Save", -1 );
		}

		this.ShowLoadingBar();

		//Session Timeout 처리
		var rowCnt = _X.SXD( this.subSystem + _Col_Separate + _PGM_CODE, sqlData, beforeSQL, afterSQL );

		if ( rowCnt != -1 ) {
			this.ResetRowsStates();
		}

		var fileCnt = 0;
		if ( rowCnt != -1 ) {
			this.FileSave(function(cnt) {
					fileCnt += cnt;
			});
		}

		this.HideLoadingBar();

		if ( rowCnt != null && rowCnt > 0 ) {
			if ( MsgYn === "Y" ) {
				if ( fileCnt ) {
					_X.Noty(rowCnt + "건의 데이터 변경작업이<br>정상적으로 이루어졌습니다.<br><br>" + fileCnt + "건의 첨부파일 변경작업이<br>정상적으로 이루어졌습니다.", null, "success", 500);
				} else {
					_X.Noty(rowCnt + "건의 데이터 변경작업이<br>정상적으로 이루어졌습니다.", null, "success", 500);
				}
			}

			if ( x_DAO_Saved ) {
				x_DAO_Saved();
			}

			return this.EndMightyEvent( "Save", rowCnt );
		}

		if ( fileCnt ) {
			_X.Noty(fileCnt + "건의 첨부파일 변경작업이<br>정상적으로 이루어졌습니다.", null, "success", 500);
		}

		this.EndMightyEvent( "Save", 0 );
	};

	//데이타 조회
	a_obj.Retrieve = function (args) {
		/* Focus 처리 */
		var $focused = $(':focus');

		this.StartMightyEvent( "Retrieve" );
		this._MightyEventObj.div.push( "Retrieve" );

		window._IsRetrieving = true;
		this._IsRetrieving   = true;

		this.Reset( true, !this.IsGroup() );
		this._DataView.beginUpdate();

		var rowData = _X.XS( this.subSystem, this.sqlFile, this.sqlKey, args, "jsonwithid", this._IsTree );

		/* Tree */
		if ( this._IsTree ) {
			this.SetTreeDataCache( rowData );
		}

		try {
			this.SetData( rowData );
		} catch (err) {
			_X.MsgBox(err);
		}

		this._DataView.endUpdate();
		this.RenderProcess();

		if ( this._Focusable ) {
			this.SetRow( this.IsGroup() ? this.GetGroupItemRow(1) : 1, null, true );
		}
		else {
			var selectionModel = this._SlickGrid.getSelectionModel();
			if ( selectionModel ) {
				var lastCell = this.GetColumns().length - 1;
				selectionModel.setSelectedRanges( [ new Slick.Range(0, 0, 0, lastCell) ] );
			}
		}

		xe_M_SlickGridDataLoad( a_obj );

		//Focus처리관련 수정(2016.05.16 KYY)
		//if($focused.length > 0 && $focused[0].id != null && $focused[0].id != "") {
		//	setTimeout( function() {alert($focused[0].id); $focused.focus();}, 0 );
		//}
		return this.EndMightyEvent( "Retrieve", rowData.length );
	};

	//행 삽입, 추가
	a_obj.InsertRow = function (rowIdx, extendArgs, forceSetRow) {
		//rowFocus(포커스 처리여부 추가(2016.12.29 KYY)
		if(forceSetRow==null){forceSetRow = true;}

		var eventName = rowIdx > 0 ? "InsertRow" : "AddRow";
		this.StartMightyEvent( eventName );
		this._MightyEventObj.div.push( eventName );

		var newRowItem = this.NewItem(),
				id				 = "id_" + _X.LPad( ++this._LastRowNumber, 8, "0" );

		$.extend( newRowItem, { id: id, num: "", job: "I" } );

		if ( extendArgs ) {
			$.extend( newRowItem, extendArgs );
		}

		this._MightyEventObj.id = newRowItem.id;
		this.GridCommit();

		/* 삽입 */
		if ( rowIdx > 0 ) {
			/* 그룹 시 row 가 아닌 idx 번호로 rowIdx를 변경 */
			if ( this.IsGroup() ) {
				var item = this.GetRowData( rowIdx );
				rowIdx = this.GetIdxById( item.id );
			}

			try {
				this._DataView.insertItem( rowIdx - 1, newRowItem );
    	} catch (err) {
    		_X.MsgBox(err);
    	}
		}
		/* 추가 */
		else {
			if ( this.RowCount() ) {
				try {
					this._DataView.addItem( newRowItem );
				} catch (err) {
					_X.MsgBox(err);
				}
			}
			else {
				try {
					this.SetData( [ newRowItem ] );
				} catch (err) {
	    		_X.MsgBox(err);
	    	}
			}
		}

		//this.RenderProcess();
		var row = this.GetRowById( id );
		if(forceSetRow) {
			this.SetRow( row, this.GetColumnIndexByName( a_obj._FirstEditColName ), true );
		}

		return this.EndMightyEvent( eventName, row );
	};

	//행 복제
	a_obj.DuplicateRow = function (rowIdx) {
		this.StartMightyEvent( "DuplicateRow" );
		this._MightyEventObj.div.push( "DuplicateRow" );

		rowIdx = rowIdx || this.GetRow();

		if ( !rowIdx ) {
			return this.EndMightyEvent( "DuplicateRow" );
		}

		var rowData = $.extend( true, {}, this.GetRowData( rowIdx ) );

		if ( rowData ) {
			rowIdx = rowIdx + 1;

			delete rowData.id;
			delete rowData.num;
			delete rowData.job;

			this.InsertRow( rowIdx, rowData );
		}

		return this.EndMightyEvent( "DuplicateRow", rowIdx );
	};

	//행 삭제
	a_obj.DeleteRow = function (rowIdx) {
		this.StartMightyEvent( "DeleteRow" );
		this._MightyEventObj.div.push( "DeleteRow" );

		rowIdx = rowIdx || this.GetRow();

		if ( !rowIdx || rowIdx === -1 ) {
			return this.EndMightyEvent( "DeleteRow" );
		}

		var item 			 = this.GetRowData( rowIdx ),
				activeCell = this._SlickGrid.getActiveCell();

		if ( (item && item.__group) || (item && item.__groupTotals) ) {
			return this.EndMightyEvent( "DeleteRow" );
		}

		/*=== 신규 추가 후 삭제되는 데이타는 저장하지 않음 ===*/
		if ( this.GetRowState( rowIdx ) !== "I" ) {
			this._DeletedRows.push( item );
		}

		try {
			this._DataView.deleteItem( item.id );
  	} catch (err) {
  		_X.MsgBox(err);
  	}

  	var rowCount = this.RowCount();

  	if ( rowCount ) {
			var nextRowIdx = rowIdx;

	  	if ( nextRowIdx > rowCount ) {
				nextRowIdx = rowCount;
			}

			this.RenderProcess();

			if ( rowIdx === nextRowIdx ) {
				var args = {
						"grid": this._SlickGrid,
						"rows": [ this.GetGroupItemRow( nextRowIdx, 0 ) - 1 ],
						"isFocusChanged": true
				};

				xe_M_SlickGridRowFocusChanged( this, null, args );
			} else {
				this.SetRow( this.GetGroupItemRow( nextRowIdx, 0 ), activeCell && activeCell.cell, true );
			}

		}
		else {
			var args = {
					"grid": this._SlickGrid,
					"rows": [ -1 ],
					"isFocusChanged": true
			};

			xe_M_SlickGridRowFocusChanged( this, null, args );
		}

		this.EndMightyEvent( "DeleteRow" );
	};

	//ExcelExport 시 그리드 데이타를 배열로 반환
	a_obj.GetExcelData = function(formatter) {
		this.StartMightyEvent( "GetExcelData" );

		var columns = this.GetColumns(),
				datas   = this.GetFilteredData(),
				columnArr  = [],
				rowDataArr = [],
				excelDatas = [];

    for ( var h_cnt = 0; h_cnt < a_obj._HeaderLineCnt; h_cnt++ ) {
    	columnArr.push([]);
    }

		for ( var i = 0, ii = a_obj._ColumnConfig.length; i < ii; i++ ) {
			if ( !a_obj._ColumnConfig[i].visible ) {
				continue;
			}

			var htitle_arr = a_obj._ColumnConfig[i].header.text.split("|");

			for ( var k = 0, kk = htitle_arr.length; k < kk; k++ ) {
				var h_text = htitle_arr[k];
				var colarr_length = columnArr[k].length;

				if ( colarr_length == 0 || columnArr[k][colarr_length-1].colname != h_text ) {
					var h_info = {};
					h_info.colname = h_text;
					h_info.colspan = 1;
					h_info.rowspan = a_obj._HeaderLineCnt - htitle_arr.length + 1;

					columnArr[k].push(h_info);
				}
				else {
					columnArr[k][colarr_length-1].colspan++;
				}
			}
		}

		var cellDataArr, value, backgroundColor;

		for ( var i = 0, ii = datas.length; i < ii; i++ ) {
			cellDataArr = [];

			for ( var j = 1, jj = columns.length; j < jj; j++ ) {
				value = null;

				//체크바 컬럼 제외
				if ( j === 1 && columns[j].field === "check" ) {
					continue;
				}

				if ( columns[j].comboData ) {
					value = columns[j].comboData.syncLabel[ datas[i][ columns[j].field ] ];
				} else {
					value = datas[i][ columns[j].field ];
				}

				if ( columns[j].dataType !== "checkbox" ) {
					if ( formatter ) {
						//formatter 적용
						value = columns[j].formatter( i, j, value, columns[j], datas[i] );
					}
					else if ( ( columns[j].dataType === "date" || columns[j].dataType === "datetime" ) && value != null && value != "" ) {
						value = _X.ToString( value, "YYYY.MM.DD" );
					}
					else if ( columns[j].dataType === "datemonth" && value != null && value != "" ) {
						value = _X.ToString( value, "YYYY.MM" );
					}
				}

				//default 공백 처리
				if ( value == null || typeof value == "undefined" || value == "undefined" ) {
					value = "";
				}

				backgroundColor = (columns[j].readonly == true && columns[j].editable == false) ? "#DCDCDC" : "#FFFFFF";
				cellDataArr.push( [columns[j].dataType, value, backgroundColor ] );
			}

			rowDataArr.push( cellDataArr );
		}

		excelDatas[0] = columnArr;
		excelDatas[1] = rowDataArr;

		return this.EndMightyEvent( "DeleteRow", excelDatas );
	};

	//엑셀 보내기
	a_obj.ExcelExport = function (formatter, filename) {
		this.StartMightyEvent( "ExcelExport" );

		var excelData    = this.GetExcelData(formatter),
			excelDataStr = "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'><table id='excel_table'><thead>";

		for ( var i = 0, ii = excelData[0].length; i < ii; i++ ) {
			var l_arr = excelData[0][i];
			excelDataStr += "<tr>";

			for ( var k = 0, kk = l_arr.length; k < kk; k++ ) {
				excelDataStr += "<th style='border: thin solid gray; background-color:#B2EBF4'";
				if(l_arr[k].colspan>1) excelDataStr += " colspan=" + l_arr[k].colspan;
				if(l_arr[k].rowspan>1) excelDataStr += " rowspan=" + l_arr[k].rowspan;
				excelDataStr += " >" + l_arr[k].colname + "</th>";
			}

			excelDataStr += "</tr>";
		}

		excelDataStr += "</thead><tbody>";

		for ( var i = 0, ii = excelData[1].length; i < ii; i++ ) {
			excelDataStr += "<tr>";

			for ( var j = 0, jj = excelData[1][i].length; j < jj; j++ ) {
				if ( excelData[1][i][j][0] === "text" ) {
					excelDataStr += "<td style='border: thin solid gray; background-color: " + excelData[1][i][j][2] + "; mso-number-format:\\@'>" + excelData[1][i][j][1] + "</td>";
				} else {
					excelDataStr += "<td style='border: thin solid gray; background-color: " + excelData[1][i][j][2] + "'>" + excelData[1][i][j][1] + "</td>";
				}
			}

			excelDataStr += "</tr>";
		}

		excelDataStr += "</tbody></table>";

		var blob     = new Blob( [excelDataStr], { type: 'text/xls;charset=utf-8;' } );
			//filename = filename != '' && filename != null ? filename+'.xls' : "ExcelExport.xls";

		filename = filename != '' && filename != null ? filename+'.xls' : mytop._MDI_MENU[mytop._CurMenuIndex].pgmname+'_'+_X.ToString(_X.GetSysDate(), 'yyyymmddhhmiss')+".xls";

		if ( navigator.msSaveBlob ) { // IE 10+
			navigator.msSaveBlob( blob, filename );
		}
		else {
			var link = document.createElement( "a" );

			if ( link.download !== undefined ) { // feature detection
			// Browsers that support HTML5 download attribute
			var url = URL.createObjectURL( blob );

			link.setAttribute( "href", url );
			link.setAttribute( "download", filename );
			link.style.visibility = 'hidden';
			document.body.appendChild( link );
			link.click();
			document.body.removeChild( link );
			}
		}

		this.EndMightyEvent( "ExcelExport" );
	};

//	//엑셀 가져오기
//	a_obj.ExcelImport = function (fileName) {
//		this.StartMightyEvent( "ExcelImport" );
//	  a_obj.GridRoot().excelCSVImport();
//		this.EndMightyEvent( "ExcelImport" );
//	};
//============================================================== Mighty-X5 Basic Method end

//============================================================== Mighty-X5 Data Control Method start
  //전체 컬럼의 옵션 반환
  a_obj.GetColumns = function () {
  	this.StartMightyEvent( "GetColumns", 5 );
  	return this.EndMightyEvent( "GetColumns", this._SlickGrid.getColumns() );
  };

  //전체 컬럼의 옵션 변경
  a_obj.SetColumns = function (columnsConfig) {
  	this.StartMightyEvent( "SetColumns", 5 );

  	this._SlickGrid.setColumns( columnsConfig );

  	this._FileMaxCnt  = null;
		this._FileMaxSize = null;
		this._FileSvrPath = "Y";

  	for ( var i = 0, ii = columnsConfig.length; i < ii; i++ ) {
  		if ( columnsConfig[i].fileInfo ) {
  			if ( columnsConfig[i].fileInfo.fileMaxCnt ) {
	  			this._FileMaxCnt = Math.max( this._FileMaxCnt, columnsConfig[i].fileInfo.fileMaxCnt );
	  		}

				if ( columnsConfig[i].fileInfo.fileMaxSize ) {
					this._FileMaxSize = Math.max( this._FileMaxSize, columnsConfig[i].fileInfo.fileMaxSize );
				}

				if ( columnsConfig[i].fileInfo.fileSvrPath === false ) {
					this._FileSvrPath = "N";
				}
  		}
		}

  	this.EndMightyEvent( "SetColumns" );
  };

  //컬럼의 옵션 반환
  a_obj.GetColumn = function (colName) {
  	this.StartMightyEvent( "GetColumn", 5 );
  	var colIdx = this.GetColumnIndexByName( colName );
  	return this.EndMightyEvent( "GetColumn", this.GetColumns()[ colIdx ] );
  };

  //컬럼의 옵션 변경
  a_obj.SetColumn = function (colName, prop, value) {
  	this.StartMightyEvent( "SetColumn", 5 );

  	var columnConfig = {};

  	columnConfig[ prop ] = value;

  	var columns = this.GetColumns();
  	var colIdx  = this.GetColumnIndexByName( colName );

  	$.extend( true, columns[ colIdx ], columnConfig );

		if ( prop === "fileInfo" ) {
			this._FileMaxCnt  = null;
			this._FileMaxSize = null;
			this._FileSvrPath = "Y";

			for ( var i = 0, ii = columns.length; i < ii; i++ ) {
				if ( columns[i].fileInfo ) {
					if ( columns[i].fileInfo.fileMaxCnt ) {
		  			this._FileMaxCnt = Math.max( this._FileMaxCnt, columns[i].fileInfo.fileMaxCnt );
		  		}

					if ( columns[i].fileInfo.fileMaxSize ) {
						this._FileMaxSize = Math.max( this._FileMaxSize, columns[i].fileInfo.fileMaxSize );
					}

					if ( columns[i].fileInfo.fileSvrPath === false ) {
						this._FileSvrPath = "N";
					}
				}
			}
		}

		this.SetColumns( columns );
  	this.EndMightyEvent( "SetColumn" );
  };

  //전체 행의 수 반환
	a_obj.RowCount = function() {
		this.StartMightyEvent( "RowCount", 3 );
		return this.EndMightyEvent( "RowCount", this._DataView.getLength() );
	};

	//전체 데이타 반환
	a_obj.GetData = function(commit) {
		this.StartMightyEvent( "GetData", 6 );
		this.GridCommit( !(commit === false) );
		return this.EndMightyEvent( "GetData", this._DataView.getItems() );
	};

	//전체 데이타 갱신
	a_obj.SetData = function(items) {
		this.StartMightyEvent( "SetData", 6 );
		this.Reset( true, !this.IsGroup() );
		this._DataView.setItems( items );

		if ( items.length ) {
			this._LastRowNumber = parseInt( items[ items.length - 1 ].id.split( "_" )[1], 10 );
		}

		this.EndMightyEvent( "SetData" );
	}

	//합계 값 가져오기
  a_obj.GetTotalValue = function (colName, expr) {
  	this.StartMightyEvent( "GetTotalValue", 6 );

  	var items = this.GetData();
  	var total = this._Plugins[ "FooterExpression" ].getTotalValue( colName, expr, items );

    return this.EndMightyEvent( "GetTotalValue", total );
  }

	//합계 값 셋팅
  a_obj.SetAllFooterData = function (colName, expr) {
  	this.StartMightyEvent( "SetAllFooterData", 6 );
  	this._Plugins[ "FooterExpression" ].setAllFooterData();
    this.EndMightyEvent( "SetAllFooterData" );
  }

	//filter 된 데이타를 가져온다.
	a_obj.GetFilteredData = function() {
		this.StartMightyEvent( "GetFilteredData", 6 );

		var data 					= this.GetData(),
				filteredItems = this._DataView.getFilteredItems(data);

		return this.EndMightyEvent( "GetFilteredData", filteredItems.rows );
	};

	//전체 데이타 삭제
	a_obj.ClearRows = function () {
		this.StartMightyEvent( "ClearRows", 4 );
	  this._DataView.setItems([]);
	  this.EndMightyEvent( "ClearRows" );
 	};

	//행의 데이타를 반환
	a_obj.GetRowData = function(rowIdx, commit) {
		this.StartMightyEvent( "GetRowData", 6 );
		if ( commit !== false && !this._IsFormatting ) {
			this.GridCommit();
		}
		return this.EndMightyEvent( "GetRowData", this._DataView.getItem( rowIdx - 1 ) );
	};

	//행의 데이타를 변경
	a_obj.SetRowData = function(rowIdx, newItem) {
		this.StartMightyEvent( "SetRowData", 6 );
		this._MightyEventObj.div.push( "SetRowData" );

		var item = this.GetRowData( rowIdx );

		// this._MightyEventObj.id = item.id;
		this._DataView.updateItem( item.id, $.extend(true, item, newItem, {"job": (item.job === "" || item.job === "N") ? "U" : item.job}) );

		x_GridRowSync(this, rowIdx);
		this.EndMightyEvent( "SetRowData" );
	};

	//셀의 값을 변경
	a_obj.SetCellData = function (rowIdx, colName, value, updateState) {
		this.StartMightyEvent( "SetCellData", 6 );
		this._MightyEventObj.div.push( "SetCellData" );

		var item = this.GetRowData( rowIdx );

		switch ( this.GetColumnTypeByName( colName ) ) {
			case "chardate":
				item[ colName ] = _X.ToString(value, "YYYYMMDD");
				break;

			default:
				item[ colName ] = value;
		}

		if ( !(updateState === false) ) {
			if ( item.job === "" || item.job === "N" ) {
				item.job = "U";
			}
		}

		if ( this._Share ) {
			switch ( this.GetColumnTypeByName( colName ) ) {
				case "checkbox":
					$(".detail_box").find( "#" + colName ).prop( "checked", value === "Y" );
					break;

				case "chardate":
					$(".detail_box").find( "#" + colName ).val( _X.ToString(value, "YYYYMMDD") );
					break;

				default:
					$(".detail_box").find( "#" + colName ).val( value );
			}
		}

		// this._MightyEventObj.id = item.id;
		this._MightyEventObj.cell = this.GetColumnIndexByName( colName );
		this._DataView.updateItem( item.id, item );
		this.EndMightyEvent( "SetCellData" );
  };

  //그리드에 데이터 행들을 추가 (a_flag true 일 때 기존자료를 삭제 후 추가)
	a_obj.AppendRowsData = function(datas, flag, focus, jobDiv, appendDefaultDatas){
		this.StartMightyEvent( "AppendRowsData", 6 );

		var data = this.GetData(),
				appendRowsDatas = [],
				deleteRowsDatas = [],
				defaultData,
				newRowItem,
				id;

		if ( flag ) {
			for ( var i = 0, ii = this._DeletedRows.length; i < ii; i++ ) {
				deleteRowsDatas.push( $.extend(true, {}, this._DeletedRows[i]) );
			}

			for ( var i = 0, ii = data.length; i < ii; i++ ) {
				deleteRowsDatas.push( $.extend(true, {}, data[i]) );
			}

			data = [];
		}

		for ( var i = 0, ii = datas.length; i < ii; i++ ) {
			defaultData = datas[i];

			delete defaultData.check;

			newRowItem = this.NewItem();
			id				 = "id_" + _X.LPad( ++this._LastRowNumber, 8, "0" );

			appendRowsDatas.push( $.extend( true, newRowItem, defaultData, { id: id, num: "", job: jobDiv || "I" }, appendDefaultDatas ) );
		}

		data = data.concat( appendRowsDatas );

		this.SetData( data );
		this._DeletedRows = deleteRowsDatas;

		if ( data.length && focus ) {
			this.SetRow( this.GetRowById( appendRowsDatas[0].id ), null, true );
		}

		this.EndMightyEvent( "AppendRowsData" );
	};

	//선택된 행의 번호 반환
	a_obj.GetRow = function() {
		this.StartMightyEvent( "GetRow", 5 );
		var rows = this._SlickGrid.getSelectedRows();
		if ( !rows || !rows.length ) {
			return this.EndMightyEvent( "GetRow", -1 );
		}
		return /* rows.length > 1 ? rows : */ this.EndMightyEvent( "GetRow", rows[0] + 1 );
	};

	//선택된 셀의 정보 반환
	a_obj.GetCell = function() {
		this.StartMightyEvent( "GetCell", 5 );
		var cell = this._SlickGrid.getActiveCell();

		if ( cell != null ) {
			cell = {
					"row": cell.row + 1,
					"cell": cell.cell
			};
		}

		return this.EndMightyEvent( "GetCell", cell );
	}

	//행을 선택
	a_obj.SetRow = function(rowIdx, colName, forceEdit, focus) {
		this.StartMightyEvent( "SetRow", 5 );
		var rowCnt = this.RowCount();

		if ( typeof colName === "number" ) {
			colName = this.GetColumnNameByIndex( colName );
		}

		rowIdx  = rowIdx < 0 ? 0 : rowIdx > rowCnt ? rowCnt : rowIdx;
		colName = colName || this._FirstColName;

		this._SlickGrid.gotoCell( rowIdx - 1, this.GetColumnIndexByName( colName ) || 0, forceEdit || false, false );
		this.EndMightyEvent( "SetRow" );
	};

	//인자 아이디의 행번호를 반환 (대상 grid)
	a_obj.GetRowById = function (rowId) {
		this.StartMightyEvent( "GetRowById", 5 );
		return this.EndMightyEvent( "GetRowById", this._DataView.getRowById( rowId ) + 1 );
	};

	//인자 아이디의 행번호를 반환 (대상 dataview)
	a_obj.GetIdxById = function (rowId) {
		this.StartMightyEvent( "GetIdxById", 5 );
		return this.EndMightyEvent( "GetIdxById", this._DataView.getIdxById( rowId ) + 1 );
	};

	//행 데이타의 기본형태를 반환
	a_obj.NewItem = function(){
		this.StartMightyEvent( "NewItem", 5 );
		var item = {};

		for ( var i = 0, ii = this._ColumnConfig.length; i < ii; i++ ){
			if ( this._ColumnConfig[i].default_value && this._ColumnConfig[i].default_value !== "" ) {
				item[ this._ColumnConfig[i].fieldName ] = this._ColumnConfig[i].default_value;
			}
			else {
				item[ this._ColumnConfig[i].fieldName ] = "";
			}
		}

		return this.EndMightyEvent( "NewItem", item );
	};

	//셀의 순수한 값을 반환
	a_obj.GetPureItem = function (rowIdx, colName, commit) {
		this.StartMightyEvent( "GetPureItem", 6 );
		var item = this.GetRowData( rowIdx, commit );

		if ( !item ) {
			return this.EndMightyEvent( "GetPureItem", null );
		}

		return this.EndMightyEvent( "GetPureItem", item[ colName ] );
	};

	//셀 값을 반환
	a_obj.GetItem = function (rowIdx, colName, commit) {
		this.StartMightyEvent( "GetItem", 6 );
		var item = this.GetRowData( rowIdx, commit );

		if ( !item ) {
			return this.EndMightyEvent( "GetItem", null );
		}

		var dataType = this.GetColumnTypeByName( colName ),
				rtValue  = item[ colName ];

		if ( rtValue == null || rtValue === "" ) {
			return this.EndMightyEvent( "GetItem", rtValue );
		}

		switch ( dataType ) {
			case "number"	 :
			case "numberf1":
			case "numberf2":
			case "numberf3":
			case "numberf4":
				rtValue = rtValue * 1;
				break;

			case "datetime":
				rtValue = toDateString( rtValue );
		}

		return this.EndMightyEvent( "GetItem", rtValue );
	};

	//셀의 값을 변경
	a_obj.SetItem = function (rowIdx, colName, value, updateState) {
		this.StartMightyEvent( "SetItem", 6 );
		this.SetCellData( rowIdx, colName, value, updateState );
		this.EndMightyEvent( "SetItem" );
	};

	//지정된 범위의 모든 행에서 해당 컬럼의 데이타를 배열로 반환
	a_obj.GetFieldValues = function(colName, startRow, endRow) {
		this.StartMightyEvent( "GetFieldValues", 6 );
		if ( !_X.ArrayContains( colName, this.GetColumnNamesForConfig() ).isEle ) {
			return this.EndMightyEvent( "GetFieldValues", null );
		}

		var items = this.GetData();
		startRow = ( startRow && startRow > 0 && startRow - 1 ) || 0;
		endRow   = ( endRow && endRow <= items.length && endRow ) || items.length;

		var fieldValues = [];

		for ( var i = startRow, ii = endRow; i < ii; i++ ) {
			fieldValues.push( items[i][ colName ] );
		}

		return this.EndMightyEvent( "GetFieldValues", fieldValues );
	};

	//지정된 범위의 모든 행에서 해당 컬럼의 데이타를 변경
	a_obj.SetFieldValues = function(colName, value, startRow, endRow) {
		this.StartMightyEvent( "SetFieldValues", 6 );
		if ( !_X.ArrayContains( colName, this.GetColumnNamesForConfig() ).isEle ) {
			return this.EndMightyEvent( "SetFieldValues", null );
		}

		var items = this.GetData();
		startRow = ( startRow && startRow > 0 && startRow - 1 ) || 0;
		endRow   = ( endRow && endRow <= items.length && endRow ) || items.length;

		for ( var i = startRow, ii = endRow; i < ii; i++ ) {
			items[i][ colName ] = value;

			if ( items[i].job === "" || items[i].job === "N" ) {
				items[i].job = "U";
			}
		}

		this._SlickGrid.invalidateAllRows();
		this.RenderProcess();
		this.EndMightyEvent( "SetFieldValues" );
	};

	//특정 컬럼의 최대값/최소값/총합/개수를 구할 때 사용(number형식 컬럼에서만 작동)(컬럼, 조건)
	a_obj.GetSummary = function(colName, div){
		this.StartMightyEvent( "GetSummary", 4 );
		var data = this.GetData();

		if ( this.RowCount() === 0 ) {
			//console.log("GetSummary msg: RowCount == 0");
			return this.EndMightyEvent( "GetSummary", 0 );
		}

		if ( typeof data[0][colName] == "undefined" ) {
			console.log("GetSummary msg: typeof data == undefined");
			return this.EndMightyEvent( "GetSummary", 0 );
		}

		if ( isNaN(data[0][colName]) ) {
			console.log("GetSummary msg: data isNaN");
			return this.EndMightyEvent( "GetSummary", 0 );
		}

		switch ( div.toUpperCase() ) {
			case "SUM":
				var sum = 0;
				for ( var i = 0, ii = data.length; i < ii; i++ ) {
					sum += Number( data[i][colName] );
				}
				return this.EndMightyEvent( "GetSummary", sum );

			case "MAX":
				var max = null;
				for ( var i = 0, ii = data.length; i < ii; i++ ) {
					if ( max == null || max < data[i][colName] ) {
						max = Number(data[i][colName]);
					}
				}
				return this.EndMightyEvent( "GetSummary", max );

			case "MIN":
				var min = null;
				for ( var i = 0, ii = data.length; i < ii; i++ ) {
					if ( min == null || min > data[i][colName] ) {
						min = Number(data[i][colName]);
					}
				}
				return this.EndMightyEvent( "GetSummary", min );

			case "AVG":
				var sum = 0;
				for ( var i = 0, ii = data.length; i < ii; i++ ) {
					sum += Number( data[i][colName] );
				}
				return this.EndMightyEvent( "GetSummary", sum / data.length || 1 );

			case "CNT":
			case "COUNT":
				return this.EndMightyEvent( "GetSummary", this.RowCount() );
		}

		return this.EndMightyEvent( "GetSummary", 0 );
	};

	//전체 컬럼의 이름을 반환 (대상: grid)
	a_obj.GetColumnNames = function () {
		this.StartMightyEvent( "GetColumnNames", 4 );
		var columns = this.GetColumns(),
				ids     = [];

		for ( var i = 1, ii = columns.length; i < ii; i++ ) {
			ids.push( columns[i].id );
		}

		return this.EndMightyEvent( "GetColumnNames", ids );
	};

	//전체 컬럼의 이름을 반환 (대상: _ColumnConfig)
	a_obj.GetColumnNamesForConfig = function() {
		this.StartMightyEvent( "GetColumnNamesForConfig", 4 );
		var columnNames = [];
		for ( var i = 0, ii = this._ColumnConfig.length; i < ii; i++ ) {
			columnNames.push( this._ColumnConfig[i].fieldName );
		}
		return this.EndMightyEvent( "GetColumnNamesForConfig", columnNames );
	};

	//컬럼의 이름 반환 (대상: Grid Columns)
	a_obj.GetColumnNameByIndex = function (colIdx) {
		this.StartMightyEvent( "GetColumnNameByIndex", 4 );
		if ( typeof colIdx !== "number" ) {
			alert("error: type of parameter is not a number.");
			return this.EndMightyEvent( "GetColumnNameByIndex" );
		}

		var columns = this.GetColumns();
		return this.EndMightyEvent( "GetColumnNameByIndex", columns && columns[ colIdx ] && columns[ colIdx ].id );
	};

	//컬럼의 이름 반환 (대상: _ColumnConfig)
	a_obj.GetColumnNameByIndexForConfig = function (colIdx) {
		this.StartMightyEvent( "GetColumnNameByIndexForConfig" );
		return this.EndMightyEvent( "GetColumnNameByIndexForConfig", this._ColumnConfig[ colIdx - 1 ].fieldName );
	};

	//컬럼의 번호를 반환 (대상: Grid Columns)
	a_obj.GetColumnIndexByName = function (colName) {
		this.StartMightyEvent( "GetColumnIndexByName", 4 );
		return this.EndMightyEvent( "GetColumnIndexByName", this._SlickGrid.getColumnIndex( colName ) );
	};

	//컬럼의 번호 반환 (대상: _ColumnConfig)
	a_obj.GetColumnIndexByNameForConfig = function (colName) {
		this.StartMightyEvent( "GetColumnIndexByNameForConfig", 4 );
		for ( var i = 0, ii = this._ColumnConfig.length; i < ii; i++ ) {
			if ( this._ColumnConfig[i].fieldName === colName ) {
				return this.EndMightyEvent( "GetColumnIndexByNameForConfig", i + 1 );
			}
		}
		return this.EndMightyEvent( "GetColumnIndexByNameForConfig", null );
	};

	//컬럼의 유형 반환
	a_obj.GetColumnTypeByName = function (colName) {
		this.StartMightyEvent( "GetColumnTypeByName", 4 );
		var columnIndex = this.GetColumnIndexByNameForConfig( colName );
		return this.EndMightyEvent( "GetColumnTypeByName", this._ColumnConfig[ columnIndex - 1 ] && this._ColumnConfig[ columnIndex - 1 ].styles && this._ColumnConfig[ columnIndex - 1 ].styles.dataType );
	};

	//변경된 데이타 반환 (sql 변환)
	a_obj.GetSQLData = function () {
		this.StartMightyEvent( "GetSQLData" );
		var rows = this.GetAllStateRows();

		if ( this._DeletedRows.length <= 0 && rows.updated.length <= 0 && rows.created.length <= 0 ) {
			return this.EndMightyEvent( "GetSQLData", "" );
		}

		var sqlDatas = this.subSystem + _Field_Separate + this.sqlFile + _Field_Separate + this.sqlKey;

		/*=== delete 구문 생성 ===*/
		for ( var i = 0, ii = this._DeletedRows.length; i < ii; i++ ) {
			sqlDatas += _Col_Separate + "D" + _Field_Separate + x_GetGridPureData(this, this._DeletedRows[i]);
		}

		/*=== Update 구문 생성 ===*/
		for ( var i = 0, ii = rows.updated.length; i < ii; i++ ) {
			sqlDatas += _Col_Separate + "U" + _Field_Separate + x_GetGridPureData(this, rows.updated[i]);
		}

		/*=== Insert 구문 생성 ===*/
		for ( var i = 0, ii = rows.created.length; i < ii; i++ ) {
			sqlDatas += _Col_Separate + "I" + _Field_Separate + x_GetGridPureData(this, rows.created[i]);
		}

		return this.EndMightyEvent( "GetSQLData", sqlDatas );
	};

	//변경된 행을 반환 (I, U)
	a_obj.GetAllStateRows = function() {
		this.StartMightyEvent( "GetAllStateRows" );
		var items = this.GetData(),
				rows  = {
					"created" : [],
					"updated" : []
				};

		for ( var i = 0, ii = items.length; i < ii; i++ ) {
			if ( items[i].job === "I" ) {
				rows.created.push( items[i] );
			}
			else if ( items[i].job === "U" ) {
				rows.updated.push( items[i] );
			}
		}

		return this.EndMightyEvent( "GetAllStateRows", rows );
	};

	//변경된 행을 반환 (I, U, D)
	a_obj.GetChangedData = function() {
		this.StartMightyEvent( "GetChangedData" );
		var rows  = this.GetAllStateRows(),
				cData = [],
				rowData;

		/*=== 추가 ===*/
		for ( var i = 0, ii = rows.created.length; i < ii; i++ ) {
			rowData 		 = {};
			rowData.job  = "I";
			rowData.idx	 = this.GetRowById( rows.created[i].id );
			rowData.data = rows.created[i];
			cData.push( rowData );
		}

		/*=== 변경 ===*/
		for ( var i = 0, ii = rows.updated.length; i < ii; i++ ) {
			rowData  	 = {};
			rowData.job  = "U";
			rowData.idx	 = this.GetRowById( rows.updated[i].id );
			rowData.data = rows.updated[i];
			cData.push( rowData );
		}

		/*=== 삭제 ===*/
		for ( var i = 0, ii = this._DeletedRows.length; i < ii; i++ ) {
			rowData  		 = {};
			rowData.job  = "D";
			rowData.idx	 = 0;
			rowData.data = this._DeletedRows[i];
			cData.push( rowData );
		}

		return this.EndMightyEvent( "GetChangedData", cData );
	};

	//행의 상태값 반환
	a_obj.GetRowState = function(rowIdx) {
		this.StartMightyEvent( "GetRowState" );
		rowIdx = rowIdx || this.GetRow();

		if ( !rowIdx ) {
			return this.EndMightyEvent( "GetRowState", null );
		}

		var item = this.GetRowData( rowIdx );
		return this.EndMightyEvent( "GetRowState", ( item && item.job ) || "N" );
	};

	//행의 상태값 변경 (지정값)
	a_obj.SetRowsState = function(a_rows, a_newState) {
		this.StartMightyEvent( "SetRowsState" );
		a_newState = a_newState || "N";

		if ( a_newState !== "I" && a_newState !== "U" && a_newState !== "N" ) {
			return this.EndMightyEvent( "SetRowsState" );
		}

		var items = this.GetData();

		if ( a_rows ) {
			for ( var i = 0, ii = a_rows.length; i < ii; i++ ) {
				items[ a_rows[i] - 1 ].job = a_newState;
			}
		}
		else {
			for ( var i = 0, ii = items.length; i < ii; i++ ) {
				items[i].job = a_newState;
			}
		}

		this.EndMightyEvent( "SetRowsState" );
	};

	//행의 상태값 변경 (old값 => new 값)
	a_obj.SetRowsStateOldNew = function(a_oldState, a_newState) {
		this.StartMightyEvent( "SetRowsStateOldNew" );
		a_newState = a_newState || "N";

		if ( ( a_newState !== "I" && a_newState !== "U" && a_newState !== "N" ) ||
				 ( a_oldState !== "I" && a_oldState !== "U" && a_oldState !== "N" ) ) {
			return this.EndMightyEvent( "SetRowsStateOldNew" );
		}

		var items = this.GetData();

		for ( var i = 0, ii = items.length; i < ii; i++ ) {
			if ( items[i].job === a_oldState ) {
				items[i].job = a_newState;
			}
		}

		this.EndMightyEvent( "SetRowsStateOldNew" );
	};

	//지정한 상태의 행의 수 반환
	a_obj.GetLengthByState = function(a_div) {
		this.StartMightyEvent( "GetLengthByState" );
		a_div = a_div || "A";

		var rows = this.GetAllStateRows();

		if( a_div === "I" ) { return this.EndMightyEvent( "GetLengthByState", rows.created.length ); }
		if( a_div === "U" ) { return this.EndMightyEvent( "GetLengthByState", rows.updated.length ); }
		if( a_div === "D" ) { return this.EndMightyEvent( "GetLengthByState", this._DeletedRows.length ); }
		if( a_div === "A" ) { return this.EndMightyEvent( "GetLengthByState", rows.created.length + rows.updated.length + this._DeletedRows.length ); }
		this.EndMightyEvent( "GetLengthByState" );
	};

	//현재 적용중인 그리드의 상태값 표기명을 가져온다.
  a_obj.GetGridState = function(a_state) {
  	this.StartMightyEvent( "GetGridState" );
  	this.EndMightyEvent( "GetGridState" );
  };

  //현재 적용중인 그리드의 상태값을 mighty의 표기명으로 가져온다.
  a_obj.GetMightyState = function(a_state) {
  	this.StartMightyEvent( "GetMightyState" );
  	this.EndMightyEvent( "GetMightyState" );
  };

	//전체 행의 상태값 초기화
	a_obj.ResetRowsStates = function() {
		this.StartMightyEvent( "ResetRowsStates" );
		this._DeletedRows.length = 0;

	  this.SetRowsState();

	 	this.EndMightyEvent( "ResetRowsStates" );
	};

	//데이타 변경여부 체크
	a_obj.IsDataChanged = function() {
		this.StartMightyEvent( "IsDataChanged" );
		var sqlData = this.GetSQLData();
		return this.EndMightyEvent( "IsDataChanged", ( sqlData != null && sqlData.length !== 0 && Object.keys(this._FileUploadInfo).length === 0 ) );
	};

	//셀의 콤보박스를 설정
	a_obj.SetCombo = function (colName, datas, abNull, code, bgColor) {
		this.StartMightyEvent( "SetCombo", 2 );
		var column   = this.GetColumn( colName ),
				c_values = [],
				c_labels = [];

		if ( column ) {
			if ( abNull ) {
				c_values.push("");
				c_labels.push("");
			}

			//수정(2017.01.19 KYY)
			if(datas.length > 0 && typeof datas[0] !== "undefined" ) {
				for ( var i = 0, ii = datas.length; i < ii; i++ ) {
					if ( datas[0].code ) {
						c_values.push( datas[i].code );
						c_labels.push( (code ? "[" + datas[i].code + "]" : "") + datas[i].label );
					}
					else if ( datas[0].CODE ) {
						c_values.push( datas[i].CODE );
						c_labels.push( (code ? "[" + datas[i].code + "]" : "") + datas[i].LABEL );
					}
					else {
						c_values.push( datas[i][0] );
						c_labels.push( (code ? "[" + datas[i].code + "]" : "") + datas[i][1] );
					}
				}
			}

    	column.comboData = {
    		"values"   : c_values,
    		"labels"   : c_labels,
    		"syncLabel": {},
    		"dropDownColor": []
    	};

    	for ( var i = 0, ii = column.comboData.values.length; i < ii; i++ ) {
    		column.comboData.syncLabel[ column.comboData.values[i] ] = column.comboData.labels[i];

    		if ( bgColor ) {
    			column.comboData.dropDownColor.push( column.comboData.values[i] );
    		}
    	}

    	switch( column.formatter ) {
    		case Slick.Formatters.Text:
    			column.formatter = Slick.Formatters.ComboBox;
    			break;

    		case Slick.Formatters.TextCenter:
    			column.formatter = Slick.Formatters.ComboBoxCenter;
    			break;

    		case Slick.Formatters.TextRight:
    			column.formatter = Slick.Formatters.ComboBoxRight;
    			break;

    		case Slick.Formatters.Tree:
    				column.formatter = Slick.Formatters.ComboBoxTree;
    	}
    }

    this.EndMightyEvent( "SetCombo" );
	};

	//셀의 콤보박스를 자동으로 설정
	a_obj.SetAutoComboOptions = function (colName, callbackFunc, abNull) {
		this.StartMightyEvent( "SetAutoComboOptions", 2 );
		var column = this.GetColumn( colName );

		if ( column ) {
			var getAutoComboOptions = function (args) {
					var datas = a_obj.CallFunction( args, callbackFunc ),
							c_values = [],
							c_labels = [];

					if ( abNull ) {
						c_values.push("");
						c_labels.push("");
					}

					for ( var i = 0, ii = datas.length; i < ii; i++ ) {
						if ( datas[0].code ) {
							c_values.push( datas[i].code );
							c_labels.push( datas[i].label );
						}
						else if ( datas[0].CODE ) {
							c_values.push( datas[i].CODE );
							c_labels.push( datas[i].LABEL );
						}
						else {
							c_values.push( datas[i][0] );
							c_labels.push( datas[i][1] );
						}
					}

		    	return {
		    		"values"   : c_values,
		    		"labels"   : c_labels
		    	};
			};
    }

    this.SetColumn( colName, "getAutoComboOptions", getAutoComboOptions );
    this.EndMightyEvent( "SetAutoComboOptions" );
	};

	//필수 입력 항목 체크
	a_obj.MustInputCheck = function() {
		this.StartMightyEvent( "MustInputCheck" );
		var rowsObj = this.GetAllStateRows(),
				rows		= rowsObj.created.concat( rowsObj.updated );

		for ( var i = 0, ii = rows.length; i < ii; i++ ) {
			for ( var j = 0, jj = this._ColumnConfig.length; j < jj; j++ ) {
				if ( this._ColumnConfig[j].mustInput || this._ColumnConfig[j].must_input === "Y" ) {

					var colName  = this._ColumnConfig[j].fieldName,
							fieldVal = this.GetItem( this.GetRowById( rows[i].id ), colName ),
							colType  = this._ColumnConfig[j].styles.dataType,
							isNumberType = colType == "number" || colType == "numberf1" || colType == "numberf2" || colType == "numberf3" || colType == "numberf4",
							fieldVal = isNumberType ? fieldVal == 0 ? "" : fieldVal : fieldVal;

					if ( fieldVal == null || fieldVal === "" ) {
						_X.MsgBox( "[" + this._ColumnConfig[j].header.text + "] 항목은 필수입력 항목입니다." );

						if ( this._Share ) {
							$("#" + colName).focus();
						}
						else if ( this._ColumnConfig[j].visible ) {
							var colIdx = this.GetColumnIndexByName( colName );

							this.SetRow( this.GetRowById( rows[i].id ), colName, true );
							this._SlickGrid.flashCell( this.GetRowById( rows[i].id ) - 1, colIdx );
						}

						return this.EndMightyEvent( "MustInputCheck", this.GetRowById( rows[i].id ) );
					}
				}
			}
		}
		return this.EndMightyEvent( "MustInputCheck", 0 );
	};

	//입력 길이 체크
	a_obj.ByteInputCheck = function() {
		this.StartMightyEvent( "ByteInputCheck" );
		var rowsObj = this.GetAllStateRows(),
				rows		= rowsObj.created.concat( rowsObj.updated );

		for ( var i = 0, ii = rows.length; i < ii; i++ ) {
			for ( var j = 0, jj = this._ColumnConfig.length; j < jj; j++ ) {
				//수정(2016.01.22 KYY)
				var maxLen = this._ColumnConfig[j].maxLength;
	  		if ( maxLen > 0) {
	  			var colName  = this._ColumnConfig[j].fieldName,
	  					fieldVal = this.GetItem( this.GetRowById( rows[i].id ), colName );
	  			if (!_X.ChkLen( String(fieldVal || ""), maxLen ) ) {
						_X.MsgBox( "[" + this._ColumnConfig[j].header.text + "] 항목의 문자열 길이가 \r허용된 길이보다 깁니다.[허용값:" + maxLen + ", 입력값:" + _X.ByteLen(fieldVal) + "]" );

						if ( this._Share ) {
							$("#" + colName).focus();
						}
						else if ( this._ColumnConfig[j].visible ) {
							var colIdx = this.GetColumnIndexByName( colName );

							this.SetRow( this.GetRowById( rows[i].id ), colName );
							this._SlickGrid.flashCell( this.GetRowById( rows[i].id ) - 1, colIdx );
						}

						return this.EndMightyEvent( "ByteInputCheck", this.GetRowById( rows[i].id ) );
					}
	  		}
		  }
		}
		return this.EndMightyEvent( "ByteInputCheck", 0 );
	};

	//정렬 비교 함수 생성
	a_obj.SetSortComparer = function (columns, multiColumn) {
		this.StartMightyEvent( "SetSortComparer" );
		/* multi */
		if ( multiColumn ) {
			this._SortInfo.sortColumns = columns;

			this._SortInfo.comparer = function (data1, data2) {
					for ( var i = 0, ii = columns.length; i < ii; i++ ) {
						var colName = columns[i].sortCol.field,
						    column  = a_obj.GetColumn( colName ),
							sign    = columns[i].sortAsc ? 1 : -1,
							value1  = typeof data1.rows != "undefined" ? data1.rows[0][colName] : data1[ colName ],
							value2  = typeof data2.rows != "undefined" ? data2.rows[0][colName] : data2[ colName ],
							result  = ( value1 == value2 ? 0 : ( value1 > value2 ? 1 : -1 ) ) * sign;

						if ( column && column.dataType.indexOf("number") !== -1 ) {
							result = ( value1 == value2 ? 0 : ( parseFloat(value1) > parseFloat(value2) ? 1 : -1 ) ) * sign;
						}

						if ( result != 0 ) {
							return result;
						}
					}
					return 0;
			};
		}
		/* single */
		else {
			this._SortInfo.sortColName = columns[0].sortCol.field;

			this._SortInfo.comparer = function (data1, data2) {
					var colName = columns[0].sortCol.field,
						column  = a_obj.GetColumn( colName ),
						sign    = columns[0].sortAsc ? 1 : -1,
						value1  = data1[ colName ],
						value2  = data2[ colName ];

					if ( column.dataType.indexOf("number") !== -1 ) {
						return ( value1 == value2 ? 0 : ( parseFloat(value1) > parseFloat(value2) ? 1 : -1 ) ) * sign;
					} else {
						return ( value1 == value2 ? 0 : ( value1 > value2 ? 1 : -1 ) ) * sign;
					}
			};
		}

		this.EndMightyEvent( "SetSortComparer" );
	};

	//정렬 실행
	a_obj.Sort = function (columns, render) {
		this.StartMightyEvent( "Sort" );
		this.SetSortComparer( columns, _SlickGrid_options.multiColumnSort );

		var item = this.GetRowData( this.GetRow() );
		this._DataView.sort( this._SortInfo.comparer );

		/* 이전에 선택된 Row로 재설정 */
		if ( item != null ) {
			var rowIdx = this.GetRowById( item.id );
			this.SetRow( rowIdx );
			this.SetSortOrder();
		}

		if ( !( render === false ) ) {
			this._ColumnFilters.items = [];
			this.RenderProcess();
		}

		this.EndMightyEvent( "Sort" );
	};

	//정렬 번호 생성
	a_obj.SetSortOrder = function ( indicator ) {
		this.StartMightyEvent( "SetSortOrder", 4 );

		if ( this._GroupData.grouping ) {
			return this.EndMightyEvent( "SetSortOrder" );
		}

		/* 기존 정렬 번호 삭제 */
		$(".slick-sort-indicator").each(
			function(i, e) {
				e.innerText = "";
			}
		);

		var sortColumns = this._SlickGrid.getSortColumns();

		if ( this.IsGroup() && this._GroupIndicator ) {
			var $columnHeaders = $(".ui-state-default.slick-header .slick-header-column").not(".slick-header-group-column").find(".slick-column-name"),
					colIdx;

			for ( var i = 0, ii = sortColumns.length; i < ii; i++ ) {
				colIdx = this._SlickGrid.getColumnIndex( sortColumns[i].columnId );
				$columnHeaders.eq( colIdx ).after( "<span class='slick-sort-indicator slick-sort-indicator-asc'>" + ( i + 1 ) + "</span>" );
			}
		}
		else {
			if ( sortColumns.length ) {
				var $sortColumns = $(".slick-sort-indicator-asc, .slick-sort-indicator-desc");

				/* 정렬 번호 생성 */
				$sortColumns.each( function(i, e) {
						for ( var i = 0, ii = sortColumns.length; i < ii; i++ ) {
							var column = a_obj.GetColumn( sortColumns[i].columnId );

							if ( column.name === e.offsetParent.innerText ) {
								e.innerText = i + 1;
								return;
							}
						}
				});
			}
		}

		this.EndMightyEvent( "SetSortOrder" );
	};

	//정렬 해제
	a_obj.ResetSort = function (render) {
		this.StartMightyEvent( "ResetSort", 4 );
		var resetComparer = function (data1, data2) {
			var value1 = data1.id,
				value2 = data2.id;

			return value1 == value2 ? 0 : value1 > value2 ? 1 : -1;
		};

		var item = this.GetRowData( this.GetRow() );
		this._DataView.sort( resetComparer, true );

		/* 이전에 선택된 Row로 재설정 */
		if ( item != null ) {
			var rowIdx = this.GetRowById( item.id );
			this.SetRow( rowIdx );
			this.SetSortOrder();
		}

		this._SortInfo = {};
		this._SlickGrid.setSortColumns( [] );
		this.SetSortOrder();

		if ( !( render === false ) ) {
			this.RenderProcess();
		}

		this.EndMightyEvent( "ResetSort" );
	};
//============================================================== Mighty-X5 Data Control Method end

//============================================================== Mighty-X5 tree Method start
	//Tree 형태의 자료구조를 생성 하여 캐시화 한다.
	a_obj.SetTreeDataCache = function (rowData) {
		this.StartMightyEvent( "SetTreeDataCache", 2 );
		if ( !rowData ) {
			rowData = this.GetData();
		}

		var parents,
				lineParent 		= null,
				treeLineInfo 	= {},
				treeLevelData = [],
				preLev 				= 0,
				lev 					= 0,
				preId 				= "",
				id 						= "";

		this._TreeData = {};

		for ( var i = 0, ii = rowData.length; i < ii; i++ ) {
			preLev = lev;
			lev 	 = rowData[i].LEV;

			if ( !treeLevelData[ lev - 1 ] ) {
				treeLevelData[ lev - 1 ] = [];
			}

			treeLevelData[ lev - 1 ].push( rowData[i] );

			preId = id;
			id 		= lev + _X.LPad( treeLevelData[ lev - 1 ].length, 4, "0" );

			/* 1 === nowLevel */
			if ( lev == 1 ) {
				parents 	 = [];
				lineParent = null;
				this.SetTreeData( 1, rowData, i, parents, false, id );
			}
			/* preLevel === nowLevel */
			else if ( preLev === lev ) {
				this.SetTreeData( 2, rowData, i, parents, true, id );

				/* firstTreeLine */
				if ( !lineParent && !treeLineInfo[ parents[0] ] ) {
					treeLineInfo[ parents[0] ] = lev;
					lineParent = parents[0];
				}
			}
			/* preLevel < nowLevel */
			else if ( preLev < lev ) {
				parents.push( preId );
				this.SetTreeData( 3, rowData, i, parents, true, id );
			}
			/* preLevel > nowLevel */
			else {
				/* level 차이: 1 */
				if ( parents.length == lev ) {
					parents.pop();
				}
				/* level 차이: 1 이상 */
				else {
					while ( parents.length >= lev ) {
						parents.pop();
					}
				}

				this.SetTreeData( 4, rowData, i, parents, true, id );

				if ( parents.length ) {
					if ( this._TreeData[ rowData[i].currentTreeData.parents ].child.length > 1 ) {
						/* firstTreeLine */
						if ( !lineParent && !treeLineInfo[ parents[0] ] ) {
							treeLineInfo[ parents[0] ] = lev;
							lineParent = parents[0];
						}
						else if ( treeLineInfo[ lineParent ] > lev ) {
							treeLineInfo[ lineParent ] = lev;
						}
					}
				}
			}
		}

		this._TreeData.treeLineInfo  = treeLineInfo;
		this._TreeData.treeLevelData = treeLevelData;
		this.EndMightyEvent( "SetTreeDataCache" );
	};

	/**
	 * Tree 형태의 자료구조를 생성
	 * currentTreeData = {
	 * 		id	   : treeLevelData[ rowData[i].LEV ].length,	행아이디
	 *		item   : rowData[i],         		 									행데이타
	 * 		child  : [ _TreeData[ treeId ] ], 								자식_TreeDatas
	 *		parent : this._TreeData[ parentsId ],     				부모행 treedata
	 *		parents: parents.join("|")        								부모행 _TreeData Key
	 *		lineParent: parents[1]
	 *    treeCurrentData: this._TreeData[ treeId ]         현재행 treedata
	 * }
	 */
	a_obj.SetTreeData = function(a_div, rowData, rowIdx, parents, isChild, id) {
		this.StartMightyEvent( "SetTreeData", 2 );
		var parentsId = parents.join( "|" ),
				treeId 		= parentsId + "|" + id;

		if ( a_div === 1 ) {
			treeId = id;
		}

		this._TreeData[ treeId ] = {};
		this._TreeData[ treeId ].id      = treeId;
		this._TreeData[ treeId ].item    = rowData[ rowIdx ];
		this._TreeData[ treeId ].child   = [];
		this._TreeData[ treeId ].parent  = this._TreeData[ parentsId ];
		this._TreeData[ treeId ].parents = parentsId;
		this._TreeData[ treeId ].lineParent = parents[0];

		if ( isChild ) {
			this._TreeData[ parentsId ].child.push( this._TreeData[ treeId ] );
		}

		rowData[ rowIdx ].currentTreeData = this._TreeData[ treeId ];
		rowData[ rowIdx ].treeData 				= this._TreeData;
		this.EndMightyEvent( "SetTreeData" );
	};

	//트리의 변화된 확장 축소를 표현
	a_obj.TreeCommitAndRender = function(a_row) {
		this.StartMightyEvent( "TreeCommitAndRender" );
		if ( a_row ) {
			this._ColumnFilters.items = [];
	    this._DataView.refresh();
	    this._SlickGrid.invalidateAllRows();
	    this.RenderProcess();
	    this.SetRow( a_row, null, false );
		}
		else {
			var cell = this.GetCell();
			var item;
			var rowId;

			if ( cell ) {
				item  = this.GetRowData( cell.row );
				rowId = item.id;
			}

	  	this._ColumnFilters.items = [];
	    this._DataView.refresh();
	    this._SlickGrid.invalidateAllRows();
	    this.RenderProcess();

	    if ( cell ) {
	    	var rowIdx = this.GetRowById( rowId );
	    	this.SetRow( rowIdx, cell.cell, false );
	    }
		}

    this.EndMightyEvent( "TreeCommitAndRender" );
	};

	//트리 확장 (확장 시 자식 트리도 확장)
	a_obj.TreeChildRecursive = function(treeChildData, collapsed) {
		this.StartMightyEvent( "TreeChildRecursive" );
		for ( var i = 0, ii = treeChildData.length; i < ii; i++ ) {
			if ( treeChildData[i].child.length ) {
				a_obj.TreeChildRecursive( treeChildData[i].child, collapsed );
			}

			treeChildData[i].item._collapsed = collapsed;
		}
		this.EndMightyEvent( "TreeChildRecursive" );
	};

	//트리 확장
	a_obj.TreeExpand = function(rowIdx, childRecursive) {
		this.StartMightyEvent( "TreeExpand" );
		var item = this.GetRowData( rowIdx );

		item._collapsed = false;

		if ( childRecursive ) {
			this.TreeChildRecursive( item.currentTreeData.child, false );
		}

		this.TreeCommitAndRender();
		this.EndMightyEvent( "TreeExpand" );
	};

	//트리 축소
	a_obj.TreeCollapse = function(rowIdx) {
		this.StartMightyEvent( "TreeCollapse" );
		var item = this.GetRowData( rowIdx );

		item._collapsed = true;

		this.TreeCommitAndRender();
		this.EndMightyEvent( "TreeCollapse" );
	};

	//트리 확장 (선택한 레벨 부터)
	a_obj.TreeExpandLevel = function(expandLevel) {
		this.StartMightyEvent( "TreeExpandLevel" );
		var levelData = this._TreeData.treeLevelData[ expandLevel ];

		if ( levelData ) {
			for ( var i = 0, ii = levelData.length; i < ii; i++ ) {
				levelData[i]._collapsed = true;
			}
		}

		this.TreeCommitAndRender();
		this.EndMightyEvent( "TreeExpandLevel" );
	};

	//트리 축소 (선택한 레벨 까지)(2017.06.287 KYY)
	a_obj.TreeCollapseLevel = function(collapseLevel) {
		this.StartMightyEvent( "TreeCollapseLevel" );
		a_obj.TreeCollapseAll();
		for(var lvl=collapseLevel -2; lvl>=0; lvl--) {
			var levelData = this._TreeData.treeLevelData[ lvl ];
			if ( levelData ) {
				for ( var i = 0, ii = levelData.length; i < ii; i++ ) {
					levelData[i]._collapsed = false;
				}
			}
		}
		this.TreeCommitAndRender();
		this.EndMightyEvent( "TreeCollapseLevel" );
	};

	//트리 전체 확장
	a_obj.TreeExpandAll = function() {
		this.StartMightyEvent( "TreeExpandAll" );
		var data = this.GetData();

	  for ( var i = 0, ii = data.length; i < ii; i++ ) {
	  	data[i]._collapsed = false;
	  }

	  this.TreeCommitAndRender();
	  this.EndMightyEvent( "TreeExpandAll" );
	};

	//트리 전체 축소
	a_obj.TreeCollapseAll = function() {
		this.StartMightyEvent( "TreeCollapseAll" );
		var data = this.GetData();

	  for ( var i = 0, ii = data.length; i < ii; i++ ) {
	  	data[i]._collapsed = true;
	  }

		this.TreeCommitAndRender();
		this.EndMightyEvent( "TreeCollapseAll" );
	};

	//부모의 행 번호를 반환
	a_obj.GetParent = function (rowIdx) {
		this.StartMightyEvent( "GetParent" );
		var currentItem = this.GetRowData( rowIdx );

		if ( currentItem ) {
			var parentTreeItem = this._TreeData[ currentItem.currentTreeData.parents ];
			if ( parentTreeItem ) {
				return this.EndMightyEvent( "GetParent", this.GetRowById( parentTreeItem.item.id ) );
			}
		}

		return this.EndMightyEvent( "GetParent", -1 );
	};

	//모든 부모의 행 번호를 반환
	a_obj.GetParents = function (rowIdx) {
		this.StartMightyEvent( "GetParents" );
		var currentItem    = this.GetRowData( rowIdx );
		var parentTreeItem = this._TreeData[ currentItem.currentTreeData.parents ];
		var parents        = [];

		while ( parentTreeItem ) {
			parents.push( this.GetRowById( parentTreeItem.item.id ) );

			currentItem    = this.GetRowData( parents[parents.length - 1] );
			parentTreeItem = this._TreeData[ currentItem.currentTreeData.parents ];
		}

		return this.EndMightyEvent( "GetParents", parents );
	};

	//부모를 변경
	a_obj.ChangeParent = function(rowIdx, parentRowIdx) {
		this.StartMightyEvent( "ChangeParent" );
		if ( rowIdx == parentRowIdx ) {
			return this.EndMightyEvent( "ChangeParent" );
		}

		var descendantsIdx = this.GetDescendants( rowIdx );

		//현재행의 자식행들로는 이동불가
		if ( descendantsIdx.indexOf(parentRowIdx) + 1 ) {
			return this.EndMightyEvent( "ChangeParent" );
		}

		var currentItem = this.GetRowData( rowIdx ),
		    parentItem  = this.GetRowData( parentRowIdx );

		if ( currentItem && parentItem ) {
			var addLev 			= parentItem.LEV - currentItem.currentTreeData.parent.item.LEV, //증감레벨
					data 				= this.GetData(),														 	 //전체데이타
					descendants = this.GetDescendants( rowIdx, true ),				 //현재행 자식행들
					newRow      = this.GetRowById( parentItem.id ), 					 //변경될 행번호
					parentChild = parentItem.currentTreeData.child,					 	 //변경부모의 자식
					parentLastChild;																					 //변경부모의 마지막자식

			//currentItem.PARENT = parentItem.ID;                          //부모변경
			currentItem.LEV 	 = parseInt( currentItem.LEV, 10 ) + addLev; //레벨변경

			if ( parentChild.length ) {
				parentLastChild = parentChild[ parentChild.length - 1 ];
			}

			if ( parentLastChild ) {
				var parentLastChildRowIdx 		 = this.GetRowById( parentLastChild.item.id ),
						parentLastChildDescendants = this.GetDescendants( parentLastChildRowIdx );

				if ( !parentLastChildDescendants.length || (parentLastChildDescendants.length === 1 && parentLastChildDescendants.indexOf( rowIdx ) + 1) ) {
					newRow = parentLastChildRowIdx;
				}
				else {
					for ( var i = parentLastChildDescendants.length - 1; i >= 0; i-- ) {
						//마지막 자식행의 번호를 이동할 행번호로 지정 (마지막 자식이 현재 자신의 행 일 경우 제외)
						if ( parentLastChildDescendants[i] !== rowIdx ) {
							newRow = parentLastChildDescendants[i];
							break;
						}
					}
				}
			}

			if ( rowIdx < newRow ) {
				newRow = newRow - descendants.length - 1;
			}

			data.splice( rowIdx - 1, descendants.length + 1 );
			data.splice( newRow++, 0, currentItem );

			for ( var i = 0, ii = descendants.length; i < ii; i++ ) {
				descendants[i].LEV = parseInt( descendants[i].LEV, 10 ) + addLev;
				data.splice( newRow + i, 0, descendants[i] );
			}
		}

		this.SetTreeDataCache( data );

		try {
			this.SetData( data );
		} catch (err) {
  		_X.MsgBox(err);
  	}

  	this.TreeCommitAndRender();
  	this.SetRow( newRow, null, false );
  	this.EndMightyEvent( "ChangeParent" );
	};

	//현재 행의 레벨을 1레벨 올린다
	a_obj.TreeLevelUp = function () {
		this.StartMightyEvent( "TreeLevelUp" );
		var rowIdx     = this.GetRow(),
				rowData    = this.GetRowData( rowIdx ),
				parentData = rowData.currentTreeData.parent;

		if ( parentData ) {
			upLevelParent = parentData.parent;

			if ( upLevelParent ) {
				this.ChangeParent( rowIdx, this.GetRowById( upLevelParent.item.id ) );
			}
		}
		this.EndMightyEvent( "TreeLevelUp" );
	};

	//자식행들의 행번호 또는 행데이타를 반환
	a_obj.GetChildren = function (rowIdx, returnTypeObject) {
		this.StartMightyEvent( "GetChildren" );
		var currentItem = this.GetRowData( rowIdx ),
				children    = [];

		if ( currentItem ) {
			var child = currentItem.currentTreeData.child;
			for ( var i = 0, ii = child.length; i < ii; i++ ) {
				if ( returnTypeObject ) {
					children.push( child[i].item );
				} else {
					children.push( this.GetRowById( child[i].item.id ) );
				}
			}
		}

		return this.EndMightyEvent( "TreeLevelUp", children );
	};

	//모든 자식행들의 행번호 또는 행데이타를 반환
	a_obj.GetDescendants = function(rowIdx, returnTypeObject) {
		this.StartMightyEvent( "GetDescendants" );
		var currentItem = this.GetRowData( rowIdx ),
				descendants = [];

		if ( currentItem ) {
			var getDescendants = function (treeItem) {
					for ( var i = 0, ii = treeItem.child.length; i < ii; i++ ) {
						if ( treeItem.child[i].item.currentTreeData.child.length ) {
							if ( returnTypeObject ) {
								descendants.push( treeItem.child[i].item );
							} else {
								descendants.push( a_obj.GetRowById( treeItem.child[i].item.id ) );
							}

							getDescendants( treeItem.child[i].item.currentTreeData );
						}
						else {
							if ( returnTypeObject ) {
								descendants.push( treeItem.child[i].item );
							} else {
								descendants.push( a_obj.GetRowById( treeItem.child[i].item.id ) );
							}
						}
					}
			};

			getDescendants( currentItem.currentTreeData );
		}

		if ( returnTypeObject ) {
			descendants.sort( function (a, b) {
					var aIdx = a_obj._DataView.getIdxById( a.id ),
							bIdx = a_obj._DataView.getIdxById( b.id );
					return aIdx === bIdx ? 0 : aIdx > bIdx ? 1 : -1;
			});
		} else {
			descendants.sort( function (a, b) {
					return a === b ? 0 : a > b ? 1 : -1;
			});
		}

		return this.EndMightyEvent( "GetDescendants", descendants );
	};

	//자식의 행 수를 반환
	a_obj.GetChildCount = function(rowIdx) {
		this.StartMightyEvent( "GetChildCount" );
		var currentItem = this.GetRowData( rowIdx );

		if ( currentItem ) {
			return this.EndMightyEvent( "GetChildCount", currentItem.currentTreeData.child.length );
		}

		return this.EndMightyEvent( "GetChildCount", 0 );
	};

	//현재 레벨의 행을 추가
	a_obj.AddCurrent = function (rowIdx) {
		this.StartMightyEvent( "AddCurrent" );
		this._MightyEventObj.div.push( "AddCurrent" );

		var currentItem,
				parentItem,
				lastLevelData;

		//수정(2017.01.24 KYY)
		if(rowIdx <= 0){
			var treeLevelData = this._TreeData.treeLevelData[0];
			if(typeof treeLevelData != "undefined") {
				lastLevelData = treeLevelData[ treeLevelData.length - 1 ].currentTreeData;
			}
		}
		else{
			//하위레벨이 있으면
			currentItem = this.GetRowData( rowIdx );
			lastLevelData = currentItem.currentTreeData;
		}

		if ( lastLevelData ) {
			rowIdx = this.GetRowById( lastLevelData.item.id );

			var descendants = this.GetDescendants( rowIdx ),
					newRowIdx;

			if ( descendants.length ) {
				newRowIdx = descendants[ descendants.length - 1 ] + 1;
			} else {
				newRowIdx = rowIdx + 1;
			}

			this.InsertRow( newRowIdx, { LEV: parseInt(lastLevelData.item.LEV, 10) } );
		} else {
			//최초 입력시 1레벨 추가(2017.01.24 KYY)
			newRowIdx = 1;
			this.InsertRow( newRowIdx, { LEV: 1 } );
		}

		return this.EndMightyEvent( "AddCurrent", newRowIdx );
	};

	//자식 행을 추가
	a_obj.AddChild = function (rowIdx) {
		this.StartMightyEvent( "AddChild" );
		this._MightyEventObj.div.push( "AddChild" );

		var currentItem = this.GetRowData( rowIdx );

		if ( currentItem ) {
			var descendants = this.GetDescendants( rowIdx ),
					newRowIdx;

			if ( descendants.length ) {
				newRowIdx = descendants[ descendants.length - 1 ] + 1;
			} else {
				newRowIdx = rowIdx + 1;
			}

			this.InsertRow( newRowIdx, { LEV: parseInt(currentItem.LEV, 10) + 1 } );
		}

		return this.EndMightyEvent( "AddChild", newRowIdx );
	};
//============================================================== Mighty-X5 tree Method end

//============================================================== Mighty-X5 group Method start
	//그롭 포멧 생성
	a_obj.SetGroupFormatter = function(column) {
		this.StartMightyEvent( "SetGroupFormatter", 2 );

		switch ( column.group.expression.toUpperCase() ) {
			case "TEXT":
				column.groupTotalsFormatter = Slick.Formatters.GroupText;
				break;

			case "AVG":
			case "MIN":
			case "MAX":
			case "SUM":
			case "CNT":
			case "COUNT":
			case "CUSTOM":
				column.groupTotalsFormatter = Slick.Formatters.GroupTotal;
		}

		this.EndMightyEvent( "SetGroupFormatter" );
	};

	//그룹 함수 생성
	a_obj.SetGroupAggregator = function (column) {
		this.StartMightyEvent( "SetGroupAggregator", 2 );

		switch ( column.group.expression.toUpperCase() ) {
			case "AVG":
				this._GroupData.aggregators.push( new Slick.Data.Aggregators.Avg( column.field, column.dataType ) );
				break;

			case "MIN":
				this._GroupData.aggregators.push( new Slick.Data.Aggregators.Min( column.field, column.dataType ) );
				break;

			case "MAX":
				this._GroupData.aggregators.push( new Slick.Data.Aggregators.Max( column.field, column.dataType ) );
				break;

			case "SUM":
				this._GroupData.aggregators.push( new Slick.Data.Aggregators.Sum( column.field, column.dataType ) );
				break;

			case "CNT":
			case "COUNT":
				if ( !this._GroupData.countIf ) {
					this._GroupData.countIf = {};
				}
				this._GroupData.aggregators.push( new Slick.Data.Aggregators.Count( column.field, column.dataType, this._GroupData.countIf ) );

			case "CUSTOM":
				if ( !this._GroupData.custom ) {
					this._GroupData.custom = {};
				}
				this._GroupData.aggregators.push( new Slick.Data.Aggregators.Custom( column.field, this._GroupData.custom ) );
		}

		this.EndMightyEvent( "SetGroupAggregator" );
	};

	//그룹 정보 생성
	a_obj.SetGroupInfos = function (colNames) {
		this.StartMightyEvent( "SetGroupInfos", 2 );
		this._GroupData.firstGroupInfo = [];
		this._GroupData.aggregators 	 = [];

		var columns = this._GroupData.preColumns;

		for ( var i = 0, ii = columns.length; i < ii; i++ ) {
			if ( columns[i].group ) {
				if ( columns[i].group.expression ) {
					this.SetGroupAggregator( columns[i] );
					this.SetGroupFormatter( columns[i] );
				}
				else if ( columns[i].group.text ) {
					columns[i].group.expression = "TEXT";
					this.SetGroupFormatter( columns[i] );
				}
			}
		}

		var getGroupInfo = function (column) {
				return {
						"getter"	 : column.field,
				    "formatter": function (g) {
				      	return "<span class='title-column'>" + column.name + ":</span>" +
				      				 "<span class='title-value'>"  + g.value 		 + "</span>" +
				      				 "<span class='title-count'>(" + g.count 		 + " items)</span>";
				    },
				    "aggregators": a_obj._GroupData.aggregators,
				    "aggregateCollapsed"	 : false,
				    "lazyTotalsCalculation": true,
				    "comparer": a_obj._SortInfo.comparer
				};
		};

		for ( var i = 0, ii = colNames.length; i < ii; i++ ) {
			this._GroupData.firstGroupInfo.push( getGroupInfo( this.GetColumn(colNames[i]) ) );
		}

		this.EndMightyEvent( "SetGroupInfos" );
	};

	//그룹 생성
	a_obj.SetGroup = function(colNames, reOrder, sortColumns, indicator) {
		this.StartMightyEvent( "SetGroup", 2 );
		if ( typeof colNames === "string" ) {
			colNames = [ colNames ];
		}
		else if ( colNames == null ) {
			this._DataView.setGrouping( [] );
			this._SlickGrid.setSortColumns( this._GroupData.preSortColumns );
			this.ResetSort();

			this._GroupData.preColumns[0].groupSeq = false;
			this.SetColumns( this._GroupData.preColumns );

			this._GroupData = $.extend( true, {}, { "aggregators": this._GroupData.aggregators } );

			return this.EndMightyEvent( "SetGroup" );
		}

		this._DataView.beginUpdate();
		this._GroupData.grouping       = true;
		this._GroupData.preSortColumns = this._SlickGrid.getSortColumns();
		this._GroupData.preColumns     = this.GetColumns();

		/* sort 정보 생성 */
		if ( typeof sortColumns != "function" ) {
			var sortCols = [];

			if ( sortColumns && sortColumns[0] && sortColumns[0].sortCol ) {
				sortCols = sortColumns;
			}
			else {
				for ( var i = 0, ii = colNames.length; i < ii; i++ ) {
					sortCols.push({
							"sortAsc": true,
							"sortCol": {
									"field": colNames[i]
							}
					});
				}
			}

			this.SetSortComparer( sortCols, _SlickGrid_options.multiColumnSort );
		}
		else {
			this._SortInfo.comparer = sortColumns;
		}

		this.SetGroupInfos( colNames );
		this._DataView.setGrouping( this._GroupData.firstGroupInfo );

		/* grid sort 정보 생성 */
		if ( typeof sortColumns != "function" ) {
			var column,
				columns         = this.GetColumns(),
				headerMenu      = x_GetColumnHeaderMenu( "filter" ),
				sortColumnsInfo = [],
				reOrderColumns  = [ columns[0] ];

			if ( this._Plugins.CheckboxSelector ) {
				reOrderColumns.push( columns[1] );
			}

			for ( var i = 0, ii = colNames.length; i < ii; i++ ) {
				var sortInfo = {
	  				"columnId": colNames[i],
						"sortAsc" : true
				};

				sortColumnsInfo.push( sortInfo );

				column = columns[ this.GetColumnIndexByName(colNames[i]) ];

				/* 컬럼 순서 변경 */
				if ( !(reOrder === false) && column.headerNames.length === 1 ) {
					reOrderColumns.push( this.GetColumn( colNames[i] ) );
				}
			}

			/* 컬럼 정보 변경 */
			columns[0].groupSeq = true;

			for ( var i = 0, ii = columns.length; i < ii; i++ ) {
				columns[i].sortable = false;

				if ( columns[i].id !== "sel" && columns[i].id !== "_checkbox_selector" ) {
					columns[i].header   = { menu: { items: headerMenu	}	};
				}

				/* 컬럼 순서 변경 */
				if ( !(reOrder === false) ) {
					if ( reOrderColumns.indexOf( columns[i] ) === -1 ) {
						reOrderColumns.push( columns[i] );
					}
				}
			}

			this._SlickGrid.setSortColumns( sortColumnsInfo );
			this.SetColumns( !(reOrder === false) ? reOrderColumns : columns );

			this._FirstColName 		 = null;
			this._FirstEditColName = null;
			x_SetFirstColumn( a_obj );

			this._GroupIndicator = indicator !== false;
		}

		this._GroupData.grouping = false;
		this._DataView.endUpdate();

		if ( typeof sortColumns != "function" ) {
			this.Sort( sortCols );
		}

		this.SetRow( this.GetGroupItemRow(1), null, false );

		this.EndMightyEvent( "SetGroup" );
	};

	//Share정보 재설정
	a_obj.ResetSyncInfo = function () {
		a_obj._SyncInfoSet = false;
		a_obj._SyncObject  = [];
		a_obj._SyncObjectJ = [];
		x_GridSetSyncInfo(a_obj);
	}

	//그룹 여부
	a_obj.IsGroup = function () {
		this.StartMightyEvent( "IsGroup", 4 );
		return this.EndMightyEvent( "IsGroup", this._DataView.getGrouping().length > 0 );
	};

	//그룹의 선택가능한 행번호를 반환
	a_obj.GetGroupItemRow = function (rowIdx, direction) {
		this.StartMightyEvent( "GetGroupItemRow", 4 );
		var rowCount = this.RowCount();
		rowIdx = rowIdx < 1 ? 1 : rowIdx > rowCount ? rowCount : rowIdx;

		var getGroupItemRow = function (row) {
			var item_ = a_obj.GetRowData( row, false );

			if ( item_ && item_ instanceof Slick.Group ) {
				if ( !item_.collapsed ) {
					item_ = item_.rows && item_.rows[0];
				}
				else {
					if ( direction === 0 ) {
						item_ = getGroupItemRow( --row );
					} else {
						item_ = getGroupItemRow( ++row );
					}
				}
			}
			else if ( item_ && item_ instanceof Slick.GroupTotals ) {
				if ( direction === 0 ) {
					item_ = getGroupItemRow( --row );
				} else {
					item_ = item_.group.rows[0];
				}
			}

			return item_;
		}

		var item = getGroupItemRow( rowIdx );

		if ( item ) {
			return this.EndMightyEvent( "SetGroupFootVisible", this.GetRowById( item.id ) );
		}

		return this.EndMightyEvent( "SetGroupFootVisible", rowIdx );
	};

	//그룹 확장
	a_obj.ExpandGroup = function(level) {
		this.StartMightyEvent( "ExpandGroup" );
		this._DataView.expandAllGroups( level );
		this.EndMightyEvent( "ExpandGroup" );
	};

	//그룹 축소
	a_obj.CollapseGroup = function(level) {
		this.StartMightyEvent( "CollapseGroup" );
		this._DataView.collapseAllGroups( level );
		this.EndMightyEvent( "CollapseGroup" );
	};

	//group footer visible 설정
	a_obj.SetGroupFootVisible = function (colNames, flag) {
		this.StartMightyEvent( "SetGroupFootVisible", 2 );

		if ( this.IsGroup() ) {
			var visibleInfo  = this._GroupData.firstGroupInfo,
					visibleInfo2 = [];

			/* 전체 컬럼 적용 */
			if ( colNames == null ) {
				for ( var i = 0, ii = visibleInfo.length; i < ii; i++ ) {
					visibleInfo2[i] = $.extend( true, {}, visibleInfo[i] );

					if ( flag === false ) {
						visibleInfo2[i].aggregators = [];
					}
				}
			}
			/* 선택 컬럼 적용 */
			else {
				if ( typeof colNames === "string" ) {
					colNames = [ colNames ];
				}

				var lastVisibleInfo = this._GroupData.lastGroupInfo || visibleInfo;

				for ( var prop in colNames ) {
					if ( colNames.hasOwnProperty( prop ) ) {
						for ( var i = 0, ii = lastVisibleInfo.length; i < ii; i++ ) {
							visibleInfo2[i] = $.extend( true, {}, lastVisibleInfo[i] );

							if ( colNames[ prop ] === lastVisibleInfo[i].getter ) {
								if ( flag === false ) {
									visibleInfo2[i].aggregators = [];
								} else {
									visibleInfo2[i].aggregators = visibleInfo[i].aggregators;
								}
							}
						}
					}
				}
			}

			this._GroupData.lastGroupInfo = visibleInfo2;
			this._DataView.setGrouping( visibleInfo2 );
			this.RenderProcess();
		}

		this.EndMightyEvent( "SetGroupFootVisible" );
	}

	//count 그룹함수 사용 시 조건 부여
	a_obj.SetCountIf = function (colName, callbackFunc, refresh) {
		this.StartMightyEvent( "SetCountIf", 2 );
		if ( !this._GroupData.countIf ) {
			this._GroupData.countIf = {};
		}
		this._GroupData.countIf[ colName ] = callbackFunc;

		if ( refresh === true ) {
			this._DataView.refresh();
		}
		this.EndMightyEvent( "SetCountIf" );
	};

	//그룹함수 custom 설정
	a_obj.SetGroupCustom = function (colName, callbackFunc, refresh) {
		this.StartMightyEvent( "SetGroupCustom", 2 );
		if ( !this._GroupData.custom ) {
			this._GroupData.custom = {};
		}
		this._GroupData.custom[ colName ] = callbackFunc;

		if ( refresh === true ) {
			this._DataView.refresh();
		}
		this.EndMightyEvent( "SetGroupCustom" );
	};

	//그룹 셀 마스크 설정
	a_obj.SetGroupMaskValue = function (colName, callbackFunc, refresh) {
		this.StartMightyEvent( "SetGroupMaskValue", 2 );
		var getGroupMaskValue = function (args) {
				return a_obj.CallFunction( args, callbackFunc );
		};

		this.SetColumn( colName, "getGroupMaskValue", getGroupMaskValue );

		if ( refresh === true ) {
			this._DataView.refresh();
		}
		this.EndMightyEvent( "SetGroupMaskValue" );
	};
//============================================================== Mighty-X5 group Method end

//============================================================== Mighty-X5 cssStyle Method start
	//셀의 좌측부터 지정한 수만큼의 컬럼을 스크롤바의 이벤트로 부터 고정
	a_obj.SetFixed = function(fixCount) {
		this.StartMightyEvent( "SetFixed" );

    var columns = this.GetColumns();

    if ( columns[1] && columns[1].id === "_checkbox_selector" ) {
    	fixCount++;
    }

    for ( var i = 0, ii = fixCount + 1; i < ii; i++ ) {
    	columns[i].fixed = true;
    }

    this.SetColumns( columns );

		this.EndMightyEvent( "SetFixed" );
	};

	//cssRules 생성
  a_obj.MakeCssRules = function (a_cssKey, a_rules) {
  	this.StartMightyEvent( "MakeCssRules", 2 );
  	var rules = [ "." + a_cssKey + " {" ];

		for ( var prop in a_rules ) {
			if ( a_rules.hasOwnProperty( prop ) ) {
				rules.push( prop + ": " + a_rules[ prop ] + " !important;" );
			}
		}

		rules.push( "}" );
		return this.EndMightyEvent( "MakeCssRules", rules.join(" ") );
  };

  //전체 스타일시트 삭제
  a_obj.RemoveAllStyle = function () {
  	this.StartMightyEvent( "RemoveAllStyle", 2 );
  	for ( var prop in this._Stylesheet ) {
  		if ( !this._Stylesheet.hasOwnProperty( prop ) ) {
				continue;
			}

  		this._SlickGrid.removeCellCssStyles( prop );
			this._Stylesheet[ prop ].remove();
			delete this._Stylesheet[ prop ];
  	}
  	this.EndMightyEvent( "RemoveAllStyle" );
  };

  //스타일시트 삭제
  a_obj.RemoveStyles = function (cssKey) {
  	this.StartMightyEvent( "RemoveStyles", 2 );
		if ( this._Stylesheet[ cssKey ] ) {
			this._SlickGrid.removeCellCssStyles( cssKey );
			this._Stylesheet[ cssKey ].remove();
			delete this._Stylesheet[ cssKey ];
		}
		this.EndMightyEvent( "RemoveStyles" );
	};


	//스타일시트 생성 및 cssRules 추가
	a_obj.CreateStylesheet = function (a_cssKey, a_cssRules) {
		this.StartMightyEvent( "CreateStylesheet", 2 );
		this._Stylesheet[ a_cssKey ] = $("<style type='text/css' rel='stylesheet' />").appendTo( $("head") );

		if ( this._Stylesheet[ a_cssKey ][0].styleSheet ) { // IE
      this._Stylesheet[ a_cssKey ][0].styleSheet.cssText = a_cssRules;
    } else {
      this._Stylesheet[ a_cssKey ][0].appendChild( document.createTextNode( a_cssRules ) );
    }
    this.EndMightyEvent( "CreateStylesheet" );
	};

	//스타일변경 전처리 (처리 객체 반환)
	a_obj.SetStylePreprocessing = function (cssKey, a_rules, isColNames) {
		this.StartMightyEvent( "SetStylePreprocessing", 2 );
		var setCssObj = {};

		setCssObj.addCssObj  = {};
		setCssObj.cssPropObj = {};
		setCssObj.cssRule		 = this.MakeCssRules( cssKey, a_rules );

		if ( isColNames ) {
			setCssObj.colNames = this.GetColumnNames();
		}

		this.RemoveStyles( cssKey );
		this.CreateStylesheet( cssKey, setCssObj.cssRule );

		return this.EndMightyEvent( "SetStylePreprocessing", setCssObj );
	};

	//행의 스타일 변경
	a_obj.SetRowStyle = function (rowIdx, a_rules) {
		this.StartMightyEvent( "SetRowStyle" );
		var cssKey    = this.id + "-r" + rowIdx + "-rowstyle",
		    setCssObj = this.SetStylePreprocessing( cssKey, a_rules, true );

		for ( var i = 0, ii = setCssObj.colNames.length; i < ii; i++ ) {
			setCssObj.cssPropObj[ setCssObj.colNames[i] ] = cssKey;
		}

		setCssObj.addCssObj[ rowIdx - 1 ] = setCssObj.cssPropObj;
		this._SlickGrid.addCellCssStyles( cssKey, JSON.parse( JSON.stringify(setCssObj.addCssObj) ) );
		this.EndMightyEvent( "SetRowStyle" );
	};

	//컬럼의 스타일 변경
	a_obj.SetColumnStyle = function (colName, a_rules) {
		this.StartMightyEvent( "SetColumnStyle" );
		var cssKey    = this.id + "-" + colName + "-columnstyle",
				setCssObj = this.SetStylePreprocessing( cssKey, a_rules );

		setCssObj.cssPropObj[ colName ] = cssKey;

		for ( var i = 0, ii = this.RowCount(); i < ii; i++ ) {
			setCssObj.addCssObj[i] = setCssObj.cssPropObj;
		}

		this._SlickGrid.addCellCssStyles( cssKey, JSON.parse( JSON.stringify(setCssObj.addCssObj) ) );
		this.EndMightyEvent( "SetColumnStyle" );
	};

	//셀의 스타일 변경
	a_obj.SetCellStyle = function (rowIdx, colName, a_rules) {
		this.StartMightyEvent( "SetCellStyle" );
		var cssKey    = this.id + "-r" + rowIdx + "-" + colName + "-cellstyle",
		    setCssObj = this.SetStylePreprocessing( cssKey, a_rules );

		setCssObj.cssPropObj[ colName ]   = cssKey;
		setCssObj.addCssObj[ rowIdx - 1 ] = setCssObj.cssPropObj;

		this._SlickGrid.addCellCssStyles( cssKey, JSON.parse( JSON.stringify(setCssObj.addCssObj) ) );
		this.EndMightyEvent( "SetCellStyle" );
	};

	//컬럼 visible 설정(flag: true, defaultNone: true 일 경우 columns에 없는 컬럼은 visible: none 를 기본값으로 한다.)
	a_obj.SetColumnVisible = function (columns, flag, defaultNone) {
		this.StartMightyEvent( "SetColumnVisible" );
		if ( columns == null ) {
			console.log("SetColumnVisible msg: columns == null");
			this.EndMightyEvent( "SetColumnVisible" );
		}

		if ( typeof a_columns === "string" ) {
			columns = [ columns ];
		}

		var columnConfig = $.extend( true, [], this._ColumnConfig );

		for ( var i = 0, ii = columnConfig.length; i < ii; i++ ) {
			if ( columns.indexOf( columnConfig[i].fieldName ) !== -1 ) {
				columnConfig[i].visible = flag;
			}
			else if ( defaultNone ) {
				columnConfig[i].visible = false;
			}
		}

		this.SetColumns( x_ConvGridColumns( columnConfig, this ) );
		this.EndMightyEvent( "SetColumnVisible" );
	};
//============================================================== Mighty-X5 cssStyle Method end

//============================================================== Mighty-X5 auto cell start
	//콜백 함수 호출
	a_obj.CallFunction = function (args, callbackFunc) {
		this.StartMightyEvent( "CallFunction", 1 );
		this._IsFormatting = true;
		var result = callbackFunc( args );
		this._IsFormatting = false;
		return this.EndMightyEvent( "CallFunction", result );
	};

	//셀 마스크 설정
	a_obj.SetMaskValue = function (colName, a_callbackFunc) {
		this.StartMightyEvent( "SetMaskValue", 2 );
		if ( this._MaskColumns.indexOf( colName ) === -1 ) {
			this._MaskColumns.push( colName );
		}

		var getMaskValue = function (args) {
				return a_obj.CallFunction( args, a_callbackFunc );
		};

		this.SetColumn( colName, "getMaskValue", getMaskValue );
		this.EndMightyEvent( "SetMaskValue" );
	};

	//동적 행 스타일 변경
	a_obj.SetAutoRowStyle = function (a_callbackFunc) {
		this.StartMightyEvent( "SetAutoRowStyle", 2 );
		var setAutoRowStyle = function (node, row, items, columnDef) {
				var $rowNodes = a_obj._SlickGrid.getRowNodes( row );
				if ( $rowNodes.length ) {
					$rowNodes = $($rowNodes);
					var args = {
							"rowNode": $rowNodes,
							"rowIdx" : row + 1,
							"colName": columnDef.id,
							"dataContext" : items
					};

					var cssRule = a_obj.CallFunction( args, a_callbackFunc );
					if ( cssRule ) {
						for ( var prop in cssRule ) {
							if ( cssRule.hasOwnProperty(prop) ) {
								$rowNodes.find(".slick-cell").css(prop, cssRule[prop]);
							}
						}
					}
				}
		};

		this.SetColumn( "sel", "asyncPostRender", setAutoRowStyle );
		this.EndMightyEvent( "SetAutoRowStyle" );
	}

	//동적 셀 스타일 변경(함수 호출 시 스타일 지정)
	a_obj.SetAutoCellStyle = function (colName, a_rules, a_callbackFunc) {
		//칼럼 배열처리 기능 추가(2018.11.12 KYY)
		if(Array.isArray(colName)) {
			$(colName).each(function(idx,val){
				a_obj.SetAutoCellStyle(val, a_rules, a_callbackFunc);
			});
			return;
		}

		this.StartMightyEvent( "SetAutoCellStyle", 2 );
		var cssKey  = this.id + "-" + colName + "-autostyle",
				cssRule = this.MakeCssRules( cssKey, a_rules ),
				getAutoClass = function (args) {
						if ( a_obj.CallFunction( args, a_callbackFunc ) ) {
							return " " + cssKey;
						}
				};

		this.CreateStylesheet( cssKey, cssRule );
		this.SetColumn( colName, "getAutoClass", getAutoClass );
		this.EndMightyEvent( "SetAutoCellStyle" );
	};

	//동적 셀 스타일 변경(콜백함수 리턴값으로 스타일 지정, 리턴값에 class 속성이 있을경우 cellNode에 클래스를 추가)
	a_obj.SetAutoCellStyle2 = function (colName, a_callbackFunc) {
		this.StartMightyEvent( "SetAutoCellStyle2", 2 );
		var setAutoCellStyle = function (node, row, items, columnDef) {
				var $cellNode = $(node),
						args = {
								"cellNode": $cellNode,
								"rowIdx"  : row + 1,
								"colName" : columnDef.id,
								"dataContext" : items
						};

				var cssRule = a_obj.CallFunction( args, a_callbackFunc );
				if ( cssRule ) {
					for ( var prop in cssRule ) {
						if ( cssRule.hasOwnProperty(prop) ) {
							if ( prop == "class" ) {
								if ( typeof cssRule[prop] === "string" ) {
									cssRule[prop] = [ cssRule[prop] ];
								}

								for ( var i = 0, ii = cssRule[prop].length; i < ii; i++ ) {
									$cellNode.addClass(cssRule[prop][i]);
								}
							}
							else if ( prop == "removeClass" ) {
								if ( typeof cssRule[prop] === "string" ) {
									cssRule[prop] = [ cssRule[prop] ];
								}

								for ( var i = 0, ii = cssRule[prop].length; i < ii; i++ ) {
									$cellNode.removeClass(cssRule[prop][i]);
								}
							}
							else {
								$cellNode.css(prop, cssRule[prop]);
							}
						}
					}
				}
		};

		this.SetColumn( colName, "asyncPostRender", setAutoCellStyle );
		this.EndMightyEvent( "SetAutoCellStyle2" );
	};

	//동적 셀값 설정
	a_obj.SetAutoValue = function (colName, a_callbackFunc, isUpdate) {
		this.StartMightyEvent( "SetAutoValue", 2 );
		var getAutoValue = function(args) {
				var autoValue = a_obj.CallFunction( args, a_callbackFunc );

				if ( !(isUpdate === false) ) {
					a_obj.SetItem( rowIdx, colName, autoValue, false );
				}
				return autoValue;
		};

		this.SetColumn( colName, "getAutoValue", getAutoValue );
		this.EndMightyEvent( "SetAutoValue" );
	};

	//동적 합계 셀값 설정
	a_obj.SetAutoFooterValue = function (colName, a_callbackFunc) {
		this.StartMightyEvent( "SetAutoFooterValue", 2 );
		var index   = this.GetColumnIndexByName(colName);
		var columns = $.extend(true, [], this.GetColumns());

		//columns[index].footerExpr         = "AUTO";
		columns[index].getAutoFooterValue = a_callbackFunc;

		this.SetColumns( columns );
		this.EndMightyEvent( "SetAutoFooterValue" );
	};

	//동적 readonly 설정
	a_obj.SetAutoReadOnly = function (colName, a_callbackFunc, appStyle) {
		this.StartMightyEvent( "SetAutoReadOnly", 2 );
		var getAutoReadOnly = function (args) {
				return a_obj.CallFunction( args, a_callbackFunc );
		};

		if ( !(appStyle === false) ) {
			this.SetAutoCellStyle2( colName, function(args) {
				var styleResult = a_obj.CallFunction( args, a_callbackFunc );
				return styleResult ? {"class": "readonly-cell"} : {"removeClass": "readonly-cell"};
			});
		}

		this.SetColumn( colName, "getAutoReadOnly", getAutoReadOnly );
		this.GridCancel();
		this.RenderProcess();
		this.EndMightyEvent( "SetAutoReadOnly" );
	};

	//셀의 Merge처리
	a_obj.SetCellMerge = function (colNames) {
		this.StartMightyEvent( "SetCellMerge" );

		var columns = a_obj.GetColumns();
		for(var i=0; i<colNames.length; i++) {
			var colIdx = a_obj.GetColumnIndexByNameForConfig( colNames[i] );
			if(colIdx>=0) {
				a_obj._ColumnConfig[colIdx].merge = true;
				columns[colIdx].merge = true;
			}
		}
		a_obj.SetColumns( columns );
		this.EndMightyEvent( "SetCellMerge" );
	};


//============================================================== Mighty-X5 auto cell end

//============================================================== other start
	//그리드 전체 readonly 설정
	a_obj.GridReadOnly = function (flag) {
		this.StartMightyEvent( "GridReadOnly" );
		if ( flag ) {
			this.classList.add( "slick-disabled" );
			this.GridCancel();
		}
		else {
			this.classList.remove( "slick-disabled" );
		}

		this._GridReadonly = flag;
		this.EndMightyEvent( "GridReadOnly" );
	};

	/**
	 * fn_name     : SetCheckBar
	 * Description : 체크바 설정
	 * param       : a_flag      : display 여부
	 *							 a_exclusive : true 체크박스 false 라디오박스 (미적용)
	 *							 a_head      : 헤더 체크박스 표시여부 (default: true)
	 *               a_showGroup : 그룹된 row에 체크박스 표시여부 (미적용)
	 *							 a_checkColor: 체크된 행의 색 (미적용)
	 * Statements  :
	 */
	a_obj.SetCheckBar = function (a_flag, a_exclusive, a_head, a_showGroup, a_checkColor) {
		this.StartMightyEvent( "SetCheckBar", 2 );
		/* 기존 checkbox plugin 삭제 */
		if ( this._Plugins.CheckboxSelector ) {
			this._SlickGrid.unregisterPlugin( this._Plugins.CheckboxSelector );
		}

		/* checkbox plugin 설치 */
		//width(20->24)조정 (2017.10.10 KYY)
		this._Plugins.CheckboxSelector = new Slick.CheckboxSelectColumn( { width: 24, cssClass: "slick-cell-checkboxsel" }, a_head == null ? true : a_head );
		this._SlickGrid.registerPlugin( this._Plugins.CheckboxSelector );

		this._Plugins.CheckboxSelector.onCheckBarClick.subscribe(function (e, args) { a_obj.StartSlickEvent( "onCheckBarClick" ); xe_M_SlickGridCheckBarClick( a_obj, e, args ); a_obj.EndSlickEvent( "onCheckBarClick" ); });
		this._Plugins.CheckboxSelector.onCheckBarHeaderClick.subscribe(function (e, args) { a_obj.StartSlickEvent( "onCheckBarHeaderClick" ); xe_M_SlickGridCheckBarHeaderClick( a_obj, e, args ); a_obj.EndSlickEvent( "onCheckBarHeaderClick" ); });

		//추가(2017.07.06 KYY)
		a_obj._CheckBar	= a_flag;
		var columns = this.GetColumns();

		/* columns 정보 재설정 */
		if ( a_flag ) {
			if ( columns[1] && columns[1].id !== "_checkbox_selector" ) {
				var checkColumn = this._Plugins.CheckboxSelector.getColumnDefinition();
				$.extend( checkColumn, { minWidth: 10, headerNames: ["<input type='checkbox'>"], fixed: true } );

				columns.splice( 1, 0, checkColumn );
			}
		}
		else {
			if ( columns[1] && columns[1].id === "_checkbox_selector" ) {
				columns.splice( 1, 1 );
			}
		}

		this.SetColumns( columns );
		this.SetSortOrder();

    	this.EndMightyEvent( "SetCheckBar" );
	};

	//체크된 행번호를 반환
	a_obj.GetCheckedRows = function () {
		this.StartMightyEvent( "GetCheckedRows" );
		var checkedRows = [],
		    items       = this.GetData();

		for ( var i = 0, ii = items.length; i < ii; i++ ) {
			if ( items[i].check === "Y" ) {
				checkedRows.push( i + 1 );
			}
		}

		return this.EndMightyEvent( "GetCheckedRows", checkedRows );
	};

	//지정한 행을 체크 또는 해제
	a_obj.SetCheckRows = function (a_rows, a_flag) {
		this.StartMightyEvent( "SetCheckRows" );
		//console.log(this._Plugins[ "CheckboxSelector" ]);
		//기능 구현(2017.06.18 KYY)
		var items = this.GetData();
		if(typeof a_flag == "boolean") {a_flag = (a_flag ? "Y" : "N");}
		$(a_rows).each(function(idx,val){
			items[val -1].check = a_flag;
		});
		this._DataView.refresh();
	    this._SlickGrid.invalidateAllRows();
		this.RenderProcess();
		this.EndMightyEvent( "SetCheckRows" );
	};

	//자식행을 체크 또는 해제
	a_obj.SetCheckChild =  function (a_row, a_checked, a_recursive, a_visibleOnly) {
		this.StartMightyEvent( "SetCheckChild" );
		this.EndMightyEvent( "SetCheckChild" );
	};

	//전체 행을 체크 또는 해제
	a_obj.SetCheckAll = function (a_flag) {
		this.StartMightyEvent( "SetCheckAll" );
		//기능 구현(2017.06.18 KYY)
		if(typeof a_flag == "boolean") {a_flag = (a_flag ? "Y" : "N");}
		var items = this.GetData();
		for (var i = 0, ii = items.length; i < ii; i++ ) {
			items[i].check = a_flag;
		}
		this._DataView.refresh();
	    this._SlickGrid.invalidateAllRows();
		this.RenderProcess();
		this.EndMightyEvent( "SetCheckAll" );
	};

	//체크된 행 인지 확인 (tree 인경 rowid 값 반환)
	a_obj.IsCheckedRow = function (a_row) {
		this.StartMightyEvent( "IsCheckedRow" );
		//기능 구현(2017.06.18 KYY)
		var items = this.GetData();
		return (items[a_row -1].check === "Y");
		this.EndMightyEvent( "IsCheckedRow" );
	};

	//체크된 행을 삭제(2017.06.18 KYY)
	a_obj.DeleteCheckedRows = function (a_delconfirm, a_nodata_msg) {
		this.StartMightyEvent( "DeleteCheckedRow" );
		if(a_delconfirm==null){a_delconfirm=true;}
		if(a_nodata_msg==null){a_nodata_msg=true;}
		var crows = a_obj.GetCheckedRows();
		if(crows.length<=0){
			if(a_nodata_msg) {_X.Noty("선택된 데이타가 없습니다.");}
		}else{
			if (!a_delconfirm || _X.MsgBoxYesNo( "확인", "선택된 " + crows.length + " 건의 데이타를 삭제 하시겠습니까?" ) === "1" ) {
				$(crows.reverse()).each(function(idx,val){a_obj.DeleteRow(val);});
			}
		}
		this._DataView.refresh();
	    this._SlickGrid.invalidateAllRows();
		this.RenderProcess();
		this.EndMightyEvent( "DeleteCheckedRow" );
	};

	//footer 설정
	a_obj.SetFooter = function (a_flag, columns) {
		this.StartMightyEvent( "SetFooter" );
		this._Plugins[ "totalsPlugin" ]  = new TotalsPlugin(16);
		this._SlickGrid.registerPlugin( this._Plugins[ "totalsPlugin" ] );
		this._TotalColumns = columns;
		this.EndMightyEvent( "SetFooter" );
	};

	//컬럼 header 명칭 변경
	a_obj.SetColumnHeaderName = function (colName, name, a_div) {
		this.StartMightyEvent( "SetColumnHeaderName" );
		this.EndMightyEvent( "SetColumnHeaderName" );
	};

	//컬럼 header 글자색 변경
	a_obj.SetColumnHeaderColor = function (colName, color, a_div) {
		this.StartMightyEvent( "SetColumnHeaderColor" );
		this.EndMightyEvent( "SetColumnHeaderColor" );
	};

	//SetFocusBy... 함수를 Find기능과 Focus 기능 분리(2017.03.16 KYY)
	//지정된 요소의 value 값과 동일한 값을 가진 셀의 행번호를 반환
	a_obj.Find = function (a_ele, colNames, toUpper) {
		this.StartMightyEvent( "Find" );
		var values;

		if ( typeof a_ele === "string" ) {
			if(typeof $('#' + a_ele).get(0) !== "undefined" && typeof $('#' + a_ele).get(0).value !== "undefined") {
				values = $('#' + a_ele).get(0).value;
			}
			else{
				values = a_ele;
			}
		} else {
			if ( typeof a_ele === "object" ) {
				values = $(a_ele).val();
			} else {
				values = a_ele;
			}
		}

		if ( values == null || values === "" ) {
			return this.EndMightyEvent( "Find", {"row":-1, "col":null} );
		}

		if ( typeof values === "string" ) {
			values = [ values ];
		}

		if ( typeof colNames === "string" ) {
			colNames = [ colNames ];
		}

		var data = this.GetData();

		//데이타가 없는 경우 오류남(2017.06.02 KYY)
		if(data.length<=0)
			return {"row":-1, "col":null};

		for ( var i = 0, ii = values.length; i < ii; i++ ) {
			for ( var j = this.GetRow(), jj = this.RowCount(); j < jj; j++ ) {
				for ( var k = 0, kk = colNames.length; k < kk; k++) {
					if ( toUpper !== false ) {
						if ( String( data[j][ colNames[k] ] ).toUpperCase().indexOf( values[i].toUpperCase() ) + 1 ) {
							return this.EndMightyEvent( "Find", {"row":j+1, "col":colNames[k]} );
						}
					}
					else {
						if ( String( data[j][ colNames[k] ] ).indexOf( values[i] ) + 1 ) {
							return this.EndMightyEvent( "Find", {"row":j+1, "col":colNames[k]} );
						}
					}
				}
			}

			for ( var j = 0, jj = this.GetRow(); j < jj; j++ ) {
				for ( var k = 0, kk = colNames.length; k < kk; k++) {
					if ( toUpper !== false ) {
						if ( String( data[j][ colNames[k] ] ).toUpperCase().indexOf( values[i].toUpperCase() ) + 1 ) {
							return this.EndMightyEvent( "Find", {"row":j+1, "col":colNames[k]} );
						}
					}
					else {
						if ( String( data[j][ colNames[k] ] ).indexOf( values[i] ) + 1 ) {
							return this.EndMightyEvent( "Find", {"row":j+1, "col":colNames[k]} );
						}
					}
				}
			}
		}

		return {"row":-1, "col":null};
	};

	//SetFocusBy... 함수를 Find기능과 Focus 기능 분리(2017.03.16 KYY)
	//지정된 요소의 value 값과 동일한 값을 가진 셀로 포커스를 이동시킨 후 행번호를 반환
	a_obj.SetFocusByElement = function (a_ele, colNames, focus, toUpper, active) {
		this.StartMightyEvent( "SetFocusByElement" );

		var rtValue = this.Find(a_ele, colNames, toUpper);
		if(rtValue!=null && rtValue.row > 0) {
			if ( !(focus === false) ) {
				this.SetRow(rtValue.row, rtValue.col, active );
			}
		}
		return this.EndMightyEvent( "SetFocusByElement", rtValue.row);
	};

	//SetFocusBy... 함수를 Find기능과 Focus 기능 분리(2017.03.16 KYY)
	//지정된 값과 동일한 값을 가진 셀로 포커스를 이동시킨 후 행번호를 반환
	a_obj.SetFocusByValue = function (values, colNames, focus, toUpper, active) {
		this.StartMightyEvent( "SetFocusByValue" );

		var rtValue = this.Find(values, colNames, toUpper);
		if(rtValue!=null && rtValue.row > 0) {
			if ( !(focus === false) ) {
				this.SetRow(rtValue.row, rtValue.col, active );
			}
		}
		return this.EndMightyEvent( "SetFocusByValue", rtValue.row);
	};

	//지정된 요소의 value 값과 동일한 값을 가진 셀로 포커스를 이동시킨 후 행번호를 반환
	a_obj.SetFocusByElement_old = function (a_ele, colNames, focus, toUpper, active) {
		this.StartMightyEvent( "SetFocusByElement" );
		var value;

		if ( typeof a_ele === "string" ) {
			value = $('#' + a_ele).get(0).value;
		} else {
			value = a_ele.value;
		}

		if ( value == null || value === "" ) {
			return this.EndMightyEvent( "SetFocusByElement" );
		}

		if ( typeof colNames === "string" ) {
			colNames = [ colNames ];
		}

		var data = this.GetData();

		for ( var i = this.GetRow(), ii = this.RowCount(); i < ii; i++ ) {
			for ( var j = 0, jj = colNames.length; j < jj; j++) {
				if ( toUpper !== false ) {
					if ( String( data[i][ colNames[j] ] ).toUpperCase().indexOf( value.toUpperCase() ) + 1 ) {
						if ( !(focus === false) ) {
							this.SetRow( i + 1, colNames[j], active );
						}

						return this.EndMightyEvent( "SetFocusByElement", i + 1 );
					}
				}
				else {
					if ( String( data[i][ colNames[j] ] ).indexOf( value ) + 1 ) {
						if ( !(focus === false) ) {
							this.SetRow( i + 1, colNames[j], active );
						}

						return this.EndMightyEvent( "SetFocusByElement", i + 1 );
					}
				}
			}
		}

		for ( var i = 0, ii = this.GetRow(); i < ii; i++ ) {
			for ( var j = 0, jj = colNames.length; j < jj; j++) {
				if ( toUpper !== false ) {
					if ( String( data[i][ colNames[j] ] ).toUpperCase().indexOf( value.toUpperCase() ) + 1 ) {
						if ( !(focus === false) ) {
							this.SetRow( i + 1, colNames[j], active );
						}

						return this.EndMightyEvent( "SetFocusByElement", i + 1 );
					}
				}
				else {
					if ( String( data[i][ colNames[j] ] ).indexOf( value ) + 1 ) {
						if ( !(focus === false) ) {
							this.SetRow( i + 1, colNames[j], active );
						}

						return this.EndMightyEvent( "SetFocusByElement", i + 1 );
					}
				}
			}
		}

		return this.EndMightyEvent( "SetFocusByElement", -1 );
	};

	//지정된 값과 동일한 값을 가진 셀로 포커스를 이동시킨 후 행번호를 반환
	a_obj.SetFocusByValue_old = function (values, colNames, focus, toUpper, active) {
		this.StartMightyEvent( "SetFocusByValue" );
		if ( typeof values === "string" ) {
			values = [ values ];
		}

		if ( typeof colNames === "string" ) {
			colNames = [ colNames ];
		}

		var data = this.GetData();

		for ( var i = 0, ii = values.length; i < ii; i++ ) {
			for ( var j = this.GetRow(), jj = this.RowCount(); j < jj; j++ ) {
				for ( var k = 0, kk = colNames.length; k < kk; k++) {
					if ( toUpper !== false ) {
						if ( String( data[j][ colNames[k] ] ).toUpperCase().indexOf( values[i].toUpperCase() ) + 1 ) {
							if ( !(focus === false) ) {
								this.SetRow( j + 1, colNames[k], active );
							}

							return this.EndMightyEvent( "SetFocusByValue", j + 1 );
						}
					}
					else {
						if ( String( data[j][ colNames[k] ] ).indexOf( values[i] ) + 1 ) {
							if ( !(focus === false) ) {
								this.SetRow( j + 1, colNames[k], active );
							}

							return this.EndMightyEvent( "SetFocusByValue", j + 1 );
						}
					}
				}
			}

			for ( var j = 0, jj = this.GetRow(); j < jj; j++ ) {
				for ( var k = 0, kk = colNames.length; k < kk; k++) {
					if ( toUpper !== false ) {
						if ( String( data[j][ colNames[k] ] ).toUpperCase().indexOf( values[i].toUpperCase() ) + 1 ) {
							if ( !(focus === false) ) {
								this.SetRow( j + 1, colNames[k], active );
							}

							return this.EndMightyEvent( "SetFocusByValue", j + 1 );
						}
					}
					else {
						if ( String( data[j][ colNames[k] ] ).indexOf( values[i] ) + 1 ) {
							if ( !(focus === false) ) {
								this.SetRow( j + 1, colNames[k], active );
							}

							return this.EndMightyEvent( "SetFocusByValue", j + 1 );
						}
					}
				}
			}
		}

		return this.EndMightyEvent( "SetFocusByValue", -1 );
	};

	//중복된 값이 있는 행번호를 반환 (excludeRows: 값 체크시 제외할 행번호)
	a_obj.GetDuplicateRowByValue = function (values, colNames, excludeRows, flash, toUpper) {
		this.StartMightyEvent( "GetDuplicateRowByValue" );
		if ( typeof values === "string" ) {
			values = [ values ];
		}

		if ( typeof colNames === "string" ) {
			colNames = [ colNames ];
		}

		if ( typeof excludeRows === "string" || typeof excludeRows === "number" ) {
			excludeRows = [ excludeRows ];
		}

		var data = this.GetData(),
				duplicatedRow  = -1,
				duplicatedCell = -1;

		for ( var i = 0, ii = values.length; i < ii; i++ ) {
			for ( var j = 0, jj = data.length; j < jj; j++ ) {
				if ( excludeRows && excludeRows.indexOf( j + 1 ) !== -1 ) {
					continue;
				}

				for ( var k = 0, kk = colNames.length; k < kk; k++) {
					if ( toUpper !== false ) {
						if ( String( data[j][ colNames[k] ] ).toUpperCase().indexOf( values[i].toUpperCase() ) + 1 ) {
							duplicatedRow  = j + 1;
							duplicatedCell = this.GetColumnIndexByName( colNames[k] );
						}
					}
					else {
						if ( String( data[j][ colNames[k] ] ).indexOf( values[i] ) + 1 ) {
							duplicatedRow  = j + 1;
							duplicatedCell = this.GetColumnIndexByName( colNames[k] );
						}
					}
				}
			}
		}

		if ( duplicatedRow + 1 && flash ) {
			this._SlickGrid.flashCell( duplicatedRow - 1, duplicatedCell );
		}

		return this.EndMightyEvent( "GetDuplicateRowByValue", duplicatedRow );
	};

	//중복된 키값이 있는 행번호를 반환
	a_obj.GetDuplicateRowByKey = function (keyValues, keyColNames, excludeRow, flash) {
		this.StartMightyEvent( "GetDuplicateRowByKey" );
		if ( typeof keyValues === "string" ) {
			keyValues = [ keyValues ];
		}

		if ( typeof colNames === "string" ) {
			keyColNames = [ keyColNames ];
		}

		var data 		 = this.GetData(),
				keyValue = keyValues.join( "" );

		for ( var i = 0, ii = data.length; i < ii; i++ ) {
			var row = this.GetRowById( data[i].id );
			if ( row === excludeRow ) {
				continue;
			}

			var keyColumnValue = "";

			for ( var j = 0, jj = keyColNames.length; j < jj; j++ ) {
				keyColumnValue += data[i][ keyColNames[j] ];
			}

			if ( keyValue === keyColumnValue ) {
				if ( flash ) {
					this._SlickGrid.flashCell( row - 1, 1 );
				}
				return this.EndMightyEvent( "GetDuplicateRowByKey", i + 1 );
			}
		}

		return this.EndMightyEvent( "GetDuplicateRowByKey", -1 );
	};

	//방향키 누를때 행 추가
	a_obj.GridAppendable = function (a_flag) {
		this.StartMightyEvent( "GridAppendable" );
		this.EndMightyEvent( "GridAppendable" );
	};

	//현재 sqlKey 반환
	a_obj.GetSqlKey = function () {
		this.StartMightyEvent( "GetSqlKey" );
		return this.EndMightyEvent( "GetSqlKey", this.sqlKey );
	};

	//현재 sqlKey 설정
	a_obj.SetSqlKey = function (sqlKey) {
		this.StartMightyEvent( "SetSqlKey" );
		this.sqlKey = sqlKey;

		this._UpdateInfo = eval( _X.GSI( this.subSystem, this.sqlFile, this.sqlKey ) );

		if ( this._UpdateInfo ) {
			this._UpDateKeyCols = [];

			for ( var i = 0, ii = this._UpdateInfo.length; i < ii; i++ ) {
				if ( this._UpdateInfo[i].iskeycol ) {
					this._UpDateKeyCols.push( this._UpdateInfo[i].name );
				}
			}

			this._Updatable = this._UpdateInfo.length > 0;
		}

		this.EndMightyEvent( "SetSqlKey" );
	};

	//하위그리드를 설정(child 버튼 action 시에 관련된 하위그리드의 예외처리를 template에서 수행하는 용도)
	a_obj.SetChildGrids = function (gridsArr) {
		this._ChildGrids = gridsArr;

		for ( var i = 0, ii = gridsArr.length; i < ii; i++ ) {
			gridsArr[i]._ParentGrid = this;
		}
	};

	//엑셀업로드
	a_obj.ExcelUpload = function (system, a_callback, params, header, pgmCode, retrieve) {
		this.StartMightyEvent( "ExcelUpload", 5 );
		if ( !(retrieve === false) ) {
			if ( _X.IsDataChangedAll(false) > 0 ) {
				if ( _X.MsgBoxYesNo( "확인", "변경된 내용이 있습니다.\n엑셀업로드를 하실경우 변경내용이 초기화 됩니다.\r\n계속 진행 하시겠습니까?" ) === "2" ) {
					return;
				}
			}
		}

		if ( typeof pf_ExcelUpload != "undefined" ) {
			if ( header == null || isNaN(header) ) {
				header = 1;
			}

			x_SaveExcelFile.headerRow.value = header;
			x_SaveExcelFile.upPgmId.value   = pgmCode || "";
			x_SaveExcelFile.upUserId.value  = mytop._UserID;

			if ( typeof a_callback == "function" ) {
				pf_ExcelUpload(function (uploadId, uploadCnt) {
					a_callback(uploadId, uploadCnt);
				});
			}
			else if ( typeof a_callback == "string" ) {
				pf_ExcelUpload(function (uploadId, uploadCnt) {
					_X.ExecProc( system, a_callback, params );
					_X.MsgBox("엑셀데이타 업로드 완료.");

					if ( !(retrieve === false) ) {
						a_obj.Reset();
						x_DAO_Retrieve(a_obj);
					}
				});
			}
		}
		else {
			if ( mytop._UserID == "ADMIN" ) {
				_X.MsgBox("확인", "UploadExcelForm.jsp 또는 UploadExcelForm_MSS.jsp 파일을 include 하십시오.\n콘솔창을 확인하시기 바랍니다.");
				console.log('<jsp:include page="/Mighty/jsp/UploadExcelForm.jsp"/>');
				console.log('<jsp:include page="/Mighty/jsp/UploadExcelForm_MSS.jsp"/>');
			}
		}

		this.EndMightyEvent( "ExcelUpload" );
	};

	//파일 확장자 변경
	a_obj.FileTypeConverter = function (fileTypes) {
		if ( typeof fileTypes === "string" ) {
			fileTypes = [ fileTypes ];
		}

		var convObj = {
				"application/vnd.ms-excel": "xls",
				"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet": "xlsx",
				"application/vnd.ms-powerpoint": "ppt",
				"application/vnd.openxmlformats-officedocument.presentationml.presentation": "pptx",
				"application/x-msdownload": "application",
				"application/haansofthwp": "application/hwp",
				"application/vnd.openxmlformats-officedocument.wordprocessingml.document": "application/docx",
				"xls" : "application/vnd.ms-excel",
				"xlsx": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
				"ppt" : "application/vnd.ms-powerpoint",
				"pptx": "application/vnd.openxmlformats-officedocument.presentationml.presentation",
				"csv" : ".csv",
				"txt" : "text/plain",
				"png" : "image/png",
				"jpg" : "image/jpg",
				"jpeg": "image/jpeg",
				"gif" : "image/gif",
				"htm" : "text/html",
				"html": "text/html",
				"avi" : "video/avi",
				"mpg" : "video/mpg",
				"mpeg": "video/mpeg",
				"mp4" : "video/mp4",
				"mp3" : "audio/mp3",
				"wav" : "audio/wav",
				"pdf" : ".pdf"
		};

		var returnTypes = [];
		for ( var i = 0, ii = fileTypes.length; i < ii; i++ ) {
			returnTypes.push( convObj[ fileTypes[i].toLowerCase() ] || fileTypes[i] );
		}

		return returnTypes;
	};

	//파일 업로드
	a_obj.FileUpload = function (row, cell) {
		if ( isNaN(cell) ) {
			cell = this.GetColumnIndexByName( cell );
		}

		var column        = this.GetColumn( this.GetColumnNameByIndex(cell) );
		var fileSaveDir   = column.fileInfo && column.fileInfo.fileSaveDir || this.subSystem.toUpperCase();
		var fileIdField   = column.fileInfo && column.fileInfo.fileIdField;
		var atchFileId    = this.GetItem( row, fileIdField );
		var formId        = this.id + "_fileUploadForm";

		if ( !this._UpdateInfo ) {
			console.log("updateInfo is null");
			return;
		}
		else if ( !fileIdField ) {
			console.log("fileIdField is null");
			return;
		}

		// form 생성
		if ( !$("#" + formId).length ) {
			var params   = "subdir=" + this.subSystem + "&fileSaveDir=" + fileSaveDir + "&userid=" + mytop._UserID + "&uptable=" + this._UpdateInfo[0].updatetable + "&keycolumns=" + this._UpDateKeyCols.join(",") + "&svrFilePathYn=" + this._FileSvrPath;
			var formHtml = "<form id='" + formId + "' name='" + formId + "' method='post' action='/Mighty/3rd/SlickGrid/v2.3/jsp/FileUpLoad.jsp?" + params + "' enctype='multipart/form-data'>" +
										 		"<div id='fileUploadList'></div>"
										 "</form>" +
										 "<div id='" + this.id + "_fileUpTempList'></div>";
			$(formHtml).appendTo(this);
		}

		var $fileUploadList = $("#" + formId + " #fileUploadList");

		// 파일 수 체크
		if ( this._FileMaxCnt != null ) {
			if ( $fileUploadList.find(":file").length >= this._FileMaxCnt ) {
				_X.MsgBox("확인", "한 번에 업로드 가능한 최대 첨부파일 수를 초과하였습니다.\n작업중인 정보를 저장한 후 다시 시도하여 주십시오.\n최대 첨부파일 수: " + this._FileMaxCnt);
				return;
			}
		}

		// 파일 크기 체크
		var maxSize  = (this._FileMaxSize || 15) * 1024 * 1024;

		if ( this._FilesSize >= maxSize ) {
			_X.MsgBox("확인", "한 번에 업로드 가능한 권장 첨부파일 크기를 초과하였습니다.\n작업중인 정보를 저장한 후 다시 시도하여 주십시오.\n권장 파일크기: " + maxSize + "\n현재 파일크기: " + this._FilesSize);
			return;
		}

		var $file = $fileUploadList.find("#fileId_" + row + "_" + cell);

		if ( !$file.length ) {
			// parameter 생성
			$("<input type='hidden' id='idfield_" + row + "_" + cell + "' name='idfield_" + row + "_" + cell + "' value='" + fileIdField + "' />").appendTo($fileUploadList);
			$("<input type='hidden' id='atchid_"  + row + "_" + cell + "' name='atchid_"  + row + "_" + cell + "' value='" + atchFileId  + "' />").appendTo($fileUploadList);

			if ( column.fileInfo && column.fileInfo.fileSaveNameField ) {
				$("<input type='hidden' id='savenamefield_"  + row + "_" + cell + "' name='savenamefield_"  + row + "_" + cell + "' value='" + column.fileInfo.fileSaveNameField + "' />").appendTo($fileUploadList);
			}

			for ( var i = 0, ii = this._UpDateKeyCols.length; i < ii; i++ ) {
				$("<input type='hidden' id='upkey_" + row + "_" + cell + "_" + i + "' name='upkey_" + row + "_" + cell + "' />").appendTo($fileUploadList);
			}

			var accept;
			if ( column.fileInfo && column.fileInfo.fileAccept ) {
				accept = this.FileTypeConverter( column.fileInfo.fileAccept.split("|") ).join(",");
			}

			// file input 생성
			$file = $("<input type='file' id='fileId_" + row + "_" + cell + "' name='fileName_" + row + "_" + cell + "' " + (accept && "accept='" + accept + "'" || "") + " />").appendTo($fileUploadList);
			$file.change(function (e) {
					a_obj._FileUploadInfo[row] = cell;
					xe_M_SlickGridFileChanged( a_obj, row, cell, this );
			});

			$file.data( "firstData", $.extend(true, {}, this.GetRowData(row)) );
		}

		$file.data( "fileSize", $file.prop("files")[0] && $file.prop("files")[0].size || 0 );

		$file.click();
	};

	//파일 저장
	a_obj.FileSave = function (callback) {
		if ( this._isFileUpload ) {
			return;
		}

		var $form = $("#" + this.id + "_fileUploadForm");

		// key 값 저장
		$form.find("input[name^='upkey_']").each(function () {
			var $ele   = $(this);
			var eleId  = $ele.attr("id");
			var eleArr = eleId.split("_");
			var row    = eleArr[1];
			var keyIdx = eleArr[3];

			$ele.val( a_obj.GetItem(row, a_obj._UpDateKeyCols[ keyIdx ]) );
		});

		if ( $form.length ) {
			var $fileUploadList = $form.find("#fileUploadList")
			$fileUploadList.find(":file").each(function () {
					var $file = $(this);
					if ( !$file[0].files || $file[0].files.length === 0 ) {
						$file.remove();
					}
			});

			if ( !$fileUploadList.find(":file").length ) {
				return;
			}

			$form.submit(function (e) {
				e.preventDefault();

				$.ajax({
						type: $form.attr("method"),
						url: $form.attr("action"),
						data: new FormData(this),
						async: false,
						cache: false,
				    contentType: false,
				    processData: false,
				    beforeSend: function(xhr) {
								a_obj._isFileUpload = true;
			      },
						success: function (data) {
								var uploadInfo = eval( _X.SD(data) );
								xe_M_SlickGridFileUploaded( a_obj, uploadInfo );
								callback( uploadInfo.length );
						},
						error: function(xhr, status, err) {
								console.log("fileupload error", xhr, status, err);
						},
						complete: function() {
								a_obj._FilesSize      = 0;
								a_obj._FileUploadInfo = {};
								a_obj._isFileUpload   = false;
								$form.remove();
						}
				});
			});

			$form.submit();
		}
	};

	//파일 다운로드
	a_obj.FileDownload = function (row, cell, fileSn) {
		if ( isNaN(cell) ) {
			cell = this.GetColumnIndexByName( cell );
		}

		var column      = this.GetColumn( this.GetColumnNameByIndex(cell) );
		var fileIdField = column.fileInfo && column.fileInfo.fileIdField;

		if ( !fileIdField ) {
			console.log("fileIdField is null");
			return;
		}

		var atchFileId = this.GetItem( row, fileIdField );
		if ( atchFileId !== "" ) {
			_X.FileDownload(atchFileId, fileSn || 1);
		}
	};

	// multi 파일 다운로드
	a_obj.MultiFileDownload = function (rows, colName) {
		if ( !rows.length ) {
			return;
		}

		var inputs = "<input type='hidden' name='subdir' value='" + this.subSystem + "' />";

		for ( var i = 0, ii = rows.length; i < ii; i++ ) {
	    inputs += "<input type='hidden' name='fileIds' value='" + dg_1.GetItem(rows[i], "FILE_ID") + "' />";
		}

    $("<form action='/Mighty/3rd/SlickGrid/v2.3/jsp/MultiFileDownload.jsp' method='post'>" + inputs + "</form>")
    	.appendTo(this)
    	.submit()
    	.remove();
	};

	// multi 파일 다운로드 (zip)
	a_obj.MultiFileDownloadZip = function (rows, colName) {
		if ( !rows.length ) {
			return;
		}

		var fileName = prompt( "파일명을 입력해 주십시오.", _X.ToString(_X.GetSysDate(), "YYYY-MM-DD HH:MI:SS") + "(" + rows.length + " files)" );

		if ( !fileName ) {
			return;
		}

		var inputs = "<input type='hidden' name='subdir'   value='" + this.subSystem + "' />" +
								 "<input type='hidden' name='filename' value='" + fileName + "' />";

		for ( var i = 0, ii = rows.length; i < ii; i++ ) {
	    inputs += "<input type='hidden' name='fileIds' value='" + dg_1.GetItem(rows[i], "FILE_ID") + "' />";
		}

    $("<form action='/Mighty/3rd/SlickGrid/v2.3/jsp/MultiFileDownloadZip.jsp' method='post'>" + inputs + "</form>")
    	.appendTo(this)
    	.submit()
    	.remove();
	};

	//파일 삭제
	a_obj.FileDelete = function (row, cell) {
		if ( isNaN(cell) ) {
			cell = this.GetColumnIndexByName( cell );
		}

		var column      = this.GetColumn( this.GetColumnNameByIndex(cell) );
		var fileSaveDir = column.fileInfo && column.fileInfo.fileSaveDir || this.subSystem.toUpperCase();
		var fileIdField = column.fileInfo && column.fileInfo.fileIdField;
		var atchFileId  = this.GetItem( row, fileIdField );

		if ( !this._UpdateInfo ) {
			console.log("updateInfo is null");
			return;
		}
		else if ( !fileIdField ) {
			console.log("fileIdField is null");
			return;
		}
		else if ( !atchFileId ) {
			console.log("atchFileId is null");
			return;
		}

		if ( _X.MsgBoxYesNo("확인", "첨부파일을 삭제 하시겠습니까?") === "2" ) {
			return;
		}

		var upkey = [];
		for ( var i = 0, ii = this._UpDateKeyCols.length; i < ii; i++ ) {
			upkey.push( this.GetItem(row, this._UpDateKeyCols[i]) );
		}

		var delColumns = "";

		if ( column.fileInfo ) {
			var isColumnIdField = false;

			for ( var prop in column.fileInfo ) {
				if ( column.fileInfo.hasOwnProperty( prop ) ) {
					if ( prop === "fileIdField" || prop === "fileOrignNameField" || prop === "fileSaveNameField" || prop === "fileSizeField" || prop === "fileTypeField" ) {
						delColumns += column.fileInfo[ prop ] + " = '',";

						if ( column.fileInfo[ prop ] === column.id ) {
							isColumnIdField = true;
						}
					}
				}
			}

			if ( isColumnIdField ) {
				delColumns = delColumns.substr(0, delColumns.length - 1);
			} else {
				delColumns += column.id + " = ''";
			}
		}

		$.ajax({
		    type: "post",
		    url: "/Mighty/3rd/SlickGrid/v2.3/jsp/FileDelete.jsp",
		    dataType: "json",
		    data: {
		    		"subdir"     : fileSaveDir,                      // 서브 디렉토리
		    		"atchid"     : atchFileId,                       // 파일 ID
		    		"uptable"    : a_obj._UpdateInfo[0].updatetable, // 그리드 테이블
		    		"keycolumns" : a_obj._UpDateKeyCols.join(","),   // 그리드 테이블 키
		    		"upkey"      : upkey,                            // 키 값
		    		"delColumns" : delColumns,                       // 동시 삭제 대상 컬럼
		    		"userid"     : mytop._UserID                     // 접속자
		    },
		    async: false,
		    beforeSend: function(xhr) {

		    },
		    success: function (jsonData) {
						var deleteInfo = eval( _X.SD(jsonData.data) );
						deleteInfo[0].row  = row;
						deleteInfo[0].cell = cell;

						xe_M_SlickGridFileDeleted( a_obj, deleteInfo[0] );
				},
				error: function(xhr, status, err) {
						console.log("filedelete error", xhr, status, err);
				},
				complete: function() {
				}
	  });
	};

	//행 삭제 시 파일 삭제
	a_obj.RowFileDelete = function () {
		var columns        = this.GetColumns();
		var delAtchFileIds = "";

		for ( var i = 0, ii = columns.length; i < ii; i++ ) {
			if ( columns[i].fileInfo ) {
				for ( var j = 0, jj = this._DeletedRows.length; j < jj; j++ ) {
					delAtchFileIds += this._DeletedRows[j][ columns[i].fileInfo.fileIdField ] + ",";
				}
			}
		}

		if ( delAtchFileIds === "" ) {
			return;
		}

		delAtchFileIds = delAtchFileIds.substr(0, delAtchFileIds.length - 1);

		$.ajax({
		    type: "post",
		    url: "/Mighty/3rd/SlickGrid/v2.3/jsp/RowFileDelete.jsp",
		    dataType: "json",
		    data: {
		    		"subdir" : this.subSystem.toUpperCase(), // 서브 디렉토리
		    		"atchids": delAtchFileIds 							 // 삭제 파일 ID
		    },
		    async: false,
		    beforeSend: function(xhr) {

		    },
		    success: function (jsonData) {

				},
				error: function(xhr, status, err) {
						console.log("rowfiledelete error", xhr, status, err);
				},
				complete: function() {
				}
	  });
	};
//============================================================== other end

	/*=== GirdLayoutComplete ===*/
	if ( typeof xe_GridLayoutComplete !== "undefined" ) {
		a_obj._IsLayoutCompleted = true;

		var allGridLoaded = true;

		if ( $(".slick-grid").length > window._Grids.length ) {
			allGridLoaded = false;
		}
		else {
			for ( var i = 0, ii = window._Grids.length; i < ii; i++ ) {
				if ( !window._Grids[i] || !window._Grids[i]._IsLayoutCompleted ) {
					allGridLoaded = false;
					break;
				}
			}
		}

		setTimeout( function() { xe_GridLayoutComplete( a_obj, allGridLoaded ); }, 0 );

		if ( allGridLoaded ) {
			_X.UnBlock();
		}
	}
};

//============================================================== _X  start
//Grid Layout 파일 생성
_X.CreateSlickGridFile = function(subSystem, pgmCode){
	_X.AjaxCall( subSystem.toLowerCase(), "ReadGridFile", "array", pgmCode );

	if ( _X_AjaxResult == null ) {
		return "";
	}

	if ( _X_AjaxResult.result === "ERROR" ) {
		_X.MsgBox( _X_AjaxResult.datas );
		return "";
	}
	else if ( _X_AjaxResult.result === "OK" ) {
		_X.MsgBox("Grid정보 파일 생성이 완료되었습니다.");
		if ( _X_AjaxResult.datas == null ) {
			return "";
		} else {
			return _X_AjaxResult.datas;
		}
	}
};

//전체 그리드 변경유무 확인
_X.IsDataChangedAll = function(ab_msg){
	var sqlData = "";
	if ( ab_msg == null ) {
		ab_msg = true;
	}

	var fileCnt = 0;
	for ( var i = 0, ii = window._Grids.length; i < ii; i++){
		if ( window._Grids[i]._Updatable ) {
			var cData = window._Grids[i].GetSQLData();
			if ( cData != null && cData != "" ) {
				sqlData += ( sqlData == "" ? "" : _Row_Separate ) + cData;
			}

			fileCnt += Object.keys(window._Grids[i]._FileUploadInfo).length;
		}
	}

	if ( (sqlData == null || sqlData == "") && fileCnt === 0 ) {
		if ( ab_msg ) {
			_X.MsgBox("변경된 데이타가 존재하지 않습니다.");
		}

		return 0;
	}

  return sqlData.length > 0 ? sqlData.length : fileCnt;
};

//그리드 클릭 시 그리드를 curGrid로 등록 (추가, 삽입등의 버튼 이벤트를 curGrid에 적용)
_X.SetGridSelection = function(flag) {
	if ( flag ) {
		if ( window._Grids.length > 1 ) {
			$(".slick-grid").click( function() {
					$(".slick-grid.active").removeClass( "active" );
					var grid = $(this).addClass( "active" );
					window._CurGrid = grid[0];
			});
		}
	} else {
		$(".slick-grid").off( "click" );
	}
}

//============================================================== _X  end

//============================================================== 정리되지 않은 함수

//그리드상에 체크박스의 체크값이 유일해야 할 때 사용(xe_GridItemClick()에서 사용한다. xe_GridDataChange()에서 사용시 루프됨.) (그리드, 컬럼명, 컬럼값)
//dao 로 변경--->a_obj.SetCheckbox(a_fields_arr,a_values_arr)
_X.SetCheckOneOnly = function(a_obj, a_field, a_value){
	var ls_field_idx = a_obj.GetColumnIndexByNameForConfig(a_field);
	var ls_row_datas = a_obj._DataGrid.getFieldValues(ls_field_idx - 1, 0, -1);

	for(var i = 0; i < ls_row_datas.length; i++) {
		if(ls_row_datas[i] == "Y") {
			a_obj.SetItem(i + 1, a_field, "N");
			return;
		}
	}
};

//화면에서 텍스트가 선택되었는지 여부(2016.01.22 KYY)
function isTextSelected() {
  if (document.selection && document.selection.Type) {
    if(document.selection.Type==="Text")
    	return true;
  } else if (window.getSelection) {
  	var selObj = window.getSelection();
  	if(selObj && selObj.toString().length >0)
  		return true;
  }
  return false;
}

//Grid Row 이동(2018.07.06 KYY)
_X.RowMove = function (a_dg, a_updown, a_col) {
	if(a_dg.RowCount()<=0)
		return;
	if(a_col==null) 
		a_col = "SORT_ORDER";

	if(a_updown=="FIX") {
		for(var i=1; i<=a_dg.RowCount(); i++) {
			if(a_dg.GetItem(i, a_col) != i * 10) {
				a_dg.SetItem(i, a_col, i * 10);
			}		
		}
		return;
	}

	var li_def = (a_updown=="UP" ? -15 : 15);
	var cRows = a_dg.GetCheckedRows();

	if(cRows.length<=0) {
		cRows = [a_dg.GetRow()];
	}

	var x_val = (cRows.length<10 ? 10 : 1000);
	for(var i=1; i<=a_dg.RowCount(); i++) {
		if(a_dg.GetItem(i, a_col) != i * x_val) {
			a_dg.SetItem(i, a_col, i * x_val);
		}		
	}
	var startval = (cRows[a_updown=="UP" ? 0 : cRows.length-1]-(a_updown=="UP" ? 2 : -1)) * x_val +1;

	for(var i=0; i<cRows.length; i++) {
		a_dg.SetItem(cRows[i], a_col, startval++);
	}

	a_dg.Sort([{"sortAsc": true,"sortCol":{"field":a_col,"dataType":"number"}}]);
	for(var i=1; i<=a_dg.RowCount(); i++) {
		if(a_dg.GetItem(i, a_col) != i * 10) {
			a_dg.SetItem(i, a_col, i * 10);
		}		
	}
}

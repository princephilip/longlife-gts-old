
 	anychart.licenseKey('hyundaiengineering-eefa714-45b99532');

	//_xChart = function(){
	_xChart = {
		x_ready : false
	};
	_xChart.get_ready = function(){ return this.x_ready;};
	
	_xChart.set_ready = function(){ 
		//colchart = anychart.column();  
		this.x_ready = true; 
		if(typeof(x_Chart_Ready) != "undefined"){
			x_Chart_Ready();
		}
	};
		
	_xChart.createChart = function(a_type, a_div, a_stack){
		
		if(!this.x_ready){
			alert('x_ready Failed');
			return;
		}
		
		var chart ;//= new Function();
		switch(a_type){
			case 'bullet': 
				chart = anychart.bullet();
				break; 
			case 'column': 
				chart = anychart.column();
				break; 
			default:
				chart = anychart.cartesian();
				break; 		
		}
		chart.x_chartType = a_type;
		chart.container(a_div); 
		if(a_stack) chart.yScale().stackMode('value');
		return chart; 
	};
		
	_xChart.setTitles = function(a_chart, a_title, a_x, a_y){
		this.setTitle(a_chart, a_title);
		this.setxAxis(a_chart, a_x);
		this.setyAxis(a_chart, a_y); 
	};
	
	
	_xChart.setTooltipFunc = function(a_chart, a_ttfunc){
		a_chart.x_tooltipfunc = a_ttfunc;
	};
	
	_xChart.setEvent = function(a_chart, a_eventname, a_event){
		if(!a_chart.x_event) a_chart.x_event={};
		a_chart.x_event[a_eventname] = a_event;
	};
	

	// Y축 간격 조정 옵션 a_scaleInterval 추가
	// (2015.04.03 김성한)
	_xChart.drawChart = function(a_chart, a_data, a_seriesName, a_metadata, a_seriesType, a_scaleInterval){ 
		var series = anychart.data.mapAsTable(a_data);
		var old_series, i=0;
		while(old_series=a_chart.getSeries(i)){  
			old_series.data(null); 
			i++;
		} 
		
		var seriestype;
		var cur_series;
		for(var i in series){  
			if(old_series=a_chart.getSeries(i)){ 
				old_series.data(series[i]); 
				cur_series = old_series;
			}else{
				seriestype = (a_seriesType && a_seriesType[i] ? a_seriesType[i] : a_chart.x_chartType);
				//alert(seriestype);
				switch(seriestype){
					case 'area':
						cur_series = a_chart.area(series[i]);
						break;
					case 'bar':
						cur_series = a_chart.bar(series[i]);
						break;
					case 'bubble':
						cur_series = a_chart.bubble(series[i]);
						break;
					case 'column':
						cur_series = a_chart.column(series[i]);
						break;
					case 'line':
						cur_series = a_chart.line(series[i]);
						break;
					case 'marker':
						cur_series = a_chart.marker(series[i]);
						break;
					case 'rangeArea':
						cur_series = a_chart.rangeArea(series[i]);
						break;
					case 'rangeBar':
						cur_series = a_chart.rangeBar(series[i]);
						break;
					case 'spline': 
					  var scale = anychart.scales.linear(); 
					  //create line series and set scale for it
					  cur_series = a_chart.spline(series[i]);
					  cur_series.yScale(scale); 
						break;
					case 'splineArea': 
					  cur_series = a_chart.splineArea(series[i]);
					  cur_series.fill('#2AD62A 0.7');
					  cur_series.hoverFill('#2AD62A 0.7');
						break;
					case 'rangeSplineArea': 
						cur_series = a_chart.rangeSplineArea(series[i]).fill('#1D8BD1 0.8'); 
						break;
					case 'marker': 
						cur_series = a_chart.marker(series[i]).size(2).hoverSize(5);
						break;
					default:
						cur_series = a_chart.column(series[i]);
						break;
						 
				} 

				if(a_chart.x_tooltipfunc){  
					var l_func = a_chart.x_tooltipfunc(i);
	  			cur_series.tooltip().enabled(true).contentFormatter(l_func);
				} 
				/*
				if(a_ttfunc){ 
					//series[i].tooltip().enabled(true).title().enabled(true).text('test'); 
					var l_func = a_ttfunc(i);
	  			cur_series.tooltip().enabled(true).contentFormatter(l_func);
				}
				*/
				 
				if(a_chart.x_event){   
					for(ev in a_chart.x_event){
		  			cur_series.listen(ev , function(e) { 
		  				if(typeof(a_chart.x_event[ev])!="undefined") a_chart.x_event[ev](e);
		  			}); 
					}
				} 
				/*
				if(a_clickfunc){  
					var l_func = a_ttfunc(i);
	  			cur_series.listen('pointClick', function(e) { 
	  				if(typeof(a_clickfunc)!="undefined") a_clickfunc(e);
	  			});
				} 
				*/ 				
				
			}  
			var ls_seriesname;
			if(a_seriesName){  
				ls_seriesname = (a_seriesName && a_seriesName[i] ? a_seriesName[i] : '');
				cur_series.name(ls_seriesname);
			}
			
			if(a_metadata){   
				var l_meta = a_metadata[i];   
				for(prop in l_meta){
					//alert(prop + ":" + l_meta[prop]);
					cur_series.meta(prop, l_meta[prop]);
				} 
			}
		}  

		var ls_scaleinterval = 1;
		if(a_scaleInterval) ls_scaleinterval = a_scaleInterval

		a_chart.yScale().minimum(0).ticks().interval(ls_scaleinterval);
		a_chart.old_dataset = anychart.data.set(a_data); 
		a_chart.draw(); 
	}
		
	_xChart.setTitle = function(a_chart, a_tit, a_size){
		var f_size = (a_size==false?16:a_size);
		title = a_chart.title();
		title.text(a_tit);
		title.fontWeight('bold');
		title.fontSize(16);
	}
	_xChart.setxAxis = function(a_chart, a_x){
		a_chart.xAxis().title().enabled(true).text(a_x); 
	}
	_xChart.setyAxis = function(a_chart, a_y){
		a_chart.yAxis().title().enabled(true).text(a_y); 
	}
    

anychart.onDocumentReady(function() { 
	_xChart.set_ready(); 
});
/**
 * Object 			: mighty-event-SlickGrid-1.2.9.js
 * @Description : Grid Event 처리 JavaScript
 * @author 			: 엑스인터넷정보 이상규
 * @since 			: 2016.12.23
 * @version 		: 1.2.9
 *
 * @Modification Information
 * <pre>
 *   since    	     author      Description
 *  ------------    --------    ----------------------------
 *   2013.02.10      김양열      RealGrid      ver 최초생성
 *   2015.07.20      이상규      SlickGrid-2.2 ver 생성
 *   2016.12.23      이상규      SlickGrid-2.3 ver 생성
 */
//============================================================== DataView event start
var xe_M_SlickGridRowCountChanged = function(a_obj, e, args) {
	if ( a_obj._MightyEventObj.div[0] !== "DeleteRow" && args.previous > args.current ) {
  	if ( !args.current ) {
  		/* 모든데이타 삭제 시 행 셀렉션 정보 초기화 */
			a_obj._Plugins[ "RowSelectionModel" ].setSelectedRows( [] );
		}
		else if ( a_obj._Filter && a_obj._MightyEventObj.div[0] == null ) {
			var activeCell = a_obj._SlickGrid.getActiveCell();
			if ( activeCell && activeCell.row + 1 > args.current ) {
				a_obj.SetRow( 1, activeCell.cell, false );
			}
		}
	}

	a_obj._SlickGrid.updateRowCount();

	/* main button event */
	switch ( a_obj._MightyEventObj.div[0] ) {
		case "Retrieve":
			if ( a_obj.IsGroup() ) {
				a_obj._DataView.sort( a_obj._SortInfo.comparer );
				a_obj._DataView.expandAllGroups();
			}

			// 조회 후 추가 시 insertAfter 되도록
			if ( a_obj._MightyEventObj.div[1] != "AddRow" ) {
				return;
			}
			// 의도된 break; 삭제

		case "InsertRow":
		case "AddRow":
		case "DuplicateRow":
		case "AddCurrent":
		case "AddChild":
			if ( a_obj._IsTree ) {
				a_obj.SetTreeDataCache();
			}

			a_obj._DataView.beginUpdate();

			var row = a_obj.GetRowById( a_obj._MightyEventObj.id );

			if ( a_obj._MightyEventObj.div[0] !== "DuplicateRow" ) {
				if ( typeof x_Insert_After !== "undefined" ) {
					x_Insert_After( a_obj, row );
				}
			}
			else {
				if ( typeof x_Duplicate_After !== "undefined" ) {
					x_Duplicate_After( a_obj, row );
				}
			}

			a_obj._DataView.endUpdate();
			break;

		case "DeleteRow":
			/* YSKIM 2018.07.05 추가 */
			if ( a_obj._IsTree ) {
				a_obj.SetTreeDataCache();
			}

			if ( !args.current ) {
				/* 모든데이타 삭제 시 행 셀렉션 정보 초기화 */
				a_obj._Plugins[ "RowSelectionModel" ].setSelectedRows( [] );
			}
	}
};

var xe_M_SlickGridRowsChanged = function(a_obj, e, args) {
	if ( a_obj._MightyEventObj.div[0] === "SetCellData" ) {
		if ( a_obj.IsGroup() ) {
			a_obj._SlickGrid.invalidateAllRows();
			a_obj.RenderProcess();
		} else {
			a_obj._SlickGrid.updateCell( args.rows[0], a_obj._MightyEventObj.cell );
		}
	}
	else if ( a_obj._MightyEventObj.div[0] === "SetRowData" ) {
		if ( a_obj.IsGroup() ) {
			a_obj._SlickGrid.invalidateAllRows();
			a_obj.RenderProcess();
		} else {
			a_obj._SlickGrid.updateRow( args.rows[0] );
		}
	}
	else {
		if ( a_obj._IsTree || a_obj.IsGroup() ) {
			a_obj._SlickGrid.invalidateAllRows();
		} else {
			a_obj._SlickGrid.invalidateRows( args.rows );
		}
		a_obj.RenderProcess();
	}
};
//============================================================== DataView event end

//============================================================== SlickGrid event start
//데이타 로딩 후
var xe_M_SlickGridDataLoad = function(a_obj) {
	a_obj._IsRetrieving = false;

	if ( typeof xe_BeforeGridDataLoad !== "undefined" ) {
		xe_BeforeGridDataLoad( a_obj );
	}

	if ( a_obj.RowCount() ) {
		if ( xe_M_SlickGridRowFocusChange( a_obj, 0, -1 ) ) {
			xe_M_SlickGridRowFocusChanged( a_obj, null, { "grid": a_obj._SlickGrid, "rows": [0] } );
		}
	}
	else {
		if ( a_obj._Share ) {
			a_obj.SyncObjectReset();
		}
	}

	if ( a_obj._IsTree ) {
		xe_M_PluginsHeaderMenuClicked( a_obj, null, { command: "filter-on", visibleNone: true } );
	}

	if ( typeof xe_GridDataLoad !== "undefined" ) {
		xe_GridDataLoad( a_obj );
	}
};

//셀 클릭 시
var xe_M_SlickGridItemClick = function(a_obj, e, args) {
	var rtValue = true,
			item    = a_obj.GetRowData( args.row + 1 );

	if ( item.__group || item.__groupTotals ) {

	}
	else if ( a_obj._CurrentRow !== args.row ) {
		rtValue = xe_M_SlickGridRowFocusChange( a_obj, args.row, a_obj._CurrentRow );
	}
	else if ( a_obj._CurrentCell !== args.cell ) {
		rtValue = xe_M_SlickGridItemFocusChange( a_obj, args.row, args.cell, a_obj._CurrentRow, a_obj._CurrentCell );
	}

	if ( rtValue ) {
		if ( typeof xe_GridItemClick !== "undefined" ) {
			if ( args.cell > 0 ) {
				xe_GridItemClick( a_obj, args.row + 1, args.cell, a_obj.GetColumnNameByIndex( args.cell ) );
			}
		}

		/* tree collapse and expand */
		if ( a_obj._IsTree ) {
			if ( $(e.target).hasClass( "toggle" ) ) {
				var item   = a_obj.GetRowData( args.row + 1 );
				var rtValue = true;

				if ( typeof xe_TreeBeforeCollapseExpand !== "undefined" ) {
					rtValue = xe_TreeBeforeCollapseExpand(a_obj, args.row + 1, item._collapsed || false);
				}

				if ( rtValue ) {
			    if ( item ) {
			      if ( !item._collapsed ) {
		        	item._collapsed = true;
			      } else {
		        	item._collapsed = false;
			      }

			    	if ( item._collapsed ) {
			    		var parents = a_obj.GetParents( a_obj.GetRow() );
			    		if ( parents.indexOf( args.row + 1 ) != -1 ) {
			    			a_obj.TreeCommitAndRender( args.row + 1 );
			    		}
			    	}

						a_obj.TreeCommitAndRender();
			    }
				}

				e.stopImmediatePropagation();
	    }
		}
		else if ( a_obj.IsGroup() && !item.__group && !item.__groupTotals ) {

		}
	}
	else {
		e.stopPropagation();
		e.stopImmediatePropagation();
	}

	/* CheckBox 클릭시 값 바로변경 */
	a_obj._ClickedColumn = args.cell;
	a_obj._ClickedTime   = Date.now();
};

var xe_M_SlickGridCheckBarClick = function (a_obj, e, args) {
	if ( typeof xe_GridCheckBarClick !== "undefined" ) {
		xe_GridCheckBarClick(a_obj, args.startRow + 1, args.endRow + 1, args.value, e.ctrlKey, e.altKey, e.shiftKey);
	}
}

var xe_M_SlickGridCheckBarHeaderClick = function (a_obj, e, args) {
	if ( typeof xe_GridCheckBarHeaderClick !== "undefined" ) {
		xe_GridCheckBarHeaderClick(a_obj, args.value);
	}
}

//셀 활성화 전
var xe_M_SlickGridBeforeEditCell = function (a_obj, e, args) {
	if ( a_obj._GridReadonly ) {
		return false;
	}
	else if ( args.column.readonly || !args.column.editable ) {
		return false;
	}
	else if ( args.column.getAutoReadOnly ) {
		var paramArgs = {
				"rowIdx" : args.row + 1,
				"colName": args.column.id,
				"value"  : args.item[ args.column.id ],
				"oriVal" : args.item[ args.column.id ],
				"dataContext": a_obj.GetRowData(args.row + 1)
		};

		if ( args.column.getAutoReadOnly( paramArgs ) ) {
			return false;
		}
	}
	else if ( a_obj._KeyCols[ args.column.field ] ) {
		return args.item.job === "I";
	}

	if ( args.column.dataType === "checkbox" ) {
		/* CheckBox 클릭시 값 바로변경 */
		if ( args.cell == a_obj._ClickedColumn ) {
			a_obj._ClickedColumn = -1;

			if ( Date.now() - a_obj._ClickedTime <= 100 ) {
			  var colName = a_obj.GetColumnNameByIndex( args.cell );
				var item    = a_obj.GetRowData( args.row + 1 );
				var value   = item[ colName ] === "Y" ? "N" : "Y";
				var oldval  = item[ colName ];

				a_obj.SetItem(args.row + 1 , colName, value);

				if ( typeof xe_GridDataChanged !== "undefined" ) {
					xe_GridDataChanged(a_obj, args.row + 1, colName, value, oldval);
				}
			}
		}
	}
};

//행 선택 변경 중
var xe_M_SlickGridRowFocusChange = function(a_obj, newRowIdx, oldRowIdx) {
	if ( newRowIdx != null ) {
		if ( typeof xe_GridRowFocusChange !== "undefined" ) {
			var rtValue = xe_GridRowFocusChange( a_obj, newRowIdx + 1, oldRowIdx + 1 );

			return rtValue == null || rtValue;
		}
	}
	return true;
};

//셀 선택 변경 중
var xe_M_SlickGridItemFocusChange = function(a_obj, newRowIdx, newColIdx, oldRowIdx, oldColIdx ) {
	if ( typeof xe_GridItemFocusChange !== "undefined" ) {
		var rtValue = xe_GridItemFocusChange( a_obj, newRowIdx + 1, a_obj.GetColumnNameByIndex( newColIdx ), oldRowIdx + 1, a_obj.GetColumnNameByIndex( oldColIdx ) );

		return rtValue == null || rtValue;
	}
	return true;
};

//행 선택 변경 후
var xe_M_SlickGridRowFocusChanged = function(a_obj, e, args) {
	/* retrieve */
	if ( a_obj._IsRetrieving ) {
		return;
	}

	if ( a_obj._Share ){
		x_GridRowSync( a_obj, args.rows[0] + 1 );
	}

	/* xe_GridRowFocusChanged */
	if ( a_obj._CurrentRow !== args.rows[0] || args.isFocusChanged ) {
		if ( typeof args.rows[0] !== "undefined" ) {
			if ( typeof xe_GridRowFocusChanged !== "undefined" ) {
				xe_GridRowFocusChanged( a_obj, args.rows[0] + 1 );
			}
		}

		a_obj._OldRow 	  = a_obj._CurrentRow;
		a_obj._CurrentRow = args.rows[0];
	}

	var activeCell = args.grid.getActiveCell();
	xe_M_SlickGridItemFocusChanged( a_obj, null, { args: { row: a_obj._CurrentRow, cell: activeCell && activeCell.cell || 1 } });
};

//셀 선택 변경 후
var xe_M_SlickGridItemFocusChanged = function(a_obj, e, args) {
	if ( typeof xe_GridItemFocusChanged !== "undefined" ) {
		xe_GridItemFocusChanged( a_obj, args.row + 1, a_obj.GetColumnNameByIndex( args.cell || 1 ) );
	}

	a_obj._OldCell     = a_obj._CurrentCell;
	a_obj._CurrentCell = args.cell || 1;
};

//셀의 값 변경 시
var xe_M_SlickGridDataChanged = function(a_obj, e, args) {
	var column 	 = args.grid.getColumns(),
  		dataType = column[ args.cell ].dataType,
  		colName	 = column[ args.cell ].id,
  		editor 	 = args.grid.getCellEditor( args.row, args.cell );

	if ( args.item.job === "" || args.item.job === "N" ) {
		args.item.job = "U";
	}

	if ( a_obj._Share ) {
		$(".freeform").find( "#" + colName ).val( args.item[ colName ] );
	}

//	if ( a_obj.IsGroup() ) {
//		if ( a_obj._SlickEventObj.indexOf( "onCellChange" ) == 0 ) {
//  		a_obj._DataView.refresh();
//			a_obj.SetRow( a_obj.GetRowById( args.item.id ), null, false );
//  	}
//	}

	if ( column[ args.cell ].merge ) {
		var $cellNode        = $(args.grid.getCellNode( args.row, args.cell )),
		    $preRowCellNode  = $(args.grid.getCellNode( args.row - 1, args.cell )),
		    $nextRowCellNode = $(args.grid.getCellNode( args.row + 1, args.cell ));

		if ( $cellNode.hasClass("merged-top") ) {
			$cellNode.removeClass("merged-top");

			if ( $nextRowCellNode.hasClass("merged-bottom") ) {
				$nextRowCellNode.removeClass("merged-bottom");
			} else {
				$nextRowCellNode.removeClass("merged")
												.addClass("merged-top");
			}
		}
		else if ( $cellNode.hasClass("merged-bottom") ) {
			$cellNode.removeClass("merged-bottom");

			if ( $preRowCellNode.hasClass("merged-top") ) {
				$preRowCellNode.removeClass("merged-top");
			} else {
				$preRowCellNode.removeClass("merged")
											 .addClass("merged-bottom");
			}
		}
		else if ( $cellNode.hasClass("merged") ) {
			$cellNode.removeClass("merged");

			if ( $preRowCellNode.hasClass("merged-top") ) {
				$preRowCellNode.removeClass("merged-top");
			} else {
				$preRowCellNode.removeClass("merged")
											 .addClass("merged-bottom");
			}

			if ( $nextRowCellNode.hasClass("merged-bottom") ) {
				$nextRowCellNode.removeClass("merged-bottom");
			} else {
				$nextRowCellNode.removeClass("merged")
												.addClass("merged-top");
			}
		}
		else {
			var m             = column[ args.cell ],
					preRowItem    = args.row > 0 ? args.grid.getDataItem( args.row - 1 ) : null,
			    nextRowItem   = args.row < args.grid.getDataLength() - 1 ? args.grid.getDataItem( args.row + 1 ) : null,
			    itemValue     = "",
      		preItemValue  = "",
      		nextItemValue = "";

    	for (var i = 0, ii = m.mergedColumns.length; i < ii; i++) {
    		itemValue     += args.item[ m.mergedColumns[i] ];
      	preItemValue  += preRowItem && preRowItem[ m.mergedColumns[i] ] || "";
      	nextItemValue += nextRowItem && nextRowItem[ m.mergedColumns[i] ] || "";
    	}

			if ( itemValue === preItemValue ) {
				if ( $preRowCellNode.hasClass("merged-bottom") ) {
					$preRowCellNode.removeClass("merged-bottom")
												 .addClass("merged");
				} else {
					$preRowCellNode.addClass("merged-top");
				}

				if ( itemValue === nextItemValue ) {
					$cellNode.addClass("merged");
				} else {
					$cellNode.addClass("merged-bottom");
				}
			}

			if ( itemValue === nextItemValue ) {
				if ( $nextRowCellNode.hasClass("merged-top") ) {
					$nextRowCellNode.removeClass("merged-top")
												  .addClass("merged");
				} else {
					$nextRowCellNode.addClass("merged-bottom");
				}

				if ( itemValue !== preItemValue ) {
					$cellNode.addClass("merged-top");
				}
			}
		}
	}

	if ( dataType !== "checkbox" ) {
		if( typeof xe_GridDataChanged !== "undefined" ) {
			xe_GridDataChanged( a_obj, args.row + 1, colName, args.item[ colName ], editor && editor.getOldValue() );
		}
	}
};

//셀 더블클릭 시
var xe_M_SlickGridItemDoubleClick = function(a_obj, e, args) {
	/* tree collapse and expand */
	if ( a_obj._IsTree ) {
		if ( $(e.target).hasClass( "toggle" ) ) {
			e.stopImmediatePropagation();
    }
	}

	var item = a_obj.GetRowData( args.row + 1 );

	if ( !item.__group && !item.__groupTotals ) {
		if ( typeof xe_GridItemDoubleClick !== "undefined" ) {
			xe_GridItemDoubleClick( a_obj, args.row + 1, args.cell, a_obj.GetColumnNameByIndex(args.cell), args.cell );
		}
	}
};

//헤더 클릭 시
var xe_M_SlickGridHeaderClick = function(a_obj, e, args) {
	if ( typeof xe_GridHeaderClick !== "undefined" ) {
		xe_GridHeaderClick( a_obj, args.column.id );
	}
};

//버튼 클릭 시
var xe_M_SlickGridButtonClick = function(a_obj, rowIdx, colIdx) {
	if ( typeof xe_GridButtonClick !== "undefined" ) {
		xe_GridButtonClick(a_obj, rowIdx + 1, a_obj.GetColumnNameByIndex( colIdx ), colIdx );
	}
};

//파일 버튼 클릭 시
var xe_M_SlickGridFileButtonClick = function(div, a_obj, rowIdx, colIdx) {
	var rtValue = true;

	if ( typeof xe_FileButtonClick !== "undefined" ) {
		rtValue = xe_FileButtonClick( a_obj, rowIdx + 1, a_obj.GetColumnNameByIndex(colIdx), div );
	}

	if ( rtValue ) {
		if ( div === "upload" ) {
			a_obj.FileUpload(rowIdx + 1, colIdx);
		}
		else if ( div === "download" ) {
			a_obj.FileDownload(rowIdx + 1, colIdx);
		}
		else if ( div === "delete" ) {
			a_obj.FileDelete(rowIdx + 1, colIdx);
		}
	}
};

//파일 선택 후
var xe_M_SlickGridFileChanged = function(a_obj, row, cell, changeEle) {
	var $changeEle = $(changeEle);
	var file       = $changeEle[0].files[0];
	var column     = a_obj.GetColumn( a_obj.GetColumnNameByIndex(cell) );
	var fileInfo   = column.fileInfo;

	if ( file ) {
		a_obj._FilesSize += file.size - $changeEle.data("fileSize");

		var fileType = a_obj.FileTypeConverter( file.type )[0];

		if ( fileType === "" ) {
			fileType = file.name.substr(file.name.lastIndexOf("."));
		}

		if ( fileInfo ) {
			if ( fileInfo.fileOrignNameField ) {
				a_obj.SetItem(row, fileInfo.fileOrignNameField, file.name);
			}

			if ( fileInfo.fileSizeField ) {
				a_obj.SetItem(row, fileInfo.fileSizeField, file.size);
			}

			if ( fileInfo.fileTypeField ) {
				a_obj.SetItem(row, fileInfo.fileTypeField, fileType);
			}
		}

		if ( typeof xe_FileChanged !== "undefined" ) {
			xe_FileChanged( a_obj, row, column.id, file.name, file.size, fileType, firstData );
		}
	}
	else {
		a_obj._FilesSize -= $changeEle.data("fileSize");

		var firstData = $changeEle.data("firstData");

		a_obj.SetItem(row, column.id, firstData[ column.id ]);

		if ( fileInfo ) {
			if ( fileInfo.fileOrignNameField ) {
				a_obj.SetItem(row, fileInfo.fileOrignNameField, firstData[ fileInfo.fileOrignNameField ]);
			}

			if ( fileInfo.fileSizeField ) {
				a_obj.SetItem(row, fileInfo.fileSizeField, firstData[ fileInfo.fileSizeField ]);
			}

			if ( fileInfo.fileTypeField ) {
				a_obj.SetItem(row, fileInfo.fileTypeField, firstData[ fileInfo.fileTypeField ]);
			}
		}
	}
}

//파일 업로드 후
var xe_M_SlickGridFileUploaded = function(a_obj, uploadInfo) {
	for ( var i = 0, ii = uploadInfo.length; i < ii; i++ ) {
		uploadInfo[i].cell = a_obj.GetColumnNameByIndex( uploadInfo[i].cell );

		var column = a_obj.GetColumn( uploadInfo[i].cell );
		if ( column.fileInfo && column.fileInfo.fileSaveNameField ) {
			a_obj.SetItem( uploadInfo[i].row, column.fileInfo.fileSaveNameField, uploadInfo[i].STRE_FILE_NM, false );
		}

		a_obj.SetItem( uploadInfo[i].row, uploadInfo[i].FILE_ID_FIELD, uploadInfo[i].ATCH_FILE_ID, false );

		delete a_obj._FileUploadInfo[ uploadInfo[i].row ];
	}

	var errorMsg = "";
	for ( var prop in a_obj._FileUploadInfo ) {
		if ( a_obj._FileUploadInfo.hasOwnProperty(prop) ) {
			errorMsg += prop + ", ";
		}
	}

	if ( errorMsg !== "" ) {
		_X.MsgBox("확인", errorMsg.substr(0, errorMsg.length - 2) + " 행의 파일이 저장되지 않았습니다.");
	}

	if ( typeof xe_FileUploaded !== "undefined" ) {
		rtValue = xe_FileUploaded( a_obj, uploadInfo );
	}
}

//파일 삭제 후
var xe_M_SlickGridFileDeleted = function(a_obj, deleteInfo) {
	var column = a_obj.GetColumn( a_obj.GetColumnNameByIndex(deleteInfo.cell) );

	a_obj.SetItem(deleteInfo.row, column.id, "", false);

	if ( column.fileInfo ) {
		if ( column.fileInfo.fileIdField ) {
			a_obj.SetItem(deleteInfo.row, column.fileInfo.fileIdField, "", false);
		}

		if ( column.fileInfo.fileOrignNameField ) {
			a_obj.SetItem(deleteInfo.row, column.fileInfo.fileOrignNameField, "", false);
		}

		if ( column.fileInfo.fileSaveNameField ) {
			a_obj.SetItem(deleteInfo.row, column.fileInfo.fileSaveNameField, "", false);
		}

		if ( column.fileInfo.fileSizeField ) {
			a_obj.SetItem(deleteInfo.row, column.fileInfo.fileSizeField, "", false);
		}

		if ( column.fileInfo.fileTypeField ) {
			a_obj.SetItem(deleteInfo.row, column.fileInfo.fileTypeField, "", false);
		}
	}

	if ( typeof xe_FileDeleted !== "undefined" ) {
		xe_FileDeleted( a_obj, deleteInfo.row, column.id );
	}
}

//키 클릭 시
var xe_M_SlickGridKeyDown = function(a_obj, e, args) {
	if ( typeof xe_GridBeforeKeyDown !== "undefined" ) {
		var rtValue = xe_GridBeforeKeyDown( a_obj, e, e.which, e.ctrlKey, e.altKey, e.shiftKey );

		if ( rtValue === 0 ) {
			e.stopImmediatePropagation();
			return;
		}
	}

	var column = args.grid.getColumns();

  /* 에디터 number 타입 제어 */
	if ( column[ args.cell ] && column[ args.cell ].editor === Slick.Editors.Integer ) {
		//35: KEY_END, 36: KEY_HOME, 110: KEY_COMMA, 190: RIGHT_KEY_COMMA
		var keys = [ KEY_BACKSPACE, KEY_TAB, KEY_ENTER, KEY_SHIFT, KEY_CONTROL, KEY_ALT, 35, 36, KEY_UP, KEY_DOWN, KEY_LEFT, KEY_RIGHT, KEY_DELETE, 107, 109, 110, 189, 190 ];

		//48: 숫자0, 57: 숫자9, 96: 오른쪽숫자0, 105: 오른쪽숫자9
		if ( (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105) && keys.indexOf( e.which ) === -1 ) {
			if ( !( e.ctrlKey &&
						( e.which === KEY_a || e.which === KEY_c      || e.which === KEY_d  || e.which === KEY_e    || e.which === KEY_i ||
						  e.which === KEY_p || e.which === KEY_r      || e.which === KEY_s  || e.which === KEY_v    || e.which === KEY_y ||
						  e.which === KEY_z || e.which === KEY_DELETE || e.which === KEY_F4 ) ) ) {

				var activeCellNode = args.grid.getCellNode( args.row, args.cell ),
  					viewport = args.grid.getViewport(),
						cellbox	 = args.grid.getCellNodeBox( args.row, args.cell ),
						point		 = ( viewport.rightPx - cellbox.right ) > ( cellbox.left - viewport.leftPx );

  			e.stopImmediatePropagation();
				clearTimeout( a_obj._SetTimeId );

				$(activeCellNode).removeClass( "keydown-invalid" );
        $(activeCellNode).width();
        $(activeCellNode).addClass( "keydown-invalid" );

				a_obj._SetTimeId = setTimeout( function() { $(activeCellNode).removeClass( "keydown-invalid" ); }, 200 );
			}
		}
		//109: - 입력, 110: KEY_COMMA, 189: - 입력, 190: RIGHT_KEY_COMMA
		else if ( column[ args.cell ].dataType.indexOf("number") !== -1 ) {
			if ( e.which === 107 ) {
				e.stopImmediatePropagation();

				var editor = args.grid.getCellEditor( args.row, args.cell );
				var colVal = editor.serializeValue();
				var newVal = null;

				if ( typeof colVal === "string" ) {
					newVal = Number( colVal.replace( /,/gi, "" ) ) * 1000;
				} else if ( typeof colVal === "number" ) {
					newVal = colVal * 1000;
				}

				if ( newVal ) {
					editor.setValue( newVal );
				}
			}
			else if ( e.which === 110 || e.which === 190 ) {
				if ( column[ args.cell ].dataType === "number" ) {
					e.stopImmediatePropagation();
				}
				else {
					var editor = args.grid.getCellEditor( args.row, args.cell );
					if ( editor.serializeValue().indexOf(".") !== -1 ) {
						e.stopImmediatePropagation();
					}
				}
			}
			else if ( e.which === 109 || e.which === 189 ) {
				var editor = args.grid.getCellEditor( args.row, args.cell );
				var colVal = editor.serializeValue();
				var newVal = null;

				if ( colVal != 0 && String(colVal || "").length > 0 ) {
					e.stopImmediatePropagation();

					if ( typeof colVal === "string" ) {
						newVal = Number( colVal.replace( /,/gi, "" ) ) * -1;
					} else if ( typeof colVal === "number" ) {
						newVal = colVal * -1;
					}

					if ( newVal ) {
						editor.setValue( newVal );
					}
				}
			}
		}
	}

  /* MaxLength 체크 */
	if ( column[ args.cell ] && column[ args.cell ].maxLength > 0 && e.which !== KEY_DELETE && e.which !== KEY_BACKSPACE && e.which !== KEY_TAB && e.which !== KEY_ENTER ) {
 		var editor = args.grid.getCellEditor( args.row, args.cell );
 		var cdata  = editor.serializeValue();
		if ( cdata && _X.ByteLen(cdata) >= column[ args.cell ].maxLength && !isTextSelected() ) {
			e.stopImmediatePropagation();
			return;
		}
	}

	if ( typeof xe_GridKeyDown !== "undefined" ) {
		xe_GridKeyDown( a_obj, e, e.which, e.ctrlKey, e.altKey, e.shiftKey );
	}
};

//컬럼 정렬 시
var xe_M_SlickGridSort = function(a_obj, e, args) {
	/* multi */
	if ( args.multiColumnSort ) {
		var columns = args.sortCols;

		if ( columns.length ) {
			var sameCount = 0;

			/* 정렬 정보가 변경되었는지 확인 */
			if ( a_obj._SortInfo && a_obj._SortInfo.sortColumns ) {
				if ( a_obj._SortInfo.sortColumns.length === args.sortCols.length ) {

					for ( var i = 0, ii = a_obj._SortInfo.sortColumns.length; i < ii; i++ ) {

						if ( a_obj._SortInfo.sortColumns[i].sortCol.field === args.sortCols[i].sortCol.field ) {
							if ( a_obj._SortInfo.sortColumns[i].sortAsc === args.sortCols[i].sortAsc ) {
								sameCount++;
							}
							else {
								break;
							}
						}
						else {
							break;
						}
					}
				}
			}

			if ( sameCount !== args.sortCols.length ) {
				a_obj.Sort( columns );
			}
		}
		else {
			a_obj.ResetSort();
		}
	}
	/* single */
	else {
		var columns = [{
				"sortAsc": args.sortAsc,
				"sortCol": {
						"field": args.sortCol.field
				}
		}];

		if ( !args.sortAsc || a_obj._SortInfo.sortColName !== columns[0].sortCol.field ) {
			a_obj.Sort( columns );
		}
		else if ( args.sortAsc ) {
			a_obj.ResetSort();
		}
	}

	var sortInfo = [];
	for ( var i = 0, ii = a_obj._SortInfo.sortColumns.length; i < ii; i++ ) {
		sortInfo.push({
				"sortAsc": a_obj._SortInfo.sortColumns[i].sortAsc,
				"sortCol": a_obj._SortInfo.sortColumns[i].sortCol.field
		});
	}

	if ( typeof xe_GridSortChanged !== "undefined" ) {
		xe_GridSortChanged(a_obj, sortInfo);
	}
};

var xe_M_SlickGridColumnsReordered = function (a_obj, e, args) {
	a_obj._FirstColName 		= null;
	a_obj._FirstEditColName = null;
	x_SetFirstColumn( a_obj );

	if ( a_obj.IsGroup() ) {
		a_obj.SetSortOrder();
	}
};

var xe_M_SlickGridColumnsResized = function (a_obj, e, args) {

};

var xe_M_SlickGridHeaderRowCellRendered = function(a_obj, e, args) {
	$(args.node).empty();

	var $headerRowCell;

	if ( args.column.field === "num" ) {
		$headerRowCell = $("<div class='slick-filter-title'>Filter</div>").appendTo( args.node );
	}
	else if ( args.column.field !== "check" ) {
		$headerRowCell = $("<input type='text' id='filter_" + a_obj.id + "_" + args.column.field + "' class='" + a_obj.id + "-filter-input'>")
				.data( "columnId", args.column.id )
				.val( a_obj._ColumnFilters[ args.column.id ] )
				.appendTo( args.node );
	}
};

var xe_M_SlickValidationError = function(a_obj, e, args) {
	if ( typeof xe_GridDataChangingError2 !== "undefined" ) {
		var result = xe_GridDataChangingError2( a_obj, args.row, args.column.field, args.validationResults.msg );

		if ( result === 100 ) {
			if ( args.validationResults ) {
				if ( args.validationResults.msg ) {
					_X.MsgBox( "확인", args.validationResults.msg );
				}
			}

			args.editor.setValue( args.editor.getOldValue() );
		}
	}
};

//============================================================== SlickGrid event end

//============================================================== Plugins event start
//헤더메뉴 클릭 시
var xe_M_PluginsHeaderMenuClicked = function( a_obj, e, args ) {
	/* 정렬 시 */
	if ( args.command === "sort-asc" || args.command === "sort-desc" ) {
		var sortAsc = args.command === "sort-asc",
				colName = args.column.field;

		if ( !a_obj._SortInfo.sortColumns ) {
			/* 신규 */
			var columns = [{
					"sortAsc": sortAsc,
					"sortCol": {
							"field": colName
					}
			}];

			a_obj._SlickGrid.setSortColumn( colName, sortAsc );
			a_obj.Sort( columns );
		}
		else {
			var sortColumns = a_obj._SlickGrid.getSortColumns(),
					isSorted    = false,
					columns			= [];

			/* 이미 정렬된 컬럼인지 확인 */
			for ( var i = 0, ii = sortColumns.length; i < ii; i++ ) {
				if ( sortColumns[i].columnId === colName ) {
					isSorted = true;
					break;
				}
			}

			/* 변경 */
		  if ( isSorted ) {
		  	var sortColumnsInfo = [];

				/* 정렬 정보 생성 (a_obj._SlickGrid.setSortColumns 함수 인자로 사용)*/
				for ( var i = 0, ii = sortColumns.length; i < ii; i++ ) {
					var sortInfo;

					if ( sortColumns[i].columnId === colName ) {
						sortInfo = {
			  				"columnId": colName,
								"sortAsc" : sortAsc
						};
					}
					else {
			  		sortInfo = {
			  				"columnId": sortColumns[i].columnId,
								"sortAsc" : sortColumns[i].sortAsc
						};
					}

					sortColumnsInfo.push( sortInfo );
		  	}

		  	a_obj._SlickGrid.setSortColumns( sortColumnsInfo );

		  	/* 정렬 정보 생성 (a_obj.Sort 함수 인자로 사용) */
				for ( var i = 0, ii = sortColumnsInfo.length; i < ii; i++ ) {
		  		var columnInfo = {
		  				"sortAsc": sortColumnsInfo[i].sortAsc,
							"sortCol": {
									"field": sortColumnsInfo[i].columnId
							}
					};

					columns.push( columnInfo );
		  	}
		  }
		  /* 추가 */
			else {
				/* 정렬 정보 생성 (a_obj._SlickGrid.setSortColumns 함수 인자로 사용)*/
				var sortColumnsInfo = [];

				for ( var i = 0, ii = sortColumns.length; i < ii; i++ ) {
		  		var sortInfo = {
		  				"columnId": sortColumns[i].columnId,
							"sortAsc" : sortColumns[i].sortAsc
					};

					sortColumnsInfo.push( sortInfo );

					/* 정렬 정보 생성 (a_obj.Sort 함수 인자로 사용) */
					var columnInfo = {
							"sortAsc": sortColumns[i].sortAsc,
							"sortCol": {
									"field": sortColumns[i].columnId
							}
					};

					columns.push( columnInfo );
		  	}

		  	var newSortInfo = {
			  		"columnId": colName,
			  		"sortAsc" : sortAsc
		  	};

				sortColumnsInfo.push( newSortInfo );

				a_obj._SlickGrid.setSortColumns( sortColumnsInfo );

				/* 정렬 정보 추가 (a_obj.Sort 함수 인자로 사용) */
				var newColumnInfo = {
						"sortAsc": sortAsc,
						"sortCol": {
								"field": colName
						}
				};

				columns.push( newColumnInfo );
			}

			a_obj.Sort( columns );
		}

		var sortInfo = [];
		for ( var i = 0, ii = a_obj._SortInfo.sortColumns.length; i < ii; i++ ) {
			sortInfo.push({
					"sortAsc": a_obj._SortInfo.sortColumns[i].sortAsc,
					"sortCol": a_obj._SortInfo.sortColumns[i].sortCol.field
			});
		}

		if ( typeof xe_GridSortChanged !== "undefined" ) {
			xe_GridSortChanged(a_obj, sortInfo);
		}
	}
	/* 정렬 삭제 시 */
	else if ( args.command === "sort-none" ) {
		var sortColumns   	= a_obj._SlickGrid.getSortColumns(),
				isDelete      	= false,
				sortColumnsInfo = [],
				columns					= [];

		for ( var i = 0, ii = sortColumns.length; i < ii; i++ ) {
			/* 정렬 삭제 컬럼 확인 */
			if ( sortColumns[i].columnId === args.column.field ) {
				isDelete = true;
			}
			else {
				/* 정렬 정보 생성 (a_obj._SlickGrid.setSortColumns 함수 인자로 사용)*/
				var sortInfo = {
	  				"columnId": sortColumns[i].columnId,
						"sortAsc" : sortColumns[i].sortAsc
				};

				sortColumnsInfo.push( sortInfo );

				/* 정렬 정보 추가 (a_obj.Sort 함수 인자로 사용) */
				var columnInfo = {
						"sortAsc": sortColumns[i].sortAsc,
						"sortCol": {
								"field": sortColumns[i].columnId
						}
				};

				columns.push( columnInfo );
			}
		}

		if ( isDelete ) {
			a_obj.ResetSort( false );
			a_obj._SlickGrid.setSortColumns( sortColumnsInfo );
			a_obj.Sort( columns );
		}

		var sortInfo = [];
		for ( var i = 0, ii = a_obj._SortInfo.sortColumns.length; i < ii; i++ ) {
			sortInfo.push({
					"sortAsc": a_obj._SortInfo.sortColumns[i].sortAsc,
					"sortCol": a_obj._SortInfo.sortColumns[i].sortCol.field
			});
		}

		if ( typeof xe_GridSortChanged !== "undefined" ) {
			xe_GridSortChanged(a_obj, sortInfo);
		}
	}
	/* 필터 시 */
	else if ( args.command === "filter-on" ) {
		a_obj.GridCommit();

		if ( !args.visibleNone ) {
			a_obj._SlickGrid.setHeaderRowVisibility( true );
			a_obj._SlickGrid.setColumns( a_obj._SlickGrid.getColumns() );
		}

		/* 필터 */
		if ( !a_obj._Filter ) {
			a_obj._Filter = function(item, filterArgs) {
				a_obj.GridCommit();

				/* Tree */
				if ( a_obj._IsTree ) {
					if ( item.currentTreeData ) {
						var parents = item.currentTreeData.parents;

	      		while ( a_obj._TreeData[ parents ] ) {
	    				if ( a_obj._TreeData[ parents ].item && a_obj._TreeData[ parents ].item._collapsed ) {
					    	return false;
					    }

					    parents = a_obj._TreeData[ parents ].parents;
	      		}
					}
        }

        /* basic */
		    for ( var colName in a_obj._ColumnFilters ) {
		    	if ( colName === "num" || colName === "items" || !a_obj._ColumnFilters.hasOwnProperty( colName ) ) {
		    		continue;
		    	}

		    	if ( colName !== undefined && a_obj._ColumnFilters[ colName ] !== "" ) {
		        var c = a_obj.GetColumns()[ a_obj.GetColumnIndexByName( colName ) ];

		        if ( c.dataType === "text" ) {
		        	/* 콤보박스 */
		        	if ( c.comboData ) {
		        		if ( String( c.comboData.syncLabel[ item[ c.field ] ] ).indexOf( a_obj._ColumnFilters[ colName ] ) === -1 ) {
				          return false;
				        }
		        	}
		        	/* 문자 */
		        	else {
		        		if ( String( item[ c.field ] ).indexOf( a_obj._ColumnFilters[ colName ] ) === -1 ) {
				         	return false;
				        }
		        	}
		        }
		        else if ( c.dataType === "checkbox" ) {
		        	/* 체크박스 */
		        	if ( item[ c.field ] != a_obj._ColumnFilters[ colName ].toUpperCase() ) {
			          return false;
			        }
		        }
		        else if ( c.dataType === "date" || c.dataType === "datetime" ) {
		        	var d1 = _X.strPurify( item[ c.field ] ),
		        			d2 = _X.strPurify( a_obj._ColumnFilters[ colName ] );

		        	/* 날짜 */
		        	if ( d1.indexOf( d2 ) === -1 ) {
			          return false;
			        }
		        }
		        else if ( c.dataType === "number" ) {
		        	/* 정수 */
		        	if ( String( item[ c.field ] ).indexOf( a_obj._ColumnFilters[ colName ] ) === -1 ) {
			          return false;
			        }
		        }
		        else if ( c.dataType.indexOf( "numberf" ) !== -1 ) {
		        	/* 실수 */
		        	if ( String( item[ c.field ] ).indexOf( a_obj._ColumnFilters[ colName ] ) === -1 ) {
			          return false;
			        }
		        }
		      }
	    	}

				if ( a_obj._ColumnFilters.items ) {
					a_obj._ColumnFilters.items[ a_obj._ColumnFilters.items.length ] = item;
				}

		    return true;
		  }
		}

		a_obj._DataView.beginUpdate();
	  a_obj._DataView.setFilter( a_obj._Filter );
  	a_obj._DataView.endUpdate();

  	/* 필터 이벤트 */
  	if ( !a_obj._IsTree ) {
			/* basic */
			if ( !a_obj._ColumnFilters.length ) {
				var headerRow = a_obj._SlickGrid.getHeaderRow();
				$([headerRow.left, headerRow.right]).delegate(":input", "change keyup", function (e) {
			    var colName = $(this).data( "columnId" );

			    if ( colName != null ) {
			    	a_obj._ColumnFilters.items = [];
			      a_obj._ColumnFilters[ colName ] = $.trim( $(this).val() );
			      a_obj._DataView.refresh();
			      a_obj.RenderProcess();
			    }
			  });
			}
		}

		if ( !args.visibleNone ) {
			$("#filter_" + a_obj.id + "_" + args.column.field).focus();
			a_obj.SetSortOrder();
		}

		if ( typeof xe_FilterOnOff_After !== "undefined" ) {
			xe_FilterOnOff_After(a_obj, true);
		}
	}
	/* 필터 해제 시 */
	else if ( args.command === "filter-off" ) {
		$("." + a_obj.id + "-filter-input").val("");
		a_obj._ColumnFilters = {};

		a_obj._DataView.beginUpdate();
	  a_obj._DataView.setFilter( null );
  	a_obj._DataView.endUpdate();

		a_obj.RenderProcess();
		a_obj._SlickGrid.setHeaderRowVisibility( false );

		if ( typeof xe_FilterOnOff_After !== "undefined" ) {
			xe_FilterOnOff_After(a_obj, false);
		}
	}
};

var xe_M_SlickGridScroll = function(a_obj, e, args) {
	if ( typeof xe_Scroll !== "undefined" ) {
		var rtValue = xe_Scroll( a_obj, e, args );
	}
};

//추가(2017.05.25 KYY)
var xe_M_SlickGridRowRendered = function(a_obj, e, args) {
	if ( typeof xe_RowRendered !== "undefined" ) {
		//var rtValue = xe_RowRendered( a_obj, e, e.which, e.ctrlKey, e.altKey, e.shiftKey );
	}
};

var xe_M_SlickGridViewportChanged = function(a_obj, e, args) {
	if ( typeof xe_ViewportChanged !== "undefined" ) {
		var rtValue = xe_ViewportChanged( a_obj, e, args );
	}
};


//============================================================== Plugins event end

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.oreilly.servlet.MultipartRequest,
	com.oreilly.servlet.multipart.DefaultFileRenamePolicy,
	java.util.*,
	java.io.*,
	jxl.*,
	xgov.core.dao.*,
	xgov.core.env.XLog,
	org.apache.poi.xssf.usermodel.*,
	org.apache.poi.hssf.usermodel.*,java.text.SimpleDateFormat,
	java.text.DecimalFormat"
%>

<%
	String ls_rep 	= request.getParameter("rep");
	String ls_fname = request.getParameter("fname");
	String ls_cust 	= request.getParameter("cust");
	String ls_dept 	= request.getParameter("dept");
	String ls_ym 		= request.getParameter("ym");

  String ls_dept_name = "";
  String ls_service_code = "";
  String ls_addr = "";
  String ls_biz_no = "";
  String ls_owner_name = "";
  String ls_cust_id = "";
  String ls_cust_name = "";
  String ls_manageno = "";
  String ls_fromdate = "";
  String ls_todate = "";
  String ls_service_amt = "";
  String ls_req_amt = "";
  String ls_pay_amt = "";
  String ls_b_amt = "";
  String ls_total_amt = "";
  String ls_pay_b_amt = "";

  String _CSep = "¸";

  String ls_params  = ls_dept+_CSep+ls_ym+_CSep+ls_cust;
	String[] typeData = xgov.core.dao.XDAO.XmlSelect(request, "array", "gs", "GS01080", "GUBYEBIYONGXLS_R01", ls_params, "all", "˛", "¸").split("˛");
	//StringBuffer sbType = new StringBuffer(10240);

	if(typeData[0]==null || typeData[0].equals("")){
		out.println("<script type=\'text/javascript'>");
		out.println("alert('데이터가 존재하지 않습니다');");
		out.println("</script>");
	}else{
		for(int i=0; i<typeData.length; i++) {
			String[] tyData = typeData[i].split("¸");
			ls_dept_name    = tyData[0];
			ls_service_code = tyData[1];
			ls_addr         = tyData[2];
			ls_biz_no       = tyData[3];
			ls_owner_name   = tyData[4];
			ls_cust_id      = tyData[5];
			ls_cust_name    = tyData[6];
			ls_manageno     = tyData[7];
			ls_fromdate     = tyData[10];
			ls_todate       = tyData[11];
			ls_service_amt  = tyData[12];
			ls_req_amt      = tyData[13];
			ls_pay_amt      = tyData[14];
			ls_b_amt        = tyData[15];
			ls_total_amt    = tyData[16];
			ls_pay_b_amt    = tyData[17];
			//System.out.println("ls_cust_name: [" + ls_cust_name + "]");
			}
		}

	String realFolder    = xgov.core.env.XConfiguration.getString("Globals.docPath");
	//System.out.println("realFolder: [" + realFolder + "]");
	String fileName  = ls_fname +'_'+ ls_cust_name + ".xml";
	String filePath  = realFolder+"tmp"+ls_rep;
	//String filePath  = "D:/Projects/GTS_X5/ROOT/APPS/gs/doc/"+"tmp"+ls_rep;
	String ls_filePath  = realFolder+ls_rep;
	//String ls_filePath  = "D:/Projects/GTS_X5/ROOT/APPS/gs/doc/"+ls_rep;

	//System.out.println("ls_rep: [" + ls_rep + "]");
	//String filePath1 = "D:/Projects/GTS_X5/ROOT/APPS/gs/doc/rpt_00010.hml";
  //String UTF8FilePath1 = new String(filePath1.getBytes("8859_1"), "UTF-8");
  String UTF8FilePath = new String(filePath.getBytes("8859_1"), "UTF-8");

  // 파일읽기
  java.io.BufferedReader reader = new java.io.BufferedReader(new java.io.InputStreamReader(new java.io.FileInputStream(ls_filePath),"UTF-8"));

  java.io.BufferedWriter fw = new java.io.BufferedWriter(new OutputStreamWriter(new FileOutputStream(UTF8FilePath), "UTF-8"));

	String line = null;

	while( (line = reader.readLine() )!=null){ //라인단위 읽기
		line = line.replace("{{dept_name}}"   , ls_dept_name);
		line = line.replace("{{service_code}}", ls_service_code);
		line = line.replace("{{addr}}"        , ls_addr);
		line = line.replace("{{biz_no}}"      , ls_biz_no);
		line = line.replace("{{owner_name}}"  , ls_owner_name);
		line = line.replace("{{cust_id}}"     , ls_cust_id);
		line = line.replace("{{cust_name}}"   , ls_cust_name);
		line = line.replace("{{manageno}}"    , ls_manageno);
		line = line.replace("{{fromdate}}"    , ls_fromdate);
		line = line.replace("{{todate}}"      , ls_todate);
		line = line.replace("{{service_amt}}" , ls_service_amt);
		line = line.replace("{{req_amt}}"     , ls_req_amt);
		line = line.replace("{{pay_amt}}"     , ls_pay_amt);
		line = line.replace("{{b_amt}}"       , ls_b_amt);
		line = line.replace("{{total_amt}}"   , ls_total_amt);
		line = line.replace("{{pay_b_amt}}"   , ls_pay_b_amt);
		fw.write(line);
	}

	reader.close();
	fw.close(); //파일핸들 닫기

  java.io.File file = new java.io.File(filePath); // 파일객체생성
  byte bytestream[] = new byte[(int)file.length()];

		response.reset();
		String userAgent      = request.getHeader("User-Agent");
		String fileNameToSave = "";

if ( userAgent.indexOf("MSIE") != -1 || userAgent.indexOf("Windows") != -1 ) {
	    // 공백이 '+'로 인코딩된것을 다시 공백으로 바꿔준다.
	    fileNameToSave = java.net.URLEncoder.encode(fileName, "UTF8").replaceAll("\\+", " ");
	    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNameToSave + "\";");
		} else {
	    fileNameToSave = new String(fileName.getBytes("UTF-8"), "8859_1");
	    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNameToSave + "\";");
		}

		response.setHeader("Content-Type","application/octet-stream; charset=UTF-8");
		response.setHeader("Content-Length", ""+ file.length());
		response.setHeader("Content-Transfer-Encoding", "binary;");
		response.setHeader("Pragma", "no-cache;");
		response.setHeader("Expires", "-1;");

	  //java.io.BufferedOutputStream fw = new java.io.BufferedOutputStream(response.getOutputStream());

		if ( file.isFile() && file.length() > 0 ) {
	    java.io.FileInputStream fis      = new java.io.FileInputStream(file);
	    java.io.BufferedInputStream bis  = new java.io.BufferedInputStream(fis);
	    java.io.BufferedOutputStream bos = new java.io.BufferedOutputStream(response.getOutputStream());

	    int read = 0;
	    while ( (read = bis.read(bytestream)) != -1) {
	      bos.write(bytestream , 0, read);
	    }

	    bos.close();
	    bis.close();
		}
		else {

		}

%>

<%@ page language="java" contentType="text/html; charset=utf-8"	session="true" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>

<jsp:include page="/Mighty/include/COMM_JS.jsp"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%
	String obj = request.getParameter("obj");
	String id = request.getParameter("id");
	String subdir = request.getParameter("subdir");

%>
<head>
<title>Image Upload</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 

<style>
	.ui-widget {
		font-size: 12px;
		font-family: 맑은 고딕;
	}
	
	#dvPreview{
		/*filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod='scale');*/
		height: 300px;
		width: 400px;
		border:0;
		border-style:solid; 
		border-color:#ced9b4; 
		border-width:1px;
	}
	#buttons{
  	padding-top:15px;
 	}
</style>

<script type="text/javaScript" language="javascript">

var _IsModal = (window.dialogArguments?true:false);
var _IsPopup = (window.opener?true:false);
var _Caller  = (_IsModal?window.dialogArguments:(_IsPopup?window.opener:window.parent));
var subdir   = "<%=subdir%>";
var obj      = "<%=obj%>";

$(function() {
  $("#start").button({icons: {primary: 'ui-icon-seek-start'}, text: true});
  $("#cancel").button({icons: {primary: 'ui-icon-cancel'}, text: true});
  
  $('#cancel').click(function() {
			if(typeof(_Caller.x_ImageUploaded)!="undefined"){
				_Caller.x_ImageUploaded(null);
			}
			
			_Caller.$("#uploadmodal").dialog("close");
	});

  $('#start').click(function(){
      if ($.browser && $.browser.msie && parseFloat(jQuery.browser.version) <= 9.0) {
				$("#dvPreview").remove();
      };
      
		  $("#uploadForm").ajaxSubmit({
				  data    : $('#uploadForm').serialize(), //파라미터
				  type: 'post',
				  dataType: 'json', 
				  beforeSubmit: function (data,form,option) {
				  		//console.log(data,form,option);
            	return true;
          },
			    success : function(r){
			    		//console.log(r);
			    		
				  		if(typeof(_Caller.x_ImageUploaded)!="undefined"){
				  			alert(obj+":"+JSON.stringify(r));
				  			_Caller.x_ImageUploaded(obj, JSON.stringify(r));
							}
							
							_Caller.$("#uploadmodal").dialog("close");
					},
					error: function(jqXHR, textStatus, errorThrown) {
							//서버단 개발 덜된듯 함.
			        console.log(jqXHR, textStatus, errorThrown);
			    }
		  });
	});
	
  $('#close').click(function(){
		_Caller.$("#uploadmodal").dialog("close");
	});
	
  $("#file1").change(function () {
      $("#dvPreview").html("");
      
      var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
      
      if (regex.test($(this).val().toLowerCase())) {
        if ($.browser && $.browser.msie && parseFloat(jQuery.browser.version) <= 9.0) {
          $("#dvPreview").show();
          $("#dvPreview")[0].filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = $(this).val();
        }
        else {
          if (typeof (FileReader) != "undefined") {
            $("#dvPreview").show();
            $("#dvPreview").append("<img />");
            
            var reader = new FileReader();
            
            reader.onload = function (e) {
		            $("#dvPreview img").attr("src", e.target.result);
		            $("#dvPreview img").css("height", "300");
            }
            
            reader.readAsDataURL($(this)[0].files[0]);
          }
          else {
              alert("브라우저에서  로컬파일 처리를 지원하지 않습니다!");
          }
        }
      }
      else {
          alert("유효하지 않은 이미지 파일 입니다.");
      }
  });    
});

</script>
</head>
<body>
<!-- 상단타이틀 -->
<div id="content">
	<form id='uploadForm' method="post" action="/image/upload.do" enctype='multipart/form-data'>
		<div id="container">
			<input id="file1" name="file1" type='file' multiple />
			<input type="hidden" name="id" id="id" value="<%=id%>"/>
			<input type="hidden" name="subdir" id="subdir" value=""/>
			<br />
			<hr />
			<b>**이미지 미리보기(IE9+이상지원)</b>
			<br />
			<br />			
		</div>
		<div id="dvPreview">
		</div>
 		<div id="buttons" style="text-align:center;">
      <button type="button" id="start">업로드</button>
      <button type="button" id="cancel">취소</button>
 		</div>
	</form>  	
</div>
<script type="text/javaScript" language="javascript">
	$("#subdir").val(subdir);
</script>
</body>
</html>
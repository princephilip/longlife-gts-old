<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui"     uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html"   uri="/WEB-INF/tlds/mighty.tld" %>

<jsp:include page="/Mighty/include/COMM_JS.jsp"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<%
	String obj    = request.getParameter("obj");
	String subdir = request.getParameter("subdir");
	String fileid = request.getParameter("fileid");
	String upcnt  = request.getParameter("upcnt");
	if(upcnt==null || upcnt.equals("")) {
		upcnt = "0";
	}
%>

<head>
<title>Foundation Layer[Util Service]</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<base target="_self" />
<style type="text/css" media="screen"></style>
<link type="text/css" charset="utf-8" href="/Mighty/3rd/bootstrap/css/bootstrap.min.css"  rel="stylesheet" media="all" />

<!-- blueimp Gallery styles -->
<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<link rel="stylesheet" href="/Mighty/3rd/FileUpload/css/jquery.fileupload.css">
<link rel="stylesheet" href="/Mighty/3rd/FileUpload/css/jquery.fileupload-ui.css">

<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="/Mighty/3rd/FileUpload/css/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" href="/Mighty/3rd/FileUpload/css/jquery.fileupload-ui-noscript.css"></noscript>

<!--script type="text/javascript" language="javascript" src="/Mighty/js/multifile.js"></script-->

<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>

<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="/Mighty/3rd/FileUpload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/Mighty/3rd/FileUpload/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="/Mighty/3rd/FileUpload/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="/Mighty/3rd/FileUpload/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="/Mighty/3rd/FileUpload/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="/Mighty/3rd/FileUpload/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="/Mighty/3rd/FileUpload/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<!--script src="/Mighty/3rd/FileUpload/js/jquery.fileupload-ui.js"></script-->

<script>
	var _IsModal = (window.dialogArguments?true:false);
	var _IsPopup = (window.opener?true:false);
	var _Caller  = (_IsModal?window.dialogArguments:(_IsPopup?window.opener:window.parent));

	var obj     = "<%=obj%>";
	var subdir  = "<%=subdir%>";
	var fileid  = "<%=fileid%>";
	var upcnt   = <%=upcnt%>;
	var loadcnt = parseInt("${fn:length(fileList)}");
	//upcnt   = eval(upcnt - loadcnt);

	var isSubmit = false;
	/*jslint unparam: true, regexp: true */
	/*global window, $ */
	$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = '/file/upload.do',
        uploadButton = $('<button/>')
            .addClass('btn btn-warning btn-upload btn-xs')
            //.prop('disabled', true)
            //.text('업로드')
            //.text('업로드중...')
            .text("취소")
            .on('click', function () {
            		if (isSubmit) {
	                var $this = $(this),
	                    data  = $this.data();

	                $this
	                    .on('click', function () {
	                        $this.remove();
	                        data.abort();
	                    });

	                data.submit().always(function () {
	                    $this.remove();
	                });
	              }
	              else {
	              	$(this).closest("div").remove();
	              }
            });

    $('#fileupload').fileupload({
	    	formData: {subdir: subdir, fileid: fileid, obj:obj},

        url: url,
        //dataType: 'json',
        dataType: 'html',
        autoUpload: false,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png|zip|xls|xlsx|doc|hwp|pdf|txt)$/i,
        maxFileSize: 100000000,
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 200,
        previewMaxHeight: 100,
        previewCrop: true,
        limitMultiFileUploads: upcnt
    }).on('fileuploadadd', function (e, data) {
    	  if($(".btn-upload:enabled,.btn-danger").length + data.files.length > upcnt) {
    	  	alert("업로드 가능 갯수(" + upcnt + "개)를 초과하였습니다.");
    	  	return;
    	  }
        data.context = $('<div/>').appendTo('#files');
        $.each(data.files, function (index, file) {
            var node = $('<p/>')
                    .append($('<span/>').text(file.name));
            if (!index) {
                node
                    //.append('<br>')
                    .append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                //.text('업로드')
                .text('취소')
                //.prop('disabled', !!data.files.error)
								;
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
    		document.write(data.result);
        $.each(data.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
</script>


<script type="text/javaScript" language="javascript">

	function pf_cancel(){
		//if(typeof(_Caller.x_FileUploaded)!="undefined"){
		//	_Caller.x_FileUploaded(obj,null);
		//}
		//_Caller.$("#uploadmodal").dialog("close");
		_Caller.$("#uploadmodal").dialog("destroy").remove();
	}

	function pf_submit(){
		isSubmit = true;
    $(".btn-upload:enabled").each(function (index, btn) {
    	$(btn).click();
		});
	}

	function pf_file_delete(obj, attch_id, file_sn){
		_X.FileDelete(attch_id, file_sn);
		$(obj).parent('div').remove();
		$("#my_file_element").attr("disabled",false);
	}

	function  x_FileDeleted(attch_id, file_sn) {
		loadcnt--;
		if(typeof(_Caller.x_FileDeleted)!="undefined"){
			if(loadcnt==0) {
				_Caller.x_FileDeleted(obj,null);
			} else {
				_Caller.x_FileDeleted(obj,attch_id);
			}
		}
	}

	//fileReadOnly 추가 (2015.06.22 이상규)
	function pf_setReadOnly() {
		if (_Caller.fileReadOnly) {
			$("#my_file_element,.deleteImg,.btn.btn-danger,.btn.btn-success.fileinput-button").hide();
		}
	}
</script>

</head>
<body onload="pf_setReadOnly();">
	<div id="content" style="width:100%;height:100%;">
		<!-- form name="Form" id="Form" method="post" action="/file/upload.do" enctype="multipart/form-data" -->
  			<div id="container" style="width:100%;">
			    <!-- The fileinput-button span is used to style the file input field as button -->
			    <span class="btn btn-success fileinput-button">
			        <i class="glyphicon glyphicon-plus"></i>
			        <span>파일 추가...</span>
			        <!-- The file input field used as target for the file upload widget -->
			        <input id="fileupload" type="file" name="files[]" multiple>
			    </span>

			    <br>
			    <!-- The global progress bar -->
			    <div id="progress" class="progress progress-striped active" style="margin-top:5px;margin-bottom:10px;">
			        <div class="progress-bar progress-bar-success"></div>
			    </div>

			    <!-- The container for the uploaded files -->
			    <div id="files" class="files">
					</div>

  				<div id="loaded_list" class="files">
  					<c:forEach var="fileVO" items="${fileList}" varStatus="status">
      				<div>
      					<c:if test="${fileVO.fileExtsn == 'jpg' || fileVO.fileExtsn == 'jpeg' || fileVO.fileExtsn == 'png' || fileVO.fileExtsn == 'gif'}">
								   <img src="d:/FileUpLoad/${fn:replace(fileVO.fileStreCours, '\\', '/')}/${fileVO.streFileNm}" style="max-width: 200px; max-height: 100px" />
								</c:if>

             		<a href="#LINK" onclick="javascript:_X.FileDownload('<c:out value="${fileVO.atchFileId}"/>','<c:out value="${fileVO.fileSn}"/>')">
              		<c:out value="${fileVO.fileSn}"/>.&nbsp;<c:out value="${fileVO.orignlFileNm}"/>&nbsp;[<c:out value="${fileVO.fileMg}"/>&nbsp;byte]
             		</a>

	    			    <span class="btn btn-danger btn-xs" aria-label="삭제" onClick="pf_file_delete(this,'<c:out value="${fileVO.atchFileId}"/>','<c:out value="${fileVO.fileSn}"/>');"><i class="glyphicon glyphicon-remove"></i></span>
			        	</br>
      				</div>
		        </c:forEach>
					</div>

		    </div>
		<!--/form-->
	</div>
</body>

<script type="text/javaScript" language="javascript">
	$("#obj").val(obj);
	$("#subdir").val(subdir);
	$("#fileid").val(fileid);
</script>

</html>
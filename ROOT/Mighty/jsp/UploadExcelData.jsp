<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.oreilly.servlet.MultipartRequest,
	com.oreilly.servlet.multipart.DefaultFileRenamePolicy,
	java.util.*,
	java.io.*,
	jxl.*,
	xgov.core.dao.*,
	xgov.core.env.XLog,
	org.apache.poi.xssf.usermodel.*,
	org.apache.poi.hssf.usermodel.*,java.text.SimpleDateFormat,
	java.text.DecimalFormat"
%>

<%
	String realFolder = application.getRealPath("/");
	String errMsg  = "OK";
	int  	 maxSize 	 = 100*1024*1024;
	int	 	 fileCnt	 = 0;
	int	 	 fileSize	 = 0;
	String uploadNum = "";
	String uploadPgmId = "";
	String upUserId = "";
	int 	 iHeaderRow			=0;
	String[][] args	= new String[207][2];
	String s_dmt = "¸";

	try
	{
		MultipartRequest multi = new MultipartRequest(request, realFolder, maxSize, "utf-8", null);
		Enumeration params = multi.getParameterNames();
		iHeaderRow = Integer.parseInt(multi.getParameter("headerRow"));

		while(params.hasMoreElements()){
			String fname = (String)params.nextElement();
			String value = multi.getParameter(fname);

			uploadNum  = multi.getParameter("uploadId");
			uploadPgmId  = multi.getParameter("upPgmId");
			upUserId  = multi.getParameter("upUserId");
			String[][] d_args	= new String[2][2];
			d_args[0][0] = "S";
			d_args[0][1] = uploadPgmId;
			d_args[1][0] = "S";
			d_args[1][1] = upUserId;
			XDAO.ExecProc(request, "sm", "PROC_XM_EXCEL_DATA_DEL", d_args);
		}


		Enumeration files = multi.getFileNames();

		while(files.hasMoreElements()){
			String fname = (String)files.nextElement();
			//String type = multi.getContentType(fname);

			File f = multi.getFile(fname);

			System.out.println("fname : " + fname);
			System.out.println("getName : " + f.getName().toUpperCase());

			ArrayList list   = new ArrayList(); //debug
			int targetcntall = 0;               //debug

			if(f!=null){
				fileCnt++;
				fileSize+=f.length();

				if(f.getName().toUpperCase().indexOf("XLSX")>0) {
				 	FileInputStream inputStream = new FileInputStream(f);
				 	XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
					XSSFRow row;
			 		XSSFCell cell;

					//첫번째 Sheet만 업로드
					//전체 sheet 업로드(2017.02.15 KYY)
					//첫번째 sheet 업로드(2017.06.16 KYY)
					int sheetCnt = 1;
					//int sheetCnt = workbook.getNumberOfSheets();
					XSSFFormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
					sheetCnt = 1;
					for(int s=0; s<sheetCnt; s++) {
						XSSFSheet sheet = workbook.getSheetAt(s);
	          			int firstRow = sheet.getFirstRowNum();
						firstRow = (iHeaderRow>0 ? iHeaderRow-1 : 0);
	          			int lastRow = sheet.getLastRowNum();
						int rowidx = 0;

						//for(int r=0; r<=lastRow; r++) {
						for(int j=0; j<args.length; j++) {
							args[j][0] = "S";
							args[j][1] = "";
						}
						args[0][1] = uploadNum;
						args[1][1] = Integer.toString(s);
						args[2][1] = workbook.getSheetName(s);
						args[3][1] = uploadPgmId;
						args[4][1] = upUserId;
						args[5][1] = s_dmt;

						//file_num number, sheet_num number, sheet_name varchar, pgm_id varchar, user_id varchar, data_dmt varchar, row_num CLOB,
						int targetcnt = 0, targetcol = 0, maxcol = 0;
						int colCnt 	  = 200;
						String arg_row = "";
						String[] arg_val = new String[colCnt];
						for(int j=0; j<colCnt; j++) {
							arg_val[j] = "";
						}

						for(int r = firstRow; r<=lastRow; r++) {
							row = sheet.getRow(r);
							rowidx = (iHeaderRow>0 ? r-firstRow  : r+1);

							if(row!=null) {
		            			int firstCell = row.getFirstCellNum();
		            			int lastCell  = row.getLastCellNum();
								targetcnt++;
								arg_row = arg_row + s_dmt + Integer.toString(rowidx);
								targetcol=0;

								for(int c=0; c<=lastCell; c++) {
									cell = row.getCell(c);
									targetcol++;

									if (cell != null) {
										String value = "";

										switch (cell.getCellType()) {
		                					case XSSFCell.CELL_TYPE_BOOLEAN:
												 boolean bdata = cell.getBooleanCellValue();
												 value = String.valueOf(bdata);

												 //System.out.println("CELL_TYPE_BOOLEAN: " + value);
												 list.add(value);
												 break;
											case XSSFCell.CELL_TYPE_NUMERIC:
													if (HSSFDateUtil.isCellDateFormatted(cell)){

														SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
														value = formatter.format(cell.getDateCellValue());
													} else{
														double ddata = cell.getNumericCellValue();
														//value = String.valueOf(ddata); //원본

														//정수 지수변환 제거 2016.08.02 이상규
														DecimalFormat df = new DecimalFormat("#.#############");
														value = df.format(ddata);
													}
													//System.out.println("CELL_TYPE_NUMERIC: " + value);
													list.add(value);
												  break;
											case XSSFCell.CELL_TYPE_STRING:
												 value = cell.toString();
												 //System.out.println("CELL_TYPE_STRING: " + value);
												 list.add(value);
												 break;
											case XSSFCell.CELL_TYPE_BLANK:
											case XSSFCell.CELL_TYPE_ERROR:
											case XSSFCell.CELL_TYPE_FORMULA:
													if(!(cell.toString()=="") ){
														if(evaluator.evaluateFormulaCell(cell)==0){
															double fddata = cell.getNumericCellValue();
															value = String.valueOf(fddata);
														}else if(evaluator.evaluateFormulaCell(cell)==1){
															value = cell.getStringCellValue();
														}else if(evaluator.evaluateFormulaCell(cell)==4){
															boolean fbdata = cell.getBooleanCellValue();
															value = String.valueOf(fbdata);
														}
													}
													//System.out.println("etc: " + value);
													list.add(value);
												  break;
										 }
										 arg_val[c] = arg_val[c] + s_dmt + value;
									 } else {
										 arg_val[c] = arg_val[c] + s_dmt + "";
									 }
								}

								//Why ? targetcnt를 체크(2017.02.15 KYY)
								//if(targetcnt>=10){
									//int targetcolcnt = 0; //debug

									if(targetcol > maxcol) { maxcol = targetcol;}

									args[6][1] = arg_row;
									for(int k=0; k<maxcol; k++) {
										args[k+7][1] = arg_val[k];
										//targetcolcnt++; //debug
									}

									XDAO.ExecProc(request, "sm", "PROC_XM_EXCEL_DATA", args);
									targetcnt = 0;
									arg_row = "";
									for(int k=0; k<colCnt; k++) {
										arg_val[k]="";
									}

								//}
							}
						}
/*						Why ? targetcnt를 체크(2017.02.15 KYY)
						if(targetcnt>0){
							args[6][1] = arg_row;
							for(int k=0; k<targetcol; k++) {
								args[k+7][1] = arg_val[k];
								//XLog.Write("INFO", "sm", args[k+7][1]);
							}

							XDAO.ExecProc(request, "sm", "PROC_XM_EXCEL_DATA", args);
						}
*/
					}
					inputStream.close();
					workbook.close();

				}
				else {

					Workbook workbook = Workbook.getWorkbook(f);
					//전체 sheet 업로드(2017.02.15 KYY)
					//첫번째 sheet 업로드(2017.06.16 KYY)
					int sheetCnt = 1;
					//int sheetCnt = workbook.getNumberOfSheets();
					for(int j=0; j<args.length; j++) {
						args[j][0] = "S";
						args[j][1] = "";
					}

					for(int s=0; s<sheetCnt; s++) {
						Sheet sheet = workbook.getSheet(s);
						int rows = sheet.getRows();   	// 시트의 행의 수를 반환한다.
						int cols = sheet.getColumns();  // 시트의 컬럼의 수를 반환한다.

						int firstRow = (iHeaderRow>0 ? iHeaderRow-1 : 0);
						int rowidx = 0;

						args[0][1] = uploadNum;
						args[1][1] = Integer.toString(s);
						args[2][1] = sheet.getName();
						args[3][1] = uploadPgmId;
						args[4][1] = upUserId;
						args[5][1] = s_dmt;
						//file_num number, sheet_num number, sheet_name varchar, pgm_id varchar, user_id varchar, data_dmt varchar, row_num CLOB,

						int targetcnt = 0;
						int colCnt 	  = 200;
						String arg_row = "";

						String[] arg_val = new String[colCnt];
						for(int j=0; j<colCnt; j++) {
							arg_val[j] = "";
						}

						for(int i=firstRow; i<rows; i++) {

							rowidx = (iHeaderRow>0 ? i-firstRow : i+1);
							targetcnt++;
							arg_row = arg_row + s_dmt + Integer.toString(rowidx);

							for(int j=0; j<cols; j++) {
								arg_val[j] = arg_val[j] + s_dmt + sheet.getCell(j, i).getContents();
							}

							//Why ? targetcnt를 체크(2017.02.15 KYY)
							//if(targetcnt>=10){
								args[6][1] = arg_row;
								for(int k=0; k<colCnt; k++) {
									args[k+7][1] = arg_val[k];
								}
								//System.out.println("args1 : " + args);
								XDAO.ExecProc(request, "sm", "PROC_XM_EXCEL_DATA", args);
								targetcnt = 0;
								arg_row = "";
								for(int k=0; k<colCnt; k++) {
									arg_val[k]="";
								}
							//}
						}
/*						Why ? targetcnt를 체크(2017.02.15 KYY)
						if(targetcnt > 0){
							args[6][1] = arg_row;
							for(int k=7; k<args.length; k++) {
								args[k][1] = arg_val[k-7];
							}
							//System.out.println("args2 : " + args);
							XDAO.ExecProc(request, "sm", "PROC_XM_EXCEL_DATA", args);
						}
*/
					}
					workbook.close();
				}
			}
	   }

	}catch(IOException ioe){
		errMsg = "UploadExcelData.jsp 오류1 : " + ioe.getMessage();
		XLog.Write("ERROR", "sm", errMsg);
	}catch(Exception ex){
		errMsg = "UploadExcelData.jsp 오류99 : " + ex.getStackTrace();
		ex.printStackTrace();
		XLog.Write("ERROR", "sm", errMsg);
	}
	if (!errMsg.equals("") && !errMsg.equals("OK"))
		System.out.println(errMsg);

%>

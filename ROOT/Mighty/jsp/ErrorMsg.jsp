<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<%
	String errorCode = request.getParameter("error");
%>
<head>
	<title>ePMS :: ERROR</title>
    <style type="text/css">
    <!--
		body {background-color:#fff}
		#daumHead {position:relative;width:490px;height:43px;margin:0 auto;padding:75px 0 0;border-bottom:3px solid #666}
		#daumHead h1 {padding:0}
		#daumServiceLogo {width:125px;height:25px;margin-left:1px;background-position:0 0}
		#daumContent {margin:0;padding:0}
		#mArticle {width:490px;margin:0 auto;padding:0}
		#daumFoot {position:static;width:490px;margin:0 auto}
		.inner_footer {margin:0;padding:0;border-top-color:#efefef;color:#919191}
		.inner_footer .txt_copyright {width:auto;padding:20px 0 20px}
		.footer_tistory .link_daum {color:#919191}

           
		/* 에러페이지 */
		.wrap_error #daumHead {width:590px;height:32px;padding:88px 0 0;border-bottom:1px solid #585a5e}
		.wrap_error #daumServiceLogo {float:left;width:93px;height:19px;background-position:-275px 0}
		.wrap_error .link_customer {float:right;padding-right:6px;margin:14px 2px 0 0;font-size:11px;line-height:13px;font-family:dotum,sans-serif;color:#444;background-position:100% -848px;letter-spacing:-1px}
		.wrap_error #mArticle {width:590px}
		.wrap_error #daumFoot {width:590px}

		.wrap_error .tit_error {font-size:18px;line-height:32px;color:#333;letter-spacing:-1px}
		.wrap_error .tit_error .emph_error {font-weight:bold;color:#f1631b}
		.wrap_error .desc_error {padding-top:15px;font-size:13px;line-height:24px;color:#919191;letter-spacing:-1px}
		.cont_error {padding:140px 0 50px;background:url(/images/error/bg_error.gif) no-repeat 50% 50px;text-align:center}
		.cont_error .link_home {display:inline-block;width:164px;height:30px;margin:45px auto 0;padding-top:14px;border:1px solid #dcdcdc;font-size:14px;line-height:17px;color:#737a84;background-color:#fff;letter-spacing:-1px}
		.cont_error .link_home:hover {background-color:#fafafa;text-decoration:none}
		.cont_error .link_prev {display:inline-block;width:81px;height:30px;margin:0 3px;padding-top:14px;border:1px solid #dcdcdc;font-size:14px;line-height:17px;color:#737a84;background-color:#fff;letter-spacing:-1px}
		.cont_error .link_prev:hover {background-color:#fafafa;text-decoration:none}
		.cont_error .link_prev .txt_prev {padding-left:15px;background-position:0 -921px;cursor:pointer}
		.cont_error .wrap_btn {position:relative;padding-top:30px}
		.cont_error .wrap_btn .link_home {margin:0 3px}
		.cont_error .wrap_btn .link_more {position:absolute;top:44px;right:43px;height:14px;padding:0 10px 1px 0;border-bottom:1px solid #f8b18d;font-size:12px;line-height:15px;color:#f1631b;background-position:100% -420px;letter-spacing:-1px}
		.cont_error .wrap_btn .link_more:hover {text-decoration:none}


		.box_error {width:512px;padding:27px 29px 30px;margin:32px auto 0;font-size:13px;color:#919191;background-color:#fafafa;text-align:left;letter-spacing:-1px}
		.box_error .info_error {line-height:24px}
		.box_error .list_error {overflow:hidden;padding-top:14px;margin-top:15px;border-top:1px solid #e9e9e9;line-height:24px}
		.box_error .list_error li {padding-left:10px;color:#333;background:url(/images/error/dot_error.gif) no-repeat 0 10px}
		.box_error .list_error .tit_subject {float:left;width:72px;padding-left:10px;margin-top:4px;background-position:0 -889px}
		.box_error .list_error .txt_subject {float:left}
		.box_error .list_error .txt_bar {float:right;padding-right:7px;color:#333}
		.box_error .list_error .desc_info {float:left;width:425px;margin-top:4px;color:#333}
		.box_error .list_error .link_error {text-decoration:underline}
		.box_error .list_bloginfo .tit_subject {width:85px}
		.box_error .list_bloginfo .desc_info {width:410px}
		.box_error .tit_subtitle {font-weight:normal;line-height:24px;color:#333}
		.box_error .link_more {display:inline-block;height:14px;padding:0 10px 1px 0;margin-top:17px;border-bottom:1px solid #c0c0c0;font-size:12px;line-height:15px;color:#919191;background-position:100% -945px;letter-spacing:-1px}
		.box_error .link_more:hover {text-decoration:none}
		.box_error .link_find {color:#f1631b;text-decoration:underline}

		.box_error2 {width:550px;padding:15px 10px;margin:30px auto 0;font-size:13px;line-height:24px;color:#919191;background-color:#fafafa;letter-spacing:-1px}
		.box_error2 .link_more {text-decoration:underline}
            
    -->
    </style>
</head>

<body>

<div id="daumWrap" class="wrap_error" style="width:100%; align:middle;">
	<div id="daumHead" role="banner">
		<h1>
			<span class="ir_wa">ePMS</span></a>
		</h1>
	</div>

	<div id="daumContent" role="main">
		<div id="mArticle">
			<div class="cont_error">

  <% if(errorCode.equals("403")) { %>
      <h3 class="tit_error">403 ERROR</h3>
			<div class="box_error2">
      무단링크가 의심되어 접근을 차단합니다.<br />
			</div>
  <% } else if(errorCode.equals("404")) { %>
      <h3 class="tit_error">404 ERROR</h3>
			<div class="box_error2">
      찾으시는 문서의 URL이 잘못되었거나 없습니다.<br />
      확인하신 후에 다시 시도하시기 바랍니다.<br />
			</div>
  <% } else if(errorCode.equals("503")) { %>
      <h3 class="tit_error">503 ERROR</h3>
			<div class="box_error2">
      트래픽 제한량을 초과하였습니다.<br />
			</div>
  <% } else { %>
      <h3 class="tit_error">ERROR</h3>
			<div class="box_error2">
      찾으시는 문서의 URL이 잘못되었거나 없습니다.<br />
      확인하신 후에 다시 시도하시기 바랍니다.<br />
			</div>
  <% } %>

			</div>
		</div>
	</div>
</div>
	
</body>
</html>

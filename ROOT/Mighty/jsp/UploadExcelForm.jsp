<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!--script type="text/javaScript" src="${pageContext.request.contextPath}/Mighty/jQuery/js/jquery.form.min.js"></script-->

<!-- ui-dialog -->
<div id="dialog" title="엑셀 업로드">
	<form id="x_SaveExcelFile" target="_self" action="/Mighty/jsp/UploadExcelData.jsp" method="post" enctype="multipart/form-data" style="display:inline">
		<div style="width:100%;height:100%;padding:25px 30px 30px 85px;">
			<div style="overflow:hidden; width:100px; height:52px; background-image:url('/Theme/images/excel/btn_openexcel.png');text-align:center;" >
				<input type=file id="fileName" name="fileName"
							 style="width:90px;height:52px;margin:0px;filter:alpha(opacity=0);opacity:0;cursor:pointer;" tabindex="0"
							 onchange="pt_FileSelected()">
			</div>
			<input type=hidden id="uploadId" name="uploadId" value="-1">
			<input type=hidden id="rowCnt" name="rowCnt">
			<input type=hidden id="headerRow" name="headerRow" value="0">
			<input type=hidden id="upPgmId" name="upPgmId" value="">
			<input type=hidden id="upUserId" name="upUserId" value="">
			<div id="upload_msg" class="process_msg mt5"></div>
		</div>
	</form>
</div>

<script type="text/javascript">
	// Dialog
	$('#dialog').dialog({
		autoOpen: false,
		width: 300,
		height: 200,
		buttons: {
			"닫기": function() {
				$(this).dialog("close");
			}
		},
		modal: true
	});

	//엑셀업로드
	function pf_ExcelUpload(as_callback) {
		uf_ExcelImported = as_callback || uf_ExcelImported;

		//x_SaveExcelFile.fileName.value = "";
		$("#fileName").val("");
		$('#dialog').dialog('open');
		x_SaveExcelFile.focus();
	}

	function pt_FileSelected(e) {
		try {
			if(!/.*\.(xls)|(xlsx)$/.test($("#fileName").val().toLowerCase())){
				_X.MsgBox("엑셀파일 형식을 선택해 주시기 바랍니다.");
				return;
			}

			if(x_SaveExcelFile.uploadId.value == "-1") {
				x_SaveExcelFile.uploadId.value = _X.XmlSelect("com", "Comm", "FileNum", null, 'array')[0][0];
			}

			upload_msg.innerHTML = "업로드 중입니다...";
			_X.Block();
			$("#x_SaveExcelFile").submit();
			//x_SaveExcelFile.submit();
		} catch (e) {
			setTimeout(function () {
				$("#x_SaveExcelFile").submit();
				//Submit();
			}, 50);
		}
 	}

	$('#x_SaveExcelFile').ajaxForm({
		beforeSubmit: function (data,form,option) {
			//validation체크
			//막기위해서는 return false를 잡아주면됨
			return true;
		},
		success: function(response,status){
			upload_msg.innerHTML = "업로드 완료!!!";
			_X.UnBlock();

			//성공후 서버에서 받은 데이터 처리
			var uploadCnt = _X.XmlSelect("com", "Comm", "EXCEL_UPLOAD_CNT", new Array(x_SaveExcelFile.uploadId.value), 'array')[0][0];

			if(typeof(uf_ExcelImported)!="undefined") {
				setTimeout("uf_ExcelImported(" + x_SaveExcelFile.uploadId.value + "," + uploadCnt + ")", 0);
			} else {
				_X.MsgBox(uploadCnt + "건의 엑셀데이타 업로드 완료.");
			}
			upload_msg.innerHTML = "";
			$('#dialog').dialog("close");
		},
		error: function(){
			_X.UnBlock();
			//에러발생을 위한 code페이지
			_X.MsgBox("엑셀파일 업로드에 실패하였습니다.");
		}
	});

</script>


<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.oreilly.servlet.MultipartRequest,
	com.oreilly.servlet.multipart.DefaultFileRenamePolicy,
	java.util.*,
	java.io.*,
	jxl.*,
	xgov.core.dao.*,
	xgov.core.env.XLog,
	org.apache.poi.xssf.usermodel.*,
	org.apache.poi.hssf.usermodel.*,java.text.SimpleDateFormat,
	java.text.DecimalFormat"
%>

<%
	String ls_rep 	= request.getParameter("rep");
	String ls_fname = request.getParameter("fname");
	String ls_cust 	= request.getParameter("cust");
	String ls_dept 	= request.getParameter("dept");
	String ls_ym 		= request.getParameter("ym");

	String ls_cust_code = "";
	String ls_cust_name = "";
	String ls_birth 		= "";
	String ls_manage_no = "";
	String ls_level 		= "";


	String[] typeData = xgov.core.dao.XDAO.XmlSelect(request, "array", "gs", "GS01020", "CODE_CUST_R02", "" + ls_cust +"", "all", "˛", "¸").split("˛");
	//StringBuffer sbType = new StringBuffer(10240);

	if(typeData[0]==null || typeData[0].equals("")){
		out.println("<script type=\'text/javascript'>");
		out.println("alert('데이터가 존재하지 않습니다');");
		out.println("</script>");
	}else{
		for(int i=0; i<typeData.length; i++) {
			String[] tyData = typeData[i].split("¸");
			ls_cust_code	= tyData[2];
			ls_cust_name 	= tyData[3];
			ls_birth 			= tyData[1];
			ls_manage_no 	= tyData[8];
			ls_level 			= tyData[9];
			//System.out.println("ls_cust_name: [" + ls_cust_name + "]");
			}
		}

	String realFolder    = xgov.core.env.XConfiguration.getString("Globals.docPath");
	System.out.println("realFolder: [" + realFolder + "]");
	String fileName  = ls_fname +'_'+ ls_cust_name + ".hml";
	String filePath  = realFolder+"tmp"+ls_rep;
	//String filePath  = "D:/Projects/GTS_X5/ROOT/APPS/gs/doc/"+"tmp"+ls_rep;
	String ls_filePath  = realFolder+ls_rep;
	//String ls_filePath  = "D:/Projects/GTS_X5/ROOT/APPS/gs/doc/"+ls_rep;

	//System.out.println("ls_rep: [" + ls_rep + "]");
	//String filePath1 = "D:/Projects/GTS_X5/ROOT/APPS/gs/doc/rpt_00010.hml";
  //String UTF8FilePath1 = new String(filePath1.getBytes("8859_1"), "UTF-8");
  String UTF8FilePath = new String(filePath.getBytes("8859_1"), "UTF-8");

  // 파일읽기
  java.io.BufferedReader reader = new java.io.BufferedReader(new java.io.InputStreamReader(new java.io.FileInputStream(ls_filePath),"UTF-8"));

  java.io.BufferedWriter fw = new java.io.BufferedWriter(new OutputStreamWriter(new FileOutputStream(UTF8FilePath), "UTF-8"));

	String line = null;

	while( (line = reader.readLine() )!=null){ //라인단위 읽기
		line = line.replace("{{cust_code}}"	, ls_cust_code);
		line = line.replace("{{name}}"			, ls_cust_name);
		line = line.replace("{{birth}}"			, ls_birth);
		line = line.replace("{{level}}"			, ls_level);
		line = line.replace("{{manage_no}}"	, ls_manage_no);

		fw.write(line);
	}

	reader.close();
	fw.close(); //파일핸들 닫기

  java.io.File file = new java.io.File(filePath); // 파일객체생성
  byte bytestream[] = new byte[(int)file.length()];

		response.reset();
		String userAgent      = request.getHeader("User-Agent");
		String fileNameToSave = "";

if ( userAgent.indexOf("MSIE") != -1 || userAgent.indexOf("Windows") != -1 ) {
	    // 공백이 '+'로 인코딩된것을 다시 공백으로 바꿔준다.
	    fileNameToSave = java.net.URLEncoder.encode(fileName, "UTF8").replaceAll("\\+", " ");
	    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNameToSave + "\";");
		} else {
	    fileNameToSave = new String(fileName.getBytes("UTF-8"), "8859_1");
	    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNameToSave + "\";");
		}

		response.setHeader("Content-Type","application/octet-stream; charset=UTF-8");
		response.setHeader("Content-Length", ""+ file.length());
		response.setHeader("Content-Transfer-Encoding", "binary;");
		response.setHeader("Pragma", "no-cache;");
		response.setHeader("Expires", "-1;");

	  //java.io.BufferedOutputStream fw = new java.io.BufferedOutputStream(response.getOutputStream());

		if ( file.isFile() && file.length() > 0 ) {
	    java.io.FileInputStream fis      = new java.io.FileInputStream(file);
	    java.io.BufferedInputStream bis  = new java.io.BufferedInputStream(fis);
	    java.io.BufferedOutputStream bos = new java.io.BufferedOutputStream(response.getOutputStream());

	    int read = 0;
	    while ( (read = bis.read(bytestream)) != -1) {
	      bos.write(bytestream , 0, read);
	    }

	    bos.close();
	    bis.close();
		}
		else {

		}

%>

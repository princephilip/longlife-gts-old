<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ page import="java.io.*" %>  
<%@ page import="java.util.*" %>   
<%@ page import="java.sql.Connection" %> 
<%@ page import="java.sql.DriverManager" %>  
<%@ page import="net.sf.jasperreports.engine.JasperExportManager" %> 
<%@ page import="net.sf.jasperreports.engine.JasperFillManager" %> 
<%@ page import="net.sf.jasperreports.engine.JasperPrint" %> 
<%@ page import="net.sf.jasperreports.engine.JasperReport" %> 
<%@ page import="net.sf.jasperreports.engine.JasperReportsContext" %> 
<%@ page import="net.sf.jasperreports.engine.SimpleJasperReportsContext" %> 
<%@ page import="net.sf.jasperreports.engine.util.JRLoader" %>
<%@ page import="java.net.URLDecoder" %> 

<%

String system = "sm";
String rptURL = URLDecoder.decode(request.getParameter("rptURL"),"UTF-8")==null ? "X.jasper" : URLDecoder.decode(request.getParameter("rptURL"),"UTF-8")+".jasper";  
String requrl = new String(request.getRequestURL());
String vMsg = "-";

String strPdfURL = "/filePDF/";
String strPdfDir = null;
String strPdfFileName = null;
String strPdfFile = null;

File rptFile = new File(application.getRealPath(rptURL));
Map<String, Object> rptParams = new HashMap<String, Object>();

Enumeration enums = request.getParameterNames(); 

while(enums.hasMoreElements()){ 
	String skey = (String)enums.nextElement();  
	String sval = URLDecoder.decode(request.getParameter(skey),"UTF-8");  
	if(!skey.equals("rptURL")){
		rptParams.put(skey, sval); 
		vMsg = vMsg + "@" + skey + ":" + sval; 
	}

	if(skey.equals("as_pdfname")){
	  strPdfFileName = URLDecoder.decode(request.getParameter(skey),"UTF-8");  
	}
	
	if(skey.equals("as_pdfdir")){
	  strPdfDir = URLDecoder.decode(request.getParameter(skey),"UTF-8");  
	}
} 


if(rptFile.exists() == false){ 
	rptParams.put("pMsg", "["+rptURL+" : arguments"+vMsg+"]Report File Not Found"); 
	rptURL = "/APPS/sm/report/MsgReport.jasper";
	rptFile = new File(application.getRealPath(rptURL)); 
}

Connection conn = null; 

try { 
	conn = xgov.core.jndi.XConnJndi.getConnection(system);
	JasperReportsContext jasperReportsContext = new SimpleJasperReportsContext();
	JasperReport jasperReport = (JasperReport)JRLoader.loadObject(jasperReportsContext,rptFile); 
	JasperFillManager fillManager = JasperFillManager.getInstance(jasperReportsContext);
	JasperPrint jasperPrint = fillManager.fill(jasperReport, rptParams, conn);
	JasperExportManager exportManager = JasperExportManager.getInstance(jasperReportsContext);
	

  String dir = application.getRealPath(strPdfURL + strPdfDir + "/");
  File fileDir = new File(dir);
  if(!fileDir.exists()){
    fileDir.mkdirs();
  }

  strPdfFile = application.getRealPath(strPdfURL + strPdfDir + '/' + strPdfFileName + ".pdf");
	exportManager.exportReportToPdfFile(jasperPrint, strPdfFile);

	System.out.println("OK");

}finally{

	if( conn != null) try{ conn.close(); }catch(Exception x){}

}

%>

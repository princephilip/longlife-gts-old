<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@page import="java.io.File"%>
<%@page import="java.util.Enumeration"%>
 
<%
	String jsonFileKey = request.getParameter("fkey");
	response.reset();
	response.setContentType("application/json");
	response.setHeader("Content-Disposition", "attachment; filename = " + jsonFileKey + ".json");
	
	try
	{
		out.clear(); 
		out=pageContext.pushBody();

		ServletOutputStream out2 = response.getOutputStream();
		out2.write(((String)xgov.core.dao.XDAO._ResultData.get(jsonFileKey)).getBytes("UTF-8"));
		out2.flush();
		out2.close();	
		if(xgov.core.dao.XDAO._ResultData.containsKey(jsonFileKey)) {
			xgov.core.dao.XDAO._ResultData.remove(jsonFileKey);
		}
	}
	catch (Exception ex)
	{
		xgov.core.env.XLog.Write("system", "err", "XFileUtil : " + ex.getMessage() + ex.getStackTrace());
	}
%>

﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.oreilly.servlet.MultipartRequest,
                com.oreilly.servlet.multipart.DefaultFileRenamePolicy,
                java.util.*,
                java.io.*,
                jxl.*,
                xgov.core.dao.*"
%>

<%
	String realFolder = application.getRealPath("/");
	String errMsg  = "OK";
	int  	 maxSize 	 = 100*1024*1024;
	int	 	 fileCnt	 = 0;
	int	 	 fileSize	 = 0;
	String uploadNum = "";
	
	try
	{
	   MultipartRequest multi = new MultipartRequest(request, realFolder, maxSize, "utf-8", null);
	   Enumeration params = multi.getParameterNames();

	   while(params.hasMoreElements()){
	      String fname = (String)params.nextElement();
	      String value = multi.getParameter(fname);
	      uploadNum  = multi.getParameter("uploadId");
	      String[][] d_args	= new String[1][2];
	      d_args[0][0] = "S";
	      d_args[0][1] = uploadNum;
	      XDAO.ExecProc(request, "sm", "SP_XM_EXCEL_DATA_DEL", d_args);
	   }

	   Enumeration files = multi.getFileNames();
	   while(files.hasMoreElements()){
			String fname = (String)files.nextElement();
			//String type = multi.getContentType(fname);
			File f = multi.getFile(fname);
			if(f!=null){
				fileCnt++;
				fileSize+=f.length();
				//errMsg="OK";				
				
				//Workbook workbook = Workbook.getWorkbook(new File(savePath + "/" + fileName));    
				Workbook workbook = Workbook.getWorkbook(f);    
								
				//일단 첫번째 Sheet만 처리
				
				int sheetCnt = workbook.getNumberOfSheets();
				for(int s=0; s<sheetCnt; s++) {
					Sheet sheet = workbook.getSheet(s);   
					int rows = sheet.getRows();   	// 시트의 행의 수를 반환한다.  
					int cols = sheet.getColumns();  // 시트의 컬럼의 수를 반환한다.    
					errMsg += " : " + Integer.toString(cols);
					errMsg += " : " + Integer.toString(rows);
					
					for(int i=0; i<rows; i++) {
						String[][] args	= new String[204][2];
						for(int j=0; j<args.length; j++) {
							args[j][0] = "S";
							args[j][1] = "";
						}
						//if(cols>args.length)
						//	cols = args.length;
						
						args[0][1] = uploadNum;
						args[1][1] = Integer.toString(s);
						args[2][1] = Integer.toString(i+1);
						args[3][1] = sheet.getName();
						for(int j=0; j<cols; j++) {
							args[4 +j][1] = sheet.getCell(j, i).getContents(); 
						}
						
						XDAO.ExecProc(request, "sm", "SP_XM_EXCEL_DATA", args);
					}
				}
				
			}
	   }
	}catch(IOException ioe){
		errMsg = "UploadExcelData.jsp 오류 : " + ioe;
	}catch(Exception ex){
		errMsg = "UploadExcelData.jsp 오류 : " + ex;
	}
	if (!errMsg.equals(""))
		System.out.println(errMsg);
%>

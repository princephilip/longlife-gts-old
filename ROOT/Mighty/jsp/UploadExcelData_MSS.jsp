	<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<%@page import="com.oreilly.servlet.MultipartRequest,
					com.oreilly.servlet.multipart.DefaultFileRenamePolicy,
					java.util.*,
					java.io.*,
					jxl.*,
					xgov.core.dao.*,
					xgov.core.env.XLog,
									org.apache.poi.xssf.usermodel.*,
									org.apache.poi.hssf.usermodel.*,java.text.SimpleDateFormat,
									java.text.DecimalFormat"
	%>

	<%
		String realFolder = application.getRealPath("/");
		String errMsg  = "OK";
		int  	 maxSize 	 = 100*1024*1024;
		int	 	 fileCnt	 = 0;
		int	 	 fileSize	 = 0;
		String uploadNum = "";

		try
		{
		   MultipartRequest multi = new MultipartRequest(request, realFolder, maxSize, "utf-8", null);
			 Enumeration params = multi.getParameterNames();

		   while(params.hasMoreElements()){
			  String fname = (String)params.nextElement();
			  String value = multi.getParameter(fname);
			  uploadNum  = multi.getParameter("uploadId");
			  String[][] d_args	= new String[1][2];
			  d_args[0][0] = "S";
			  d_args[0][1] = uploadNum;
			  XDAO.ExecProc(request, "sm", "PROC_XM_EXCEL_DATA_DEL", d_args);
		   }

		   Enumeration files = multi.getFileNames();

		   while(files.hasMoreElements()){
				String fname = (String)files.nextElement();
				//String type = multi.getContentType(fname);

				File f = multi.getFile(fname);

				System.out.println("fname : " + fname);
				System.out.println("getName : " + f.getName().toUpperCase());
				System.out.println("getName : " + f.getName().toUpperCase().indexOf("XLSX"));

				if(f!=null){
					fileCnt++;
					fileSize+=f.length();

					//if(f.getName().toUpperCase().indexOf("XLSX")>0) {
							FileInputStream inputStream = new FileInputStream(f);
							XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
							XSSFRow row;
							XSSFCell cell;

							//첫번째 Sheet만 업로드
							//전체 sheet 업로드(2016.08.17 KYY)
							int sheetCnt = workbook.getNumberOfSheets();
							XSSFFormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();

							for(int s=0; s<sheetCnt; s++) {
								XSSFSheet sheet = workbook.getSheetAt(s);
								int firstRow = sheet.getFirstRowNum();
								int lastRow = sheet.getLastRowNum();

								for(int r=0; r<=lastRow; r++) {
									String[][] args	= new String[204][2];
									for(int j=0; j<args.length; j++) {
										args[j][0] = "S";
										args[j][1] = "";
									}
									row = sheet.getRow(r);

									if(row!=null) {
										int firstCell = row.getFirstCellNum();
										int lastCell  = row.getLastCellNum();

										args[0][1] = uploadNum;
										args[1][1] = Integer.toString(s);
										args[2][1] = Integer.toString(r+1);
										args[3][1] = workbook.getSheetName(s);

										for(int c=0; c<=lastCell; c++) {
											cell = row.getCell(c);
											if (cell != null) {
												 String value = "";
												 switch (cell.getCellType()) {
													case XSSFCell.CELL_TYPE_BOOLEAN:
														 boolean bdata = cell.getBooleanCellValue();
														 value = String.valueOf(bdata);
														 break;
													case XSSFCell.CELL_TYPE_NUMERIC:
															if (HSSFDateUtil.isCellDateFormatted(cell)){
																SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
																value = formatter.format(cell.getDateCellValue());
															} else{
																double ddata = cell.getNumericCellValue();
																//value = String.valueOf(ddata); //원본

																//정수 지수변환 제거 2016.08.02 이상규
																DecimalFormat df = new DecimalFormat("#.#############");
																value = df.format(ddata);
															}
														 break;
													case XSSFCell.CELL_TYPE_STRING:
														 value = cell.toString();
														 break;
													case XSSFCell.CELL_TYPE_BLANK:
													case XSSFCell.CELL_TYPE_ERROR:
													case XSSFCell.CELL_TYPE_FORMULA:
															if(!(cell.toString()=="") ){
																if(evaluator.evaluateFormulaCell(cell)==0){
																	double fddata = cell.getNumericCellValue();
																	value = String.valueOf(fddata);
																}else if(evaluator.evaluateFormulaCell(cell)==1){
																	value = cell.getStringCellValue();
																}else if(evaluator.evaluateFormulaCell(cell)==4){
																	boolean fbdata = cell.getBooleanCellValue();
																	value = String.valueOf(fbdata);
																}
															}
														 break;
												 }
												 args[4 +c][1] = value;
											 } else {
												 args[4 +c][1] = "";
											 }
										}

										XDAO.ExecProc(request, "sm", "PROC_XM_EXCEL_DATA", args);
										//값 초기화
										for(int j=0; j<args.length; j++) {
											args[j][1] = "";
										}
									}
								}
							}
							inputStream.close();
							workbook.close();
	/*
					}
					else {

							Workbook workbook = Workbook.getWorkbook(f);
							//첫번째 Sheet만 업로드
							//전체 sheet 업로드(2016.08.17 KYY)
							int sheetCnt = workbook.getNumberOfSheets();
							for(int s=0; s<sheetCnt; s++) {
								try {
										Sheet sheet = workbook.getSheet(s);
										int rows = sheet.getRows();   	// 시트의 행의 수를 반환한다.
										int cols = sheet.getColumns();  // 시트의 컬럼의 수를 반환한다.

										for(int i=0; i<rows; i++) {
											String[][] args	= new String[204][2];
											for(int j=0; j<args.length; j++) {
												args[j][0] = "S";
												args[j][1] = "";
											}
											//if(cols>args.length)
											//	cols = args.length;

											args[0][1] = uploadNum;
											args[1][1] = Integer.toString(s);
											args[2][1] = Integer.toString(i+1);
											args[3][1] = sheet.getName();
											for(int j=0; j<cols; j++) {
												args[4 +j][1] = sheet.getCell(j, i).getContents();
											}

											XDAO.ExecProc(request, "sm", "PROC_XM_EXCEL_DATA", args);
										}

								} catch( Exception e) {
								}

							}

							workbook.close();
					}
	*/
				}
		   }

		}catch(IOException ioe){
			errMsg = "UploadExcelData.jsp 오류1 : " + ioe.getMessage();
		}catch(Exception ex){
			errMsg = "UploadExcelData.jsp 오류2 : " + ex.getMessage();
		}
		if (!errMsg.equals("") && !errMsg.equals("OK"))
			System.out.println(errMsg);
	%>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@page import="java.io.File"%>
<%@page import="java.util.Enumeration"%>
 
<%
	String csvFileKey = request.getParameter("fkey");
	response.reset();
	response.setContentType("application/text");
	response.setHeader("Content-Disposition", "attachment; filename = " + csvFileKey + ".csv");
	
	try
	{
		out.clear(); 
		out=pageContext.pushBody();

	  ServletOutputStream out2 = response.getOutputStream();
	  out2.write(((String)xgov.core.dao.XDAO._ResultData.get(csvFileKey)).getBytes("UTF-8"));
	  out2.flush();
	  out2.close();	
		if(xgov.core.dao.XDAO._ResultData.containsKey(csvFileKey)) {
			xgov.core.dao.XDAO._ResultData.remove(csvFileKey);
		}
	}
	catch (Exception ex)
	{
		xgov.core.env.XLog.Write("system", "err", "XFileUtil : " + ex.getMessage() + ex.getStackTrace());
	}
%>
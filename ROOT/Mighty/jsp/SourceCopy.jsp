<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%
	String	f_pgm = request.getParameter("fpgm");
	String	t_pgm = request.getParameter("tpgm");
	String	app_dir =  xgov.core.com.XGlobals.ROOT_PATH + "/APPS/";
	java.util.Date dt = new java.util.Date();
	java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyyMMdd-HHmmss"); 
					
	String[]  sourceFiles = {
		app_dir + f_pgm.substring(0,2).toLowerCase() + "/js/" + f_pgm + ".js",
		app_dir + f_pgm.substring(0,2).toLowerCase() + "/jsp/" + f_pgm + ".jsp",
		//app_dir + f_pgm.substring(0,2).toLowerCase() + "/grid/" + f_pgm + ".Grid.js",
		app_dir + f_pgm.substring(0,2).toLowerCase() + "/query/" + f_pgm + ".xmlx"
	};

	String[]  destFiles = {
		app_dir + t_pgm.substring(0,2).toLowerCase() + "/js/" + t_pgm + ".js",
		app_dir + t_pgm.substring(0,2).toLowerCase() + "/jsp/" + t_pgm + ".jsp",
		//app_dir + t_pgm.substring(0,2).toLowerCase() + "/grid/" + t_pgm + ".Grid.js",
		app_dir + t_pgm.substring(0,2).toLowerCase() + "/query/" + t_pgm + ".xmlx"
	};

	String[]  destFiles2 = {
		app_dir + t_pgm.substring(0,2).toLowerCase() + "/js/" + t_pgm + "_" + sdf.format(dt) + ".js",
		app_dir + t_pgm.substring(0,2).toLowerCase() + "/jsp/" + t_pgm + "_" + sdf.format(dt) + ".jsp",
		//app_dir + t_pgm.substring(0,2).toLowerCase() + "/grid/" + t_pgm + "_" + sdf.format(dt) + ".Grid.js",
		app_dir + t_pgm.substring(0,2).toLowerCase() + "/query/" + t_pgm + ".xmlx"
	};
	
	for(int i=0; i<sourceFiles.length; i++) {
		java.io.File file = new java.io.File(destFiles[i]);
		if (file.exists()) {
			//백업파일 생성
			xgov.core.util.XFileUtil.CopyFile(destFiles[i], destFiles2[i]);
		}	
		xgov.core.util.XFileUtil.CopyFile(sourceFiles[i], destFiles[i]);
	}
%>

<html>
<head>
	<title>소스 복사</title>
</head>

<body>
	OK
	<%=destFiles2[2]%>
</body>

</html>
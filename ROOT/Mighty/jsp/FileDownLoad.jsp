<%
	/* 
	 * Object 			: FileDownLoad.jsp
	 * @Description : 파일다운로드
	 * @author 			: 엑스인터넷정보 이상규
	 * @since 			: 2017.05.15
	 * @version 		: 
	 *
	 * @Modification Information
	 * <pre>
	 *   since    	     author      Description
	 *  ------------    --------    ----------------------------
	 *   2017.05.15      이상규      최초생성
 	 */
  String fileName = request.getParameter("fileName");
  String filePath = request.getParameter("filePath");
  
	if ( filePath != null && !"".equals(filePath) ) {
		String sRootPath    = xgov.core.env.XConfiguration.getString("Globals.fileStorePath");
		String UTF8FilePath = new String(filePath.getBytes("8859_1"), "UTF-8");
		
		if ( sRootPath.substring(sRootPath.length() - 1).equals("\\") && UTF8FilePath.substring(0, 1).equals("/") ) {
			sRootPath = sRootPath.substring(0, sRootPath.length() - 1);
			sRootPath = sRootPath.replaceAll("\\\\", "/");
		}
		
		java.io.File file = new java.io.File(sRootPath + UTF8FilePath);
    byte bytestream[] = new byte[(int)file.length()];
		
		response.reset();
		
		String userAgent      = request.getHeader("User-Agent");
		String fileNameToSave = "";
		
		if ( userAgent.indexOf("MSIE") != -1 || userAgent.indexOf("Windows") != -1 ) {
	    // 공백이 '+'로 인코딩된것을 다시 공백으로 바꿔준다.
	    fileNameToSave = java.net.URLEncoder.encode(fileName, "UTF8").replaceAll("\\+", " ");
	    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNameToSave + "\";");
		} else {
	    fileNameToSave = new String(fileName.getBytes("UTF-8"), "8859_1");
	    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNameToSave + "\";");
		}
		
		response.setHeader("Content-Type","application/octet-stream; charset=UTF-8");
		response.setHeader("Content-Length", ""+ file.length());
		response.setHeader("Content-Transfer-Encoding", "binary;");
		response.setHeader("Pragma", "no-cache;");
		response.setHeader("Expires", "-1;");
		
		/*
		System.out.println("fileName: [" + fileName + "]");
		System.out.println("filePath: [" + filePath + "]");
		System.out.println("fileNameToSave: [" + fileNameToSave + "]");
		System.out.println("sRootPath: [" + sRootPath + "]"); 
		System.out.println("UTF8FilePath: [" + UTF8FilePath + "]"); 
		System.out.println("RealFilePath: [" + sRootPath + UTF8FilePath + "]"); 
		System.out.println("file.isFile(): [" + file.isFile() + "]");
		System.out.println("file.length(): [" + file.length() + "]");
		*/
		
		if ( file.isFile() && file.length() > 0 ) {
	    java.io.FileInputStream fis      = new java.io.FileInputStream(file);
	    java.io.BufferedInputStream bis  = new java.io.BufferedInputStream(fis);
	    java.io.BufferedOutputStream bos = new java.io.BufferedOutputStream(response.getOutputStream());

	    int read = 0;

	    while ( (read = bis.read(bytestream)) != -1 ) {
	      bos.write(bytestream , 0, read);
	    }
	    
	    bos.close();
	    bis.close();
		}
		else {
			
		}
	}
%>
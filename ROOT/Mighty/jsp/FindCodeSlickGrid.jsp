<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>

<style>
	.sp_hd{overflow:hidden;display:inline-block;font-size:0;color:transparent;line-height:0}
	.sp_hd:after{content:'';display:inline-block;background:url(/Theme/images/btn/sp_buttons.png) no-repeat;background-size:300px auto}

	.nav li.recent_wrap button.sp_hd{background:none}
	.nav li.recent_wrap button.sp_hd:after{position:absolute;top:6px;left:50%;width:25px;height:27px;margin-left:13px;background-position:-233px 0;background-color:none}
	.nav li.recent_wrap button.sp_hd:active:after{background-position:-260px 0}
	.nav li.recent_wrap button.sp_hd img{position:absolute;top:10px;left:50%;z-index:10;width:23px;height:21px;margin-left:-12px}
	.nav li.recent_wrap.disable button.sp_hd:after{background-position:-206px 0}
	.nav li.recent_wrap.disable button.sp_hd img{display:none}

	.keyword_swipe{float:left;overflow:hidden;height:39px;margin:3px 40px 0 2px;white-space:nowrap;overflow-x:auto;-webkit-overflow-scrolling:touch}
	.keyword_swipe ul{display:table}
	.keyword_swipe li{display:table-cell;padding-right:6px;vertical-align:top}
	.keyword_swipe li span{position:relative;display:inline-block;height:22px;padding:0 29px 0 8px;border-radius:2px;background:#2e8de5}
	.keyword_swipe li span:after{display:block;position:absolute;top:0;right:21px;width:1px;height:100%;background:url(/Theme/images/bg_srp_line.png) repeat-y;background-size:1px auto;content:""}
	.keyword_swipe li .word{font-size:12px;font-weight:normal;line-height:24px;text-decoration:none;color:#fff}
	.keyword_swipe li .del{display:inline-block;position:absolute;top:4px;right:3px;width:15px;height:14px}
	.keyword_swipe li .del:after{position:absolute;top:3px;right:4px;width:8px;height:8px;background-position:-189px -61px}
	.keyword_swipe li button{background:none;font-weight:normal; border:0px;}
	.keyword_swipe .inp_blank{display:inline-block;width:80px;height:24px;font-size:0;box-sizing:border-box;-webkit-box-sizing:border-box}
	.keyword_swipe button.word {font-size:13px;}


	#BOX_FIND_CODE {
		float:left;
		border-style: solid;
		border-width: 2px 0px 2px 2px;
		border-color: #0f6ba8;
		/*border-color: #c0dade;*/
		background-color: #ffffff; 
		height:32px;
		margin:0px 0px 0px 0px;
		padding:0px 0px 0px 1px;
		/*margin:0px 0px 2px 1px;  padding:0px 1px 1px 1px;*/
		text-align:left;
		vertical-align:middle;
		color:#000000; 
	}

	.FIND_CODE {
		border:0px !important;
		padding:0px 0px 0px 6px;
		border-width: 0px 0px 0px 0px !important;
		font-family:나눔고딕,NanumGothic,'Nanum Gothic', Dotum, Gulim, verdana, arial, sans-serif;   
		font-size:13px;
	}

	.FIND_CODE.focus {
		border:0px !important;
		border-width: 0px 0px 0px 0px !important;
	}

</style>

<div id='div_criteria' style='float:left;margin-bottom:5px;line-height:40px;'>
	<div id='BOX_FIND_CODE' >
		<input id='FIND_CODE' class='FIND_CODE' type='text' style="ime-mode:active;height:28px;border:0px;">

		<div class="keyword_swipe" id="keyword_swipe">
			<ul>
			</ul>
		</div>
	</div><button id="btn_search" class="btn_search" aria-label="검색" name="" style="height: 32px;"><span class="gb_bg gb_mag"></span></button>
</div>

<div id="grid_1" style='float:left;width:100%;height:655px;'>
	<div id="dg_1" style='float:left;width:100%;height:100%;' class='slick-grid'>
	</div>
</div>

<div id="btn_space" class="grid_row_space" style="height:7px" style="display:none;"></div>
<div id="dialog_btns" class="ui-dialog-buttonpane ui-widget-content" style="display:none;">
	<div class="ui-dialog-buttonset" style="float:right;">
		<button id="btn_ok" 	class="btn btn-success btn-sm" >선택</button>
		<button id="btn_cancel" class="btn btn-default btn-sm" >취소</button>
	</div>
</div>

<script type="text/javascript" charset="UTF-8">
	var _XSerachMode = false;

	div_criteria.ResizeInfo = {init_width:1000, init_height:40, anchor:{x1:1,y1:1,x2:1,y2:0}};
	BOX_FIND_CODE.ResizeInfo = {init_width:950,  init_height:32, anchor:{x1:1,y1:0,x2:1,y2:0}};
	FIND_CODE.ResizeInfo = {init_width:950,  init_height:28, anchor:{x1:1,y1:0,x2:1,y2:0}};

	//수정(2015.06.30 KYY)
	if(isIE) {
		grid_1.ResizeInfo = {init_width:1000, init_height:630, anchor:{x1:1,y1:1,x2:1,y2:1}};
		dialog_btns.ResizeInfo = {init_width:1000, init_height:40, anchor:{x1:1,y1:1,x2:1,y2:1}};

		$("#btn_ok").click(function() { x_Confirm(); });
		$("#btn_cancel").click(function() { window.close(); });
		$("#btn_space").show();
		$("#dialog_btns").show();
	} else {
		grid_1.ResizeInfo = {init_width:1000, init_height:670, anchor:{x1:1,y1:1,x2:1,y2:1}};
	}

	var setFindFocus = function() {
		_XSerachMode = false;
		$("#keyword_swipe").hide();
		$("#FIND_CODE").show().focus();
		//$("#FIND_CODE").show().focus().select();		
	}

	$("#btn_search").click(function(){StartSearch();});
	//엔터키 조회 처리(2016.06.08 이상규)
	$("#FIND_CODE").keydown(function(e) {
		switch(e.keyCode) {
			case KEY_ENTER:
				StartSearch();
				break;
			case KEY_DOWN:
				dg_1.SetRow(1);
				break;
		}		
	});

	$("#BOX_FIND_CODE").click(function(){
		setFindFocus();
		//$("#keyword_swipe").hide();
		//$("#FIND_CODE").show().focus().select();		
	});

	function xe_GridBeforeKeyDown(a_obj, event, a_key, a_ctrl, a_alt, a_shift) {
		switch(a_key) {
			case KEY_ENTER:
			case KEY_SPACE_BAR:
			//case KEY_F7:
				x_ReturnRowData(dg_1, dg_1.GetRow());
				return 0;
			case KEY_UP:
				if(dg_1.GetRow()<=1 || a_ctrl) {
					setFindFocus();
				}
				break;
		}
		return 100;
	}
</script>

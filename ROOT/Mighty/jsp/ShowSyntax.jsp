<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%
 /**
  * @JSP Name : MF010.jsp
  * @Description : 코드찾기 templete
  * @Modification Information
  * 
  *   수정일     수정자          수정내용
  *  -------    --------    ---------------------------
  *  2012.12.01  김양열          최초 생성
  *
  * author (주)엑스인터넷정보 
  * Copyright (C) 2012 by X-Internet Info.  All right reserved.
  */
 %>

<HTML>
<HEAD>
	<TITLE>소스 보기</TITLE>
	<script type="text/javascript">
		var _Compact = true;
		var _IsModal = (window.dialogArguments?true:false);
		var _IsPopup = (window.opener?true:false);
		var _Caller  = (_IsModal?window.dialogArguments:(_IsPopup?window.opener:window.parent));
		
	</script>

	<ui:resource/>

	<link type="text/css" rel="stylesheet" href="<c:url value='/Mighty/syntaxhighlighter/styles/shCore.css'/>">
	<link type="text/css" rel="stylesheet" href="<c:url value='/Mighty/syntaxhighlighter/styles/shCoreDefault.css'/>">

	<script type="text/javaScript" src="<c:url value='/Mighty/syntaxhighlighter/scripts/shCore.js'/>"></script>
	<script type="text/javaScript" src="<c:url value='/Mighty/syntaxhighlighter/scripts/shBrushJScript.js'/>"></script>

	<script type="text/javascript">
		function xm_UnloadForm() {
		}		
	</script>
</HEAD>

<body style="verticalAlign: top; text-align: left; display: inline; " bgcolor="#ffffff">

	<div id="pageLayout" style='position:absolute;left:0px;top:0px;background-color:#ffffff;font-family:Arial;font-size:11pt;color:#5BF9F5;overflow:auto;z-index:1;margin:5px; padding:0px;display:none;'>
		<pre id='main_syntax' class='brush: js;'>
		</pre>
	</div>

	<script type="text/javascript">
		function xm_InitForm() {
			$("#main_syntax").html(_Caller._ParamValue);
			SyntaxHighlighter.all();
			pageLayout.style.display = "inline";
		}

		$(window).bind("load", xm_InitForm);
	</script>

</body>

</html>
<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>

<%
	String pcd = "sm0000";	//request.getParameter("pcd");
	String params = request.getParameter("params");
	String subUrl = "/APPS/" + pcd.substring(0,2).toLowerCase() + "/jsp/" + pcd + ".jsp?params="+params;
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
	<title>Mighty eGov Framework</title>

	<link type="text/css" charset="utf-8" href="/Mighty/css/mighty-1.2.5.css"  rel="stylesheet" media="all" />

	<script type="text/javascript">
		var _Compact = false;
		var _PGM_CODE = "<%=pcd%>";
		var _WIN_OPEN_DATE = "<%=xgov.core.util.XUtility.GetCurrentDate("yyyy-MM-dd HH:mm:ss")%>";
	</script>

	<style>
		html{height:100%;}
		body{height:100%;}
	</style>
</head>

<body style="verticalAlign: top; text-align: left; display: inline;" bgcolor="#ffffff" topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" onLoad="jf_initial()" onresize="jf_resize()">

	<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="12" height="29"><img src="/images/msgbox/prog_tb_top_left.gif"></td>
			<td height="29" background="/images/msgbox/prog_tb_top_center.gif">
				<div style="float:left"><img src="/images/msgbox/prog_mighty-x.gif"></div>
				<div style="float:right;padding-top:7px;"><img src="/images/msgbox/prog_btn_close.gif" onclick="jf_close()" style="cursor:pointer;"></div>
			</td>
			<td width="12" height="29"><img src="/images/msgbox/prog_tb_top_right.gif"></td>
		</tr>

		<tr>
			<td height="20" background="/images/msgbox/prog_tb_mid_left.gif" bgcolor="#f0f0f0"></td>
			<td bgcolor="#f0f0f0" valign="middle" align="center"></td>
			<td background="/images/msgbox/prog_tb_mid_right.gif" bgcolor="#f0f0f0"></td>
		</tr>

		<tr>
			<td background="/images/msgbox/prog_tb_mid_left.gif" bgcolor="#f0f0f0"></td>
			<td bgcolor="#ff0000" valign="middle" align="center">
				대한민국
			</td>
			<td background="/images/msgbox/prog_tb_mid_right.gif" bgcolor="#f0f0f0"></td>
		</tr>

		<tr>
			<td height="35" background="/images/msgbox/prog_tb_mid_left.gif" bgcolor="#f0f0f0"></td>
			<td bgcolor="#f0f0f0" valign="top" align="center">
				<a href="javascript:;" onclick="jf_close()"><img src="/images/msgbox/prog_btn_ok.gif" border="0"></a>
			</td>
			<td background="/images/msgbox/prog_tb_mid_right.gif" bgcolor="#f0f0f0"></td>
		</tr>

		<tr>
			<td height="20"><img src="/images/msgbox/prog_tb_bot_left.gif"></td>
			<td background="/images/msgbox/prog_tb_bot_center.gif"></td>
			<td ><img src="/images/msgbox/prog_tb_bot_right.gif"></td>
		</tr>
	</table>
</body>

<SCRIPT language="JavaScript">
	function	jf_initial() {
	}

	function	jf_resize() {
		var	newheight = window.document.body.offsetHeight - 40 + 13;
		var	newtop	 = window.document.body.offsetHeight - 15;
		var	newwidth  = window.document.body.offsetWidth  - 4;
		var	newwidth2 = window.document.body.offsetWidth  - 7;

		//td_contents.style.height = newheight;

		//alert(newheight);
	}

	function jf_close() {
	}

</SCRIPT>

</html>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
 /**
  * @JSP Name : mdimenu.jsp
  * @Description : 메뉴관련 처리
  * @Modification Information
  * 
  *   수정일     수정자          수정내용
  *  -------    --------    ---------------------------
  *  2012.12.01  김양열          최초 생성
  *
  * author (주)엑스인터넷정보 
  * Copyright (C) 2012 by X-Internet Info.  All right reserved.
  */
%>

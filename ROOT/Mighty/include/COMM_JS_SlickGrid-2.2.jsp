﻿<link rel="stylesheet" href="/Mighty/3rd/SlickGrid/v2.2/slick.grid.css" type="text/css"/>
<link rel="stylesheet" href="/Mighty/3rd/SlickGrid/v2.2/slick-default-theme.css" type="text/css"/>
<link rel="stylesheet" href="/Mighty/3rd/SlickGrid/v2.2/controls/slick.pager.css" type="text/css"/>
<link rel="stylesheet" href="/Mighty/3rd/SlickGrid/v2.2/controls/slick.columnpicker.css" type="text/css"/>
<link rel="stylesheet" href="/Mighty/3rd/SlickGrid/v2.2/plugins/slick.headermenu.css" type="text/css"/>

<link rel="stylesheet" href="/Mighty/3rd_extends/SlickGrid/slick.grid.css" type="text/css"/>
<link rel="stylesheet" href="/Mighty/3rd_extends/SlickGrid/slick-default-theme.css" type="text/css"/>
<link rel="stylesheet" href="/Mighty/3rd_extends/SlickGrid/plugins/slick.headermenu.css" type="text/css"/>

<style>
  .cell-title {
    font-weight: bold;
  }

  .cell-effort-driven {
    text-align: center;
  }
</style>

<script type="text/javaScript" src="/Mighty/3rd/SlickGrid/v2.2/lib/firebugx.js"></script>
<script type="text/javaScript" src="/Mighty/3rd/SlickGrid/v2.2/lib/jquery.event.drag-2.2.js"></script>

<script type="text/javaScript" src="/Mighty/3rd/SlickGrid/v2.2/slick.core-2.2.js"></script>
<script type="text/javaScript" src="/Mighty/3rd/SlickGrid/v2.2/slick.formatters.js"></script>
<script type="text/javaScript" src="/Mighty/3rd/SlickGrid/v2.2/slick.editors.js"></script>
<script type="text/javaScript" src="/Mighty/3rd/SlickGrid/v2.2/slick.grid-2.2.js"></script>
<script type="text/javaScript" src="/Mighty/3rd/SlickGrid/v2.2/slick.dataview.js"></script>
<script type="text/javaScript" src="/Mighty/3rd/SlickGrid/v2.2/slick.groupitemmetadataprovider.js"></script>

<script type="text/javaScript" src="/Mighty/3rd/SlickGrid/v2.2/plugins/slick.rowselectionmodel.js"></script>
<script type="text/javaScript" src="/Mighty/3rd/SlickGrid/v2.2/plugins/slick.headermenu.js"></script>
<script type="text/javaScript" src="/Mighty/3rd/SlickGrid/v2.2/plugins/slick.checkboxselectcolumn.js"></script>

<script type="text/javaScript" src="/Mighty/js/mighty-event-SlickGrid-1.2.8.js"></script>
<script type="text/javaScript" src="/Mighty/js/mighty-dao-SlickGrid-1.4.0.js"></script>

<%
	//StringBuffer htmlBuffer = new StringBuffer();
	String revcssPath = "/Mighty/css/old";
	String userAgent = request.getHeader("User-Agent");

	boolean _Compact = false;
%>

<script type="text/javascript" charset="utf-8">
//<![CDATA[
var _web_context = {
        path     : '',
        jsPath   : '/Mighty/js',
        cssPath  : '/Mighty/css',
        themePath : '/Theme'
        //,theme    : 'ui-lightness'
        //,themeCssPath    : '/Mighty/jQuery/css/cupertino'
};
//]]>
  var _Epms = true;
</script>

<jsp:include page="/Theme/jsp/comm_inc_pre.jsp" flush="true" >   
	<jsp:param name="revcssPath" value="<%=revcssPath%>"/>
</jsp:include>

<script type="text/javaScript" src="/Mighty/3rd/jQuery/js/jquery-1.11.3.min.js"></script>
<script type="text/javaScript" src="/Mighty/3rd/jQuery/ui/jquery.ui.core.min.js"></script>
<script type="text/javaScript" src="/Mighty/3rd/jQuery/ui/jquery.ui.widget.min.js"></script>

<script type="text/javaScript" src="/Mighty/js/mighty-2.0.0.js?v=201810180000"></script>
<script type="text/javaScript" src="/Mighty/js/mighty-comm-1.2.8.js?v=201810180000"></script>
<script type="text/javaScript" src="/Mighty/js/mighty-dao-1.2.6.js?v=201810180000"></script>

<script type="text/javaScript" src="/Mighty/3rd/jQuery/jquery-ui-1.11.4/jquery-ui.min.js"></script>
<script type="text/javaScript" src="/Mighty/3rd/selectBox/jquery.selectBox.null.js"></script>
<script type="text/javaScript" src="/Mighty/js/mighty-maskedinput-1.0.1.min.js"></script>
<script type="text/javaScript" src="/Mighty/3rd/jQuery/js/jquery.blockUI-2.7.0.js"></script>

<script type="text/javaScript" src="/Mighty/3rd/crypto/base64.js"></script> 
<script type="text/javaScript" src="/Mighty/3rd/jQuery/js/jquery.form.min.js"></script>
<script type="text/javaScript" src="/Mighty/js/mighty-print-ireport-1.0.0.js"></script>

<% if(false) { %>
  <script type="text/javaScript" src="/Mighty/js/mighty-dev-1.0.0.js"></script>
<% } %>

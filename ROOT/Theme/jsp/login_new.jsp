<%@ page contentType="text/html; charset=utf-8"%>
<%
 /**
  * @Class Name : login.jsp
  * @Description : Login 인증 화면
  * @Modification Information
  * @
  * @  수정일         수정자                   수정내용
  * @ -------    --------    ---------------------------
  * @ 2015.10.13    김성한        CSS 레이아웃 사용하도록 로그인 재작성
  * @
  */
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
  <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
  <link rel="stylesheet" href="/Theme/css/login.css" type="text/css">
  <title>수발주시스템</title>
  <script type="text/javaScript" src="/Mighty/3rd/jQuery/js/jquery-1.11.2.js"></script>
  <script type="text/javaScript" language="javascript">
    try {document.execCommand('BackgroundImageCache', false, true);} catch(e) {}    
    
    function pf_actionLogin() {
        if ($("#companyCode").val() =="") {
            $("#companyCode").val("100");
        }
        if (document.loginForm.business.value =="") {
            alert("사업자번호를 입력해 주시기 바랍니다.");
            document.loginForm.business.focus();
        } else if (document.loginForm.userid.value =="") {
            alert("아이디를 입력해 주시기 바랍니다.");
            document.loginForm.userid.focus();
        } else if (document.loginForm.password.value =="") {
        		alert("비밀번호를 입력해 주시기 바랍니다.");
            document.loginForm.password.focus();
        } else {
          pf_saveid(document.loginForm);
          document.loginForm.action="/com/actionLogin.do";
          
          document.loginForm.strongpwd.value = (chkPassword(document.loginForm.password.value)?"Y":"N");
          document.loginForm.submit();
        }
    }
    
    function chkPassword(str) {
        var passCnt = 0;

        if(/[a-z]/.test(str)) passCnt++;
        if(/[A-Z]/.test(str)) passCnt++;
        if(/[0-9]/.test(str)) passCnt++;
        if(/[!@#\$%\^&\*]/.test(str)) passCnt++;
        
        //세가지 조건이상 만족하면 PASS
        return (passCnt>=3);
    }
    
    function pf_goHelp() {
      alert('도움말 찾기 준비 중입니다');
    }
    
    function pf_setCookie_old (name, value, expires) {
        document.cookie = name + "=" + escape (value) + "; path=/; expires=" + expires.toGMTString() ;
        // + "; httpOnly";  //보안관련 설정추가 (2015.02.26 KYY) - 쿠키정보를 읽을수없다.
    }
    
    function pf_setCookie (name, value) {
      var argv = pf_setCookie.arguments;
      var argc = pf_setCookie.arguments.length;
      var expires;
      if (argc > 2) {
        expires = argv[2];
      } else {
        expires = new Date();
        expires.setTime(expires.getTime() + (30 * 24 * 60 * 60 * 1000));
      }
      var path = (argc > 3) ? argv[3] : null;
      var domain = (argc > 4) ? argv[4] : null;
      var secure = (argc > 5) ? argv[5] : false;
      document.cookie = name + "=" + escape (value) +
      ((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
      ((path == null) ? "" : ("; path=" + path)) +
      ((domain == null) ? "" : ("; domain=" + domain)) +
      ((secure == true) ? "; secure" : "");
    }
        
    function getCookie(Name) {
        var search = Name + "=";
        if (document.cookie.length > 0) { // 쿠키가 설정되어 있다면
            offset = document.cookie.indexOf(search);
            if (offset != -1) { // 쿠키가 존재하면
                offset += search.length;
                // set index of beginning of value
                end = document.cookie.indexOf(";", offset);
                // 쿠키 값의 마지막 위치 인덱스 번호 설정
                if (end == -1)
                    end = document.cookie.length;
                return unescape(document.cookie.substring(offset, end));
            }
        }
        return "";
    }
    
    function pf_saveid(form) {
       var expdate = new Date();
        // 기본적으로 30일동안 기억하게 함. 일수를 조절하려면 * 30에서 숫자를 조절하면 됨
       expdate.setTime(expdate.getTime() + 1000 * 3600 * 24 * 7); // 30일
       pf_setCookie("pf_saveid", (form.saveid.checked ? form.userid.value : ""), expdate);
       pf_setCookie("pf_savebusiness", (form.saveid.checked ? form.business.value : ""), expdate);
    }
    
    function pf_getid(form) {
      var cookieId = getCookie("pf_saveid");
      if(cookieId!=null && cookieId!="") {
        form.saveid.checked = true;
        form.userid.value = getCookie("pf_saveid");
      }
      var cookieBusiness = getCookie("pf_savebusiness");
      if(cookieBusiness!=null && cookieBusiness!="") {
        form.saveid.checked = true;
        form.business.value = getCookie("pf_savebusiness");
      }
    }
    
    $(document).ready(function () {
        var message = document.loginForm.message.value;
        var account = document.loginForm.account.value;
        if (message != "") {
            if (account=="5") alert("비밀번호가 5회이상 틀리면 일시 사용중지가 됩니다!\r\n관리자에게 문의 하십시요!");
              else alert(message);
        }
    
        //pf_saveid(document.loginForm);
        pf_getid(document.loginForm);
        document.loginForm.business.focus();

    });
  </script>
</head>

<body>
  <form name="loginForm" action="/com/actionLogin.do" method="post">
    <input type="hidden" name="strongpwd" value="N">
    <input type="hidden" name="message" value="${message}">
    <input type="hidden" name="account" value="${account}"/>
    <input type="hidden" id="companyCode" name="companyCode" value="${companyCode}"/>
    <div id="login_area">
      <div id="login_bg">
        <div id="login_txt">
          <ul>
            <li><input type="text" name="business" id="business" maxlength="20" tabindex="1" onkeydown="if(event.keyCode==13) userid.focus();" /></li>
            <li><input type="text" name="userid" id="userid" maxlength="20" tabindex="2" onkeydown="if(event.keyCode==13) password.focus();" /></li>
            <li><input type="password" name="password" id="password" maxlength="12" tabindex="3" onkeydown="if(event.keyCode==13) pf_actionLogin();" /></li>
          </ul>
        </div>   
        <div id="login_btn"> 
          <a href="javascript:pf_actionLogin();"><img src="/Theme/images/main/button.png" tabindex="4" /></a>
        </div>
        <div id="login_opt"> 
          <input type="checkbox" id="saveid" name="saveid" value=""/ onFocus="this.blur()"> <span id="login_saveid">아이디저장</span>
        </div>
      </div>
    </div>
  </form>
</body>
</html>

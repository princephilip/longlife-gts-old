<%@ page language="java" contentType="text/html; charset=utf-8" session="true" pageEncoding="utf-8" trimDirectiveWhitespaces="true"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Language" content="ko" >
<base target="_self">
<link href="/Theme/css/password.css" rel="stylesheet" type="text/css" >
<title>비밀번호 변경</title>
<script type="text/javascript" src="<c:url value="/com/validator.do"/>"></script>
<validator:javascript formName="passwordChgVO" staticJavascript="false" xhtml="true" cdata="false"/>

<script type="text/javaScript" language="javascript" defer="defer">
<!--
var _IsModal = (window.dialogArguments?true:false);
var _IsPopup = (window.opener?true:false);
var _Caller  = (_IsModal?window.dialogArguments:(_IsPopup?window.opener:window.parent));


//Override(2017.08.22 KYY) 
function validatePasswordChgVO(form) {                                                                   
  if (bCancel) 
    return true; 
  else
    //자리수만 체크(2017.08.22 KYY)
    return validateRequired(form) && validatePassword1(form); 
    //return validateRequired(form) && validatePassword1(form) && validatePassword2(form) && validatePassword3(form) && validatePassword4(form); 
} 

function pf_UpdatePwd(){
    if(validatePasswordChgVO(document.passwordChgVO)){
        if(passwordChgVO.oldPassword.value == passwordChgVO.newPassword.value ) {
          alert("종전 패스워드와 동일하게 변경할 수 없습니다.");
          return;
        }
        if(document.passwordChgVO.newPassword.value != document.passwordChgVO.newPassword2.value){
            alert("<spring:message code="fail.user.passwordUpdate2" />");
            return;
        }
        document.passwordChgVO.submit();
    }
}

function fn_cancel(){
  _Caller.$("#findmodal").dialog("close");
}
<c:if test="${!empty resultMsg}">
  alert("<spring:message code="${resultMsg}" />");
  if("${resultMsg}"=="success.common.update") _Caller.$("#findmodal").dialog("close");
</c:if>
  

//-->
</script>
</head>
<body>
<noscript>자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>    
<div style="width:420px;height:200px;">
<!-- 상단타이틀 -->
<form name="passwordChgVO" method="post" action="${pageContext.request.contextPath}/user/EgovUserPasswordUpdt.do">
  <div style="height:20px;padding-left:45px;padding-top:20px;">
  <span style="font-size: 14px;font-weight:bold;color:#326004;">사용자 암호변경</span>
  </div>
  <fieldset>
    <dl>
      <dt style="float:left;height:25px;width:100px;padding:5px 0px 0px 80px;">
        <img src="<c:url value='/images/excel/pop_icon.png' />"; alt=""  width="4" height="6">&nbsp;<span style="font-size: 12px;font-weight:bold;color:#326004;">사용자 아이디</span>
      </dt>
      <dd style="float:left;height:25px;padding:5px 0px 0px 20px;">
        <input name="emplyrId" id="emplyrId" title="사용자아이디" type="text" size="25" value="<c:out value='${loginVO}'/>"  maxlength="20" readonly/>
      </dd>
    </dl>
  </fieldset>
  <fieldset>
    <dl>
      <dt style="float:left;height:25px;width:100px;padding:5px 0px 0px 80px;">
        <img src="<c:url value='/images/excel/pop_icon.png' />" alt=""  width="4" height="6">&nbsp;<span style="font-size: 12px;font-weight:bold;color:#326004;">이전 비밀번호</span>
      </dt>
      <dd style="float:left;height:25px;padding:5px 0px 0px 20px;">
        <input name="oldPassword" id="oldPassword" title="이전 비밀번호" type="password" size="25" value=""  maxlength="100" />
      </dd>
    </dl>
  </fieldset>
  <fieldset>
    <dl>
      <dt style="float:left;height:25px;width:100px;padding:5px 0px 0px 80px;">
        <img src="<c:url value='/images/excel/pop_icon.png' />" alt=""  width="4" height="6">&nbsp;<span style="font-size: 12px;font-weight:bold;color:#326004;">신규 비밀번호</span>
      </dt>
      <dd style="float:left;height:25px;padding:5px 0px 0px 20px;">
        <input name="newPassword" id="newPassword" title="신규 비밀번호" type="password" size="25" value=""  maxlength="100" />
      </dd>
    </dl>
  </fieldset>   
  <fieldset>
    <dl>
      <dt style="float:left;height:25px;width:100px;padding:5px 0px 0px 80px;">
        <img src="<c:url value='/images/excel/pop_icon.png' />" alt=""  width="4" height="6">&nbsp;<span style="font-size: 12px;font-weight:bold;color:#326004;">비밀번호 확인</span>
      </dt>
      <dd style="float:left;height:25px;padding:5px 0px 0px 20px;">
        <input name="newPassword2" id="newPassword2" title="비밀번호 확인" type="password" size="25" value=""  maxlength="100" />
      </dd>
    </dl>
  </fieldset> 

  <div class='grid_row_space mt3' style='height: 40px; color: #0000ff; font-weight: bold; line-height: 20px; width:100%; margin-top:15px;'>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;※ 8~15자리 입력<br />
<!--     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;※ 8~15자리 영문 대,소,숫자,특수문자 4가지 혼용<br />
 -->    <!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;※ 패스워드 유효기간 3개월-->
  </div>

</form>
</div>
<!-- //전체 레이어 끝 -->
</body>
</html>


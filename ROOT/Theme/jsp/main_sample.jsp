<%@ page contentType="text/html; charset=utf-8"%>

  <div class="main-photo-wrap">
    <div class="main-photo">

      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
          <!-- li data-target="#carousel-example-generic" data-slide-to="3"></li -->
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <img src="/Theme/images/001.jpg" alt="">
            <div class="carousel-caption">
              
            </div>
          </div>
          <div class="item">
            <img src="/Theme/images/002.jpg" alt="">
            <div class="carousel-caption">
              
            </div>
          </div>
          <div class="item">
            <img src="/Theme/images/003.jpg" alt="">
            <div class="carousel-caption">
              
            </div>
          </div>
          <!-- div class="item">
            <img src="/Theme/images/004.jpg" alt="">
            <div class="carousel-caption">
              
            </div>
          </div -->
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
          <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>

    </div>
  </div>

  <div class="main-more-wrap">
    <ul>
      <li><img src="/Theme/images/more-temp.jpg" /></li>
      <li><img src="/Theme/images/more-temp.jpg" /></li>
      <li><img src="/Theme/images/more-temp.jpg" /></li>
    </ul>
  </row>

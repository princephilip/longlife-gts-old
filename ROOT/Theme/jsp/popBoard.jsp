<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.net.URLDecoder" %> 

<jsp:include page="/Mighty/include/COMM_JS.jsp"/>

<%
String ar_BOARDID=request.getParameter("BOARDID");
%>

<head>
	<title>공지사항</title>
	<link rel="stylesheet" href="/Theme/css/egovbbsTemplate.css" type="text/css">
	<link rel="stylesheet" href="/Theme/css/bbs.css" type="text/css">
</head>
<body style="margin:10px;padding:0" onload="jf_initial()">
	<table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="ffffff" class="generalTable">
		<tbody>
			<tr>
				<th width="15%" height="23" nowrap="" class="title_left">제목</th>
				<td width="85%" colspan="3" nowrap="" id="td_title"></td>
			</tr>
			<tr>
				<th width="15%" height="23" nowrap="" class="title_left">작성자</th>
				<td width="15%" class="lt_text3" nowrap="" id="td_writer"></td>
				<th width="15%" height="23" nowrap="" class="title_left">작성시간</th>
				<td width="45%" class="listCenter" nowrap="" id="td_wdate"></td>
			</tr>
			<tr>
				<th width="15%" height="23" nowrap="" class="title_left">첨부파일</th>
				<td width="85%" colspan="3" nowrap="" id="td_atch" style="line-height:150%"></td>
			</tr>
			<tr>
				<td colspan="4">
					<div id="bbs_cn">
					</div>
				</td>
			</tr>
		</tbody>
	</table>

	<div class="modal" align="right" style="margin-top:5px">
    <a href="javascript:jf_closePop();" class="btn_close" style="color:blue;font-weight:bold">[ 오늘하루열지않음 ]</a>
	</div>
</body>

<script type="text/javaScript" language="javascript">
	function jf_initial(){
		var ls_id = <%=ar_BOARDID%>;
		var ls_rtn = _X.XmlSelect("com", "POPUP", "BOARD_R02", [ls_id],"array");

		td_title.innerHTML  = ls_rtn[0][0];		// 제목
		td_writer.innerHTML = ls_rtn[0][2];		// 작성자
		td_wdate.innerHTML  = ls_rtn[0][1];		// 작성일자
		// 내용
		bbs_cn.innerHTML = ls_rtn[0][3].replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&amp;/g, "&");

		// 첨부파일 ------------------------------------------------------------
		var ls_atch = _X.XmlSelect("com", "POPUP", "BOARD_R03", [ls_rtn[0][4]],"array");
		var ls_atchfile = "";
		var ls_img = '<img align="absMiddle" src="/Theme/images/bbs/Content_file.gif" border="0" hspace="0"></img> ';

		for(var i=0; i < ls_atch.length; i++) {
			if(i > 0) ls_atchfile+="<br>";
			if(_X.ToInt(ls_atch[i][1]) > 0) {
				ls_atchfile += ls_img+ "<a href=\"javascript:_X.FileDownload(\'"+ls_atch[i][0]+"\', "+ls_atch[i][1]+")\" style=\"color:black\">"+ls_atch[i][2]+"</a>";
			}
		}
		td_atch.innerHTML = ls_atchfile;
		// 첨부파일 [끝] ------------------------------------------------------------
	}

	function jf_closePop() {
		setCookie( "popNotice_"+<%=ar_BOARDID%>, "end" , 1);
		window.close();
	}


	/* Cookie 쿠저장 */
	function setCookie(cname, value, expire) {
	   var todayValue = new Date();
	   // 오늘 날짜를 변수에 저장

	   todayValue.setDate(todayValue.getDate() + expire);
	   document.cookie = cname + "=" + encodeURI(value) + "; expires=" + todayValue.toGMTString() + "; path=/;";
	}

</script>
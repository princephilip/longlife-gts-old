<%@ page contentType="text/html; charset=utf-8"%>
<%
  String vI = request.getParameter("i");
%>

<!DOCTYPE html>
<html lang="ko">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
  <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />

  <title>노인장기요양보험 업무 관리 시스템</title>

  <link rel="stylesheet" href="/Mighty/3rd/font-awesome/css/all.css">
  <link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="/Theme/css/login.css?v=20171215172800">

  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <script type="text/javaScript" src="${pageContext.request.contextPath}/Mighty/3rd/jQuery/js/jquery-1.11.2.js"></script>
  <script src="/Mighty/js/AesUtil.js"></script>
  <script src="/Mighty/js/aes.js"></script>
  <script src="/Mighty/js/pbkdf2.js"></script>

  <script type="text/javaScript" language="javascript">
    try {document.execCommand('BackgroundImageCache', false, true);} catch(e) {}

    function pf_actionLogin() {
        if ($("#companyCode").val() =="") {
            $("#companyCode").val("01");
        }
        if (document.loginForm.id.value =="") {
            alert("아이디를 입력하세요");
            document.loginForm.id.focus();
        } else if (document.loginForm.password.value =="") {
            alert("비밀번호를 입력하세요");
            document.loginForm.password.focus();
        } else {
          pf_saveid(document.loginForm);
          document.loginForm.action="/com/actionLogin.do";

          document.loginForm.strongpwd.value = (chkPassword(document.loginForm.password.value)?"Y":"N");
          document.loginForm.submit();
        }
    }

    function chkPassword(str) {
        var passCnt = 0;

        if(/[a-z]/.test(str)) passCnt++;
        if(/[A-Z]/.test(str)) passCnt++;
        if(/[0-9]/.test(str)) passCnt++;
        if(/[!@#\$%\^&\*]/.test(str)) passCnt++;

        //세가지 조건이상 만족하면 PASS
        return (passCnt>=3);
    }

    function pf_goHelp() {
      alert('도움말 찾기 준비 중입니다');
    }

    function pf_setCookie_old (name, value, expires) {
        document.cookie = name + "=" + escape (value) + "; path=/; expires=" + expires.toGMTString() ;
        // + "; httpOnly";  //보안관련 설정추가 (2015.02.26 KYY) - 쿠키정보를 읽을수없다.
    }

    function pf_setCookie (name, value) {
      var argv = pf_setCookie.arguments;
      var argc = pf_setCookie.arguments.length;
      var expires;
      if (argc > 2) {
        expires = argv[2];
      } else {
        expires = new Date();
        expires.setTime(expires.getTime() + (30 * 24 * 60 * 60 * 1000));
      }
      var path = (argc > 3) ? argv[3] : null;
      var domain = (argc > 4) ? argv[4] : null;
      var secure = (argc > 5) ? argv[5] : false;
      document.cookie = name + "=" + escape (value) +
      ((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
      ((path == null) ? "" : ("; path=" + path)) +
      ((domain == null) ? "" : ("; domain=" + domain)) +
      ((secure == true) ? "; secure" : "");
    }

    function getCookie(Name) {
        var search = Name + "=";
        if (document.cookie.length > 0) { // 쿠키가 설정되어 있다면
            offset = document.cookie.indexOf(search);
            if (offset != -1) { // 쿠키가 존재하면
                offset += search.length;
                // set index of beginning of value
                end = document.cookie.indexOf(";", offset);
                // 쿠키 값의 마지막 위치 인덱스 번호 설정
                if (end == -1)
                    end = document.cookie.length;
                return unescape(document.cookie.substring(offset, end));
            }
        }
        return "";
    }

    function pf_saveid(form) {
       var expdate = new Date();
        // 기본적으로 30일동안 기억하게 함. 일수를 조절하려면 * 30에서 숫자를 조절하면 됨
       expdate.setTime(expdate.getTime() + 1000 * 3600 * 24 * 7); // 30일
       pf_setCookie("pf_saveid", (form.saveid.checked ? form.id.value : ""), expdate);
    }

    function pf_getid(form) {
      var cookieId = getCookie("pf_saveid");
      if(cookieId!=null && cookieId!="") {
        form.saveid.checked = true;
        form.id.value = getCookie("pf_saveid");
      }
    }

      function uf_open_ezhelp() {
          //window.showModalDialog("./ezhelp/ezhelp.aspx", window, "dialogWidth:370px; dialogHeight:210px");
          window.open("http://939.co.kr/xinternet");
      }

    $(document).ready(function () {
        var message = document.loginForm.message.value;
        var account = document.loginForm.account.value;
        if (message != "") {
            if (account=="5") alert("비밀번호가 5회이상 틀리면 일시 사용중지가 됩니다!\r\n관리자에게 문의 하십시요!");
              else alert(message);
        }

        //pf_saveid(document.loginForm);
        pf_getid(document.loginForm);
        document.loginForm.id.focus();

    });

    function Upper(e,r) {
      r.value = r.value.toUpperCase();
    }

  </script>

</head>

<body>

	<div class="container-wrap" style="BACKGROUND-IMAGE:url(/Theme/images/login/gts_login_20160216.png); BACKGROUND-REPEAT:no-repeat; margin-left: 165px;">
	  <div class="container">
	    <div class="card card-container">

		    	<form name="loginForm" action="/com/actionLogin.do" method="post" class="form-signin" onsubmit="return pf_actionLogin()" >
		        	<input type="hidden" name="strongpwd" value="N">
		        	<input type="hidden" name="message" value="${message}">
		        	<input type="hidden" name="account" value="${account}"/>
		        	<input type="hidden" id="companyCode" name="companyCode" value="${companyCode}"/>
		        	<span id="reauth-email" class="reauth-email"></span>
		    		  <div id="remember" class="checkbox" style='margin-top:0px;margin-bottom:0px;display:none'>
		           <label style='color:#000000; font-family:"Malgun Gothic"; font-size:12px'>
		             <input type="checkbox" id="saveid" name="saveid" style='margin-left:0px;' value=""> &nbsp;&nbsp;&nbsp; 아이디저장
		           </label>
		          </div>
		    		<div class='row wrap-form-login'>

							<div class="col-xs-1">
								<div class="form-group">
							    <div class="col-xs-1">
							      <input type="text" id="id" name="id" onkeyup="Upper(event,this)" maxlength="20" class="form-control" placeholder="아이디" required style='width:110px;height:21px;'>
							    </div>
						  	</div>
						  	<div class="form-group">
  						    <div class="col-xs-12">
  						      <input type="password" id="password" name="password" class="form-control" placeholder="비밀번호" required style='width:110px;height:21px;'>
  						    </div>
  						  </div>
							</div>

							<div class="col-xs-4 wrap-btn-login">
                <button class="btn btn-signin" type="submit" ></button>
              </div>
						</div>
					</form>
	    </div><!-- /card-container -->
	  </div><!-- /container -->
    <img src="/Theme/images/login/footer_201804.png" style='width: 570px;height:75px;margin:0px 0px 0px 0px'>
  </div><!-- /container-wrap -->

</body>


<script>
  <% if(vI!=null && !vI.equals("null")) { %>
    var keySize = 128;
    var iterationCount = 10000;
    var iv = "F27D5C9927726BCEFE7510B1BDD3D137";
    var salt = "3FF2EC019C627B945225DEBAD71A01B6985FE84C95A70EB132882F88C0A59A55";
    var passPhrase = "passPhrase passPhrase aes encoding algorithm";
    var aesUtil = new AesUtil(keySize, iterationCount);

    var _E_ID = aesUtil.decrypt(salt, iv, passPhrase, "<%=vI%>");
    setTimeout(function() {
      $("#id").val(_E_ID);
      $("#id").prop("readonly", true);
      //$("#password").val("[Auto.Login.STEP]");
      $("#password").val("");
      //loginForm.submit();
     }, 100);
    //$("#password").val("");
  <% } %>
    $("#password").focus();
</script>


</html>

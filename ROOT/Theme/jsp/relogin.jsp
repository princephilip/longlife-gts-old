<%@ page contentType="text/html; charset=utf-8"%>


<!DOCTYPE html>
<html lang="ko">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
  <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />

  <title>라인건설</title>

  <link rel="stylesheet" href="/Mighty/3rd/font-awesome/css/all.css">
  <link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="/Theme/css/login.css">

  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <script type="text/javaScript" src="/Mighty/3rd/jQuery/js/jquery-1.11.2.js"></script>
  <script type="text/javaScript" language="javascript">
    function pf_actionLogin() {
        if ($("#companyCode").val() =="") {
            $("#companyCode").val("HD00");
        }
        if (document.loginForm.id.value =="") {
            alert("아이디를 입력하세요");
            document.loginForm.id.focus();
        } else if (document.loginForm.password.value =="") {
            alert("비밀번호를 입력하세요");
            document.loginForm.password.focus();
        } else {
            //pf_saveid(loginForm);
            document.loginForm.companyCode.value = top._CompanyCode;
            //document.loginForm.action="/com/actionLogin.do";
            document.loginForm.submit();
        }
    }
    
    function pf_setCookie (name, value, expires) {
        document.cookie = name + "=" + escape (value) + "; path=/; expires=" + expires.toGMTString() + "; httpOnly";  //보안관련 설정추가 (2015.02.26 KYY)
    }
    
    function getCookie(Name) {
        var search = Name + "=";
        if (document.cookie.length > 0) { // 쿠키가 설정되어 있다면
            offset = document.cookie.indexOf(search);
            if (offset != -1) { // 쿠키가 존재하면
                offset += search.length;
                // set index of beginning of value
                end = document.cookie.indexOf(";", offset);
                // 쿠키 값의 마지막 위치 인덱스 번호 설정
                if (end == -1)
                    end = document.cookie.length;
                return unescape(document.cookie.substring(offset, end));
            }
        }
        return "";
    }
    
    function pf_saveid(form) {
        var expdate = new Date();
        // 기본적으로 30일동안 기억하게 함. 일수를 조절하려면 * 30에서 숫자를 조절하면 됨
       expdate.setTime(expdate.getTime() + 1000 * 3600 * 24 * 7); // 30일
        pf_setCookie("pf_saveid", form.id.value, expdate);
    }
    
    function pf_getid(form) {
        form.id.value = getCookie("pf_saveid");
    }
    
    function pf_Init() {
        document.loginForm.id.value = top._UserID;
        var message = document.loginForm.message.value;
        var account = document.loginForm.account.value;
        if(message == "OK") {
          //document.loginForm.id.value = "";
          document.loginForm.password.value = "";
          top.CloseReLogin();
        } else { 
          if (message != "") {
            if (account=="5") alert("비밀번호가 5회이상 틀리면 일시 사용중지가 됩니다!\r\n관리자에게 문의 하십시요!");
              else alert(message);
          }
          //pf_saveid(document.loginForm.id);
          //pf_getid(document.loginForm.id);
          document.loginForm.password.focus();
        }
    }
    
    //로그아웃 처리(2015.06.26 KYY) 
    pf_logout = function() {
      //if(confirm("접속을 종료 하시겠습니까?")){
        top.document.location.href="/com/actionLogout.do";
      //}
    }
    
  </script>
</head>

<body onLoad="pf_Init()">
  <div class="container-wrap">
    <div class="container">
      <div class="card card-container">
        <form name="loginForm" action ="/com/actionLogin.do" method="post" class="form-signin">
          <input type="hidden" name="strongpwd" value="N">
          <input type="hidden" name="message" value="${message}">
          <input type="hidden" name="account" value="${account}"/>
          <input type="hidden" id="companyCode" name="companyCode" value="${companyCode}"/>
            <div id="notuse_msg" >
              장기 미사용으로 인하여 로그아웃 되었습니다. 
            </div>

            <span id="reauth-email" class="reauth-email"></span>
            <input type="text" id="id" name="id" maxlength="20" class="form-control input-sm" placeholder="아이디" onKeyDown="javascript:if (event.keyCode == 13) { document.loginForm.password.focus(); }" required readonly>
            <input type="password" id="password" name="password" class="form-control input-sm" placeholder="비밀번호" onKeyDown="javascript:if (event.keyCode == 13) { pf_actionLogin(); }" required autofocus>
            <div class="checkbox">
            </div>
            <button class="btn-signin" type="button" onclick="javascript:pf_actionLogin();"></button>
        </form><!-- /form -->
      </div><!-- /card-container -->
    </div><!-- /container -->
  </div>
  <div id="sso_login" style="display:none;">
    <iframe id="sso" src="" frameborder="0" width="10" height="10"></iframe>
  </div>
</body>
</html>



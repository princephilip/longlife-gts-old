<%
  StringBuffer htmlBuffer = new StringBuffer();
  String revcssPath = "/Mighty/css";
  String userAgent = request.getHeader("User-Agent");

  if (userAgent.indexOf("Firefox/") > -1) {
    htmlBuffer.append("<link type=\"text/css\" charset=\"utf-8\" href=\"" + revcssPath + "/revision_ff.css" + "\" rel=\"stylesheet\" media=\"all\" />").append("\n");
  } else if (userAgent.indexOf("Chrome/") > -1) {
    htmlBuffer.append("<link type=\"text/css\" charset=\"utf-8\" href=\"" + revcssPath + "/revision_cr.css" + "\" rel=\"stylesheet\" media=\"all\" />").append("\n");
  } else if (userAgent.indexOf("Safari/") > -1) {
    htmlBuffer.append("<link type=\"text/css\" charset=\"utf-8\" href=\"" + revcssPath + "/revision_sf.css" + "\" rel=\"stylesheet\" media=\"all\" />").append("\n");
  }

  boolean _Compact = false;
%>

<%=htmlBuffer.toString()%>

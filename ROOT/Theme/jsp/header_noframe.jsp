<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import ="xgov.core.vo.XLoginVO"%>
<%@ page import ="xgov.core.env.XConfiguration"%>
<%
  // 세션 정보를 가져온다. LoginVO
  XLoginVO user = (XLoginVO)session.getAttribute("loginVO");
  // user.getCompanyCode()
  // user.getUserId()
  // cust_id = user.getVendorCode()
  // cust_code = user.getUserId()
%>

<link type="text/css" charset="utf-8" href="/Theme/css/custom_noframe.css" rel="stylesheet" media="all" />
<script type="text/javaScript" src="/Theme/js/script_noframe.js"></script>

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container top-line">
  </div>
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/"><img src="/Theme/images/menu/logo_top_noframe.gif" ></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul id="main-menu" class="nav navbar-nav">
        <!-- li><a href="/XG_Slick1.do?pcd=OM03020">협력사정보 </a></li>
        <li><a href="/XG_Slick1.do?pcd=OM03030">공지사항조회및관리</a></li>
        <li><a href="/XG_Slick1.do?pcd=OM03040">발주내역</a></li>
        <li><a href="/XG_Slick1.do?pcd=OM03050">거래명세서(월)</a></li>
        <li><a href="/XG_Slick1.do?pcd=OM03060">재고관리</a></li -->
      </ul>


      <ul class="nav navbar-nav navbar-right">
        <!-- li><a href="#">Link</a></li -->
        <li><a href="javascript:pf_logout()">로그아웃</a></li>
      </ul>

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<script type="text/javascript">
var _MyPgmList;
var _UserID = "<%=user.getVendorCode()%>";
var _CustCode = "<%=user.getUserId()%>";

$(function() {
  _MyPgmList = _X.XmlSelect("sm", "SM_AUTH_PGMCODE", "R_EGOVMAIN_01", new Array('<%= user.getCompanyCode()%>','<%= user.getUserId()%>', '%'), "json", "PGM_CODE");
  //pf_SetTopMenu();
  _Xn.SetTopMenu();

});

//노프레임 헤더 세팅
var pf_set_noframe_menu = function() {
  if(parent && parent!=this) {
    // alert("부모님 있음");
    $(".scm-left-bar").hide();
    $(".container").css( {"background": "none"} );
    $("#pageLayout").css( {"overflow": "auto"} );
  } else {
    xm_BodyResize = function() {}

    // alert("부모님 없음");
    $("#topsearch").hide();
    $("nav.navbar").show();
    $("html, body").css( {"overflow-y": "auto", "overflow-x": "hidden"} );
    $("#pageLayout, #div_cont").css( {"width": "100%", "height": "100%"} );
    $("#pageLayout").css( {"margin-top": "74px", "height": "auto"} );
  }
}

//로그아웃 처리(2015.05.10 KYY)
var pf_logout = function() {
  if(confirm("접속을 종료 하시겠습니까?")){
    document.location.href="/com/actionLogout.do";
  }
}

//노프레임 메뉴 그리기
var pf_SetTopMenu = function() {
  var thtml = "";

  for(var i=0; i<_MyPgmList.length; i++) {
    thtml = "";

    if(_MyPgmList[i].PGM_CODE.length>6) {

      thtml = "<li class=\"dropdown-mainmenu\">";
      thtml += "<a href=\"/XG_Slick1.do?pcd="+_MyPgmList[i].PGM_CODE+"\" >"+_MyPgmList[i].PGM_NAME+"</a>";
      thtml += "</li>";

      $("#main-menu").append( thtml );
    }
  }
}
</script>

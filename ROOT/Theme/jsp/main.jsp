<%@ page language="java" contentType="text/html; charset=utf-8" session="true" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<link rel="stylesheet" href="<c:url value='/Theme/css/main.css'/>" type="text/css">
<script type="text/javaScript" src="<c:url value='/Theme/js/main.js'/>"></script>

<style type="text/css">
	.style4 { color: #a863a8; font-weight: bold; }
	.style5 { color: #f26d7d; font-weight: bold; }
	.style6 { color: #8dc63f; font-weight: bold; }
	.style7 { color: #66cccc; font-weight: bold; }
	.style8 { color: #6699cc; font-weight: bold; }
	.style9 { color: #6666cc; font-weight: bold; }
	.style11 { color: #FFFFFF; font-weight: bold; }
	
	.mbox1 { width: 100%; height: 248px; }
	.mbox2 { width: 100%; height: 248px; margin-top: 1px; clear:both; }
	
	.mbox1 img, .mbox2 img { border: 0; }
	
	.mbox1 .sbox1 { width: 152px; height: 100%; background-color: #5f7791; float:left; }
	.mbox1 .sbox2 { width: 332px; height: 100%; background-image: url(/Theme/images/gts/201602_01.jpg); float:left; margin-left: 1px; }
	.mbox1 .sbox3 { width: 152px; height: 100%; float:left; margin-left: 1px; }
	.mbox1 .sbox4 { width: 216px; height: 100%; float:left; margin-left: 1px; }
	
	.mbox2 .sbox11 { width: 152px; height: 100%; background-color: #a4cf46; float:left; }
	.mbox2 .sbox12 { width: 152px; height: 100%; float:left; margin-left: 1px; }
	.mbox2 .sbox13 { width: 331px; height: 100%; float:left; margin-left: 1px; }
	.mbox2 .sbox14 { width: 216px; height: 100%; background-image: url(/Theme/images/gts/201602_02.jpg); float:left; margin-left: 1px; }
	
	.main-wrap { width: 862px; height: 492px; padding: 0; margin: 0; float: left; }
	.main-btn { width: 430px; height: 245px; border-right: 1px solid #e5e5e5; border-top: 1px solid #e5e5e5; float: left; background-color: #fff;  padding: 0; margin: 0; }
	.main-btn a, .main-btn a:visited { display: block; width: 430px; height: 245px; background-color:#fff; font-family: Malgun Gothic; font-size: 24px; font-weight: 700; color: #999; padding-top: 190px; padding-right: 30px; text-align: right; margin: 0; }
	.main-btn a:hover { background-color:#f2f2f2; height: 245px; font-family: Malgun Gothic; font-size: 24px; font-weight: 700; color: #666; text-decoration: none; }
	.main-right { width: 300px; height: 100%; background-color: #fff; border-top: 1px solid #e5e5e5; border-right: 1px solid #e5e5e5; float: left; }
	.main-right-menu { margin: 0; padding: 0; list-style: none; width: 300px; padding-top: 50px; }
	.main-right-menu li a, .main-right-menu li a:visited { display: block; font-family: Malgun Gothic; font-size: 15px; font-weight: 700; color: #999; text-align: center; padding: 2px 0; }
	.main-right-menu li a:hover { font-family: Malgun Gothic; font-size: 15px; font-weight: 700; color: #666; }
</style>

<div id="content">

  <div id="main">

		<table width="100%" border="0" cellpadding="0" cellspacing="0" >
			<!-- 컨텐츠시작  -->
			<tr>
				<td bgcolor="#fafafa" height="492">
					<div class="mbox1">
						<div class="sbox1"></div>
						<div class="sbox2"></div>
						<div class="sbox3"><a href="javascript:mytop._X.MDI_Open('GS01010', '대상자 정보관리', '/XG_Slick1.do?pcd=GS01010&');"><img src="/Theme/images/gts/2016_btn1.png"></a></div>
						<div class="sbox4"><a href="javascript:mytop._X.MDI_Open('GS02010', '직원관리', '/XG_Slick1.do?pcd=GS02010&');"><img src="/Theme/images/gts/2016_btn2.png"></a></div>
					</div>

					<div class="mbox2">
						<div class="sbox11"></div>
						<div class="sbox12"><a href="javascript:mytop._X.MDI_Open('GS01040', '월별일정/변경(방문)', '/XG_Slick1.do?pcd=GS01040&');"><img src="/Theme/images/gts/2016_btn3.png"></a></div>
						<div class="sbox13"><a href="javascript:mytop._X.MDI_Open('GS02020', '급여관리', '/XG_Slick1.do?pcd=GS02020&');"><img src="/Theme/images/gts/2016_btn4.jpg"></a></div>
						<div class="sbox14"></div>
					</div>
				</td>
			</tr>
			<!-- 컨텐츠끝  -->			
			<!-- Footer 시작  -->
			<tr height="98">
				<td bgcolor="#ffffff">
				<table width="100%" height="98" border="0" style="border-top: 1px dotted #ccc">
					<tr>
						<td  width="50"></td>
						<td  align="left" valign="middle" width="762" ><img src="/Theme/images/gts/footer_201804.png" /></td>
						<td  width="50"></td>
						<td  align="left" valign="top"></td>
					</tr>
				</table>
				</td>
			</tr>
			<!-- Footer 끝  -->
		</table>

  </div>
</div>

<%@ page contentType="text/html; charset=utf-8"%>

  <link rel="stylesheet" href="/Theme/3rd/bootsidemenu/BootSideMenu.css">
  <script src="/Theme/3rd/bootsidemenu/BootSideMenu.js"></script>

  <div class="x5-side-panel">
    
    <div class="sidebar-header">
      <small><%=user.getDeptName()%></small> <%=loginVO.getUserName()%>
      <div class="pull-right">
        <button type="button" class="btn btn-default btn-xs" id="btn-side-position">
          <i class="fa fa-hand-o-right"></i>
        </button>
      </div>
      <div class="clearfix"></div>
    </div><!-- sidebar-header -->

  </div><!-- x5-side-panel -->
<style>
  .sidebar-right > .toggler { left: -15px }
  .x5-side-panel .navbar-nav li a {
    padding-left: 8px;
  }
  ul.sub-menu-dl li a {
    padding-left: 25px !important;
  }
</style>
<script>
var _UserConfig;
var _SidepanelDuration = 400;
var _cIniPosition = "left";
var _cIniShow = "Y";
var _cIniDataStatus = "opened";

$(function() {
  // 초기 sidepanel 세팅
  if(pf_GetConfig()) {
    pf_changePosition(_UserConfig.sidepanel.position);
    pf_SetLayoutSide(_UserConfig.sidepanel.position);
    //pf_changePosition( _UserConfig.sidepanel.position );
    $('.x5-side-panel').css("top", $(".menu-header").height() );

    //if( _UserConfig.sidepanel.data_status=="opened" ) {
    //  $(this).find(".toggler").trigger("click");
    //}
  	
    // 첫번째 대메뉴 show
    setTimeout("$(\"#main-menu li:first a\").trigger(\"click\");", 200);
  }

  $("#btn-side-position").click(function() {
    pf_changePosition();
  });

  $(".toggler").click(function() {
  	var tStatus = $('.x5-side-panel').attr('data-status') == "opened" ? "closed" : "opened";
	  _UserConfig.sidepanel.data_status = tStatus;
	  pf_SetLayoutSide();
	  pf_SaveConfig();
  });

  // 2뎁스 이하 메뉴 선택시
  $('ul.sub-menu-dl li a').click(function() {
    _X.OpenSheet($(this).attr("data-pgmcode"));
  });
});

var pf_GetConfig = function() { 
  var rConfig = _X.XmlSelect("sm", "SM_AUTH_USER_CONFIG", "GET_CONFIG_LAYOUT", new Array(_CompanyCode,_UserID), "json");
  if(!rConfig[0].CONFIG_LAYOUT) {
    // 설정이 없으면 디폴트값 넣어주고 저장
    var ucIni = new Object();
    ucIni.sidepanel = new Object();
    ucIni.sidepanel.position = _cIniPosition;
    ucIni.sidepanel.show = _cIniShow;
    ucIni.sidepanel.data_status = _cIniDataStatus;
    _UserConfig = ucIni;

    pf_SaveConfig();
  } else {
    eval( "_UserConfig = " + rConfig[0].CONFIG_LAYOUT );

    if(!_UserConfig.sidepanel) {
      _UserConfig = new Object();
      _UserConfig.sidepanel = new Object();
    }

    if(!_UserConfig.sidepanel.position) _UserConfig.sidepanel.position = _cIniPosition;
    if(!_UserConfig.sidepanel.show) _UserConfig.sidepanel.show = _cIniShow;
    if(!_UserConfig.sidepanel.data_status) _UserConfig.sidepanel.data_status = _cIniDataStatus;
  }

  return true;
}

// user Config 저장
var pf_SaveConfig = function() {
  var strCfg = JSON.stringify( _UserConfig );

  // 전송결과값 UPDATE
  var param = new Array(_CompanyCode, _UserID, strCfg);
  var li_result = _X.ExecSql("sm","SM_AUTH_USER_CONFIG","SET_CONFIG_LAYOUT", param);

  return li_result;
}

// 최근메뉴 위치 변경
var pf_changePosition = function(aPos) {
  // 기본값은 aPos 없을때 Toggle 동작
  var fPos = _UserConfig.sidepanel.position;  // 출발위치
  var tPos = (fPos=="left"?"right":"left"); // 도착위치

  // aPos 있으면, aPos대로 동작
  if(aPos) {
    tPos = aPos;
    fPos = (aPos=="left"?"right":"left"); 
  }

  // right 로 가기..
  $("#btn-side-position").html( '<i class="fa fa-hand-o-'+fPos+'"></i>' );

  if(tPos=="right") {
    $('.x5-side-panel').removeClass("sidebar-"+fPos).css({ left: "" , right: "0px" });  
  } else {
    $('.x5-side-panel').removeClass("sidebar-"+fPos).css({ left: "0px" , right: "" });
  }
  
  $('.x5-side-panel').BootSideMenu({side: tPos});
  pf_SetLayoutSide(tPos);
  
  // 설정 저장 - Toggle 동작일 때만, 저장하기
  if(!aPos) {
    _UserConfig.sidepanel.position = tPos;
    pf_SaveConfig();
  }
}

// 사이드 탭이 열리거나 닫힌 후, 이벤트
var pf_SetLayoutSide = function(aPos) {
  if(!aPos) aPos = _UserConfig.sidepanel.position;
  var x5_side_width = 0;
  var fPos = (aPos=="left" ? "right" : "left");
  var fStatus = "opened";
  var tStatus = "closed";
  
  var sidePanelYn = true;
  var activeMenu  = $("#main-menu .active a")
  
  if ( !activeMenu[0] ) {
  	activeMenu = $("#main-menu li:first a");
  }
  if ( activeMenu[0] ) {
  	sidePanelYn = activeMenu.attr("data-sidepanel");
  }

	if ( sidePanelYn == "Y" ) {
		if( _UserConfig.sidepanel.data_status=="opened" ) {
	    fStatus = "closed";
	    tStatus = "opened";
	    x5_side_width = $(".x5-side-panel").width() + 3;
	  }
	}
	else {
		$(".toggler").hide();
	}
  
  $("#main-container").removeClass("x5-side-panel-p"+fPos+" x5-side-panel-"+fStatus).addClass("x5-side-panel-p"+aPos+" x5-side-panel-"+tStatus, _SidepanelDuration);
  $("#mdimenu").removeClass("x5-side-panel-p"+fPos+" x5-side-panel-"+fStatus).addClass("x5-side-panel-p"+aPos+" x5-side-panel-"+tStatus, _SidepanelDuration);
  if((aPos=="left"&&tStatus=="closed")||(aPos=="right"&&tStatus=="opened")) {
    $('.toggler span.glyphicon-chevron-left').hide();
    $('.toggler span.glyphicon-chevron-right').show();
  } else {
    $('.toggler span.glyphicon-chevron-left').show();
    $('.toggler span.glyphicon-chevron-right').hide();
  }
}
</script>
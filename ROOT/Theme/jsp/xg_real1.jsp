<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true"%>

<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
  <title>X-Internet eGov Framework</title>

  <script type="text/javascript">
    var _Compact = false;
    var _PGM_CODE = "<%=pcd%>";
    var _WIN_OPEN_DATE = "<%=xgov.core.util.XUtility.GetCurrentDate("yyyy-MM-dd HH:mm:ss")%>";    
  </script>
  
  <jsp:include page="/Mighty/include/COMM_JS.jsp"/>

  <link type="text/css" charset="utf-8" href="/Mighty/css/mighty-2.0.0.css"  rel="stylesheet" media="all" />

  <script type="text/javaScript" src="/APPS/com/js/Common.js"></script>
  <script type="text/javaScript" src="/APPS/com/js/<%=pcd.substring(0,2).toLowerCase()%>Common.js"></script>
  <script type="text/javaScript" src="/APPS/<%=pcd.substring(0,2).toLowerCase()%>/grid/<%=pcd%>.Grid.js"></script>
  <script type="text/javascript" src="/APPS/<%=pcd.substring(0,2).toLowerCase()%>/js/<%=pcd%>.js"></script>
</head>

<body>
  <div id="element_to_pop_up" ></div>
  <div id='formLoadImg' style='position:absolute; left:50%; top:40%; z-index:10000;'>
    <img src='/Theme/images/viewLoading.gif'/>
  </div>

  <div id="pageLayout">
    <div class="x5-wrap container-fluid">
      <jsp:include page="<%=subUrl%>"/>
    </div>
  </div>

  <script type="text/javascript">
    // 최근메뉴 설정
    _X.ExecProc("sm", "PROC_SM_AUTH_USER_RECENT", new Array("<%=user.getCompanyCode()%>", "<%=user.getUserId()%>", "<%=pcd%>"));

    //x_InitForm 호출위치 변경(2014.06.10 김양열)
    if(typeof(x_InitForm)!="undefined"){x_InitForm();}
    
    _X.Block();
    function xm_InitForm() {    
      //pageLayout.ResizeInfo = {init_width:1000, init_height:710};
      $(window).bind("resize", xm_BodyResize);
      xm_BodyResize(true);
      
      if(window._GridRequestCnt==0){
        _X.UnBlock();
      } else if(window._GridRequestCnt == window._Grids.length) {//UnBlock 추가 (2015.06.15 이상규)
        _X.UnBlock();
      }
      setTimeout("xm_InitForm_After()",0);
    }

    function xm_InitForm_After() {
      //input style 과 event attach
      xm_InputInitial();
      $(":input:visible:enabled:first").focus();
    }
    $(window).bind("load", xm_InitForm);

  </script>

</body>

</html>
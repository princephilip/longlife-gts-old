<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true"%>
<%@ page import ="xgov.core.vo.XLoginVO"%>
<%@ page import ="xgov.core.env.XConfiguration"%>

<%
  StringBuffer sbSys = new StringBuffer();

  sbSys.append("var _GlobalCache = new Array();");
  sbSys.append("var _SystemName = '세방TEC ERP';");
  sbSys.append("var _DBMS = 'Oracle';");
  sbSys.append("var _StringBuilder = '';");
  sbSys.append("var _MsgContent = '';");
  sbSys.append("var _Msg = '';");
  sbSys.append("var _SqlSecurityCheck = true;");
  sbSys.append("var _FooterHeight = 0;");
  sbSys.append("var _MyPgmList = null;");

  sbSys.append("var _rootDIR = '';");
  sbSys.append("var _rootFileUpload = '" + request.getContextPath() + "/FileUpload/';");
  sbSys.append("var _rootImageUpload = '" + request.getContextPath() + "/FileUpload/PHOTO/';");
  sbSys.append("var _CPATH ='" + request.getContextPath() + "';");

  sbSys.append("var _CompanyCode='" + user.getCompanyCode() + "';");
  sbSys.append("var _CompanyName='" + user.getCompanyName() + "';");
  sbSys.append("var _CompanyCodehm='01';");
  sbSys.append("var _UserID = '" + user.getUserId() + "';");
  sbSys.append("var _DeptCode = '" + user.getDeptCode() + "';");
  sbSys.append("var _UserName = '" + user.getUserName() + "';");
  sbSys.append("var _DeptName = '" + user.getDeptName() + "';");
  sbSys.append("var _IdStatus = '" + user.getuseYesNo() + "';");
  sbSys.append("var _StdCode = '" + user.getstdCode() + "';");
  sbSys.append("var _Email = '" + user.getEMail() + "';");
  sbSys.append("var _MobileNo = '" + user.getMobileNo() + "';");
  sbSys.append("var _LoginTag = '" + user.getLoginLockYesNo() + "';");
  sbSys.append("var _ClientIP = '" + request.getRemoteAddr()  + "';");
  sbSys.append("var _CustCode = '" + user.getVendorCode()  + "';");
  sbSys.append("var _CustName = '" + user.getVendorName()  + "';");
  sbSys.append("var _UserTag = '" + user.getUserTag() + "';");
  sbSys.append("var _PasswordUpdate = '" + user.getPasswordUpdate() + "';");
  sbSys.append("var _PasswordExpire = " + user.getPasswordExpire() + ";");

  //울트라 추가(2015.08.24 KYY)
  sbSys.append("var _AuthCode = '';");
  sbSys.append("var _UserRole = '';");
  sbSys.append("var _DataRole = '';");
  sbSys.append("var _MenuAuth = '';");
  sbSys.append("var _GradeCode = '';");
  sbSys.append("var _GradeName = '';");

  //공사용 마지막 현장
  sbSys.append("var _csProjCode = '';");

  String sysinfo = new StringBuffer( xgov.core.util.XBase64Coder.encodeString(sbSys.toString()) ).reverse().toString();

%>

<!DOCTYPE html>
<html lang="ko">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
  <meta name="viewport" content="width=device-width, user-scalable=no">

  <title>X-Internet eGov Framework</title>

  <script type="text/javascript">
    var _Compact = false;
    var _PGM_CODE = "<%=pcd%>";
    var _WIN_OPEN_DATE = "<%=xgov.core.util.XUtility.GetCurrentDate("yyyy-MM-dd HH:mm:ss")%>";

    // Mighty-win Variable
    var _Grids = new Array();
    var _CurGrid = null;
  </script>

  <jsp:include page="/Mighty/include/COMM_JS.jsp"/><!-- 추가(2018.08.10 YSKIM) -->

  <link rel="stylesheet" href="/Mighty/3rd/jQuery/jquery-ui-1.11.4/jquery-ui.css">
  <link rel="stylesheet" href="/Mighty/3rd/font-awesome/css/all.css">
  <link rel="stylesheet" href="/Theme/3rd/themify/themify-icons.css">
  <link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="/Theme/3rd/bootstrap-datepicker/css/bootstrap-datepicker.css">
  <link rel="stylesheet" href="/Theme/3rd/handsontable/handsontable.full.min.css">
  <link rel="stylesheet" href="/Theme/css/paper-dashboard.css">

  <link type="text/css" href="https://fonts.googleapis.com/css?family=Muli:400,300" rel="stylesheet">

  <link rel="stylesheet" href="/Theme/css/custom_partner.css?v=<%=xgov.core.util.XUtility.GetCurrentDate("yyyyMMddHHmmss")%>">

  <style>
    .content { padding: 6px 0 0 6px; }
    .wrapper { background-color: #f4f3ef; }
  </style>

  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!--   Core JS Files   -->
  <script type="text/javascript" src="/Mighty/3rd/jQuery/js/jquery-1.11.3.min.js"></script>
  <script type="text/javaScript" src="/Mighty/3rd/jQuery/jquery-ui-1.11.4/jquery-ui.min.js"></script>
  <script type="text/javaScript" src="/Mighty/3rd/jQuery/js/jquery.form.min.js"></script>
  <script type="text/javaScript" src="/Mighty/3rd/jQuery/js/jquery.blockUI-2.7.0.js"></script><!-- 추가(2018.08.07 YSKIM) -->

  <script type="text/javascript" src="/Mighty/3rd/bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javaScript" src="/Mighty/3rd/bootstrap/js/bootstrap-notify.min.js"></script><!-- 추가(2016.12.27 KYY) -->
  <script type="text/javascript" src="/Theme/3rd/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="/Theme/3rd/bootstrap-datepicker/locales/bootstrap-datepicker.ko.min.js"></script>
  <script type="text/javascript" src="/Theme/3rd/handsontable/handsontable.full.min.js"></script>

  <script type="text/javascript" src="/Mighty/js/mighty-2.0.0.js?v=20180511113000"></script>
  <script type="text/javascript" src="/Mighty/js/mighty-comm-1.2.8.js?v=201810180000"></script>
  <script type="text/javascript" src="/Mighty/js/mighty-dao-1.2.6.js?v=201810180000"></script>
  <script type="text/javascript" src="/Mighty/js/mighty-ui-1.2.9.js?v=201810180000"></script>
  <script type="text/javascript" src="/Mighty/js/mighty-dao-handsontable-0.0.1.js?v=201810180000"></script>
  <script type="text/javaScript" src="/Mighty/js/mighty-ui-bootstrap-1.0.3.js?v=201810180000"></script><!-- 추가(2018.08.07 YSKIM) -->

  <script type="text/javascript" src="/Mighty/3rd/crypto/base64.js"></script>
  <script type="text/javascript" src="/Mighty/js/mighty-print-ireport-1.0.0.js?v=201810180000"></script>

  <script type="text/javascript" src="/Theme/3rd/vuejs/vue.js"></script>
  <script type="text/javascript" src="/Theme/js/script_partner_comm.js"></script>

  <script type="text/javascript">
    var _XU = new Object();
    var _XV = new Object();
  </script>


    <script type="text/javascript" src="/Theme/js/script_partner.js"></script>

    <script type="text/javascript">
      var ifo = "<%=sysinfo%>";
      var pops = "";

      var vMenus;

      $(document).ready(function () {
        //if (_LoginTag=="Y") pf_changePwd();

        <% if(session.getAttribute("strongpwd")!=null && ((String)session.getAttribute("strongpwd")).equals("N") && !user.getUserId().toLowerCase().equals("admin")) { %>
          //pf_changePwd(true);
        <% } %>
        if(_PasswordExpire>0) {
          //pf_changePwd(true);
        }
        if(_PasswordExpire==-5 || _PasswordExpire==-10 ) {
        //  alert("패스워드 유효기간이 " + (_PasswordExpire)*-1 + "일 남았습니다.\n패스워드를 변경해 주시기 바랍니다.");
        }

        //InitPage();

        //65분에 한번씩 Session 체크(2015.06.02 KYY) - 웹로직 세션타임아웃 60분으로 설정
        sessionTimer = setInterval(sessionAliveCheck, 65 * 60 * 1000);

        //_MyPgmList = _X.XmlSelect("sm", "SM_AUTH_PGMCODE", "R_EGOVMAIN_01", new Array(_CompanyCode,_UserID, '%'), "json", "PGM_CODE");
        _MyPgmList = _X.XmlSelect("sm", "SM_AUTH_PGMCODE", "R_EGOVMAIN_01", new Array(_CompanyCode,_UserID, 'PT'), "json", "PGM_CODE");
        //실행권한 설정(2017.03.21 KYY)
        _MyActAuth = _X.XmlSelect("sm", "SM_AUTH_PGMCODE", "MY_AUTH_ACT", new Array(_CompanyCode,_UserID), "array", "AUTH_ACT").join();

        vMenus = new Vue({
          el: '#v-menu',
          data: {
            menus: _MyPgmList
          }
        });

        vMenus.$nextTick(function () {
          lbd.initRightMenu();
        });

        //console.log(_MyPgmList.length, _MyActAuth.length)
        //_X.SetTopMenuD2();
        //_X.SetTopMenuTS();

        // init_pgmsearch();
        if (typeof(_XU.Initial)!="undefined") {

          // YSKIM
          // if(_UserTag!="9") {
          //   $("#wrap-cond").removeClass("hide");
          //   $(".sidebar").addClass("hide");
          //   $(".main-panel").css("width", "100%");
          // }

          _XU.Initial();
        }
      });

      eval(_X.SD(ifo));
      //eval(_X.SD(pops));

      //세션 활성화여부 체크
      var sessionAliveCheck = function() {
        _X.ChkSession();
      }

      //로그아웃 처리(2015.05.10 KYY)
      var pf_logout = function() {
        if(confirm("접속을 종료 하시겠습니까?")){
          document.location.href="/com/actionLogout.do";
        }
      }

      //비밀번호 변경 창..
      var pf_changePwd = function(mustchange) {
        //반드시 바꾸어야 하는 경우
        if(mustchange==null)
          mustchange = false;
        var url = "<c:url value='/user/EgovUserPasswordUpdtView.do'/>";
        var modal = "<div id='findmodal'><iframe id='if_findmodal' src='" + url + "' class='iframe_popup' style='width:430px; height:300px;' frameborder=0 marginwidh=0 marginheight=0></iframe></div>";
        if(mustchange) {
          var title = "비밀번호변경";
          $(modal).dialog({title: title, show:false, width:455, height:420, modal:true, resizable:false,
                closeOnEscape: false,
                buttons: {
                  "확인": function() { $("#if_findmodal").get(0).contentWindow.pf_UpdatePwd(); }
                },
                open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); }  ,
                close:function(){try {$(this).remove();} catch(e) {}}
          });
        } else {
          $(modal).dialog({title:'비밀번호변경', show:false, width:455, height:420, modal:true, resizable:false, //325
                closeOnEscape: true,
                buttons: {
                  "확인": function() { $("#if_findmodal").get(0).contentWindow.pf_UpdatePwd(); },
                  "취소": function() { $("#findmodal").dialog("close"); }
                },
                close:function(){try {$(this).remove();} catch(e) {}}
          });
        }
      }
    </script>


  <%
    String grid_schima = "";
    try {
      String gridparams  = pcd.substring(0,2) + _Col_Separate + pcd;
      grid_schima = xgov.core.dao.XDAO.XmlSelect3(request,"array", "com", "X5", "GRID_SCRIPT", gridparams, "all", _Row_Separate,_Col_Separate);
    } catch (Exception e) {
      e.printStackTrace();
    }
  %>
  <script type="text/javaScript">
    <%=grid_schima%>
  </script>
  <script type="text/javascript" src="/APPS/<%=pcd.substring(0,2).toLowerCase()%>/js/<%=pcd%>.js?v=<%=xgov.core.util.XUtility.GetCurrentDate("yyyyMMddHHmmss")%>"></script>

</head>
<body>

  <div class="wrapper">

    <div class="sidebar" data-background-color="black" data-active-color="info">

      <div class="sidebar-wrapper">
        <div class="logo" style="width:100%;">
          <a href="/" class="simple-text"><img src="/Theme/images/menu/logo_top_partner.gif" class="img-responsive" style='width: 149px;height:48px;margin:0px 0px 0px 30px'></a>
        </div>

        <div class="sidebar-header" align="center" style="color:#68B3C8;font-weight:700;margin-top:5px;">
          <div class="clearfix"><small><%=user.getUserId()%></small></div>
          <div class="clearfix"><%=user.getUserName()%></div>
        </div><!-- sidebar-header -->

        <ul id="v-menu" class="nav">
          <template v-for="(menu, idx) in menus">
            <template v-if="menu.MENU_LEVEL > 1">
              <li v-bind:class="'level'+menu.MENU_LEVEL+' '+(menu.PGM_CODE=='<%=pcd%>' ? 'active' : '')">
                <a v-bind:href="(menu.MENU_LEVEL==2 ? '#' : '/XG_page.do?pcd='+menu.PGM_CODE)">
                  <i v-if="menu.MENU_LEVEL==2" v-bind:class="menu.CONN_URL"></i>
                  <i v-if="menu.MENU_LEVEL==3" class="ti-angle-right"></i>
                  <p>{{menu.PGM_NAME}}</p>
                </a>
              </li>
            </template>
          </template>
        </ul>
      </div>
    </div>

    <div class="main-panel">

      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar bar1"></span>
              <span class="icon-bar bar2"></span>
              <span class="icon-bar bar3"></span>
            </button>
            <a class="navbar-brand" href="#"></a>
          </div>
          <div class="collapse navbar-collapse">
            <ul id="v-menu2" class="nav navbar-nav navbar-right">
              <!-- li>
                <a href="#" data-toggle="modal" data-target="#infoModal">
                  <i class="ti-settings"></i>
                  <p>정보수정</p>
                </a>
              </li -->
              <li>
                <a href="/partner/down_manual.jsp" target="_blank">
                  <i class="ti-help-alt"></i>
                  <p>도움말 다운로드</p>
                </a>
              </li>
              <li>
                <a href="#" data-toggle="modal" data-target="#pwModal">
                  <i class="ti-key"></i>
                  <p>비밀번호 변경</p>
                </a>
              </li>
              <li>
                <a href="javascript:pf_logout()">
                  <i class="ti-power-off"></i>
                  <p>로그아웃</p>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <% if( pcd.equals("PT01010") ) { %>
        <div class="content">
          <div class="container-fluid">
            <jsp:include page="<%=subUrl%>"/>
          </div>
        </div>
      <% } else { %>
        <div class="pcontent" style="height:calc(100% - 75px)">
          <iframe id="if_main" src="/XG_Slick1.do?pcd=<%=pcd%>" scrolling="yes" width="100%" height="100%" align="top" frameborder="0" marginheight="0" marginwidth="0"></iframe>
        </div>
      <% } %>

    </div>
  </div>

<!-- Modal -->
<div class="modal fade" id="ptModal" tabindex="-1" role="dialog" aria-labelledby="ptModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="ptModalLabel">{{modal_title}}</h4>
      </div>
      <div id="ptModalBody" class="modal-body">
        <div id="dg_detail" class="grid-div">
        </div>
        <div id="modal_detail">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">닫 기</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="pwModal" tabindex="-1" role="dialog" aria-labelledby="pwModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="pwModalLabel"><i class="ti-key"></i> 비밀번호 변경</h4>
      </div>
      <div id="pwModalBody" class="modal-body">
        <div class="form-horizontal">
          <div class="form-group form-group-sm">
            <label for="cur_pass" class="col-sm-3 control-label">현재 비밀번호</label>
            <div class="col-sm-9">
              <input type="password" class="form-control border-input input-sm" id="cur_pass" placeholder="현재 비밀번호">
            </div>
          </div>
          <div class="form-group form-group-sm">
            <label for="pass1" class="col-sm-3 control-label">새 비밀번호</label>
            <div class="col-sm-9">
              <input type="password" class="form-control border-input input-sm" id="pass1" placeholder="새 비밀번호">
            </div>
          </div>
          <div class="form-group form-group-sm">
            <label for="pass2" class="col-sm-3 control-label">새 비밀번호 확인</label>
            <div class="col-sm-9">
              <input type="password" class="form-control border-input input-sm" id="pass2" placeholder="새 비밀번호 확인">
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning btn-sm" id="btn-password">수 정</button>
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">닫 기</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="infoModalLabel"><i class="ti-settings"></i> 정보수정</h4>
      </div>
      <div id="infoModalBody" class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">닫 기</button>
      </div>
    </div>
  </div>
</div>

</body>
</html>


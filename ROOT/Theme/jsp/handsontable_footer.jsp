<table>
  <tr>
    <td class="visible_col" style="width: 50px">&nbsp;</td>
    <td v-for="(dt, idx) in data" v-if="dt.visible" v-bind:id="(dt.footerExpr=='SUM'?'FT-'+dt.fieldName:'')" v-bind:class="'visible_col '+(dt.footerExpr=='SUM'?'text-right':(dt.footerExpr?'text-center':''))" v-bind:style="'width: '+dt.width+'px'">{{(dt.footerExpr=="SUM"?"0":dt.footerExpr)}}</td>
    <td>&nbsp;</td>
  </tr>
</table>
<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<%@ page import ="xgov.core.vo.XLoginVO" %>
<%@ page import ="org.springframework.util.StopWatch" %>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%
 /**
  * @JSP Name : XG010.jsp
  * @Description : 기본 페이지 구조
  * @Modification Information
  * 
  *   수정일     수정자         수정내용
  *  ----------  --------       ---------------------------
  *  2012.12.01  김양열         최초 생성
  *  2015.06.08  SKLEE          SLICKGRID 버전으로 변경
  *
  * author (주)엑스인터넷정보 
  * Copyright (C) 2015 by X-Internet Info.  All right reserved.
  */
%>

<%
	String sys = request.getParameter("sys").toLowerCase();
	String pcd = request.getParameter("pcd");
	String params = request.getParameter("params");
	
	XLoginVO user = (XLoginVO)session.getAttribute("loginVO");
	if(session.getAttribute("userId")==null) {
		 session.setAttribute("userId",user.getUserId());
	}
	if(session.getAttribute("deptCode")==null) {
		 session.setAttribute("deptCode",user.getDeptCode());
	}
	if(session.getAttribute("companyCode")==null) {
		 session.setAttribute("companyCode",user.getCompanyCode());
	}
	if(session.getAttribute("userTag")==null) {
		 session.setAttribute("userTag",user.getUserTag());
	}

	//프로그램 실행권한 체크(2015.04.20 KYY)
	String _Row_Separate = "˛";
	String _Col_Separate = "¸";

	String subUrl = "/APPS/" + sys + "/jsp/" + pcd + ".jsp?params="+params;
%>

<!DOCTYPE>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
  <title>X-Internet eGov Framework</title>

  <script type="text/javascript">
    var _Compact = false;
    var _SYS_ID = "<%=sys%>";
    var _PGM_CODE = "<%=pcd%>";
    var _WIN_OPEN_DATE = "<%=xgov.core.util.XUtility.GetCurrentDate("yyyy-MM-dd HH:mm:ss")%>";    
  </script>
  
  <!-- INCLUDE 변경 (2015.06.08 이상규) -->
  <jsp:include page="/Mighty/include/COMM_JS.jsp"/>
  
  <script type="text/javaScript" src="/APPS/com/js/Common.js"></script>
  <script type="text/javaScript" src="/APPS/<%=sys%>/grid/<%=pcd%>.Grid.js"></script>
  <script type="text/javaScript" src="/Theme/js/Real2Slick.js"></script>

</head>

<body style="verticalAlign: top; text-align: left; display: inline;" bgcolor="#ffffff" topmargin="10px">
  <div id="pageLayout">
    <jsp:include page="<%=subUrl%>"/>
  </div>

  <script type="text/javascript">
  	
		$( document ).ready(function() {
			$(".grid_div").each(function(idx,val) {
				$(val).children().each(function(cidx, cval) {
					var grid_id = cval.id;
					if(grid_id.indexOf("dg")>=0) {
						try {
							alert(grid_id);
							var grid_val;
							eval("grid_val = columns_" + grid_id + ";");									
							if(grid_val!=null && typeof(grid_val)!="undefined") {
								Slick2DB(_SYS_ID, _PGM_CODE, grid_id, grid_val);								
							}	
						} catch(e) {alert(e)}
					}
				});	
			});
		});




		function Slick2DB(sys_id, pgm_code, grid_id, grid_val) {

			var sql = "DELETE FROM SM_DEV_GRID_COLS WHERE SYS_ID='"+ sys_id.toUpperCase() + "' AND PGM_CODE='" + pgm_code + "' AND GRID_ID='" + grid_id + "'";
			_X.EP("sm", "PROC_XM_ES", new Array(sql));
			var dgSql = "BEGIN ";
			for(var i=0; i<grid_val.length; i++) {
				dgSql += "INSERT INTO SM_DEV_GRID_COLS "
						+ "(SYS_ID, PGM_CODE, GRID_ID, SEQ, FIELD_SEQ, FIELDNAME, TABLE_NAME, WIDTH, MUST_INPUT, VISIBLE, READONLY, EDITABLE, SORTABLE, HEADER, HEADER_TEXT, "
						+ " STYLES, EDITOR, LOOKUPDISPLAY, RENDERER, BUTTON, ALWAYSSHOWBUTTON, HEADERTOOLTIP, MERGE, MAXLENGTH, FOOTEREXPR, FOOTERALIGN, RESIZABLE, UPDATE_TYPE, UPDATABLE, UPDATE_ISKEY, "
						+ " ROW_INPUT_DATE, ROW_INPUT_EMP_NO, ROW_INPUT_IP, ROW_UPDATE_DATE, ROW_UPDATE_EMP_NO, ROW_UPDATE_IP, FREEFORM_RATE)"
						+ " values ("
						+ "'" + sys_id.toUpperCase() + "', "
						+ "'" + pgm_code + "', "
						+ "'" + grid_id + "', "
						+ "" + (i+1) + ", "
						+ "" + ((i+1)*10) + ", "
						+ "'" + grid_val[i].fieldName + "', "
						+ "'" + "" + "', "
						+ "" + grid_val[i].width + ", "
						+ "'" + (typeof(grid_val[i].must_input   )=="undefined" ? "N" : grid_val[i].must_input    ) + "', "
						+ "'" + tf2yn(grid_val[i].visible) + "', "
						+ "'" + tf2yn(grid_val[i].readonly) + "', "
						+ "'" + tf2yn(grid_val[i].editable) + "', "
						+ "'" + tf2yn(grid_val[i].sortable) + "', "
						+ "'" + "" + "', "
						+ "'" + (typeof(grid_val[i].header.text)=="undefined" || grid_val[i].header.text=="" ? grid_val[i].fieldName : grid_val[i].header.text) + "', "
						
						+ "'" + (typeof(grid_val[i].styles   )=="undefined" ? "_Styles_text" : get_stylesname(grid_val[i].styles)) + "', "
						+ "'" + (typeof(grid_val[i].editor   )=="undefined" ? "_Editor_text" : get_editorname(grid_val[i].editor)) + "', "
						+ "'" + (typeof(grid_val[i].lookupDisplay   )=="undefined" ? "" : tf2yn(grid_val[i].lookupDisplay)    ) + "', "
						+ "'" + (typeof(grid_val[i].renderer        )=="undefined" ? "" : grid_val[i].renderer         ) + "', "
						+ "'" + (typeof(grid_val[i].button          )=="undefined" ? "" : grid_val[i].button           ) + "', "
						+ "'" + (typeof(grid_val[i].alwaysShowButton)=="undefined" ? "" : tf2yn(grid_val[i].alwaysShowButton) ) + "', "
						+ "'" + (typeof(grid_val[i].headerToolTip   )=="undefined" ? "" : grid_val[i].headerToolTip    ) + "', "
						+ "'" + (typeof(grid_val[i].merge           )=="undefined" ? "" : grid_val[i].merge            ) + "', "
						+ "'" + (typeof(grid_val[i].maxLength       )=="undefined" ? "" : grid_val[i].maxLength        ) + "', "
						+ "'" + (typeof(grid_val[i].footer      )=="undefined" ? "" : grid_val[i].footer.expression.toUpperCase()) + "', "
						+ "'" + (typeof(grid_val[i].footerAlign     )=="undefined" ? "" : grid_val[i].footerAlign      ) + "', "
						+ "'" + (typeof(grid_val[i].resizable       )=="undefined" ? "" : tf2yn(grid_val[i].resizable) ) + "', "
						
						+ "'" + get_updateType(grid_val[i].styles) + "', "
						+ "'" + "Y" + "', "
						+ "'" + "N" + "', "

						+ "sysdate,'ADMIN','127.0.0.1',null,null,null,null);"
						;
				if(i==40 || i==80 || i==120 || i==160 || i==200) {
					dgSql += " END;";
					_X.EP("sm", "PROC_XM_ES", new Array(dgSql));
					dgSql = "BEGIN ";
				}
			}	
			dgSql += " END;";
			_X.EP("sm", "PROC_XM_ES", new Array(dgSql));
			_X.EP("sm", "SP_SM_DEV_GRID_FIX", new Array(sys_id.toUpperCase(),pgm_code,grid_id));

		}
		

  </script>

</body>

</html>
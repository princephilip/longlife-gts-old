<%@ page language="java" contentType="text/html; charset=utf-8" session="true" pageEncoding="utf-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ page import ="xgov.core.vo.XLoginVO"%>
<%@ page import ="xgov.core.env.XConfiguration"%>
<%@ page import ="egovframework.rte.com.service.EgovLoginService"%>
<%@ page import ="org.springframework.context.ApplicationContext"%>
<%@ page import ="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>

<%


%>


<%
  // 세션 정보를 가져온다. LoginVO
  //EgovLoginService loginService;

  ApplicationContext act = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
  EgovLoginService loginService = (EgovLoginService) act.getBean("loginService");

  XLoginVO loginVO = (XLoginVO)session.getAttribute("loginVO");

  String userId = (String)request.getParameter("i");
  //String userPassword = "[Auto.Login.STEP]";
  String userPassword = (String)request.getParameter("p");
  String companyCode = "01";

  if(loginVO == null) {
	loginVO = new XLoginVO();
  }
  loginVO.setUserId(userId);
  loginVO.setUserPassword(userPassword);
  loginVO.setCompanyCode(companyCode);
  loginVO.setHostName(request.getServerName());
  loginVO.setEncPassword(userPassword);

  XLoginVO resultVO = loginService.actionLogin(loginVO);
  String chkPassword = resultVO.getEncPassword();

  if (resultVO != null && resultVO.getUserId() != null && !resultVO.getUserId().equals("") && resultVO.getUserName() != null && !resultVO.getUserName().equals("")) {
    //사용여부 체크로직  추가(2015.05.10 KYY)

    resultVO.setIpAddr(egovframework.rte.com.util.service.EgovClntInfo.getClntIP(request));
    resultVO.setCompanyCode(companyCode);

    String sePw = new StringBuffer( xgov.core.util.XBase64Coder.encodeString(userPassword) ).reverse().toString();
    resultVO.setSePw(sePw);

    request.getSession().setAttribute("loginVO", resultVO);
    request.getSession().setAttribute("strongpwd", sePw);

    loginService.updateLogin(resultVO);
  }

  StringBuffer sbSys = new StringBuffer();

  sbSys.append("var _GlobalCache = new Array();");
  sbSys.append("var _SystemName = '세방TEC ERP';");
  sbSys.append("var _DBMS = 'Oracle';");
  sbSys.append("var _StringBuilder = '';");
  sbSys.append("var _MsgContent = '';");
  sbSys.append("var _Msg = '';");
  sbSys.append("var _SqlSecurityCheck = true;");
  sbSys.append("var _FooterHeight = 0;");
  sbSys.append("var _MyPgmList = null;");

  sbSys.append("var _rootDIR = '';");
  sbSys.append("var _rootFileUpload = '" + request.getContextPath() + "/FileUpload/';");
  sbSys.append("var _rootImageUpload = '" + request.getContextPath() + "/FileUpload/PHOTO/';");
  sbSys.append("var _CPATH ='" + request.getContextPath() + "';");

  sbSys.append("var _CompanyCode='" + resultVO.getCompanyCode() + "';");
  sbSys.append("var _CompanyName='" + resultVO.getCompanyName() + "';");
  sbSys.append("var _CompanyCodehm='100';");
  sbSys.append("var _UserID = '" + resultVO.getUserId() + "';");
  sbSys.append("var _LoginID = '" + resultVO.getLoginId() + "';");
  sbSys.append("var _DeptCode = '" + resultVO.getDeptCode() + "';");
  sbSys.append("var _UserName = '" + resultVO.getUserName() + "';");
  sbSys.append("var _DeptName = '" + resultVO.getDeptName() + "';");
  sbSys.append("var _IdStatus = '" + resultVO.getuseYesNo() + "';");
  sbSys.append("var _StdCode = '" + resultVO.getstdCode() + "';");
  sbSys.append("var _Email = '" + resultVO.getEMail() + "';");
  sbSys.append("var _MobileNo = '" + resultVO.getMobileNo() + "';");
  sbSys.append("var _LoginTag = '" + resultVO.getLoginLockYesNo() + "';");
  sbSys.append("var _ClientIP = '" + request.getRemoteAddr()  + "';");
  sbSys.append("var _CustCode = '" + resultVO.getVendorCode()  + "';");
  sbSys.append("var _CustName = '" + resultVO.getVendorName()  + "';");
  sbSys.append("var _UserTag = '" + resultVO.getUserTag() + "';");
  sbSys.append("var _PasswordUpdate = '" + resultVO.getPasswordUpdate() + "';");
  sbSys.append("var _PasswordExpire = " + resultVO.getPasswordExpire() + ";");
  //sbSys.append("var _SePW = '" + resultVO.getSePw() + "';");

  sbSys.append("var _SePW = '" + (String)session.getAttribute("sePw") + "';");

  //울트라 추가(2015.08.24 KYY)
  sbSys.append("var _AuthCode = '';");
  sbSys.append("var _UserRole = '';");
  sbSys.append("var _DataRole = '';");
  sbSys.append("var _MenuAuth = '';");
  sbSys.append("var _GradeCode = '"+ resultVO.getGradeCode() +"';");
  sbSys.append("var _GradeName = '';");

  //공사용 마지막 현장
  sbSys.append("var _csProjCode = '';");

  String sysinfo = new StringBuffer( xgov.core.util.XBase64Coder.encodeString(sbSys.toString()) ).reverse().toString();

  String[] pops = xgov.core.dao.XDAO.XmlSelect(request, "array", "com", "POPUP", "popup", resultVO.getCompanyCode(), "all", "˛", "¸").split("˛");
  String popids = "";

  StringBuffer sbPops = new StringBuffer();

  for(int i=0; i<pops.length; i++) {
    String[] popData = pops[i].split("¸");
    popids += (i==0 ? "" : "|") + popData[0];
  }
  sbPops.append("var _PopIds = '" + popids + "';");
  String popinfo = new StringBuffer( xgov.core.util.XBase64Coder.encodeString(sbPops.toString()) ).reverse().toString();

  response.setHeader("X-Frame-Options", "ALLOW-FROM http://gw.sebangtec.com");
%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=EDGE" />

<script type="text/javascript" src="/Mighty/egov/js/pwm/prototype-1.6.0.3.js"></script>
<title>세방TEC ERP</title>

<script type="text/javascript">
  var _idleTime = 0;
</script>

<jsp:include page="/Mighty/include/COMM_JS.jsp"/>


<!-- main 화면에만 bs3 theme 적용 시작 -->
<link rel="stylesheet" href="/Mighty/3rd/font-awesome/css/all.css">
<link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap-theme.min.css">

<link rel="stylesheet" href="/Mighty/css/mighty-bootstrap-1.0.2.css" />

<script type="text/javaScript" src="/Mighty/3rd/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javaScript" src="/Mighty/3rd/bootstrap/js/bootstrap-notify.min.js"></script><!-- 추가(2016.12.27 KYY) -->

<script type="text/javaScript" src="/Mighty/js/mighty-win-bootstrap-1.0.0.js"></script>
<script type="text/javaScript" src="/Mighty/js/mighty-mdi-bootstrap-1.0.1.js"></script>
<script type="text/javaScript" src="/Mighty/js/mighty-ui-bootstrap-1.0.3.js"></script>
<!-- main 화면에만 bs3 theme 적용 끝 -->

<link rel="stylesheet" href="/Theme/css/custom.css" />

<script type="text/javascript" src="/Mighty/3rd/crypto/core.js"></script>
<script type="text/javascript" src="/Mighty/3rd/crypto/base64.js"></script>

<script type="text/javascript" src="/Mighty/3rd/typeahead/handlebars.js"></script>
<link rel="stylesheet" href="/Mighty/3rd/typeahead/typeahead.css" />
<script type="text/javascript" src="/Mighty/3rd/typeahead/bloodhound.js"></script>
<script type="text/javascript" src="/Mighty/3rd/typeahead/typeahead.bundle.min.js"></script>

<%
  //xgov.core.vo.XLoginVO loginVO = (xgov.core.vo.XLoginVO)egovframework.rte.com.util.service.EgovUserDetailsHelper.getAuthenticatedUser();
%>

<script type="text/javascript">
  var ifo = "<%=sysinfo%>";
  var pops = "<%=popinfo%>";

  var sessionTimer;
    //_MyPgmList = _X.XmlSelect("sm", "SM_AUTH_PGMCODE", "R_EGOVMAIN_01", new Array(_CompanyCode,_UserID, '%'), "json", "PGM_CODE");
  var _MyActAuth;


  // 신사고 하이테크 OpenSheet 할 때, Key Params 전달용 변수 추가 2016 07 27 - JH
  var pgmParams ;

    $(document).ready(function () {
      //if (_LoginTag=="Y") pf_changePwd();

      <% if(session.getAttribute("strongpwd")!=null && ((String)session.getAttribute("strongpwd")).equals("N") && !resultVO.getUserId().toLowerCase().equals("admin")) { %>
        //pf_changePwd(true);
      <% } %>
      if(_PasswordExpire>0) {
        //pf_changePwd(true);
      }
      if(_PasswordExpire==-5 || _PasswordExpire==-10 ) {
      //  alert("패스워드 유효기간이 " + (_PasswordExpire)*-1 + "일 남았습니다.\n패스워드를 변경해 주시기 바랍니다.");
      }

      InitPage();

      //65분에 한번씩 Session 체크(2015.06.02 KYY) - 웹로직 세션타임아웃 60분으로 설정
      sessionTimer = setInterval(sessionAliveCheck, 65 * 60 * 1000);

      //_MyPgmList = _X.XmlSelect("sm", "SM_AUTH_PGMCODE", "R_EGOVMAIN_01", new Array(_CompanyCode,_UserID, '%'), "json", "PGM_CODE");
      _MyPgmList = _X.XmlSelect("sm", "SM_AUTH_PGMCODE", "R_EGOVMAIN_01", new Array(_CompanyCode,_UserID, '%'), "json", "PGM_CODE");
      //실행권한 설정(2017.03.21 KYY)
      _MyActAuth = _X.XmlSelect("sm", "SM_AUTH_PGMCODE", "MY_AUTH_ACT", new Array(_CompanyCode,_UserID), "array", "AUTH_ACT").join();

      _X.SetTopMenuD2();
      _X.SetTopMenuTS();

      // init_pgmsearch();
    });

    eval(_X.SD(ifo));
    eval(_X.SD(pops));

		function MainMdiResize() {
      //추가(2016.12.18 KYY)
      var newWidth  = _X.MDI_Width();
  		var newHeight = _X.MDI_Height();

      mdi_main.style.width  = newWidth + "px";
      mdi_main.style.height = newHeight + "px";
		}

    //화면 리사이즈 제어
    $(window).bind('resize', function () {
      // 모바일일때, resize 안하기 - mobile keyboard 사라짐 문제
      if (!navigator.userAgent.match(/iPad/i)&&!navigator.userAgent.match(/Android|webOS|iPhone|iPod|Blackberry/i) ) {
        _X.MDI_Resize();
				MainMdiResize();
      }
    });

    $( window ).unload(function() {
      _X.MDI_CloseAll();
      _X.purge(window);
      //로그아웃 방식 변경으로 불필요한 로직 (2015.05.10 KYY)
      //document.location.href="/com/actionLogout.do";
    });

    function OpenReLogin() {
      //if(if_relogin.frameElement.src == "") {
       if_relogin.frameElement.src = "/com/actionReLogin.do";
      //}
      //alert(if_relogin.frameElement.src);
      //alert('mytop.OpenReLogin_02');
      w_relogin.style.display = 'inline';
      //if_relogin.loginForm.id.focus();
    }

    function CloseReLogin() {
      w_relogin.style.display = 'none';
        ResetIdleTime();
    }

    function ResetIdleTime() {
      _idleTime=0;
    }

    function InitPage() {
      try {
      	MainMdiResize();
        // 공지사항 팝업
        //view_PopupManage();
      }
      catch (e) {
        alert("InitPage()" + "\r\n\r\n" + e);
        return false;
      }

      $('#mdimenu ul').on('click', ' li a .btn-mdiclose', function() {
        var tIdx = $(this).parent().attr("href").replace("#x-mdi-", "");
        _X.MDI_Close(tIdx);
      });
    }

    //팝업창띄우기
    function view_PopupManage(){

      //var FPops = _PopIds.split("|");
      //if( FPops.length == 1){
      //  ajaxPopupInfo_PopupManage( FPops );
      //}{

      var FPops = _X.XmlSelect("com", "POPUP", "popup", new Array(_CompanyCode),"Array");
        for(var i=0; i < FPops.length; i++)
        {
          ajaxPopupInfo_PopupManage( FPops[i][0] );
        }
      //}
      return;

    }

    // 팝업창 정보 Ajax통신으로 정보 획득
    function ajaxPopupInfo_PopupManage(popupIds){

      var returnValueArr = _X.XmlSelect("com", "POPUP", "popup_detail", new Array(popupIds),"Array");

      if(GetCookie(popupIds) == null ){
          popupOpen_PopupManage(popupIds,
                returnValueArr[0][0],
                returnValueArr[0][1],
                returnValueArr[0][2],
                returnValueArr[0][3],
                returnValueArr[0][4],
                returnValueArr[0][5],
                returnValueArr[0][6],
                returnValueArr[0][7]);
      }
    }

    //팝업창  오픈
    function popupOpen_PopupManage(popupId,fileUrl,bbsId,nttId,width,height,top,left,stopVewAt){

      var url = "<c:url value='/pwm/openPopupManage.do' />?";
      url = url + "fileUrl=" + fileUrl;
      url = url + "&stopVewAt=" + stopVewAt;
      url = url + "&popupId=" + popupId;
      url = url + "&bbsId=" + bbsId;
      url = url + "&nttId=" + nttId;
      var name = popupId;
      var openWindows = window.open(url,name,"width="+width+",height="+height+",top="+top+",left="+left+",toolbar=no,status=no,location=no,scrollbars=yes,menubar=no,resizable=yes");

      if (window.focus) {openWindows.focus()}
    }

    //팝업창  오픈 쿠키 정보 OPEN
    function GetCookie(name) {
      var prefix = name + "=";

      var cookieStartIndex = document.cookie.indexOf(prefix);
      if (cookieStartIndex == -1) return null;
      var cookieEndIndex = document.cookie.indexOf(";", cookieStartIndex + prefix.length);
      if (cookieEndIndex == -1) cookieEndIndex = document.cookie.length;

      return unescape(document.cookie.substring(cookieStartIndex + prefix.length, cookieEndIndex));
    }

    //세션 활성화여부 체크
    function sessionAliveCheck(){
      _X.ChkSession();
    }

    $(document).ready(function() {
      _X.MDI_Open("HR990100", "▶ 개인 인사정보", "/XG_Slick1.do?pcd=HR990100");
      _X.MDI_Open("HR990300", "▶ 증명서 발급신청", "/XG_Slick1.do?pcd=HR990300");
      _X.MDI_Open("HR990205", "▶ 휴가 신청서", "/XG_Slick1.do?pcd=HR990205");
      _X.MDI_Open("HR990201", "▶ 출장 신청서", "/XG_Slick1.do?pcd=HR990201");
      _X.MDI_Open("HR990301", "▶ 교육 신청서", "/XG_Slick1.do?pcd=HR990301");
      _X.MDI_Open("HR990501", "▶ 학자금 신청", "/XG_Slick1.do?pcd=HR990501");
      _X.MDI_Open("HR990507", "▶ 경조금 신청", "/XG_Slick1.do?pcd=HR990507");
      _X.MDI_Select(0);

      $(".btn-mdiclose").hide();
    });
</script>

</head>
<body class="top-body" style='overflow: hidden'> <!-- MOD: 2017.01.03 스크롤바 삭제 -->

  <!-- header 시작 -->
  <div class="menu-header">
  </div>

  <!-- MDI 메뉴 시작 -->
  <div id="mdimenu" onMouseOver="_X.ClosePopWin();">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs hide" role="tablist">
    </ul>

    <!-- div style="float:left; width:2px; height:22px;">&nbsp;</div -->
  </div>

  <div id="main-container">
    <!-- Tab panes -->
    <div id="main-mdi-content" class="tab-content hide">
    </div>

    <div id="mdi_main" style='display:none;'>
    </div>

    <!-- //MDI 메뉴 끝 -->
  </div>


</body>
</html>

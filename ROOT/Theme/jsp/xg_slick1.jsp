<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true"%>

<!DOCTYPE>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
  <title>X-Internet eGov Framework</title>

  <script type="text/javascript">
    var _Compact = false;
    var _PGM_CODE = "<%=pcd%>";
    var _WIN_OPEN_DATE = "<%=xgov.core.util.XUtility.GetCurrentDate("yyyy-MM-dd HH:mm:ss")%>";    
  </script>
  
  <!-- INCLUDE 변경 (2015.06.08 이상규) -->
  <jsp:include page="/Mighty/include/COMM_JS.jsp"/>
  
  <link type="text/css" charset="utf-8" href="/Mighty/css/mighty-2.0.3.css?v=20180701"  rel="stylesheet" media="all" />
  <link type="text/css" charset="utf-8" href="/Theme/css/custom_sub.css?v=20171121"  rel="stylesheet" media="all" />

  <% //추가(2016.12.27 KYY) %>
  <link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap-theme.min.css">

  <script type="text/javaScript" src="/APPS/com/js/Common.js?v=<%=xgov.core.util.XUtility.GetCurrentDate("yyyyMMddHHmmss")%>"></script>
	<%
		String grid_schima = "";
 		try {
		 	String gridparams  = pcd.substring(0,2) + _Col_Separate + pcd;
			grid_schima = xgov.core.dao.XDAO.XmlSelect3(request,"array", "com", "X5", "GRID_SCRIPT", gridparams, "all", _Row_Separate,_Col_Separate);
		} catch (Exception e) {
			e.printStackTrace();
		}
	%>
	<%if(grid_schima==null || grid_schima.equals("")){%>
	  <script type="text/javaScript" src="/APPS/<%=pcd.substring(0,2).toLowerCase()%>/grid/<%=pcd%>.Grid.js?v=<%=xgov.core.util.XUtility.GetCurrentDate("yyyyMMddHHmmss")%>"></script>
	<%}else{%>
	  <script type="text/javaScript"><%=grid_schima%>
	  </script>
	<%}%>

  <script type="text/javaScript" src="/APPS/com/js/<%=pcd.substring(0,2).toLowerCase()%>Common.js?v=<%=xgov.core.util.XUtility.GetCurrentDate("yyyyMMddHHmmss")%>"></script>
  <script type="text/javascript" src="/APPS/<%=pcd.substring(0,2).toLowerCase()%>/js/<%=pcd%>.js?v=<%=xgov.core.util.XUtility.GetCurrentDate("yyyyMMddHHmmss")%>"></script>
  
  
</head>

<body style="verticalAlign: top; text-align: left; display: inline;" bgcolor="#ffffff" topmargin="10px">
  <div id="element_to_pop_up" ></div>
  <div id='formLoadImg' style='position:absolute; left:50%; top:40%; z-index:10000;'>
    <img src='/Theme/images/viewLoading.gif'/>
  </div>
  <div id="pageLayout">
    <jsp:include page="<%=subUrl%>"/>
  </div>

  <script type="text/javascript">
    //입력필드 Cache 처리 값 불러오기 (2017.03.02 KYY)
    $(".cache").each(function(index){
      var chcheVal = _X.GetValue($(this).attr("id"));
      if(chcheVal!=null && chcheVal.length>0) {
        $(this).val(chcheVal);
      }
    });

    //입력필드 값 변경시 Cache 값 저장 (2017.03.02 KYY)
    $(".cache").change(function(){
        _X.SetValue($(this).attr("id"), $(this).val());
    });

    // 최근메뉴 설정
    _X.ExecProc("sm", "PROC_SM_AUTH_USER_RECENT", new Array("<%=user.getCompanyCode()%>", "<%=user.getUserId()%>", "<%=pcd%>"));

    //x_InitForm 호출위치 변경(2014.06.10 김양열)
    if(typeof(x_InitForm)!="undefined"){x_InitForm();}
    
    _X.Block();
    function xm_InitForm() {    
      pageLayout.ResizeInfo = {init_width:1000, init_height:710};
      // 모바일일때, resize 안하기 - mobile keyboard 사라짐 문제
      if (!navigator.userAgent.match(/iPad/i)&&!navigator.userAgent.match(/Android|webOS|iPhone|iPod|Blackberry/i) ) {
        $(window).bind("resize", xm_BodyResize);
      }
      xm_BodyResize(true);
      
      pageLayout.style.display = 'inline';
      if(window._GridRequestCnt==0){
        _X.UnBlock();
      } else if(window._GridRequestCnt == window._Grids.length) {//UnBlock 추가 (2015.06.15 이상규)
        _X.UnBlock();
      }
      setTimeout("xm_InitForm_After()",0);
    }

    function xm_InitForm_After() {
      //input style 과 event attach
      xm_InputInitial();
      $(":input:visible:enabled:first").not(".search_date,.detail_date,.option_date").focus();

      $("#pageLayout").on("click", function() {
        $(top.document).find("li.dropdown-mainmenu").removeClass("open");
      });

      // 도움말 확인 
      pf_chk_sbmanual();
      
    }
    $(window).bind("load", xm_InitForm);
    
    // 세방 메뉴얼 버튼 표시 
    function pf_chk_sbmanual() {
      if(mytop._CompanyCode&&"<%=pcd%>") {
        var tManualYn = _X.XmlSelect("sm", "SB_MANUAL", "GET_MANUAL_STATUS" , new Array(mytop._CompanyCode, "<%=pcd%>")  , 'json2');
        // pgmcode 에서 메뉴얼 버튼을 보이게 표시하고, 메뉴얼이 등록된 경우, 
        if(tManualYn[0].manual_yn=="Y") {
          //alert("Y")
          $("#buttons").append('<li><button class="btn btn-info btn-sm btn-circle-help" onclick="pf_open_manual(\''+_PGM_CODE+'\')">?</button></li>');
        }
      }
    }

    // 세방 메뉴얼 페이지열기 
    function pf_open_manual(aPgm) {
      if(aPgm) {
        window.open("/XG_Slick1.do?pcd=SM01160&pgm="+aPgm+"&sys="+aPgm.substr(0,2), "sbManual", "height=880,width=1900");
      }
    }

  
  </script>

</body>

</html>
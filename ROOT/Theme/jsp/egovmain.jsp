<%@ page language="java" contentType="text/html; charset=utf-8" session="true" pageEncoding="utf-8" trimDirectiveWhitespaces="true"%>

<script type="text/javascript">
  //팝업창띄우기
  function view_PopupManage(){
  	var li_left = 100;
  	var li_top  = 100;
    var aBoard = _X.XmlSelect("com", "POPUP", "BOARD_R01", new Array(_CompanyCode),"Array");
    for(var i=0; i < aBoard.length; i++) {
      if(_X.ToInt(aBoard[i][0]) > 0) {
        var ls_Cookie = GetCookie('popNotice_'+aBoard[i][0]);
        if (ls_Cookie != 'end') {
          var pop = window.open("/Theme/jsp/popBoard.jsp?BOARDID="+aBoard[i][0],"popNotice_"+aBoard[i][0],"width=700, height=450, scrollbars=yes, toolbar=no, resizable=yes, left="+li_left.toString()+", top="+li_top.toString() );
          //pop.focus();
          li_left += 50;
          li_top += 50;
        }
      }
    }
    return;
  }


  //팝업창  오픈 쿠키 정보 OPEN
  function GetCookie(name) {
    var prefix = name + "=";

    var cookieStartIndex = document.cookie.indexOf(prefix);
    if (cookieStartIndex == -1) return null;
    var cookieEndIndex = document.cookie.indexOf(";", cookieStartIndex + prefix.length);
    if (cookieEndIndex == -1) cookieEndIndex = document.cookie.length;

    return unescape(document.cookie.substring(cookieStartIndex + prefix.length, cookieEndIndex));
  }

  //세션 활성화여부 체크
  function sessionAliveCheck(){
    _X.ChkSession();
  }

</script>

<% if( !user.getPageTag().toString().equals("N") ) { %>

  <!DOCTYPE html>
  <html>
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />

  <script type="text/javascript" src="/Mighty/egov/js/pwm/prototype-1.6.0.3.js"></script>
  <title>노인장기요양보험 업무 관리 시스템</title>

  <script type="text/javascript">
    var _idleTime = 0;
  </script>

<style>
  #mdi_main {
    background-repeat: no-repeat;
    /*background: url(/Theme/images/main/main_bg.jpg) top left no-repeat;*/
}
</style>

  <jsp:include page="/Mighty/include/COMM_JS.jsp"/>


  <!-- main 화면에만 bs3 theme 적용 시작 -->
  <link rel="stylesheet" href="/Mighty/3rd/font-awesome/css/all.css">
  <link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap-theme.min.css">

  <link rel="stylesheet" href="/Mighty/css/mighty-bootstrap-1.0.2.css" />

  <script type="text/javaScript" src="/Mighty/3rd/bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javaScript" src="/Mighty/3rd/bootstrap/js/bootstrap-notify.min.js"></script><!-- 추가(2016.12.27 KYY) -->

  <script type="text/javaScript" src="/Mighty/js/mighty-win-bootstrap-1.0.0.js?v=201810051200"></script>
  <script type="text/javaScript" src="/Mighty/js/mighty-mdi-bootstrap-1.0.1.js?v=201810051400"></script>
  <script type="text/javaScript" src="/Mighty/js/mighty-ui-bootstrap-1.0.3.js?v=201810051200"></script>
  <!-- main 화면에만 bs3 theme 적용 끝 -->

  <link rel="stylesheet" href="/Theme/css/custom.css" />

  <script type="text/javascript" src="/Mighty/3rd/crypto/core.js"></script>
  <script type="text/javascript" src="/Mighty/3rd/crypto/base64.js"></script>

  <script type="text/javascript" src="/Mighty/3rd/typeahead/handlebars.js"></script>
  <link rel="stylesheet" href="/Mighty/3rd/typeahead/typeahead.css" />
  <script type="text/javascript" src="/Mighty/3rd/typeahead/bloodhound.js"></script>
  <script type="text/javascript" src="/Mighty/3rd/typeahead/typeahead.bundle.min.js"></script>

  <script type="text/javascript">
    var ifo = "<%=sysinfo%>";
    var pops = "<%=popinfo%>";

    var sessionTimer;
    var _MyActAuth;


    // 신사고 하이테크 OpenSheet 할 때, Key Params 전달용 변수 추가 2016 07 27 - JH
    var pgmParams ;

      $(document).ready(function () {
        //if (_LoginTag=="Y") pf_changePwd();

        <% if(session.getAttribute("strongpwd")!=null && ((String)session.getAttribute("strongpwd")).equals("N") && !user.getUserId().toLowerCase().equals("admin")) { %>
          //pf_changePwd(true);
        <% } %>
        if(_PasswordExpire>0) {
          //pf_changePwd(true);
        }
        if(_PasswordExpire==-5 || _PasswordExpire==-10 ) {
        //  alert("패스워드 유효기간이 " + (_PasswordExpire)*-1 + "일 남았습니다.\n패스워드를 변경해 주시기 바랍니다.");
        }

        InitPage();

        //65분에 한번씩 Session 체크(2015.06.02 KYY) - 웹로직 세션타임아웃 60분으로 설정
        sessionTimer = setInterval(sessionAliveCheck, 65 * 60 * 1000);

        _MyPgmList = _X.XmlSelect("sm", "SM_AUTH_PGMCODE", "R_EGOVMAIN_01", new Array(_CompanyCode,_UserID, '%'), "json", "PGM_CODE");
        //실행권한 설정(2017.03.21 KYY)
        _MyActAuth = _X.XmlSelect("sm", "SM_AUTH_PGMCODE", "MY_AUTH_ACT", new Array(_CompanyCode,_UserID), "array", "AUTH_ACT").join();

        _X.SetTopMenuD2();
        _X.SetTopMenuTS();

        // init_pgmsearch();
      });

      eval(_X.SD(ifo));
      eval(_X.SD(pops));

      function MainMdiResize() {
        //추가(2016.12.18 KYY)
        var newWidth  = _X.MDI_Width();
        var newHeight = _X.MDI_Height();

        mdi_main.style.width  = newWidth + "px";
        mdi_main.style.height = newHeight + "px";
      }

      //화면 리사이즈 제어
      $(window).bind('resize', function () {
        // 모바일일때, resize 안하기 - mobile keyboard 사라짐 문제
        if (!navigator.userAgent.match(/iPad/i)&&!navigator.userAgent.match(/Android|webOS|iPhone|iPod|Blackberry/i) ) {
          _X.MDI_Resize();
          MainMdiResize();
        }
      });

      $( window ).unload(function() {
        _X.MDI_CloseAll();
        _X.purge(window);
        //로그아웃 방식 변경으로 불필요한 로직 (2015.05.10 KYY)
        //document.location.href="/com/actionLogout.do";
      });

      function OpenReLogin() {
        //if(if_relogin.frameElement.src == "") {
         if_relogin.frameElement.src = "/com/actionReLogin.do";
        //}
        //alert(if_relogin.frameElement.src);
        //alert('mytop.OpenReLogin_02');
        w_relogin.style.display = 'inline';
        //if_relogin.loginForm.id.focus();
      }

      function CloseReLogin() {
        w_relogin.style.display = 'none';
          ResetIdleTime();
      }

      function ResetIdleTime() {
        _idleTime=0;
      }

      function InitPage() {
        try {
          MainMdiResize();
          // 공지사항 팝업
          view_PopupManage();
        }
        catch (e) {
          alert("InitPage()" + "\r\n\r\n" + e);
          return false;
        }

        $('#mdimenu ul').on('click', ' li a .btn-mdiclose', function() {
          var tIdx = $(this).parent().attr("href").replace("#x-mdi-", "");
          _X.MDI_Close(tIdx);
        });
      }

  </script>

  </head>
  <body class="top-body" style='overflow: hidden'> <!-- MOD: 2017.01.03 스크롤바 삭제 -->

    <!-- header 시작 -->
    <div class="menu-header">
      <%@ include file="/Theme/jsp/header.jsp"%>
    </div>

    <!-- MDI 메뉴 시작 -->
    <div id="mdimenu" onMouseOver="_X.ClosePopWin();">
      <!-- button class="btn btn-xs btn-default btn-mdiallclose pull-right">전체 <i class="fa fa-close"></i></button -->

      <!-- Nav tabs -->
      <ul class="nav nav-tabs hide" role="tablist">
      </ul>

      <!-- div style="float:left; width:2px; height:22px;">&nbsp;</div -->
    </div>

    <div id="main-container">
      <div id="w_relogin">
        <iframe id="if_relogin" src="" scrolling="no" width="100%" height="100%" align="top" frameborder="0" marginheight="0" marginwidth="0"></iframe>
      </div>

      <!-- //header 끝 -->

      <!-- Tab panes -->
      <div id="main-mdi-content" class="tab-content hide">
      </div>

      <div id="mdi_main">
        <%@ include file="/Theme/jsp/main.jsp"%>
        <!-- <iframe id="if_main" src="/XG_Slick1.do?pcd=GS09010" scrolling="yes" width="100%" height="100%" align="top" frameborder="0" marginheight="0" marginwidth="0"></iframe> -->
      </div>

      <!-- //MDI 메뉴 끝 -->

    </div>

    <%@ include file="/Theme/jsp/sidepanel.jsp"%>

  </body>
  </html>



<% } else { %>

  <!DOCTYPE html>
  <html>
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
  <meta name="viewport" content="width=device-width, user-scalable=no">

  <!-- <script type="text/javascript" src="/Mighty/egov/js/pwm/prototype-1.6.0.3.js"></script> -->
  <title>노인장기요양보험 업무 관리 시스템</title>

  <script type="text/javascript">
    var _idleTime = 0;
  </script>

  <style>
    /*#mdi_main {
      background-repeat: no-repeat;
      background: url(/Theme/images/main/main_bg.jpg) top left no-repeat;
    }*/
    .content { padding: 6px 0 0 6px; }
    .wrapper { background-color: #f4f3ef; }
  </style>


  <jsp:include page="/Mighty/include/COMM_JS.jsp"/>
  <link rel="stylesheet" href="/Mighty/3rd/jQuery/jquery-ui-1.11.4/jquery-ui.css">
  <link rel="stylesheet" href="/Mighty/3rd/font-awesome/css/all.css">
  <link rel="stylesheet" href="/Theme/3rd/themify/themify-icons.css">
  <link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="/Theme/3rd/bootstrap-datepicker/css/bootstrap-datepicker.css">
  <link rel="stylesheet" href="/Theme/3rd/handsontable/handsontable.full.min.css">
  <link rel="stylesheet" href="/Theme/css/paper-dashboard.css">
  <script type="text/javascript" src="/Mighty/3rd/jQuery/js/jquery-1.11.3.min.js"></script>
  <script type="text/javaScript" src="/Mighty/3rd/jQuery/jquery-ui-1.11.4/jquery-ui.min.js"></script>
  <script type="text/javaScript" src="/Mighty/3rd/jQuery/js/jquery.form.min.js"></script>
  <script type="text/javaScript" src="/Mighty/3rd/jQuery/js/jquery.blockUI-2.7.0.js"></script>

  <script type="text/javascript" src="/Mighty/3rd/bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javaScript" src="/Mighty/3rd/bootstrap/js/bootstrap-notify.min.js"></script>
  <script type="text/javascript" src="/Theme/3rd/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="/Theme/3rd/bootstrap-datepicker/locales/bootstrap-datepicker.ko.min.js"></script>
  <script type="text/javascript" src="/Theme/3rd/handsontable/handsontable.full.min.js"></script>



  <!-- <script type="text/javascript" src="/Mighty/js/mighty-2.0.0.js?v=20180511113000"></script> -->
  <script type="text/javascript" src="/Mighty/js/mighty-comm-1.2.8.js?v=201810180000"></script>
  <script type="text/javascript" src="/Mighty/js/mighty-dao-1.2.6.js?v=201810180000"></script>
  <script type="text/javascript" src="/Mighty/js/mighty-ui-1.2.9.js?v=201810180000"></script>
  <script type="text/javascript" src="/Mighty/js/mighty-dao-handsontable-0.0.1.js?v=201810180000"></script>
  <script type="text/javaScript" src="/Mighty/js/mighty-ui-bootstrap-1.0.3.js?v=201810180000"></script>




  <script type="text/javascript" src="/Mighty/3rd/crypto/base64.js"></script>
  <script type="text/javascript" src="/Mighty/js/mighty-print-ireport-1.0.0.js?v=201810180000"></script>

  <script type="text/javascript" src="/Theme/3rd/vuejs/vue.js"></script>
  <script type="text/javascript" src="/Theme/js/script_partner_comm.js"></script>

  <link type="text/css" href="https://fonts.googleapis.com/css?family=Muli:400,300" rel="stylesheet">
  <link rel="stylesheet" href="/Theme/css/custom_partner.css?v=<%=xgov.core.util.XUtility.GetCurrentDate("yyyyMMddHHmmss")%>">
  <link rel="stylesheet" href="/Mighty/css/mighty-bootstrap-1.0.2.css" />  <!-- css 적용 -->


  <script type="text/javaScript" src="/Mighty/js/mighty-win-bootstrap-1.0.0.js?v=201810051200"></script>
  <script type="text/javaScript" src="/Mighty/js/mighty-mdi-bootstrap-1.0.1.js?v=201810051400"></script>
  <script type="text/javaScript" src="/Mighty/js/mighty-ui-bootstrap-1.0.3.js?v=201810051200"></script>

  <link rel="stylesheet" href="/Theme/css/custom.css" />

  <script type="text/javascript" src="/Mighty/3rd/crypto/core.js"></script>

  <script type="text/javascript" src="/Mighty/3rd/typeahead/handlebars.js"></script>
  <link rel="stylesheet" href="/Mighty/3rd/typeahead/typeahead.css" />
  <script type="text/javascript" src="/Mighty/3rd/typeahead/bloodhound.js"></script>
  <script type="text/javascript" src="/Mighty/3rd/typeahead/typeahead.bundle.min.js"></script>

  <script type="text/javascript">
    var _XU = new Object();
    var _XV = new Object();

    var ifo = "<%=sysinfo%>";
    var pops = "<%=popinfo%>";

    var sessionTimer;
    var _MyActAuth;

    var pgmParams ;

    var vMenus;

      $(document).ready(function () {
        <% if(session.getAttribute("strongpwd")!=null && ((String)session.getAttribute("strongpwd")).equals("N") && !user.getUserId().toLowerCase().equals("admin")) { %>
        <% } %>
        if(_PasswordExpire>0) {
        }
        if(_PasswordExpire==-5 || _PasswordExpire==-10 ) {
        }

        InitPage();
        sessionTimer = setInterval(sessionAliveCheck, 65 * 60 * 1000);

        _MyPgmList = _X.XmlSelect("sm", "SM_AUTH_PGMCODE", "R_EGOVMAIN_01", new Array(_CompanyCode,_UserID, '%'), "json", "PGM_CODE");
        _MyActAuth = _X.XmlSelect("sm", "SM_AUTH_PGMCODE", "MY_AUTH_ACT", new Array(_CompanyCode,_UserID), "array", "AUTH_ACT").join();
        //_X.SetTopMenuD2();
        //_X.SetTopMenuTS();
         vMenus = new Vue({
          el: '#v-menu',
          data: {
            menus: _MyPgmList
          }
        });

        /*vMenus.$nextTick(function () {
          lbd.initRightMenu();
        });*/

        //console.log(_MyPgmList.length, _MyActAuth.length)
        //_X.SetTopMenuD2();
        //_X.SetTopMenuTS();

        // init_pgmsearch();
        if (typeof(_XU.Initial)!="undefined") {

          // YSKIM
          // if(_UserTag!="9") {
          //   $("#wrap-cond").removeClass("hide");
          //   $(".sidebar").addClass("hide");
          //   $(".main-panel").css("width", "100%");
          // }

          _XU.Initial();
        }
      });

      eval(_X.SD(ifo));
      eval(_X.SD(pops));

      function MdiOpen(a_pgmcode, a_pgmname) {
        mytop._X.MDI_Open(a_pgmcode,a_pgmname,'/Mighty/template/XG_Slick1.jsp?pcd='+a_pgmcode);
      }

      function MainMdiResize() {
        var newWidth  = _X.MDI_Width();
        var newHeight = _X.MDI_Height();

        mdi_main.style.width  = (newWidth - 500) + "px";
        mdi_main.style.height = (newHeight - 500)  + "px";
      }

      $(window).bind('resize', function () {
        if (!navigator.userAgent.match(/iPad/i)&&!navigator.userAgent.match(/Android|webOS|iPhone|iPod|Blackberry/i) ) {
          _X.MDI_Resize();
          MainMdiResize();
        }
      });

      $( window ).unload(function() {
        _X.MDI_CloseAll();
        _X.purge(window);
      });

      function OpenReLogin() {
         if_relogin.frameElement.src = "/com/actionReLogin.do";
        w_relogin.style.display = 'inline';
      }

      function CloseReLogin() {
        w_relogin.style.display = 'none';
          ResetIdleTime();
      }

      function ResetIdleTime() {
        _idleTime=0;
      }

      function InitPage() {
        try {
          MainMdiResize();
          // 공지사항 팝업
          view_PopupManage();
        }
        catch (e) {
          alert("InitPage()" + "\r\n\r\n" + e);
          return false;
        }

        $('#mdimenu ul').on('click', ' li a .btn-mdiclose', function() {
          var tIdx = $(this).parent().attr("href").replace("#x-mdi-", "");
          _X.MDI_Close(tIdx);
        });
      }

      //로그아웃 처리(2015.05.10 KYY)
      var pf_logout = function() {
        if(confirm("접속을 종료 하시겠습니까?")){
          document.location.href="/com/actionLogout.do";
        }
      }

      //비밀번호 변경 창..
      var pf_changePwd = function(mustchange) {
        //반드시 바꾸어야 하는 경우
        if(mustchange==null)
          mustchange = false;
        var url = "<c:url value='/user/EgovUserPasswordUpdtView.do'/>";
        var modal = "<div id='findmodal'><iframe id='if_findmodal' src='" + url + "' class='iframe_popup' style='width:430px; height:300px;' frameborder=0 marginwidh=0 marginheight=0></iframe></div>";
        if(mustchange) {
          var title = "비밀번호변경";
          $(modal).dialog({title: title, show:false, width:455, height:420, modal:true, resizable:false,
                closeOnEscape: false,
                buttons: {
                  "확인": function() { $("#if_findmodal").get(0).contentWindow.pf_UpdatePwd(); }
                },
                open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); }  ,
                close:function(){try {$(this).remove();} catch(e) {}}
          });
        } else {
          $(modal).dialog({title:'비밀번호변경', show:false, width:455, height:420, modal:true, resizable:false, //325
                closeOnEscape: true,
                buttons: {
                  "확인": function() { $("#if_findmodal").get(0).contentWindow.pf_UpdatePwd(); },
                  "취소": function() { $("#findmodal").dialog("close"); }
                },
                close:function(){try {$(this).remove();} catch(e) {}}
          });
        }
      }

      function changeIframeUrl(url){
        document.getElementById("if_main").src = "/XG_Slick1.do?pcd=" +url;
      }

      function mid(code){
        console.log(code);
        //mytop._X.MDI_Open(code, name,'/Mighty/template/XG_Slick1.jsp?pcd='+menucode);
      }

  </script>



  </head>
  <body class="top-body" >

    <div class="wrapper">

      <div class="sidebar" data-background-color="black" data-active-color="info">

        <div class="sidebar-wrapper">
          <div class="logo" style="width:100%; height: 55px;">
            <a href="/" class="simple-text"><img src="/Theme/images/menu/logo_top.gif" class="img-responsive" style='width: 149px;height:48px;margin:-15px 0px 0px 40px'></a>
          </div>

          <div class="sidebar-header" align="center" style="color:#68B3C8;font-weight:700;margin-top:5px;">
            <div class="clearfix"><small><%=user.getUserId()%></small></div>
            <div class="clearfix"><%=user.getUserName()%></div>
          </div><!-- sidebar-header -->

          <ul id="v-menu" class="nav">
            <template v-for="(menu, idx) in menus">
              <template v-if="menu.MENU_LEVEL > 1" id="v-menu7">
                <li v-bind:class="'level'+menu.MENU_LEVEL+' '+(menu.PGM_CODE=='<%=user.getstdCode()%>' ? 'active' : '')">
                <!-- <a v-bind:href="menu.MENU_LEVEL==2" onclick="mytop._X.MDI_Open('PT02050','menu.PGM_NAME' ,'/Mighty/template/XG_Slick1.jsp?pcd=PT02050');"> -->
                <a v-bind:onclick="(menu.MENU_LEVEL==2 ? '#' : 'MdiOpen(\''+menu.PGM_CODE+'\', \''+menu.PGM_NAME+'\')')" style="cursor:pointer;">
                  <i v-if="menu.MENU_LEVEL==2" v-bind:class="menu.CONN_URL"></i>
                  <i v-if="menu.MENU_LEVEL==3" class="ti-angle-right"></i>
                  <p>{{menu.PGM_NAME}}</p>
                </a>
              </li>
              </template>
            </template>
          </ul>
        </div>
      </div>

      <div class="main-panel" style="height: 800px;">

        <div class="menu-header">
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar bar1"></span>
                <span class="icon-bar bar2"></span>
                <span class="icon-bar bar3"></span>
              </button>
              <a class="navbar-brand" href="#"></a>
            </div>
            <div class="collapse navbar-collapse">
              <ul id="v-menu2" class="nav navbar-nav navbar-right">
                <!-- li>
                  <a href="#" data-toggle="modal" data-target="#infoModal">
                    <i class="ti-settings"></i>
                    <p>정보수정</p>
                  </a>
                </li -->
                <li>
                  <a href="/partner/down_manual.jsp" target="_blank">
                    <i class="ti-help-alt"></i>
                    <p>도움말 다운로드</p>
                  </a>
                </li>
                <li>
                  <a href="#" data-toggle="modal" data-target="#pwModal">
                    <i class="ti-key"></i>
                    <p>비밀번호 변경</p>
                  </a>
                </li>
                <li>
                  <a href="javascript:pf_logout()">
                    <i class="ti-power-off"></i>
                    <p>로그아웃</p>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        </div>

        <div id="mdimenu" onMouseOver="_X.ClosePopWin();">
          <ul class="nav nav-tabs hide" role="tablist">
          </ul>
        </div>

        <div id="main-container">
          <div id="w_relogin">
            <iframe id="if_relogin" src="" scrolling="no" width="100%" height="100%" align="top" frameborder="0" marginheight="0" marginwidth="0"></iframe>
          </div>

          <div id="main-mdi-content" class="tab-content hide"></div>

          <div id="mdi_main">
            <%@ include file="/Theme/jsp/main.jsp"%>
          </div>
        </div>
    </div>
  </div>

<!-- Modal -->
<div class="modal fade" id="ptModal" tabindex="-1" role="dialog" aria-labelledby="ptModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="ptModalLabel">{{modal_title}}</h4>
      </div>
      <div id="ptModalBody" class="modal-body">
        <div id="dg_detail" class="grid-div">
        </div>
        <div id="modal_detail">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">닫 기</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="pwModal" tabindex="-1" role="dialog" aria-labelledby="pwModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="pwModalLabel"><i class="ti-key"></i> 비밀번호 변경</h4>
      </div>
      <div id="pwModalBody" class="modal-body">
        <div class="form-horizontal">
          <div class="form-group form-group-sm">
            <label for="cur_pass" class="col-sm-3 control-label">현재 비밀번호</label>
            <div class="col-sm-9">
              <input type="password" class="form-control border-input input-sm" id="cur_pass" placeholder="현재 비밀번호">
            </div>
          </div>
          <div class="form-group form-group-sm">
            <label for="pass1" class="col-sm-3 control-label">새 비밀번호</label>
            <div class="col-sm-9">
              <input type="password" class="form-control border-input input-sm" id="pass1" placeholder="새 비밀번호">
            </div>
          </div>
          <div class="form-group form-group-sm">
            <label for="pass2" class="col-sm-3 control-label">새 비밀번호 확인</label>
            <div class="col-sm-9">
              <input type="password" class="form-control border-input input-sm" id="pass2" placeholder="새 비밀번호 확인">
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning btn-sm" id="btn-password">수 정</button>
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">닫 기</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="infoModalLabel"><i class="ti-settings"></i> 정보수정</h4>
      </div>
      <div id="infoModalBody" class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">닫 기</button>
      </div>
    </div>
  </div>
</div>
</body>
</html>

<% } %>

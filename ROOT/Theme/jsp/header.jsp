<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%
  xgov.core.vo.XLoginVO loginVO = (xgov.core.vo.XLoginVO)egovframework.rte.com.util.service.EgovUserDetailsHelper.getAuthenticatedUser();
%>

<nav class="navbar navbar-default navbar-depth-2">
  <div class="container-fluid menu-depth-1">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#" onclick="_X.MDI_ShowMain()" style="background-color: #FFFFFF">
        <img src="/Theme/images/menu/logo_top.gif"  style='width: 149px;height:48px;margin-top:0px;margin-left:13px' >
      </a>
    </div>

    <div class="collapse navbar-collapse" id="navbar-collapse-1">
      <ul id="main-menu" class="nav navbar-nav">

      </ul>

      <ul id="right-menu" class="nav navbar-nav navbar-right">
        <li id="search-pgmcode">
          <input id="find-pgmname" class="typeahead" type="text" placeholder="프로그램명">
        </li>
        <li id="btn-recent-menu" class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">최근메뉴 <span class="caret"></span></a>
          <ul id="recent-menu" class="dropdown-menu">
            <li><a href="#">최근 사용한 메뉴가 없습니다.</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><%=loginVO.getUserName()%> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#" onclick="pf_changePwd()">비밀번호 변경</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="javascript:pf_logout()">logout</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
  <div class="container-fluid menu-depth-2">



  </div>
</nav>

<script>
$(function() {
	$(".menu-depth-2 .navbar-nav li a").not(".dropdown-toggle").click(function() {
    _X.OpenSheet($(this).attr("data-pgmcode"));
  });

  $('#btn-recent-menu').on('show.bs.dropdown', function () {
    pf_mymenu();
  });

  $('#manage-company').change(function() {
    _CompanyCode = $(this).val();
  });

	// 대메뉴 선택시
  $('#main-menu a').click(function() {
    var tCd         = $(this).attr('data-pgmcode');
    var sidePanelYn = $(this).attr('data-sidepanel');

  	// 사이드 패널 메뉴 설정
    if ( sidePanelYn == "Y" ) {
	    $("#main-menu.navbar-nav li").removeClass('active');
	    $(this).parent().addClass('active');

			$('.sub-menu-panel').addClass("hide");
			$('.menu-depth-2 .navbar-nav').addClass("hide");
	    $('#sub-menu-'+tCd.substring(0, 2).toLowerCase()).removeClass("hide");

	    if( $('.x5-side-panel').attr('data-status')=="closed" ) {
	      $(".toggler").trigger("click")
	                   .show(); //$(".toggler").click();
	    }
    }
    // 기본 메뉴 설정
    else {
    	$("#main-menu.navbar-nav li").removeClass('active');
	    $(this).parent().addClass('active');

			$('.sub-menu-panel').addClass("hide");
	    $('.menu-depth-2 .navbar-nav').addClass("hide");
	    $('#main-menu-'+tCd.toLowerCase()).removeClass("hide");

	    if( $('.x5-side-panel').attr('data-status')=="opened" ) {
	      $(".toggler").trigger("click")
	                   .hide(); //$(".toggler").click();
	    }
    }
  });

  $("#search-pgmcode .typeahead").focus(function() {
    $(this).val("");
  });

  init_pgmsearch();

  $('ul.sub-menu-dl li a').on('click', ' li a .btn-mdiclose', function() {
    var tIdx = $(this).parent().attr("href").replace("#x-mdi-", "");
    _X.MDI_Close(tIdx);
  });
});

//메인 보일때 메인 내리기
var pf_showtab = function() {
  if( !$("#mdi_main").hasClass("hide") ) _X.MDI_ShowTab();
}

//로그아웃 처리(2015.05.10 KYY)
var pf_logout = function() {
  if(confirm("접속을 종료 하시겠습니까?")){
    document.location.href="/com/actionLogout.do";
  }
}

//비밀번호 변경 창..
var pf_changePwd = function(mustchange) {
  //반드시 바꾸어야 하는 경우
  if(mustchange==null)
    mustchange = false;
  var url = "<c:url value='/user/EgovUserPasswordUpdtView.do'/>";
  var modal = "<div id='findmodal'><iframe id='if_findmodal' src='" + url + "' class='iframe_popup' style='width:430px; height:300px;' frameborder=0 marginwidh=0 marginheight=0></iframe></div>";
  if(mustchange) {
    var title = "비밀번호변경";
    $(modal).dialog({title: title, show:false, width:455, height:420, modal:true, resizable:false,
          closeOnEscape: false,
          buttons: {
            "확인": function() { $("#if_findmodal").get(0).contentWindow.pf_UpdatePwd(); }
          },
          open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); }  ,
          close:function(){try {$(this).remove();} catch(e) {}}
    });
  } else {
    $(modal).dialog({title:'비밀번호변경', show:false, width:455, height:420, modal:true, resizable:false, //325
          closeOnEscape: true,
          buttons: {
            "확인": function() { $("#if_findmodal").get(0).contentWindow.pf_UpdatePwd(); },
            "취소": function() { $("#findmodal").dialog("close"); }
          },
          close:function(){try {$(this).remove();} catch(e) {}}
    });
  }
}

//나의메뉴 표시
var pf_mymenu = function(a_area){
  var mymenu_html = "";

  var mymenu = _X.XmlSelect("com", "COMMON", "MENU_RECENT", ['<%=loginVO.getCompanyCode()%>','<%=loginVO.getUserId()%>'], "json");

  $("#recent-menu li").remove();

  if(mymenu.length>0) {
    for(var i=0; i<mymenu.length; i++) {
      $("#recent-menu").append('<li><a href="#" data-pgmcode="' + mymenu[i].PGM_CODE + '" data-pgmname="' + mymenu[i].PGM_NAME + '" data-openurl="' + mymenu[i].PGM_URL + '" >' + mymenu[i].PGM_NAME + '</a></li>');
    }
  } else {
    $("#recent-menu").append('<li><a href="#">최근 사용한 메뉴가 없습니다.</a></li>');
  }

  $("#recent-menu li a").not(".dropdown-toggle").click(function() {
    _X.OpenSheet($(this).attr("data-pgmcode"));
  });
};


var substringMatcher = function(strs) {
  return function findMatches(q, cb) {
    var matches, substringRegex;

    // an array that will be populated with substring matches
    matches = [];

    // regex used to determine if a string contains the substring `q`
    substrRegex = new RegExp(q, 'i');

    // iterate through the pool of strings and for any string that
    // contains the substring `q`, add it to the `matches` array
    $.each(strs, function(i, str) {
      if (substrRegex.test(str.PGM_NAME) && str.CHILD_MENU_CNT=="0") {
        matches.push(str);
      }
    });

    cb(matches);
  };
};

var init_pgmsearch = function() {
  $('#search-pgmcode .typeahead').typeahead({
    hint: true,
    highlight: true,
    minLength: 1.,
    limit: 10,
  },
  {
    name: 'pgms',
    displayKey: 'PGM_NAME',
    source: substringMatcher(_MyPgmList),
    templates: {
      empty: '<div class="empty-message">일치하는 목록이 없습니다.</div>',
      suggestion: Handlebars.compile('<div><strong>{{PGM_NAME}}</strong> <small>[{{PARENT_PGM_NAME}}]</small></div>')
    }
  }).on('typeahead:selected', function($e, datum){
    _X.OpenSheet(datum.PGM_CODE);
  });
}
</script>

$(function() {
  $(window).trigger("resize");

  if(mytop._UserID) {
    pf_draw_main();
    pf_set_list('%');
  }
});

// 메인화면 메뉴 탭 그리기
var pf_draw_main = function() {
  var tPid;
  var tIdx = 0;
  var tHtml = "";
  var tFirst = "";  // 처음 열리는 탭 MCODE

  var main_lists = [];

  $.each(main_lists, function(i, e) {
    tPid = e.MCODE.substr(0, 1)+"0000";

    if(e.MCODE.substr(1, 4)=="0000") {
      // 대메뉴 타이틀 추가
      $("#"+tPid+" .title-tab a").html(e.MNAME);
      tIdx=0;
      $("#"+tPid+" .tab-content").empty();

      // 첫번째 탭 활성화
      pf_active_tab(tFirst);
    } else {
      // 기본은 첫번째 탭 열림
      if(tIdx==0||e.FIRST_YN=="Y") tFirst = e.MCODE;

      // 좌측탭 그리기
      $("#"+tPid+" .nav-tabs.list-tabs").append('<li id="li-'+e.MCODE+'">'+(!e.MNAME?'':'<div pgm-code="'+e.PGM_CODE+'" class="pull-right"><i class="fa fa-external-link"></i>')+'</div><a href="#'+e.MCODE+'" '+(e.ACTION_TYPE=="LINK"?' pgm-code="'+e.PGM_CODE+'" pgm-params="'+e.PGM_PARAMS+'" class="action-opensheet"':'data-toggle="tab"')+'>'+(!e.MNAME?"&nbsp;":e.MNAME)+(e.CNT_YN=="Y"?" ("+e.CNT+")":"")+'</a></li>');

      tHtml = "";
      tHtml += '<div class="tab-pane fade" id="'+e.MCODE+'">';
      tHtml += '<div class="wrap-more text-right"><button class="btn btn-xs" pgm-code="'+e.PGM_CODE+'"><i class="fa fa-plus"></i> 더보기</button></div>';
      tHtml += '<ul class="latest-list"></ul>';
      tHtml += '</div>';
      $("#"+tPid+" .tab-content").append(tHtml);

      tIdx++;
    }
  });
  pf_active_tab(tFirst);

  // 탭 선택할 때, 게시물 가져오기
  $('a[data-toggle="tab"]').on("show.bs.tab", function() {
    pf_set_list( $(this).attr('href').replace(/#/, "") );
  });

  // 더보기 버튼 이벤트 연결
  $(".wrap-main .btn-xs, .wrap-main .list-tabs>li .pull-right").off().on("click", function() {
    if($(this).attr("pgm-code")) {
      _X.OpenSheet($(this).attr("pgm-code"));
    } else {
      _X.MsgBox("확인", "더보기 링크가 연결되지 않았습니다.");
    }
  });

  // LINK 방식의 탭 이벤트 연결
  $(".list-tabs a.action-opensheet").off().on("click", function() {
    if($(this).attr("pgm-code")) {
      mytop.pgmParams = $(this).attr("pgm-params");
      _X.OpenSheet($(this).attr("pgm-code"));
    }
  });
}

// 메인화면 목록 넣기
var pf_set_list = function(aCd) {
  var main_lists = [];
  var tHtml = "";
  var tObj = "";

  $.each(main_lists, function(i, e) {
    if(tObj==""||tObj!="#"+e.MCODE+" ul.latest-list") {
      tObj = "#"+e.MCODE+" ul.latest-list";
      $(tObj).empty();

    }

    tHtml = "";
    tHtml += '<li>';
    //tHtml += '<td>'+(e.RNK>0 ? e.RNK : "" )+'</td>';
    tHtml += '<div class="item-title"><a href="#" pgm-code="'+e.PGM_CODE+'" pgm-params="'+e.PGM_PARAMS+'">'+e.TITLE+'</a></div>';
    tHtml += '<div class="item-date">'+e.WDATE+'</div>';
    tHtml += '</li>';

    $(tObj).append(tHtml);
  });

  // 목록 클릭시 프로그램 열기 이벤트 추가
  $("ul.latest-list .item-title a").off().on("click", function() {
    if($(this).attr("pgm-code")) {
      mytop.pgmParams = $(this).attr("pgm-params");
      _X.OpenSheet($(this).attr("pgm-code"));
    }
  });
}

// 코드로 탭 활성화
var pf_active_tab = function(aMc) {
  $("#li-"+aMc).addClass("active");
  $("#"+aMc).addClass("active in");
}
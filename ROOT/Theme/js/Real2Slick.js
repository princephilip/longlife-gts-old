var _Renderer_check_YN = {type: "check", editable: true, startEditOnClick: true, trueValues: "Y", falseValues: "N", labelPosition: "hidden"};
var _Renderer_check_edit_YN = {type: "check", editable: false, startEditOnClick: true, trueValues: "Y", falseValues: "N", labelPosition: "hidden"};

var _Styles_numberf ;
var _Editor_numberf	;

function tf2yn(val) {
	return (val==null || val?"Y":"N");
}

function get_stylesname(val) {
	switch(val) {
		case _Styles_seq			 : return "_Styles_seq"; break;
		case _Styles_text      : return "_Styles_text"; break;
		case _Styles_textc     : return "_Styles_textc"; break;
		case _Styles_textr     : return "_Styles_textr"; break;
		case _Styles_number    : return "_Styles_number"; break;
		case _Styles_numberc   : return "_Styles_numberc"; break;
		case _Styles_numberl   : return "_Styles_numberl"; break;
		case _Styles_numberf   : return "_Styles_numberf2"; break;
		case _Styles_numberf1  : return "_Styles_numberf1"; break;
		case _Styles_numberf2  : return "_Styles_numberf2"; break;
		case _Styles_numberf3  : return "_Styles_numberf3"; break;
		case _Styles_numberf4  : return "_Styles_numberf4"; break;
		case _Styles_date      : return "_Styles_date"; break;
		case _Styles_datemonth : return "_Styles_datemonth"; break;
		case _Styles_datetime  : return "_Styles_datetime"; break;
		case _Styles_checkbox  : return "_Styles_checkbox"; break;
		case _Styles_tree      : return "_Styles_tree"; break;
	}
}

function get_editorname(val) {
	switch(val) {
		case _Editor_text 		 : return "_Editor_text"; break;
		case _Editor_multiline : return "_Editor_multiline"; break;
		case _Editor_number 	 : return "_Editor_number"; break;
		case _Editor_numberf   : return "_Editor_numberf2"; break;
		case _Editor_numberf1  : return "_Editor_numberf1"; break;
		case _Editor_numberf2  : return "_Editor_numberf2"; break;
		case _Editor_numberf3  : return "_Editor_numberf3"; break;
		case _Editor_numberf4  : return "_Editor_numberf4"; break;
		case _Editor_date		   : return "_Editor_date"; break;
		case _Editor_datemonth : return "_Editor_datemonth"; break;
		case _Editor_datetime	 : return "_Editor_datetime"; break;
		case _Editor_checkbox  : return "_Editor_checkbox"; break;
		case _Editor_dropdown  : return "_Editor_dropdown"; break;
	}
}

function get_updateType(val) {
	switch(val) {
		case _Styles_seq			 : return "number"; break;
		case _Styles_text      : return "char"; break;
		case _Styles_textc     : return "char"; break;
		case _Styles_textr     : return "char"; break;
		case _Styles_number    : return "number"; break;
		case _Styles_numberc   : return "number"; break;
		case _Styles_numberl   : return "number"; break;
		case _Styles_numberf   : return "number"; break;
		case _Styles_numberf1  : return "number"; break;
		case _Styles_numberf2  : return "number"; break;
		case _Styles_numberf3  : return "number"; break;
		case _Styles_numberf4  : return "number"; break;
		case _Styles_date      : return "date"; break;
		case _Styles_datemonth : return "date"; break;
		case _Styles_datetime  : return "date"; break;
		case _Styles_checkbox  : return "char"; break;
		case _Styles_tree      : return "char"; break;
		default								 : return "char"; break;
	}
}

function Real2Slick(sys_id, pgm_code, grid_id, grid_val) {

	var sql = "DELETE FROM SM_DEV_GRID_COLS WHERE SYS_ID='"+ sys_id.toUpperCase() + "' AND PGM_CODE='" + pgm_code + "' AND GRID_ID='" + grid_id + "'";
	_X.EP("sm", "PROC_XM_ES", new Array(sql));
	var dgSql = "BEGIN ";
	for(var i=0; i<grid_val.length; i++) {
		dgSql += "INSERT INTO SM_DEV_GRID_COLS "
				+ "(SYS_ID, PGM_CODE, GRID_ID, SEQ, FIELD_SEQ, FIELDNAME, TABLE_NAME, WIDTH, MUST_INPUT, VISIBLE, READONLY, EDITABLE, SORTABLE, HEADER, HEADER_TEXT, "
				+ " STYLES, EDITOR, LOOKUPDISPLAY, RENDERER, BUTTON, ALWAYSSHOWBUTTON, HEADERTOOLTIP, MERGE, MAXLENGTH, FOOTEREXPR, FOOTERALIGN, RESIZABLE, UPDATE_TYPE, UPDATABLE, UPDATE_ISKEY, "
				+ " ROW_INPUT_DATE, ROW_INPUT_EMP_NO, ROW_INPUT_IP, ROW_UPDATE_DATE, ROW_UPDATE_EMP_NO, ROW_UPDATE_IP, FREEFORM_RATE)"
				+ " values ("
				+ "'" + sys_id.toUpperCase() + "', "
				+ "'" + pgm_code + "', "
				+ "'" + grid_id + "', "
				+ "" + (i+1) + ", "
				+ "" + ((i+1)*10) + ", "
				+ "'" + grid_val[i].fieldName + "', "
				+ "'" + "" + "', "
				+ "" + grid_val[i].width + ", "
				+ "'" + (typeof(grid_val[i].must_input   )=="undefined" ? "N" : grid_val[i].must_input    ) + "', "
				+ "'" + tf2yn(grid_val[i].visible) + "', "
				+ "'" + tf2yn(grid_val[i].readonly) + "', "
				+ "'" + tf2yn(grid_val[i].editable) + "', "
				+ "'" + tf2yn(grid_val[i].sortable) + "', "
				+ "'" + "" + "', "
				+ "'" + (typeof(grid_val[i].header.text)=="undefined" || grid_val[i].header.text=="" ? grid_val[i].fieldName : grid_val[i].header.text) + "', "
				
				+ "'" + (typeof(grid_val[i].styles   )=="undefined" ? "_Styles_text" : get_stylesname(grid_val[i].styles)) + "', "
				+ "'" + (typeof(grid_val[i].editor   )=="undefined" ? "_Editor_text" : get_editorname(grid_val[i].editor)) + "', "
				+ "'" + (typeof(grid_val[i].lookupDisplay   )=="undefined" ? "" : tf2yn(grid_val[i].lookupDisplay)    ) + "', "
				+ "'" + (typeof(grid_val[i].renderer        )=="undefined" ? "" : grid_val[i].renderer         ) + "', "
				+ "'" + (typeof(grid_val[i].button          )=="undefined" ? "" : grid_val[i].button           ) + "', "
				+ "'" + (typeof(grid_val[i].alwaysShowButton)=="undefined" ? "" : tf2yn(grid_val[i].alwaysShowButton) ) + "', "
				+ "'" + (typeof(grid_val[i].headerToolTip   )=="undefined" ? "" : grid_val[i].headerToolTip    ) + "', "
				+ "'" + (typeof(grid_val[i].merge           )=="undefined" ? "" : grid_val[i].merge            ) + "', "
				+ "'" + (typeof(grid_val[i].maxLength       )=="undefined" ? "" : grid_val[i].maxLength        ) + "', "
				+ "'" + (typeof(grid_val[i].footer      )=="undefined" ? "" : grid_val[i].footer.expression.toUpperCase()) + "', "
				+ "'" + (typeof(grid_val[i].footerAlign     )=="undefined" ? "" : grid_val[i].footerAlign      ) + "', "
				+ "'" + (typeof(grid_val[i].resizable       )=="undefined" ? "" : tf2yn(grid_val[i].resizable) ) + "', "
				
				+ "'" + get_updateType(grid_val[i].styles) + "', "
				+ "'" + "Y" + "', "
				+ "'" + "N" + "', "

				+ "sysdate,'ADMIN','127.0.0.1',null,null,null,null);"
				;
		if(i==40 || i==80 || i==120 || i==160 || i==200) {
			dgSql += " END;";
			_X.EP("sm", "PROC_XM_ES", new Array(dgSql));
			dgSql = "BEGIN ";
		}
	}	
	dgSql += " END;";
	_X.EP("sm", "PROC_XM_ES", new Array(dgSql));
	_X.EP("sm", "SP_SM_DEV_GRID_FIX", new Array(sys_id.toUpperCase(),pgm_code,grid_id));

}
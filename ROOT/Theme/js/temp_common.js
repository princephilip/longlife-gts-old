/* 
임시로 쓸지도 모르는 js 
java에서 처리하는 부분 적용하기 전.
*/

$(function() {
  $("button.ui-button").css("float", "left").addClass("btn btn-default btn-xs");
  //$(".ui-icon").hidden();
  $("#buttons #xBtnSave").html("<i class=\"fa fa-save\"></i> 저장").removeClass("save");
  $("#buttons #xBtnRetrieve").html("<i class=\"fa fa-search\"></i> 조회").removeClass("retrieve");
  $("#buttons #xBtnInsert").html("<i class=\"fa fa-plus-square-o\"></i> 삽입").removeClass("insert");
  $("#buttons #xBtnAppend").html("<i class=\"fa fa-plus-square-o\"></i> 추가").removeClass("append");
  $("#buttons #xBtnDuplicate").html("<i class=\"fa fa-copy\"></i> 복제").removeClass("duplicate");
  $("#buttons #xBtnExcel").html("<i class=\"fa fa-file-excel-o\"></i> 엑셀").removeClass("excel");
  $("#buttons #xBtnDelete").html("<i class=\"fa fa-close\"></i> 삭제").removeClass("delete");
  $("#buttons #xBtnClose").html("<i class=\"fa fa-close\"></i> 닫기").removeClass("close");
  $("#buttons a ").addClass("btn btn-xs btn-default");
});

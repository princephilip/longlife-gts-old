var _Xn = new Object();

// 메뉴를 그립니다. 2레벨 부터 그리기 - 1단 버전
_Xn.SetTopMenu = function() {
  var thtml = "";
  for(var i=0; i<_MyPgmList.length; i++) {
    thtml = "";
    if(_MyPgmList[i].MENU_LEVEL!="1") {
      if(_MyPgmList[i].MENU_LEVEL=="2") {
        if( $("#main-menu").length == 0 ) {
          thtml = "<ul id=\"main-menu\" class=\"nav navbar-nav\"></ul>";

          $(".navbar .navbar-collapse").append(thtml);
        }

        thtml = "<li class=\"dropdown-mainmenu\">";
        if(_MyPgmList[i].CHILD_MENU_CNT!="0") {
          thtml += "<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">"+_MyPgmList[i].PGM_NAME+"</a>";
        } else {
          thtml += "<a href=\"#\" data-pgmcode=\""+_MyPgmList[i].PGM_CODE+"\" data-pgmname=\""+_MyPgmList[i].PGM_NAME+"\" data-openurl=\""+_MyPgmList[i].OPEN_URL+"\">"+_MyPgmList[i].PGM_NAME+"</a>";
        }
        thtml += "<ul id=\"main-ddmenu-"+_MyPgmList[i].PGM_CODE.toLowerCase()+"\" class=\"dropdown-menu\"></ul>";
        thtml += "</li>";
        
        $("#main-menu").append( thtml );
      } else {
        var tObjNm = "";
        if(_MyPgmList[i].MENU_LEVEL=="3") {
          tObjNm = "#main-ddmenu-"+_MyPgmList[i].PARENT_SORT_CODE.toLowerCase();
        } else {
          tObjNm = "#main-ddmenu-"+_MyPgmList[i].PARENT_SORT_CODE.toLowerCase();
        }

        if(_MyPgmList[i].CHILD_MENU_CNT>0) {
          thtml = "<li class=\"dropdown-submenu\">";
          thtml += "<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">"+_MyPgmList[i].PGM_NAME+"</a>";
          thtml += "<ul id=\"main-ddmenu-"+_MyPgmList[i].SORT_CODE.toLowerCase()+"\" class=\"dropdown-menu\"></ul>";
          thtml += "</li>";
        } else {
          if(_MyPgmList[i].PGM_CODE=="PT0201020"||_MyPgmList[i].PGM_CODE=="PT0301010") {
            thtml = "<li>";
            thtml += "<a href=\"/XG_Slick1.do?pcd="+_MyPgmList[i].PGM_CODE+"\" data-pgmcode=\""+_MyPgmList[i].PGM_CODE+"\" data-pgmname=\""+_MyPgmList[i].PGM_NAME+"\" data-openurl=\""+_MyPgmList[i].OPEN_URL+"\">"+_MyPgmList[i].PGM_NAME+"</a>";
            thtml += "</li>";
          } else {
            thtml = "<li>";
            thtml += "<a href=\"javascript:alert('작업중입니다.');\" data-pgmcode=\""+_MyPgmList[i].PGM_CODE+"\" data-pgmname=\""+_MyPgmList[i].PGM_NAME+"\" data-openurl=\""+_MyPgmList[i].OPEN_URL+"\">"+_MyPgmList[i].PGM_NAME+"</a>";
            thtml += "</li>";
          }
        }

        $(tObjNm).append(thtml);
      }
    }
  }
}
// var _X_AjaxResult;
// var _Row_Separate = "˛";  //String.fromCharCode(6); // Record Separator 
// var _Col_Separate = "¸";  //String.fromCharCode(5); // Field  Separator
// var _Field_Separate = "ª";
// var _Field_Separate2 = "º";
// var _Param_Separate = String.fromCharCode(7); // Record Separator

// var _XP = new Object();

// _XP.updateAll = function() {

//   /*=== Insert 구문 생성 ===*/
//   for ( var i = 0, ii = rows.updated.length; i < ii; i++ ) {
//     sqlDatas += _Col_Separate + "I" + _Field_Separate + x_GetGridPureData(this, rows.updated[i]);
//   }  
// }

// _XP.GetSQLData = function(a_obj) {
//   var sqlDatas = "";

//   for ( var i = 0, i < a_obj.length, i++ ) {

//   }
// }
$(function() {
  $("#btn-password").on("click", function() {
    if(!$.trim($("#cur_pass").val())) {
      alert('현재 비밀번호를 입력하세요.');

      $("#cur_pass").val('');
      $("#cur_pass").focus();
      return false;
    }

    // 현재 비밀번호 체크 
    var ls_pass = _X.XmlSelect('com', 'COMMON', 'CHK_CUR_PASS', [_CompanyCode, _UserID, $("#cur_pass").val()], 'json2');
    if(ls_pass[0].result!="OK") {
      alert("현재 비밀번호가 일치하지 않습니다.");

      $("#cur_pass").focus();
      return false;
    }

    if(!$.trim($("#pass1").val())) {
      alert('새 비밀번호를 입력하세요.');

      $("#pass1").val('');
      $("#pass1").focus();
      return false;
    }

    if(!pf_chkPwd( $.trim($('#pass1').val()) )) { 
      alert('비밀번호를 확인하세요.\n(영문,숫자를 혼합하여 8~15자 이내)');    

      $('#pass1').focus(); 
      return false;
    }

    if(!$.trim($("#pass2").val())) {
      alert('새 비밀번호 확인을 입력하세요.');

      $("#pass2").val('');
      $("#pass2").focus();
      return false;
    }

    if( $.trim($('#pass1').val())!=$.trim($('#pass2').val()) ) { 
      alert('[새 비밀번호]와 [새 비밀번호 확인]이 일치하지 않습니다.');    

      $('#pass1').focus(); 
      return false;
    }

    // 비밀번호 수정 
    var ls_result = _X.ExecProc("com", "SP_PT_CHANGE_PASSWORD",  new Array(_CompanyCode, _UserID, $.trim($('#cur_pass').val()), $.trim($('#pass1').val())));

    if(ls_result < 1) {
      _X.MsgBox("확인", "비밀번호 수정 중 오류가 발생했습니다. 담당자에게 문의하세요.");
      return;
    } else {
      _X.MsgBox("확인", "비밀번호가 수정됐습니다.");
      $("#pwModal").modal("hide");
    }
  });
});

function pf_chkPwd(str){
  var reg_pwd = /^.*(?=.{8,15})(?=.*[0-9])(?=.*[a-zA-Z]).*$/;

  if(!reg_pwd.test(str)){ 
    return false;
  }

  return true;
}


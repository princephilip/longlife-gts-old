<%@ page contentType="text/html; charset=utf-8"%>

<!DOCTYPE html>
<html lang="ko">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
  <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
  
  <title>협력업체 등록신청 1/3단계</title>

  <link rel="stylesheet" href="/Mighty/3rd/font-awesome/css/all.css">
  <link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="/partner/join.css">


  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <script type="text/javaScript" src="/Mighty/3rd/jQuery/js/jquery-1.11.2.js"></script>
  <script type="text/javaScript" src="/Mighty/3rd/jQuery/ui/jquery.ui.core.min.js"></script>
  <script type="text/javaScript" src="/Mighty/3rd/jQuery/jquery-ui-1.11.4/jquery-ui.js"></script>

  <script type="text/javascript" src="/Mighty/js/mighty-2.0.0.js"></script>
  <script type="text/javascript" src="/Mighty/js/mighty-comm-1.2.8.js"></script>

  <script type="text/javascript" src="/Mighty/3rd/crypto/base64.js"></script>

  <script type="text/javaScript" language="javascript">
    var pf_close = function() {
      window.close();
    }

    var pf_chk_form = function() {
      var tVal = $("#cust_code1").val().trim();
      if(!tVal||tVal.length!=3) {
        alert("사업자등록번호가 잘못입력되었습니다.");
        $("#cust_code1").focus();

        return false;
      }

      var tVal = $("#cust_code2").val().trim();
      if(!tVal||tVal.length!=2) {
        alert("사업자등록번호가 잘못입력되었습니다.");
        $("#cust_code2").focus();

        return false;
      }

      var tVal = $("#cust_code3").val().trim();
      if(!tVal||tVal.length!=5) {
        alert("사업자등록번호가 잘못입력되었습니다.");
        $("#cust_code3").focus();

        return false;
      }

      if(!pf_chk_val("cust_name", "[회사명]을")) return false;
      if(!pf_chk_val("owner",     "[대표자]를")) return false;
      if(!pf_chk_val("condition", "[업태]를")) return false;
      if(!pf_chk_val("category",  "[업종]을")) return false;
      if(!pf_chk_val("tel",       "[연락처]를")) return false;
      if(!pf_chk_val("addr",      "[주소]를")) return false;
      if(!pf_chk_val("intro",     "[업체소개]를")) return false;

      return true;
    }

    var pf_chk_val = function(a_obj, a_title) {
      var tVal = $("#"+a_obj).val().trim();
      if(!tVal) {
        alert(a_title+" 입력해야 합니다.");
        $("#"+a_obj).focus();

        return false;
      } 

      return true;
    }
  </script>

</head>

<body>


  <div class="container">

    <div class="row">
      <div class="col-xs-12">
        <h2><i class="fa fa-handshake-o"></i> 협력업체 등록신청 <span class="small">- 회사정보입력(1/3)</span></h2>
      </div>
    </div>

    <hr />
    
    <form action="/partner/join2.jsp" class="form-horizontal" onsubmit="return pf_chk_form();">
      <input type="hidden" id="join_id" name="join_id" value="<%=xgov.core.util.XUtility.GetCurrentDate("yyyyMMddHHmmssSSS")%>" />
      <div class="form-group form-inline">
        <label for="cust_name" class="col-xs-3 control-label text-right">*사업자등록번호</label>
        <div class="col-xs-9">
          <input type="text" class="form-control input-sm" id="cust_code1" name="cust_code1" maxlength="3"> - 
          <input type="text" class="form-control input-sm" id="cust_code2" name="cust_code2" maxlength="2"> - 
          <input type="text" class="form-control input-sm" id="cust_code3" name="cust_code3" maxlength="5"> 
        </div>
      </div>
      <div class="form-group">
        <label for="cust_name" class="col-xs-2 control-label text-right">회사명</label>
        <div class="col-xs-10">
          <input type="text" class="form-control input-sm" id="cust_name" name="cust_name" >
        </div>
      </div>
      <div class="form-group">
        <label for="owner" class="col-xs-2 control-label text-right">대표자</label>
        <div class="col-xs-4">
          <input type="text" class="form-control input-sm" id="owner" name="owner" >
        </div>
      </div>
      <div class="form-group">
        <label for="condition" class="col-xs-2 control-label text-right">업태</label>
        <div class="col-xs-4">
          <input type="text" class="form-control input-sm" id="condition" name="condition" >
        </div>
        <label for="category" class="col-xs-2 control-label text-right">업종</label>
        <div class="col-xs-4">
          <input type="text" class="form-control input-sm" id="category" name="category" >
        </div>
      </div>
      <div class="form-group">
        <label for="tel" class="col-xs-2 control-label text-right">연락처</label>
        <div class="col-xs-10">
          <input type="text" class="form-control input-sm" id="tel" name="tel" >
        </div>
      </div>
      <div class="form-group">
        <label for="addr" class="col-xs-2 control-label text-right">주소</label>
        <div class="col-xs-10">
          <input type="text" class="form-control input-sm" id="addr" name="addr" >
        </div>
      </div>
      <div class="form-group">
        <label for="intro" class="col-xs-2 control-label text-right">업체소개<br /><span class="small">(100자내외)</span></label>
        <div class="col-xs-10">
          <textarea class="form-control input-sm" rows="3" id="intro" name="intro" maxlength="600"></textarea>
        </div>
      </div>

      <div class="row pt10">
        <div class="col-xs-offset-2 col-xs-4">
          <button type="submit" class="btn btn-warning btn-block">
            <i class="fa fa-save"></i> 저장 및 다음단계 
          </button>
        </div>
        <div class="col-xs-3">
          <button class="btn btn-default btn-block" onclick="pf_close()">
            <i class="fa fa-close"></i> 닫 기
          </button>
        </div>
      </div>
    </form>
        

    </div><!-- /card-container -->
  </div><!-- /container -->

</body>
 
</html>

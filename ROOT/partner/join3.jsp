<%@ page contentType="text/html; charset=utf-8"%>

<% 
  String pJoin_id   = request.getParameter("join_id");
  String pFile_id   = request.getParameter("file_id");
  String pFile_name = request.getParameter("file_name");
  String pFile_type = request.getParameter("file_type");
  String pFile_path = request.getParameter("file_path");

  String _RSep = "˛";
  String _CSep = "¸";
  String tResult = "";
  Integer tCnt = 0;

  try {
    String custparams = pJoin_id+_CSep+pFile_id+_CSep+pFile_name+_CSep+pFile_type+_CSep+pFile_path;
    tCnt = xgov.core.dao.XDAO.ExecSql(request, "pt", "PT0101010", "UPDATE_FILE", custparams, _CSep); 
  } catch (Exception e) {
    e.printStackTrace();
  }

%>

<!DOCTYPE html>
<html lang="ko">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
  <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
  
  <title>협력업체 등록신청 3/3단계</title>

  <link rel="stylesheet" href="/Mighty/3rd/font-awesome/css/all.css">
  <link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="/partner/join.css">


  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <script type="text/javaScript" src="/Mighty/3rd/jQuery/js/jquery-1.11.2.js"></script>
  <script type="text/javaScript" src="/Mighty/3rd/jQuery/ui/jquery.ui.core.min.js"></script>
  <script type="text/javaScript" src="/Mighty/3rd/jQuery/jquery-ui-1.11.4/jquery-ui.js"></script>

  <script type="text/javascript" src="/Mighty/js/mighty-2.0.0.js"></script>
  <script type="text/javascript" src="/Mighty/js/mighty-comm-1.2.8.js"></script>

  <script type="text/javascript" src="/Mighty/3rd/crypto/base64.js"></script>

  <script type="text/javaScript" language="javascript">
    var pf_close = function() {
      window.close();
    }
  </script>

</head>

<body>
  
  <div class="container">

    <div class="row">
      <div class="col-xs-12">
        <h2><i class="fa fa-handshake-o"></i> 협력업체 등록신청 <span class="small">- 신청완료(3/3)</span></h2>
      </div>
    </div>

    <hr />
    
    
    <div class="alert alert-success text-center" role="alert">
      협력업체 신청이 완료되었습니다.<br />
      담당자 검토 후, 연락드리겠습니다.
    </div>

    <div class="row pt10">
      <div class="col-xs-offset-4 col-xs-4">
        <button class="btn btn-default btn-block" onclick="pf_close()">
          <i class="fa fa-close"></i> 닫 기
        </button>
      </div>
    </div>
  
        

    </div><!-- /card-container -->
  </div><!-- /container -->

</body>
 
</html>

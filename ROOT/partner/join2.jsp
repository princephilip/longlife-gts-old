<%@ page contentType="text/html; charset=utf-8"%>

<% 
  String pJoin_id    = request.getParameter("join_id");
  String pCust_code1 = request.getParameter("cust_code1");
  String pCust_code2 = request.getParameter("cust_code2");
  String pCust_code3 = request.getParameter("cust_code3");
  String pCust_name  = request.getParameter("cust_name");
  String pOwner      = request.getParameter("owner");
  String pCategory   = request.getParameter("category");
  String pCondition  = request.getParameter("condition");
  String pTel        = request.getParameter("tel");
  String pAddr       = request.getParameter("addr");
  String pIntro      = request.getParameter("intro");

  String _RSep = "˛";
  String _CSep = "¸";
  String tResult = "";
  Integer tCnt = 0;

  try {
    String gridparams  = pCust_code1+_CSep+pCust_code2+_CSep+pCust_code3;
    tResult = xgov.core.dao.XDAO.XmlSelect(request, "array", "pt", "PT0101010", "CNT_DUP", gridparams, "all", _RSep, _CSep); //.split(_RSep).split(_CSep);
  } catch (Exception e) {
    e.printStackTrace();
  }

  // 저장 
  if(tResult.equals("0")) {
    try {
      String custparams = pJoin_id+"_"+pCust_code1+pCust_code2+pCust_code3+_CSep+pCust_code1+_CSep+pCust_code2+_CSep+pCust_code3+_CSep+pCust_name+_CSep+pOwner+_CSep+pCategory+_CSep+pCondition+_CSep+pTel+_CSep+pAddr+_CSep+pIntro;
      tCnt = xgov.core.dao.XDAO.ExecSql(request, "pt", "PT0101010", "JOIN_CUST", custparams, _CSep); 
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
%>

<!DOCTYPE html>
<html lang="ko">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
  <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
  
  <title>협력업체 등록신청 2/3단계</title>

  <link rel="stylesheet" href="/Mighty/3rd/font-awesome/css/all.css">
  <link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="/partner/join.css">


  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <script type="text/javaScript" src="/Mighty/3rd/jQuery/js/jquery-1.11.2.js"></script>
  <script type="text/javaScript" src="/Mighty/3rd/jQuery/ui/jquery.ui.core.min.js"></script>
  <script type="text/javaScript" src="/Mighty/3rd/jQuery/jquery-ui-1.11.4/jquery-ui.js"></script>

  <script type="text/javascript" src="/Mighty/js/mighty-2.0.0.js"></script>
  <script type="text/javascript" src="/Mighty/js/mighty-comm-1.2.8.js"></script>

  <script type="text/javascript" src="/Mighty/3rd/crypto/base64.js"></script>

  <script type="text/javaScript" language="javascript">
    var pf_close = function() {
      window.close();
    }
  </script>

</head>

<body>
  
  <div class="container">

    <div class="row">
      <div class="col-xs-12">
        <h2><i class="fa fa-handshake-o"></i> 협력업체 등록신청 <span class="small">- 관련문서 첨부(2/3)</span></h2>
      </div>
    </div>

    <hr />
    
    <% if(!tResult.equals("0")) { %>
      <div class="alert alert-danger text-center" role="alert">
        이미 등록된 사업자번호 입니다.<br />
        세방테크 담당자에게 문의하세요.
      </div>

      <div class="row pt10">
        <div class="col-xs-offset-4 col-xs-4">
          <button class="btn btn-default btn-block" onclick="pf_close()">
            <i class="fa fa-close"></i> 닫 기
          </button>
        </div>
      </div>
    <% } else { %>
      <div class="row">
        <div class="col-xs-12">
          <iframe id='if_uploadmodal' src='about:blank' class='iframe_popup' frameborder=0 marginwidth=0 marginheight=0></iframe>
        </div>
      </div>
      
      <div class="row pt10">
        <div class="col-xs-offset-2 col-xs-4">
          <button type="button" onclick="pf_file_upload()" class="btn btn-warning btn-block">
            <i class="fa fa-save"></i> 저장 및 다음단계 
          </button>
        </div>
        <div class="col-xs-3">
          <button class="btn btn-default btn-block" onclick="pf_close()">
            <i class="fa fa-close"></i> 닫 기
          </button>
        </div>
      </div>

      <form id="upload_form" method="post" action="/partner/join3.jsp" class="form-horizontal">
        <input type="hidden" id="join_id" name="join_id" value="<%=(pJoin_id+"_"+pCust_code1+pCust_code2+pCust_code3)%>" />
        <input type="hidden" id="file_id" name="file_id" value="" />
        <input type="hidden" id="file_name" name="file_name" value="" />
        <input type="hidden" id="file_type" name="file_type" value="" />
        <input type="hidden" id="file_path" name="file_path" value="" />
      </form>
    <% } %>



        

    </div><!-- /card-container -->
  </div><!-- /container -->

  <% if(tResult.equals("0")) { %>
  <script type="text/javascript">
    $(function() {
      var fileId = "<%=pCust_code1+pCust_code2+pCust_code3%>"; // dg_1.GetItem(a_row, "FILE_ID");

      var sUrl = "/mighty/fileupload.do?upcnt=1&subdir=pt&obj=FILE_ID&fileid="+fileId;
      $("#if_uploadmodal").attr("src", sUrl);
    });

    $('#if_uploadmodal').on("load", function() {
      $("#if_uploadmodal").contents().find("body").css("background-color", "#f4f3ef");
    });

    var pf_file_upload = function() {
      $("#if_uploadmodal").get(0).contentWindow.pf_submit()  
    }

    var x_FileUploaded = function(obj, ReturnValue) {
      if(ReturnValue==null){
        return;
      }

      switch(obj) {
        case "FILE_ID":
          $("#file_id").val(ReturnValue[0].atchFileId);
          $("#file_name").val(ReturnValue[0].orignlFileNm);
          $("#file_type").val(ReturnValue[0].fileExtsn);
          $("#file_path").val(ReturnValue[0].fileStreCours);
          $("#upload_form").submit();

          break;
      }
    }
    
  </script>
  <% } %>
</body>
 
</html>

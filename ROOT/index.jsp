<%--
  Class Name : index.jsp
  Description : 최초화면으로 메인화면으로 이동한다.(system)
  Modification Information

      수정일         	  수정자                        수정내용
  -------    --------    ---------------------------
  2013.02.17  박영찬       경량환경 버전 생성
 
    author   : 엑스인터넷정보
    since    : 2013.02.17
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page import ="xgov.core.vo.XLoginVO"%>
<%@ page import ="xgov.core.env.XConfiguration"%>

<%
  XLoginVO user = (XLoginVO)session.getAttribute("loginVO");
  String   go_url = "/com/egovLoginUsr.do";

  if(user!=null && user.getuseYesNo().equals("Y")) {
    go_url = "/com/actionMain.do";
  }
%>
<%-- 
<jsp:forward page="/com/egovLoginUsr.do"/>
--%>
<script type="text/javaScript">
	//redirect 시 최상위로..
	if (top.location.href != self.location.href)  top.location.href = self.location.href;
	document.location.href="<%= go_url %>";
</script> 

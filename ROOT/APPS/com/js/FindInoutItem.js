function xc_DAO_Retrieve()
{

	var ls_code =_Caller._FindCode.findcode;
	var ls_incust = ls_code[0];
	var ls_build = ls_code[1];
	
	if(FIND_CODE.value == "" ||FIND_CODE.value == null){
		var ls_item = '%';
	}else{
		var ls_item = FIND_CODE.value;
	}

   dataXML = "<Datas><Params><BUILD_CODE>"+ls_build+"</BUILD_CODE><INCUST_CODE>"+ls_incust+"</INCUST_CODE><ITEM_CODE>"+ls_item+"</ITEM_CODE></Params></Datas>";
   _X.WmsCall("com","INVENTORY_LIST ","xml", dataXML);
   
   dg_1.GridApp().addLoadingBar();
   dg_1.GridRoot().removeAll();
   dg_1.GridRoot().resetChangedData();
   dg_1._CurrentRow = -1;
   
   if(_X_AjaxResult.result == "ERROR"){
   	  _X.MsgBox('확인', '재고가 없거나 통신 오류입니다.'); 
  		dg_1.GridApp().removeLoadingBar();
  		return;
  	}
  	
	_X_AjaxResult.datas = _X_AjaxResult.datas.replace(/\:{}/gi, ':""'); 
   dg_1.GridApp().setData(_X_AjaxResult.datas);
   dg_1.GridApp().removeLoadingBar();
   
   
}

var _Caller = window.dialogArguments;

//화면 디자인관련 요소들 초기화 작업
function x_InitForm()
{
  
  div_criteria.innerHTML = _X.Button("xBtnRetrieve", "x_DAO_Retrieve()", "/Theme/images/btn/x_retrieve")
                         + _X.Button("xBtnClose", "x_Close()", "/Theme/images/btn/x_close")
   
	if(typeof(xc_InitForm)!="undefined"){xc_InitForm();return;}
  
	_X.Grid(grid_1, "dg_1", "100%", "100%", "com", _POP_CODE, "FindCode|" + _POP_CODE);
	
	
	rMateGridInit();
}

function x_DAO_Retrieve()
{
	if(typeof(xc_DAO_Retrieve)!="undefined"){xc_DAO_Retrieve();return;}
	
	dg_1.Retrieve(new Array(_POP_PAR));
}

function x_DAO_Save(){}
function x_DAO_Insert(row){}
function x_DAO_Delete(row){}
function x_DAO_Duplicate(row){}
function x_Duplicate_After(obj, rowIdx){}
function x_DAO_Excel(){dg_1.ExcelExport();}

function xe_GridDataLoad(a_obj){

}

function xe_EditChanged(a_obj, a_val, a_label, a_cobj)
{
	if(typeof(xc_EditChanged)!="undefined"){xc_EditChanged(a_obj, a_val, a_label, a_cobj);return;}
	
}

function xe_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey)
{
}

function xe_GridRowFocusChange(a_obj, a_row, a_col){}

function xe_GridDoubleClick(a_obj, a_ctrlKey, a_altKey, a_shiftKey, a_stageX, a_stageY){}

function xe_GridItemDoubleClick(a_obj, a_row, a_col){
}


function x_Close() {window.close();}

function xe_GridLayoutComplete(a_obj){
	if(typeof(a_obj)!="undefined") {
		setTimeout("x_DAO_Retrieve()",100);
	}

}
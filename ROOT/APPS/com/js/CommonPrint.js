﻿
function includeJavaScript(file)   
{     
  //var script   = document.createElement('script');   
  //script.src   = file;   
  //script.type  = 'text/javascript';   
  //script.defer = true;   
  //document.getElementsByTagName('head').item(0).appendChild(script);  
  document.write("<script type='text/javascript'  src='" + _web_context.path + file + "' charset='utf-8'></script>");
}  

includeJavaScript('/RexServer30/rexscript/rexpert.js');
includeJavaScript('/RexServer30/rexscript/rexpert_properties.js');


function fn_rexprint(a_rpt, a_div){

	var oReport = GetfnParamSet();
	
	var ls_Data = "";
	var l_pos = 0;
	var l_length = 0;
	var ls_rptname = ""; 
	var l_rebcnt = 0;
	
	// 박종현(추가)
	// 1: viewer, 2:바로 프린트 3:파일로 내려받기, 4: 특정경로에 파일로 내려받기(결재상신)
	if(a_div == "" || a_div == null) a_div = '1';
	
	for(var i=0; i<a_rpt.length; i++){

		var ls_reb = a_rpt[i].rebfile;
		
		if(ls_rptname != ls_reb){
			// 필수 - 레포트 파일명 
			oReport.reb(ls_reb).rptname = ls_reb;
		}
		oReport.reb(ls_reb).con(a_rpt[i].con).type = "memo";
		oReport.reb(ls_reb).con(a_rpt[i].con).datatype = "xml";
	
		ls_Data = _X.XmlSelect(a_rpt[i].subsystem, a_rpt[i].queryfile, a_rpt[i].queryid, a_rpt[i].args, "xml");
		
		l_pos = ls_Data.indexOf(">");
		l_length = ls_Data.length;
		ls_Data = ls_Data.substr(l_pos + 1, l_length - l_pos); 
		
		var sData = "";
		sData = ls_Data; 	
		oReport.reb(ls_reb).con(a_rpt[i].con).dataset(a_rpt[i].dataset).xpath = "GridData/RowData"; 
		oReport.reb(ls_reb).con(a_rpt[i].con).data = sData;
		
		ls_rptname = ls_reb;
	
	}
	
	if(a_div == '1'){
		oReport.open();
	}else if(a_div == '2'){
		oReport.print(true, 1, -1, 1, ""); // 다이얼로그표시유무, 시작페이지, 끝페이지, 카피수, 옵션
		//oReport.printdirect("HP LaserJet 3050" , 260, 1, -1, 1, "");	// [프린터명],[트레이아이디],[시작페이지],[끝페이지],[출력매수],[옵션]
	}else if(a_div == '3'){
		oReport.save(true, "xls", "c:\\print.xls", 1, -1, ""); // 다이얼로그표시유무, 파일타입, 파일명, 시작페이지, 끝페이지, 카피수, 옵션
	}else if(a_div == '4'){
		//alert(mytop._CPATH+'^^^'+mytop._rootFileUpload+ "REXPERT/test.htm");
		//oReport.save(false, "htm", "c:\\test.htm", 1, -1, "");
		//oReport.save(false, "htm", mytop._rootFileUpload+ "REXPERT/test1.htm", 1, -1, ""); 
		oReport.save(true, "htm", "http://fiddle.jshell.net:9000/APPS/cs/report/test11.pdf", 1, -1, "");
	}else{
		oReport.open();
	}
}

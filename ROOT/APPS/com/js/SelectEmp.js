//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 : SelectEmp
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
//
//===========================================================================================
var _DeptID = "";

function x_InitForm2(){
  _DeptID = _Caller._FindCode.findcode[0];

 	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "com", "SelectEmp", "SelectEmp|FIND_MEMBER", false, false);

  var la_DEPT = _X.XmlSelect("com", "COMMON", "CODE_DEPT_AUTHORITY_R01" , new Array(mytop._UserID), 'json2');
  _X.DDLB_SetData(S_DEPT_ID, la_DEPT, null, true, (_X.IsHeadoffice() ? true : false));
  S_DEPT_ID.value = mytop._DeptCode;
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
  if(ab_allcompleted){
		dg_1.SetCheckBar(true);
  	setTimeout("x_DAO_Retrieve()",100);
  }
}

function x_DAO_Retrieve2(){
  dg_1.Retrieve([_DeptID, S_DEPT_ID.value]);
}

function xe_EditChanged2(a_obj, a_val){
  switch(a_obj.id) {
    case  "S_DEPT_ID":
          x_DAO_Retrieve();
          break;
  }
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
  return 100;
}

function x_DAO_ChkErr2(){
  return true;
}

function x_DAO_Saved2(){
  return 100;
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}

function x_Insert_After2(a_dg, rowIdx){
}

function x_DAO_Duplicate2(a_dg, row){
  return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg){
  return 100;
}

function x_DAO_Excel2(a_dg){
  return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
  return 1;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
  return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridDataLoad2(a_dg){

}

function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){
}

function x_ReceivedCode2(a_retVal){
  switch(_FindCode.finddiv) {
    case  "S_DEPT_ID" :
          S_DEPT_ID.value = _FindCode.returnValue.DEPT_ID;
          x_DAO_Retrieve();
          break;
  }
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){
}

function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){
}

function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){
}

function xe_ChartPointClick2(a_dg, a_event){
}

function xe_ChartPointSelect2(a_dg, a_event){
}

function xe_ChartPointMouseOver2(a_dg){
}

function xe_ChartPointMouseOut2(a_dg, a_event){
}

function x_Close2() {

  if(_IsModal || _IsPopup) {
    window.close();
  }else{
    if(_Caller.$("#findmodal").length>0?_Caller.$("#findmodal").dialog("close"):window.close());
  }
}

// 지사찾기
function uf_findDept(a_param) {
  _X.FindDeptCode(a_param,'');
}



function x_Confirm(){
	var cRows = dg_1.GetCheckedRows();
	if(cRows.length == 0) {
		_X.MsgBox('사원을 선택 하세요.');
		return;
	}

	_Caller.pf_addcall(window.dg_1);

	x_Close2();
}

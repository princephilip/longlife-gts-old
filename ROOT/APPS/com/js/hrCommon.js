/****************************************************************
 *
 * 파일명 : mighty-find-1.0.0.js
 * 설  명 : 코드찾기 popup JavaScript
 *
 *    수정일      수정자     Version        Function 명
 * ------------    ---------   -------------  ----------------------------
 * 2013.02.10    김양열       1.0.0            최초생성
 * 2014.10.10    이상규       1.0.1            1차수정
 *
 */
var _HR;
if (!_HR) {
	_HR = {};
	_HR.DHR_Data = {};
	_HR.DSM_Data = {};
}

// 인사공통코드 dropdown 리턴
_HR.DHR = function (selectId, param1, param2, param3, param4, param5, param6, param7, param8, param9, param10) {
	var key = selectId + (param1 || "") + (param2 || "") + (param3 || "") + (param4 || "") + (param5 || "") + (param6 || "") + (param7 || "") + (param8 || "") + (param9 || "") + (param10 || "");

	if ( !_HR.DHR_Data[key] ) {
		_HR.DHR_Data[key] = _X.XS("hr", "HR_COMMON", selectId, [param1 || "", param2 || "", param3 || "", param4 || "", param5 || "", param6 || "", param7 || "", param8 || "", param9 || "", param10 || ""], "json2");
	}

	return _HR.DHR_Data[key];
}

// 공통코드 dropdown 리턴
_HR.DSM = function (hcode, sysId) {
	var key = (sysId || "") + (hcode || "");

	if ( !_HR.DSM_Data[key] ) {
		_HR.DSM_Data[key] = _X.XS('com', 'COMMON', 'SM_COMCODE_D', [sysId || "HR", hcode || ""], "json2");
	}

	return _HR.DSM_Data[key];
}

// 수당별 기준금액 찾기
_HR.FindAllowBasicAmt = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=hr&fcd=FindAllowBasicAmt&fnm="+encodeURIComponent('수당별 기준금액 찾기')+"&msize=0&fgrid=FindAllowBasicAmt|FindAllowBasicAmt|FindAllowBasicAmt";
	_X.OpenFindWin3(a_win, param, '수당별 기준금액 찾기', 1055, 500);
};

// 기초수당 변경
_HR.FindBaseAllowContChange = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=hr&fcd=FindBaseAllowContChange&fnm="+encodeURIComponent('기초수당 변경')+"&msize=0&fgrid=FindBaseAllowContChange|FindBaseAllowContChange|FindBaseAllowContChange";
	_X.OpenFindWin3(a_win, param, '기초수당 변경', 1100, 600);
}

// 고정수당 일괄등록
_HR.FindFixAllowBundleInsert = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=hr&fcd=FindFixAllowBundleInsert&fnm="+encodeURIComponent('고정수당 일괄등록')+"&msize=0&fgrid=FindFixAllowBundleInsert|FindFixAllowBundleInsert|FindFixAllowBundleInsert";
	_X.OpenFindWin3(a_win, param, '고정수당 일괄등록', 700, 600);
};

// 변동수당 일괄등록
_HR.FindVarAllowBundleInsert = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=hr&fcd=FindVarAllowBundleInsert&fnm="+encodeURIComponent('변동수당 일괄등록')+"&msize=0&fgrid=FindVarAllowBundleInsert|FindVarAllowBundleInsert|FindVarAllowBundleInsert";
	_X.OpenFindWin3(a_win, param, '변동수당 일괄등록', 700, 600);
};

// ------------------------------------------ 현재 줄 이하 20171219 이전 소스들 ---------------------------------------------------------------

// var _coPAY_DIV = [{"code":"","label":" "},{"code":"1","label":"지급"},{"code":"2","label":"공제"}];
// var _coUSE_YN = [{'code':'Y','label':'예'},{'code':'N','label':'아니오'}];

// _HR.FindPgmCode = function(a_win, a_find_div, a_findstr, a_findcode) {
// 	a_win._FindCode.finddiv = a_find_div;
// 	a_win._FindCode.findstr = a_findstr;
// 	a_win._FindCode.findcode = a_findcode;
// 	var param = "sys=sm&fcd=FindPgmCode&fnm="+encodeURIComponent('프로그램코드 찾기')+"&msize=0&fgrid=FindPgmCode|SmFindCode|FindPgmCode";
// 	_X.OpenFindWin3(a_win, param, '프로그램코드 찾기', 500, 550);
// };
// _HR.findPgmCode = _HR.FindPgmCode;

// //오픈시트
// //remove
// _HR.FindWin3 = function(a_win, a_find_div, a_findstr, a_findcode, a_findtitle, a_param, a_width, a_height){
// 	a_win._FindCode.finddiv = a_find_div;
// 	a_win._FindCode.findstr = a_findstr;
// 	a_win._FindCode.findcode = a_findcode;
// 	var ls_params = a_param.split('|');
// 	var param = "sys=hr&fcd="+ls_params[0]+"&fnm="+encodeURIComponent(a_findtitle)+"&msize=0&fgrid="+ls_params[0]+"|"+ls_params[1]+"|"+ls_params[2];
// 	_X.OpenFindWin3(a_win, param, a_findtitle, a_width, a_height);
// };
// _HR.findWin3 = _HR.FindWin3;


// /*
// _X.FindCode(window, '사원 찾기', 'hr', 'F_EMP_NO_NAME', '', '%', 'FindEmpNoName|HrFindCode|FindEmpNoName', 850, 650); //부서별 사원찾기 가능
// _X.FindCode(window, '부서코드 찾기', 'hr', 'F_DEPT_CODE', '', '%', 'FindDeptCode|HrFindCode|FindDeptCode', 500, 650);
// _X.FindCode(window, '부서코드 찾기', 'hr', 'F_DEPT_CODE', '', '%', 'FindAsstDept|HrFindCode|FindAsstDept', 500, 650); //비품전출등록 부서찾기
// _X.FindCode(window, '직위코드 찾기', 'hr', 'F_GRADE_CODE', '', '', 'F_GradeCode|HrFindCode|F_GradeCode', 350, 550);
// _X.FindCode(window, '직무코드 찾기', 'hr', 'F_JOBKIND_CODE', '', '', 'F_Jobkind|HrFindCode|F_Jobkind', 430, 550);
// _X.FindCode(window, '금융기관 찾기', 'hr', 'F_BANK_CODE', '', '%', 'FindBankCode|HrFindCode|FindBankCode', 455, 650);
// _X.FindCode(window, '학교 찾기', 'hr', 'F_SCHOOL_CODE', '', new Array(ls_school_car_code), 'F_SchoolCode|HrFindCode|F_SchoolCode', 460, 550);
// _X.FindCode(window, '전공/부전공 찾기', 'hr', 'F_MAJOR_CODE', '', '', 'F_MajorCode|HrFindCode|F_MajorCode', 380, 550);
// _X.FindCode(window, '발령문서 찾기', 'hr', 'F_ORDER_NO', '', '%', 'FindOrderNo|HrFindCode|FindOrderNo', 570, 650);
// _X.FindCode(window, '보증보험코드 찾기', 'hr', 'F_INSURENCE_AMT', '', '%', 'FindInsuComp|HrFindCode|FindInsuComp', 450, 350);
// _X.FindCode(window, '거래처 찾기', 'hr', 'F_CUST_CODE', '', '%', 'FindCustCode|HrFindCode|FindCustCode', 850, 650);
// _X.FindCode(window, '계정과목 찾기', 'hr', 'F_ACNT_CODE', '', '%', 'FindAcntCode|HrFindCode|FindAcntCode', 850, 650);
// _X.FindCode(window, '계정과목 찾기', 'hr', 'F_ACNT_CODE', '', '%', 'FindAcntCode2|HrFindCode|FindAcntCode2', 400, 400); 자산분류코드등록
// _X.FindCode(window, '조직도문서번호 찾기', 'hr', 'F_ORGA_SHEET', '', new Array(top._CompanyCode, 'Y'), 'F_OrgaSheet|HrFindCode|F_OrgaSheet', 560, 500);
// _HR.FindWin3(window, 'F_ALLOW_CODE', null, '%', '수당항목 찾기', 'FindAllow|HrFindCode|FindAllow', 850, 550);         HR070201
// _HR.FindWin3(window, 'F_ALLOW_CODE2', null, '%', '공제항목 찾기', 'FindAllow2|HrFindCode|FindAllow', 650, 550);       HR070201
// _HR.FindWin3(window, 'F_ALLOW_MASS_ENR', null, '%', '수당일괄등록', 'FindAllow|HrFindCode|FindAllow', 850, 550);		HR070201
// _HR.FindWin3(window, 'F_DEDUCT_MASS_ENR', null, '%', '공제일괄등록', 'FindAllow2|HrFindCode|FindAllow', 650, 550);	HR070201
// _HR.FindWin3(window, 'F_ALLOW_MASS_ENR', null, '%', '월급여변동 수당/공제 일괄등록', 'FindAllow3|HrFindCode|FindAllow', 300, 250);	HR070203
// _HR.FindWin3(window, 'F_SEIZURER_EMP', null, '%', '가압류 사원 찾기', 'SeizurerEmp|HrFindCode|SeizurerEmp', 500, 550);	HR070211
// _HR.FindWin3(window, 'F_SFIELD_CODE', ls_field_code, null, '전문분야 찾기', 'FindSfield|HrFindCode|FindSfield', 300, 250);
// _HR.FindWin3(window, 'F_CERT_KIND_CODE', ls_cert_field_code, null, '자격면허종류 찾기', 'FindCertKind|HrFindCode|FindCertKind', 350, 250);
// _HR.FindWin3(window, 'F_EXCEP_EMP_NO', sle_basicdate.value, '%', '제외대상자 등록', 'FindExcepEmp|HrFindCode|FindExcepEmp', 850, 550);
// _HR.FindWin3(window, 'F_APPL_INFO', new Array(), null, '입사지원 상세내역', 'FindApplInfo|HrFindCode|FindApplInfo', 850, 550);
// _HR.MakeAllow(window, 'F_MAKE_ALLOW', null, new Array(top._CompanyCode, S_SALARY_DIV.value, dg_1.GetItem(dg_1.GetRow(), 'APPLY_YYMM'))); //수당등록
// _HR.FindWin3(window, 'F_BTRIPAPPFORM', new Array(), null, '출장명령서 상세현황', 'F_BtripAppForm|HrFindCode|F_BtripAppForm', 850, 550);
// */

// //가변컬럼 셋팅용 객체
// var Column = function() {
// 	var ls_column = {
// 		fieldName  : "",
// 		width      : 100,
// 		must_input : "N",
// 		visible    : true,
// 		readonly   : false,
// 		editable   : true,
// 		sortable   : true,
// 		header     : {text: ""},
// 		styles     : _Styles_text,
// 		editor     : _Editor_text,
// 		mergeRule  : null,
// 		setFiledName  : function(a_filedName)  {if(a_filedName  != null && a_filedName  != "") this.fieldName  = a_filedName;},
// 		setWidth      : function(a_width)      {if(a_width      != null && a_width      != "") this.width      = a_width;},
// 		setMust_Input : function(a_must_input) {if(a_must_input != null && a_must_input != "") this.must_input = a_must_input;},
// 		setVisible    : function(a_visible)    {if(a_visible    != null && a_visible    != "") this.visible    = a_visible;},
// 		setReadonly   : function(a_readonly)   {if(a_readonly   != null && a_readonly   != "") this.readonly   = a_readonly;},
// 		setEditable   : function(a_editable)   {if(a_editable   != null && a_editable   != "") this.editable   = a_editable;},
// 		setSortable   : function(a_sortable)   {if(a_sortable   != null && a_sortable   != "") this.sortable   = a_sortable;},
// 		setHeader     : function(a_header)     {if(a_header     != null && a_header     != "") this.header     = {text: a_header};},
// 		setStyles     : function(a_styles)     {if(a_styles     != null && a_styles     != "") this.styles     = a_styles;},
// 		setEditor     : function(a_editor)     {if(a_editor     != null && a_editor     != "") this.editor     = a_editor;},
// 		setMergeRule  : function(a_merge)      {if(a_merge      != null && a_merge      != "") this.mergeRule  = a_merge;}
// 	}
// 	return ls_column;
// }

// //가변컬럼 셋팅용 객체생성 후 리턴
// function gf_getColumn(a_filedName, a_width, a_must_input, a_visible, a_readonly, a_editable, a_sortable, a_header, a_styles, a_editor, a_button, a_show_button, a_footer, a_renderer, a_lookup, a_merge) {
// 	var column = new Column();
// 	column.setFiledName(a_filedName);
// 	column.setWidth(a_width);
// 	column.setMust_Input(a_must_input);
// 	column.setVisible(a_visible);
// 	column.setReadonly(a_readonly);
// 	column.setEditable(a_editable);
// 	column.setSortable(a_sortable);
// 	column.setHeader(a_header);
// 	column.setStyles(a_styles);
// 	column.setEditor(a_editor);
// 	column.setMergeRule(a_merge);
// 	if(a_button      != null && a_button      != "") column.prototype.button = a_button;
// 	if(a_show_button != null && a_show_button != "") column.prototype.alwaysShowButton = a_show_button;
// 	if(a_footer      != null && a_footer      != "") column.prototype.footer = a_footer;
// 	if(a_renderer    != null && a_renderer    != "") column.prototype.renderer = a_renderer;
// 	if(a_lookup      != null && a_lookup      != "") column.prototype.lookupDisplay = a_lookup;
// 	return column;
// }

// ////가변컬럼 셋팅용 객체생성 후 리턴2
// //function gf_getColumn2(a_filedName, a_width, a_must_input, a_visible, a_readonly, a_editable, a_sortable, a_header, a_styles, a_editor, a_button, a_show_button, a_footer, a_renderer, a_lookup) {
// //	var column = new RealGrids.DataColumn();
// //	column.fieldName  = (a_filedName  == null || a_filedName  == "") ? ""           : a_filedName;
// //	column.width      = (a_width      == null || a_width      == "") ? 100          : a_width;
// //	column.must_Input = (a_must_input == null || a_must_input == "") ? "N"          : a_must_input;
// //	column.visible    = (a_visible    == null || a_visible    == "") ? true         : a_visible;
// //	column.readonly   = (a_readonly   == null || a_readonly   == "") ? false        : a_readonly;
// //	column.editable   = (a_editable   == null || a_editable   == "") ? true         : a_editable;
// //	column.sortable   = (a_sortable   == null || a_sortable   == "") ? true         : a_sortable;
// //	column.header     = (a_header     == null || a_header     == "") ? {text: ""}   : a_header;
// //	column.styles     = (a_styles     == null || a_styles     == "") ? _Styles_text : a_styles;
// //	column.editor     = (a_editor     == null || a_editor     == "") ? _Editor_text : a_editor;
// //	if(a_button      != null && a_button      != "") column.button = a_button;
// //	if(a_show_button != null && a_show_button != "") column.alwaysShowButton = a_show_button;
// //	if(a_footer      != null && a_footer      != "") column.footer = a_footer;
// //	if(a_renderer    != null && a_renderer    != "") column.renderer = a_renderer;
// //	if(a_lookup      != null && a_lookup      != "") column.lookupDisplay = a_lookup;
// //	return column;
// //}

// //remove(버튼  visible/hidden 으로 변경처리)
// //==================== 함수 =====================
// //메인버튼의 추가 버튼을 동적으로 생성 또는 삭제 한다 (그리드, 행번호, 컬럼명배열, 비교값배열, 비교연산자배열, 비교대상이 둘일때 and or 연산자)
// function pf_setToggleInsertButton(a_dg, a_row, a_col_arr, a_val_arr, a_comparison_arr, a_and_or){
// 	var ls_btn = "";
// 	var ls_res = a_and_or == "and" ? true : false;

// 	for(var i = 0; i < a_col_arr.length; i++) {
// 		switch(a_comparison_arr[i]) {
// 			case "==":
// 				if(a_dg.GetItem(a_row, a_col_arr[i]) == a_val_arr[i]) ls_res = a_and_or == "and" ? false : true;
// 				break;
// 			case "!=":
// 				if(a_dg.GetItem(a_row, a_col_arr[i]) != a_val_arr[i]) ls_res = a_and_or == "and" ? false : true;
// 				break;
// 			case ">=":
// 				if(a_dg.GetItem(a_row, a_col_arr[i]) >= a_val_arr[i]) ls_res = a_and_or == "and" ? false : true;
// 				break;
// 			case "<=":
// 				if(a_dg.GetItem(a_row, a_col_arr[i]) <= a_val_arr[i]) ls_res = a_and_or == "and" ? false : true;
// 				break;
// 			case ">":
// 				if(a_dg.GetItem(a_row, a_col_arr[i]) > a_val_arr[i]) ls_res = a_and_or == "and" ? false : true;
// 				break;
// 			case "<":
// 				if(a_dg.GetItem(a_row, a_col_arr[i]) < a_val_arr[i]) ls_res = a_and_or == "and" ? false : true;
// 				break;
// 		}
// 	}

// 	if(ls_res){
// 		ls_btn += _X.Button("xBtnSave", "x_DAO_Save(dg_1)", "/Theme/images/btn/x_save")
// 					  + _X.Button("xBtnRetrieve", "x_DAO_Retrieve(dg_1)", "/Theme/images/btn/x_retrieve")
// 						+ _X.Button("xBtnDelete", "x_DAO_Delete(dg_1)", "/Theme/images/btn/x_delete")
// 						+ _X.Button("xBtnClose", "x_Close()", "/Theme/images/btn/x_close");
// 	} else {
// 		ls_btn = _X.MainButtons(_PGM_CODE, 'dg_1');
// 	}
// 	xbtns.innerHTML = "<span style='width:100%;height:100%;vertical-align:middle;'>" + ls_btn + "</span>"	;
// }


// //인수로 받은 name에 해당하는 라디오 버튼들의 체크를 풀고 인수로 받은 id에 해당하는 라디오버튼 체크 (라디오버튼 name 배열, 라디오버튼 id (값이 "" 일때는 체크를 하지 않는다))
// //모듈의 setstyle 으로 변경하여 mighty-com  으로 이동 (_X.SetRadioValue)
// function gf_RadioValueSync(a_elename_arr, a_eleid_arr){
// 	for(var i = 0; i < (typeof(a_elename_arr) == "string" ? 1 : a_elename_arr.length); i++) {
// 		var ls_radioEles = document.getElementsByName(typeof(a_elename_arr) == "string" ? a_elename_arr : a_elename_arr[i]);
// 		for(var j = 0; j < ls_radioEles.length; j++) {
// 			$("#" + ls_radioEles[j].id).prop('checked', false).attr('checked', false).parent().find('a:first').removeClass();
// 		}
// 		if((typeof(a_eleid_arr) == "string" ? a_eleid_arr : a_eleid_arr[i]) != "") {
// 			$("#" + (typeof(a_eleid_arr) == "string" ? a_eleid_arr : a_eleid_arr[i])).prop('checked', true).attr('checked', true).parent().find('a:first').addClass('checked');
// 		}
// 	}
// }


// //해당 필드에 같은 값이 있는지 확인하여 flag, rowIdx를 가진 object 리턴(그리드, 컬럼명배열, 값배열)
// function gf_checkFieldValue(a_dg, a_fields_arr, a_values_arr){
// 	var lo_obj = new Object()
// 	lo_obj.flag = false;
// 	lo_obj.rowIdx = null;
// 	for(var i = 0; i < (typeof(a_fields_arr) == "string" ? 1 : a_fields_arr.length); i++) {
// 		for(var j = 1; j <= a_dg.RowCount(); j++) {
// 			if(a_dg.GetItem(j, (typeof(a_fields_arr) == "string" ? a_fields_arr : a_fields_arr[i])) == (typeof(a_values_arr) == "string" ? a_values_arr : a_values_arr[i])) {
// 				lo_obj.flag = true;
// 				lo_obj.rowIdx = j;
// 				return lo_obj;
// 			}
// 		}
// 	}
// 	return lo_obj;
// }

// //삭제를 제외한 변경 데이타를 체크 a_chkjob 가 있을경우 해당항목이 있는지 체크하여 리턴 (그리드, 체크할항목 I=insert : U=update : D=delete null=update+insert)
// function gf_insertUpdateChk(a_dg, a_chkjob){
// 	var cData = a_dg.GetChangedData();

// 	if(cData == null || cData == "" || typeof(cData) == "undefined") return false;

// 	if(a_chkjob == null) {
// 		for(var i = 0; i < cData.length; i++) {
// 			if(cData[i].job != "D") return true;
// 		}
// 	} else if(a_chkjob == "I" || a_chkjob == "U" || a_chkjob == "D") {
// 		for(var i = 0; i < cData.length; i++) {
// 			if(cData[i].job == a_chkjob) return true;
// 		}
// 	}
// 	return false;
// }

// //그리드의 해당 필드의 MAX값을 리턴(SEQ(숫자)에 사용)
// function gf_gridMaxSeq(a_dg, a_colname, a_flag, a_val, a_colname2){
// 	var li_seq = 0;
// 	if(a_flag == null || a_flag == "") a_flag = false;

// 	if(a_flag) {
// 		for(var i = 1; i <= a_dg.RowCount(); i++) {
// 			if(a_dg.GetItem(i, a_colname) == a_val) {
// 				var ls_num = _X.ToInt(a_dg.GetItem(i, a_colname2))
// 				if(li_seq < ls_num) li_seq = ls_num;
// 			}
// 			if(li_seq < li_seq2) li_seq = li_seq2;
// 		}
// 	} else {
// 		for(var i = 1; i <= a_dg.RowCount(); i++) {
// 			var li_seq2 = _X.ToInt(a_dg.GetItem(i, a_colname));
// 			if(li_seq < li_seq2) li_seq = li_seq2;
// 		}
// 	}
// 	return li_seq + 1;
// }

// //그리드의 해당 컬럼의 가장 큰 날짜를 가져온다 (그리드, 컬럼명, 포멧타입, 행번호)
// function gf_gridMaxDate(a_dg, a_colname, a_format, a_row){
// 	var ls_rows = a_dg.RowCount();
// 	var ls_max_date = _X.ToDate("19000101");

// 	for(var i = 0; i < ls_rows; i++) {
// 		if(i + 1 == a_row) continue;

// 		ls_date = a_dg.GetItem(i + 1, a_colname);
// 		if(ls_date.length < 8) ls_date += "01";
// 		var ls_date = _X.ToDate(_X.ToString(ls_date, "YYYYMMDD"));

// 		if(_X.DaysAfter(ls_max_date, ls_date) > 0) ls_max_date = ls_date;
// 	}
// 	return _X.ToString(ls_max_date, a_format);
// }

// //날짜오류체크(6자리) 오류시 true 반환 (날짜, 메세지)
// //gf_ChkDate2
// function gf_chkDate2(as_date, as_msg, as_len){
// 	var lb_result=false;
// 	if(as_len == null) as_len = 6;

// 	if(as_len == 4) {
// 		if (as_date.length != 4) lb_result = true;
// 		else if (_X.ToInt(as_date.substr(0,4)) < 1 || _X.ToInt(as_date.substr(0,4)) > 9999) 	lb_result = true;
// 		if (lb_result && as_msg != null) _X.MsgBox("날짜 유효성 체크", "[" + as_msg + "](" + as_date + ")가 유효하지 않습니다.");
// 	} else if(as_len == 6) {
// 		if (as_date.length != 6) lb_result = true;
// 		else if (_X.ToInt(as_date.substr(0,4)) < 1 || _X.ToInt(as_date.substr(0,4)) > 9999) 	lb_result = true;
// 		else if (_X.ToInt(as_date.substr(4,2)) < 1 || _X.ToInt(as_date.substr(4,2)) > 12) 	lb_result = true;
// 		if (lb_result && as_msg != null) _X.MsgBox("날짜 유효성 체크", "[" + as_msg + "](" + as_date + ")가 유효하지 않습니다.");
// 	}
// 	return lb_result;
// }

// //이미지가 없는경우
// function x_img_notfound() {
// 	photo.src = top._rootImageUpload + "/" + "0000000.gif";
// }

// //==================== find =====================================================================================================================================

// //a_findcode를 setTimeout함수에서 에서 사용가능 하도록 변환하여 리턴
// function pf_setFindcode(a_findcode){
// 	if(typeof(a_findcode) == "string") {
// 		return "['" + a_findcode + "']";
// 	} else {
// 		var ls_returnValue = "['";
// 		for(var i = 0; i < a_findcode.length; i++) {
// 			ls_returnValue += a_findcode[i] + "','";
// 		}
// 		ls_returnValue = _X.Left(ls_returnValue, ls_returnValue.length - 2) + "]";
// 		return ls_returnValue;
// 	}
// }
// //pf_setTimeout(2, a_title == null ? "사원 찾기" : a_title, a_findId == null ? 'F_EMP_NO_NAME' : a_findId, a_findstr, a_findcode//, findkey, 850, 650, a_multi);
// //setTimeout으로 파인드창 열기
// function pf_setTimeout(a_mode, a_title, a_findId, a_findstr, a_findcode, a_query, a_width, a_height, a_multi){
// 	switch(a_mode) {
// 		case 2:
// 			//수정(2015.08.24 KYY)
// 			window._FindCode.finddiv = a_findId;
// 			window._FindCode.findcode = a_findcode.split(",");
// 			//window._FindCode.findcode = a_findcode
// 			var param = "sys=hr&fcd=" + a_findId + "&fnm="+encodeURIComponent(a_title)+"&msize=0&fgrid=" + a_query +"|HrFindCode|" + a_query + "";
// 			_X.OpenFindWin2(window, param, a_title, a_height, a_width);
// 			//setTimeout("_X.FindCode(window, '" + a_title + "', 'hr', '" + a_findId + "', " + (a_findstr == null ? "''" : "'" + a_findstr + "'") + ", " + (a_findcode == null ? "'%'" : pf_setFindcode(a_findcode)) + ", '" + a_query + "|HrFindCode|" + a_query + "', " + a_width + ", " + a_height + ", " + a_multi + ")", 0);
// 			break;
// 		case 3:
// 			setTimeout("_HR.FindWin3(window, '" + a_findId + "', " + (a_findcode == null ? "'%'" : pf_setFindcode(a_findcode)) + ", " + (a_findstr == null ? "''" : a_findstr) + ", '" + a_title + "', '" + a_query + "|HrFindCode|" + a_query + "', " + a_width + ", " + a_height + ", " + a_multi + ")", 0);
// 			break;
// 	}
// }


// function x_FindCode(a_dg, a_row, a_findKey, a_findstr, a_win) {
// 	if(typeof(x_FindCode2)!="undefined") {
// 		if(x_FindCode2()==null)  return;
// 	}

// 	if(a_findstr==null) a_findstr="";
// 	if(a_win==null) a_win=window;


// 	if(a_dg==null) {
// 		switch(a_findKey) {
// 			case "COST_DEPT_CODE":
// 			case "COST_DEPT_NAME":
// 					_FindCode.grid_row = 1;
// 					_X.FindCode(window,"원가부서 찾기","hr",a_findKey, a_findstr, new Array(top._CompanyCode), "FindDeptCode|HrFindCode|FindDeptCode",500, 500);
// 				break;
// 		}
// 	}
// }

// function x_ReceivedCode(a_returnValue) {
// 	if(typeof(x_ReceivedCode2)!="undefined") {
// 		if(x_ReceivedCode2(a_returnValue)==null)  return;
// 	}

// 	if (_FindCode.grid==null||_FindCode.grid==undefined) {
// 		switch(_FindCode.finddiv){
// 			case "COST_DEPT_CODE":
// 			case "COST_DEPT_NAME":
// 				COST_DEPT_CODE.value =  _FindCode.returnValue.DEPT_CODE;
// 				COST_DEPT_NAME.value =  _FindCode.returnValue.DEPT_NAME;
// 				break;
// 		}
// 	}
// }


// //파인드창 호출 ex) pf_findCode("FindEmpNoName", null, null, S_EMP_NO_NAME.value, new Array(a_dg.GetItem(a_row, "DEPT_CODE")));
// //function pf_findCode(findkey, a_user, a_findId, a_findstr, a_findcode, a_title , a_msg, a_multi){
// //	               //쿼리selectId, 권한, receiveId, sub파라메터, 검색창기본값, 창제목
// //	if(a_user != null && (top._UserRole != a_user && top._UserRole != "SM")) {
// //		_X.MsgBox(a_msg == null ? "권한이 없는 사용자 입니다." : a_msg);
// //		return;
// //	}
// //
// //	switch(findkey) {
// //		case "FindDeptCode":
// //			pf_setTimeout(2, a_title == null ? "부서코드 찾기" : a_title, a_findId == null ? 'F_DEPT_CODE' : a_findId, a_findstr, top._CompanyCode, findkey, 500, 650, a_multi);
// //			break;
// //		case "FindBankCode":
// //			pf_setTimeout(2, a_title == null ? "금융기관 찾기" : a_title, a_findId == null ? 'F_BANK_CODE' : a_findId, a_findstr, a_findcode, findkey, 455, 650, a_multi);
// //			break;
// //		case "FindEmpNoName":
// //			if(a_findcode == null){
// //				//a_findcode = new Array("", top._CompanyCode);
// //				a_findcode = '' + ',' + '' + ',' + top._CompanyCode;
// //			}
// //			else if (typeof(a_findcode) == "string") a_findcode = new Array(a_findcode, top._CompanyCode);
// //			pf_setTimeout(2, a_title == null ? "사원 찾기" : a_title, a_findId == null ? 'F_EMP_NO_NAME' : a_findId, a_findstr, a_findcode, findkey, 850, 650, a_multi);
// //			break;
// //		case "FindEmpNoName2":
// //			pf_setTimeout(2, a_title == null ? "사원 찾기" : a_title, a_findId == null ? 'F_EMP_NO_NAME' : a_findId, a_findstr, a_findcode, findkey, 850, 650, a_multi);
// //			break;
// //	  case "FindEmpNoName3":
// //			pf_setTimeout(2, a_title == null ? "사원 찾기" : a_title, a_findId == null ? 'F_EMP_NO_NAME' : a_findId, a_findstr, a_findcode, findkey, 850, 650, a_multi);
// //			break;
// //		case "FindOrderNo":
// //			pf_setTimeout(2, a_title == null ? "발령문서 찾기" : a_title, a_findId == null ? 'F_ORDER_NO' : a_findId, a_findstr, a_findcode, findkey, 570, 650, a_multi);
// //			break;
// //		case "FindSchoolCode":
// //			pf_setTimeout(2, a_title == null ? "학교 찾기" : a_title, a_findId == null ? 'F_SCHOOL_CODE' : a_findId, a_findstr, a_findcode, findkey, 460, 550, a_multi);
// //			break;
// //		case "FindMajorCode":
// //			pf_setTimeout(2, a_title == null ? "전공/부전공 찾기" : a_title, a_findId == null ? 'F_MAJOR_CODE' : a_findId, a_findstr, a_findcode, findkey, 380, 550, a_multi);
// //			break;
// //		case "FindLicense":
// //			pf_setTimeout(2, a_title == null ? "자격증 찾기" : a_title, a_findId == null ? 'F_LICENSE' : a_findId, a_findstr, a_findcode, findkey, 530, 550, a_multi);
// //			break;
// //		case "FindNotLabEmp":
// //			pf_setTimeout(2, a_title == null ? "사원 찾기" : a_title, a_findId == null ? 'F_EMP_NO_NAME' : a_findId, a_findstr, a_findcode, findkey, 850, 650, a_multi);
// //			break;
// //		case "FindInsuComp":
// //			if(a_findcode == null){
// //				//a_findcode = new Array("", top._CompanyCode);
// //				a_findcode = '' + ',' + '%';
// //			}
// //			pf_setTimeout(2, a_title == null ? "보증보험코드 찾기" : a_title, a_findId == null ? 'F_INSURENCE_AMT' : a_findId, a_findstr, a_findcode, findkey, 450, 350, a_multi);
// //			break;
// //		case "FindAcntCode":
// //			pf_setTimeout(2, a_title == null ? "계정과목 찾기" : a_title, a_findId == null ? 'F_ACNT_CODE' : a_findId, a_findstr, a_findcode, findkey, 850, 650, a_multi);
// //			break;
// //		case "FindAcntCode2":
// //			pf_setTimeout(2, a_title == null ? "계정과목 찾기" : a_title, a_findId == null ? 'F_ACNT_CODE' : a_findId, a_findstr, a_findcode, findkey, 400, 400, a_multi);
// //			break;
// //		case "FindCustCode":
// //			pf_setTimeout(2, a_title == null ? "거래처 찾기" : a_title, a_findId == null ? 'F_CUST_CODE' : a_findId, a_findstr, a_findcode, findkey, 850, 650, a_multi);
// //			break;
// //		case "FindAsset":
// //			pf_setTimeout(2, a_title == null ? "자산코드 찾기" : a_title, a_findId == null ? 'F_ASSET_CODE' : a_findId, a_findstr, a_findcode, findkey, 800, 550, a_multi);
// //			break;
// //		case "FindAsstDept":
// //			pf_setTimeout(2, a_title == null ? "부서코드 찾기" : a_title, a_findId == null ? 'F_DEPT_CODE' : a_findId, a_findstr, a_findcode, findkey, 500, 650, a_multi);
// //			break;
// //		case "FindOrgaSheet":
// //			pf_setTimeout(2, a_title == null ? "조직도문서번호 찾기" : a_title, a_findId == null ? 'F_ORGA_SHEET' : a_findId, a_findstr, a_findcode, findkey, 560, 500, a_multi);
// //			break;
// //		case "FindDutyCode":
// //			pf_setTimeout(2, a_title == null ? "직무코드 찾기" : a_title, a_findId == null ? 'F_DUTY_CODE' : a_findId, a_findstr, a_findcode, findkey, 400, 550, a_multi);
// //			break;
// //		case "FindJobkind":
// //			pf_setTimeout(2, a_title == null ? "직종코드 찾기" : a_title, a_findId == null ? 'F_JOBKIND_CODE' : a_findId, a_findstr, a_findcode, findkey, 430, 550, a_multi);
// //			break;
// //		case "FindGradeCode":
// //			pf_setTimeout(2, a_title == null ? "직위코드 찾기" : a_title, a_findId == null ? 'F_GRADE_CODE' : a_findId, a_findstr, a_findcode, findkey, 350, 550, a_multi);
// //			break;
// //		case "FindAllowMulti":
// //			pf_setTimeout(2, a_title == null ? "수당항목 찾기" : a_title, a_findId == null ? 'F_ALLOW_CODE_MULTI' : a_findId, a_findstr, a_findcode, findkey, 380, 550, a_multi);
// //			break;
// //		case "FindFlicenseCode":
// //			pf_setTimeout(2, a_title == null ? "시험내용 찾기" : a_title, a_findId == null ? 'F_FLICENSE_CODE' : a_findId, a_findstr, a_findcode, findkey, 350, 550, a_multi);
// //			break;
// //		case "FindEduCode":
// //			pf_setTimeout(2, a_title == null ? "교육과정 찾기" : a_title, a_findId == null ? 'F_EDU_CODE' : a_findId, a_findstr, a_findcode, findkey, 350, 550, a_multi);
// //			break;
// //
// //		case "FindMakeAllow":
// //			pf_setTimeout(3, a_title == null ? "수당등록" : a_title, a_findId == null ? 'F_MAKE_ALLOW' : a_findId, a_findstr, a_findcode, findkey, 1000, 500, a_multi);
// //			break;
// //		case "FindMakeEmployee":
// //			pf_setTimeout(3, a_title == null ? "사원등록" : a_title, a_findId == null ? 'F_ADD_EMPLOYEE' : a_findId, a_findstr, a_findcode, findkey, 270, 260, a_multi);
// //			break;
// //		case "FindApplInfo":
// //			pf_setTimeout(3, a_title == null ? "입사지원 상세내역" : a_title, a_findId == null ? 'F_APPL_INFO' : a_findId, a_findstr, a_findcode, findkey, 850, 550, a_multi);
// //			break;
// //		case "FindApplInfo10":
// //			pf_setTimeout(3, a_title == null ? "자기소개서" : a_title, a_findId == null ? 'F_APPL_INFO2' : a_findId, a_findstr, a_findcode, findkey, 750, 450, a_multi);
// //			break;
// //		case "FindBtripAppDetail":
// //			pf_setTimeout(3, a_title == null ? "츨장명령신청서 상세내역" : a_title, a_findId == null ? 'F_DETAIL_PERS' : a_findId, a_findstr, a_findcode, findkey, 1080, 442, a_multi);
// //			break;
// //		case "FindBtripReportDetail":
// //			pf_setTimeout(3, a_title == null ? "츨장복명서현황 상세내역" : a_title, a_findId == null ? 'F_DETAIL_PERS' : a_findId, a_findstr, a_findcode, findkey, 1080, 445, a_multi);
// //			break;
// //		case "FindVacaDetail":
// //			pf_setTimeout(3, a_title == null ? "휴가신청현황 상세내역" : a_title, a_findId == null ? 'F_DETAIL_PERS' : a_findId, a_findstr, a_findcode, findkey, 1080, 470, a_multi);
// //			break;
// //		case "FindEduRequest":
// //			pf_setTimeout(3, a_title == null ? "교육신청현황 상세내역" : a_title, a_findId == null ? 'F_EDU_REQUEST' : a_findId, a_findstr, a_findcode, findkey, 1080, 390, a_multi);
// //			break;
// //		case "FindEduRequestRes":
// //			pf_setTimeout(3, a_title == null ? "교육결과현황 상세내역" : a_title, a_findId == null ? 'F_EDU_REQUEST_RES' : a_findId, a_findstr, a_findcode, findkey, 1200, 390, a_multi);
// //			break;
// //		case "FindAllow":
// //			pf_setTimeout(3, a_title == null ? "수당항목 찾기" : a_title, a_findId == null ? 'F_ALLOW_CODE' : a_findId, a_findstr, a_findcode, findkey, 850, 550, a_multi);
// //			break;
// //		case "FindAllow10":
// //			pf_setTimeout(3, a_title == null ? "공제항목 찾기" : a_title, a_findId == null ? 'F_ALLOW_CODE2' : a_findId, a_findstr, a_findcode, findkey, 650, 550, a_multi);
// //			break;
// //		case "FindAllow20":
// //			pf_setTimeout(3, a_title == null ? "월급여변동 수당/공제 일괄등록" : a_title, a_findId == null ? 'F_ALLOW_MASS_ENR' : a_findId, a_findstr, a_findcode, findkey, 310, 260, a_multi);
// //			break;
// //		case "FindSeizurerEmp":
// //			pf_setTimeout(3, a_title == null ? "가압류 사원 찾기" : a_title, a_findId == null ? 'F_SEIZURER_EMP' : a_findId, a_findstr, a_findcode, findkey, 700, 550, a_multi);
// //			break;
// //		case "FindExcepEmp":
// //			pf_setTimeout(3, a_title == null ? "제외대상자 등록" : a_title, a_findId == null ? 'F_EXCEP_EMP_NO' : a_findId, a_findstr, a_findcode, findkey, 850, 550, a_multi);
// //			break;
// //		case "FindSfield":
// //			pf_setTimeout(3, a_title == null ? "전문분야 찾기" : a_title, a_findId == null ? 'F_SFIELD_CODE' : a_findId, a_findstr, a_findcode, findkey, 300, 250, a_multi);
// //			break;
// //		case "FindCertKind":
// //			pf_setTimeout(3, a_title == null ? "자격면허종류 찾기" : a_title, a_findId == null ? 'F_CERT_KIND_CODE' : a_findId, a_findstr, a_findcode, findkey, 350, 250, a_multi);
// //			break;
// //		case "FindCommitWork":
// //			pf_setTimeout(3, a_title == null ? "마감작업" : a_title, a_findId == null ? 'F_COMMIT_WORK' : a_findId, a_findstr, a_findcode, findkey, 390, 180, a_multi);
// //			break;
// //		case "FindPayxResultInfo":
// //			pf_setTimeout(3, a_title == null ? "발령, 수당 정보" : a_title, a_findId == null ? 'F_PAYX_RESULT_INFO' : a_findId, a_findstr, a_findcode, "HR079902", 1000, 500, a_multi);
// //			break;
// //	}
// //}


// //=========== _X.XmlSelect =================================================================================================================================
// var ls_combo = {};
// function gf_getComboData(a_prop, a_new) {
// 	var ls_getCombo = {
// 			"DUTY_CODE"           : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'DUTY_CODE'             , new Array() , 'json2');}//직무
// 		,	"GRADE_CODE"          : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'GRADE_CODE'            , new Array() , 'json2');}//직급,직위
// 		,	"JOBKIND_CODE"        : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'JOBKIND_CODE'          , new Array() , 'json2');}//직종
// 		,	"ORDER_CODE"          : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'ORDER_CODE'            , new Array() , 'json2');}//발령구분
// 		,	"DEPT_CODE"           : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'DEPT_CODE'             , new Array() , 'json2');}//근무지
// 		,	"PEOPOLE_CODE"        : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'PEOPOLE_CODE'          , new Array() , 'json2');}//지인관계코드
// 		,	"LICENSE_GRADE_CODE"  : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'LICENSE_GRADE_CODE'    , new Array() , 'json2');}//등급
// 		,	"CLASS_CODE"          : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'CLASS_CODE'            , new Array() , 'json2');}//계급
// 		,	"KIND_ARMY_CODE"      : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'KIND_ARMY_CODE'        , new Array() , 'json2');}//군별
// 		,	"CMSS_CODE"           : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'CMSS_CODE'             , new Array() , 'json2');}//역종
// 		,	"ARM_CODE"            : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'ARM_CODE'              , new Array() , 'json2');}//병과
// 		,	"EMPLOY_CODE"         : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'EMPLOY_CODE'           , new Array() , 'json2');}//채용구분
// 		,	"RELIGION_CODE"       : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'RELIGION_CODE'         , new Array() , 'json2');}//종교
// 		,	"BORN_CODE"           : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'BORN_CODE'             , new Array() , 'json2');}//출신도
// 		,	"SCHOOL_CAR_CODE"     : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'SCHOOL_CAR_CODE'       , new Array() , 'json2');}//학력구분
// 		,	"FLICENSE_CODE"       : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'FLICENSE_CODE'         , new Array() , 'json2');}//외국어자격코드
// 		,	"EDU_CODE"            : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'EDU_CODE'              , new Array() , 'json2');}//교육코드
// 		,	"LABSTA_CODE"         : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'LABSTA_CODE'           , new Array() , 'json2');}//근태사유코드
// 		,	"LABSTA_CODE_VACA"    : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'LABSTA_CODE_VACA'      , new Array() , 'json2');}//근태사유코드(경조휴가, 예비군/민방위, 해외정기휴가, 교육)
// 		,	"INSU_COMP_CODE"      : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'INSU_COMP_CODE'        , new Array() , 'json2');}//보증보험회사
// 		,	"FIELD_CODE1"         : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'FIELD_CODE1'           , new Array() , 'json2');}//건설직무분야코드1
// 		,	"CERT_FIELD_CODE"     : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'CERT_FIELD_CODE'       , new Array() , 'json2');}//건설자격코드
// 		,	"HANDITYPE_CODE"      : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HANDITYPE_CODE'        , new Array() , 'json2');}//장애명칭
// 		,	"HANDIGRADE_CODE"     : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HANDIGRADE_CODE'       , new Array() , 'json2');}//장애등급
// 		,	"VETERANS_CODE"       : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'VETERANS_CODE'         , new Array() , 'json2');}//보훈명
// 		,	"LICENSE_AREA_CODE"   : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'LICENSE_AREA_CODE'     , new Array() , 'json2');}//자격면허분야
// 		,	"LICENSE_JOB_CODE"    : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'LICENSE_JOB_CODE'      , new Array() , 'json2');}//자격면허직종
// 		,	"LICENSE_TYPE_CODE"   : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'LICENSE_TYPE_CODE'     , new Array() , 'json2');}//자격면허종류
// 		,	"RETIRE_CODE"         : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'RETIRE_CODE'           , new Array() , 'json2');}//퇴사사유
// 		,	"USING_TAG"       	  : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'YN_TAG'      					, new Array() , 'json2');}//사용여부
// 		,	"TAX_CONFIG_WORK_YYMM": function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'TAX_CONFIG_WORK_YYMM'  , new Array() , 'json2');}//연말정산 -> 세금계산환경설정 -> 기준년월
// 		,	"YETA_ITEM_WORK_YEAR" : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'YETA_ITEM_WORK_YEAR'   , new Array() , 'json2');}//연말정산 -> 소득공제신고항목 -> 기준년도
// 		,	"RNP_CODE_1"          : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'RNP_CODE'              , new Array('1') , 'json2');}//표창명칭
// 		,	"RNP_CODE_2"          : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'RNP_CODE'              , new Array('2') , 'json2');}//징계종류
// 		,	"RNP_CODE_3"          : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'RNP_CODE'              , new Array('%') , 'json2');}//표창명칭+징계종류
// 		,	"ALLOW_CODE"          : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_CODE_ALLOW' , new Array('%', '1') , 'json2');}//수당코드
// 		,	"DEDUCT_CODE"         : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_CODE_ALLOW' , new Array('%', '2') , 'json2');}//공제코드
// 		,	"ORG_SHEET"           : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'ORG_SHEET'             , new Array(top._CompanyCode, 'Y') , 'json2');}//조직도문서번호
// 		,	"LABOR_APP_DIV"       : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'LABOR_APP_DIV')     , 'json2');}//노동조합직책
// 		,	"LABOR_JOIN_DIV"      : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'LABOR_JOIN_DIV')    , 'json2');}//노동조합가입여부
// 		,	"VETERANS_ADDVAL"     : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'VETERANS_ADDVAL')   , 'json2');}//보훈가점
// 		,	"VETERANS_CHILD_DIV"  : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'VETERANS_CHILD_DIV'), 'json2');}//보훈자녀
// 		,	"VETERANS_YN"         : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'VETERANS_YN')       , 'json2');}//보훈여부
// 		,	"MARRIED_YN"          : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'MARRIED_YN')        , 'json2');}//결혼여부
// 		,	"BLOOD_DIV"           : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'BLOOD_DIV')         , 'json2');}//혈액형
// 		,	"CERT_GRADE"          : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'CERT_GRADE')        , 'json2');}//건설자격종류
// 		,	"SDUTY_GRADE"         : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'SDUTY_GRADE')       , 'json2');}//건설분야등급
// 		,	"DUTY_GRADE"          : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'DUTY_GRADE')        , 'json2');}//건설직무등급
// 		,	"ASSET_YN"            : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'ASSET_YN')          , 'json2');}//자산여부
// 		,	"APPL_PROC_FLAG"      : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'APPL_PROC_FLAG')    , 'json2');}//채용공고진행상태
// 		,	"APPL_DIV"            : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'APPL_DIV')          , 'json2');}//채용공고구분
// 		,	"CUTOFF_UNIT_DIV"     : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'CUTOFF_UNIT_DIV')   , 'json2');}//절사단위
// 		,	"CUTOFF_STD_DIV"      : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'CUTOFF_STD_DIV')    , 'json2');}//절사기준
// 		,	"PAY_CALC_TAG"        : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'PAY_CALC_TAG')      , 'json2');}//계산구분
// 		,	"TAX_FREE_KIND"       : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'TAX_FREE_KIND')     , 'json2');}//비과세종류
// 		,	"TAX_FREE_YN"         : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'TAX_FREE_YN')       , 'json2');}//과세, 비과세
// 		,	"OFFICE_CERTI_DIV"    : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'OFFICE_CERTI_DIV')  , 'json2');}//제증명서종류
// 		,	"SERVICE_DIV"         : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'SERVICE_DIV')       , 'json2');}//재직구분
// 		,	"TRIP_PAY_DIV"        : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'TRIP_PAY_DEV')      , 'json2');}//지급방법구분
// 		,	"TRIP_AREA_CODE"      : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'TRIP_AREA_CODE')    , 'json2');}//출장지구분
// 		,	"PAY_KIND"            : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'PAY_KIND')          , 'json2');}//지급구분
// 		,	"EMPLOYEE_DIV"        : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'EMPLOYEE_DIV')      , 'json2');}//직원구분
// 		,	"EMP_DIV"             : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'EMP_DIV')           , 'json2');}//직원구분(한자리)
// 		,	"HEADERFAMILY_REL"    : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array("HR", "HEADERFAMILY_REL")  , 'json2');}//호주와의관계
// 		,	"HOUSEHOLDER_REL"     : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array("HR", "HOUSEHOLDER_REL")   , 'json2');}//세대주와의관계
// 		,	"ADDR_DIV"            : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array("HR", "ADDR_DIV")          , 'json2');}//주소구분
// 		,	"SCHOOL_BRANCH_DIV"   : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'SCHOOL_BRANCH_DIV') , 'json2');}//본교,분교
// 		,	"NIGHT_YN"            : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'NIGHT_YN')          , 'json2');}//주야구분
// 		,	"SCHOOL_CAR_DIV"      : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'SCHOOL_CAR_DIV')    , 'json2');}//학사,석사,박사(학위구분)
// 		,	"GRADUATE_DIV"        : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'GRADUATE_DIV')      , 'json2');}//졸업여부
// 		,	"FAMREL_CD"           : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'FAMREL_CD')         , 'json2');}//가족관계
// 		,	"LIFE_YN"             : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'SURVIVAL_YN')       , 'json2');}//생존,사망
// 		,	"TOGETHERTAG"         : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'TOGETHER_YN')       , 'json2');}//동거,비동거
// 		,	"DEPENDENT_YN"        : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'DEPENDENT_YN')      , 'json2');}//부양,비부양
// 		,	"INCOMP_DIV"          : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'INCOMP_DIV')        , 'json2');}//사내구분
// 		,	"END_ARMY_CD"         : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'END_ARMY_CD')       , 'json2');}//병역필구분
// 		,	"DISCHARGE_CD"        : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'DISCHARGE_CD')      , 'json2');}//제대구분
// 		,	"FLANG_CD"            : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'FLANG_CD')          , 'json2');}//외국어코드
// 		,	"RATE_DIV"            : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'RATE_DIV')          , 'json2');}//상중하구분
// 		,	"COMPLETION_YN"       : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'EDU_COMPLET_YN')    , 'json2');}//수료여부
// 		,	"AFFILIATE_CD"        : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'AFFILIATE_CD')      , 'json2');}//기관분류코드
// 		,	"BLIND_DIV"           : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'BLIND_DIV')         , 'json2');}//색맹
// 		,	"PHY_STATUS"          : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'PHY_STATUS')        , 'json2');}//신검판정
// 		,	"NATION_CD"           : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'NATION_CD')         , 'json2');}//국가코드
// 		,	"PROFESS_FIELD"       : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'PROFESS_FIELD')     , 'json2');}//전문분야
// 		,	"APPROVAL_FLAG"       : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'APPROVAL_FLAG')     , 'json2');}//결제진행상태
// 		,	"DEPT_TYPE_DIV"       : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('SM', 'DEPT_TYPE_DIV')     , 'json2');}//공사종류
// 		,	"WORKEVAL_DIV"        : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('SM', 'WORKEVAL_DIV')      , 'json2');}//근무평가
// 		,	"DEPT_DIV"       		  : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'DEPT_DIV')      	 	 , 'json2');}//부서구분
// 		,	"AREA_DIV"       		  : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'AREA_DIV')      	 	 , 'json2');}//지역구분
// 		,	"DEPT_TYPE_DIV"       : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'DEPT_TYPE_DIV')     , 'json2');}//현장구분
// 		,	"DEDUCT_DIV"          : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'       , new Array('HR', 'DEDUCT_DIV')        , 'json2');}//연말정산소득공제구분
// 		,	"COST_CODE"       	  : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'      	, new Array('SM', 'COSTCODE')          , 'json2');}//사용여부
// 		,	"WORKAREA_DIV"     	  : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'      	, new Array('HR', 'WORKAREA_DIV')      , 'json2');}//주종근무지구분
// 		,	"LUNAR_SOLAR"     	  : function() {return _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100'      	, new Array('HR', 'LUNAR_SOLAR')       , 'json2');}//양력, 음력
// 		, "MTA_YN"              : function() {return [{"code":"Y", "label":"중도정산"},{"code":"N", "label":"연말정산"}];}
// 	}
// 	if(a_new || typeof(ls_combo[a_prop]) == "undefined")
// 		ls_combo[a_prop] = ls_getCombo[a_prop]();

// 	return ls_combo[a_prop];
// }

// //메뉴코드 중복체크
// function gf_dupCode(a_dg, a_value, a_row, a_col, a_type, a_queryid) {
// 	if (a_queryid==null) a_type="GRID";
// 	if (a_type==null) a_type="GRID";
// 	if (a_col==null) return true;
// 	if (a_queryid==null) a_queryid="";
// 	var ls_queryid = a_queryid.split("|");

// 	if (a_type=="GRID")
// 	{
// 		for(var i=1; i<a_dg.RowCount(); i++){
// 			if (a_dg.GetItem(i,a_col)==a_value&&i!=a_row) {
// 				return false;
// 			}
// 		}
// 	} else {
// 			sData = _X.XmlSelect(ls_queryid[0], ls_queryid[1], ls_queryid[2], new Array(a_value), "array");
// 			if(sData==a_value) return false;
// 	}
// 	return true;
// }

// //=========== 변경 데이타 체크 예외처리 객체 ===========================================================================================
// var gf_chkObj = {
// 		"ls_option" : null
// 	, "ls_focusRes" : true
// 	, "focus" :
// 			function(a_dg) {
// 				if(this.ls_option == null) return false;
// 				if(this.is_equlAdg(a_dg)) return false;
// 				if(this.ls_focusRes) {
// 					var ls_res = this.is_dataChanged(this.ls_option.childAdg);
// 			 		if(ls_res && _X.MsgBoxYesNo(this.ls_option.focusMsgAll == null ? "추가/변경 중인 데이타가 있습니다.\n포커스 변경 시 " + (this.ls_option.focusMsg == null ? "" : (this.ls_option.focusMsg + "의 ")) + "데이타가 초기화 됩니다.\n\n계속 진행 하시겠습니까?" : this.ls_option.focusMsgAll) == "2")
// 			 			return true;
// 		 		} else {
// 		 			this.ls_focusRes = true;
// 		 		}
// 		 		return false;
// 			}
// 	, "insert" :
// 			function(a_dg) {
// 				if(this.ls_option == null) return false;
// 				if(this.is_equlAdg(a_dg)) return false;
// 				var ls_res = this.is_dataChanged(this.ls_option.childAdg);
// 		 		if(ls_res && _X.MsgBoxYesNo(this.ls_option.insertMsgAll == null ? "추가/변경 중인 데이타가 있습니다.\n추가 시 포커스가 변경되며\n" + (this.ls_option.insertMsg == null ? "" : (this.ls_option.insertMsg + "의 ")) + "데이타가 초기화 됩니다.\n\n계속 진행 하시겠습니까?" : this.ls_option.insertMsgAll) == "2")
// 		 			return true;
// 		 		this.ls_focusRes = false;
// 		 		return false;
// 			}
// 	, "delete" :
// 			function(a_dg) {
// 				if(this.ls_option == null) return false;
// 				if(this.is_equlAdg(a_dg)) return false;
// 				this.ls_focusRes = false;
// 			}
// 	, "Retrieve" :
// 			function(a_dg) {
// 				if(this.ls_option == null) return false;
// 				var ls_res = this.is_dataChanged();
// 				if(ls_res && _X.MsgBoxYesNo(this.ls_option.retrieveMsgAll == null ? "추가/변경중인 데이타가 있습니다.\n조회 시 변경된 데이타가 초기화 됩니다.\n\n계속 진행 하시겠습니까?" : this.ls_option.retrieveMsgAll) == "2")
// 					return true;
// 				this.ls_focusRes = false;
// 				return false;
// 			}
// 	, "close" :
// 			function(a_dg) {
// 				if(this.ls_option == null) return false;
// 				var ls_res = this.is_dataChanged();
// 				if(ls_res && _X.MsgBoxYesNo(this.ls_option.closeMsgAll == null ? "추가/변경중인 데이타가 있습니다.\n\n계속 진행 하시겠습니까?" : this.ls_option.closeMsgAll) == "2")
// 					return true;
// 				return false;
// 			}
// 	, "save" :
// 			function(a_dg) {
// 				if(this.ls_option == null) return false;
// 				var ls_res = this.is_dataChanged();
// 				if(ls_res) {
// 					_X.MsgBox(this.ls_option.saveMsgAll == null ? "추가/변경중인 데이타가 있습니다.\n데이타 저장 후 실행 가능합니다." : this.ls_option.saveMsgAll);
// 					return true;
// 				}
// 				return false;
// 			}
// 	, "is_dataChanged" :
// 			function(a_dgs) {
// 				if(a_dgs != null) {
// 					for(var i = 0; i < a_dgs.length; i++) {
// 			 			if(a_dgs[i].IsDataChanged()) return true;
// 			 		}
// 			 	} else {
// 			 		for(var i = 0; i < window._Grids.length; i++) {
// 						if(window._Grids[i].IsDataChanged()) return true;
// 					}
// 			 	}
// 		 		return false;
// 			}
// 	, "is_equlAdg" :
// 			function(a_dg) {
// 				for(var i = 0; i < this.ls_option.childAdg.length; i++) {
// 					if(a_dg.id == this.ls_option.childAdg[i].id) return true;
// 				}
// 				return false;
// 			}
// }

// function gf_chkObjFunc(a_prop, a_dg, row, a_newrow, a_oldrow) {
// 	var ls_delete = a_prop == "delete" ? true : false;
// 	var ls_index = -1;

// 	if(!ls_delete && typeof(pf_chkObjProp) != "undefined") {
// 		if(gf_chkObj.ls_option == null) gf_chkObj.ls_option = pf_chkObjProp();
// 		ls_index = gf_chkObj.ls_option.prop.indexOf(a_prop.substring(0, 1).toUpperCase());
// 	}
// 	if(ls_delete || ls_index != -1) {
// 		if(gf_chkObj[a_prop](a_dg)) return true;
// 	}
// 	return false;
// //	switch(a_prop) {
// //		case "focus"    : xe_GridRowFocusChange(a_dg, a_newrow, a_oldrow); break;
// //		case "insert"   : x_DAO_Insert(a_dg, row); break;
// //		case "Retrieve" : x_DAO_Retrieve(a_dg); break;
// //		case "close"    : x_Close(); break;
// //	}
// }

// function pf_chkObjProp() {
// 	var ls_obj = {};
// 	ls_obj.prop = ["F", "I", "R", "C", "S"];
// 	ls_obj.childAdg = [dg_2];
// 	ls_obj.focusMsg = "연말정산 대상자";
// 	ls_obj.insertMsg = "연말정산 대상자";
// 	return ls_obj;
// }


// //a_dg.setStyles({body: {dynamicStyles: [{criteria: ["value['ROW_DIV'] = '0'"], styles: ["background=#f5f5f5"]}]}});
// //		a_dg.AddCellStyle("styleReadOnly", {
// //	    "background": "#f5f5f5",
// //	    "editable": false,
// //	    "readonly": true
// //	  });
// //	  a_dg.AddCellStyle("styleNone", {
// //	    "background": "#00000000",
// //	    "editable": true,
// //	    "readonly": false
// //	  });


// //function tests(a_dg, a_sub, a_sqlFile, a_sqlKey, a_param){
// //	var row = _X.XmlSelect("hr", "HR_PAYX_SCHOOL", "HR_PAYX_SCHOOL_R02", new Array(top._CompanyCode, top._UserID), "json2");
// //	for(var i = 0; i < row.length; i++) {
// //		var ls_rowdatas = {};
// //		for(var name in row[i]) {
// //			ls_rowdatas[name.toUpperCase()] = row[i][name];
// //		}
// //		a_dg._DataGrid.addRow(ls_rowdatas);
// //	}
// //}
// //
// //	//그리드 옵션변경 (2014.10.23 이상규)
// //	_X.SetOptions = function(a_dg, a_options){
// //		a_dg._GridRoot.setOptions(a_options);
// //	}

// //SKLEE
// var _HR_APPOINT = [{"code":"Y","label":"지정"},{"code":"N","label":"미지정"}];
// //--------------------------------------------------------------------------------------------------------------------------------------------



// //------------------------------Used OpenFindWin3-------------------------------------------------------

// _HR.FindApplInfo = function(a_win, a_find_div, a_findstr, a_findcode) {
// 	a_win._FindCode.finddiv = a_find_div;
// 	a_win._FindCode.findstr = a_findstr;
// 	a_win._FindCode.findcode = a_findcode;
// 	var param = "sys=hr&fcd=FindApplInfo&fnm="+encodeURIComponent('입사지원 상세내역')+"&msize=0&fgrid=FindApplInfo|HrFindCode|FindApplInfo";
// 	_X.OpenFindWin3(a_win, param, '입사지원 상세내역', 850, 550);
// };
// _HR.findApplInfo = _HR.FindApplInfo;

// _HR.FindApplInfo10 = function(a_win, a_find_div, a_findstr, a_findcode) {
// 	a_win._FindCode.finddiv = a_find_div;
// 	a_win._FindCode.findstr = a_findstr;
// 	a_win._FindCode.findcode = a_findcode;
// 	var param = "sys=hr&fcd=FindApplInfo10&fnm="+encodeURIComponent('자기소개서')+"&msize=0&fgrid=FindApplInfo10|HrFindCode|FindApplInfo10";
// 	_X.OpenFindWin3(a_win, param, '자기소개서', 750, 450);
// };
// _HR.findApplInfo = _HR.FindApplInfo;

// // 수당항목 POPUP
// _HR.FindAllow = function(a_win, a_find_div, a_findstr, a_findcode) {
// 	a_win.name = 'FindAllow';
// 	a_win._FindCode.finddiv = a_find_div;
// 	a_win._FindCode.findstr = a_findstr;
// 	a_win._FindCode.findcode = a_findcode;

// 	var param = "sys=hr&fcd=FindAllow&fnm="+encodeURIComponent('수당 항목 검색')+"&msize=0&fgrid=FindAllow|FindAllow|FindAllow";
// 	_X.OpenFindWin3(a_win, param, '수당 항목 검색', 1200, 540);
// };
// _HR.findAllow = _HR.FindAllow;

// // 공제항목 POPUP
// _HR.FindAllow10 = function(a_win, a_find_div, a_findstr, a_findcode) {
// 	a_win.name = 'FindAllow10';
// 	a_win._FindCode.finddiv = a_find_div;
// 	a_win._FindCode.findstr = a_findstr;
// 	a_win._FindCode.findcode = a_findcode;

// 	var param = "sys=hr&fcd=FindAllow10&fnm="+encodeURIComponent('공제 항목 검색')+"&msize=0&fgrid=FindAllow10|HrFindCode|FindAllow";
// 	_X.OpenFindWin3(a_win, param, '공제 항목 검색', 800, 540);
// };
// _HR.findAllow10 = _HR.FindAllow10;

// // 월급여변동 수당/공제 일괄등록 POPUP
// _HR.FindAllow20 = function(a_win, a_find_div, a_findstr, a_findcode) {
// 	a_win.name = 'FindAllow20';
// 	a_win._FindCode.finddiv = a_find_div;
// 	a_win._FindCode.findstr = a_findstr;
// 	a_win._FindCode.findcode = a_findcode;

// 	var param = "sys=hr&fcd=FindAllow20&fnm="+encodeURIComponent('월급여변동 수당/공제 일괄등록')+"&msize=0&fgrid=FindAllow20|HrFindCode|FindAllow";
// 	_X.OpenFindWin3(a_win, param, '공제 항목 검색', 310, 260);
// };
// _HR.findAllow20 = _HR.FindAllow20;

// // 가압류 사원 찾기 POPUP
// _HR.FindSeizurerEmp = function(a_win, a_find_div, a_findstr, a_findcode) {
// 	a_win.name = 'FindSeizurerEmp';
// 	a_win._FindCode.finddiv = a_find_div;
// 	a_win._FindCode.findstr = a_findstr;
// 	a_win._FindCode.findcode = a_findcode;

// 	var param = "sys=hr&fcd=FindSeizurerEmp&fnm="+encodeURIComponent('가압류 사원 찾기')+"&msize=0&fgrid=FindSeizurerEmp|HrFindCode|FindSeizurerEmp";
// 	_X.OpenFindWin3(a_win, param, '가압류 사원 찾기', 700, 550);
// };
// _HR.findSeizurerEmp = _HR.FindSeizurerEmp;


// // 보증보험코드 찾기 POPUP
// _HR.FindPayxResultInfo = function(a_win, a_find_div, a_findstr, a_findcode) {
// 	a_win.name = 'FindPayxResultInfo';
// 	a_win._FindCode.finddiv = a_find_div;
// 	a_win._FindCode.findstr = a_findstr;
// 	a_win._FindCode.findcode = a_findcode;

// 	var param = "sys=hr&fcd=HR079902&fnm="+encodeURIComponent('발령, 수당 정보')+"&msize=0&fgrid=HR079902|HR_ORDE_MASTER|HR_ORDE_MASTER_R04";
// 	_X.OpenFindWin3(a_win, param, '보증보험코드 찾기', 1000, 480);
// };
// _HR.findPayxResultInfo = _HR.FindPayxResultInfo;

// // 보증보험코드 찾기 POPUP
// _HR.FindExcepEmp = function(a_win, a_find_div, a_findstr, a_findcode) {
// 	a_win.name = 'FindExcepEmp';
// 	a_win._FindCode.finddiv = a_find_div;
// 	a_win._FindCode.findstr = a_findstr;
// 	a_win._FindCode.findcode = a_findcode;

// 	var param = "sys=hr&fcd=FindExcepEmp&fnm="+encodeURIComponent('발령, 수당 정보')+"&msize=0&fgrid=FindExcepEmp|HrFindCode|FindExcepEmp";
// 	_X.OpenFindWin3(a_win, param, '보증보험코드 찾기', 850, 550);
// };
// _HR.findExcepEmp = _HR.FindExcepEmp;

// // 교육신청 상세내역1 POPUP
// _HR.FindEduRequest = function(a_win, a_find_div, a_findstr, a_findcode) {
// 	a_win.name = 'FindEduRequest';
// 	a_win._FindCode.finddiv = a_find_div;
// 	a_win._FindCode.findstr = a_findstr;
// 	a_win._FindCode.findcode = a_findcode;

// 	var param = "sys=hr&fcd=FindEduRequest&fnm="+encodeURIComponent('교육신청 상세내역')+"&msize=0&fgrid=FindEduRequest|HrFindCode|FindEduRequest";
// 	_X.OpenFindWin3(a_win, param, '교육내역 검색', 1000, 400);
// };
// _HR.findEduRequest = _HR.FindEduRequest;


// // 교육신청 상세내역2 POPUP
// _HR.FindEduRequestRes = function(a_win, a_find_div, a_findstr, a_findcode) {
// 	a_win.name = 'FindEduRequestRes';
// 	a_win._FindCode.finddiv = a_find_div;
// 	a_win._FindCode.findstr = a_findstr;
// 	a_win._FindCode.findcode = a_findcode;

// 	var param = "sys=hr&fcd=FindEduRequestRes&fnm="+encodeURIComponent('교육신청 상세내역')+"&msize=0&fgrid=FindEduRequestRes|HrFindCode|FindEduRequestRes";
// 	_X.OpenFindWin3(a_win, param, '교육내역 검색', 1000, 460);
// };
// _HR.findEduRequestRes = _HR.FindEduRequestRes;

// // 교육신청 상세내역2 POPUP
// _HR.FindCommitWork = function(a_win, a_find_div, a_findstr, a_findcode) {
// 	a_win.name = 'FindCommitWork';
// 	a_win._FindCode.finddiv = a_find_div;
// 	a_win._FindCode.findstr = a_findstr;
// 	a_win._FindCode.findcode = a_findcode;

// 	var param = "sys=hr&fcd=FindCommitWork&fnm="+encodeURIComponent('마감작업')+"&msize=0&fgrid=FindCommitWork|HrFindCode|FindEduRequestRes";
// 	_X.OpenFindWin3(a_win, param, '마감작업', 390, 180);
// };
// _HR.FindCommitWork = _HR.FindCommitWork;

// //인사기본정보[HR030101] 사원등록 POPUP
// _HR.FindMakeEmployee = function(a_win, a_find_div, a_findstr, a_findcode) {
// 	a_win.name = 'FindMakeEmployee';
// 	a_win._FindCode.finddiv = a_find_div;
// 	a_win._FindCode.findstr = a_findstr;
// 	a_win._FindCode.findcode = a_findcode;

// 	var param = "sys=hr&fcd=FindMakeEmployee&fnm="+encodeURIComponent('마감작업')+"&msize=0&fgrid=FindMakeEmployee|HrFindCode|FindMakeEmployee";
// 	_X.OpenFindWin3(a_win, param, '사원등록', 270, 260);
// };
// _HR.FindCommitWork = _HR.FindCommitWork;


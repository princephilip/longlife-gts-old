/****************************************************************
 * 
 * 파일명 : mighty-find-1.0.0.js
 * 설  명 : 코드찾기 popup JavaScript
 * 
 *    수정일      수정자     Version        Function 명
 * ------------    ---------   -------------  ----------------------------
 * 2013.02.10    김양열       1.0.0            최초생성
 * 
 * 
 */
var _AC;
if (!_AC) {
	_AC = {};
}

_AC.PopupSlipList = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var temp = a_findcode.split("|");
	var title = (temp[0] == "Slip" ? "전표" : "결의서");
	var param = "sys=ac&fcd=PopupSlipList&fnm="+encodeURIComponent('전표목록 선택')+"&msize=0&fgrid=PopupSlipList|PopupSlipList|PopupSlipList_R01";
	_X.OpenFindWin3(a_win, param, title+'목록 선택', 650, 600);
}
_AC.popupSlipList = _AC.PopupSlipList;

_AC.PopupSlipList2 = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var temp = a_findcode.split("|");
	var title = "대손상각비";
	var param = "sys=ac&fcd=PopupSlipList2&fnm="+encodeURIComponent('전표목록 선택')+"&msize=0&fgrid=PopupSlipList2|PopupSlipList2|PopupSlipList2_R01";
	_X.OpenFindWin3(a_win, param, title+' 상세', 730, 600);
}
_AC.popupSlipList2 = _AC.PopupSlipList2;


//어음등록, 일괄수령등록
_AC.BillRegister = function(a_win, a_find_div, a_findstr, a_findcode) {
	//alert(a_find_div+';'+a_findstr+';'+a_findcode);
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=af&fcd=BillRegister&fnm="+encodeURIComponent('어음등록 일괄수령')+"&msize=0&fgrid=BillRegister|BillRegister|BillRegister";
	//alert(param);
	_X.OpenFindWin3(a_win, param, '어음등록 일괄수령', 400, 250);
};
_AC.billRegister = _AC.BillRegister;
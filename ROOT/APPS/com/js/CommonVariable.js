//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 : [공통]
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
//  
//
//   

//===========================================================================================

// 각 업무 파트에서 쓰는 부분은 요기로 이동해 주세요(이동이 않되어 있는 거는 지워바려요)


//===========================================================================================

var _CB_BUILD_DIV = [{"code":"","label":" "},{"code":"1000","label":"사무실"},{"code":"2000","label":"창고"},{"code":"3000","label":"집배송장"},{"code":"4000","label":"작업장"},{"code":"5000","label":"회의실"}];
var _CB_INCUST_DIV = [{"code":"","label":" "},{"code":"1","label":"입주업체"},{"code":"2","label":"거래소업체"},{"code":"3","label":"일반임대업체"}];
var _CB_REPORT_DIV = [{"code":"","label":" "},{"code":"1","label":"사업장단위"},{"code":"2","label":"사업자단위"}];
var _CB_YN = [{"code":"","label":" "},{"code":"Y","label":"예"},{"code":"N","label":"아니오"}];
var _CB_CASHYN = [{"code":"","label":" "},{"code":"Y","label":"완납"},{"code":"N","label":"미납"}];
var _CB_YN_NN = [{"code":"Y","label":"예"},{"code":"N","label":"아니오"}];
var _CB_INOUT_TAG = [{"code":"","label":" "},{"code":"1","label":"입고"},{"code":"2","label":"출고"}];
var _CB_INOUT_TAG_NN = [{"code":"1000","label":"입고"},{"code":"2000","label":"출고"}];
var _CB_CLOSE_DIV = [{"code":"1000","label":"종합거래"},{"code":"2000","label":"운영관리"},{"code":"3000","label":"품질유통"}];
var _CB_CINOUT_DIV = [{"code":"","label":" "},{"code":"3000","label":"입하입차"},{"code":"4000","label":"배송입차"},{"code":"5000","label":"반품입차"}];
var _CB_PERIOD_DIV = [{"code":"1","label":"월"},{"code":"2","label":"분기"},{"code":"3","label":"반기"},{"code":"4","label":"년"}];
var _CB_QUARTER_DIV = [{"code":"1","label":"1분기"},{"code":"2","label":"2분기"},{"code":"3","label":"3분기"},{"code":"4","label":"4분기"}];
var _CB_HALF_DIV = [{"code":"1","label":"상반기"},{"code":"2","label":"하반기"}];
var _CB_PRINT_DIV = [{"code":"1","label":"거래명세서"},{"code":"2","label":"상차지시서"}];
var _CB_PRINT_DIV2 = [{"code":"1","label":"친환경"},{"code":"2","label":"일반"}];
var _CB_CONT_DIV = [{"code":"","label":" "},{"code":"CONT","label":"시설계약"},{"code":"RENT","label":"임대계약"}];

var _CB_KIND_PRINT = [{"code":"1","label":"입주증 발급"},{"code":"2","label":"열쇠인수증 발급"}];
var _CB_PERIOD_PRINT = [{"code":"1","label":"기간별 입주예약배정내역"},{"code":"2","label":"기간별 입주증발급내역"},{"code":"3","label":"기간별 열쇠인수증발급내역"},{"code":"4","label":"기간별 시설물입주증발급내역"}];
var _CB_REG_PRINT = [{"code":"1","label":"등기내역"},{"code":"2","label":"미등기내역"}];
var _CB_PROCESS = [{"code":"%","label":"전체"},{"code":"Y","label":"처리"},{"code":"N","label":"미처리"}];
var _CB_CANCELYN = [{"code":"Y","label":"정상"},{"code":"N","label":"취소"}];
var _CB_CALCYN = [{"code":"Y","label":"적용"},{"code":"N","label":"미적용"}];
var _CB_OVERYN = [{"code":"%","label":"전체"},{"code":"Y","label":"과오납"}];
var _CB_RTTE_DIV = [{"code":"1","label":"예정"},{"code":"2","label":"확인"},{"code":"3","label":"반려"}];

var _FILE_PATH = 'D:\\CenterOP\\FileUpload';

//SKLEE
var _HR_APPOINT = [{"code":"Y","label":"지정"},{"code":"N","label":"미지정"}];





function getDataNextSeq(a_dg,a_col,a_pos){
	var maxseq = 0;
	for(i=1;i<=a_dg.RowCount();i++){
		if(_X.ToInt(a_dg.GetItem(i, a_col)) >= maxseq) maxseq = _X.ToInt(a_dg.GetItem(i, a_col));
	}
	if(a_pos != null){ 
		var rtnseq = _X.LPad(maxseq+1, a_pos, '0');
	} else {
		var rtnseq = maxseq+1;
	}		
	return rtnseq;
//	var max_seq = 0;
//	for(var i = 1; i <= a_grid.RowCount(); i++){
//		if(max_seq < Number(a_grid.GetItem(i,a_colname))){
//			max_seq = Number(a_grid.GetItem(i,a_colname));
//		}
//	}		
//	
//	
//	if(a_pos == null) a_pos = 
//	return max_seq += 1;
}


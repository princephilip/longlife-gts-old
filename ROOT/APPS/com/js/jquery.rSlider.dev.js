/*! 
* Print Slider v2.1.0
* http://rslider.com
* Copyright 2013, Mathieu BRUNO
*/
(function ( $, window, document, undefined ) {

    function rslider(element, options) {

        var self = this;
        self.options = $.extend( {}, defaults, options) ;
        self._name = 'pluginName';

        self.rSlider = $(element);
        self.slide = $('.slide', self.rSlider);

        self.nbslide = self.slide.length;
        self.array = [];
        self.i = 0;
        self.current_slide;
        self.prev_slide;
        self.busy = true;
        self.onair = null;
        self.callback_onair = null;
        self.one = true;
        self.oneinit = true;
        self.oneslide = false;
        self.pause;
        self.clear;

        var $imgs = $('img', self.rSlider),
            limg = $imgs.length,
            i_img=0;

        self.slide.hide().css('position', 'absolute');
        self.rSlider.css('overflow', 'visible').prepend('<div class="loading" />');

        if (self.nbslide === 0){
            self.rSlider.append('<div class="rs_center"><p class="noslide">'+self.options.message+'</p></div>');
            $('.loading', self.rSlider).fadeOut();
            return false;
        };

        function apShuffle(o) { for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x); return o; };
        self.slide.each(function(index){ self.array.push(index) });

        if(self.options.suffle){
            apShuffle(self.array);
        };
        self.current_slide = self.array[self.nbslide-1];
        self.prev_slide = self.array[0];

        if(self.options.pagination){
            self.rSlider.append('<ul class="pagination"/>');
            $('.pagination').hide();
            self.slide.each(function(){
                $('.pagination', self.rSlider).append('<li/>');
            });
            if(self.options.pagControl){
                $('.pagination li', self.rSlider).css('cursor', 'pointer');
            };
        };

        if(self.options.arrowsNav){
            self.rSlider.append('<div class="arrowsNav"> <span class="prev"></span> <span class="next"></span> </div>');
            $('.arrowsNav', self.rSlider).hide();
            $('.next', self.rSlider).click(function(){
                self.next();
                if(self.options.autoplay && !self.pause) self._autoplay(); 
            });
            $('.prev', self.rSlider).click(function(){
                self.previous();
                if(self.options.autoplay && !self.pause) self._autoplay();
            });
        };

        if(self.options.callback_onair) {
            self._callback_onair();
            $(window).on('scroll resize', function(){
                self._callback_onair();
            });
        };

        // start init
        if(limg>0){
            $imgs.each(function(){
                $(this).load(function(){
                    if (i_img+1==limg) {
                        $('.loading', self.rSlider).fadeOut();
                        if(self.options.callback_init) self.options.callback_init();
                        if(!self.options.publicstarting) {
                            if(self.options.playOnAir){
                                self._onair();
                                $(window).on('scroll resize', function(){
                                    self._onair();
                                });
                            }else{
                                self.onair = true;
                                self.init();
                            };
                        }else{self.onair = true;};
                    } else {
                        i_img++; 
                    };
                }).attr( 'src', $(this).attr('src') );
            });
        } else {
            $('.loading', self.rSlider).hide();
            if(self.options.callback_init) self.options.callback_init();
            if(!self.options.publicstarting) {
                if(self.options.playOnAir){
                    self._onair();
                    $(window).on('scroll resize', function(){
                        self._onair();
                    });
                }else{
                    self.onair = true;
                    self.init();
                };
            }else{self.onair = true;};
        };

    };

    rslider.prototype = {

        init: function() {
            var self = this;

            if(self.oneinit && !self.oneslide){

                self.busy = false;
                self.oneinit = false;

                if(self.options.keyboard) self._keyboard();

                if(self.options.arrowsNav){
                    var $arrowsNav = $('.arrowsNav', self.rSlider);
                    $arrowsNav.fadeIn(1000, function(){
                        if(self.options.autoHide){
                            $arrowsNav.delay(1000).fadeOut(1000);
                            self.rSlider.hover(function(){
                                $arrowsNav.stop(true, true).fadeIn(300);
                            },function(){
                                $arrowsNav.delay(1000).fadeOut(1000);
                            });
                        };
                    });
                };

                if(self.options.pagination && self.nbslide != 1 ){
                    $('.pagination', self.rSlider).delay(10).fadeIn(1000);
                };

                if( $('.rs_imgAutoResize', self.rSlider).length > 0 ){
                    $(window).resize(function(){
                        self.scale();
                    });
                };

                if ( self.options.callback_first ) self.options.callback_first(0);

                self.slide.eq(self.array[0]).fadeIn();
                self.scale();
                $('.pagination li', self.rSlider).eq(0).addClass('current');
                if(self.options.pagControl) self._pagControl();

                if(self.options.autoplay) self._autoplay();
                if(self.options.hoverpause) self._hover();

            };
        },

        _autoplay : function(p) {

            var self = this;

            clearInterval(self.clear);

            if(!p){
                self.clear = setInterval(function(){
                    self.next();
                }, self.options.interval);
            };
  
        },

        _hover : function(){

            var self = this;

            self.rSlider.on({
                mouseenter:(function(){
                    if(!self.pause && self.options.autoplay==true) self._autoplay(true);
                }), 
                mouseleave:(function(){
                    if(!self.pause && self.options.autoplay==true) self._autoplay();
                })
            });

        },

        play : function() {

            var self = this;

            self.pause = false;
            self._autoplay();

        },

        stop : function() {

            var self = this;

            self.pause = true;
            self._autoplay(true);

        },

        page : function(data) {

            var self = this;

            if (data>self.nbslide) data=self.nbslide;

            if (data==self.i) return false;

            if(data>self.i){

                self.i = data-1;
                self.next();

            } else {

                self.i = data+1;
                self.previous();

            };

        },

        next : function() {

            var self = this;

            if(self.options.onair) self._onair();

            if( self.i>=self.nbslide-1 && !self.options.loop && self.onair ) {
                self._autoplay(true);
                self._bounce('next');
            };

            if( self.i>=self.nbslide-2 && !self.options.loop ) self._autoplay(true);

            if( ( self.i>=self.nbslide-1 && !self.options.loop ) || self.busy || !self.onair ) return false;

            if(self.i>=self.nbslide-1){self.i=0}else{self.i++};
            self.current_slide = self.array[self.i];

            if (self.options.callback_begin) self.options.callback_begin();
            if (self.i == self.slide.length-1 && self.options.callback_last) {
                self.options.callback_last(self.i);
            } else if (self.i == 0 && self.options.callback_first) {
                self.options.callback_first(self.i);
            } else if (self.options.callback_inter) {
                self.options.callback_inter(self.i);
            };
            
            self._animate( 'next', self.prev_slide, self.current_slide, self.i );
            self.scale();

            self.prev_slide = self.current_slide;

            self.pagination = $('.pagination li', self.rSlider).eq(self.i);
            $('.pagination li', self.rSlider).removeClass('current');
            self.pagination.addClass('current');
        },

        previous : function() {

            var self = this;

            if(self.options.onair) self._onair();

            if ( self.i == 0 && !self.options.loop && self.onair ) self._bounce('prev');

            if ( (self.i == 0 && !self.options.loop) || self.busy || !self.onair ) return false;

            if (self.i == 0){self.i=self.nbslide-1;}else{self.i--;}
            self.current_slide = self.array[self.i];

            if(self.options.callback_begin) self.options.callback_begin();
            if (self.i == self.slide.length-1 && self.options.callback_last) {
                self.options.callback_last(self.i);
            } else if (self.i == 0 && self.options.callback_first) {
                self.options.callback_first(self.i);
            } else if (self.options.callback_inter) {
                self.options.callback_inter(self.i);
            };

            self._animate( 'prev', self.prev_slide, self.current_slide);
            self.scale();

            self.prev_slide = self.current_slide;

            self.pagination = $('.pagination li', self.rSlider).eq(self.i);
            $('.pagination li', self.rSlider).removeClass('current');
            self.pagination.addClass('current');
        },

        _bounce : function(type, index_prev, index_next ){
            var self = this,
                $slide_first = self.slide.eq(self.array[0]),
                $slide_last = self.slide.eq(self.array[self.nbslide-1]);

            if ( self.options.animType == 'slide' && type=='next' ){

                $slide_last.animate({'left': -25}, 200, 'swing', function(){
                    $(this).animate({ 'left': 0 }, 200, 'swing');
                });

            };
            
            if ( self.options.animType == 'slide' && type=='prev' ){

                $slide_first.animate({'left': 25}, 200, 'swing', function(){
                    $(this).animate({ 'left': 0 }, 200, 'swing');
                });

            };
        },

        _animate : function( type, index_prev, index_next ) {
            
            var self = this,
                $widthSlide = self.rSlider.width(),
                $heightSlide = self.rSlider.height(),
                $prev_slide = self.slide.eq(index_prev),
                $next_slide = self.slide.eq(index_next);

            self.busy = true;

            if(type=='next'){

                if ( self.options.animType == 'slide' ){

                    // placement, affichage et decalage du current slide 
                    $next_slide.css('left', $widthSlide).show().animate({'left': '0'}, self.options.transitionSpeed, self.options.easing, function() {
                        if(self.options.callback_end) self.options.callback_end(self.i);
                        self.busy = false; 
                    });
                    // decalage du slide et disparition
                    $prev_slide.animate({'left': -$widthSlide}, self.options.transitionSpeed, self.options.easing, function(){ $(this).hide(0); });

                } else if ( self.options.animType == 'slideVertical' ){

                    // placement, affichage et decalage du current slide 
                    $next_slide.css('top', $heightSlide).show().animate({'top': '0'}, self.options.transitionSpeed, self.options.easing, function() { 
                        if(self.options.callback_end) self.options.callback_end(self.i);
                        self.busy = false; 
                    });
                    // decalage du slide et disparition
                    $prev_slide.animate({'top': -$heightSlide}, self.options.transitionSpeed, self.options.easing, function(){ $(this).hide(0); });

                } else if ( self.options.animType == 'fade' ){

                    $next_slide.css('z-index', 1).fadeIn( self.options.transitionSpeed, function(){
                        $prev_slide.hide();
                        if(self.options.callback_end) self.options.callback_end(self.i);
                        self.busy = false;
                        $prev_slide.css('z-index', 0);
                    });
                    
                } else if ( self.options.animType == 'fadein' ){

                    $next_slide.css('z-index', 1).fadeIn( self.options.transitionSpeed, function(){
                        $prev_slide.fadeOut(self.options.transitionSpeed/2, function(){
                            if(self.options.callback_end) self.options.callback_end(self.i);
                            self.busy = false;
                        });
                        $prev_slide.css('z-index', 0);
                    });

                } else if ( self.options.animType == 'fadeout' ){

                    $prev_slide.css('z-index', 0).fadeOut(self.options.transitionSpeed, function(){
                        $next_slide.css('z-index', 1).fadeIn( self.options.transitionSpeed);
                        if(self.options.callback_end) self.options.callback_end(self.i);
                        self.busy = false;
                    });

                };

            } else if(type=='prev') {
                
                if ( self.options.animType == 'slide' ) {

                    // placement, affichage et decalage du current slide 
                    $next_slide.css('left', -$widthSlide).show().animate({'left': '0'}, self.options.transitionSpeed, self.options.easing, function () {
                        if(self.options.callback_end) self.options.callback_end(self.i);
                        self.busy = false;
                    });
                    // decalage du slide et disparition
                    $prev_slide.show().animate({'left': $widthSlide}, self.options.transitionSpeed, self.options.easing, function(){ $(this).hide(0); });
                     
                } else if ( self.options.animType == 'slideVertical' ){

                    // placement, affichage et decalage du current slide 
                    $next_slide.css('top', -$heightSlide).show().animate({'top': '0'}, self.options.transitionSpeed, self.options.easing, function () {
                        if(self.options.callback_end) self.options.callback_end(self.i);
                        self.busy = false;
                    });
                    // decalage du slide et disparition
                    $prev_slide.show().animate({'top': $heightSlide}, self.options.transitionSpeed, self.options.easing, function(){ $(this).hide(0); });

                } else if ( self.options.animType == 'fade' ){

                    $next_slide.css('z-index', 1).fadeIn(self.options.transitionSpeed, function(){
                        $prev_slide.hide();
                        if(self.options.callback_end) self.options.callback_end(self.i);
                        self.busy = false;
                        $prev_slide.css({'z-index': 0});
                    });

                } else if ( self.options.animType == 'fadein' ){

                    $next_slide.css('z-index', 1).fadeIn(self.options.transitionSpeed, function(){
                        $prev_slide.fadeOut(self.options.transitionSpeed/2, function(){
                            if(self.options.callback_end) self.options.callback_end(self.i);
                            self.busy = false;
                        });
                        $prev_slide.css({'z-index': 0});
                    });

                } else if ( self.options.animType == 'fadeout' ){

                    $prev_slide.fadeOut(self.options.transitionSpeed, function(){
                        $next_slide.css('z-index', 1).fadeIn(self.options.transitionSpeed);
                        $(this).css({'z-index': 0});
                        if(self.options.callback_end) self.options.callback_end(self.i);
                        self.busy = false;
                    });
 
                };

            }

            $prev_slide.css('z-index', 0);

        },

        _keyboard : function() {

            var self = this;

            $(document).keydown(function t(e){
                var posW = $(window).scrollTop(),
                    heiWindow = $(window).height()+2,
                    posSlider = self.rSlider.offset().top+1,
                    heiSlider = self.rSlider.height();

                if( 39 == (e.which) ){
                    if ( self.options.keyboardOnAir) {
                        if ( posSlider > posW && posSlider < posW+(heiWindow-heiSlider)) {
                            self.next();
                            if(self.options.autoplay && !self.pause) self._autoplay();
                        };
                    } else { 
                        self.next();
                        if(self.options.autoplay && !self.pause) self._autoplay();
                    };
                };
                if( 37 == (e.which) ){
                    if ( self.options.keyboardOnAir ) {
                        if ( posSlider > posW && posSlider < posW+(heiWindow-heiSlider)) {
                            self.previous();
                            if(self.options.autoplay && !self.pause) self._autoplay();
                        };
                    } else { 
                        self.previous();
                        if(self.options.autoplay && !self.pause) self._autoplay();
                    };
                };
            });
        },

        _pagControl : function() {

            var self = this;

            $('.pagination li', self.rSlider).each(function(index){

                $(this).click(function(){

                    if(index>self.i){

                        self.i = index-1;
                        self.next();

                    }else if(index<self.i){

                        self.i = index+1;
                        self.previous();

                    };

                });

            });

        },

        _callback_onair : function() {

            var self = this,
                posW = $(window).scrollTop(),
                heiWindow = $(window).height()+2,
                posSlider = self.rSlider.offset().top+1,
                heiSlider = self.rSlider.height();

            if ( posSlider > posW && posSlider < posW+(heiWindow-heiSlider)) {
                if( !self.callback_onair || (self.callback_onair==null) ) {
                    if(self.options.callback_onair) self.options.callback_onair( self.rSlider, true );
                    self.callback_onair=true;
                };
            } else {
                if( self.callback_onair || (self.callback_onair==null) ){
                    if(self.options.callback_onair) self.options.callback_onair( self.rSlider, false );
                    self.callback_onair=false;
                };
            };

        },

        _onair : function() {

            var self = this,
                posW = $(window).scrollTop(),
                heiWindow = $(window).height()+2,
                posSlider = self.rSlider.offset().top+1,
                heiSlider = self.rSlider.height();

            if ( posSlider > posW && posSlider < posW+(heiWindow-heiSlider)) {
                if( !self.onair || self.onair==null ) {
                    self.onair=true;
                    if(self.one){
                        self.init();
                        self.one=false;
                    };
                };
            } else {
                if( self.onair ){
                    self.onair=false;
                };
            };

        },

        scale : function() {

            var self = this;

            $('.rs_imgAutoResize', self.rSlider ).each(function(index){

                 var $imgscale = $(this);

                function getNaturalWidth(img) {
                    if(typeof img.naturalWidth == "undefined") {
                        var temp_image = new Image();
                        temp_image.src = img.src;
                        return temp_image.width;
                    } else {
                        return img.naturalWidth;
                    }
                }

                function getNaturalHeight(img){
                    if(typeof img.naturalHeight == "undefined") {
                        var temp_image = new Image();
                        temp_image.src = img.src;
                        return temp_image.height;
                    } else {
                        return img.naturalHeight;
                    }
                }

                var $parent = $imgscale.parent(),
                    img_width =  getNaturalWidth( $imgscale[0] ),
                    img_height =  getNaturalHeight( $imgscale[0] ),
                    // img_width = $imgscale[0].naturalWidth,
                    // img_height = $imgscale[0].naturalHeight,
                    ratio1 = img_height/img_width,
                    ratio2 = img_width/img_height,
                    body_width = $parent.width()+2,
                    body_height = $parent.height()+2,
                    vertical, horizontal;

                $imgscale.css('position', 'absolute');

                if(!self.options.rs_imgAutoResize.overflow) $imgscale.parent().css('overflow', 'hidden');

                if ( $imgscale.data('rs-type') ) {
                    var type = $imgscale.data('rs-type');
                } else {
                    var type = self.options.rs_imgAutoResize.scale;
                };

                if( $imgscale.data('rs-posx') ) {
                    var posx = $imgscale.data('rs-posx');
                } else {
                    var posx = self.options.rs_imgAutoResize.posx;
                };

                if( $imgscale.data('rs-posy') ) {
                    var posy = $imgscale.data('rs-posy');
                } else {
                    var posy = self.options.rs_imgAutoResize.posy;
                };

                if ( type == 'cover' ) {
                    cover( $imgscale, posx, posy, ratio1, ratio2, body_width, body_height );
                } else if ( type == 'contain' ) {
                    contain( $imgscale, posx, posy, ratio1, ratio2, body_width, body_height );
                } else if ( type == 'adjust' ) {
                    adjust( $imgscale, body_width, body_height );
                } else if ( type == 'none' ) {
                    none( $imgscale, posx, posy, img_width, img_height, body_width, body_height );
                };

            });


            function cover( $el, posx, posy, ratio1, ratio2, body_width, body_height ) {
                var horizontal,
                    vertical;

                if ( posx == 'left' ) {
                    horizontal = '-1px';
                } else if ( posx == 'center' ) {
                    horizontal = Math.abs( (body_height * ratio2) - body_width) / -2 + 'px';
                } else if ( posx == 'right' ) {
                    horizontal = Math.ceil(body_width-(body_height * ratio2))+'px';
                };

                if ( posy == 'top' ) {
                    vertical = '-1px';
                } else if ( posy == 'center' ) {
                    vertical = Math.abs( (body_width * ratio1) - body_height ) / -2 + 'px';
                } else if ( posy == 'bottom' ) {
                    vertical = Math.ceil(body_height-(body_width * ratio1))+'px';
                };

                if (body_width / body_height >= ratio2) { 
                    $el.css({ 
                        'width': body_width + 'px',
                        'height': Math.ceil(body_width * ratio1) + 'px', 
                        'left':  '-1px',
                        'top': vertical
                    }); 
                } else { 
                    $el.css({ 
                        'width': Math.ceil(body_height * ratio2) + 'px', 
                        'height': body_height + 'px', 
                        'left': horizontal,
                        'top': '-1px'
                    }); 
                };
            };


            function contain( $el, posx, posy, ratio1, ratio2, body_width, body_height ) {
                var horizontal,
                    vertical;

                if ( posx == 'left' ) {
                    horizontal = '-1px';
                } else if ( posx == 'center' ) {
                    horizontal = Math.abs(((body_height*ratio2)-body_width)/2)+'px';
                } else if ( posx == 'right' ) {
                    horizontal = Math.ceil(body_width-Math.ceil(body_height*ratio2))+'px';
                };

                if ( posy == 'top' ) {
                    vertical = '-1px';
                } else if ( posy == 'center') {
                    vertical = Math.abs(((body_width*ratio1)-body_height)/2)+'px';
                } else if ( posy == 'bottom' ) {
                    vertical = Math.ceil(body_height-Math.ceil(body_width*ratio1))+'px';
                };

                if (body_width / body_height >= ratio2) {
                    $el.css({ 
                        'width': Math.ceil(body_height*ratio2)+'px', 
                        'height': body_height+'px', 
                        'left': horizontal,
                        'top': '-1px'
                    }); 
                } else { 
                    $el.css({ 
                        'width': body_width+'px', 
                        'height': Math.ceil(body_width*ratio1)+'px', 
                        'left': '-1px', 
                        'top': vertical
                    }); 
                };
            };

            function adjust( $el, body_width, body_height ) {

                $el.css({ 
                    'width': body_width-2 + 'px',
                    'height': body_height-2 + 'px',
                    'left':  '0px',
                    'top': '0px'
                });
            };

            function none( $el, posx, posy, img_width, img_height, body_width, body_height ) {
                
                var horizontal,
                    vertical;

                if ( posx =='left' ) {
                    horizontal = '0px';
                } else if ( posx =='center' ) {
                    horizontal = Math.ceil(body_width-img_width)/2 + 'px';
                } else if ( posx =='right' ) {
                    horizontal = Math.ceil(body_width-img_width)-2+'px';
                };

                if ( posy == 'top' ) {
                    vertical = '0px';
                } else if ( posy == 'center' ) {
                    vertical = Math.ceil(body_height-img_height)/2 + 'px';
                } else if ( posy == 'bottom' ) {
                    vertical = Math.ceil(body_height-img_height)-2+'px';
                };

                $el.css({ 
                    'left': horizontal,
                    'top': vertical
                });
            };

        }
    };

    $.fn.rSlider = function ( options ) {
        return this.each(function () {
            if (!$.data(this, 'rSlider')) {
                $.data(this, 'rSlider', 
                new rslider( this, options ));
            };
        });
    };

    var defaults = {

        animType        : 'slide',          // fade, fadein, fadeout, slideVertical or slide
        loop            : true,             // true or false
        suffle          : false,            // true or false
        transitionSpeed : 1000,             // 'slow' 'fast' or number (milliseconde)
        easing          : 'swing',          // 'linear', 'swing' and more

        arrowsNav       : false,            // true or false
        autoHide        : false,            // true or false
        keyboard        : false,            // true or false
        pagination      : false,            // true or false
        pagControl      : true,

        onair           : false,
        
        autoplay        : true,             // true or false
        interval        : 7000,             // 'slow', 'fast' or number (milliseconde)
        hoverpause      : false,            // true or false
        playOnAir       : false,            // true or false
        publicstarting  : false,            // true or false

        rs_imgAutoResize:{
            scale       : 'contain',        // contain - cover - adjust - none
            posx        : 'center',         // left - center - right
            posy        : 'center',         // top - center - bottom
            overflow    : false             // true or false
        },

        message         : 'Ooops... no slide to see !',

        callback_init   : null,
        callback_first  : null,
        callback_inter  : null,
        callback_last   : null,
        callback_begin  : null,
        callback_end    : null,
        callback_onair  : null
    };


})( jQuery, window, document );






﻿/**
 * Object :  common.js
 * @Description : 업무별 공통 javascript libaray
 * @author :엑스인터넷정보 김양열
 * @since : 2013.02.10
 * @version : 1.2.0
 *
 * @Modification Information
 * <pre>
 *   since    	  author           Description
 *  ----------    --------    ---------------------------
 *   2013.02.10    김양열        최초생성
 */

//$.ui.dialog.prototype._focusTabbable = function(){};

var _FIND_BTN_PARAM = [
				//{text: "선택", class : 'btn btn-success', click: function() { $("#if_findmodal").get(0).contentWindow.x_Confirm(); $("#findmodal").dialog("focus");}, autofocus: false},
				{text: "선택", class : 'btn btn-success', click: function() { $("#if_findmodal").get(0).contentWindow.x_Confirm();}, autofocus: false},
				{text: "취소", class : 'btn btn-default', click: function() { $("#findmodal").dialog("close"); }, autofocus: false}
			];


/*
var _FIND_BTN_PARAM = {
				"선택": function() { $("#if_findmodal").get(0).contentWindow.x_Confirm(); $("#findmodal").dialog("focus");},
				"취소": function() { $("#findmodal").dialog("close"); }
			};
*/

//공통의 자원을 이용한 find 찾기
_X.OpenFindWin = function(a_win, a_param, a_title, a_height, a_width) {
	var url = mytop._CPATH+"/XF010.do?" + a_param;
	a_win._FindCode.returnValue = null;
	a_win._FindCode.title = a_title;

	if (isIE) {
		//_X.OpenModal(a_url, a_width, a_height, a_left, a_top, a_win, a_resizable)
		var rtValue = _X.OpenModal(url, a_width, a_height, null, null, a_win, true);
		if(rtValue!=null) {
			try {
				a_win._FindCode.returnValue = rtValue;
				if(typeof(x_ReceivedCode)!="undefined") {
					a_win.x_ReceivedCode(rtValue);
				}
				$("#"+a_win._FindCode.finddiv).focus();
			}
			catch(e) {}
		}
	} else {
		var modal = "<div id='findmodal'><iframe src='" + url + "' class='iframe_popup' frameborder=0 marginwidh=0 marginheight=0></iframe></div>";
		$(modal).dialog({title:a_title, show:false, width:a_width, height:a_height, modal:true, resizable:true,
				close:function(){
								try {
									if(typeof(x_ReceivedCode)!="undefined") {
										$(this).remove();
										//x버튼 클릭 시 x_ReceivedCode(null); 호출 (2014.10.01 이상규)
										if(a_win._FindCode.returnValue == null) x_ReceivedCode(null);
									}else{
										$(this).remove();}
										$("#"+a_win._FindCode.finddiv).focus();
									}
								catch(e) {}
								}
		});
	}
};

//OpenFindWinDo로 소스 통합(2015.07.15 KYY)
_X.OpenFindWinDo = function(a_win, a_template, a_param, a_title, a_width, a_height, a_buttons) {
	//구분자 변경 (2018.11.04 KYY)
	var url = mytop._CPATH+"/" + a_template + ".do?" + _X.StrReplace(a_param,'|',_Col_Separate);
	a_win._FindCode.returnValue = null;
	a_win._FindCode.title = a_title;

	if (isIE) {
		//IE9이하 버전 모달창 메모리 문제로 수정(2015.07.15 KYY)
		/*
		var rtValue = _X.OpenModal(url, a_width, a_height, null, null, a_win, false);
		if(rtValue!=null) {
			try {
				a_win._FindCode.returnValue = rtValue;
				if(typeof(x_ReceivedCode)!="undefined") {
					a_win.x_ReceivedCode(rtValue);
				}
				$("#"+a_win._FindCode.finddiv).focus();
			}
			catch(e) {}
		}
		*/
		var popWin = _X.OpenPopup(url, "", a_width, a_height +20, null, null, a_win);
		_X.Block();
		popWin.focus();
		//0.5초단위로 윈도우 닫혔는지 체크
		var timer = setInterval(function() {
			try {
		    if(popWin.closed) {
	        clearInterval(timer);
					_X.UnBlock();
					//a_win._FindCode.returnValue = popWin;
					/*
					if(typeof(a_win.x_ReceivedCode)!="undefined") {
						a_win.x_ReceivedCode(a_win._FindCode.returnValue);
					}
					*/
					$("#"+a_win._FindCode.finddiv).focus();
					popWin = null;
        	//alert('closed');
		    }
		  }
			catch(e) {
	      clearInterval(timer);
				_X.UnBlock();
				popWin = null;
			}
		},500);

	} else {
		var modal = "<div id='findmodal'><iframe id='if_findmodal' src='" + url + "' class='iframe_popup'  frameborder=0 marginwidh=0 marginheight=0></iframe></div>";
		$(modal).dialog({
				title: a_title,
				show: false,
				width: a_width,
				height: a_height,
				modal: true,
				resizable: true,
				buttons: a_buttons, //버튼 추가(2015.05.12 KYY)
				open: function() {
						$("#for_ime").click(function(){
								$("#for_ime").hide();
								$("#if_findmodal").get(0).contentWindow.FIND_CODE.focus();
						});
						//$(this).parents('.ui-dialog').attr('tabindex', -1)[0].focus();
				},
				close:function(){
						try {
							if(typeof(x_ReceivedCode)!="undefined") {
								$(this).remove();
								//x버튼 클릭 시 x_ReceivedCode(null); 호출 (2014.10.01 이상규)
								if(a_win._FindCode.returnValue == null) x_ReceivedCode(null);
							}else{$(this).remove();}
								$("#"+a_win._FindCode.finddiv).focus();
							}
						catch(e) {}
				},
				create:function () {
				}
		});
	}
};

//OpenFindWinDo로 소스 통합(2015.07.15 KYY)
_X.OpenFindWin2 = function(a_win, a_param, a_title, a_width, a_height) {
	_X.OpenFindWinDo(a_win, "XF020", encodeURI(a_param), a_title, a_width, a_height, _FIND_BTN_PARAM);
};

_X.OpenFindWin3 = function(a_win, a_param, a_title, a_width, a_height, a_buttons) {
	_X.OpenFindWinDo(a_win, "XF030", encodeURI(a_param), a_title, a_width, a_height, a_buttons);
};

_X.OpenFindWin4 = function(a_win, a_param, a_title, a_width, a_height, a_buttons) {
	_X.OpenFindWinDo(a_win, "XF040", encodeURI(a_param), a_title, a_width, a_height, a_buttons);
};

/*=== FreeForm DataSet Test (2015.06.10 이상규) ===*/
_X.OpenFindWin5 = function(a_win, a_param, a_title, a_width, a_height, a_buttons) {
	_X.OpenFindWinDo(a_win, "XF050", encodeURI(a_param), a_title, a_width, a_height, a_buttons);
};

//find code code 자동호출
function x_FindCode(a_dg, a_row, a_findKey, a_findstr, a_win) {
	if(typeof(x_FindCode2)!="undefined") {
		if(x_FindCode2(a_dg, a_row, a_findKey, a_findstr, a_win)!=100)  return;
	}
	if(a_findstr==null) a_findstr="";
	if(a_win==null) a_win=window;

	if(a_dg==null) {
		switch(a_findKey) {
			case "COST_DEPT_CODE":
			case "COST_DEPT_NAME":
					_FindCode.grid_row = 1;
					_X.CommonFindCode(window,"원가부서 찾기","hr",a_findKey, a_findstr, new Array(mytop._CompanyCode), "FindDeptCode|HrFindCode|FindDeptCode",500, 500);
				break;
		}
	} else {
		switch(a_findKey) {
			case "PALLOW_CODE":
			case "PALLOW_NAME":
					_FindCode.grid_row = a_row;
					_X.CommonFindCode(window,"수당찾기","hr",a_findKey, a_findstr, new Array(mytop._CompanyCode,'%'), "FindAllowCode|HrFindCode|FindAllowCode",500, 500);
				break;
			case "BANK_CODE":
			case "BANK_NAME":
					_FindCode.grid_row = a_row;
					_X.CommonFindCode(window,"은행찾기","hr",a_findKey, a_findstr, new Array(), "FindBankCode|HrFindCode|FindBankCode",500, 500);
				break;
		}
	}
}

function x_ReceivedCode(a_returnValue) {

	if(_FindCode.finddiv == "OpenApprLine"){

	  var ls_apprid = _FindCode.returnValue.appr_id;
	  var l_data = _X.XmlSelect("ea", "EA01010", "R_EA01010_ENDYN", [ls_apprid] , "json2");
    var ls_endyn = l_data[0].end_yn;
    if(ls_endyn=='Y'){
  	  var ls_orderid = _FindCode.returnValue.order_id;
  	  var ls_doctype = _FindCode.returnValue.doc_type;
  	  var ls_report = _X.GetReportName(ls_doctype);
  	  var ls_specid = '0';
  	  var ls_params = "as_orderid="+ ls_orderid + "&as_doctype=" + ls_doctype + "&as_apprid=" + ls_apprid+ "&as_specid=" + ls_specid+ "&as_userid=";
      ls_params = ls_params + "&as_pdfname=" + ls_apprid + "&as_pdfdir=EA";
      var l_rtn = _X.MakeReportPDF(ls_report, ls_params);
    }

	}

	if(typeof(x_ReceivedCode2)!="undefined") {
		if(x_ReceivedCode2(a_returnValue)!=100)  return;
	}

	if (_FindCode.grid==null||_FindCode.grid==undefined) {
		switch(_FindCode.finddiv){
			case "COST_DEPT_CODE":
			case "COST_DEPT_NAME":
				COST_DEPT_CODE.value =  _FindCode.returnValue.DEPT_CODE;
				COST_DEPT_NAME.value =  _FindCode.returnValue.DEPT_NAME;
				break;
		}
	} else {

		switch(_FindCode.finddiv){
			case "PALLOW_CODE":
			case "PALLOW_NAME":
				_FindCode.grid .SetItem(_FindCode.grid_row, "PALLOW_CODE", _FindCode.returnValue.ALLOW_CODE);
				_FindCode.grid .SetItem(_FindCode.grid_row, "PALLOW_NAME", _FindCode.returnValue.ALLOW_NAME);
				break;
		}
	}
}

//공토코드찾기
_X.CommonFindCode = function(a_win, a_title, a_sub, a_find_div, a_findstr, a_findcode, a_grid, a_height, a_width, a_multi_yn, a_searchColName) {
	a_win.name = 'CommonFindCode';
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	//파인드창 멀티선택 여부 (2014.10.07 이상규)
	a_win._FindCode.findmulti = typeof(a_multi_yn) == "undefined" ? false : a_multi_yn;
	if(a_searchColName != null) a_win._FindCode.findsearchcolname = a_searchColName;
	var findCode = a_grid.split('|');
	var param = "sys="+a_sub+"&fcd="+findCode[0]+encodeURIComponent(a_title)+"&msize=2&fgrid="+a_grid;
	_X.OpenFindWin2(a_win, param, a_title, a_height, a_width);
};

_X.MakeReportPDF = function(a_report, a_param){

  var ls_param = a_param;
	var la_temp = a_param.split("&");
	var ls_doctype = "";

	for(var i=0;i<la_temp.length;i++){
		var ar = la_temp[i].split("=");
    //최초, 최종 구분 추가
		if(ar[0]=='as_doctype'){
			ls_doctype = ar[1];
    	switch(ls_doctype){
    		case "E1":
    			ls_param = ls_param + "&as_reportdiv=최초";
    			break;
    		case "E9":
    			ls_param = ls_param + "&as_reportdiv=최종";
    			break;
    	}
    }
	}

	var ls_enc_param = "";
	var la_param = ls_param.split("&");
	for(var i=0;i<la_param.length;i++){
		var ar = la_param[i].split("=");
		ls_enc_param = ls_enc_param + "&" + ar[0] + "=" + encodeURIComponent(encodeURI(ar[1],"UTF-8"));
	}
  var s_param = "rptURL=/APPS/ea/report/"+encodeURIComponent(encodeURI(a_report,"UTF-8"))+ls_enc_param;

	var ls_retVAL = 1;
	var ls_url = "/Mighty/jsp/iReportMakePDF.jsp";

	$.ajax({
		type: 'POST',
		url : ls_url,
		data: s_param,
		dataType: 'text',
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		async: false,
		cache: false,
		timeout: 300000,
		success: function(data) {
					//_X.MsgBox("확인", "성공");

		},
		error: function(data, status, err) {
			_X.MsgBox("확인", "실패하였습니다."+ err);
			ls_retVAL = -1;
		}
	});

	//견적서일 경우 고객용 견적서 생성
	if(ls_doctype=="E1" || ls_doctype=="E9"){
		var ls_report2 = "고객견적서";
		var ls_param2 = a_param.replace("as_pdfdir=EA", "as_pdfdir=CUST");
		ls_param2 = ls_param2.replace("as_doctype="+ls_doctype, "as_doctype=");
		_X.MakeReportPDF(ls_report2, ls_param2);

/*
		ls_report2 = "견적서(총괄견적서)_온라인_메인";
		ls_param2 = a_param.replace("as_pdfdir=CUST", "as_pdfdir=CUST_TOT");
		ls_param2 = ls_param2.replace("as_doctype="+ls_doctype, "as_doctype=");
		_X.MakeReportPDF(ls_report2, ls_param2);
*/
	}

	return ls_retVAL;
};

_X.ReadPDF = function(a_dir, a_file){

	var ls_url = "/filePDF/" + a_dir + "/" + a_file + ".pdf";

	$.ajax({
		type: 'POST',
		url : ls_url,
		data: '',
		dataType: 'pdf',
		contentType: "application/pdf; charset=UTF-8",
		async: false,
		cache: false,
		timeout: 300000,
		success: function(data) {
					//_X.MsgBox("확인", "성공");

		},
		error: function(data, status, err) {
			_X.MsgBox("확인", "실패하였습니다."+ err);
			ls_retVAL = -1;
		}
	});

	return ls_retVAL;
};


_X.InsertCmSaleAmtModify = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=com&fcd=InsertCmSaleAmtModify&fnm="+encodeURIComponent('매출업체 및 금액변경')+"&msize=0&fgrid=InsertCmSaleAmtModify|InsertCmSaleAmtModify|R_InsertCmSaleAmtModify_01";
	_X.OpenFindWin3(a_win, param, '매출업체 및 금액변경', 600, 550);
};
_X.insertCmSaleAmtModify = _X.InsertCmSaleAmtModify;



//===========================================================================================
// 울트라 호환성으로 추가(2015.08.24 KYY)
//===========================================================================================
var _CB_BUILD_DIV = [{"code":"","label":" "},{"code":"1000","label":"사무실"},{"code":"2000","label":"창고"},{"code":"3000","label":"집배송장"},{"code":"4000","label":"작업장"},{"code":"5000","label":"회의실"}];
var _CB_INCUST_DIV = [{"code":"","label":" "},{"code":"1","label":"입주업체"},{"code":"2","label":"거래소업체"},{"code":"3","label":"일반임대업체"}];
var _CB_REPORT_DIV = [{"code":"","label":" "},{"code":"1","label":"사업장단위"},{"code":"2","label":"사업자단위"}];
var _CB_YN = [{"code":"","label":" "},{"code":"Y","label":"예"},{"code":"N","label":"아니오"}];
var _CB_CASHYN = [{"code":"","label":" "},{"code":"Y","label":"완납"},{"code":"N","label":"미납"}];
var _CB_YN_NN = [{"code":"Y","label":"예"},{"code":"N","label":"아니오"}];
var _CB_INOUT_TAG = [{"code":"","label":" "},{"code":"1","label":"입고"},{"code":"2","label":"출고"}];
var _CB_INOUT_TAG_NN = [{"code":"1000","label":"입고"},{"code":"2000","label":"출고"}];
var _CB_CLOSE_DIV = [{"code":"1000","label":"종합거래"},{"code":"2000","label":"운영관리"},{"code":"3000","label":"품질유통"}];
var _CB_CINOUT_DIV = [{"code":"","label":" "},{"code":"3000","label":"입하입차"},{"code":"4000","label":"배송입차"},{"code":"5000","label":"반품입차"}];
var _CB_PERIOD_DIV = [{"code":"1","label":"월"},{"code":"2","label":"분기"},{"code":"3","label":"반기"},{"code":"4","label":"년"}];
var _CB_QUARTER_DIV = [{"code":"1","label":"1분기"},{"code":"2","label":"2분기"},{"code":"3","label":"3분기"},{"code":"4","label":"4분기"}];
var _CB_HALF_DIV = [{"code":"1","label":"상반기"},{"code":"2","label":"하반기"}];
var _CB_PRINT_DIV = [{"code":"1","label":"거래명세서"},{"code":"2","label":"상차지시서"}];
var _CB_PRINT_DIV2 = [{"code":"1","label":"친환경"},{"code":"2","label":"일반"}];
var _CB_CONT_DIV = [{"code":"","label":" "},{"code":"CONT","label":"시설계약"},{"code":"RENT","label":"임대계약"}];

var _CB_KIND_PRINT = [{"code":"1","label":"입주증 발급"},{"code":"2","label":"열쇠인수증 발급"}];
var _CB_PERIOD_PRINT = [{"code":"1","label":"기간별 입주예약배정내역"},{"code":"2","label":"기간별 입주증발급내역"},{"code":"3","label":"기간별 열쇠인수증발급내역"},{"code":"4","label":"기간별 시설물입주증발급내역"}];
var _CB_REG_PRINT = [{"code":"1","label":"등기내역"},{"code":"2","label":"미등기내역"}];
var _CB_PROCESS = [{"code":"%","label":"전체"},{"code":"Y","label":"처리"},{"code":"N","label":"미처리"}];
var _CB_CANCELYN = [{"code":"Y","label":"정상"},{"code":"N","label":"취소"}];
var _CB_CALCYN = [{"code":"Y","label":"적용"},{"code":"N","label":"미적용"}];
var _CB_OVERYN = [{"code":"%","label":"전체"},{"code":"Y","label":"과오납"}];

var _FILE_PATH = 'D:\\CenterOP\\FileUpload';
var _HR_APPOINT = [{"code":"Y","label":"지정"},{"code":"N","label":"미지정"}];
//2016-11-16 ParkHs
var _CB_CONFIRM_YN = [{"code":"1","label":"예정"},{"code":"2","label":"확인"},{"code":"3","label":"반려"}];
var _CB_RCV_DIV = [{"code":"1","label":"원도급"},{"code":"2","label":"변경"}];
var _CB_MONEY_STD = [{"code":"1","label":"￦"},{"code":"2","label":"＄"}];
var _CB_SLIP_DIV = [{"code":"AP","label":"AP"},{"code":"GL","label":"GL"}];
var _CB_CLOSE_YN = [{"code":"Y","label":"완료"},{"code":"N","label":"미완료"}];
var _CB_SEX_DIV = [{"code":"F","label":"여"},{"code":"M","label":"남"}];


function getDataNextSeq(a_dg,a_col,a_pos){
	var maxseq = 0;
	for(i=1;i<=a_dg.RowCount();i++){
		if(_X.ToInt(a_dg.GetItem(i, a_col)) >= maxseq) maxseq = _X.ToInt(a_dg.GetItem(i, a_col));
	}
	if(a_pos != null){
		var rtnseq = _X.LPad(maxseq+1, a_pos, '0');
	} else {
		var rtnseq = maxseq+1;
	}
	return rtnseq;
}

// 2016.04.06
function x_FindRow(a_dg, a_col, a_findval) {
	for(var i=1; i<=a_dg.RowCount(); i++) {
		if(a_dg.GetItem(i, a_col)+"" == a_findval+"")
			return i;
	}
	return 0;
}


_X.SlipInfoApply = function(a_slipid, a_msgshow, a_slipdate) {

	//if( _X.MsgBoxYesNo( "확인", "전표를 적용 하시겠습니까?" ) === "2" ) return;
  var param = new Array( a_slipid, mytop._UserID, a_slipdate);

  var l_rtn   = _X.XmlSelect("cm", "SLIP", "APPLY_SLIP", param , "array");
  var rtValue = {};
  rtValue.result = l_rtn[0][0];
  rtValue.msg = l_rtn[0][1];

  if(a_msgshow){
    if(rtValue.result!="Y"){
      _X.MsgBox("확인", rtValue.msg);
    }else{
      _X.MsgBox("확인","전표에 적용 되었습니다.");
    }
  }
	return rtValue;

};


// 문자보내기 (단건)
_X.SendSMS = function(a_from_tel, a_to_tel, a_to_name, a_msg, a_ref_div, a_ref_id, a_ref_key, a_ref_type, a_user) {
  var ls_type = '1'; // 단문 sms

  // 리얼로 SMS 전송
  var li_rtn = _X.ExecSql("ss", "SMS", "SM_SMS_REAL_SEND", new Array(a_to_tel, a_from_tel, a_msg));
  //var li_rtn = _X.ExecSql("ss", "SMS", "SM_SMS_TEMP_SEND", new Array(a_to_tel, '07040109061', a_msg));

	if(li_rtn==1) {
		// SMS 전송로그 저장 (Linked Server의 접속 계정에 Select 권한을 부여하지 않아서 발송과 로그 분리됨)
		var li_rtn = _X.ExecSql("com_site","SendSMS", "SEND_SMS", new Array(a_user, ls_type, a_from_tel, a_to_name, a_to_tel, a_msg, a_ref_div, a_ref_id, a_ref_key, a_ref_type));

	  if(li_rtn==1){
	  	_X.MsgBox('확인', 'SMS를 보냈습니다.');
	  } else {
	  	_X.MsgBox('확인', 'SMS를 보낸 후, 로그 저장에 실패했습니다.');
	  }
	} else {
		_X.MsgBox('확인', 'SMS 보내기가 실패했습니다.');
	}
}

//pdf 파일 열기
_X.OpenPdf = function(a_folder, a_file){
	var ls_file = "/filePDF/" + a_folder + "/" + a_file + ".pdf";
  var pop = window.open(ls_file,"firstSheet","width=1024,height=768, scrollbars=yes, resizable=yes");
  pop.focus();
}

//관련 pdf파일 보기
_X.OpenOrderPdf = function (a_orderid, a_doctype){
  var l_data = _X.XS("com_site", "BmOrderCommon", "BM_GET_APPRID", [a_orderid, a_doctype] , "json2");
  var ls_apprid = l_data[0].appr_id;
  _X.OpenPdf('EA', ls_apprid);

}

//20161018 세방

//공통코드 리스트
_X.CommXmlSelect = function(a_sysid, a_dcode) {
	return _X.XmlSelect("com", "COMMON", "SM_COMCODE_D" , new Array(a_sysid, a_dcode), 'json2');
}
_X.commXmlSelect = _X.CommXmlSelect;

// _X.FindEmpHead = function(a_win, a_find_div, a_findstr, a_findcode) {
// 	if( $.isArray(a_findcode) && a_findcode.length==1) {a_findcode[1]='123';}
// 	a_win._FindCode.finddiv = a_find_div;
// 	a_win._FindCode.findstr = a_findstr;
// 	a_win._FindCode.findcode = a_findcode;
// 	var param = "sys=com&fcd=FindEmp&fnm="+encodeURIComponent('사원 찾기')+"&msize=0&fgrid=FindEmp|FindCode|FindEmpHead_00";
// 	_X.OpenFindWin2(a_win, param, '사원 찾기', 500, $(document).height()-300);
// };
// _X.findEmpHead = _X.FindEmpHead;

//거래처 찾기 2016.11.16
_X.FindCustCode = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=com&fcd=FindCustCode&fnm="+encodeURIComponent('거래처 찾기   (***한/영 변환이 안되면 이곳을 더블클릭하세요***)')+"&msize=0&fgrid=FindCustCode|FindCode|FindCustCode";
	_X.OpenFindWin2(a_win, param, '거래처 찾기   (***한/영 변환이 안되면 이곳을 더블클릭하세요***)', 600, $(document).height()-300, _FIND_BTN_PARAM);
};
_X.findCustCode = _X.FindCustCode;

_X.FindMenu = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win.name = 'FindMenu';
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=com&fcd=FindMenu&fnm="+encodeURIComponent('메뉴 찾기')+"&msize=0&fgrid=FindMenu|FindCode|FindMenu_01";
	_X.OpenFindWin2(a_win, param, '메뉴 찾기', 500, $(document).height()-300);
};
_X.findEmp = _X.FindEmp;


//그리드의 해당 필드의 MAX값을 리턴(SEQ(숫자)에 사용)
_X.GetGridMaxSeq = function(a_dg, a_colname, a_flag, a_val, a_colname2){
	var li_seq = 0;
	if(a_flag == null || a_flag == "") a_flag = false;

	if(a_flag) {
		for(var i = 1; i <= a_dg.RowCount(); i++) {
			if(a_dg.GetItem(i, a_colname) == a_val) {
				var ls_num = _X.ToInt(a_dg.GetItem(i, a_colname2))
				if(li_seq < ls_num) li_seq = ls_num;
			}
			if(li_seq < li_seq2) li_seq = li_seq2;
		}
	} else {
		for(var i = 1; i <= a_dg.RowCount(); i++) {
			var li_seq2 = _X.ToInt(a_dg.GetItem(i, a_colname));
			if(li_seq < li_seq2) li_seq = li_seq2;
		}
	}
	return li_seq;
}


//그리드의 해당 컬럼의 가장 큰 날짜를 가져온다 (그리드, 컬럼명, 포멧타입, 행번호)
_X.GetGridMaxDate = function(a_dg, a_colname, a_format, a_row){
	var ls_rows = a_dg.RowCount();
	var ls_max_date = _X.ToDate("19000101");

	for(var i = 0; i < ls_rows; i++) {
		if(i + 1 == a_row) continue;

		ls_date = a_dg.GetItem(i + 1, a_colname);
		if(ls_date.length < 8) ls_date += "01";
		var ls_date = _X.ToDate(_X.ToString(ls_date, "YYYYMMDD"));

		if(_X.DaysAfter(ls_max_date, ls_date) > 0) ls_max_date = ls_date;
	}
	return _X.ToString(ls_max_date, a_format);
}

//도로명주소 찾기
_X.FindStreetAddr = function(a_win, a_find_div, a_findstr, a_findcode) {

	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var pop = window.open(mytop._CPATH+"/zip_code/jusoPopup.jsp","pop","width=570,height=420, scrollbars=yes, resizable=yes");
	pop.focus();
};
_X.findStreetAddr = _X.FindStreetAddr;

//도로명 주소 파라미터 받음
_X.jusoCallBack = function (roadFullAddr,roadAddrPart1,addrDetail,roadAddrPart2,engAddr,jibunAddr,zipNo,admCd,rnMgtSn,bdMgtSn, a_win, a_find_div, a_findstr, a_findcode){
	var retVal = {
			"roadFullAddr" : roadFullAddr,
			"roadAddrPart1": roadAddrPart1,
			"addrDetail"   : addrDetail,
			"roadAddrPart2": roadAddrPart2,
			"engAddr"      : engAddr,
			"jibunAddr"    : jibunAddr,
			"zipNo"        : zipNo,
			"admCd"        : admCd,
			"rnMgtSn"      : rnMgtSn,
			"bdMgtSn"      : bdMgtSn
	};
	x_ReceivedCode(retVal);
}

_X.FindBankCode = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win.name = 'FindBankCode';
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var tHeight = $(document).height()-400;
	if(tHeight<450) tHeight = 450;

	var param = "sys=com&fcd=FindBankCode&fnm="+encodeURIComponent('은행 찾기   (***한/영 변환이 안되면 이곳을 더블클릭하세요***)')+"&msize=0&fgrid=FindBankCode|FindBankCode|FindBankCode";
	_X.OpenFindWin2(a_win, param, '은행 찾기   (***한/영 변환이 안되면 이곳을 더블클릭하세요***)', 400, tHeight, _FIND_BTN_PARAM);
};
_X.findBankCode = _X.FindBankCode;

//실행권한 체크
_X.HaveActAuth = function(a_act_name, a_proj_code) {
	//alert(mytop._MyActAuth);
	var findStr = a_act_name + (a_proj_code != null && a_proj_code != '' ? '_' + a_proj_code : '');
	if((a_act_name == '소장' || a_act_name == 'PM') && a_proj_code=='%') {
		//수정(2017.09.25 KYY)
		findStr = '_' + a_act_name;
	}
	return (mytop._MyActAuth.indexOf(findStr) >=0);
};

//해당월의 첫번째 일요일 (2017.05.25 KYY)
_X.GetFirstSunday = function(a_yyyymm) {
	var l_rtn = _X.XmlSelect("com", "COMMON", "FIRST_SUNDAY", [a_yyyymm] , "array");
	return (l_rtn==null || l_rtn==''? 1 : parseInt(l_rtn[0][0]));
};

//본사인지 확인
_X.IsHeadoffice = function() {
	if(mytop._DeptCode=="01001"||mytop._DeptCode=="0000") {
		return true;
	}

	return false;
};

// SMARTEDITOR CREATE
_X.CreateEditor = function(a_elPlaceHolder) {
	// 추가 글꼴 목록
	//var aAdditionalFontSet = [["MS UI Gothic", "MS UI Gothic"], ["Comic Sans MS", "Comic Sans MS"],["TEST","TEST"]];
	var aAdditionalFontSet = [["맑은 고딕","맑은 고딕"]];

	nhn.husky.EZCreator.createInIFrame({
		oAppRef: oEditors,
		elPlaceHolder: a_elPlaceHolder,
		sSkinURI: "/Mighty/3rd/se2/SmartEditor2Skin.htm",
		fCreator: "createSEditor2",
		htParams : {
			bUseToolbar : true,				// 툴바 사용 여부 (true:사용/ false:사용하지 않음)
			bUseVerticalResizer : true,		// 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
			bUseModeChanger : true,			// 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
			//bSkipXssFilter : true,		// client-side xss filter 무시 여부 (true:사용하지 않음 / 그외:사용)
			aAdditionalFontList : aAdditionalFontSet,		// 추가 글꼴 목록
			fOnBeforeUnload : function(){
				//alert("완료!");
			}
		}
	});
};


// 지사코드찾기
_X.FindDeptCode = function(a_find_div, a_findstr) {
	_X.CommonFindCode(window,"지사코드 찾기","com",a_find_div,a_findstr,[mytop._UserID],"FindDeptCode|FindCode|FIND_CODE_DEPT", 700, 700);
}

// 지사코드찾기2
_X.FindDeptCode2 = function(a_find_div, a_findstr) {
	_X.CommonFindCode(window,"지사코드 찾기","com",a_find_div,a_findstr,[mytop._UserID],"FindDeptCode|FindCode|FIND_CODE_DEPT2", 700, 700);
}

// 물류센터코드찾기
_X.FindSalecenterCode = function(a_find_div, a_findstr) {
	_X.CommonFindCode(window,"물류센터 코드 찾기","com",a_find_div,a_findstr,[mytop._UserID],"FindDeptCode|FindCode|FIND_CODE_SALECENTER", 700, 700);
}

// 보장기관찾기
_X.FindComdivCode = function(a_find_div, a_findstr) {
	_X.CommonFindCode(window,"보장기관 찾기","com",a_find_div,a_findstr,[mytop._UserID],"FindComdivCode|FindCode|FIND_CODE_COMDIV", 700, 700);
}

// 지사선택(멀티)
_X.SelectDept = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win.name = 'SelectDept';
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=com&fcd=SelectDept&fnm="+encodeURIComponent('지사선택')+"&msize=0&fgrid=SelectDept|SelectDept|SelectDept";
	_X.OpenFindWin3(a_win, param, '지사선택', 550, 800);
};

// 사원선택(멀티)
_X.SelectEmp = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win.name = 'SelectEmp';
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=com&fcd=SelectEmp&fnm="+encodeURIComponent('사원선택')+"&msize=0&fgrid=SelectEmp|SelectEmp|SelectEmp";
	_X.OpenFindWin3(a_win, param, '사원선택', 600, 800);
};


// 공지사항 팝업
_X.FindBoard = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win.name = 'FindBoard';
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=com&fcd=FindBoard&fnm="+encodeURIComponent('공지사항')+"&msize=0&fgrid=FindBoard|FindCode|FindBoard";
	_X.OpenFindWin3(a_win, param, '공지사항',1000,600);
};


// 마지막 센터 -_gsDeptCode
_X.SetDept = function() {
	if(typeof(S_DEPT_ID)!="undefined" && S_DEPT_ID.value != null && mytop._gsDeptCode != null){
		$('#S_DEPT_ID').selectBox('destroy');
		$("#S_DEPT_ID").val(mytop._gsDeptCode);
		$('#S_DEPT_ID').selectBox();
	}
}

_X.SaveDept = function(a_val) {
	if(a_val != '%'){
		mytop._gsDeptCode = a_val;
	}
}

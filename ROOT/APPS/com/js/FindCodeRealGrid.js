var _IsModal = (window.dialogArguments?true:false);
var _IsPopup = (window.opener?true:false);
var _Caller  = (_IsModal?window.dialogArguments:(_IsPopup?window.opener:window.parent));
var _AutoRetrieve = true;

var options_dg_1 = {
		panel: {visible: false},
		footer: {visible: false},
		checkBar: {visible: _Caller._FindCode.findmulti == null ? false : _Caller._FindCode.findmulti},
		statusBar: {visible: false},
		select: {style: RealGrids.SelectionStyle.ROWS},
    edit: {
			insertable: false,
			appendable: false,
			updatable: false,
			deletable: false
    }
};

var findsearchCnt = 0;

//화면 디자인관련 요소들 초기화 작업
function x_InitForm() {
	if(typeof(xc_InitForm)!="undefined"){xc_InitForm();return;}
	if(typeof(_FIND_GRID)!="undefined") _X.InitGrid(grid_1, "dg_1", "100%", "100%", _FIND_SYS, _FIND_GRID, _FIND_QUERY+"|" + _FIND_SELECT);
	else _X.InitGrid(grid_1, "dg_1", "100%", "100%", _FIND_SYS, _FIND_CODE, "FindCode|" + _FIND_CODE);
	
	//최초 그리드 조회 후 한번만 검색조건 항목을 표시한다.(2015.03.19 이상규)
	if(_Caller._FindCode.findstr != null && _Caller._FindCode.findstr != "") {
		FIND_CODE.value = _Caller._FindCode.findstr;
		$("#FIND_CODE").focus();
	}
	$("#FIND_CODE").click(function() {
		if(findsearchCnt == 1) {
			FIND_CODE.value = "";
			$("#FIND_CODE").css("color", "#000000").css("opacity", "1");
			findsearchCnt = 2;
		}
	});
	//FIND_CODE.focus();
}

function x_DAO_Retrieve(){	
	//dg_1.Retrieve(new Array('%신한%','20'));		
	if(typeof(xc_DAO_Retrieve)!="undefined" ){
		xc_DAO_Retrieve();
		return;
	}

	//if(FIND_CODE.value.length<2){alert("2자 이상의 검색문자열을 입력해 주세요"); $('#FIND_CODE').focus(); return;}
	//dg_1.Retrieve(new Array('%' + FIND_CODE.value+ '%'));	
	
	if(_Caller._FindCode.findcode=="undefined" || _Caller._FindCode.findcode == null){
	//if(_Caller._FindCode.findcode)=="undefined"){
		dg_1.Retrieve(new Array('%' + FIND_CODE.value+ '%'));	
	}else{
	  var ls_code =_Caller._FindCode.findcode;
	  var ls_arr = new Array('%' + FIND_CODE.value+ '%');
	  for(var i=0; i<ls_code.length; i++){
	  	ls_arr[i+1] = ls_code[i];
	  }
	  dg_1.Retrieve(ls_arr);		
	}
}

function x_DAO_Save(){}
function x_DAO_Insert(row){}
function x_DAO_Delete(row){}
function x_DAO_Duplicate(row){}
function x_Duplicate_After(obj, rowIdx){}
function x_DAO_Excel(){dg_1.ExcelExport();}

function xe_GridDataLoad(a_dg){
	//최초 그리드 조회 후 한번만 검색조건 항목을 표시한다.(2015.03.19 이상규)
	if(_Caller._FindCode.findsearchcolname != null) {
		if(findsearchCnt <= 0) {
			FIND_CODE.value = "검색항목 : " + _Caller._FindCode.findsearchcolname;
			$("#FIND_CODE").css("color", "gray").css("opacity", "0.8");
			findsearchCnt = 1;
		}
	} else {
		$('#FIND_CODE').focus();
	}
}

function xe_EditChanged(a_obj, a_val, a_label, a_cobj){
	if(typeof(xc_EditChanged)!="undefined"){xc_EditChanged(a_obj, a_val, a_label, a_cobj);return;}
	
}

function xe_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){
	if(typeof(xc_InputKeyDown)!="undefined"){xc_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey);return;}

	if(a_obj==FIND_CODE && a_keyCode==KEY_DOWN)
		$('#dg_1').focus();
}

//입력 컨트롤에서 Enter키가 눌러진 경우 호출
function xe_InputKeyEnter(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	switch(a_obj.id) {
		case "FIND_CODE":
			x_DAO_Retrieve();
			break;
	}
}

function xe_GridRowFocusChange(a_dg, a_row, a_col){}
function xe_GridDoubleClick(a_dg, a_ctrlKey, a_altKey, a_shiftKey, a_stageX, a_stageY){}
function xe_GridItemDoubleClick(a_dg, a_row, a_col){
	if(typeof(xc_GridItemDoubleClick)!="undefined"){xc_GridItemDoubleClick(a_dg, a_row, a_col);return;}
	x_ReturnRowData(a_dg, a_row);
}

function xe_GridButtonClick(a_dg, a_row, a_col, a_colname){
	if(a_dg.id=="dg_1" && a_colname == "SELECT_RETURN") {
		x_ReturnRowData(a_dg, a_row);
	}
}

function x_ReturnRowData(a_dg, a_row){
	//파인드창 멀티선택여부 true 일 때 선택된 행들의 데이타객체를 배열로 넘김 (선택된 행이 없을 경우 현재 포커스의 row data를 넘김 data가 없을경우 null) (2014.10.07 이상규)
	if(_Caller._FindCode.findmulti) {
		var ls_rows = dg_1.GetCheckedRows();
		var ls_arr;
		if(ls_rows == null || ls_rows == "") {
			if(dg_1.RowCount() <= 0)
				ls_arr = null;
			else
				ls_arr = [dg_1.GetRowData(dg_1.GetRow())];
		} else {
			ls_arr = new Array();
			for(var i = 0; i < ls_rows.length; i++) {
				ls_arr.push(dg_1.GetRowData(ls_rows[i] + 1));
			}
		}
		if(_IsModal) {
			window.returnValue = ls_arr;
		} else {
			if(_Caller) {	
				_Caller._FindCode.returnValue = ls_arr;
				if(ls_arr != null)
					_Caller.x_ReceivedCode(ls_arr);
			}
		}
	} else {
		if(typeof(xc_ReturnRowData)!="undefined"){xc_ReturnRowData(a_dg, a_row);return;}
		var rtValue = a_dg.GetRowData(a_row);
		//var rtValue = a_dg.GetItem(a_row,);
		if(_IsModal) {
			window.returnValue = rtValue;
		} else {
			if(_Caller) {	
				_Caller._FindCode.returnValue = rtValue;
				_Caller.x_ReceivedCode(rtValue); 
				
				//_Caller._X.UnBlock();
			}
		}
	}
	setTimeout('x_Close()',0);
}

function x_Confirm(){
	//if (dg_1.GetRow()==0) return;
	x_ReturnRowData(dg_1, dg_1.GetRow());
}

function x_Close() {
	if(_IsModal || _IsPopup) {
		window.close();
	} else {
		if(_Caller.$("#findmodal").length>0?_Caller.$("#findmodal").dialog("close"):window.close());
	}
}

function xe_GridLayoutComplete(a_dg){
	if(typeof(a_dg)!="undefined") {
		if(_AutoRetrieve) {
				if(typeof(xc_DAO_Retrieve)!="undefined"){
				xc_DAO_Retrieve();
				return;
			}
			setTimeout("x_DAO_Retrieve()",0);
		}
	}
}
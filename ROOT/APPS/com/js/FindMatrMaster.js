//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 :
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
//
//===========================================================================================
var ls_cust = _Caller._FindCode.findcode[0];

function x_InitForm2(){

  var ls_large  = _X.XmlSelect("com", "FindAddRqst", "FindAddRqst_03" , new Array('') , 'json2');
  _X.DDLB_SetData(S_LARGE, ls_large, '%', false, true);
  var ls_middle = _X.XmlSelect("com", "FindAddRqst", "FindAddRqst_04" , new Array(S_LARGE.value) , 'json2');
  _X.DDLB_SetData(S_MIDDLE, ls_middle, '%', false, true);
  var ls_small  = _X.XmlSelect("com", "FindAddRqst", "FindAddRqst_05" , new Array(S_LARGE.value, S_MIDDLE.value) , 'json2');
  _X.DDLB_SetData(S_SMALL, ls_small, '%', false, true);

  _X.InitGrid(grid_1, "dg_1", "100%", "100%", "com", "FindMatrMaster", "FindMatrMaster|FindMatrMaster_01", false, false);
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
  if(ab_allcompleted){
  	//dg_1.SetCheckBar(true);
   //x_DAO_Retrieve2(dg_1);
  }
}

function x_DAO_Retrieve2(){
  var param = [S_LARGE.value, S_MIDDLE.value, S_SMALL.value, '', S_SEARCH.value];
  dg_1.Retrieve(param);
}

function xe_EditChanged2(a_obj, a_val){
  if(a_obj.id == 'S_LARGE'){
    var ls_middle = _X.XmlSelect("com", "FindAddRqst", "FindAddRqst_04" , new Array(a_val) , 'json2');
    _X.DDLB_SetData(S_MIDDLE, ls_middle, null, false, true);
    var ls_small = _X.XmlSelect("com", "FindAddRqst", "FindAddRqst_05" , new Array(S_LARGE.value, S_MIDDLE.value) , 'json2');
    _X.DDLB_SetData(S_SMALL, ls_small, null, false, true);
  }
  if(a_obj.id == 'S_MIDDLE'){
    var ls_small = _X.XmlSelect("com", "FindAddRqst", "FindAddRqst_05" , new Array(S_LARGE.value, a_val) , 'json2');
    _X.DDLB_SetData(S_SMALL, ls_small, null, false, true);
  }
  if(a_obj.id == 'S_SMALL'){
  }  
  x_DAO_Retrieve2(dg_1);
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
  return 100;
}

function x_DAO_ChkErr2(){
  return true;
}

function x_DAO_Saved2(){
  return 100;
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}

function x_Insert_After2(a_dg, rowIdx){
}

function x_DAO_Duplicate2(a_dg, row){
  return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg){
  return 100;
}

function x_DAO_Excel2(a_dg){
  return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
  return 1;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
  return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridDataLoad2(a_dg){

}

function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){
}

function x_ReceivedCode2(a_retVal){
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){
}

function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
  x_Confirm();
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){
}

function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){
}

function xe_ChartPointClick2(a_dg, a_event){
}

function xe_ChartPointSelect2(a_dg, a_event){
}

function xe_ChartPointMouseOver2(a_dg){
}

function xe_ChartPointMouseOut2(a_dg, a_event){
}

function x_Close2() {
  if(_IsModal || _IsPopup) {
    window.close();
  }else{
    if(_Caller.$("#findmodal").length>0?_Caller.$("#findmodal").dialog("close"):window.close());
  }
}

// function x_Confirm(){
// 	var checkedRows = dg_1.GetCheckedRows(true);
// 	if(checkedRows.length==0) {
// 		_X.MsgBox("자재를 선택 하세요.");
// 		return;
// 	}

// 	_Caller.pf_addcall(window.dg_1);

// }

function x_Confirm(){
  //if (dg_1.GetRow()==0) return;
  x_ReturnRowData(dg_1, dg_1.GetRow());
}

function x_ReturnRowData(a_dg, a_row){
  //파인드창 멀티선택여부 true 일 때 선택된 행들의 데이타객체를 배열로 넘김 (선택된 행이 없을 경우 현재 포커스의 row data를 넘김 data가 없을경우 null) (2014.10.07 이상규)
  if(_Caller._FindCode.findmulti) {
    var ls_rows = dg_1.GetCheckedRows();
    var ls_arr;
    if(ls_rows == null || ls_rows == "") {
      if(dg_1.RowCount() <= 0)
        ls_arr = null;
      else
        ls_arr = [dg_1.GetRowData(dg_1.GetRow())];
    } else {
      ls_arr = new Array();
      for(var i = 0; i < ls_rows.length; i++) {
        ls_arr.push(dg_1.GetRowData(ls_rows[i]));
      }
    }
    if(_IsModal) {
      window.returnValue = ls_arr;
    } else {
      if(_Caller) {
        _Caller._FindCode.returnValue = ls_arr;
        if(ls_arr != null)
          _Caller.x_ReceivedCode(ls_arr);
      }
    }
  } else {
    if(typeof(xc_ReturnRowData)!="undefined"){xc_ReturnRowData(a_dg, a_row);return;}
    var rtValue = a_dg.GetRowData(a_row);
    //var rtValue = a_dg.GetItem(a_row,);
    if(_IsModal) {
      window.returnValue = rtValue;
    } else {
      if(_Caller) {
        _Caller._FindCode.returnValue = rtValue;
        _Caller.x_ReceivedCode(rtValue);

        //_Caller._X.UnBlock();
      }
    }
  }
  // if(_AutoClose) {
    setTimeout('x_Close()',0);
  // }else{
  //   dg_1.SetRow(dg_1.GetRow());
  // }
}

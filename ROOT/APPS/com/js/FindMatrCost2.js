//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 : [FindMatrCost2.js]
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
//   Parkhs					엑스인터넷정보		 20170628 신규
//===========================================================================================

var _IsModal = (window.dialogArguments?true:false);
var _IsPopup = (window.opener?true:false);
var _Caller  = (_IsModal?window.dialogArguments:(_IsPopup?window.opener:window.parent));

function x_InitForm2(){
	if(typeof(_FIND_GRID)!="undefined")
		_X.InitGrid(grid_1, "dg_1", "100%", "100%", _FIND_SYS, _FIND_GRID, _FIND_QUERY+"|" + _FIND_SELECT);
	else
		_X.InitGrid(grid_1, "dg_1", "100%", "100%", _FIND_SYS, _FIND_CODE, "FindCode|" + _FIND_CODE);

}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	x_DAO_Retrieve2(dg_1);
}

function x_DAO_Retrieve2(a_dg){
 	var params = _Caller._FindCode.findcode
 	//console.log(params);
 	dg_1.Retrieve([params[0],
 								 params[1],
 								 params[2],
 								 params[3],
 								 params[4],
 								 params[5],
 								 params[6],
 								 _X.strPurify(sle_basicmonth.value)
 								]
 	);
}

function xe_EditChanged2(a_obj, a_val){
	if(a_obj.id == 'sle_basicmonth'){
		x_DAO_Retrieve2();
	}
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

function x_DAO_Saved2(){

}

function x_DAO_Insert2(a_dg, row){
	return 100;
}

function x_Insert_After2(a_dg, rowIdx){
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){

}

function x_DAO_Delete2(a_dg){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){

}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){

}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridItemFocusChanged(a_dg, a_newrow, a_newcol){

}

function xe_GridDataLoad2(a_dg){

}

function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){
}

function x_ReceivedCode2(a_retVal){
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){

}

function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
	x_Confirm();

}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){

}

function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){

}

function xe_ChartPointClick2(a_dg, a_event){

}

function xe_ChartPointSelect2(a_dg, a_event){

}

function xe_ChartPointMouseOver2(a_dg){

}

function xe_ChartPointMouseOut2(a_dg, a_event){

}

function x_Close2() {
	return 100;
}


function x_ReturnRowData(a_dg, a_row){
	if (dg_1.RowCount()==0) return;
  var rtValue = dg_1.GetRowData(dg_1.GetRow());
  if(_IsModal) {
    window.returnValue = rtValue;
  } else {
    if(_Caller) {
      _Caller._FindCode.returnValue = rtValue;
      _Caller.x_ReceivedCode(rtValue);
    }
  }
  setTimeout('x_Close2()',0);
}

function x_Confirm(){
	x_ReturnRowData(dg_1, dg_1.GetRow());
}

function x_Close2() {
	if(_IsModal || _IsPopup) {
		window.close();
	} else {
		if(_Caller.$("#findmodal").length>0?_Caller.$("#findmodal").dialog("close"):window.close());
	}
}

function xe_GridLayoutComplete(a_dg){
	if(typeof(a_dg)!="undefined") {
		if(_AutoRetrieve) {
				setTimeout(function(){x_DAO_Retrieve()});
			}
	}
}
/****************************************************************
 * 
 * 파일명 : mighty-find-1.0.0.js
 * 설  명 : 코드찾기 popup JavaScript
 * 
 *    수정일      수정자     Version        Function 명
 * ------------    ---------   -------------  ----------------------------
 * 2013.02.10    김양열       1.0.0            최초생성
 * 
 * 
 */
var _AF;
if (!_AF) {
	_AF = {};
}

//어음등록, 일괄수령등록
_AF.BillRegister = function(a_win, a_find_div, a_findstr, a_findcode) {
	//alert(a_find_div+';'+a_findstr+';'+a_findcode);
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=af&fcd=BillRegister&fnm="+encodeURIComponent('어음등록 일괄수령')+"&msize=0&fgrid=BillRegister|BillRegister|BillRegister";
	//alert(param);
	_X.OpenFindWin3(a_win, param, '어음등록 일괄수령', 400, 250);
};
_AF.billRegister = _AF.BillRegister;
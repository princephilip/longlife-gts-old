var _Caller = window.dialogArguments;

//화면 디자인관련 요소들 초기화 작업
function x_InitForm()
{

	if(typeof(xc_InitForm)!="undefined"){xc_InitForm();return;}
  
	_X.Grid(grid_1, "dg_1", "100%", "100%", "com", _FIND_CODE, "FindCode|" + _FIND_CODE);
	
	
	rMateGridInit();	
	//rMateGridInit();	
	
	
	FIND_CODE.value = _Caller._FindCode.findstr;
	//alert(FIND_CODE.value);
}

function x_DAO_Retrieve()
{
	if(typeof(xc_DAO_Retrieve)!="undefined"){xc_DAO_Retrieve();return;}
	//if(FIND_CODE.value.length<2){alert("2자 이상의 검색문자열을 입력해 주세요"); $('#FIND_CODE').focus(); return;}
	//dg_1.Retrieve(new Array('%' + FIND_CODE.value+ '%'));		
	
	if(typeof(_Caller._FindCode.findcode) =="undefined"){

		dg_1.Retrieve(new Array('%' + FIND_CODE.value+ '%'));	
		
	}else{
	
	  var ls_code =_Caller._FindCode.findcode;
	  var ls_arr = new Array('%' + FIND_CODE.value+ '%');
	  
	  for(var i=0; i<ls_code.length; i++){
	  	ls_arr[i+1] = ls_code[i];
	  }
   
		dg_1.Retrieve(ls_arr);	
		
	}	
}

function x_DAO_Save(){}
function x_DAO_Insert(row){}
function x_DAO_Delete(row){}
function x_DAO_Duplicate(row){}
function x_Duplicate_After(obj, rowIdx){}
function x_DAO_Excel(){dg_1.ExcelExport();}

function xe_GridDataLoad(a_obj){
	$('#FIND_CODE').focus();
}

function xe_EditChanged(a_obj, a_val, a_label, a_cobj)
{
	if(typeof(xc_EditChanged)!="undefined"){xc_EditChanged(a_obj, a_val, a_label, a_cobj);return;}
	
}

function xe_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey)
{
	if(typeof(xc_InputKeyDown)!="undefined"){xc_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey);return;}

	if(a_obj==FIND_CODE && a_keyCode==KEY_ENTER)
		x_DAO_Retrieve();

	if(a_obj==FIND_CODE && a_keyCode==KEY_DOWN)
		$('#dg_1').focus();
}

function xe_GridRowFocusChange(a_obj, a_row, a_col){}

function xe_GridDoubleClick(a_obj, a_ctrlKey, a_altKey, a_shiftKey, a_stageX, a_stageY){alert('9');}

function xe_GridItemDoubleClick(a_obj, a_row, a_col){
	if(typeof(xc_GridItemDoubleClick)!="undefined"){xc_GridItemDoubleClick(a_obj, a_row, a_col);return;}
	x_ReturnRowData(a_obj, a_row);
}



function x_ReturnRowData(a_obj, a_row){

	if(typeof(xc_ReturnRowData)!="undefined"){xc_ReturnRowData(a_obj, a_row);return;}
	_Caller._FindCode.returnValue = a_obj.GetRowData(a_row);
	
	window.returnValue = _Caller._FindCode.returnValue;
	alert();
	window.close();
}

function x_Close() {window.close();}

function xe_GridLayoutComplete(a_obj){
	if(typeof(a_obj)!="undefined") {
		
		if(typeof(xc_DAO_Retrieve)!="undefined"){setTimeout("xc_DAO_Retrieve()",100) ;return;}
		
		setTimeout("x_DAO_Retrieve()",100);
	}

}
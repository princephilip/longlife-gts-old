/**
 *<pre>
 * Statements
 *</pre>

 * @Class Name : IM01010.js
 * @Descriptio	 거래처등록
 * @Modification Information
 * @
 * @   수정일       수정자               수정내용
 * @ ----------    --------    ---------------------------
 * @  20160425 			srJin					최초수정작성
 */


 var o_local = {};
     o_local.trade_arr = ["10","20"];


 var _IsModal = (window.dialogArguments?true:false);
 var _IsPopup = (window.opener?true:false);
 var _Caller  = (_IsModal?window.dialogArguments:(_IsPopup?window.opener:window.parent));
 var _IsSearch = false; // 검색이 탔을때, 1건이면 자동 선택하기 위한 변수. true 일때, 결과가 1건이면 자동선택

function x_InitForm2(){
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "com", "FindAddCustCode", "FindAddCustCode|FindAddCustCode_01", true, true);

	$(".search_find_img").click(function() {
		pf_ZipFind($(this).attr("id"));
	});

	return 100;
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {

		//드랍다운 구성데이터 쿼리 매개변수에 담음.
		var	comboData   = _X.XmlSelect("sm", "SM01022", "R_SM01022_03", new Array('SM','CUSTTAG')		 			 	 , "json2");			 //거래처구분
		var	comboData2  = _X.XmlSelect("sm", "SM01022", "R_SM01022_03", new Array('SM','TRADETYPE')	 			 	 , "json2");		 	 //거래유형
		var	comboData3  = _X.XmlSelect("sm", "SM01022", "R_SM01022_03", new Array('SM','BUSINESSTYPE')			 , "json2");       //과세정보
		var	comboData4  = _X.XmlSelect("sm", "SM01022", "R_SM01022_03", new Array('SM','PAYGROUP')	 				 , "json2");			 //지급그룹

		// 현업에서 거래처코드 등록시 무조건 일반으로 등록 2017-11-24 관리팀 요청
		comboData = [{"code":"1", "label":"일반"}];
		_X.DDLB_SetData(CUST_TAG			, comboData 		, null, false, false); 	 //거래처구분 	드랍다운리스트 셋팅 . 쿼리에서 code, label 알리아스 지정해줘야함.
		_X.DDLB_SetData(TRADE_TYPE		, comboData2		, null, false, false); 	 //거래유형 		드랍다운리스트 셋팅
		_X.DDLB_SetData(BUSINESS_TYPE	, comboData3		, null, false, false); 	 //과세정보 		드랍다운리스트 셋팅
		_X.DDLB_SetData(PAY_GROUP			, comboData4		, null, false, false); 	 //지급그룹 		드랍다운리스트 셋팅

		dg_1.SetCombo("CUST_TAG"		 , comboData );			// 그리드 코드->한글 , grid 파일에서 lookupDispay:true 걸어줘야함. 쿼리에서 code, label 알리아스 지정해줘야함.
		dg_1.SetCombo("TRADE_TYPE"	 , comboData2);			// 그리드 코드->한글
		dg_1.SetCombo("BUSINESS_TYPE", comboData3);			// 그리드 코드->한글
		dg_1.SetCombo("PAY_GROUP"		 , comboData4);			// 그리드 코드->한글

		if(_Caller._FindCode.findstr!=""){
			FIND_CODE.value = _Caller._FindCode.findstr;
			x_DAO_Retrieve2(dg_1);
		}
	}
	return 100;
}

function x_DAO_Retrieve2(a_dg){
	dg_1.Retrieve(new Array('%' + FIND_CODE.value+ '%'));

	return 0;
}

function xe_EditChanged2(a_obj, a_val){
	if(a_obj.id == 'FIND_CODE' ){
		_IsSearch = true;
	}
	if(a_obj.id == 'S_USE_YESNO' ){
		x_DAO_Retrieve2(dg_1);
	}
	if(a_obj==CUST_TAG) {
		if(a_val=="2") {
			$(".detail_cust").inputmask("999999-9999999",{placeholder:" ", clearMaskOnLostFocus: true });
		} else {
			$(".detail_cust").inputmask("999-99-99999",{placeholder:" ", clearMaskOnLostFocus: true });
		}
	}
	return 100;
}

//enter key event 발생시 호출
function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	return 0;
}

//keydown event 발생시 호출
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){
	if(typeof(xc_InputKeyDown)!="undefined"){xc_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey);return;}

	if(a_obj==FIND_CODE && a_keyCode==KEY_ENTER)
		x_DAO_Retrieve();

	if(a_obj==FIND_CODE && a_keyCode==KEY_DOWN)
		$('#dg_1').focus();
}

//데이타 저장
function x_DAO_Save2(a_dg){

	return 100;
}

//데이타 저장후 callback
function x_DAO_Saved2(){
	//x_Confirm();

	return 100;
}

//데이타 유효성 check
function x_DAO_ChkErr2(){
	var cData = dg_1.GetChangedData();
	if(cData==null || cData=="") return true;

	// 거래처코드 중복체크
	var	li_rcnt = _X.XmlSelect("com", "FindAddCustCode", "CHK_EXIST_CUST", new Array( cData[0].data["CUST_CODE"] ), "json2");
	if(li_rcnt[0].rcnt>0) {
		chkMsg = "이미 등록된 거래처코드 입니다.";
		_X.MsgBox("확인", chkMsg);

		FIND_CODE.value = cData[0].data["CUST_CODE"];
		pf_new("N");
		return false;
	}

	var sNo = 0;
	var chkMsg = "";

	for(var i=0; i<cData.length; i++){

		sNo = cData[i].idx;

		if (cData[i].job=="I") {
			if (cData[i].data["CUST_NAME"] == null || cData[i].data["CUST_NAME"] == '' || cData[i].data["CUST_NAME"] == ' ') {
				chkMsg = "거래처명을 기입하시기 바랍니다.";
				_X.MsgBox("확인", chkMsg);
				return 0;
			}

			if (cData[i].data["CUST_CODE"] == null || cData[i].data["CUST_CODE"] == '' || cData[i].data["CUST_CODE"] == ' ') {
				chkMsg = "사업자번호를 기입하시기 바랍니다.";
				_X.MsgBox("확인", chkMsg);
				return 0;
			}

			if (cData[i].data["CUST_CODE"].indexOf('-')>0) {
				chkMsg = "사업자번호에 -를 제외하고 입력하시기 바랍니다.";
				_X.MsgBox("확인", chkMsg);
				return 0;
			}
			if (cData[i].data["REGISTER_NO"].indexOf('-')>0) {
				chkMsg = "법인등록번호에 -를 제외하고 입력하시기 바랍니다.";
				_X.MsgBox("확인", chkMsg);
				return 0;
			}

			if (cData[i].data["CUST_CODE"].trim().length != 10 && cData[i].data["CUST_CODE"].trim().length != 13 ) {
				chkMsg = "거래처 코드는 10자리로 입력해야 합니다.";
				_X.MsgBox("확인", chkMsg);
				return 0;
			}

			dg_1.SetItem(cData[i].idx, "INPUT_DATE", _X.strPurify(dg_1.GetItem(cData[i].idx, "INPUT_DATE")));
			dg_1.SetItem(cData[i].idx, "BUSINESS_CHK_DATE", _X.strPurify(dg_1.GetItem(cData[i].idx, "BUSINESS_CHK_DATE")));
			dg_1.SetItem(cData[i].idx, "CLOSING_DATE", _X.strPurify(dg_1.GetItem(cData[i].idx, "CLOSING_DATE")));
			dg_1.SetItem(cData[i].idx, "DEPOSIT_FROMDATE", _X.strPurify(dg_1.GetItem(cData[i].idx, "DEPOSIT_FROMDATE")));
			dg_1.SetItem(cData[i].idx, "DEPOSIT_TODATE", _X.strPurify(dg_1.GetItem(cData[i].idx, "DEPOSIT_TODATE")));
		}
	}

	return true;
}

//grid insert/add
function x_DAO_Insert2(a_dg, row){return 100;}

//grid insert 수행후 발생
function x_Insert_After2(a_dg, rowIdx){
	dg_1.SetItem(rowIdx, 'TRADE_TYPE', '20');
	dg_1.SetItem(rowIdx, 'BUSINESS_TYPE', 'A');
	dg_1.SetItem(rowIdx, 'PAY_GROUP', '10');

	// A: 등록, O: 승인, R: 반려
	dg_1.SetItem(rowIdx, 'CUSTOMER_USE', 'A');
	dg_1.SetItem(rowIdx, 'CUST_TAG', '1');
	dg_1.SetItem(rowIdx, 'USE_YN', 'Y');

	dg_1.SetItem(rowIdx, 'EMP_NO', mytop._UserID);
	dg_1.SetItem(rowIdx, 'INPUT_DATE', _X.ToString(_X.GetSysDate(),'yyyy-mm-dd') );
	// dg_1.SetItem(rowIdx, 'HT_NEW_YN', 'Y');
	// dg_1.SetItem(rowIdx, 'USE_YN', 'Y');
	// dg_1.SetItem(rowIdx, 'TRADE_TYPE', o_local.trade_arr[Tab_Idx]);
	// dg_1.SetItem(rowIdx, 'MEMB_STATE', 'Y');
	return 0;
}

//grid 복제
function x_DAO_Duplicate2(a_dg, row){return 100;}

//grid 복제후
function x_Duplicate_After2(a_dg, rowIdx){return 100;}

//grid 삭제
function x_DAO_Delete2(a_dg, row){
	return 100;
}

//excel 저장
function x_DAO_Excel2(a_dg){return 100;}

//출력
function x_DAO_Print2(){return 100;}

//tab 변경시
function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;}

//tab 변경시
function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

//grid data 변경시
function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){

	switch(a_col){
		case "CUST_TAG":
		case "CUST_NAME":
		case "OWNER":
		case "TRADE_TYPE":
		case "USE_YN":
			if(a_dg.GetItem(a_row, "HT_NEW_YN")=="N"){
				a_dg.SetItem(a_row, a_col, a_oldvalue);
				_X.MsgBox("확인", "ERP 연동된 거래처 데이터는 변경 불가합니다.");
				return false;
			}
			break;
	}

	return 0;
}

//grid row focus change
function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){

	return true;
}

//grid row focus change
function xe_GridRowFocusChanged2(a_dg, a_newrow, a_oldrow){
	// console.log(a_newrow)
	// $(dg_1).on("keydown", function(e) {
	// 	alert(0)
	// })
}

//item focus change event
function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){return true;}

//grid data loaded
function xe_GridDataLoad2(a_dg){
	if(_IsSearch&&a_dg.RowCount()==1) {
		x_ReturnRowData(a_dg, 1);
	}
	return 100;
}

//찾기창 호출 후
function x_FindCode2(a_dg, a_row, a_findKey, a_findstr, a_win) {
	return 100;
}


//찾기 화면에서 선택값을 넘긴 경우 호출
function x_ReceivedCode(a_retVal){
	switch(_FindCode.finddiv){
 		case 'POST_CODE':

			var ls_zipNo 			= a_retVal.zipNo;
			var ls_Addr 			= a_retVal.roadAddrPart1 + a_retVal.roadAddrPart2;
			var ls_AddrDetail = a_retVal.addrDetail;

			dg_1.SetItem(dg_1.GetRow(), 'ZIP' 	, ls_zipNo);
			dg_1.SetItem(dg_1.GetRow(), 'ADDR' 	, ls_Addr);
			dg_1.SetItem(dg_1.GetRow(), 'ADDR2' , ls_AddrDetail);

		break;

		case 'POST_CODE2':

			var ls_zipNo 			= a_retVal.zipNo;
			var ls_Addr 			= a_retVal.roadAddrPart1 + a_retVal.roadAddrPart2;
			var ls_AddrDetail = a_retVal.addrDetail;

			dg_1.SetItem(dg_1.GetRow(), 'RECEIVE_ZIP' 	, ls_zipNo);
			dg_1.SetItem(dg_1.GetRow(), 'RECEIVE_ADDR' 	, ls_Addr);
			dg_1.SetItem(dg_1.GetRow(), 'RECEIVE_ADDR2' , ls_AddrDetail);

		break;

		case 'BANK_CODE':

      var ls_bank_code = a_retVal.BANK_CODE;
      var ls_bank_name = a_retVal.BANK_NAME;

			dg_1.SetItem(dg_1.GetRow(), 'BANK_CODE' 	, ls_bank_code);
			dg_1.SetItem(dg_1.GetRow(), 'BANK_NAME' 	, ls_bank_name);

		break;

		case 'CUST_CODE':
			ls_cust_code = a_retVal.CUST_CODE ;

			x_DAO_Retrieve2(dg_1);

		break;

	}
}

//grid item button click event
function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){
  if(a_dg.id === 'dg_1'){
 	  if(a_col === 'SEL') {
 	  	if (a_dg.GetItem(a_row, "HT_CUST_TYPE")===""){
        _X.MsgBox("확인", "먼저 선택하신 고객사의 유형을 저장하세요.");
				return 0;
 	  	}
 	  	x_ReturnRowData(a_dg, a_row);
 	  }
  }
}

//header click event
function xe_GridHeaderClick2(a_dg, a_col, a_colname){return 100;}

//item double click event
function xe_GridItemDoubleClick2(a_obj, a_row, a_col){
	if(typeof(xc_GridItemDoubleClick)!="undefined"){
		xc_GridItemDoubleClick(a_obj, a_row, a_col);
		return;
	}

	x_ReturnRowData(a_obj, a_row);
}

function x_ReturnRowData(a_obj, a_row){

  var rtValue = a_obj.GetRowData(a_row);
  if(_IsModal) {
    window.returnValue = rtValue;
  } else {
    if(_Caller) {
      _Caller._FindCode.returnValue = rtValue;
      _Caller.x_ReceivedCode(rtValue);
    }
  }
  setTimeout('x_Close()',0);

}

//tree item checked event
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){return 100;}

//tree item checked event
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){return 100;}

//- anychart  point click event
function xe_ChartPointClick2(a_dg, a_event){return 100;}

//- anychart  point select event
function xe_ChartPointSelect2(a_dg, a_event){return 100;}

//- anychart  point mouse over event
function xe_ChartPointMouseOver2(a_dg){return 100;}

//- anychart  point mouse out event
function xe_ChartPointMouseOut2(a_dg, a_event){return 100;}

//window(mdi) close
function x_Close2() {

  if(_IsModal || _IsPopup) {
    window.close();
  }else{
    if(_Caller.$("#findmodal").length>0?_Caller.$("#findmodal").dialog("close"):window.close());
  }

}

function x_Confirm() {
	if(dg_1.RowCount() > 0 && dg_1.GetRow() > 0){
		x_ReturnRowData(dg_1, dg_1.GetRow());
  }
}

function xe_GridBeforeKeyDown2( a_obj, e, e_which, e_ctrlKey, e_altKey, e_shiftKey ) {
 	// 에디트 칼럼이 아닐때 이벤트 작동 안함 ..
 	return 100;
}

function pf_new(a_div) { // 175, 150 , 90, 120
	if(a_div=="Y") {
		dg_1.Reset();

		$("#ff_left,#ff_right,#freeform,#freeform2,#freeform3,#freeform4,#ff_btn").show();
		grid_1.ResizeInfo       = {init_width:1000, init_height:300, width_increase:1, height_increase:1};

		xm_BodyResize();
		x_DAO_Insert(dg_1);
	} else {
		dg_1.Reset();

		$("#ff_left,#ff_right,#freeform,#freeform2,#freeform3,#freeform4,#ff_btn").hide();
		grid_1.ResizeInfo       = {init_width:1000, init_height:650, width_increase:1, height_increase:1};

		xm_BodyResize();

		if(FIND_CODE.value.trim()=="") {
			dg_1.Reset();
		} else {
			x_DAO_Retrieve(dg_1);
		}

		//x_DAO_Delete(dg_1);
	}
}

//우편번호찾기
function pf_ZipFind(a_param) {
	var ls_Crow = dg_1.GetRow();
	if(ls_Crow <= 0) {return 0;}
		 switch (a_param){
		 	case 'btn_addrfind':
				_X.FindStreetAddr(window,"POST_CODE","", "");
			break;
			case 'btn_addrfind2':
				_X.FindStreetAddr(window,"POST_CODE2","", "");
			break;
			case 'btn_bankcode':
			 	_X.FindBankCode(window,"BANK_CODE", "", "");
			break;
		}
}
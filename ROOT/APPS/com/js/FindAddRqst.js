//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 :
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
//
//===========================================================================================
var is_proj = _Caller._FindCode.findcode[0];
var is_cust = _Caller._FindCode.findcode[1];
var is_Tab_no = 3;
var is_callElecOrd = ""; // 전산장비 발주화면에서 호출한경우 체크용

function x_InitForm2(){
  _X.Tabs(tabs_1, is_Tab_no);
  uf_tab_ch_style(is_Tab_no);
  _X.InitGrid(grid_3, "dg_3", "100%", "100%", "cm", "CM101525", "CM101525|CM101525_02", false, false);
  _X.InitGrid(grid_4, "dg_4", "100%", "100%", "pm", "FindMatrCustReq", "FindMatrCustReq|FindMatrCustReq_01", false, false);
  _X.InitGrid(grid_5, "dg_5", "100%", "100%", "pm", "FindMatrCustReq", "FindMatrCustReq|FindMatrCustReq_02", false, false);
  _X.InitGrid(grid_6, "dg_6", "100%", "100%", "com", "FindAddRqst", "FindAddRqst|FindAddRqst_06",false,false);
  _X.InitGrid(grid_7, "dg_7", "100%", "100%", "com", "FindAddRqst", "FindAddRqst|FindAddRqst_07", false, false);
  _X.InitGrid(grid_8, "dg_8", "100%", "100%", "com", "FindAddRqst", "FindAddRqst|FindAddRqst_08", false, false);
  _X.InitGrid(grid_9, "dg_9", "100%", "100%", "com", "FindAddRqst", "FindAddRqst|FindAddRqst_09", false, false);

  var ls_large  = _X.XmlSelect("com", "FindAddRqst", "FindAddRqst_03" , new Array('') , 'json2');
  _X.DDLB_SetData(S_LARGE, ls_large, '%', false, true);

  // 대분류 넘어왔을시 대분류 못고치게
  if(_Caller._FindCode.findcode[2] != null || _Caller._FindCode.findcode[2] != undefined) {
    S_LARGE.value = _Caller._FindCode.findcode[2];
    _X.FormSetDisable([S_LARGE], true);

    // 전산장비인경우 전체자재목록만 보이게(가로,세로,길이 숨김) -- 항목숨김은 xe_GridLayoutComplete 에서 처리
    if(_Caller._FindCode.findcode[2] == "25") {
      $("#tabs_1_nav_0").hide();
      $("#tabs_1_nav_1").hide();
      $("#tabs_1_nav_2").hide();
      $("#btn_past_temp").hide();
    }

  }

  // 대분류 넘어왔을시 대분류 못고치게
  if(_Caller._FindCode.findcode[3] != null || _Caller._FindCode.findcode[3] != undefined) {
    is_callElecOrd = _Caller._FindCode.findcode[3];
    // 전산장비 발주에서 호출한 경우 선택 버튼 안보이게 처리
    if(is_callElecOrd == "E") {
      $("#btn_mat_select").hide();
    }

  }


  var ls_middle = _X.XmlSelect("com", "FindAddRqst", "FindAddRqst_04" , new Array(S_LARGE.value) , 'json2');
  _X.DDLB_SetData(S_MIDDLE, ls_middle, '%', false, true);
  var ls_small  = _X.XmlSelect("com", "FindAddRqst", "FindAddRqst_05" , new Array(S_LARGE.value, S_MIDDLE.value) , 'json2');
  _X.DDLB_SetData(S_SMALL, ls_small, '%', false, true);

  var ls_ETC_ATT1 = _X.XmlSelect("sm", "COMMON", "SM_COMCODE_D" , new Array('PM', 'ETC_ATT1') , 'json2');
  dg_6.SetCombo("ETC_ATT1", ls_ETC_ATT1);
  var ls_USING_TAG = _X.XmlSelect("sm", "COMMON", "SM_COMCODE_D" , new Array('PM', 'USING_TAG') , 'json2');
  dg_6.SetCombo("USING_TAG", ls_USING_TAG);

  var ls_mproduct  = _X.XmlSelect("sm", "COMMON", "SM_COMCODE_D" , new Array('PM', 'MPRODUCT') , 'json2');
  dg_7.SetCombo("PRODUCT_TYPE", ls_mproduct);

  var ls_mmaterial = _X.XmlSelect("sm", "COMMON", "SM_COMCODE_D" , new Array('PM', 'MMATERIAL') , 'json2');
  var ls_mthk      = _X.XmlSelect("sm", "COMMON", "SM_COMCODE_D" , new Array('PM', 'MTHK') , 'json2');
  var ls_L1        = _X.XmlSelect("sm", "COMMON", "SM_COMCODE_D" , new Array('PM', 'ML1') , 'json2');

  var ls_producttag = _X.XmlSelect("sm", "COMMON", "SM_COMCODE_D" , new Array('PM', 'PRODUCT_TAG') , 'json2');
  var ls_ETC_ATT1 = _X.XmlSelect("sm", "COMMON", "SM_COMCODE_D" , new Array('PM', 'ETC_ATT1') , 'json2');

  dg_8.SetCombo('MATERIAL', ls_mmaterial);
  dg_8.SetCombo('THK' , ls_mthk);
  dg_8.SetCombo('L1' , ls_L1);
  dg_8.SetCombo('PRODUCT_TAG', ls_producttag);
  dg_8.SetCombo("ETC_ATT1", ls_ETC_ATT1);

  $("#btn-tempMatrCreate").click(function() {
    if(_Caller.dg_1.RowCount() <= 0 || _Caller.dg_1.GetItem(_Caller.dg_1.GetRow(), 'REQUEST_NO') == '' || _Caller.dg_1.GetItem(_Caller.dg_1.GetRow(), 'STATUS_CODE') != '1'){
      _X.MsgBox("확인", "청구서저장 후 또는 [자재등록] 상태일 때만 추가 하실 수 있습니다.");
      return 0;
    }
    var itemName = $("#S_ITEM_NAME").val();
    var itemStd  = $("#S_ITEM_STD").val();
    var itemUnit = $("#S_ITEM_UNIT").val();
    var remarks  = $("#S_REMARKS").val();

    if(itemName=="") {alert("품명을 입력해 주시기 바랍니다.");$("#S_ITEM_NAME").focus();return;}
    if(itemStd=="") {alert("규격을 입력해 주시기 바랍니다.");$("#S_ITEM_STD").focus();return;}
    if(itemUnit=="") {alert("단위를 입력해 주시기 바랍니다.");$("#S_ITEM_UNIT").focus();return;}

    var itemChk = _X.XS("com", "COMMON", "MatrExist", new Array(itemName, itemStd, itemUnit) , "json2");

    if(itemChk[0].item_code != "000000") {
      alert("자재코드가 이미 존재합니다.\r\n코드 : " + itemChk[0].item_code);
      return;
    }

    itemChk = _X.XS("com", "COMMON", "NextTempMatrCode", new Array() , "json2");

    $("#S_ITEM_CODE").val(itemChk[0].item_code);
    var itemCode = $("#S_ITEM_CODE").val();

    var param = new Array(itemCode, itemName, itemStd, itemUnit, remarks, mytop._UserID, mytop._ClientIP);
    var rtValue = _X.ExecProc('pm', 'SP_PM_CREATE_TEMP_MATR', param);

    if(rtValue < 1) {
      _X.Noty("임시자재코드 생성 에러!");
      return;
    }

    var newRow = _Caller.dg_2.InsertRow(0, null, false);

    _Caller.dg_2.SetItem(newRow, 'ITEM_CODE'  ,  itemCode);
    _Caller.dg_2.SetItem(newRow, 'ITEM_NAME'  ,  itemName);
    _Caller.dg_2.SetItem(newRow, 'ITEM_STD'   ,  itemStd);
    _Caller.dg_2.SetItem(newRow, 'ITEM_UNIT'  ,  itemUnit);
    _Caller.dg_2.SetItem(newRow, 'REMARKS'    ,  remarks);
    _Caller.dg_2.SetItem(newRow, "SORT_ORDER", (newRow > 1 ? _Caller.dg_2.GetItem(newRow -1, "SORT_ORDER") + 10: 10));

    alert("임시자재코드 생성이 완료되었습니다.");
    $("#btn-tempMatrClose").click();
  });

  $("#btn-tempMatrClose").click(function() {
    $("#S_ITEM_CODE").val("");
    $("#S_ITEM_NAME").val("");
    $("#S_ITEM_STD").val("");
    $("#S_ITEM_UNIT").val("");
    $("#S_REMARKS").val("");
  });

}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
  if(ab_allcompleted) {
    // 전산장비인경우 전체자재목록만 보이게(가로,세로,길이 숨김)
    if(_Caller._FindCode.findcode[2] == "25") {
      var columns = ["WIDTH", "HEIGHT", "AREA"];
      dg_6.SetColumnVisible(columns, false);
    }

    uf_tag2_input();
    xe_BodyResize();
    //x_DAO_Retrieve2();
    $("#S_FIND").focus();
  }
}

function x_DAO_Retrieve2(a_dg){
  var la_param = [mytop._CompanyCode, is_proj];
  var la_param2 = [S_LARGE.value, S_MIDDLE.value, S_SMALL.value, '', S_FIND.value];
  switch(is_Tab_no){
    case 0:
      la_param.push(is_cust)
      dg_3.Retrieve(la_param);
      break;
    case 1:
      la_param.push('%')
      dg_5.Reset();
      dg_4.Retrieve(la_param);
      break;
    case 2:
      la_param.push(S_MATR_NM.value)
      dg_8.Reset();
      dg_9.Reset();
      dg_7.Retrieve(la_param);
      break;
    case 3:
      if(S_LARGE.value=='%' && S_MIDDLE.value =='%' && S_SMALL.value =='%' && S_FIND.value == '') {
        _X.Noty("자재구분이나 검색 문자열을 입력해 주시기 바랍니다.");
        $("#S_FIND").focus();
        return;
      }
      dg_6.Retrieve(la_param2);
      break;
  }
}

function xe_EditChanged2(a_obj, a_val){
  if(a_obj.id == 'S_LARGE'){
    var ls_middle = _X.XmlSelect("com", "FindAddRqst", "FindAddRqst_04" , new Array(a_val) , 'json2');
    _X.DDLB_SetData(S_MIDDLE, ls_middle, null, false, true);

    var ls_small = _X.XmlSelect("com", "FindAddRqst", "FindAddRqst_05" , new Array(S_LARGE.value, S_MIDDLE.value) , 'json2');
    _X.DDLB_SetData(S_SMALL, ls_small, null, false, true);
    x_DAO_Retrieve(dg_6);
  }
  if(a_obj.id == 'S_MIDDLE'){
    var ls_small = _X.XmlSelect("com", "FindAddRqst", "FindAddRqst_05" , new Array(S_LARGE.value, a_val) , 'json2');
    _X.DDLB_SetData(S_SMALL, ls_small, null, false, true);
    x_DAO_Retrieve(dg_6);
  }
  if(a_obj.id == 'S_SMALL'){
    x_DAO_Retrieve(dg_6);
  }



//  x_DAO_Retrieve(dg_1);
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
   if(a_obj.id=="S_FIND") {
    x_DAO_Retrieve(dg_6);
  }
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
  return 100;
}

function x_DAO_ChkErr2(){
  return true;
}

function x_DAO_Saved2(){
  return 100;
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}

function x_Insert_After2(a_dg, rowIdx){
}

function x_DAO_Duplicate2(a_dg, row){
  return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg){
  return 100;
}

function x_DAO_Excel2(a_dg){
  return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
  return 1;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
  is_Tab_no = a_new_idx;
  uf_tab_ch_style(is_Tab_no);
  if(a_new_idx!=3) {
    x_DAO_Retrieve2();
  }
  xe_BodyResize();
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
  return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
   if(a_dg.id == 'dg_4' || a_dg.id == 'dg_7'){
    // var la_add_del = [["REQUEST_YYMM",dg_5], ["PRODUCT_TYPE",dg_8]];
    var ls_parm_T = a_dg.id == 'dg_4' ? 'REQUEST_YYMM' : 'PRODUCT_TYPE';
    var ls_dg     = a_dg.id == 'dg_4' ? dg_5 : dg_8;

    var ls_projcode   = a_dg.GetItem(a_newrow, "PROJECT_CODE");
    var ll_rqstno     = Number(a_dg.GetItem(a_newrow, "REQUEST_NO"));
    var ls_parm       = a_dg.GetItem(a_newrow, ls_parm_T);

    ls_dg.Retrieve([mytop._CompanyCode, ls_projcode, ls_parm, ll_rqstno]);
    if(a_dg.id == 'dg_7')dg_9.Retrieve([mytop._CompanyCode, ls_projcode, ls_parm, ll_rqstno]);
  }
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridDataLoad2(a_dg){

}

function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){
}

function x_ReceivedCode2(a_retVal){
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){
}

function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
  if(a_dg.id != 'dg_7' && a_dg.id != 'dg_8') {
    if(typeof(_Caller.pf_is_ductrqst)!="undefined" && _Caller.pf_is_ductrqst()) {
      _X.Noty("덕트청구 진행중입니다.<br>일반자재와 동시 청구 불가능합니다!!", null, null, 2000);
      return;
    }
  }
  if(a_dg.id == 'dg_3'){
    _Caller.pf_addcall_one(window.dg_3);
  }
  if(a_dg.id == 'dg_4'){
    if(_Caller.pf_addcall_1(window.dg_5) == 0)x_Close2();
  }
  if(a_dg.id == 'dg_5'){
    _Caller.pf_addcall_one(window.dg_5);
  }
  if(a_dg.id == 'dg_6'){
    if(a_dg.GetItem(a_row, "WHA_YN").indexOf('Y') > -1){
      var lo_ITEM0 = ["WIDTH", "HEIGHT", "AREA"];
      var lo_ITEM1 = ["W:","H:","L:"];
      var lo_ITEM2 = [];
      var ls_ITEM2 = "";
      for (var i = 0; i <=2; i++) {
          if(a_dg.GetItem(a_row, lo_ITEM0[i])) lo_ITEM2.push(lo_ITEM1[i]+a_dg.GetItem(a_row, lo_ITEM0[i]))
      }
      if(lo_ITEM2.length > 0){
        for (var i = 0; i <lo_ITEM2.length; i++) {
          if(ls_ITEM2.length > 0) ls_ITEM2 += ","
          ls_ITEM2 += lo_ITEM2[i]
        }
        a_dg.SetItem(a_row, "ITEM_NAME2", "_{"+ls_ITEM2+"}");
        a_dg.SetRowsState([a_row], "N");
      }else{
        _X.Noty("해당 자재는 사이즈를 입력해 주시기 바랍니다.!!", null, null, 2000);
        return;
      }
    }
    //_Caller.pf_addcall_one(window.dg_6, a_row, 'D');
    _Caller.pf_addcall_one(window.dg_6, a_row);
    dg_6.SetItem(a_row, "ITEM_QTY", 0);

    // 전산장비 발주 화면에서 연경우는 닫아줌
    if(is_callElecOrd == "E") x_Close2();
  }
  if(a_dg.id == 'dg_7'){
    //if(_Caller.pf_addcall_1(window.dg_5) == 0)x_Close2();
  }
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){
}

function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){
}

function xe_ChartPointClick2(a_dg, a_event){
}

function xe_ChartPointSelect2(a_dg, a_event){
}

function xe_ChartPointMouseOver2(a_dg){
}

function xe_ChartPointMouseOut2(a_dg, a_event){
}

function x_Close2() {
  if(_IsModal || _IsPopup) {
    window.close();
  }else{
    if(_Caller.$("#findmodal").length>0?_Caller.$("#findmodal").dialog("close"):window.close());
  }
}

function x_Confirm(){
  // var checkedRows = dg_1.GetCheckedRows(true);
  // if(checkedRows.length==0) {
  //  _X.MsgBox("청구할 내역을 선택 하세요.");
  //  return;
  // }
  if(a_dg.id == 'dg_3'){
    _Caller.pf_addcall_one(window.dg_3);
  }
  if(a_dg.id == 'dg_4'){
    if(_Caller.pf_addcall_1(window.dg_5) == 0)x_Close2();
  }
  if(a_dg.id == 'dg_5'){
    _Caller.pf_addcall_one(window.dg_5);
  }
  if(a_dg.id == 'dg_6'){
    _Caller.pf_addcall_one(window.dg_6);
  }
}

function pf_duct_rqst(){
  if(typeof(_Caller.pf_is_ductrqst)!="undefined" && !_Caller.pf_is_ductrqst() && _Caller.dg_2.RowCount()>0) {
    _X.Noty("일반자재 청구 진행중입니다.<br>덕트와 동시에 청구 불가능합니다!!", null, null, 2000);
    return;
  }
  if(_Caller.pf_addcall_1(window.dg_9, 'D') == 0) {
    x_Close2();
  }
}

function uf_tab_ch_style(as_new_idx){
  $('#tabsub_1,#tabsub_2,#tabsub_3,#tabsub_4,#S_SUB1,#S_SUB2,#S_SUB3,#S_TYPE1,#S_TYPE2,#S_TYPE3').hide();
  switch(as_new_idx){
    case  0:
          $('#tabsub_1,#S_TYPE1').show();
          break;
    case  1:
          // $('#tabsub_1,#tabsub_3,#tabsub_4,#S_SUB3,#S_TYPE2').hide();
          $('#tabsub_2,#S_SUB2,#S_TYPE1').show();
          break;
    case  2:
          // $('#tabsub_1,#tabsub_2,#tabsub_4,#S_SUB3,#S_TYPE1').hide();
          $('#tabsub_3,#S_SUB2,#S_TYPE2').show();
          break;
    case  3:
          // $('#tabsub_1,#tabsub_2,#tabsub_3,#tabsub_4,#S_SUB2,#S_TYPE2').hide();
          $('#tabsub_4,#S_SUB3,#S_TYPE1,#S_TYPE3').show();
          break;
  }
}


function uf_tag2_input(){
  //여러 dg 처리
  var la_dg = [dg_6];
  var ls_styleY = {"background": "#f0f0f0"};
  var ls_styleN = {"background": "#fff6f6"};
  for (var i = 0; i<la_dg.length; i++) {
    la_dg[i].SetAutoReadOnly("WIDTH", function (args) {return args.dataContext.WIDTH_YN != 'Y';}, false);
    la_dg[i].SetAutoReadOnly("HEIGHT", function (args) {return args.dataContext.HEIGHT_YN != 'Y';}, false);
    la_dg[i].SetAutoReadOnly("AREA", function (args) {return args.dataContext.AREA_YN != 'Y';}, false);
    la_dg[i].SetAutoReadOnly("DIAMETER", function (args) {return args.dataContext.DIAMETER_YN != 'Y';}, false);

    la_dg[i].SetAutoCellStyle2("WIDTH", function (args) {return args.dataContext.WIDTH_YN != 'Y'?ls_styleY:ls_styleN;});
    la_dg[i].SetAutoCellStyle2("HEIGHT", function (args) {return args.dataContext.HEIGHT_YN != 'Y'?ls_styleY:ls_styleN;});
    la_dg[i].SetAutoCellStyle2("AREA", function (args) {return args.dataContext.AREA_YN != 'Y'?ls_styleY:ls_styleN;});
    la_dg[i].SetAutoCellStyle2("DIAMETER", function (args) {return args.dataContext.DIAMETER_YN != 'Y'?ls_styleY:ls_styleN;});

  }
}


function pf_mat_select() {
  if(typeof(_Caller.pf_is_ductrqst)!="undefined" && _Caller.pf_is_ductrqst()) {
    _X.Noty("덕트청구 진행중입니다.<br>일반자재와 동시 청구 불가능합니다!!", null, null, 2000);
    return;
  }

  var a_dg = dg_6;
  var sCnt = 0;
  for(var a_row=1; a_row<= a_dg.RowCount() ;a_row++) {
    if(a_dg.GetItem(a_row, "ITEM_QTY") > 0 || a_dg.GetItem(a_row, "WIDTH") > 0) {
      sCnt++;
      if(a_dg.GetItem(a_row, "WHA_YN").indexOf('Y') > -1){
        var lo_ITEM0 = ["WIDTH", "HEIGHT", "AREA"];
        var lo_ITEM1 = ["W:","H:","L:"];
        var lo_ITEM2 = [];
        var ls_ITEM2 = "";
        for (var i = 0; i <=2; i++) {
            if(a_dg.GetItem(a_row, lo_ITEM0[i])) lo_ITEM2.push(lo_ITEM1[i]+a_dg.GetItem(a_row, lo_ITEM0[i]))
        }
        if(lo_ITEM2.length > 0){
          for (var i = 0; i <lo_ITEM2.length; i++) {
            if(ls_ITEM2.length > 0) ls_ITEM2 += ","
            ls_ITEM2 += lo_ITEM2[i]
          }
          a_dg.SetItem(a_row, "ITEM_NAME2", "_{"+ls_ITEM2+"}");
          a_dg.SetRowsState([a_row], "N");
        }else{
          _X.Noty("해당 자재는 사이즈를 입력해 주시기 바랍니다.!!", null, null, 2000);
          return;
        }
      }
      //_Caller.pf_addcall_one(a_dg, a_row, 'D');
      _Caller.pf_addcall_one(a_dg, a_row);
      a_dg.SetItem(a_row, "ITEM_QTY", 0);
    }
  }

  if(sCnt==0){
    xe_GridItemDoubleClick2(a_dg, a_dg.GetRow(), null, null);
  }
}


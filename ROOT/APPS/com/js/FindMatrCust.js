//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 :
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
//
//===========================================================================================
// ls_gbn이 1이면 공통자재, 2이면 거래처관리자제

var ls_gbn = _Caller._FindCode.findcode[0];
var ls_cust = _Caller._FindCode.findcode[1];

function x_InitForm2(){
	if(ls_gbn == '1'){
  	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "com", "FindMatrCust", "FindMatrCust|FindMatrCust_02", false, false);
  }else{
  	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "com", "FindMatrCust", "FindMatrCust|FindMatrCust_01", false, false);
  }
  _X.InitGrid(grid_2, "dg_2", "100%", "100%", "com", "FindMatrCust", "FindMatrCust|FindMatrCust_03", false, false);

  if(ls_gbn == '1'){
	  var ls_large_code = _X.XmlSelect('com', 'FindMatrCust', 'FindMatrCust_S04', new Array(), 'json2');
		_X.DDLB_SetData(S_LARGE_CODE, ls_large_code, null, false, true);

		var ls_middle_code = _X.XmlSelect('com', 'FindMatrCust', 'FindMatrCust_S05', new Array(S_LARGE_CODE.value), 'json2');
		_X.DDLB_SetData(S_MIDDLE_CODE, ls_middle_code, null, false, true);

		var ls_small_code = _X.XmlSelect('com', 'FindMatrCust', 'FindMatrCust_S06', new Array(S_LARGE_CODE.value, S_MIDDLE_CODE.value), 'json2');
		_X.DDLB_SetData(S_SMALL_CODE, ls_small_code, null, false, true);
	}else{
		var ls_large_code = _X.XmlSelect('com', 'FindMatrCust', 'FindMatrCust_S01', new Array(ls_cust), 'json2');
		_X.DDLB_SetData(S_LARGE_CODE, ls_large_code, null, false, true);

		var ls_middle_code = _X.XmlSelect('com', 'FindMatrCust', 'FindMatrCust_S02', new Array(ls_cust, S_LARGE_CODE.value), 'json2');
		_X.DDLB_SetData(S_MIDDLE_CODE, ls_middle_code, null, false, true);

		var ls_small_code = _X.XmlSelect('com', 'FindMatrCust', 'FindMatrCust_S03', new Array(ls_cust, S_LARGE_CODE.value, S_MIDDLE_CODE.value), 'json2');
		_X.DDLB_SetData(S_SMALL_CODE, ls_small_code, null, false, true);
	}
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
  if(ab_allcompleted){
    x_DAO_Retrieve2(dg_1);
  }
}

function x_DAO_Retrieve2(){
  if(ls_gbn == '1'){
  	var param = new Array(S_LARGE_CODE.value, S_MIDDLE_CODE.value, S_SMALL_CODE.value, S_SEARCH_NM.value);
  }else{
  	var param = new Array(ls_cust, S_LARGE_CODE.value, S_MIDDLE_CODE.value, S_SMALL_CODE.value, S_SEARCH_NM.value);
  }
  dg_1.Retrieve(param);
}

function xe_EditChanged2(a_obj, a_val){
	if(a_obj.id == "S_LARGE_CODE"){

		if(ls_gbn == '1'){
			var ls_middle_code = _X.XmlSelect('com', 'FindMatrCust', 'FindMatrCust_S05', new Array(S_LARGE_CODE.value), 'json2');
			_X.DDLB_SetData(S_MIDDLE_CODE, ls_middle_code, null, false, true);

			var ls_small_code = _X.XmlSelect('com', 'FindMatrCust', 'FindMatrCust_S06', new Array(S_LARGE_CODE.value, S_MIDDLE_CODE.value), 'json2');
			_X.DDLB_SetData(S_SMALL_CODE, ls_small_code, null, false, true);
		}else{
			var ls_middle_code = _X.XmlSelect('com', 'FindMatrCust', 'FindMatrCust_S02', new Array(ls_cust, S_LARGE_CODE.value), 'json2');
			_X.DDLB_SetData(S_MIDDLE_CODE, ls_middle_code, null, false, true);

			var ls_small_code = _X.XmlSelect('com', 'FindMatrCust', 'FindMatrCust_S03', new Array(ls_cust, S_LARGE_CODE.value, S_MIDDLE_CODE.value), 'json2');
			_X.DDLB_SetData(S_SMALL_CODE, ls_small_code, null, false, true);
		}
	}

  if(a_obj.id == "S_MIDDLE_CODE"){
  	if(ls_gbn == '1'){
			var ls_small_code = _X.XmlSelect('com', 'FindMatrCust', 'FindMatrCust_S06', new Array(S_LARGE_CODE.value, S_MIDDLE_CODE.value), 'json2');
			_X.DDLB_SetData(S_SMALL_CODE, ls_small_code, null, false, true);
		}else{
			var ls_small_code = _X.XmlSelect('com', 'FindMatrCust', 'FindMatrCust_S03', new Array(ls_cust, S_LARGE_CODE.value, S_MIDDLE_CODE.value), 'json2');
			_X.DDLB_SetData(S_SMALL_CODE, ls_small_code, null, false, true);
		}
	}

	x_DAO_Retrieve2(dg_1);
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	if(S_SEARCH_NM.value != ""){
		x_DAO_Retrieve(dg_1);
	}
	if(a_obj.id == 'S_MATR_NM'){
		if(S_MATR_NM.value != ""){
			if(dg_1.SetFocusByElement(S_MATR_NM,['ITEM_STD','ITEM_UNIT'],true) < 0){
				_X.MsgBox("확인","검색한 자재가 존재하지 않습니다.")
			}
		}
	}
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
  return 100;
}

function x_DAO_ChkErr2(){
  return true;
}

function x_DAO_Saved2(){
  return 100;
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}

function x_Insert_After2(a_dg, rowIdx){
	var ls_data = dg_1.GetRowData(dg_1.GetRow());
	dg_2.SetItem(rowIdx,'ITEM_CODE',			ls_data.ITEM_CODE);
	dg_2.SetItem(rowIdx,'ITEM_NAME',			ls_data.ITEM_NAME);
	dg_2.SetItem(rowIdx,'ITEM_STD', 			ls_data.ITEM_STD);
	dg_2.SetItem(rowIdx,'ITEM_UNIT', 			ls_data.ITEM_UNIT);
	dg_2.SetItem(rowIdx,'ESTIMATE_COST',	ls_data.ESTIMATE_COST);
	dg_2.SetItem(rowIdx,'ITEM_MAKER', 			ls_data.ITEM_MAKER);
	dg_2.SetItem(rowIdx,'MAKER_NAME', 			ls_data.MAKER_NAME);
}



function x_DAO_Duplicate2(a_dg, row){
  return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg){
  return 100;
}

function x_DAO_Excel2(a_dg){
  return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
  return 1;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
  return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridDataLoad2(a_dg){

}

function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){
}

function x_ReceivedCode2(a_retVal){
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){
}

function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
	if(a_dg.id == 'dg_1'){

		var ls_matcd = dg_1.GetItem(a_row, 'ITEM_CODE');

		for(var i = 1 ; i <= dg_2.RowCount(); i++){
			if(dg_2.GetItem(i,'ITEM_CODE') == ls_matcd){
				_X.MsgBox('확인','해당 자재는 이미 선택된 자재입니다.');
				return;
			}
		}
		x_DAO_Insert(dg_2,0);
	}

	if(a_dg.id == 'dg_2'){
		dg_2.DeleteRow(a_row);
	}
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){
}

function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){
}

function xe_ChartPointClick2(a_dg, a_event){
}

function xe_ChartPointSelect2(a_dg, a_event){
}

function xe_ChartPointMouseOver2(a_dg){
}

function xe_ChartPointMouseOut2(a_dg, a_event){
}

function x_Close2() {

  if(_IsModal || _IsPopup) {
    window.close();
  }else{
    if(_Caller.$("#findmodal").length>0?_Caller.$("#findmodal").dialog("close"):window.close());
  }
}

function x_Confirm(){

	_Caller.pf_addcall(window.dg_2);

	x_Close2();
}

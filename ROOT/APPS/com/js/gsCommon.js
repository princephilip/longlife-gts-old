var _GS;
if (!_GS) {
	_GS = {};
}

// 최근 계약번호 가져오기
_GS.GetMaxContSeq = function(a_cust_id, a_dept, a_yymm) {
	var ls_result = _X.XmlSelect("gs", "GS_COMMON", "GET_MAX_CONTSEQ", [a_cust_id, a_dept, a_yymm], "array");
	return ls_result[0][0];
}
_GS.GetMaxContSeqOld = function(a_cust_id) {
	var ls_result = _X.XmlSelect("gs", "GS_COMMON", "GET_MAX_CONTSEQ_OLD", [a_cust_id], "array");
	return ls_result[0][0];
}

// 고객 지사별 비고 수정
_GS.UpdateCustRemarkDept = function(a_dept, a_cust, a_value) {
	if(a_dept && a_cust) {
		var ls_result = _X.ES("gs", "GS_COMMON", "CODE_CUSTINDEPT_U01", [a_dept, a_cust, a_value]);
		return ls_result;
	} else {
		return false;
	}
};

// 다음 순번의 고객ID를 반환
_GS.GetDataCustId = function() {
	var ls_result = _X.XmlSelect("gs", "GS_COMMON", "NEXT_CUST_ID", [], "array");
	return ls_result[0][0];
};

// 재가요양서비스 월 한도금액 가져오기 ( 등급 입력용 )
_GS.GetMonthLimitAmt = function(a_level, a_rate) {
	var param = new Object();
	param.limit_amt = 0;     // 공단지원금 한도
	param.req_limit_amt = 0; // 자기분담금 한도

  var ls_result = _X.XmlSelect("gs", "GS_COMMON", "GET_CARE_BASIC_AMT", [a_level, a_rate], "array");
  if (ls_result.length > 0) {
    param.limit_amt     = _X.ToInt(ls_result[0][0]);
		param.req_limit_amt = _X.ToInt(ls_result[0][1]);
  }

  return param;
};


// 대상자정보수정
_GS.UpdateCustomer = function(a_deptid, a_custid) {
	mytop._X.MDI_Open('GS01010', mytop._MyPgmList['GS01010'].PGM_NAME, '/Mighty/template/XG_Slick1.jsp?pcd=GS01010&params='+a_deptid+'@'+a_custid);
};

// 서비스 product_id 가져오기
_GS.GetProductId = function(a_pcode) {
	var ls_result = _X.XmlSelect("gs", "GS_COMMON", "GET_PRODUCT_ID", [a_pcode], "array");
	return ls_result[0][0];
}

/* 다음 순번의 서비스ID를 반환 */
_GS.GetDataServiceId = function() {
	var ls_result = _X.XmlSelect("gs", "GS_COMMON", "GET_SEQ_S_ID", [], "array");
	return ls_result[0][0];
}


// 각종 마감 체크
_GS.IsClosed = function(a_ym, a_dept, a_div) {
	var ls_col = "";
	var ls_col2 = "";

	switch(a_div) {
		case "gong" :
			ls_col = "gongcloseyn";
			ls_col2 = "gongclosedate";
			break;
		case "service" :
			ls_col = "servicecloseyn";
			ls_col2 = "serviceclosedate";
			break;
		case "pay" :
			ls_col = "paycloseyn";
			ls_col2 = "payclosedate";
			break;
		case "acnt" :
			ls_col = "acntcloseyn";
			ls_col2 = "acntclosedate";
			break;
		case "sell" :
			ls_col = "sellcloseyn";
			ls_col2 = "sellclosedate";
			break;
		case "special" :
			ls_col = "specialcloseyn";
			ls_col2 = "specialclosedate";
			break;
	}

	// 마감 개수 체크해서, 지사마감 또는 본사마감 둘다 체크
	var li_cnt = _X.XmlSelect("gs", "GS_COMMON", "CHK_CLOSING", [a_ym, a_dept, ls_col], "array")[0][0];

	if(li_cnt>0) {
		return true;
	} else {
		return false;
	}

	return false;
}

// 각종 마감 처리
_GS.ClosingDept = function(a_ym, a_dept, a_div) {
	var ls_col = "";
	var ls_col2 = "";

	switch(a_div) {
		case "gong" :
			ls_col = "gongcloseyn";
			ls_col2 = "gongclosedate";
			break;
		case "service" :
			ls_col = "servicecloseyn";
			ls_col2 = "serviceclosedate";
			break;
		case "pay" :
			ls_col = "paycloseyn";
			ls_col2 = "payclosedate";
			break;
		case "acnt" :
			ls_col = "acntcloseyn";
			ls_col2 = "acntclosedate";
			break;
		case "sell" :
			ls_col = "sellcloseyn";
			ls_col2 = "sellclosedate";
			break;
		case "special" :
			ls_col = "specialcloseyn";
			ls_col2 = "specialclosedate";
			break;
	}

	// 마감 개수 체크해서, 지사마감 또는 본사마감 둘다 체크
	var li_cnt = _X.XmlSelect("gs", "GS_COMMON", "CHK_CLOSING", [a_ym, a_dept, ls_col], "array")[0][0];
	//alert('li_cnt : ' + li_cnt.toString());

	if(li_cnt>0) {
		_X.MsgBox("이미 마감되었습니다.");
		return true;
	} else {
		//alert([a_ym, a_dept, ls_col]);
		var ls_result = _X.ES("gs", "GS_COMMON", "CODE_CLOSING_U01", [a_ym, a_dept, ls_col]);
		//alert('ls_result : ' + ls_result.toString());
		if ( li_result < 0){
			_X.MsgBox("마감중 오류가 발생했습니다.");
			return false;
		} else {
			_X.MsgBox("마감처리 했습니다.");
			return true;
		}
	}

	return false;
}


// 보호자 관리
_GS.FindGS01012 = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win.name = 'FindGS01012';
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=gs&fcd=GS01012&fnm="+encodeURIComponent('보호자 관리')+"&msize=0&fgrid=GS01012|GS01012|GS01012";
	_X.OpenFindWin3(a_win, param, '보호자 관리', 1200, 400);
};
_GS.findGS01012 = _GS.FindGS01012;

// 서비스 일정조회
_GS.FindGS02041 = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win.name = 'FindGS02041';
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=gs&fcd=GS02041&fnm="+encodeURIComponent('서비스 일정조회')+"&msize=0&fgrid=GS02041|GS02041|GS02041";
	_X.OpenFindWin3(a_win, param, '서비스 일정조회', 1200, 600);
};
_GS.findGS02041 = _GS.FindGS02041;


// 반복일정 추가/삭제 프로그램 열기 (팝업으로)
_GS.OpenScheduleCreate = function(a_win, a_dept, a_cust, a_custname, a_custinfo) {
	if(!a_dept) {
		_X.MsgBox('센터를 선택해야 합니다.');
		return;
	}
	if(!a_cust || !a_custname) {
		_X.MsgBox('고객을 선택해야 합니다.');
		return;
	}

	a_win.name = 'FindGS01049';
	a_win._FindCode.finddiv  = "GS01049";
	a_win._FindCode.findstr  = "";
	a_win._FindCode.findcode = [a_dept, a_cust, a_custname, a_custinfo];

	var param = "sys=gs&fcd=GS01049&fnm="+encodeURIComponent('반복일정 추가/삭제')+"&msize=0&fgrid=GS01049|GS01049|GS01049";
	_X.OpenFindWin3(a_win, param, '반복일정 추가/삭제', 780, 520);
};

// ???
_GS.SetRemainAmtCalc = function(a_dg, lst_cust_id, is_date, sep) {
	var Row = a_dg.GetRow();
  var ls_sql_check = "";
  var ls_sql = "";
  var is_supply_pay = true;

  if(sep == "sale" || sep == "rent" ){  //고객대여 물품이 공단미지정일 경우를 체크 , 잔여한도액을 가져오지 않음
    var li_product = a_dg.GetItem(Row, "PRODUCT_ID");
    if(!(li_product == null || li_product == '' || isNaN(li_product))){
    	// E : 미지정 O : 공단지정
    	var ls_proffi = _X.XmlSelect("gs", "GS_COMMON", "SetRemainAmtCalc_R01", [li_product], "array")[0][0];
      if(ls_proffi == 'E'){
        is_supply_pay = false;
        var lt_amt = 0;
        if (sep == "sale"){
          lt_amt = a_dg.GetItem(Row, "SALE_AMT");    //판매가
        }else if (sep == "rent"){
          lt_amt = a_dg.GetItem(Row, "AMT");    //대여가
        }

        a_dg.SetItem(Row, "REQ_AMT", 0);    						//구청청구금세팅
        a_dg.SetItem(Row, "PAY_AMT", Number(lt_amt));    //본인부담금세팅
        a_dg.SetItem(Row, "REMAIN_AMT", 0);     					//잔여요양비용세팅
      }
    }
    else if(ls_proffi == 'E'){
      is_supply_pay = true; // 공단지원
    }
  }
  if(is_supply_pay == true){
      var ls_app_rate = ""; //dwc.GetItemString(Row,"app_rate_div");
      if( ls_app_rate == "" || isNaN(ls_app_rate)){
    		ls_app_rate = _X.XmlSelect("gs", "GS_COMMON", "SetRemainAmtCalc_R02", [lst_cust_id, is_date], "array")[0][0];
      }

			//적용요율
    	var lt_rate = _X.XmlSelect("gs", "GS_COMMON", "SetRemainAmtCalc_R03", [ls_app_rate], "array")[0][0];
      var lt_guamt     = 0;
      var lt_bonamt    = 0;
      var lt_janamt    = 0;
      var lt_amt = 0;
      // var lt_intro_rate = 1;

      if(sep == "care"){  //요양서비스    GS01070서비스일정관리, GS01110 요양보호사별 서비스 일정 관리
        lt_amt = a_dg.GetItem(Row, "SERVICE_AMT");    //서비스수가
        lt_intro_rate = a_dg.GetItem(Row, "INTRO_RATE");
        if(isNaN(lt_intro_rate)) lt_intro_rate = 1;
      }else if (sep == "sale"){
        lt_amt = a_dg.GetItem(Row, "SALE_AMT");    //판매가
      }else if (sep == "rent"){
        lt_amt = a_dg.GetItem(Row, "AMT");    //대여가
      }

			//구청청구금계산 = 적용요율 * 서비스수가 /100
			lt_guamt =  parseInt(lt_amt - (lt_rate/100)*lt_amt);  //구청청구금
			lt_bonamt = lt_amt - lt_guamt; //본인부담금

			// 본인부담금 원단위 절사
			lt_bonamt = Math.floor(lt_bonamt/10)*10;
			// 구청청구금 재반영
			lt_guamt   = lt_amt - lt_bonamt;

      a_dg.SetItem(Row, "REQ_AMT", Number(lt_guamt));   //구청청구금세팅
      a_dg.SetItem(Row, "PAY_AMT", Number(lt_bonamt));   //본인부담금세팅
  }
}

_GS.SetRemainAmtCalc_xx = function(lst_cust_id, is_date, sep) {
  var ls_sql_check = "";
  var ls_sql = "";
  var is_supply_pay = true;

  if(sep == "sale" || sep == "rent" ){  //고객대여 물품이 공단미지정일 경우를 체크 , 잔여한도액을 가져오지 않음
    var li_product = PRODUCT_ID.value;
    if(!(li_product == null || li_product == '' || isNaN(li_product))){
    	// E : 미지정 O : 공단지정
    	var ls_proffi = _X.XmlSelect("gs", "GS_COMMON", "SetRemainAmtCalc_R01", [li_product], "array")[0][0];
      if(ls_proffi == 'E'){
        is_supply_pay = false;
        var lt_amt = 0;
        if (sep == "sale"){
          lt_amt = SALE_AMT.value;    //판매가
        }else if (sep == "rent"){
          lt_amt = AMT.value;    //대여가
        }

        REQ_AMT.value    = 0;     //구청청구금세팅
        PAY_AMT.value    = Number(lt_amt);     //본인부담금세팅
        REMAIN_AMT.value = 0;     //잔여요양비용세팅
      }
    }
    else if(ls_proffi == 'E'){
      is_supply_pay = true; // 공단지원
    }
  }
  if(is_supply_pay == true){
      var ls_app_rate = ""; //dwc.GetItemString(Row,"app_rate_div");
      if( ls_app_rate == "" || isNaN(ls_app_rate)){
    		ls_app_rate = _X.XmlSelect("gs", "GS_COMMON", "SetRemainAmtCalc_R02", [lst_cust_id, is_date], "array")[0][0];
      }

			//적용요율
    	var lt_rate = _X.XmlSelect("gs", "GS_COMMON", "SetRemainAmtCalc_R03", [ls_app_rate], "array")[0][0];
      var lt_guamt     = 0;
      var lt_bonamt    = 0;
      var lt_janamt    = 0;
      var lt_amt = 0;
      // var lt_intro_rate = 1;

      if(sep == "care"){  //요양서비스    GS01070서비스일정관리, GS01110 요양보호사별 서비스 일정 관리
        lt_amt = SERVICE_AMT.value;    //서비스수가
        lt_intro_rate = INTRO_RATE.value;
        if(isNaN(lt_intro_rate)) lt_intro_rate = 1;
      }else if (sep == "sale"){
        lt_amt = SALE_AMT.value;    //판매가
      }else if (sep == "rent"){
        lt_amt = AMT.value;    //대여가
      }

			//구청청구금계산 = 적용요율 * 서비스수가 /100
			lt_guamt =  parseInt(lt_amt - (lt_rate/100)*lt_amt);  //구청청구금
			lt_bonamt = lt_amt - lt_guamt; //본인부담금

			// 본인부담금 원단위 절사
			lt_bonamt = Math.floor(lt_bonamt/10)*10;
			// 구청청구금 재반영
			lt_guamt   = lt_amt - lt_bonamt;

			REQ_AMT.value = Number(lt_guamt);     //구청청구금세팅
			PAY_AMT.value = Number(lt_bonamt);     //본인부담금세팅
  }
}

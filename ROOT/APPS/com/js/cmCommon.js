var _CM;
if (!_CM) {
	_CM = {};
}

//공사관리 건설사정보 조회
_CM.FindConstructCode = function(a_win, a_find_div, a_findstr, a_findcode) {
	//alert(a_find_div+';'+a_findstr+';'+a_findcode);
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cm&fcd=FindConstructCode&fnm="+encodeURIComponent('건설사 찾기')+"&msize=0&fgrid=FindConstructCode|FindConstructCode|FindConstructCode";
	_X.OpenFindWin3(a_win, param, '', 400, 250);
};
_CM.FindConstructCode = _CM.FindConstructCode;

// 노무>출역현황>단가변경
_CM.CostCh = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cm&fcd=CM251016&fnm="+encodeURIComponent('단가변경')+"&msize=0&fgrid=CM251016|CM251016|CS502010_dg1";
	_X.OpenFindWin3(a_win, param, '단가변경', 650, 600);
};
_CM.costCh = _CM.CostCh;

//계약찾기
_CM.FindOsctConsult = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cm&fcd=FindOsctConsult&fnm="+encodeURIComponent('계약찾기')+"&msize=0&fgrid=FindOsctConsult|CM_COMMON|FindOsctConsult_01";
	_X.OpenFindWin2(a_win, param, '계약찾기', 505, 500);
};
_CM.findOsctConsult = _CM.FindOsctConsult;

// 퇴직금계산
_CM.RpayApply = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cm&fcd=CM251516&fnm="+encodeURIComponent('퇴직금계산')+"&msize=0&fgrid=CM251516|CM251516|CM251516_01";
	_X.OpenFindWin3(a_win, param, '퇴직금계산', 900, 600);
};
_CM.rpayApply = _CM.RpayApply;


//실행내역 - 품의서작성
_CM.FindExecConsultDetail = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win.name = 'FindExecConsultDetail';
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=cm&fcd=FindExecConsultDetail&fnm="+encodeURIComponent('상세내역등록')+"&msize=0&fgrid=FindExecConsultDetail|FindExecConsultDetail|FindExecConsultDetail_01";
	_X.OpenFindWin3(a_win, param, '상세내역등록', 900, 600);
};
_CM.findExecConsultDetail = _CM.FindExecConsultDetail;


//공종찾기 - 품의서작성
_CM.FindExecConsultKind = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win.name = 'FindExecConsultKind';
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=cm&fcd=FindExecConsultKind&fnm="+encodeURIComponent('공종찾기')+"&msize=0&fgrid=FindExecConsultKind|FindExecConsultKind|FindExecConsultKind_01";
	_X.OpenFindWin3(a_win, param, '공종찾기', 500, 400);
};
_CM.findExecConsultKind = _CM.FindExecConsultKind;

//장비찾기
_CM.FindEquipCode = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win.name = 'FindExecConsultKind';
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=cm&fcd=FindEquipCode&fnm="+encodeURIComponent('장비찾기')+"&msize=0&fgrid=FindEquipCode|CM_COMMON|FindEquipCode_01";
	_X.OpenFindWin2(a_win, param, '장비찾기', 600, 500);
};

//장비_현장별 업체 찾기
_X.FindEquipCust = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win.name = 'FindEquipCust';
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=cm&fcd=FindEquipCust&fnm="+encodeURIComponent('장비_현장별 거래처 찾기')+"&msize=0&fgrid=FindEquipCust|CM_COMMON|FindEquipCust";
	_X.OpenFindWin2(a_win, param, '장비_현장별 거래처 찾기', 640, $(document).height()-300, _FIND_BTN_PARAM);
};
_X.FindEquipCust = _X.FindEquipCust;

//팀별 숙소 필요 목록
_X.FindEmpHouseLabo = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=cm&fcd=FindEmpHouseLabo&fnm="+encodeURIComponent('숙소직원 찾기')+"&msize=0&fgrid=FindEmpHouseLabo|CM_COMMON|FindEmpHouseLabo_01";
	_X.OpenFindWin2(a_win, param, '숙소직원 찾기', 520, $(document).height()-300, _FIND_BTN_PARAM);
};

//단가변경 이력
_X.FindSalaryReq = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=cm&fcd=FindSalaryReq&fnm="+encodeURIComponent('단가변경 이력')+"&msize=0&fgrid=FindSalaryReq|CM_COMMON|FindSalaryReq_01";
	_X.OpenFindWin2(a_win, param, '단가변경 이력', 900, $(document).height()-200, _FIND_BTN_PARAM);
};

//단가변경 이력
_X.FindWorkHis = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=cm&fcd=FindWorkHis&fnm="+encodeURIComponent('근무 이력')+"&msize=0&fgrid=FindWorkHis|CM_COMMON|FindWorkHis_01";
	_X.OpenFindWin2(a_win, param, '근무 이력', 900, $(document).height()-200, _FIND_BTN_PARAM);
};

//경비- 거래처별 인증계좌번호 가져오기
_CM.GetCustDepositNo = function(a_cust, a_proj) {
	var ls_result = _X.XmlSelect('cm', 'CM_COMMON', 'GET_DEPOSIT_NO', [a_cust, a_proj], 'json'); // 경비 최근입력 계좌

	return ls_result;
};
_CM.getCustDepositNo = _CM.GetCustDepositNo;

//경비- 개인카드 결제계좌번호 가져오기
_CM.GetCreditDepositNo = function(a_cd_no) {
	var ls_result = _X.XmlSelect('cm', 'CM_COMMON', 'GET_CREDIT_DEPOSIT_NO', [a_cd_no], 'json'); // 신용카드 등록계좌

	return ls_result;
};
_CM.getCreditDepositNo = _CM.GetCustDepositNo;

//팀찾기
_CM.FindTeamCode = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=cm&fcd=FindTeamCode&fnm="+encodeURIComponent('팀찾기')+"&msize=0&fgrid=FindTeamCode|CM_COMMON|FindTeamCode";
	_X.OpenFindWin2(a_win, param, '팀찾기', 905, 500);
};

//마감데이터 생성
_CM.CreateClose = function(aYm, aCode) {
	var li_rtn = _X.ExecProc("cm", "SP_CM_CLOSE_CREATE", [mytop._CompanyCode, aYm, aCode ]);
	if(li_rtn<0) _X.MsgBox("확인", "마감데이터 생성중 오류가 발생했습니다.");
}
_CM.createClose = _CM.CreateClose;

//현장마감 여부
_CM.ChkCloseProj = function(aProj, aYm) {
	var ls_result = _X.XmlSelect('cm', 'CM_COMMON', 'GET_CLOSE_PROJECT', [mytop._CompanyCode, aProj, aYm, 'PROJECT'], 'array')[0][0]; // 현장마감여부 체크

	return ls_result;
}
_CM.chkCloseProj = _CM.ChkCloseProj;

//노무비 마감여부
_CM.IsPayClosed = function(aProj, aYm) {
	var ls_result = _X.XmlSelect('cm', 'CM_COMMON', 'GET_CLOSE_PROJECT', [mytop._CompanyCode, aProj, _X.strPurify(aYm,'-'), 'PROJECT'], 'array')[0][0]; // 현장마감여부 체크
	if(ls_result=='Y') {return true;}

	return _X.IsApprClosed("출역집계", mytop._CompanyCode, aProj, _X.strPurify(aYm,'-'));
}

// 임차 연장 이력
_CM.FindReconList = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win.name = 'FindReconList';
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=cm&fcd=FindReconList&fnm="+encodeURIComponent('연장이력')+"&msize=0&fgrid=FindReconList|FindReconList|FindReconList";
	_X.OpenFindWin3(a_win, param, '연장이력', 700, 600);
};
_CM.findReconList = _CM.FindReconList;

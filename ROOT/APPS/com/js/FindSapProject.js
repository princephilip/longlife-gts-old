//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 : @SM0211|사업장 찾기
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
// 김성한       	 x-internetinfo      2014.12.01 
//
//===========================================================================================
//	
//===========================================================================================
var _IsModal = (window.dialogArguments?true:false);
var _IsPopup = (window.opener?true:false);
var _Caller  = (_IsModal?window.dialogArguments:(_IsPopup?window.opener:window.parent));

function x_InitForm(){	
	if(typeof(xc_InitForm)!="undefined"){xc_InitForm();return;}
	
	if($(document).height() % 2 == 1){
  	$('#findmodal', parent.document).height($(document).height() + 1);
  }else{
  	$('#findmodal', parent.document).height($(document).height() - 1);  
  }
  
	if(typeof(_FIND_GRID)!="undefined") 
		_X.SlickGrid(grid_1, "dg_1", "100%", "100%", _FIND_SYS, _FIND_GRID, _FIND_QUERY+"|" + _FIND_SELECT);
	else 
		_X.SlickGrid(grid_1, "dg_1", "100%", "100%", _FIND_SYS, _FIND_CODE, "FindCode|" + _FIND_CODE);
	
	$("#FIND_CODE").val(_Caller._FindCode.findstr);
	$("#FIND_CODE").focus();	
}

function x_DAO_Retrieve(a_dg){	
	if(_Caller._FindCode.findcode=="undefined" || _Caller._FindCode.findcode == null){
		dg_1.Retrieve(new Array(FIND_CODE.value));	
	}else{
	  var ls_code =_Caller._FindCode.findcode;
	  var ls_arr = new Array(FIND_CODE.value);
	  for(var i=0; i<ls_code.length; i++){
	  	ls_arr[i+1] = ls_code[i];
	  }
		dg_1.Retrieve(ls_arr);
	}
}

function xe_GridLayoutComplete(a_dg){
	if(a_dg.id == 'dg_1'){
		x_DAO_Retrieve(a_dg)
	}
}

function x_DAO_Save(){}
function x_DAO_Insert(row){}
function x_DAO_Delete(row){}
function x_DAO_Duplicate(row){}
function x_Duplicate_After(obj, rowIdx){}
function x_DAO_Excel(){dg_1.ExcelExport();}

function xe_BodyResize(a_init){ 
	//SlickGrid 리사이징(2015.05.11 KYY)
	dg_1._SlickGrid.resizeCanvas();
}
	
function xe_GridDataLoad(a_dg){
	$('#FIND_CODE').focus();
}

function xe_EditChanged(a_obj, a_val, a_label, a_cobj){
	if(typeof(xc_EditChanged)!="undefined"){
		xc_EditChanged(a_obj, a_val, a_label, a_cobj);
		return;
	}
}

function xe_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){
	if(typeof(xc_InputKeyDown)!="undefined"){xc_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey);return;}

	if(a_obj==FIND_CODE && a_keyCode==KEY_DOWN)
		$('#dg_1').focus();
}

function xe_InputKeyEnter(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	switch(a_obj.id) {
		case "FIND_CODE":
			x_DAO_Retrieve();
			break;
	}
}

function xe_GridRowFocusChange(a_dg, a_row, a_col){}
function xe_GridDoubleClick(a_dg, a_ctrlKey, a_altKey, a_shiftKey, a_stageX, a_stageY){alert('9');}

function xe_GridItemDoubleClick(a_dg, a_row, a_col){
	if(typeof(xc_GridItemDoubleClick)!="undefined"){xc_GridItemDoubleClick(a_dg, a_row, a_col);return;}
	x_ReturnRowData(a_dg, a_row);
}

function xe_GridButtonClick(a_dg, a_row, a_col, a_colname){
	if(a_dg.id=="dg_1" && a_colname == "SELECT_RETURN") {
		x_ReturnRowData(a_dg, a_row);
	}
}

function x_ReturnRowData(a_dg, a_row){
	if(typeof(xc_ReturnRowData)!="undefined"){xc_ReturnRowData(a_dg, a_row);return;}

	var rtValue = a_dg.GetRowData(a_row);
	if(_IsModal) {
		window.returnValue = rtValue;
	} else {
		if(_Caller) {	
			_Caller._FindCode.returnValue = rtValue;
			_Caller.x_ReceivedCode(rtValue); 
		}
	}
	setTimeout('x_Close()',0);
}

function x_Confirm(){
	if (dg_1.GetRow()==0) return;
	if(_Caller._FindCode.findmulti) {
			var ls_rows = dg_1.GetCheckedRows();
			var ls_arr;
			if(ls_rows == null || ls_rows == "") {
				ls_arr = null;
			} else {
				ls_arr = new Array();
				for(var i = 0; i < ls_rows.length; i++) {
					ls_arr.push(dg_1.GetRowData(ls_rows[i] + 1));
				}
			}
			if(_IsModal) {
				window.returnValue = ls_arr;
			} else {
				if(_Caller) {	
					_Caller._FindCode.returnValue = ls_arr;
					if(ls_arr != null)
						_Caller.x_ReceivedCode(ls_arr);
				}
			}
			setTimeout('x_Close()',0);
	} else {
		x_ReturnRowData(dg_1, dg_1.GetRow());
	}
}

function x_Close() {
	if(_IsModal || _IsPopup) {
		window.close();
	} else {
		if(_Caller.$("#findmodal").length>0?_Caller.$("#findmodal").dialog("close"):window.close());
	}
}

_AutoRetrieve = true;

var _IsModal = (window.dialogArguments?true:false);
var _IsPopup = (window.opener?true:false);
var _Caller  = (_IsModal?window.dialogArguments:(_IsPopup?window.opener:window.parent));

function xc_DAO_Retrieve()
{
	//if(FIND_CODE.value.length<1){alert("1자 이상의 검색문자열을 입력해 주세요"); $('#FIND_CODE').focus(); return;}
	dg_1.Retrieve(new Array('%' + FIND_CODE.value+ '%'));		
	
}

function x_ReturnRowData(a_dg, a_row){
	if(typeof(xc_ReturnRowData)!="undefined"){xc_ReturnRowData(a_dg, a_row);return;}

	var rtValue = a_dg.GetRowData(a_row);
	 if(_IsModal) {
	 	window.returnValue = rtValue;
	 } else {
		if(_Caller) {	
			_Caller._FindCode.returnValue = rtValue;
			_Caller.x_ReceivedCode(rtValue); 
		}
	 }
	setTimeout('x_Close()',0);
}


function x_Confirm(){
	if (dg_1.GetRow()==0) return;
	if(_Caller._FindCode.findmulti) {
			var ls_rows = dg_1.GetCheckedRows();
			var ls_arr;
			if(ls_rows == null || ls_rows == "") {
				ls_arr = null;
			} else {
				ls_arr = new Array();
				for(var i = 0; i < ls_rows.length; i++) {
					ls_arr.push(dg_1.GetRowData(ls_rows[i] + 1));
				}
			}
			if(_IsModal) {
				window.returnValue = ls_arr;
			} else {
				if(_Caller) {	
					_Caller._FindCode.returnValue = ls_arr;
					if(ls_arr != null)
						_Caller.x_ReceivedCode(ls_arr);
				}
			}
			//setTimeout('x_Close()', 0);
			self.close();
	} else {
		x_ReturnRowData(dg_1, dg_1.GetRow());
	}
}


function x_Close() {
	if(_IsModal || _IsPopup) {
		window.close();
	} else {
		if(_Caller.$("#findmodal").length>0?_Caller.$("#findmodal").dialog("close"):window.close());
	}
}

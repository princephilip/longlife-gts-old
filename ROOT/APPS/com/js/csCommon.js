/****************************************************************
 * 
 * 파일명 : mighty-find-1.0.0.js
 * 설  명 : 코드찾기 popup JavaScript
 * 
 *    수정일      수정자     Version        Function 명
 * ------------    ---------   -------------  ----------------------------
 * 2013.02.10    김양열       1.0.0            최초생성
 * 
 * 
 */
var _CS;
if (!_CS) {
	_CS = {};
}

_CS.FindLaboMaster = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cs&fcd=FindLaboMaster&fnm="+encodeURIComponent('현장팀원찾기')+"&msize=0&fgrid=FindLaboMaster|CsFindCode|FindLaboMaster";
	_X.OpenFindWin2(a_win, param, '현장팀원찾기', 550, 600);
};
_CS.findLaboMaster = _CS.FindLaboMaster;

_CS.FindOsctConsult = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cs&fcd=FindOsctConsult&fnm="+encodeURIComponent('계약찾기')+"&msize=0&fgrid=FindOsctConsult|CsFindCode|FindOsctConsult";
	_X.OpenFindWin2(a_win, param, '계약찾기', 550, 550);
};
_CS.findOsctConsult = _CS.FindOsctConsult;

_CS.FindComCode = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cs&fcd=FindComCode&fnm="+encodeURIComponent('공통코드찾기')+"&msize=0&fgrid=FindComCode|CsFindCode|FindComCode";
	_X.OpenFindWin2(a_win, param, '공통코드찾기', 550, 550);
};
_CS.findComCode = _CS.FindComCode;

//사용화면:공사관리-중장비-장비계약등록 작성자:임형섭
_CS.FindEquiCode = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cs&fcd=FindEquiCode&fnm="+encodeURIComponent('장비코드찾기')+"&msize=0&fgrid=FindEquiCode|CsFindCode|FindEquiCode";
	//                      JS                                                                    GRID         XMLX       SELECT ID
	_X.OpenFindWin2(a_win, param, '장비코드찾기', 550, 550);
};
_CS.findEquiCode = _CS.FindEquiCode;

_CS.FindCsProject = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cs&fcd=FindCsProject&fnm="+encodeURIComponent('현장찾기')+"&msize=0&fgrid=FindCsProject|CsFindCode|FindCsProject";
	_X.OpenFindWin2(a_win, param, '현장찾기', 550, 550);
};
_CS.findCsProject = _CS.FindCsProject;


//사용화면:공사관리-기준정보-예산계정매핑관리 작성자:임형섭
_CS.FindBudgetCode = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cs&fcd=FindBudgetCode&fnm="+encodeURIComponent('예산코드찾기')+"&msize=0&fgrid=FindBudgetCode|CsFindCode|FindBudgetCode";
	_X.OpenFindWin2(a_win, param, '예산코드찾기', 550, 550);
};
_CS.FindBudgetCode = _CS.FindBudgetCode;


//사용화면:공사관리-기준정보-예산계정매핑관리 작성자:임형섭
_CS.FindAcntCode = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cs&fcd=FindAcntCode&fnm="+encodeURIComponent('계정과목코드찾기')+"&msize=0&fgrid=FindAcntCode|CsFindCode|FindAcntCode";
	_X.OpenFindWin2(a_win, param, '계정과목코드찾기', 550, 550);
};
_CS.FindAcntCode = _CS.FindAcntCode;

// 거래처찾기
_CS.FindCsCust = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cs&fcd=FindCsCust&fnm="+encodeURIComponent('거래처찾기')+"&msize=0&fgrid=FindCsCust|CsFindCode|FindCsCust";
	_X.OpenFindWin2(a_win, param, '거래처찾기', 550, 550);
};
_CS.findCsCust = _CS.FindCsCust;

// 자재찾기
_CS.FindCsMat = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cs&fcd=FindCsMat&fnm="+encodeURIComponent('자재찾기')+"&msize=0&fgrid=FindCsMat|CsFindCode|FindCsMat";
	_X.OpenFindWin3(a_win, param, '자재찾기', 800, 700);
};
_CS.findCsMat = _CS.FindCsMat;

// 단가변경
_CS.CostCh = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cs&fcd=CS502010&fnm="+encodeURIComponent('단가변경')+"&msize=0&fgrid=CS502010|CS_SALY_LABOR_ACCOUNT|CS502010_dg1";
	_X.OpenFindWin3(a_win, param, '단가변경', 650, 600);
};
_CS.costCh = _CS.CostCh;

// 사원등록
_CS.LaborCode = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cs&fcd=CS501003&fnm="+encodeURIComponent('노무자등록')+"&msize=0&fgrid=CS501003|CS_LABO_MASTER|CS501003_dg1";
	_X.OpenFindWin3(a_win, param, '노무자등록', 850, 570);
};
_CS.laborCode = _CS.LaborCode;


//작성자:임형섭
_CS.FindOrdrLevel = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cs&fcd=FindOrdrLevel&fnm="+encodeURIComponent('발주공종')+"&msize=0&fgrid=FindOrdrLevel|CsFindCode|FindOrdrLevel";
	_X.OpenFindWin2(a_win, param, '발주공종찾기', 680, 550);
};
_CS.FindOrdrLevel = _CS.FindOrdrLevel;


//작성자:임형섭
_CS.FindPerformCode = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cs&fcd=FindPerformCode&fnm="+encodeURIComponent('실행공종')+"&msize=0&fgrid=FindPerformCode|CS_PERF_DETAIL|CS_PERF_DETAIL_R01";
	_X.OpenFindWin3(a_win, param, '실행공종찾기', 700, 700);
};
_CS.FindPerformCode = _CS.FindPerformCode;


//작성자:임형섭
_CS.FindMatetmp = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cs&fcd=FindMatetmp&fnm="+encodeURIComponent('가설재 공종')+"&msize=0&fgrid=FindMatetmp|FindMatetmp_|FindMatetmp_";
	_X.OpenFindWin3(a_win, param, '가설재 공종 일괄복사', 700, 700);
};
_CS.FindMatetmp = _CS.FindMatetmp;

//작성자:임형섭
_CS.FindCsMatSpec = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cs&fcd=FindCsMatSpec&fnm="+encodeURIComponent('자재실행찾기')+"&msize=0&fgrid=FindCsMatSpec|CsFindCode|FindCsMatSpec";
	_X.OpenFindWin3(a_win, param, '자재실행찾기', 1070, 700);
};
_CS.FindCsMatSpec = _CS.FindCsMatSpec;

// 실행내역타현장복사
_CS.PerfDetailCopy = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cs&fcd=CS200400&fnm="+encodeURIComponent('타현장실행내역복사')+"&msize=0&fgrid=CS200400|CS_PERF_DETAIL|CS200400_dg1";
	_X.OpenFindWin3(a_win, param, '타현장실행내역복사', 1000, 800);
};
_CS.perfDetailCopy = _CS.PerfDetailCopy;

//작성자:임형섭 //공사관리-자재관리-자재입고(입고목차)
_CS.FindCsMatOrd = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cs&fcd=FindCsMatOrd&fnm="+encodeURIComponent('자재발주찾기')+"&msize=0&fgrid=FindCsMatOrd|CsFindCode|FindCsMatOrd";
	_X.OpenFindWin3(a_win, param, '자재발주찾기', 800, 700);
};
_CS.findCsMatOrd = _CS.FindCsMatOrd;

//작성자:임형섭 //공사관리-자재관리-자재출고(출고목차)
_CS.FindCsMatIn = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cs&fcd=FindCsMatIn&fnm="+encodeURIComponent('자재재고내역')+"&msize=0&fgrid=FindCsMatIn|CsFindCode|FindCsMatIn";
	_X.OpenFindWin3(a_win, param, '자재재고내역', 800, 700);
};
_CS.findCsMatIn = _CS.FindCsMatIn;

//작성자:임형섭 //공사관리-자재관리-가설재전출입요청
_CS.FindCsMatTmpdiv = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cs&fcd=FindCsMatTmpdiv&fnm="+encodeURIComponent('전출입대상자재찾기')+"&msize=0&fgrid=FindCsMatTmpdiv|CsFindCode|FindCsMatTmpdiv";
	_X.OpenFindWin3(a_win, param, '전출입대상자재찾기', 800, 700);
};
_CS.FindCsMatTmpdiv = _CS.FindCsMatTmpdiv;

// 퇴직금계산
_CS.RpayApply = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cs&fcd=CS503006&fnm="+encodeURIComponent('퇴직금계산')+"&msize=0&fgrid=CS503006|CS_SALY_LABOR_ACCOUNT|CS502010_dg1";
	_X.OpenFindWin3(a_win, param, '퇴직금계산', 900, 600);
};
_CS.rpayApply = _CS.RpayApply;

// 노무자 찾기
_CS.FindLaborCodeSc = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cs&fcd=FindLaborCodeSc&fnm="+encodeURIComponent('노무자찾기')+"&msize=0&fgrid=FindLaborCodeSc|CsFindCode|FindLaborCodeSc";
	_X.OpenFindWin2(a_win, param, '노무자찾기', 650, 500);
};
_CS.findLaborCodeSc = _CS.FindLaborCodeSc;



// 조달 - 추천자 찾기
_CS.FindPrPersMaster = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=cs&fcd=FindPrPersMaster&fnm="+encodeURIComponent('추천자찾기')+"&msize=0&fgrid=FindPrPersMaster|CsFindCode|FindPrPersMaster";
	_X.OpenFindWin2(a_win, param, '추천자찾기', 550, 550);
};
_CS.findPrPersMaster = _CS.FindPrPersMaster;




// 마지막 현장 -_csProjCode

function pf_last_proj(){
	
	if(typeof(S_PROJ)!="undefined" && S_PROJ.value != null && top._csProjCode != null){
		$('#S_PROJ').selectBox('destroy');
		$("#S_PROJ").val(top._csProjCode);
		$('#S_PROJ').selectBox();
	}
}

function pf_last_proj_save(a_val){
	
	if(a_val != '%'){
		top._csProjCode = a_val;
	}
}
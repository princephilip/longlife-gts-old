/****************************************************************
 *
 * 파일명 : mighty-find-1.0.0.js
 * 설  명 : 코드찾기 popup JavaScript
 *
 *    수정일      수정자     Version        Function 명
 * ------------    ---------   -------------  ----------------------------
 * 2013.02.10    김양열       1.0.0            최초생성
 *
 *
 */
var _PM;
if (!_PM) {
	_PM = {};
}

// 발주자재내역 수정요청 POPUP (2016.11.02 srJin)
_PM.FindMdfySeq = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win.name = 'PM0101071';
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=pm&fcd=PM0101071&fnm="+encodeURIComponent('수정요청')+"&msize=0&fgrid=PM0101071|PM0101071|PM0101071_01";
	_X.OpenFindWin3(a_win, param, '수정요청', 500, ($(document).height() > 610) ? 600 : $(document).height()-10);
}
_PM.findMdfySeq = _PM.FindMdfySeq;

//자재속성 찾기 2016.11.25
_PM.FindMatPro = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=com&fcd=FindMatPro&fnm="+encodeURIComponent('자재속성 찾기   (***한/영 변환이 안되면 이곳을 더블클릭하세요***)')+"&msize=0&fgrid=FindMatPro|FindCode|OpenCommonFind_01";
	_X.OpenFindWin2(a_win, param, '자재속성 찾기', 570, $(document).height()-420, _FIND_BTN_PARAM);
};
_X.findMatPro = _X.FindMatPro;

//단가변경내역
_PM.FindUnprHistory = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win.name = 'FindUnprHistory';
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=pm&fcd=FindUnprHistory&fnm="+encodeURIComponent('단가변경내역')+"&msize=0&fgrid=FindUnprHistory|PmFindCode|FindUnprHistory_01";
	_X.OpenFindWin3(a_win, param, '단가변경내역', 900, 400);
};
_PM.findUnprHistory = _PM.FindUnprHistory;


//마스터업체변경
_PM.FindPm2526 = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win.name = 'FindPm2526';
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=pm&fcd=PM2526&fnm="+encodeURIComponent('마스터업체변경')+"&msize=0&fgrid=PM2526|PM2526|PM2526_01";
	_X.OpenFindWin3(a_win, param, '마스터업체변경', 900, $(document).height()-300);
};
_PM.findPm2526 = _PM.FindPm2526;


//품의서등록
_PM.FindPm2527 = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win.name = 'FindPm2527';
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=pm&fcd=PM2527&fnm="+encodeURIComponent('품의서등록')+"&msize=0&fgrid=PM2527|PM2527|PM2527_01";
	_X.OpenFindWin3(a_win, param, '품의서등록', 990, $(document).height()-300);
};
_PM.findPm2527 = _PM.FindPm2527;


//청구자재찾기
_PM.FindMatrCustReq = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win.name = 'FindMatrCustReq';
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=pm&fcd=FindMatrCustReq&fnm="+encodeURIComponent('자재청구복사')+"&msize=0&fgrid=FindMatrCustReq|FindMatrCustReq|FindMatrCustReq_01";
	_X.OpenFindWin3(a_win, param, '자재청구 복사', 900, 600);
};
_PM.FindMatrCustReq = _PM.FindMatrCustReq;

//발주이력 상세
_X.FindOrderReq = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=pm&fcd=FindOrderReq&fnm="+encodeURIComponent('발주이력 상세')+"&msize=0&fgrid=FindOrderReq|PM_COMMON|FindOrderReq_01";
	_X.OpenFindWin2(a_win, param, '발주이력 상세', 1100, $(document).height()-400);
};

//자재정산 상세
_X.FindOrderAcntReq = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=pm&fcd=FindOrderAcntReq&fnm="+encodeURIComponent('정산내역 상세')+"&msize=0&fgrid=FindOrderReq|PM_COMMON|FindOrderAcntReq_01";
	_X.OpenFindWin2(a_win, param, '정산내역 상세', 1100, $(document).height()-400);
};

//덕트 선택
_X.FindDuct = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=pm&fcd=FindDuct&fnm="+encodeURIComponent('덕트선택')+"&msize=0&fgrid=FindDuct|PM_COMMON|FindDuctReq_00";
	_X.OpenFindWin3(a_win, param, '덕트선택', 760, 555);
};
_X.FindDuct = _X.FindDuct;

//덕트 상세 청구
_X.FindDuctReq = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=pm&fcd=FindDuctReq&fnm="+encodeURIComponent('덕트청구 상세')+"&msize=0&fgrid=FindDuctReq|PM_COMMON|FindDuctReq_01";
	_X.OpenFindWin3(a_win, param, '덕트청구 상세', 760, 555);
};
_X.FindDuctReq = _X.FindDuctReq;

//덕트 상세 업채
_X.FindDuctIn = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=pm&fcd=FindDuctReq&fnm="+encodeURIComponent('덕트청구 납품상세')+"&msize=0&fgrid=FindDuctReq|PM_COMMON|FindDuctReq_02";
	_X.OpenFindWin3(a_win, param, '덕트청구 납품상세', 760, 555);
};
_X.FindDuctIn = _X.FindDuctIn;


//공도구 검색
_X.FindMatrAsset = function(a_win, a_find_div, a_findstr, a_findcode, a_title, a_autoclose) {
	a_win.name = 'FindMatrAsset';
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	if(a_autoclose==null) {a_autoclose=true;}
	//var param = "sys=pm&fcd=FindMatrAsset&fnm="+encodeURIComponent(a_title)+"&msize=0&fgrid=FindMatrAsset|PM_COMMON|FindMatrAsset_01&mustinput=Y" + (a_autoclose?"&autoclose=N":"");
	var param = "sys=pm&fcd=FindMatrAsset&fnm="+encodeURIComponent(a_title)+"&msize=0&fgrid=FindMatrAsset|PM_COMMON|FindMatrAsset_01&"+ (a_autoclose?"&autoclose=Y":"");
	_X.OpenFindWin2(a_win, param, a_title, 700, 600);
};
_X.findMatrAsset = _X.FindMatrAsset;

//단가변경요청
_PM.FindPm2510 = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win.name = 'FindPm2510';
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=pm&fcd=PM2510&fnm="+encodeURIComponent('단가변경 요청 등록')+"&msize=0&fgrid=PM2510|PM2510|PM2510_01";
	//_X.OpenFindWin3(a_win, param, '단가변경 요청 등록', 1400, 800);

	// 파트너사이트에서 호출함
	var url = "/" + "XF030" + ".do?" + param;
	a_win._FindCode.returnValue = null;
	a_win._FindCode.title = '단가변경 요청 등록';

	var popWin = _X.OpenPopup(url, "", 1400, 820, null, null, a_win);
	_X.Block();
	popWin.focus();

	//0.5초단위로 윈도우 닫혔는지 체크
	var timer = setInterval(function() {
		try {
	    if(popWin.closed) {
        clearInterval(timer);
				_X.UnBlock();

				$("#"+a_win._FindCode.finddiv).focus();
				popWin = null;
	    }
	  }
		catch(e) {
      clearInterval(timer);
			_X.UnBlock();
			popWin = null;
		}
	},500);
};
_PM.findPm2510 = _PM.FindPm2510;

//발주단가변경요청
_PM.FindReqChangeCost = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win.name = 'FindReqChangeCost';
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=pm&fcd=FindReqChangeCost&fnm="+encodeURIComponent('단가변경 요청 등록')+"&msize=0&fgrid=FindReqChangeCost|FindReqChangeCost|FindReqChangeCost";
	//_X.OpenFindWin3(a_win, param, '단가변경 요청 등록', 1400, 800);

	// 파트너사이트에서 호출함
	var url = "/" + "XF030" + ".do?" + param;
	a_win._FindCode.returnValue = null;
	a_win._FindCode.title = '발주 단가변경 요청 등록';

	var popWin = _X.OpenPopup(url, "", 1400, 820, null, null, a_win);
	_X.Block();
	popWin.focus();

	//0.5초단위로 윈도우 닫혔는지 체크
	var timer = setInterval(function() {
		try {
	    if(popWin.closed) {
        clearInterval(timer);
				_X.UnBlock();

				$("#"+a_win._FindCode.finddiv).focus();
				popWin = null;
	    }
	  }
		catch(e) {
      clearInterval(timer);
			_X.UnBlock();
			popWin = null;
		}
	},500);
};
_PM.findReqChangeCost = _PM.FindReqChangeCost;

/****************************************************************
 *
 * 파일명 : mighty-find-1.0.0.js
 * 설  명 : 코드찾기 popup JavaScript
 *
 *    수정일      수정자     Version        Function 명
 * ------------    ---------   -------------  ----------------------------
 * 2013.02.10    김양열       1.0.0            최초생성
 * 2017.06.05    LJH        1.1.  _AC.PopupSlipList3 추가
 *
 */
var _AC;
if (!_AC) {
	_AC = {};
}

_AC.PopupSlipList = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var temp = a_findcode.split("|");
	var title = (temp[0] == "Slip" ? "전표" : "결의서");
	var param = "sys=ac&fcd=PopupSlipList&fnm="+encodeURIComponent('전표목록 선택')+"&msize=0&fgrid=PopupSlipList|PopupSlipList|PopupSlipList_R01";
	_X.OpenFindWin3(a_win, param, title+'목록 선택', 650, 600);
}
_AC.popupSlipList = _AC.PopupSlipList;

_AC.PopupSlipList2 = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var temp = a_findcode.split("|");
	var title = "대손상각비";
	var param = "sys=ac&fcd=PopupSlipList2&fnm="+encodeURIComponent('전표목록 선택')+"&msize=0&fgrid=PopupSlipList2|PopupSlipList2|PopupSlipList2_R01";
	_X.OpenFindWin3(a_win, param, title+' 상세', 730, 600);
}
_AC.popupSlipList2 = _AC.PopupSlipList2;


//어음등록, 일괄수령등록
_AC.BillRegister = function(a_win, a_find_div, a_findstr, a_findcode) {
	//alert(a_find_div+';'+a_findstr+';'+a_findcode);
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=af&fcd=BillRegister&fnm="+encodeURIComponent('어음등록 일괄수령')+"&msize=0&fgrid=BillRegister|BillRegister|BillRegister";
	//alert(param);
	_X.OpenFindWin3(a_win, param, '어음등록 일괄수령', 400, 250);
};
_AC.billRegister = _AC.BillRegister;


// SBTEC 용 slip list
_AC.PopupSlipList3 = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win.name = 'PopupSlipList2';
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var title = (a_findcode[0] == "Slip" ? "전표" : "결의서");
	var param = '';
	if (a_findcode[0] == "Slip") {
		param = "sys=ac&fcd=PopupSlipList2&fnm="+encodeURIComponent('전표 목록')  +"&msize=0&fgrid=PopupSlipList2|PopupSlipList|PopupSlipList_R03";
	} else {
		param = "sys=ac&fcd=PopupSlipList2&fnm="+encodeURIComponent('결의서 목록')+"&msize=0&fgrid=PopupSlipList2|PopupSlipList|PopupWorkList_R03";
	}

	_X.OpenFindWin2(a_win, param, title + ' 찾기', 650, 600, _FIND_BTN_PARAM);
}
_AC.popupSlipList3 = _AC.PopupSlipList3;


_AC.PopupSlipList_set = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win.name = 'PopupSlipList_set';
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=ac&fcd=AC0101001&fnm="+encodeURIComponent('설정전표 목록')  +"&msize=0&fgrid=AC0101001|AC0101001|AC0101001_01";

	_X.OpenFindWin2(a_win, param, '설정전표 찾기', 900, 600, _FIND_BTN_PARAM);
}
_AC.popupSlipList_set = _AC.PopupSlipList_set;



//계정과목 찾기 2017.06.12
_AC.FindAcntCode = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=ac&fcd=FindAcntCode&fnm="+encodeURIComponent('계정과목 찾기')+"&msize=0&fgrid=FindAcntCode|AC_FIND_CODE|FindAcntCode";
	_X.OpenFindWin2(a_win, param, '계정과목 찾기', 400, 600, _FIND_BTN_PARAM);
};
_AC.findAcntCode = _AC.FindAcntCode;

_AC.FindContractCode = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;

	var param = "sys=ac&fcd=FindContractCode&fnm="+encodeURIComponent('계약코드 찾기')+"&msize=0&fgrid=FindContractCode|AC_FIND_CODE|FindContractCode_dept";
	_X.OpenFindWin2(a_win, param, '계약코드 찾기', 500, 600, _FIND_BTN_PARAM);
};
_AC.findContractCode = _AC.FindContractCode;


_AC.FindCode = function(a_win, a_code, a_find_div, a_findstr, a_findmore) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findmore;

	// a_code : 코드 조건 ACNT, EMP, DEPT, CONT , BANK, FUND,  CUST, DEPO
	// a_find_div : 찾는 개체
	// a_findstr : 첫번째 무조건 쿼리에서 as_find/찾고자하는 코드및 코드명
	// a_findmore : 추가 검색 조건 array ( 필요시 as_cond1 ... 없으면 '')
	// recieved value 의 grid return 값은 CODE, CODE_NAME

	var ls_grid = 'AC_Find_Code';
	var ls_xml_file = 'AC_FIND_CODE';
	var ls_xml_id = '';
	var ls_title = ''
	var ls_width = 400;
	var ls_height = 500;
	var ls_param = '';

	switch (a_code) {
		case 'CONT':
			ls_title	= '계약코드 찾기'
			ls_xml_id   = 'FindCode_Contract';
			ls_param 	= new Array(mytop._CompanyCode, (a_findmore==null? '%':a_findmore)); // 조건은 company, dept(%)
			break;
		case 'ACNT':
			ls_title	= '계정과목 찾기'
			ls_xml_id   = 'FindCode_Acnt';
			ls_param	= new Array('Y');  // entry_tag
			break;
		case 'CUST':
			ls_title	= '거래처 찾기'
			ls_grid     = 'FindCustCode';
			ls_xml_id   = 'FindCustCode';
			break;
		case 'DEPT':
			ls_title	= '부서 찾기'
			ls_xml_id   = 'FindCode_Dept';  // ''
			break;
		case 'PROJ':
			ls_title	= '현장 찾기'
			ls_xml_id   = 'FindCode_Proj';  // ''
			break;
		case 'FUND':
			ls_title	= '자금코드 찾기'
			ls_grid     = 'FindFundCode';
			ls_xml_id   = 'FindCode_Fund'; // as_inouttag
			ls_param 	= new Array((a_findmore==null? new Array('%') :a_findmore));
			break;
		case 'BANK':
			ls_title	= '은행코드 찾기'
			ls_xml_id   = 'FindCode_Bank';  // ''
			break;
		case 'DEPO':
			ls_title	= '계좌번호 찾기'
			ls_grid     = 'FindDepositNo';
			ls_xml_id   = 'FindCode_Deposit';
			ls_param 	= a_findmore==null? new Array('%', '%') :a_findmore;   // acnt(%), bank(%)
			break;
		case 'CD':
			ls_title	= '카드코드 찾기'
			ls_xml_id   = 'FindCode_cd';
			ls_grid     = 'FindCode_cd';
			ls_param 	= a_findmore==null? new Array('%') :a_findmore;  // emp_no
			break;
		case 'EMP':
			ls_title	= '사원 찾기'
			ls_xml_id   = 'FindCode_Emp';
			ls_param 	= a_findmore==null? new Array('%', '%') :a_findmore;  // emp_no or name(%), dept(%)
			break;
		case 'PAY':
			ls_title	= '자금지급계좌 찾기'
			ls_grid     = 'FindDepositNo';
			ls_xml_id   = 'Find_PayDeposit'; // ''
			break;
	}

	_X.CommonFindCode(a_win, ls_title, 'ac', a_find_div, a_findstr, ls_param, ls_grid+"|"+ls_xml_file+"|"+ls_xml_id, ls_width, ls_height);
};
_AC.findCode = _AC.FindCode;


_AC.FindCodeName = function( a_div, a_find_str, a_findmore ) {

	var ls_xml_file = 'AC_FIND_CODE';
	var ls_xml_id = '';
	var ls_param = new Array( a_find_str );

	switch (a_div) {
		case 'CONT':
			ls_xml_id   = 'FindCode_Contract';
			ls_param 	= new Array(a_find_str, mytop._CompanyCode, a_findmore) ;
			break;
		case 'ACNT':
			ls_xml_id   = 'FindCode_Acnt';
			ls_param	= new Array(a_find_str, 'Y');
			break;
		case 'CUST':
			ls_xml_id   = 'FindCustCode';
			break;
		case 'DEPT':
			ls_xml_id   = 'FindCode_Dept';  // ''
			break;
		case 'PROJ':
			ls_xml_id   = 'FindCode_Proj';  // ''
			break;
		case 'FUND':
			ls_xml_id   = 'FindCode_Fund'; // as_inouttag
			ls_param	= new Array(a_find_str, '%');
			break;
		case 'BANK':
			ls_xml_id   = 'FindCode_Bank';  // ''
			break;
		case 'DEPO':
			ls_xml_id   = 'FindCode_Deposit';
			ls_param 	= new Array(a_find_str, '%', '%');
			break;
		case 'CD':
			ls_xml_id   = 'FindCode_cd';
			ls_param	= new Array(a_find_str, '%');
			break;
		case 'EMP':
			ls_xml_id   = 'FindCode_Emp';
			ls_param 	= new Array(a_find_str, '%', '%');
			break;
		case 'PAY':
			ls_xml_id   = 'Find_PayDeposit'; // ''
			break;
	}

	var ls_rtn = _X.XS("AC", "AC_FIND_CODE", ls_xml_id, ls_param, 'json2');
	return ls_rtn;
};
_AC.findCodeName = _AC.FindCodeName;

_AC.FindCommon = function(a_win, a_title, a_find_div, a_findstr, a_findmore, a_grid_info, a_width, a_height) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findmore;

	ls_width = (a_width == null || a_width == 0 ) ? 400 : a_width ;
	ls_height = (a_height == null || a_height == 0 ) ? 500 : a_height ;

	_X.CommonFindCode(a_win, a_title, 'ac', a_find_div, a_findstr, a_findmore, a_grid_info, ls_width, ls_height);
};
_AC.findCommon = _AC.FindCommon;

_AC.PopupNewSlip = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var title = '';
	switch (a_find_div) {
		case 'SLIP_NEW' : title = "전표 신규작성"; break;
		case 'WORK_NEW' : title = "결의서 신규작성"; break;
		case 'SLIP_COPY' : title = "전표 복사작성"; break;
		case 'WORK_COPY' : title = "결의서 복사작성"; break;
	}
	var param = "sys=ac&fcd=AC010100_P1&fnm="+encodeURIComponent(title)+"&msize=0&fgrid=AC010100_P1|AC010100|AC010100_P1";
	_X.OpenFindWin3(a_win, param, title , 300, 300);
};
_AC.popupNewSlip = _AC.PopupNewSlip;


_AC.OpenSlipWindow = function(a_type, a_slip_no) {
	if (a_slip_no=='') return;
	var ls_slip_array = a_slip_no.split('-');

	var ls_slip_div  = ls_slip_array[0];
	var ls_slip_date = ls_slip_array[1].replace('.','-').replace('.','-');
	var ls_slip_dept = ls_slip_array[2];
	var ls_slip_seq  = ls_slip_array[3];

	if (a_type == 'SLIP' ){
		mytop._X.MDI_Open('AC010100', mytop._MyPgmList['AC010100'].PGM_NAME, '/Mighty/template/XG_Slick1.jsp?pcd=AC010100&params='+ls_slip_div+'@'+ls_slip_date+'@'+ls_slip_dept+'@'+ls_slip_seq);
	}
	else {
		mytop._X.MDI_Open('AC010200', mytop._MyPgmList['AC010200'].PGM_NAME, '/Mighty/template/XG_Slick1.jsp?pcd=AC010200&params='+ls_slip_div+'@'+ls_slip_date+'@'+ls_slip_dept+'@'+ls_slip_seq);
	}

}
_AC.openSlipWindow = _AC.OpenSlipWindow;


_AC.execACProc = function( strProc, params) {

	_X.AjaxCall('AC', "EP", "json", strProc, _X.ArrayToStr(params) );

	if(_X_AjaxResult.result=="ERROR"){
		var proc_rtn = _X_AjaxResult;
		var ls_rtn = _X.XmlSelect("ac", "AC_COMMON", "PROC_MSG", new Array(strProc, mytop._UserID, mytop._ClientIP), "array");
		if ( ls_rtn == null ) {
			alert( '오류발생 : ' + proc_rtn.datas);
			return -1;
		}
		alert( ls_rtn[0] );
		return -1;
	} else if(_X_AjaxResult.result=="OK"){
		return 0;
	}
}
_AC.ExecACProc = _AC.execACProc;


//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 : 
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
//   
//===========================================================================================

var _IsModal = (window.dialogArguments?true:false);
var _IsPopup = (window.opener?true:false);
var _Caller  = (_IsModal?window.dialogArguments:(_IsPopup?window.opener:window.parent));

var ls_code =_Caller._FindCode.findcode;
	console.log(ls_code);
function x_InitForm2(){ 
  _X.InitGrid(grid_1, "dg_1", "100%", "100%", "com", "FindMatrCost", "FindMatrCost|FindMatrCost_01", false, false); 
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
  if(ab_allcompleted){
  	//dg_1.SetCheckBar(true); 
    x_DAO_Retrieve2(dg_1);
  }
}

function x_DAO_Retrieve2(){
	alert('a')
  var param = new Array('010100100001');
  console.log(param);
  dg_1.Retrieve(param); 
}

function xe_EditChanged2(a_obj, a_val){
	x_DAO_Retrieve2(dg_1);
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
  return 100;
}

function x_DAO_ChkErr2(){
  return true;
}

function x_DAO_Saved2(){ 
  return 100;
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}

function x_Insert_After2(a_dg, rowIdx){
}

function x_DAO_Duplicate2(a_dg, row){
  return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg){
  return 100;
}

function x_DAO_Excel2(a_dg){
  return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
  return 1;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){ 
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
  return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridDataLoad2(a_dg){
 
}

function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){
}

function x_ReceivedCode2(a_retVal){ 
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){
}

function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){   
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){
}

function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){
}

function xe_ChartPointClick2(a_dg, a_event){
}

function xe_ChartPointSelect2(a_dg, a_event){
}

function xe_ChartPointMouseOver2(a_dg){
}

function xe_ChartPointMouseOut2(a_dg, a_event){
}

function x_Close2() { 
  if(_IsModal || _IsPopup) {
    window.close();
  }else{
    if(_Caller.$("#findmodal").length>0?_Caller.$("#findmodal").dialog("close"):window.close());
  }
}

function x_Confirm(){
	var checkedRows = dg_1.GetCheckedRows(true);
	if(checkedRows.length==0) {
		_X.MsgBox("단가를 선택 하세요.");
		return;
	}

	_Caller.pf_addcall2(window.dg_1);
	
	x_Close2();
	
}
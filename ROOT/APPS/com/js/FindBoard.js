//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 :
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
//
//===========================================================================================
var is_div  = "";
var is_comp = "";
var is_id   = "";
var is_maxid = "";

var oEditors = [];

function x_InitForm2(){
  $("#xBtnRetrieve").hide();
  $("#xBtnAppend").hide();
  $("#xBtnDelete").hide();

  is_div  = _Caller._FindCode.findcode[0];
  is_comp = _Caller._FindCode.findcode[1];
  is_id   = _Caller._FindCode.findcode[2];

  _X.CreateEditor("ir1");

  _X.InitGrid(grid_1, "dg_1", "100%", "100%", "com", "FindBoard", "FindBoard|FindBoard", true, true);
  _X.InitGrid(grid_2, "dg_2", "100%", "100%", "com", "FindBoard", "FindBoard|FindBoard_File", false, false);   
  
  // if(is_div == '1'){
  //   $('#ntce_list').css("display","block");
  // }else if(ls_gbn == '2'){
  //   $('#ntce_list').css("display","none");
  // }
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
  if(ab_allcompleted){
    //_X.FormSetAllDisable("freeform", true);

  	setTimeout("x_DAO_Retrieve()",100);
  }
}

function x_DAO_Retrieve2(){
  // 수정가능 : 1.신규, 2.본인이작성, 3.관리자
  // 수정불가 : 조회 and 본인작성아님
  switch(is_div) {
    case  "I":  // 신규
          dg_1.InsertRow(0);
          $("#btn_file").show();
          $("#xBtnSave").show();
          break;
    case  "R":  // 조회
          dg_1.Retrieve([is_comp, is_id]);

          $("#ir1").text(dg_1.GetItem(dg_1.GetRow(), "BOARDCONTENTS"));
          oEditors.getById["ir1"].exec("LOAD_CONTENTS_FIELD");

          var ls_id = dg_1.GetItem(dg_1.GetRow(), "ATCH_FILE_ID");
          dg_2.Retrieve([ls_id]);

          if(mytop._UserID == dg_1.GetItem(dg_1.GetRow(), "WRITER_ID") || mytop._UserID == 'ADMIN' || mytop._UserID == 'DEVELOPER') {
            _X.FormSetAllDisable("freeform", false);
            _X.FormSetDisable([S_CONTENT], false);
            $("#btn_file").show();
            $("#xBtnSave").show();
            $("#xBtnDelete").show();
          } else {
            _X.FormSetAllDisable("freeform", true);
            _X.FormSetDisable([S_CONTENT], true);
            $("#btn_file").hide();
            $("#xBtnSave").hide();
            $("#xBtnDelete").hide();
          }
          break;
  }
}

function xe_EditChanged2(a_obj, a_val){
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
  var li_row = dg_1.GetRow();
  dg_1.SetItem(li_row, 'BOARDCONTENTS', oEditors.getById["ir1"].getIR());

  return 100;
}

function x_DAO_ChkErr2(){

  var cData  = dg_1.GetChangedData();

  if(cData.length > 0){
    for (i=0 ; i < cData.length; i++){
      if(cData[i].data.BOARDTITLE == "") {
        _X.MsgBox('확인', '제목을 입력 하세요.');
        return false;
      }

      if(cData[i].data.POPUP_YN == "Y") {
        if(cData[i].data.SDATE == "" || cData[i].data.EDATE == "" ) {
          _X.MsgBox('확인', '공지기간을 입력 하세요.');
          return false;
        }
        if(_X.StrPurify(cData[i].data.SDATE) > _X.StrPurify(cData[i].data.EDATE) ) {
          _X.MsgBox('확인', '공지기간을 확인 하세요.');
          return false;
        }
      }

      if(cData[i].data.BOARDCONTENTS == "") {
        _X.MsgBox('확인', '내용을 입력 하세요.');
        return false;
      }
    }
  }

  // 신규인경우 ID 부여
  if(is_div == "I") {
    is_maxid = _X.XmlSelect("com", "FindBoard", "GET_NEXT_ID", [], "array")[0][0];

    var li_row = dg_1.GetRow();
    dg_1.SetItem(li_row, 'BOARDID'  , is_maxid);
    dg_1.SetItem(li_row, 'WRITER'   , mytop._UserName);
    dg_1.SetItem(li_row, 'WRITER_ID', mytop._UserID);
  }

  return true;
}

function x_DAO_Saved2(){
  if(is_div == "I") {
    id_id = is_maxid;
    _X.ES("com", "FindBoard", "SM_BOARD_U01", [mytop._CompanyCode, id_id]); // 작성일자 update
    is_div = "R";
  }

  setTimeout("x_DAO_Retrieve()",100);
  return 100;
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}

function x_Insert_After2(a_dg, rowIdx){
  if(a_dg.id == 'dg_1') {
    dg_1.SetItem(rowIdx, "BOARDGB", "2");
    dg_1.SetItem(rowIdx, "SITE_DIV", mytop._CompanyCode);
    dg_1.SetItem(rowIdx, "POPUP_YN", "N");
  }
}

function x_DAO_Duplicate2(a_dg, row){
  return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg){
	if(_X.MsgBoxYesNo('확인', '삭제 하시겠습니까?') != 1) return;
	// 첨부파일 삭제
	for(i=1; i<=dg_2.RowCount(); i++) {
    var ls_id = dg_2.GetItem(a_row, "ATCH_FILE_ID");
    var ls_sn = dg_2.GetItem(a_row, "FILE_SN");
		_X.FileDelete(ls_id, ls_sn, false);
	}

	dg_1.DeleteRow(dg_1.GetRow());
		
	var li_result = x_SaveGridData("Y");
	if (li_result > 0) {
		x_Close();
	}else{
		// 저장오류
		_X.MsgBox("삭제중 오류가 발생했습니다.\n문제가 지속되면 시스템 개발자에게 문의해 주세요.");
	}

  return 0;
  //return 100;
}

function x_DAO_Excel2(a_dg){
  return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
  return 1;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
  return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridDataLoad2(a_dg){

}

function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){
  if(a_dg.id == 'dg_2') {
    switch(a_col) {
      case  "ORIGNL_FILE_NM":
            var ls_id = dg_2.GetItem(a_row, "ATCH_FILE_ID");
            var ls_sn = dg_2.GetItem(a_row, "FILE_SN");
            
            _X.FileDownload(ls_id, ls_sn);
            break;
    }
  }
}

function x_ReceivedCode2(a_retVal){
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){
}

function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){
}

function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){
}

function xe_ChartPointClick2(a_dg, a_event){
}

function xe_ChartPointSelect2(a_dg, a_event){
}

function xe_ChartPointMouseOver2(a_dg){
}

function xe_ChartPointMouseOut2(a_dg, a_event){
}

function x_Close2() {
  if(_IsModal || _IsPopup) {
    window.close();
  }else{
    if(_Caller.$("#findmodal").length>0?_Caller.$("#findmodal").dialog("close"):window.close());
  }
  _Caller.x_DAO_Retrieve();
}

function pf_file(){
  if(dg_1.GetItem(dg_1.GetRow(), "BOARDID") == "") {
    _X.MsgBox('확인', '첨부파일은 공지사항 저장 후에 등록해 주세요.');
    return;
  }

  var ls_id = dg_1.GetItem(dg_1.GetRow(), "ATCH_FILE_ID");
  _X.FileUpload(5, 'HO', "ATCH_FILE_ID", ls_id, false);
}

function x_FileUploaded(obj, ReturnValue) {
  if(ReturnValue==null) return;

  switch(obj) {
    case  "ATCH_FILE_ID":
          dg_1.SetItem(dg_1.GetRow(), "ATCH_FILE_ID"  , ReturnValue[0].atchFileId);
          dg_1.Save(null, null, "N");
          dg_2.Retrieve([ReturnValue[0].atchFileId]);
          break;
  }
}

function x_FileDeleted(obj) {
  switch(obj) {
    case  "ATCH_FILE_ID":
          dg_1.Save(null, null, "N");
          dg_2.Retrieve([dg_1.GetItem(dg_1.GetRow(), "ATCH_FILE_ID")]);
          break;
  }
}

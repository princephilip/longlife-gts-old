/****************************************************************
 * 
 * 파일명 : mighty-find-1.0.0.js
 * 설  명 : 코드찾기 popup JavaScript
 * 
 *    수정일      수정자     Version        Function 명
 * ------------    ---------   -------------  ----------------------------
 * 2013.02.10    김양열       1.0.0            최초생성
 * 
 * 
 */
var _SM;
if (!_SM) {
	_SM = {};
}

// 거래처 찾기
_SM.FindCustCode = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win.name = 'FindSiteCode';
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=sm&fcd=FindSiteCode&fnm="+encodeURIComponent('거래처 찾기')+"&msize=0&fgrid=FindCustCode|FindCustCode|FindCustCode";
	_X.OpenFindWin3(a_win, param, '사업장 찾기', 600, 600);
};
_SM.findCustCode = _SM.FindCustCode;


//그룹미등록프로그램 찾기
_SM.FindNotPgmCode = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=sm&fcd=SM0991&fnm="+encodeURIComponent('미등록프로그램찾기')+"&msize=0&fgrid=SM0991|SM_AUTH_PGMCODE|R_SM9901_01";
	_X.OpenFindWin3(a_win, param, '미등록프로그램 찾기', 700, 600);
};

//사용자미등록프로그램 찾기
_SM.FindNotPgmCode2 = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv = a_find_div;
	a_win._FindCode.findstr = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=sm&fcd=SM0992&fnm="+encodeURIComponent('미등록프로그램찾기')+"&msize=0&fgrid=SM0992|SM_AUTH_PGMCODE|R_SM0992_01";
	_X.OpenFindWin3(a_win, param, '미등록프로그램 찾기', 700, 600);
};

// 업무권한 찾기
_SM.FindAuthAct = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=sm&fcd=FindAuthAct&fnm="+encodeURIComponent('업무권한 찾기')+"&msize=0&fgrid=FindAuthAct|FindAuthAct|FindAuthAct";
	_X.OpenFindWin3(a_win, param, '업무권한 찾기', 490, 600);
};

// 사원 찾기
_SM.FindAuthUser = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=sm&fcd=FindAuthUser&fnm="+encodeURIComponent('사원 찾기')+"&msize=0&fgrid=FindAuthUser|FindAuthUser|FindAuthUser";
	_X.OpenFindWin3(a_win, param, '사원 찾기', 685, 600);
};

// 프로그램 권한 복사
_SM.FindAuthCopy = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=sm&fcd=FindAuthCopy&fnm="+encodeURIComponent('프로그램 권한 복사')+"&msize=0&fgrid=FindAuthCopy|FindAuthCopy|FindAuthCopy";
	_X.OpenFindWin3(a_win, param, '프로그램 권한 복사', 850, 600);
};

// 프로그램 권한 삭제
_SM.FindAuthDelete = function(a_win, a_find_div, a_findstr, a_findcode) {
	a_win._FindCode.finddiv  = a_find_div;
	a_win._FindCode.findstr  = a_findstr;
	a_win._FindCode.findcode = a_findcode;
	var param = "sys=sm&fcd=FindAuthDelete&fnm="+encodeURIComponent('프로그램 권한 삭제')+"&msize=0&fgrid=FindAuthDelete|FindAuthDelete|FindAuthDelete";
	_X.OpenFindWin3(a_win, param, '프로그램 권한 삭제', 850, 600);
};
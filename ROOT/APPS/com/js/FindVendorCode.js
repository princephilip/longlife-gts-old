﻿//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 : @FindVendorCode|거래처찾기
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
// 이요한       	 x-internetinfo      2014.12.15
//
//===========================================================================================
//	
//===========================================================================================

var _IsModal = (window.dialogArguments?true:false);
var _IsPopup = (window.opener?true:false);
var _Caller  = (_IsModal?window.dialogArguments:(_IsPopup?window.opener:window.parent));
var _COCORPDIVCD = _X.XmlSelect('rm', 'SM_COMCODE_D', 'W_RM0101_01', new Array('BI','CORPDIVCD','%'), 'json2');
var _COCORPTYPECD = _X.XmlSelect('rm', 'SM_COMCODE_D', 'W_RM0101_01', new Array('BI','CORPTYPECD','%'), 'json2');
var _MVendor_yn = '%';

function x_InitForm2(){
	
	if(typeof(xc_InitForm)!="undefined"){xc_InitForm();return;}
	if(typeof(_FIND_GRID)!="undefined") _X.SlickGrid(grid_1, "dg_1", "100%", "100%", _FIND_SYS, _FIND_GRID, _FIND_QUERY+"|" + _FIND_SELECT);
	else _X.SlickGrid(grid_1, "dg_1", "100%", "100%", _FIND_SYS, _FIND_CODE, "FindCode|" + _FIND_CODE);
	
	$("#FIND_CODE").focus();
	FIND_CODE.value = _Caller._FindCode.findstr;

	if(_Caller._FindCode.findcode == null || _Caller._FindCode.findcode == ""){
		_MVendor_yn = '%';
	}
	else
	{
		_MVendor_yn = _Caller._FindCode.findcode;
	}
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(a_dg.id == 'dg_1'){
		dg_1.SetCombo("VENDOR_DIV",_COCORPDIVCD);
		dg_1.SetCombo("VENDOR_TYPE",_COCORPTYPECD);
		//x_DAO_Retrieve(a_dg);
	}
	if(ab_allcompleted){
		if(_Caller._FindCode.findstr != "") setTimeout("x_DAO_Retrieve();", 100);
	}
}

function x_DAO_Retrieve2(a_dg){
	dg_1.Retrieve(new Array(mytop._CompanyCode,'%' + FIND_CODE.value+ '%', '%', '%', _MVendor_yn));
}

function xe_EditChanged2(a_obj, a_val){
	if(a_obj.id == 'S_CORPDIVCD' ||  a_obj.id == 'S_CORPTYPECD'){
		x_DAO_Retrieve();
	}
	
	if(typeof(xc_EditChanged)!="undefined"){
		xc_EditChanged(a_obj, a_val, a_label, a_cobj);
		return;
	}
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	switch(a_obj.id) {
		case "FIND_CODE":
			x_DAO_Retrieve();
			break;
	}
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){
	if(typeof(xc_InputKeyDown)!="undefined"){xc_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey);return;}

	if(a_obj==FIND_CODE && a_keyCode==KEY_DOWN)
		$('#dg_1').focus();
}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

function x_DAO_Saved2(){

}

function x_DAO_Insert2(a_dg, row){
	return 100;
}

function x_Insert_After2(a_dg, rowIdx){

}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){

}

function x_DAO_Delete2(a_dg){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){

}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 1;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){

}

function xe_GridDataChange2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){

}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){

}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){

}

function xe_GridDataLoad2(a_dg){
	$('#FIND_CODE').focus();
}

function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){
	if(a_dg.id=="dg_1" && a_colname == "SELECT_RETURN") {
		x_ReturnRowData(a_dg, a_row);
	}
}

function x_ReturnRowData(a_dg, a_row){
	if(typeof(xc_ReturnRowData)!="undefined"){xc_ReturnRowData(a_dg, a_row);return;}
	var rtValue = a_dg.GetRowData(a_row);
	//var rtValue = a_dg.GetItem(a_row,);
	if(_IsModal) {
		window.returnValue = rtValue;
	} else {
		if(_Caller) {	
			_Caller._FindCode.returnValue = rtValue;
			_Caller.x_ReceivedCode(rtValue); 
			
			//_Caller._X.UnBlock();
		}
	}
	setTimeout('x_Close()',0);
}

function x_Confirm(){
	if (dg_1.GetRow()==0) return;
	
	if(_Caller._FindCode.findmulti) {
			var ls_rows = dg_1.GetCheckedRows();
			var ls_arr;
			if(ls_rows == null || ls_rows == "") {
				ls_arr = null;
			} else {
				ls_arr = new Array();
				for(var i = 0; i < ls_rows.length; i++) {
					ls_arr.push(dg_1.GetRowData(ls_rows[i] + 1));
				}
			}
			if(_IsModal) {
				window.returnValue = ls_arr;
			} else {
				if(_Caller) {	
					_Caller._FindCode.returnValue = ls_arr;
					if(ls_arr != null)
						_Caller.x_ReceivedCode(ls_arr);
				}
			}
			setTimeout('x_Close()',0);
	} else {
		x_ReturnRowData(dg_1, dg_1.GetRow());
	}
}

function x_ReceivedCode2(a_retVal){
	
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){

}

function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
	if(typeof(xc_GridItemDoubleClick)!="undefined"){xc_GridItemDoubleClick(a_dg, a_row, a_col);return;}
	x_ReturnRowData(a_dg, a_row);
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){

}

function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){

}

function xe_ChartPointClick2(a_dg, a_event){

}

function xe_ChartPointSelect2(a_dg, a_event){

}

function xe_ChartPointMouseOver2(a_dg){

}

function xe_ChartPointMouseOut2(a_dg, a_event){

}

function x_Close2() {
	if(_IsModal || _IsPopup) {
		window.close();
	} else {
		if(_Caller.$("#findmodal").length>0?_Caller.$("#findmodal").dialog("close"):window.close());
	}
}

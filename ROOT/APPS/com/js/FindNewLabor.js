//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 :
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
//
//===========================================================================================

function x_InitForm2(){
  _X.InitGrid(grid_1, "dg_1", "100%", "100%", "com", "FindNewLabor", "FindNewLabor|FindNewLabor_01",false,false);

	// 이미지 클릭 이벤트
	$('.btn-example').click(function(){
	    var $href   = $(this).attr('href');
	    var imgPath = $(this).find("img").attr("src");
	    pf_layer_popup($href, imgPath);
	});  
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
  if(ab_allcompleted) {
    xe_BodyResize();
    if(_Caller.$("#S_PROJ_CD").val().length > 5) {
     $("#S_FIND").val(_Caller.$("#S_PROJ_CD").val());
    }


    x_DAO_Retrieve2();
    $("#S_FIND").focus();
  }
}

function x_DAO_Retrieve2(a_dg){
	var la_param = [S_FIND.value];
	dg_1.Retrieve(la_param);
}

function xe_EditChanged2(a_obj, a_val){
//  x_DAO_Retrieve(dg_1);
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
 if(a_obj.id=="S_FIND") {
		x_DAO_Retrieve(dg_1);
 }
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
  return 100;
}

function x_DAO_ChkErr2(){
  return true;
}

function x_DAO_Saved2(){
  return 100;
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}

function x_Insert_After2(a_dg, rowIdx){
}

function x_DAO_Duplicate2(a_dg, row){
  return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg){
  return 100;
}

function x_DAO_Excel2(a_dg){
  return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
  return 1;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
 if(a_newrow>0) {
		$("#photo_1").attr("src", "http://step.sebangtec.com" + dg_1.GetItem(a_newrow, "PHOTO_URL"));
 }
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridDataLoad2(a_dg){

}

function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){
}

function x_ReceivedCode2(a_retVal){
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){
}

function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
  if(a_row > 0 ) {
    _Caller._FindCode.returnValue = {
      "CARD_NO": dg_1.GetItem(a_row, "CARD_NO")
     , "PROJ_NAME": dg_1.GetItem(a_row, "PROJ_NAME")
     , "INSTALL_PLACE": dg_1.GetItem(a_row, "INSTALL_PLACE")
     , "CU_ID": dg_1.GetItem(a_row, "CU_ID")
     , "PHOTO_URL": dg_1.GetItem(a_row, "PHOTO_URL")
     , "CU_ID": dg_1.GetItem(a_row, "CU_ID")
     , "PROJ_CODE": dg_1.GetItem(a_row, "PROJ_CODE")

    }

    _Caller.x_ReceivedCode(_Caller._FindCode.returnValue);
    //_Caller.$("#RFID_NO").val(dg_1.GetItem(a_row, "CARD_NO"));
    x_Close();
  }
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){
}

function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){
}

function xe_ChartPointClick2(a_dg, a_event){
}

function xe_ChartPointSelect2(a_dg, a_event){
}

function xe_ChartPointMouseOver2(a_dg){
}

function xe_ChartPointMouseOut2(a_dg, a_event){
}

function x_Close2() {
  if(_IsModal || _IsPopup) {
    window.close();
  }else{
    if(_Caller.$("#findmodal").length>0?_Caller.$("#findmodal").dialog("close"):window.close());
  }
}

function x_Confirm(){
	alert("x_Confirm");
}


// 이미지 팝업
function pf_layer_popup(el, imgPath){
	$(".cont-div").html("<img src='" + imgPath + "' style='width: 100%; height: 100%;' >");
	
  var $el   = $(el);                          //레이어의 id를 $el 변수에 저장
  var isDim = $el.prev().hasClass('dimBg');   //dimmed 레이어를 감지하기 위한 boolean 변수

  isDim ? $('.dim-layer').fadeIn() : $el.fadeIn();

  var $elWidth  = ~~($el.outerWidth()),
      $elHeight = ~~($el.outerHeight()),
      docWidth  = $(document).width(),
      docHeight = $(document).height();

  // 화면의 중앙에 레이어를 띄운다.
  if ($elHeight < docHeight || $elWidth < docWidth) {
      $el.css({
          marginTop: -$elHeight /2,
          marginLeft: -$elWidth/2
      })
  } else {
      $el.css({top: 0, left: 0});
  }


  $('.dim-layer').click(function(){
	  $('.dim-layer').fadeOut();
	  return false;
  });
  
}

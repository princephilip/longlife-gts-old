
var fields_dg_1 = [
	{fieldName : 'SELECT_RETURN'			, dataType : 'text' },		
	{fieldName : 'LOC_CODE'           , dataType : 'text' },
	{fieldName : 'LOC_NAME'           , dataType : 'text' }
];

var columns_dg_1 = [
	{fieldName: 'SELECT_RETURN'			 , width: 23 , visible: true , styles: _Styles_textc, button: 'action', alwaysShowButton: true, header: {text: ' '} },
	{fieldName: 'LOC_CODE'           , width: 100, visible: true , styles: _Styles_textc, header: {text: '작업장소코드'}   },
	{fieldName: 'LOC_NAME'           , width: 150, visible: true , styles: _Styles_text , header: {text: '작업장소명'}   }
];


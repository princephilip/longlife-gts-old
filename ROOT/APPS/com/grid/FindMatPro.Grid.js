var columns_dg_1 = [
	{fieldName : 'HCODE'	 , width: 100, visible: false , readonly: false, editable: false, sortable: true, header: {text : "대코드"}		 			, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DCODE'	 , width: 150, visible: true , readonly: false, editable: false, sortable: true, header: {text : "자재속성코드"}		, styles: _Styles_text , 		editor: _Editor_text },
	{fieldName : 'SYS_ID'	 , width: 150, visible: false, readonly: false, editable: true , sortable: true, header: {text : "시스템ID"}				, styles: _Styles_text , 		editor: _Editor_text },
	{fieldName : 'DNAME'	 , width: 150, visible: true, readonly: false, editable: true , sortable: true, header: {text : "자재속성"}					, styles: _Styles_text , 		editor: _Editor_text }
];
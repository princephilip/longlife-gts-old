var columns_dg_1 = [
	{fieldName : 'ACNT_CODE'	      , width: 80, visible: true , readonly: false, editable: false, sortable: true, header: {text : "계정코드"}		 			, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ACNT_NAME'	      , width: 150, visible: true , readonly: false, editable: false, sortable: true, header: {text : "계정과목명"}				, styles: _Styles_text , 		editor: _Editor_text },
  {fieldName : 'COST_ACNT_CODE'   , width: 80, visible: true , readonly: false, editable: false, sortable: true, header: {text : "공사계정코드"}     , styles: _Styles_textc,    editor: _Editor_text }, 
  {fieldName : 'CONST_ACNT_NAME'  , width: 150, visible: true , readonly: false, editable: false, sortable: true, header: {text : "공사계정과목명"}   , styles: _Styles_text ,    editor: _Editor_text },
	{fieldName : 'SUM_TAG'	        , width: 150, visible: false, readonly: false, editable: true , sortable: true, header: {text : "대분류명"}					, styles: _Styles_text , 		editor: _Editor_text }
];

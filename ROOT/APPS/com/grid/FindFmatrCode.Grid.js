
var fields_dg_1 = [
	{fieldName : 'SELECT_RETURN'			, dataType : 'text' },		
	{fieldName : 'FMATR_CODE'         , dataType : 'text' },
	{fieldName : 'FMATR_NAME'         , dataType : 'text' },
	{fieldName : 'EXPLAIN'            , dataType : 'text' }
];

var columns_dg_1 = [
	{fieldName: 'SELECT_RETURN'				, width: 23 , styles: _Styles_textc, button: 'action', alwaysShowButton: true, header: {text: ' '} },
	{fieldName: 'FMATR_CODE'          , width: 100, styles: _Styles_textc, header: {text: '마감재코드'}         },
	{fieldName: 'FMATR_NAME'          , width: 150, styles: _Styles_text, header: {text: '마감재명'}            },
	{fieldName: 'EXPLAIN'             , width: 150, styles: _Styles_text, header: {text: '비고'}        }
];


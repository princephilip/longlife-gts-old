var columns_dg_1 = [
	{fieldName : 'UPPER1_CODE'        , width: 120, visible: false , readonly: false, editable: false, sortable: true, merge: true, header: {text : "구분"}           , styles: _Styles_text,   editor: _Editor_text },
    {fieldName : 'UPPER1_NAME'        , width: 100, visible: true , readonly: false, editable: false, sortable: true, merge: true, header: {text : "대분류"}       , styles: _Styles_text,   editor: _Editor_text },
    {fieldName : 'UPPER2_CODE'	      , width: 80,  visible: false , readonly: false, editable: false, sortable: true, merge: true, header: {text : "회계계정코드"}		, styles: _Styles_textc, 	editor: _Editor_text },
	{fieldName : 'UPPER2_NAME'	      , width: 110, visible: true , readonly: false, editable: false, sortable: true, merge: true, header: {text : "중분류"}			, styles: _Styles_text , 	editor: _Editor_text },
    {fieldName : 'UPPER3_CODE'        , width: 80,  visible: false , readonly: false, editable: false, sortable: true, merge: true, header: {text : "공사계정코드"}   , styles: _Styles_textc,    editor: _Editor_text },
    {fieldName : 'UPPER3_NAME'	      , width: 120, visible: true , readonly: false, editable: false, sortable: true, merge: true, header: {text : "소분류"}			, styles: _Styles_text , 	editor: _Editor_text },
    {fieldName : 'CONST_ACNT_CODE'    , width: 80,  visible: false , readonly: false, editable: false, sortable: true, merge: false, header: {text : "공사계정코드"}   , styles: _Styles_textc,    editor: _Editor_text },
    {fieldName : 'CONST_ACNT_NAME'	  , width: 150, visible: true , readonly: false, editable: false, sortable: true, merge: false, header: {text : "공사계정명"}			, styles: _Styles_text , 	editor: _Editor_text }
    ];

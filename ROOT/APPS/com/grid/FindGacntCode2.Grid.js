var columns_dg_1 = [
	{fieldName : 'ACNT_WORD1'       , width: 80, visible: true , readonly: false, editable: false, sortable: true, merge: true, header: {text : "구분"}           , styles: _Styles_text,   editor: _Editor_text }, 
  {fieldName : 'ACNT_WORD2'       , width: 200, visible: true , readonly: false, editable: false, sortable: true, merge: false, header: {text : "사용내역"}       , styles: _Styles_text,   editor: _Editor_text }, 
  {fieldName : 'ACNT_CODE'	      , width: 80,  visible: false , readonly: false, editable: false, sortable: true, merge: false, header: {text : "회계계정코드"}		, styles: _Styles_textc, 	editor: _Editor_text }, 
	{fieldName : 'ACNT_NAME'	      , width: 170, visible: true , readonly: false, editable: false, sortable: true, merge: false, header: {text : "회계계정명"}			, styles: _Styles_text , 	editor: _Editor_text },
  {fieldName : 'COST_ACNT_CODE'   , width: 80,  visible: false , readonly: false, editable: false, sortable: true, merge: false, header: {text : "공사계정코드"}   , styles: _Styles_textc,    editor: _Editor_text }, 
  {fieldName : 'CONST_ACNT_NAME'  , width: 170, visible: true , readonly: false, editable: false, sortable: true, merge: false, header: {text : "공사계정명"}     , styles: _Styles_text ,    editor: _Editor_text }
];

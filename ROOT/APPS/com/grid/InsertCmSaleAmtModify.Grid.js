_AutoRetrieve = true;

var columns_dg_1 = [ 
		
	{fieldName:'ORDER_ID'      		, width: 100,  must_input: 'N', visible: false,  readonly: true ,  editable: true,  sortable: true,  header: {text: "견적서ID"}					,    styles: _Styles_number		, editor: _Editor_number},
	{fieldName:'CALC_SEQ'      		, width: 100,  must_input: 'N', visible: false ,  readonly: true ,  editable: true,  sortable: true,  header: {text: "변경순번"}					,    styles: _Styles_number		, editor: _Editor_number},
	{fieldName:'CALC_YYMM'      	, width: 100,  must_input: 'N', visible: true ,  readonly: false ,  editable: true,  sortable: true,  header: {text: "정산월"}						,    styles: _Styles_datemonth		, editor: _Editor_datemonth},
	{fieldName:'CUST_ID'      		, width: 100,  must_input: 'N', visible: false,  readonly: true ,  editable: true,  sortable: true,  header: {text: "거래처번호"}				,    styles: _Styles_number		, editor: _Editor_number},
	{fieldName:'CUST_NAME'      	, width: 200,  must_input: 'N', visible: true ,  readonly: true ,  editable: true,  sortable: true,  header: {text: "고객사"}					,      styles: _Styles_text		, editor: _Editor_text, button: "action", alwaysShowButton: true},
	{fieldName:'SUPPLY_AMT'       , width: 100,  must_input: 'N', visible: true,  readonly: false,  editable: true,  sortable: true,  header: {text: "변경공급가액"}		,      styles: _Styles_number		, editor: _Editor_number , footerExpr: "SUM"},
	{fieldName:'VAT_AMT'          , width: 80,  must_input: 'N', visible: false,  readonly: false,  editable: true,  sortable: true,  header: {text: "부가세"}		,            styles: _Styles_number		, editor: _Editor_number , footerExpr: "SUM"},
	{fieldName:'TOT_AMT'          , width: 100,  must_input: 'N', visible: false,  readonly: false,  editable: true,  sortable: true,  header: {text: "매출액"}		,        styles: _Styles_number		, editor: _Editor_number , footerExpr: "SUM"},
	{fieldName:'MOD_COST'      		, width: 100,  must_input: 'N', visible: true ,  readonly: false,  editable: true,  sortable: true,  header: {text: "원가조정액"}				,    styles: _Styles_number		, editor: _Editor_number , footerExpr: "SUM"},
	{fieldName:'BILL_SEQ'         , width: 100,  must_input: 'N', visible: false,  readonly: false,  editable: true,  sortable: true,  header: {text: "계산서번호"}		,    styles: _Styles_number		, editor: _Editor_number}
		
	];

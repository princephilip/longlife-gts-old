<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
 /**
  * @Class Name  : ExcelRegist.jsp
  * @Description : ExcelRegist 화면
  * @Modification Information
  * @
  * @  수정일             수정자                   수정내용
  * @ -------    --------    ---------------------------
  * @ 2009.04.01   이중호              최초 생성
  *
  *  @author 공통서비스팀
  *  @since 2009.04.01
  *  @version 1.0
  *  @see
  *
  */
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<base target="_self" /> 
<title>엑셀파일 업로드</title>
<script type="text/javaScript" src="<c:url value='/Mighty/js/jquery-1.8.3.min.js'/>"></script>
<script type="text/javaScript" src="<c:url value='/Mighty/js/jquery-3.28.0.form.js'/>"></script>
<script type="text/javaScript" language="javascript">
<!--

function fn_init(){
	
	var ls_pqueryid = window.dialogArguments["pquery"];
	document.all.pqueryid.value = ls_pqueryid;
	
	var ls_pqueryid = window.dialogArguments["equery"];
	document.all.equeryid.value = ls_pqueryid;
	
}

/* ********************************************************
 * 저장처리화면
 ******************************************************** */

$(document).ready(function(){

	$("#Form").ajaxForm({ 
		beforeSubmit : function(){
			return fn_filevalidate();
		},
		dataType : 'json',
		success : function(data){
			fn_success(data); 
		}
	});

});


function fn_success(a_data){
	
	var args = new Array();
	args["rslt"] = a_data.result;
	args["upid"] = a_data.upid;

	window.returnValue = args;
	alert('excel 업로드가 정상처리 되었습니다!');
	window.close();
}

function fn_filevalidate(){
	
	var varForm				 = document.getElementById("Form");

	// 파일 확장명 확인
	var arrExt      = "xls";
	var arrExt1     = "xlsx";
	var objInput    = varForm.elements["fileNm"];
	var strFilePath = objInput.value;
	var arrTmp      = strFilePath.split(".");
	var strExt      = arrTmp[arrTmp.length-1].toLowerCase();

	if (!(arrExt == strExt || arrExt1 == strExt)) {
		alert("엑셀 파일을 첨부하지 않았습니다.\n확인후 다시 처리하십시오.");
		return false;
	}
	return true;
}

function fn_cancel(){
	window.returnValue = null;
	window.close();
}

-->
</script>
</head>
<body onload="fn_init();">
<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
<!-- 엑셀 등록 메시지  -->
${sResult}
<div id="content" style="width:420px;height:260px;background:url('${pageContext.request.contextPath}/images/excel/pop_bg.jpg') no-repeat;">
<!-- 상단타이틀 -->
<form name="Form" id="Form" method="post" target="_self" action="/com/jsp/excelTempRegist.do" enctype="multipart/form-data" >
	<div style="height:45px;padding-left:45px;padding-top:35px;">
	<span style="font-size: 14px;font-weight:bold;color:#326004;">엑셀파일 업로드</span>
	</div>
	<div style="height:90px;padding-left:50px;padding-top:50px;">
		<img src="<c:url value='${pageContext.request.contextPath}/images/excel/pop_icon.png' />" alt=""  width="4" height="6">&nbsp;<span style="font-size: 12px;font-weight:bold;color:#326004;">엑셀 파일</span>&nbsp;<input name="fileNm" type="file" id="fileNm"/>
	</div>
	<div style="height:30px;padding-top:10px;text-align: center;">
		<input type="image" src="/Theme/images/excel/btn_upload1.png">
		<input type="image" src="/Theme/images/excel/btn_cancel1.png" onclick="fn_cancel(); return false;">
		<!--<span class="button"><input type="submit" class="btn_submit" value="업로드"></span><span class="button"><input type="submit" value="취  소" onclick="fn_cancel(); return false;"></span>-->
	</div>

<!-- 상단 타이틀  영역 -->
			<!--
			<table width="100%" cellpadding="8" border="0">
			 <tr>
			  <td width="100%" class="title_left">
			  	<h1 class="title_left">
					<img src="<c:url value='${pageContext.request.contextPath}/images/egovframework/com/cmm/icon/tit_icon.gif' />" width="16" height="16" hspace="3" style="vertical-align: middle" alt="제목아이콘이미지">
					&nbsp;엑셀파일 등록
					</h1>
				</td>
			 </tr>
			</table>
			
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td height="3px"></td>
				</tr>
			</table>
			
			<table width="100%" border="0" cellpadding="0" cellspacing="1" class="table-register" summary="TEMP 엑셀파일을 첨부할 수 있는 등록 테이블이다.">
			<CAPTION style="display: none;">엑셀파일 등록</CAPTION>
			  <tr>
			    <th width="20%" height="23" class="required_text" scope="row" nowrap >
			    	<label for="fileNm">엑셀파일</label>
			    	<img src="<c:url value='${pageContext.request.contextPath}/images/egovframework/com/cmm/icon/required.gif' />" alt="필수입력표시"  width="15" height="15">
			    </th>
			  	<td><input name="fileNm" type="file" id="fileNm"/></td>
			  </tr>
			</table>
			
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			    <td height="10"></td>
			  </tr>
			</table>
			
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td height="3px"></td>
				</tr>
			</table>
			
			<table border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
			  <td><span class="button"><input type="submit" value="저장" onclick="fn_regist_ExcelTemp(); return false;"></span></td>
			  <td><span class="button"><input type="submit" class="btn_submit" value="업로드"></span></td>
			  <td><span class="button"><input type="submit" value="취  소" onclick="fn_cancel(); return false;"></span></td>
			</tr>
			</table>
			-->


<input name="cmd" type="hidden" value="ExcelRegist"/>
<input name="pqueryid" type="hidden" value=""/>     
<input name="equeryid" type="hidden" value=""/> 
</form>
</div> 
</body>
</html>
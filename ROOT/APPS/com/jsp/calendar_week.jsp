<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<jsp:include page="/Mighty/include/COMM_JS.jsp"/>

<style>
	table.summary_total { font-size:9pt; font-family:"굴림"; padding:1px; }
	table.summary_total th { font-weight: bold; text-align: center; padding: 4px 0; color: #666; background-color: #efefef; border-bottom: 1px dashed #999; border-right: 1px dashed #999; }
	table.summary_total td { text-align:right; font-weight: bold; padding: 4px 4px 4px 0; border-bottom: 1px dashed #999; border-right: 1px dashed #999; color: #666; }
	table.summary_total td.total_td { background-color: #efefef; color: #666; }
	table td.htitle { background-color: #efefef; font-weight: bold; color: #666; text-align: center; padding:2px;}
	table td.detldata { color: #666; text-align: center;}
</style>

<%
// DecimalFormat df = new DecimalFormat("00");
// Calendar calendar = Calendar.getInstance();

// String curr_yy = Integer.toString(calendar.get(Calendar.YEAR)); //년도를 구한다
// String curr_mm = df.format(calendar.get(Calendar.MONTH) + 1); //달을 구한다
// String curr_dd = df.format(calendar.get(Calendar.DATE)); //날짜를 구한다
// String curr_dy = "";

// int iDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK); //요일을 구한다
// switch(iDayOfWeek){
//   case 1: curr_dy = "일"; break;
//   case 2: curr_dy = "월"; break;
//   case 3: curr_dy = "화"; break;
//   case 4: curr_dy = "수"; break;
//   case 5: curr_dy = "목"; break;
//   case 6: curr_dy = "금"; break;
//   case 7: curr_dy = "토"; break;
// }

	String 	year = "";
	String 	month = "";
	String 	day = "";
	//int 	lastday=0;
	int 	rowCnt=0;
	int 	startWeek=0;
	//String 	calendarHtml = "";

	Calendar today 		= Calendar.getInstance();
    Calendar firstDay 	= Calendar.getInstance();
    Calendar lastDay  	= Calendar.getInstance();

	StringBuffer sb = new StringBuffer(1024);
	year  = request.getParameter("year");
	month = request.getParameter("month");
	day   = request.getParameter("day");

	if(year==null)
		year=Integer.toString(today.get(Calendar.YEAR));
	if(month==null)
		month=Integer.toString(today.get(Calendar.MONTH)+1);
	if(day==null)
		day=Integer.toString(today.get(Calendar.DATE));

 //    firstDay.set(Integer.parseInt(year),Integer.parseInt(month)-1,1);  	// 해당 월의 다음 달의 첫 날짜를 구한다.
 //    lastDay.set(Integer.parseInt(year),Integer.parseInt(month),1);  	// 해당 월의 다음 달의 첫 날짜를 구한다.
 //    lastDay.add(Calendar.DATE,-1); 				// 해당 월의 마지막날을 구한다.

	// lastday = lastDay.get(Calendar.DATE);
	// if(Integer.parseInt(day)>lastday)
	// 	day=Integer.toString(lastday);

	// startWeek = firstDay.get(Calendar.DAY_OF_WEEK);

	// if(firstDay.get(Calendar.DAY_OF_WEEK)!=Calendar.SUNDAY)
	// {
	// 	sb.append("<TR align=left valign=top>");
	// 	rowCnt++;
	// 	for(int i=1;i<startWeek;i++)
	// 		sb.append("<TD></TD>");
	// }
	// for(int i=1;i<=lastday;i++)
	// {
	// 	Calendar dt = Calendar.getInstance();
 //    	dt.set(Integer.parseInt(year),Integer.parseInt(month) -1,i);  		// 해당 월의 다음 달의 첫 날짜를 구한다.
	// 	String font1 = (dt.get(Calendar.DAY_OF_WEEK)==Calendar.SUNDAY?"<font color=red>":dt.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY?"<font color=blue>":"");
	// 	String font2 = (dt.get(Calendar.DAY_OF_WEEK)==Calendar.SUNDAY||dt.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY?"</font>":"");
	// 	if(dt.get(Calendar.DAY_OF_WEEK)==Calendar.SUNDAY)
	// 	{
	// 		sb.append("<TR align=left valign=top>");
	// 		rowCnt++;
	// 	}
	// 	sb.append("<TD id='" + "td_day_" + i + "' onclick='SelectionChanged(" + i + ");'>" + font1 + i + font2 + "</TD>");
	// 	if(dt.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY)
	// 		sb.append("</TR>");
	// }
	// if(lastDay.get(Calendar.DAY_OF_WEEK)!=Calendar.SATURDAY)
	// {
	// 	for(int i=lastDay.get(Calendar.DAY_OF_WEEK);i<6;i++)
	// 		sb.append("<TD></TD>");
	// 	sb.append("</TR>");
	// }
	// calendarHtml = sb.toString();

%>

<HTML>
	<HEAD>
		<title></title>
	</HEAD>
	<body onload="jf_initial()">
	<table width="100%" height="100%" border="1" bordercolor="#c0c0c0" cellspacing="0" cellpadding="2"
		style="FONT-SIZE: 9pt; FONT-FAMILY: 굴림; BORDER-COLLAPSE: collapse">
		<tr height="30" align="center" valign="middle" bgcolor="#efeffa">
			<td width="14.2%" id="td_title_1"></td>
			<td width="14.3%" id="td_title_2"></td>
			<td width="14.3%" id="td_title_3"></td>
			<td width="14.3%" id="td_title_4"></td>
			<td width="14.3%" id="td_title_5"></td>
			<td width="14.3%" id="td_title_6"></td>
			<td width="14.3%" id="td_title_7"></td>
		</tr>
		<tr height="95%" align="left" valign="top">
			<td id="td_day_1" onclick='SelectionChanged(1);' tdData="" style="FONT-FAMILY: 돋음;FONT-SIZE: 8pt;overflow-y:auto"></td>
			<td id="td_day_2" onclick='SelectionChanged(2);' tdData="" style="FONT-FAMILY: 돋음;FONT-SIZE: 8pt;overflow-y:auto"></td>
			<td id="td_day_3" onclick='SelectionChanged(3);' tdData="" style="FONT-FAMILY: 돋음;FONT-SIZE: 8pt;overflow-y:auto"></td>
			<td id="td_day_4" onclick='SelectionChanged(4);' tdData="" style="FONT-FAMILY: 돋음;FONT-SIZE: 8pt;overflow-y:auto"></td>
			<td id="td_day_5" onclick='SelectionChanged(5);' tdData="" style="FONT-FAMILY: 돋음;FONT-SIZE: 8pt;overflow-y:auto"></td>
			<td id="td_day_6" onclick='SelectionChanged(6);' tdData="" style="FONT-FAMILY: 돋음;FONT-SIZE: 8pt;overflow-y:auto"></td>
      <td id="td_day_7" onclick='SelectionChanged(7);' tdData="" style="FONT-FAMILY: 돋음;FONT-SIZE: 8pt;overflow-y:auto"></td>
		</tr>
	</table>
	</body>

<script type="text/javascript">
	var curYear	= <%=year%>;
	var curMonth= <%=month%>;
	var curDay =  <%=day%>;
	var curIndex =  2;
	var rowCnt	= <%=rowCnt%>;
	var startWeek = <%=startWeek%>;
	

	var init = "Y";

	function jf_initial(){
		if(parent!=null&&typeof(parent.jf_CalendarLoaded)!="undefined"){
			SelectionChanged(curIndex);
			init = "N";
		}
	}

	function SelectionChanged(newIndex){
		if(parent!=null&&typeof(parent.DateChanged)!="undefined"){
			if(!parent.DateChanged(newIndex,init))
				return;
		}
		document.getElementById("td_day_"+curIndex).bgColor="";
		document.getElementById("td_day_"+newIndex).bgColor="#CCCCCC";   
		curIndex = newIndex;
	}


	function SetData(day, data, cdate){
		if(document.getElementById("td_day_"+day)!=null)
		{
			document.getElementById("td_day_"+day).innerHTML = data;
			document.getElementById("td_day_"+day).tdData    = cdate;
		}
	}
	
	function SetTitle(day, data){
		document.getElementById("td_title_"+day).innerHTML= data;
	}

	function GetDayHtml(day){
		var preHtml = document.getElementById("td_day_"+day).innerHTML;
		if(preHtml.indexOf("<BR>")>0)
			return preHtml.substring(0,preHtml.indexOf("<BR>"));
		else
			return preHtml;
	}


</script>


</HTML>


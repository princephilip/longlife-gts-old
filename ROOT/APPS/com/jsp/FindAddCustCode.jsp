<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<style>
#btn_search {
  border: 1px solid transparent;
  border-bottom-left-radius: 0;
  border-top-left-radius: 0;
  height: 32px;
  margin: 0;
  outline: none;
  padding: 0 0;
  width: 50px;
  -webkit-box-shadow: none;
  box-shadow: none;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  background: #0f6ba8;
  vertical-align: top;
}

.gb_mag {
    display: inline-block;
    height: 25px;
    width: 25px;
    background-position: 0px -1625px;
    margin: 1px;
    margin-top: 2px;
}

#FIND_CODE {
  border-style: solid;
  border-width: 2px 0px 2px 2px;
  border-color: #0f6ba8;
  /*border-color: #c0dade;*/
  background-color: #ffffff;
  height:26px;
  margin:0px 0px 0px 0px;
  padding:1px 0px 1px 4px;
  /*margin:0px 0px 2px 1px;  padding:0px 1px 1px 1px;*/
  text-align:left;
  vertical-align:middle;
  color:#000000;
}

.btn-info {
	padding: 8px 10px;
	font-weight: 700;
}

.btn-warning, .btn-default {
	padding: 4px 20px;
	font-weight: 700;
}

.detail_box {
	border-top: 0;
}
</style>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>


<div id='div_criteria' style='float:left;margin-bottom:5px;line-height:30px;'>
	<input id='FIND_CODE' type='text'><button id="btn_search" aria-label="검색" name="" ><span class="gb_bg gb_mag"></span></button>
</div>
<div id='wrap_btn' class='grid_div ml10'>
	<button id='btn_new_cust' class="btn btn-info w120">신규 거래처 추가</button>
</div>
<div class='detail_input_bg noborder w5'>
</div>


<div id='ff_left' class='grid_div'>
	<div id='freeform' class='detail_box ver2 has_title'>
		<label class='sub_title'>거래처 기본정보</label>

		<div class='detail_row detail_row_bb'>
			<label class='detail_label w100'>거래처구분</label>
			<div class='detail_input_bg w70'>
				<select id='CUST_TAG'></select>
			</div>

			<label class='detail_label w90'>*사업자번호</label>
			<div class='detail_input_bg w120'>
				<input id='CUST_CODE' type='text' class='detail_cust tac' >
			</div>

			<label class='detail_label w70'>*상호명</label>
			<div class='detail_input_bg'>
				<input id='CUST_NAME' type='text'>
			</div>

		</div>
	</div>

	<div id='freeform5' class='detail_box ver2 has_title'>
		<label class='sub_title'>거래처 상세정보</label>

		<div class='detail_row '>

			<label class='detail_label w100'>법인등록번호</label>
			<div class='detail_input_bg w120'>
				<input id='REGISTER_NO' type='text' >
			</div>

			<label class='detail_label w60'>업태</label>
			<div class='detail_input_bg w150'>
				<input id='CONDITION' 	type='text'>
			</div>

			<label class='detail_label w80'>과세정보</label>
			<div class='detail_input_bg'>
				<select id='BUSINESS_TYPE'></select>
			</div>
		</div>

		<div class='detail_row'>
			<label class='detail_label w100'>대표자</label>
			<div class='detail_input_bg w120'>
				<input id='OWNER' type='text' >
			</div>

			<label class='detail_label w60'>업종</label>
			<div class='detail_input_bg w150'>
				<input id='CATEGORY' 	type='text'>
			</div>

			<label class='detail_label w80'>지급그룹</label>
			<div class='detail_input_bg'>
				<select id='PAY_GROUP' ></select>
			</div>
		</div>

		<div class='detail_row detail_row_bb'>
			<label class='detail_label w100'>비 고</label>
			<div class='detail_input_bg w320'>
				<input id='REMARK' type='text' ><!-- 632px -->
			</div>

			<label class='detail_label w80'>거래유형</label>
			<div class='detail_input_bg'>
				<select id='TRADE_TYPE' ></select>
			</div>
		</div>
	</div><!-- #freeform -->

	<div id='freeform3' class='detail_box ver2 has_title mt5'>
		<label class='sub_title'>세금계산서 정보</label>

		<div class='detail_row'>
			<label class='detail_label w100'>담당부서</label>
			<div class='detail_input_bg w220'>
				<input id='TAXEMP_DEPT_NAME' 		type='text' >
			</div>

			<label class='detail_label w80'>담당자</label>
			<div class='detail_input_bg'>
				<input id='TAXEMP_NAME' type='text'  >
			</div>

		</div>

		<div class='detail_row detail_row_bb'>
			<label class='detail_label w100'>이메일</label>
			<div class='detail_input_bg w220'>
				<input id='TAXEMP_EMAIL' type='text' >
			</div>

			<label class='detail_label w80'>전화</label>
			<div class='detail_input_bg'>
				<input id='TAXEMP_TEL' type='text' >
			</div>

		</div>

	</div><!-- #freeform3 -->

</div><!-- #ff_left -->



<div id='ff_right' class='grid_div ml10'>
	<div id='freeform2' class='detail_box ver2 has_title'>
		<label class='sub_title'>세부정보</label>

		<div class='detail_row'>
			<label class='detail_label w80'>담당자</label>
			<div class='detail_input_bg w150'>
				<input id='CHARGE_NAME' type='text' >
			</div>

			<label class='detail_label w80'>전화번호</label>
			<div class='detail_input_bg'>
				<input id='TEL' 		type='text' >
			</div>

		</div>

		<div class='detail_row'>
			<label class='detail_label w80'>휴대폰</label>
			<div class='detail_input_bg w150'>
				<input id='TEL2' type='text'  >
			</div>

			<label class='detail_label w80'>팩스</label>
			<div class='detail_input_bg'>
				<input id='FAX' type='text' >
			</div>

		</div>

		<div class='detail_row'>
			<label class='detail_label w80' style='border-bottom: 0px;'>주소</label>
<!-- 			<div class='detail_input_bg has_button w130'>
				<input id='ZIP' type='text' >
				<button id='btn_addrfind' class="search_find_img" />
			</div> -->

			<div class='detail_input_bg'>
				<input id='ADDR' type='text' >
			</div>
		</div>

		<div class='detail_row'>
			<!-- <label class='detail_label w80'>상세주소</label>
			<div class='detail_input_bg w240'>
				<input id='ADDR2' type='text' >
			</div> -->

			<label class='detail_label w80'>승인일</label>
			<div class='detail_input_bg w150'>
				<input id='BUSINESS_CHK_DATE' type='text' readonly disabled />
			</div>

			<label class='detail_label w80'>폐업일</label>
			<div class='detail_input_bg'>
				<input id='CLOSING_DATE' type='text' class='detail_date'>
			</div>

			

		</div>

		<div class='detail_row detail_row_bb'>
			<label class='detail_label w80'>등록자</label>
			<div class='detail_input_bg w150'>
				<input id='EMP_NO' type='text' readonly disabled />
			</div>

			<label class='detail_label w80'>등록일</label>
			<div class='detail_input_bg'>
				<input id='INPUT_DATE' type='text' readonly disabled/>
			</div>

		</div>

	</div><!-- #freeform2 -->


	<div id='freeform4' class='detail_box ver2 has_title mt5'>
		<label class='sub_title'>계좌정보</label>
			※ 계좌정보 등록은 재경팀에 문의 바랍니다. 
		<!-- <div class='detail_row'>
			<label class='detail_label w80'>은행</label>
			<div class='detail_input_bg w210 has_button'>
				<input id='BANK_NAME' type='text' >
				<button id='btn_bankcode' class="search_find_img" />
			</div>
			<div class='detail_input_bg noborder'>
				<input id='BANK_CODE' type='text' >
			</div>

		</div>

		<div class='detail_row'>
			<label class='detail_label w80'>예금주</label>
			<div class='detail_input_bg w130'>
				<input id='DEPOSIT_OWNER' type='text' >
			</div>

			<label class='detail_label w80'>계좌번호</label>
			<div class='detail_input_bg'>
				<input id='DEPOSIT_NO' type='text' >
			</div>

		</div>

		<div class='detail_row detail_row_bb'>
			<label class='detail_label w120'>계좌유효일자</label>
			<div class='detail_input_bg w100'>
				<input id='DEPOSIT_FROMDATE' type='text' class='detail_date'>
			</div>

			<div class='detail_input_bg w20 text_center noborder'>~</div>

			<div class='detail_input_bg w100 noborder'>
			  <input id='DEPOSIT_TODATE' type='text' class='detail_date'>
			</div>

			<div class='row-closing'></div>
		</div> -->


	</div><!-- #freeform4 -->

</div><!-- #ff_right -->

<div id='ff_btn' class='grid_div tac'>
	<button class="btn btn-warning" onclick="x_DAO_Save(dg_1)"> 저장 및 선택 </button>
	<button class="btn btn-default" onclick="pf_new('N')"> 취소 </button>
</div>

<div id='grid_1' class='grid_div'>
	<div id="dg_1" class="slick-grid"></div>
</div>


<script type="text/javascript" charset="UTF-8">
	$("#ff_left,#ff_right,#freeform,#freeform2,#freeform3,#freeform4,#ff_btn").hide();
	div_criteria.ResizeInfo = {init_width:860, init_height:40, width_increase:1, height_increase:0};
	wrap_btn.ResizeInfo     = {init_width:120, init_height:40, width_increase:0, height_increase:0};

	ff_left.ResizeInfo      = {init_width:600,  init_height:285, width_increase:0, height_increase:0};
		freeform.ResizeInfo   = {init_width:600,  init_height:60, width_increase:0, height_increase:0};
		freeform5.ResizeInfo  = {init_width:600,  init_height:110, width_increase:0, height_increase:0};
		freeform3.ResizeInfo  = {init_width:600,  init_height:110, width_increase:0, height_increase:0};
	ff_right.ResizeInfo     = {init_width:385,  init_height:285, width_increase:1, height_increase:0};
		freeform2.ResizeInfo  = {init_width:385,  init_height:174, width_increase:1, height_increase:0};
		freeform4.ResizeInfo  = {init_width:385,  init_height:110, width_increase:1, height_increase:0};
	ff_btn.ResizeInfo       = {init_width:1000, init_height:30,  width_increase:1, height_increase:0};
	grid_1.ResizeInfo       = {init_width:1000, init_height:659, width_increase:1, height_increase:1};

	FIND_CODE.ResizeInfo = {init_width:810,  init_height:22, anchor:{x1:1,y1:0,x2:1,y2:0}};
	$("#btn_search").bind("click", function(){x_DAO_Retrieve();});
	$("#FIND_CODE").bind("keydown", function(evt){xe_InputKeyDown( FIND_CODE, evt, evt.which);});
	$("#btn_new_cust").bind("click", function(){pf_new("Y");});

</script>








<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>

<div id='div_criteria' style='float:left;margin-bottom:5px;line-height:30px;'>
	<input id='FIND_CODE' type='text'> <img id='btn_search' src='${pageContext.request.contextPath}/images/btn/search.gif' width="60px" height="19px"/>
</div>	

<div id='grid_1' style='float:left;'></div>

<div id="btn_space" class="grid_row_space" style="height:7px" style="display:none;"></div>
<div id="dialog_btns" class="ui-dialog-buttonpane ui-widget-content" style="display:none;">
	<div class="ui-dialog-buttonset" style="float:right;">
		<button id="btn_ok" 		class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" type="button"><span class="ui-button-text">선택</span></button>
		<button id="btn_cancel" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" type="button"><span class="ui-button-text">취소</span></button>
	</div>
</div>

<script type="text/javascript" charset="UTF-8">
	div_criteria.ResizeInfo = {init_width:1000, init_height:30, width_increase:1, height_increase:0};
	FIND_CODE.ResizeInfo = {init_width:920,  init_height:0, anchor:{x1:1,y1:0,x2:1,y2:0}};
	//FIND_CODE.ResizeInfo = {init_width:950,  init_height:22, anchor:{x1:1,y1:0,x2:1,y2:0}};
	
	//수정(2015.06.30 KYY)
	if(isIE) {
		grid_1.ResizeInfo = {init_width:1000, init_height:640, width_increase:1, height_increase:1};
		dialog_btns.ResizeInfo = {init_width:1000, init_height:40, width_increase:1, height_increase:1};
		
		$("#btn_ok").bind("click", function() { x_Confirm(); });
		$("#btn_cancel").bind("click", function() { window.close(); });
		$("#btn_space").show();
		$("#dialog_btns").show();
	} else {
		grid_1.ResizeInfo = {init_width:1000, init_height:680, width_increase:1, height_increase:1};
	}

	$("#btn_search").bind("click", function(){x_DAO_Retrieve();});
	
</script>


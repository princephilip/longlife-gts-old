<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='div_criteria' style='float:left;margin-bottom:5px;line-height:30px;'>
	<input id='FIND_CODE' type='text' class='search_input' value="검색 문자열을 입력해 주세요.">> <img id='btn_search' src='${pageContext.request.contextPath}/images/btn/search.gif' width="60px" height="19px"/>
</div>	

<div id='grid_1' class='grid_div mr5'>
	<div id="dg_1" class='slick-grid'></div>
</div>

<script type="text/javascript" charset="UTF-8">
	div_criteria.ResizeInfo = {init_width:1000, init_height:30, anchor:{x1:1,y1:1,x2:1,y2:0}};
	grid_1.ResizeInfo = {init_width:990, init_height:655, anchor:{x1:1,y1:1,x2:1,y2:1}};
	
	FIND_CODE.ResizeInfo = {init_width:920,  init_height:0, anchor:{x1:1,y1:0,x2:1,y2:0}};
		
	$("#btn_search").bind("click", function(){x_DAO_Retrieve()});
</script>


<%@ page language="java" contentType="text/html; charset=utf-8" %>
<%@ page import="java.io.*,
                 java.io.File,
                 java.io.FileInputStream,
                 java.io.FileOutputStream,
                 java.io.IOException,
                 java.io.InputStream,
                 java.io.OutputStream,
                 java.util.*,
                 java.text.*,
                 com.clipsoft.rextoolkit.*,
                 com.clipsoft.rextoolkit.enumtype.*,
                 com.clipsoft.rextoolkit.oof.*, 
                 com.clipsoft.rextoolkit.oof.enumtype.*,
                 com.clipsoft.rextoolkit.parameter.*,
                 com.clipsoft.rextoolkit.parameter.enumtype.* " %>
                 
<%

  String rebURL = request.getParameter("rebURL");
	String saveFileName = request.getParameter("fileName");
  String pdfgbn = request.getParameter("pdfgbn");
  
  String asValue1 = request.getParameter("asValue1");
  String asValue2 = request.getParameter("asValue2");
  String asValue3 = request.getParameter("asValue3");
  String asValue4 = request.getParameter("asValue4");
  String asValue5 = request.getParameter("asValue5");
  String asValue6 = request.getParameter("asValue6");
  String asValue7 = request.getParameter("asValue7");
  String asValue8 = request.getParameter("asValue8");
  String asValue9 = request.getParameter("asValue9");
  
  
  String httpURL;
  String rexEspUrl;
  File root;
 
	if(pdfgbn.equals("1")) {
		httpURL = "http://epms.xinternet.co.kr/RexServer30/rexservice.jsp";
	  rexEspUrl = "http://localhost:90/RexESP/RexSOPServer";
	  root = new File("D:\\uploadfiles\\pmis_file\\pdf\\");	
	}else if(pdfgbn.equals("2")) {
		httpURL = "https://epmsdev.hec.co.kr/RexServer30/rexservice.jsp"; 
		rexEspUrl = "http://10.176.226.39:8090/RexESP/RexSOPServer";	
		root = new File("\\\\10.176.224.197\\pmis_file\\pdf\\");
	} else {
		httpURL = "https://epms.hec.co.kr/RexServer30/rexservice.jsp"; 
		rexEspUrl = "http://101.1.33.231:8090/RexESP/RexSOPServer";
		root = new File("\\\\101.1.33.130\\pmis_file\\pdf\\");
	}
	
	//String rexEspUrl = "http://localhost:8090/RexESP/RexSOPServer";	
	
	File newfolder = new File(root, saveFileName);
	newfolder.mkdir();
	
	String exportFilePath = "D:/uploadfiles/";
	String exportFileName = saveFileName;
	
%><%

	RexSOPToolkit rexSOPToolkit = new RexSOPToolkit();
	
	Oof oof = new Oof();
	oof.setTitle("보고서");
	oof.setEnableLog(true);
	oof.setEnableThread(true);
	
	//데이터 타입 기본 CSV
	CsvContent csvContent = (CsvContent) ContentFactory.createContent(ContentType.CSV);
	csvContent.setRowDelim("|#|");
	csvContent.setColDelim("|*|");
	csvContent.setDataSetDelim("|@|");
	csvContent.setEncoding(EncodingType.UTF_8);
	
	
	//커넥션 설정,(기본 OOF 방식의 커넥션 사용)	
	HttpConnection httpConnection = (HttpConnection) ConnectionFactory.createConnection(ConnectionType.HTTP);
	httpConnection.setPath(httpURL);
	httpConnection.setMethod("post");
	httpConnection.setNamespace("*");
	httpConnection.addHttpParam(new HttpParam("Q1SQL",OofSpecialField.AUTO.getValue()));
	httpConnection.addHttpParam(new HttpParam("OE","None"));
	httpConnection.addHttpParam(new HttpParam("CN","sql1")); //수정 DataConnection.properties 의 Connection1.Name=oracle1
	httpConnection.addHttpParam(new HttpParam("ID","SDCSV"));
	httpConnection.addHttpParam(new HttpParam("PE", "TRUE"));
	httpConnection.addHttpParam(new HttpParam("QC","1"));
	httpConnection.addHttpParam(new HttpParam("OT", "DataOnly"));
	httpConnection.addHttpParam(new HttpParam("Q1Type", "SQL"));	
	httpConnection.addHttpParam(new HttpParam("REBNM", "http://localhost:90/RexServer30/rebfiles/Rexpert1.reb"));
	httpConnection.addContent(csvContent);
	
	
	CryptoClipsoftPlugIn cryptoClipsoftPlugIn = new CryptoClipsoftPlugIn();
	cryptoClipsoftPlugIn.setName("name");
	cryptoClipsoftPlugIn.setCommonEnableEncode(true);
	cryptoClipsoftPlugIn.setCommonEnableDecode(false);
	cryptoClipsoftPlugIn.setCommonDelimiter("|!|");
	cryptoClipsoftPlugIn.setCommonEncoding("euc-kr");
	cryptoClipsoftPlugIn.setCommonEnableLog(false);
	cryptoClipsoftPlugIn.setCommonLogFileName("D:/Report/crypto/logs/test2.log");
	oof.addPlugIn(cryptoClipsoftPlugIn);
	
	
	
	com.clipsoft.rextoolkit.oof.File file = new com.clipsoft.rextoolkit.oof.File();
	file.setType(FileType.REB);
	file.setPath(rebURL);
	file.setShowParameterDialog(true);
	
	//매개변수
	oof.addFile(file);
	oof.addConnection(httpConnection);
	
	oof.addField(new Field("asValue1", asValue1)); 
	oof.addField(new Field("asValue2", asValue2)); 
	oof.addField(new Field("asValue3", asValue3)); 
	oof.addField(new Field("asValue4", asValue4)); 
	oof.addField(new Field("asValue5", asValue5)); 
	oof.addField(new Field("asValue6", asValue6)); 
	oof.addField(new Field("asValue7", asValue7)); 
	oof.addField(new Field("asValue8", asValue8)); 
	oof.addField(new Field("asValue9", asValue9)); 
	
	//ESP 변환서버 호출
	rexSOPToolkit.setRexEspUrl(rexEspUrl);
	rexSOPToolkit.setSubmitEncodingType(SubmitEncodingType.UTF_8);
	
	//PDF 생성
	PdfExportParameter pdfExportParameter = ParameterFactory.createPdfExportParameter();
	pdfExportParameter.setOof(oof.createOof());
	pdfExportParameter.setOofProcessType(OofProcessType.PATH);
	pdfExportParameter.setFilePath(exportFilePath);
	pdfExportParameter.setFileName(exportFileName+ ".pdf");
	
	RexToolkitResult result = rexSOPToolkit.export(pdfExportParameter);

  //web페이지에서 pdf 뷰잉 할경우
	
	HashMap info = result.getInfoMap();
	
	InputStream is = rexSOPToolkit.fileStream(
		(String)info.get("filePath"), 
		(String)info.get("fileName"), 
		ExportType.PDF.getValue(), 
		false
	);
	
	RexToolkitUtility rexToolkitUtility = new RexToolkitUtility();
	
	rexToolkitUtility.setContentType(ExportType.PDF);
	rexToolkitUtility.responseOutputStream(response, is);	
	
	
%>
<%

	File af;
  File bf;
  
  if(pdfgbn.equals("1")) {
		af = new File("D:\\uploadfiles\\"+saveFileName+".pdf");
		bf = new File("D:\\uploadfiles\\pmis_file\\pdf\\"+saveFileName+"\\"+saveFileName+".pdf");
	}else if(pdfgbn.equals("2")) {
		af = new File("D:\\uploadfiles\\"+saveFileName+".pdf");
		bf = new File("\\\\10.176.224.197\\pmis_file\\pdf\\"+saveFileName+"\\"+saveFileName+".pdf");
	} else {
		af = new File("D:\\uploadfiles\\"+saveFileName+".pdf");
		bf = new File("\\\\101.1.33.130\\pmis_file\\pdf\\"+saveFileName+"\\"+saveFileName+".pdf");
	}

	FileInputStream fis = new FileInputStream(af);
  FileOutputStream fos = new FileOutputStream(bf);
  
  int data = 0;
  while((data=fis.read())!=-1) {
   fos.write(data);
  }

  fis.close();
  fos.close();
  af.delete();
%>    
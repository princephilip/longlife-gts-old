﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>

<div id='div_criteria' style='float:left;margin-bottom:5px;line-height:23px;'>
	<input id='FIND_CODE' class='FIND_CODE' type='text'><button id="btn_search" class="btn_search" aria-label="검색" name="" ><span class="gb_bg gb_mag"></span></button>
</div>

<div id="grid_1" style='float:left;width:100%;height:655px;'>
	<div id="dg_1" style='float:left;width:100%;height:100%;' class='slick-grid'>
	</div>
</div>

<script type='text/javascript' charset='UTF-8'>
	div_criteria.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};
	grid_1.ResizeInfo = {init_width:1000, init_height:685, width_increase:1, height_increase:1};

	FIND_CODE.ResizeInfo = {init_width:950,  init_height:22, anchor:{x1:1,y1:0,x2:1,y2:0}};
	$("#btn_search").bind("click", function(){x_DAO_Retrieve();});
	
</script>

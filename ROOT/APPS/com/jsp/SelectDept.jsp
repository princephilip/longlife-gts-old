<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box'>

	<div class="option_label">검색</div>
	<div class='option_input_bg w160'>
		<input id='S_FIND' type='text' class='option_input_fix w150'>
	</div>

	<div id='xbtns' style='height:23px;'>
		<span style='width:100%;height:100%;'>
			<html:button name='S_CONFIRM' id='S_CONFIRM' type='button' icon='ui-icon-check' desc="일괄등록" onClick="x_Confirm();"></html:button>
			<html:button name='S_RETRIEVE' id='S_RETRIEVE' type='button' icon='ui-icon-refresh' desc="조회" onClick="x_DAO_Retrieve2();"></html:button>
			<html:button name='S_CLOSE' id='S_CLOSE' type='button' icon='ui-icon-circle-close' desc="닫기" onClick="x_Close();"></html:button>
		</span>
	</div>

</div>

<div id='grid_1' class='grid_div'>
	<div id="dg_1" class='slick-grid'></div>
</div>


<script type="text/javascript">
	topsearch.ResizeInfo 	= {init_width:1000, init_height: 33, width_increase:1, height_increase:0};
	grid_1.ResizeInfo     = {init_width:1000, init_height:660, width_increase:1, height_increase:1};

</script>

<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box'>
	<div class='option_label' >단가적용 년월</div>
		<div class="option_input_bg" >
	    	<html:basicdate id="dateimages" classType="search" dateType="month" moveBtn="true" readOnly="true"></html:basicdate>
	  </div>
</div>

<div id='grid_1' class='grid_div'>
	<div id="dg_1"class='slick-grid'></div>
</div>

<div id="btn_space" class="grid_row_space" style="height:7px" style="display:none;"></div>
<div id="dialog_btns" class="ui-dialog-buttonpane ui-widget-content" style="display:none;">
	<div class="ui-dialog-buttonset" style="float:right;">
		<button id="btn_ok" 	class="btn btn-success btn-sm" >선택</button>
		<button id="btn_cancel" class="btn btn-default btn-sm" >취소</button>
	</div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo 	= {init_width: 1000, init_height:33 , width_increase:1, height_increase:0};
	grid_1.ResizeInfo    = {init_width:1000, init_height:665, anchor:{x1:1,y1:1,x2:1,y2:1}};
</script>
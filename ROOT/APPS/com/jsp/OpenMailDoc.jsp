<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ page import="java.io.*" %>  
<%@ page import="java.util.*" %>   
<%@ page import="java.sql.Connection" %> 
<%@ page import="java.sql.DriverManager" %>  
<%@ page import="net.sf.jasperreports.engine.JasperExportManager" %> 
<%@ page import="net.sf.jasperreports.engine.JasperFillManager" %> 
<%@ page import="net.sf.jasperreports.engine.JasperPrint" %> 
<%@ page import="net.sf.jasperreports.engine.JasperReport" %> 
<%@ page import="net.sf.jasperreports.engine.JasperReportsContext" %> 
<%@ page import="net.sf.jasperreports.engine.SimpleJasperReportsContext" %> 
<%@ page import="net.sf.jasperreports.engine.util.JRLoader" %>
<%@ page import="java.net.URLDecoder" %> 
<%@ page import="java.sql.ResultSetMetaData"%>

<%
String token = request.getParameter("token");
String pwd = request.getParameter("pwd");
String orderId = ""; 
String custId = ""; 
String calcYYmm = ""; 
String mailPwd = "";
String maildiv = "";
String repName = "";
String repDiv ="";

try {
	String[] sqlData = xgov.core.dao.XDAO.XmlSelect(request, "array", "com", "OpenMailDoc", "GetDocNo", token, "all", "˛", "¸").split("˛");
	String[] orderData;

	if(sqlData.length==1) {
		orderData = sqlData[0].split("¸");
		orderId = orderData[0];
		mailPwd = orderData[1]; 
		custId = orderData[2];
		calcYYmm = orderData[3];
		maildiv = orderData[4];
		repName = orderData[5];
		repDiv = orderData[6];                
	}
} 
catch (Exception ex) {
  throw ex;
}
finally {
  //if (xsql != null) {xsql.close();}  
  //xsql = null;
}



if(mailPwd.equals("")|| (!mailPwd.equals("")&&mailPwd.equals(pwd))) {
	// 문서 출력

	String system = "sm";
	String rptURL = "/APPS/pm/report/"+ repName+".jasper" ;  

	String requrl = new String(request.getRequestURL());
	String vMsg = "-";

	File rptFile = new File(application.getRealPath(rptURL));
	Map<String, Object> rptParams = new HashMap<String, Object>();
	

	Enumeration enums = request.getParameterNames(); 
	%>
	<script language="javascript">

 console.log(<%=custId%>);
</script>
<%
	if(repName.equals("월매출정산서") ){
		rptParams.put("as_cust_id", custId); 
		rptParams.put("as_yymm", calcYYmm);
	}
	else{
		rptParams.put("as_orderid", orderId); 
		rptParams.put("as_apprid", 0);
		rptParams.put("as_reportdiv", repDiv);
	}
	 
	if(rptFile.exists() == false){ 
		rptParams.put("pMsg", "["+rptURL+" : arguments"+vMsg+"]Report File Not Found"); 
		rptURL = "/APPS/sm/report/MsgReport.jasper";
		rptFile = new File(application.getRealPath(rptURL)); 
	}


	Connection conn = null;
	OutputStream os = null;

	try { 
		conn = xgov.core.jndi.XConnJndi.getConnection(system);
		JasperReportsContext jasperReportsContext = new SimpleJasperReportsContext();
		JasperReport jasperReport = (JasperReport)JRLoader.loadObject(jasperReportsContext,rptFile); 
		JasperFillManager fillManager = JasperFillManager.getInstance(jasperReportsContext);
		JasperPrint jasperPrint = fillManager.fill(jasperReport, rptParams, conn);
		JasperExportManager exportManager = JasperExportManager.getInstance(jasperReportsContext);
		
		out.clear();
		out=pageContext.pushBody();
		
		response.setHeader("Cache-Control","no-store");
		response.setHeader("Pragma","no-cache");
		response.setDateHeader("Expires",0);
		
		if (request.getProtocol().equals("HTTP/1.1")){
			response.setHeader("Cache-Control", "no-cache");
		}
		
		response.setHeader("Content-Disposition","inline");
		response.setContentType("application/pdf");
		os = response.getOutputStream();
		exportManager.exportToPdfStream(jasperPrint, os);
		os.flush();

	}finally{

		if( os != null) try{ os.close(); }catch(Exception x){}
		if( conn != null) try{ conn.close(); }catch(Exception x){}

	}

} else {
	// 비밀번호 묻기
%>




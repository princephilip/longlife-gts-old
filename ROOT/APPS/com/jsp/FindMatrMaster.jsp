<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2' >
	<div id='xbtns' >
		<span style='vertical-align:middle;'>
			<button type="button" id='S_RETRIEVE' class="btn btn-success btn-xs" onClick="x_DAO_Retrieve();">조회</button>
			<button type="button" id='S_CLOSE' class="btn btn-warning btn-xs" onClick="x_Close();">닫기</button>
<!-- 			<html:button name='S_RETRIEVE' 	id='S_RETRIEVE' type='button' icon='ui-icon-refresh'      desc="조회" onClick="x_DAO_Retrieve();"></html:button>
			<html:button name='S_CLOSE' 	id='S_CLOSE' 	type='button' icon='ui-icon-circle-close' desc="닫기" onClick="x_Close();"></html:button>
 -->		</span>
	</div>

	<div>
		<div class='option_label' >자재구분</div>
		<div class="option_input_bg" >
			<select id="S_LARGE" style="width:90px"></select>
			<select id="S_MIDDLE" style="width:110px"></select>
			<select id="S_SMALL" style="width:120px"></select>
		</div>
		<div class='option_label'>검색</div>
		<div class='option_input_bg w200'>
			<input id='S_SEARCH' type='text' class='option_input_fix' style="width:190px">
		</div>
	</div>
</div>


<!-- <div id='topsearch' class='option_box2' >
	<div id='xbtns' >
		<span style='vertical-align:middle;'>
			<html:button name='S_RETRIEVE' 	id='S_RETRIEVE' type='button' icon='ui-icon-refresh' 			desc="조회" onClick="x_DAO_Retrieve2();"></html:button>
			<html:button name='S_CONFIRM' 	id='S_CONFIRM' 	type='button' icon='ui-icon-check' 				desc="확인" onClick="x_Confirm();"></html:button>
			<html:button name='S_CLOSE' 	id='S_CLOSE' 	type='button' icon='ui-icon-circle-close' desc="닫기" onClick="x_Close();"></html:button>
		</span>
	</div>
	
	<div class='option_label' >자재명(입력시 조회됩니다.)</div>
	<div class="option_input_bg" >
		<input id="S_SEARCH" style="width:100px">
	</div>
</div> -->


<div id='grid_1' class='grid_div' >
	<div id="dg_1" style="width:100%; height:100%;" class="slick-grid"></div>
</div>


<script type="text/javascript">
	topsearch.ResizeInfo 	= {init_width: 1000, init_height:33 , width_increase:1, height_increase:0};
	grid_1.ResizeInfo 		= {init_width: 1000, init_height:665, width_increase:1, height_increase:1};	
</script>
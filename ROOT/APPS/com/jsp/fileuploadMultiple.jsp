<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Foundation Layer[Util Service]</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<base target="_self" />
<style type="text/css" media="screen"></style>
<script type="text/javascript" language="javascript" src="/APPS/com/js/multifile.js"></script>

<script type="text/javaScript" language="javascript">

function fn_cancel(){
	window.returnValue = null;
	window.close();
}

</script>

</head>
<body id="about">
	<div id="content" style="width:420px;height:260px;background:url('${pageContext.request.contextPath}/images/excel/pop_bg.jpg') no-repeat;">
		<form name="Form" id="Form" method="post" action="<c:url value='/upload/genericMulti.do'/>" enctype="multipart/form-data">
			<div id="landscape">
  			<div id="container" style="width:420px;height:100px; padding-left:25px;padding-top:30px;" >
    			<div id="content" style="width:420px;height:100px;">
      			<div id="contenttext" style="width:420px;height:100px;">
	       			<input name="type" type="hidden" value="genericFileMulti" size="100" />
        			<div style="width:420px; height:40px; vertical-align:bottom;">
	            	<input name="file_1" type="file" id="my_file_element" size="15"/>
        			</div>
        		<div> <img src="<c:url value='${pageContext.request.contextPath}/images/excel/pop_icon.png' />" alt=""  width="4" height="6">&nbsp;<span style="font-size: 12px;font-weight:bold;color:#326004;">파일업로드 리스트</span>
	        		<div id="files_list"></div>
	       			 <script>
								var up_cnt = window.dialogArguments["upcnt"];
								var multi_selector = new MultiSelector( document.getElementById('files_list'), up_cnt);
								multi_selector.addElement( document.getElementById( 'my_file_element' ) );
	        		</script>
        		 </div>
      			</div>
    			</div>
  			</div>
				<div style="padding-top:100px;text-align: center;">
					<input type="image" src="/Theme/images/excel/btn_upload1.png">
					<input type="image" src="/Theme/images/excel/btn_cancel1.png" onclick="fn_cancel(); return false;">
				</div>
			</div>
	</form>
</body>
</html>
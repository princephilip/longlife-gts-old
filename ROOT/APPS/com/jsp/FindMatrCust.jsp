<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box'>

	<div class='option_label' >자재구분</div>
	<div class="option_input_bg" >
		<select id="S_LARGE_CODE" style="width:100px"></select>
		<select id="S_MIDDLE_CODE" style="width:120px"></select>
		<select id="S_SMALL_CODE" style="width:140px"></select>
	</div>
	<div class='option_line'></div>
	<div class="option_label">검색</div>
		<div class='option_input_bg w160'>
			<input id='S_SEARCH_NM' type='text' class='option_input_fix w150'>
	</div>

	<div class="option_label">규격/단위</div>
		<div class='option_input_bg w160'>
			<input id='S_MATR_NM' type='text' class='option_input_fix w150'>
	</div>

	<div style="float: right; height: 20px; color: #4374D9; font-weight: bold; color:red; line-height: 23px;">
		<img src='${pageContext.request.contextPath}/Theme/images/btn/tit_icon_pop2.gif'>&nbsp; 더블클릭으로 자재선택목록에 추가
	</div>

	<div class='option_line'></div>
	<div id='xbtns' style='height:23px; margin-top: -50px;'>
		<span style='width:100%;height:100%;vertical-align:middle;'>
			<html:button name='S_RETRIEVE' id='S_RETRIEVE' type='button' icon='ui-icon-refresh' desc="조회" onClick="x_DAO_Retrieve2();"></html:button>
			<html:button name='S_CONFIRM' id='S_CONFIRM' type='button' icon='ui-icon-check' desc="확인" onClick="x_Confirm();"></html:button>
			<html:button name='S_CLOSE' id='S_CLOSE' type='button' icon='ui-icon-circle-close' desc="닫기" onClick="x_Close();"></html:button>
		</span>
	</div>

</div>

<div id='grid_1' class='grid_div'>
	<div id="dg_1" style='width:100%;height:100%;' class='slick-grid'></div>
</div>
<div style="float: left; height: 20px; color: #4374D9; font-weight: bold; line-height: 23px;">
		<img src='${pageContext.request.contextPath}/Theme/images/btn/tit_icon_pop.gif'>&nbsp; 자재선택목록
	</div>
<div style="float: right; height: 20px; color: #4374D9; font-weight: bold; color:red; line-height: 23px;">
		<img src='${pageContext.request.contextPath}/Theme/images/btn/tit_icon_pop2.gif'>&nbsp; 더블클릭으로 자재선택목록에서 삭제
	</div>
<div id='grid_2' class='grid_div mt5'>
	<div id="dg_2" style='width:100%;height:100%;' class='slick-grid'></div>
</div>


<script type="text/javascript">
	topsearch.ResizeInfo 	= {init_width: 1000, init_height:58 , width_increase:1, height_increase:0};
	grid_1.ResizeInfo    = {init_width:1000, init_height:269, anchor:{x1:1,y1:1,x2:1,y2:0.5}};
	grid_2.ResizeInfo    = {init_width:1000, init_height:345, anchor:{x1:1,y1:1,x2:1,y2:0.5}};

</script>
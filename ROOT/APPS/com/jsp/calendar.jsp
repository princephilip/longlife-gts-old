<%@ page contentType="text/html;charset=euc-kr" %>

<html>
<head>
<title>날짜 선택</title>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<LINK href="/Mighty/css/mighty-1.0.0.css" rel="stylesheet" type="text/css">




<%
	String year = request.getParameter("year");;
	String month = request.getParameter("month");
	String day = request.getParameter("day");

	java.util.Calendar cal = java.util.Calendar.getInstance(java.util.Locale.KOREA);
	
%>

<SCRIPT LANGUAGE="JAVASCRIPT">
	var	clientwin = window.dialogArguments;


	function Datetime(y, m, d)
	{
		this.year = y;
		this.month = m;
		this.date = d;

		this.compare = function ( dt )
		{
			return parseInt(this.toString()) - parseInt(dt.toString());
		};

		this.toString = function()
		{
			return ( this.year + "" + this.month + ""  + this.date );
		};

		this.validateLength = function()
		{
			this.year = ("0000" + this.year).substring( (new String(this.year)).length );
			this.month = ("00" + this.month).substring( (new String(this.month)).length );
			this.date = ("00" + this.date).substring( (new String(this.date)).length );
		};

		this.clone = function ()
		{
			return new Datetime(this.year, this.month, this.date);
		};
	}

	var leap = new Array();
	leap[0] = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	leap[1] = new Array(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

	var cur = new Datetime(<%=year%>, <%=month%>, <%=day%>);
	var offset = 0;

	window.onload = function ()
	{
		drawCalendar( cur );
		setYearForm( cur );
		setMonthForm( cur );
		setDateForm( cur );
		drawCalendar( cur );
	}

	function isLeap( year )
	{
		return (( year % 4 == 0 ) && ( year % 100  != 0 ) || ( year % 400 == 0 )) ? 1 : 0;
	}

	function drawCalendar( obj )
	{
		var d = new Date( obj.year, obj.month - 1, 1 );
		offset = d.getDay();
		var spn, td, td2;

		for ( var i = 1; i <= 42; i++) {
			spn = document.getElementById("d" + i);
			spn.innerText = " ";
		}

		var maxMonth = leap[isLeap(obj.year)][obj.month - 1];
		obj.date = Math.min(obj.date, maxMonth);
		for ( var i = 0; i < maxMonth; i++ ) {
			spn = document.getElementById("d" + (i + 1 + offset));
			spn.innerText = (i + 1);
			td = document.getElementById("c" + (i + 1 + offset));
			td.onclick = selectDate;
		}
	}

	function selectYear( offset )
	{
		cur.year = parseInt(cur.year) + offset;
		setYearForm( cur );
		resetDateForm( cur );
		drawCalendar( cur );
		setDateForm( cur );
	}

	function setYearForm( obj )
	{
		DTForm["year"].value = obj.year;
	}

	function setMonthForm( obj )
	{
		DTForm["month"].selectedIndex = obj.month - 1;
	}

	function setDateForm( obj )
	{
		var dateIndex = parseInt(obj.date) +  parseInt(offset);
		var spn = document.getElementById("d" + dateIndex);
		spn.style.backgroundColor = "#0000FF";
		oldspncolor = spn.style.color;
		spn.style.color = "#FFFFFF";
	}

	function resetDateForm( obj )
	{
		var dateIndex = parseInt(obj.date) + parseInt(offset);
		var spn = document.getElementById("d" + dateIndex);
		spn.style.backgroundColor = "#FFFFFF";
		spn.style.color = oldspncolor;
	}

	function selectYear( obj, offset )
	{
		//cur.year = parseInt(cur.year) + offset;
		cur.year = parseInt(obj.value) + parseInt(offset);
		setYearForm( cur );
		resetDateForm( cur );
		drawCalendar( cur );
		setDateForm( cur );
	}

	function selectMonth()
	{
		cur.month = DTForm["month"].value;
		setMonthForm( cur );
		resetDateForm( cur );
		drawCalendar( cur );
		setDateForm( cur );
	}

	function selectDate()
	{
		var selVal = event.srcElement.id.substring(1);
		if ( cur.date != (selVal - offset) ) {
			resetDateForm( cur );
			cur.date = parseInt(selVal) - offset;
			setDateForm( cur );
		}
	}

	function selected() {
		cur.validateLength();
		returnValue = cur;
		if (clientwin) clientwin.ii_rtvalue = cur.toString();
		window.close();
	}

	function SetYearOption() {
		var optionHtml="";
		var curYear=cur.year;
		for(i=curYear-100;i<=curYear+100;i++){
			optionHtml += "<option value=" + i + (i==curYear?" selected":"") + ">" + i + "년&nbsp;</option>"
		}
		return optionHtml;
	}
</SCRIPT>

</HEAD>
<BODY topmargin="0" bgcolor="white" leftmargin="1" onmouseup="upButton=false;downButton=false;move=false;">
<form name="DTForm" id="DTForm">
<table name="DTTable" width="100%" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" align="center" border="0">
  <tr>
    <td height="25" bgcolor="#699DBA" class="title1" style="padding-left:10">
      <font color="#FFFFFF"><strong><font color="#FFFFFF"><img src="/Theme/images/calendar/po_title.gif" width="13" height="12"></font></strong>
      날짜 선택</font>
    </td>
  </tr>
  <tr><td height="6px"></td></tr>
	<tr>
		<td  bgcolor="white" colspan="2" align="center">
			
			<table border="0" width="223">
				<tr>
					<td>
						<table width="220" cellspacing="0" cellpadding="0"  bgcolor="white"  align="center" border="0">
							<tr>
								<td width="50" align=right bgcolor="#FFFFFF">
									<img src="/Theme/images/calendar/p_bu1.gif" onclick="selectYear( document.DTForm.year, '-1' )"	ondrag="return false" align=absmiddle>
								</td>
								<td width="40" align=right bgcolor="#FFFFFF">
				   					<select name="year" onchange="selectYear(this, '0')" style=width:70px class="input">
				   						<script language="javascript">document.write(SetYearOption());</script>
									</select>
								</td>
								<td width="25" align=left bgcolor="#FFFFFF">
									<img src="/Theme/images/calendar/p_bu2.gif" onclick="selectYear( document.DTForm.year, '1' )" ondrag="return false" align=absmiddle><br>
							  </td>
								<td width="100" bgcolor="#FFFFFF" align="center">
									<select name="month" onchange="selectMonth()" width="50" class="input">
										<option value="1">  1월&nbsp;&nbsp;</option>
										<option value="2">  2월&nbsp;&nbsp;</option>
										<option value="3">  3월&nbsp;&nbsp;</option>
										<option value="4">  4월&nbsp;&nbsp;</option>
										<option value="5">  5월&nbsp;&nbsp;</option>
										<option value="6">  6월&nbsp;&nbsp;</option>
										<option value="7">  7월&nbsp;&nbsp;</option>
										<option value="8">  8월&nbsp;&nbsp;</option>
										<option value="9">  9월&nbsp;&nbsp;</option>
										<option value="10">10월&nbsp;&nbsp;</option>
										<option value="11">11월&nbsp;&nbsp;</option>
										<option value="12">12월&nbsp;&nbsp;</option>
									</select>
							  </td>
							</tr>
							<tr><td colspan="4" height="6px"></td></tr>
						</table>

						<table width="225" cellspacing="0" cellpadding="0" bgcolor="#000000" align="center" border="1" bordercolordark="white" bordercolorlight="#CCCCCC"  style="font-family: Arial; font-size: 10pt">
							<tr  height="23px" bgcolor="#efeffa" align="center" style="font-family: 돋움; font-size: 10pt">
								<td><font color="#FF2222">일</font></td>
								<td>월</td>
								<td>화</td>
								<td>수</td>
								<td>목</td>
								<td>금</td>
								<td><font color="#2222FF">토</font></td>
							</tr>
							<tr  height="23px">
								<td id="c1" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><font color="#FF2222"><span ondblclick="selected()" id="d1" style="width:100%"></span></font></td>
								<td id="c2" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d2" style="width:100%"></span></td>
								<td id="c3" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d3" style="width:100%"></span></td>
								<td id="c4" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d4" style="width:100%"></span></td>
								<td id="c5" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d5" style="width:100%"></span></td>
								<td id="c6" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d6" style="width:100%"></span></td>
								<td id="c7" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><font color="#2222FF"><span ondblclick="selected()" id="d7" style="width:100%"></span></font></td>
							</tr>
							<tr height="23px">
								<td id="c8" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><font color="#FF2222"><span ondblclick="selected()" id="d8" style="width:100%"></span></font></td>
								<td id="c9" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d9" style="width:100%"></span></td>
								<td id="c10" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d10" style="width:100%"></span></td>
								<td id="c11" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d11" style="width:100%"></span></td>
								<td id="c12" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d12" style="width:100%"></span></td>
								<td id="c13" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d13" style="width:100%"></span></td>
								<td id="c14" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><font color="#2222FF"><span ondblclick="selected()" id="d14" style="width:100%"></span></font></td>
							</tr>
							<tr height="23px">
								<td id="c15" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><font color="#FF2222"><span ondblclick="selected()" id="d15" style="width:100%"></span></font></td>
								<td id="c16" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d16" style="width:100%"></span></td>
								<td id="c17" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d17" style="width:100%"></span></td>
								<td id="c18" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d18" style="width:100%"></span></td>
								<td id="c19" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d19" style="width:100%"></span></td>
								<td id="c20" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d20" style="width:100%"></span></td>
								<td id="c21" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><font color="#2222FF"><span ondblclick="selected()" id="d21" style="width:100%"></span></font></td>
							</tr>
							<tr height="23px">
								<td id="c22" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><font color="#FF2222"><span ondblclick="selected()" id="d22" style="width:100%"></span></font></td>
								<td id="c23" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d23" style="width:100%"></span></td>
								<td id="c24" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d24" style="width:100%"></span></td>
								<td id="c25" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d25" style="width:100%"></span></td>
								<td id="c26" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d26" style="width:100%"></span></td>
								<td id="c27" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d27" style="width:100%"></span></td>
								<td id="c28" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><font color="#2222FF"><span ondblclick="selected()" id="d28" style="width:100%"></span></font></td>
							</tr>
							<tr height="23px">
								<td id="c29" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><font color="#FF2222"><span ondblclick="selected()" id="d29" style="width:100%"></span></font></td>
								<td id="c30" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d30" style="width:100%"></span></td>
								<td id="c31" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d31" style="width:100%"></span></td>
								<td id="c32" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d32" style="width:100%"></span></td>
								<td id="c33" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d33" style="width:100%"></span></td>
								<td id="c34" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d34" style="width:100%"></span></td>
								<td id="c35" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><font color="#2222FF"><span ondblclick="selected()" id="d35" style="width:100%"></span></font></td>
							</tr>
							<tr height="23px">
								<td id="c36" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><font color="#FF2222"><span ondblclick="selected()" id="d36" style="width:100%"></span></font></td>
								<td id="c37" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d37" style="width:100%"></span></td>
								<td id="c38" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d38" style="width:100%"></span></td>
								<td id="c39" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d39" style="width:100%"></span></td>
								<td id="c40" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d40" style="width:100%"></span></td>
								<td id="c41" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><span ondblclick="selected()" id="d41" style="width:100%"></span></td>
								<td id="c42" bgcolor="#FFFFFF" align="center" style="padding-left:2px;padding-right:3px"><font color="#2222FF"><span ondblclick="selected()" id="d42" style="width:100%"></span></font></td>
							</tr>
						</table>		
					</td>
				</tr>
			</table>						
						
		</td>
	</tr>
</table>
<br />
<div align=center>
  <input type="button" value=" 확인 " class="selbox" onclick="selected()">
	<input type="button" value=" 취소 " class="selbox" onclick="window.close()">
</div>
</form>

</BODY>
</HTML>
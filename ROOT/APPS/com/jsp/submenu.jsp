<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!--서브메뉴 시작 -->
<div id='jqxMenu_SM' style="display:none; float:none; left:0; width:100%; height:24px; text-align:center; overflow:hidden; ">
	<ul>

		<li id="smenu_SM">기준정보관리
		    <ul style='width: 180px;'>
		       <li><a href="javascript:_X.OpenSheet('SM010010')">센터정보관리</a></li>
		       <li><a href="javascript:_X.OpenSheet('SM010020')">품목코드관리</a></li>
		       <li><a href="javascript:_X.OpenSheet('SM010030')">농수산표준코드</a></li>
		       <li><a href="javascript:_X.OpenSheet('SM010040')">공통코드</a></li>
		       <li><a href="javascript:_X.OpenSheet('SM010050')">사무실코드</a></li>
		       <li><a href="javascript:_X.OpenSheet('SM010060')">창고코드</a></li>
		       <li><a href="javascript:_X.OpenSheet('SM010070')">작업장코드</a></li>
		       <li><a href="javascript:_X.OpenSheet('SM010080')">회의실코드</a></li>
		       <li><a href="javascript:_X.OpenSheet('SM010090')">DOCK코드</a></li>
		       <li><a href="javascript:_X.OpenSheet('SM010100')">마감관리</a></li>
		   </ul>
		</li>
		<li id="smenu_SM">권한관리
		    <ul style='width: 180px;'>
		       <li><a href="javascript:_X.OpenSheet('SM020010')">사용자관리</a></li>
		       <li><a href="javascript:_X.OpenSheet('SM020020')">프로그램관리</a></li>
		       <li><a href="javascript:_X.OpenSheet('SM020030')">권한등급관리</a></li>
		   </ul>
		</li>
		
	</ul>
</div>

<div id='jqxMenu_TT' style="display:none; float:none; left:0; width:100%; height:24px; text-align:center; overflow:hidden; ">
	<ul>

		<li id="smenu_TT">입주업체정보관리
		    <ul style='width: 180px;'>
		       <li><a href="javascript:_X.OpenSheet('TT010010')">기본정보관리</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT010020')">채권정보관리</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT010030')">계약정보관리</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT010040')">품목매칭정보관리</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT010050')">운송회사정보관리</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT010060')">차량정보관리</a></li>
		   </ul>
		</li>
		<li id="smenu_TT">출하처정보관리
		    <ul style='width: 180px;'>
		       <li><a href="javascript:_X.OpenSheet('TT020010')">기본정보관리</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT020020')">차량정보관리</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT020030')">출하자정보관리</a></li>
		   </ul>
		</li>
		<li id="smenu_TT">송품장관리
		    <ul style='width: 180px;'>
		       <li><a href="javascript:_X.OpenSheet('TT030010')">발주서등록</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT030020')">채권정보조회</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT030030')">발주서조회</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT030040')">송품장등록</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT030050')">송품장승인</a></li>
		   </ul>
		</li>
		<li id="smenu_TT">거래정산관리
		    <ul style='width: 180px;'>
		       <li><a href="javascript:_X.OpenSheet('TT040010')">가격결정서등록</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT040020')">선대요청서등록</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT040030')">선대지불등록</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT040040')">선대지불현황</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT040050')">선대미정산현황</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT040060')">선대입금등록</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT04006010')">입금예정조회(팝업)</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT040070')">선대정산현황조회</a></li>
		   </ul>
		</li>
		<li id="smenu_TT">정산정보
		    <ul style='width: 180px;'>
		       <li><a href="javascript:_X.OpenSheet('TT050010')">출하처정산정보조회</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT050020')">입고업체정산정보조회</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT050030')">기간별거래정보조회</a></li>
		   </ul>
		</li>
		
		<li id="smenu_TT">거래정보관리
		    <ul style='width: 180px;'>
		       <li><a href="javascript:_X.OpenSheet('TT060010')">품목별가격</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT060020')">주요품목가격</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT060030')">일일등락품목</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT060040')">주간등락품목</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT060050')">월간등락품목</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT060060')">산지별가격정보</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT060070')">품목별가격등락조회</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT060080')">물량분포표</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT060090')">기간별거래물량추이</a></li>
		       <li><a href="javascript:_X.OpenSheet('TT060100')">노출품목관리</a></li>
		   </ul>
		</li>
	</ul>
</div>

<div id='jqxMenu_OM' style="display:none; float:none; left:0; width:100%; height:24px; text-align:center; overflow:hidden; ">
	<ul>

		<li id="smenu_OM">시설운영관리
		    <ul style='width: 180px;'>
		       <li><a href="javascript:_X.OpenSheet('OM010010')">시설현황관리</a></li>
		       <li><a href="javascript:_X.OpenSheet('OM010020')">시설임대등록</a></li>
		       <li><a href="javascript:_X.OpenSheet('OM010030')">시설임대현황</a></li>
		       <li><a href="javascript:_X.OpenSheet('OM010040')">창고별재고현황</a></li>
		   </ul>
		</li>
		<li id="smenu_OM">작업장예약
		    <ul style='width: 180px;'>
		       <li><a href="javascript:_X.OpenSheet('OM020010')">집배송장이용예약</a></li>
		       <li><a href="javascript:_X.OpenSheet('OM020020')">선별포장장이용예약</a></li>
		       <li><a href="javascript:_X.OpenSheet('OM020030')">작업장이용예약</a></li>
		       <li><a href="javascript:_X.OpenSheet('OM020040')">창고이용예약</a></li>
		       <li><a href="javascript:_X.OpenSheet('OM020050')">회의실이용예약</a></li>
		       <li><a href="javascript:_X.OpenSheet('OM020060')">예약조정승인</a></li>
		       <li><a href="javascript:_X.OpenSheet('OM020070')">시설이용정보입력</a></li>
		       <li><a href="javascript:_X.OpenSheet('OM020080')">입출고예약요청</a></li>
		   </ul>
		</li>
		<li id="smenu_OM">운송차량관리
		    <ul style='width: 180px;'>
		       <li><a href="javascript:_X.OpenSheet('OM030010')">입차차량예약</a></li>
		       <li><a href="javascript:_X.OpenSheet('OM030020')">입출차현황</a></li>
		       <li><a href="javascript:_X.OpenSheet('OM030030')">차량DOCK배정</a></li>
		   </ul>
		</li>
		<li id="smenu_OM">작업관리
		    <ul style='width: 180px;'>
		       <li><a href="javascript:_X.OpenSheet('OM040010')">작업지시등록</a></li>
		       <li><a href="javascript:_X.OpenSheet('OM040020')">작업연결등록</a></li>
		       <li><a href="javascript:_X.OpenSheet('OM040030')">일일작업조회</a></li>
		   </ul>
		</li>
		<li id="smenu_OM">매출관리
		    <ul style='width: 180px;'>
		       <li><a href="javascript:_X.OpenSheet('OM050010')">매출등록</a></li>
		       <li><a href="javascript:_X.OpenSheet('OM050020')">반품등록</a></li>
		       <li><a href="javascript:_X.OpenSheet('OM050030')">반품승인</a></li>
		       <li><a href="javascript:_X.OpenSheet('OM050040')">매출(매입)집계</a></li>
		       <li><a href="javascript:_X.OpenSheet('OM050050')">통합이용수수료청구</a></li>
		       <li><a href="javascript:_X.OpenSheet('OM050060')">통합이용수수료입금</a></li>
		       <li><a href="javascript:_X.OpenSheet('OM050070')">통합이용수수료현황</a></li>
		   </ul>
		</li>
		<li id="smenu_OM">팔레트관리
		    <ul style='width: 180px;'>
		       <li><a href="javascript:_X.OpenSheet('OM060010')">기초재고등록</a></li>
		       <li><a href="javascript:_X.OpenSheet('OM060020')">입출고등록</a></li>
		       <li><a href="javascript:_X.OpenSheet('OM060030')">입출고현황</a></li>
		       <li><a href="javascript:_X.OpenSheet('OM060040')">재고처별현황</a></li>
		   </ul>
		</li>
		<li id="smenu_OM">현황정보관리
		    <ul style='width: 180px;'>
		       <li><a href="javascript:_X.OpenSheet('OM070010')">시설사용현황</a></li>
		       <li><a href="javascript:_X.OpenSheet('OM070020')">시설이용현황</a></li>
		   </ul>
		</li>

	</ul>
</div>

<div id='jqxMenu_QM' style="display:none; float:none; left:0; width:100%; height:24px; text-align:center; overflow:hidden; ">
	<ul>
      	
		<li id="smenu_QM">전자지도
		    <ul style='width: 180px;'>
		       <li><a href="javascript:_X.OpenSheet('QM010010')">차량위치추적</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM01001010')">차량정보(Popup)</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM01001020')">거점정보(Popup)</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM010020')">차량운행경로</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM01002010')">차량정보 및 센서정보(Popup)</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM01002020')">거점정보(Popup)</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM010030')">거점검색</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM010040')">지도검색</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM010050')">즐겨찾기</a></li>
		   </ul>
		</li>
		<li id="smenu_QM">위치/경로
		    <ul style='width: 180px;'>
		       <li><a href="javascript:_X.OpenSheet('QM020010')">차량위치</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM020020')">운행경로</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM020030')">운행장애</a></li>
		   </ul>
		</li>
		<li id="smenu_QM">모니터링 관리
		    <ul style='width: 180px;'>
		       <li><a href="javascript:_X.OpenSheet('QM030010')">주문별 품질이력 </a></li>
		       <li><a href="javascript:_X.OpenSheet('QM030020')">차량별 품질이력</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM030030')">알람 모니터링</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM030040')">차량별 배송 모니터링</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM030050')">지역별 차량 모니터링</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM030060')">창고별 모니터링</a></li>
		   </ul>
		</li>
		<li id="smenu_QM">기준정보관리
		    <ul style='width: 180px;'>
		       <li><a href="javascript:_X.OpenSheet('QM040010')">단말기 관리</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM040020')">센서태그 관리</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM040030')">차량 탈부착 관리</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM040040')">단말기 창고 탈부착 관리</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM040050')">품질그룹 관리</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM040060')">거점 관리</a></li>
		   </ul>
		</li>
		<li id="smenu_QM">공통화면 참조용
		    <ul style='width: 180px;'>
		       <li><a href="javascript:_X.OpenSheet('QM050010')">입주업체정보관리 〉 기본정보관리</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM050020')">권한관리 〉 사용자정보</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM050030')">입주업체정보관리 〉 차량정보관리</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM050040')">기준정보관리 〉 창고코드</a></li>
		   </ul>
		</li>
		<li id="smenu_QM">품질유통 조회
		    <ul style='width: 180px;'>
		       <li><a href="javascript:_X.OpenSheet('QM070010')">로그인/약관동의</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM070020')">메인 화면</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM070040')">주문별 품질모니터링</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM070050')">차량별 품질모니터링</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM070060')">차량위치 지도</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM070070')">Push 수신정보 조회</a></li>
		       <li><a href="javascript:_X.OpenSheet('QM070080')">환경설정</a></li>
		           <li id="li_car">차량 위치/경로
		                <ul style='width: 180px;'>
		                   <li><a href="javascript:_X.OpenSheet('QM07003010')">차량위치리스트</a></li>
		                   <li><a href="javascript:_X.OpenSheet('QM07003020')">운행경로 및 품질상태 정보</a></li>
		                   <li><a href="javascript:_X.OpenSheet('QM07003030')">센서별 품질이력</a></li>
		           	</ul>
		            </li>
		   </ul>
		</li>

	</ul>
</div>

<div id='jqxMenu_IM' style="display:none; float:none; left:0; width:100%; height:24px; text-align:center; overflow:hidden; ">
	<ul>
	
		<li id="smenu_IM">거래정보
		    <ul style='width: 180px;'>
		       <li id="li_test"><a href="javascript:_X.OpenSheet('IM010010')">품목별가격</a></li>
		       <li><a href="javascript:_X.OpenSheet('IM010020')">주요품목별가격</a></li>
		       <li><a href="javascript:_X.OpenSheet('IM010030')">일일 등락품목</a></li>
		       <li><a href="javascript:_X.OpenSheet('IM010040')">주간 등락품목</a></li>
		       <li><a href="javascript:_X.OpenSheet('IM010050')">월간 등락품목</a></li>
		       <li><a href="javascript:_X.OpenSheet('IM010060')">산지별 가격정보</a></li>
		       <li><a href="javascript:_X.OpenSheet('IM010070')">품목별 가격등락조회</a></li>
		   </ul>
		</li>
		<li id="smenu_IM">거래량정보
		    <ul style='width: 180px;'>
		       <li><a href="javascript:_X.OpenSheet('IM020010')">물량분포표</a></li>
		       <li><a href="javascript:_X.OpenSheet('IM020020')">기간별 거래물량추이</a></li>
		   </ul>
		</li>
		<li id="smenu_IM">경영지표
		    <ul style='width: 180px;'>
		       <li><a href="javascript:_X.OpenSheet('IM030010')">거래일보</a></li>
		       <li><a href="javascript:_X.OpenSheet('IM030020')">출하처 정산정보조회</a></li>
		       <li><a href="javascript:_X.OpenSheet('IM030030')">입주업체 정산정보조회</a></li>
		       <li><a href="javascript:_X.OpenSheet('IM030040')">통합이용수수료현황</a></li>
		   </ul>
		</li>
		<li id="smenu_IM">시설현황
		    <ul style='width: 180px;'>
		       <li><a href="javascript:_X.OpenSheet('IM040010')">시설현황</a></li>
		       <li><a href="javascript:_X.OpenSheet('IM040020')">시설임대현황</a></li>
		       <li><a href="javascript:_X.OpenSheet('IM040030')">시설사용현황</a></li>
		   </ul>
		</li>

	</ul>
</div>

<!--서브메뉴 끝 -->

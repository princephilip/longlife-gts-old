<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>

<div id='div_criteria' style='float:left;margin-bottom:5px;line-height:30px;'>
	<input id='FIND_CODE' type='text'> 
	<img id='btn_search' src='${pageContext.request.contextPath}/images/btn/search.gif' width="42px" height="19px"/>
	<img id='btn_confirm' src='${pageContext.request.contextPath}/images/btn/confirm.gif' width="42px" height="19px"/>
</div>	

<div id='grid_1' style='float:left;'></div>

<script type="text/javascript" charset="UTF-8">
	div_criteria.ResizeInfo = {init_width:1000, init_height:30, width_increase:1, height_increase:0};
	grid_1.ResizeInfo = {init_width:1000, init_height:655, width_increase:1, height_increase:1};

	FIND_CODE.ResizeInfo = {init_width:900,  init_height:0, anchor:{x1:1,y1:0,x2:1,y2:0}};
	$("#btn_search").bind("click", function(){x_DAO_Retrieve();});
	$("#btn_confirm").bind("click", function(){x_Confirm();});
</script>

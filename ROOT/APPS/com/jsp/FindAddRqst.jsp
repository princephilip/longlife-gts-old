<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<style>
  .wrap-btn-grids { padding-top: 2px; min-width: 100px; }
  .wrap-btn-grids .btn { margin-left: 6px; }
  #ta-sql { border: 1px solid #ccc; }
  .modal-lg { width: 500px !important; }
  #grid-sql { height: 400px; }
  select.input-sm { height: 22px !important; padding: 1px 5px !important; font-size: 12px !important; }
</style>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap-theme.min.css">
<script type="text/javaScript" src="/Mighty/3rd/bootstrap/js/bootstrap.min.js"></script>

<div id='topsearch' class='option_box2' >
	<div id='xbtns' >
		<span style='vertical-align:middle;'>
      <ul id="buttons">
        <li><button type="button" id='btn_past_temp' class="btn btn-primary btn-xs" data-toggle="modal" data-target="#tempMatrModal" style='vertical-align:middle;'>임시자재등록</button></li>
        <li><a class="retrieve" id="xBtnRetrieve" href="javascript:;" onclick="javascript:x_DAO_Retrieve();" title="[Ctrl+R] 데이타를 다시 조회합니다."></a></li>
        <li><a class="close" id="xBtnClose" href="javascript:;" onclick="javascript:x_Close();" title="[Ctrl+F4] 윈도우 화면을 닫습니다."></a></li>
      </ul>
<!-- 
			<html:button name='S_RETRIEVE' 	id='S_RETRIEVE' type='button' icon='ui-icon-refresh'  desc="조회" onClick="x_DAO_Retrieve();"></html:button>
			<html:button name='S_CLOSE' 	id='S_CLOSE' 	type='button' icon='ui-icon-circle-close' desc="닫기" onClick="x_Close();"></html:button> -->
		</span>
	</div>

	<div  id='S_SUB2' style="display:none">
		<div class="option_label">검색</div>
		<div class='option_input_bg w210'>
			<input id='S_MATR_NM' type='text' class='option_input_fix w200'>
		</div>
	</div>

	<div  id='S_SUB3' style="display:none">
		<div class='option_label' >자재구분</div>
		<div class="option_input_bg" >
			<select id="S_LARGE" style="width:90px"></select>
			<select id="S_MIDDLE" style="width:110px"></select>
			<select id="S_SMALL" style="width:120px"></select>
		</div>
		<div class='option_label'>검색</div>
		<div class='option_input_bg w200'>
			<input id='S_FIND' type='text' class='option_input_fix' style="width:190px">
		</div>
	</div>
</div>

<div style="float: left; height: 25px; color: #4374D9; font-weight: bold; color:red; line-height: 23px;">
  <div id="S_TYPE1" style="float: left; height: 25px; color: #4374D9; font-weight: bold; color:red; line-height: 23px;">
    <img src='${pageContext.request.contextPath}/Theme/images/btn/tit_icon_pop2.gif'>&nbsp;더블클릭으로 자재 선택&nbsp;&nbsp;
  </div>
  &nbsp;
</div>

<!-- <div id="S_TYPE2" style="float: right; height: 25px; color: #4374D9; font-weight: bold; color:red; line-height: 23px;">  
</div> -->

<div id="tabs_1" class='tabs_div'>
	<div id="ctab_nav" class="tabs_nav">
  	<ul>
      <li><a href="#ctab_1">도급자재</a></li>
      <li><a href="#ctab_1">청구내역</a></li>
      <li><a href="#ctab_1">덕트청구</a></li>
      <li><a href="#ctab_1">전체자재목록</a></li>
  	</ul>
	</div>

  <div id='S_TYPE2' style='position:absolute; width:100%; margin-top: -5px;'>
    <div style="float:right;">
      <button type="button" id='btn_past_ordr'      class="btn btn-success btn-xs" onClick="pf_duct_rqst();">덕트청구</button>
    </div>
  </div>

  <div id='S_TYPE3' style='position:absolute; width:100%; margin-top: -5px;'>
    <div style="float:right;">
      <button type="button" id='btn_mat_select'  class="btn btn-warning btn-xs" onClick="pf_mat_select();">선택</button>
    </div>
  </div>

	<div id="ctab_1" >
		<div id='tabsub_1' class='xtabc_div' >
			<div id='grid_3' class='grid_div' >
				<div id="dg_3" style='width:100%;height:100%;' class="slick-grid"></div>
			</div>
		</div>
		<div id='tabsub_2' class='xtabc_div' >
			<div id='grid_4' class='grid_div' >
				<div id="dg_4"  style='width:100%;height:100%;' class="slick-grid"></div>
			</div>
			<div id='grid_5' class='grid_div mt5' >
				<div id="dg_5" style='width:100%;height:100%;' class="slick-grid"></div>
			</div>
		</div>
    <div id='tabsub_3' class='xtabc_div' >
      <div id='grid_7' class='grid_div' >
        <div id="dg_7"  style='width:100%;height:100%;' class="slick-grid"></div>
      </div>
      <div id='grid_8' class='grid_div mt5' >
        <div id="dg_8" style='width:100%;height:100%;' class="slick-grid"></div>
      </div>
      <div id='grid_9' class='grid_div' >
        <div id="dg_9" class="slick-grid"></div>
      </div>
    </div>
		<div id='tabsub_4' class='xtabc_div' >
			<div id='grid_6' class='grid_div' >
				<div id="dg_6" style='width:100%;height:100%;' class="slick-grid"></div>
			</div>
		</div>
	</div>
</div>

</div> <!-- #pageLayout 밖에 Modal 놓기 위한 Trick -->

<div class="modal fade" id="tempMatrModal" tabindex="-1" role="dialog" aria-labelledby="tempMatrModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="tempMatrModalLabel">임시자재코드 생성</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <label class='option_label w140'>자재코드</label>
            <div class='option_input_bg'>
              <input id='S_ITEM_CODE' type='text' class='option_input_fix w100' readonly>
            </div>
        </div><!-- .row -->

        <div class="row">
          <label class='option_label w140'>품명</label>
            <div class='option_input_bg'>
              <input id='S_ITEM_NAME' type='text' class='option_input_fix w300' maxlength=50>
            </div>
        </div><!-- .row -->

        <div class="row">
          <label class='option_label w140'>규격</label>
            <div class='option_input_bg'>
              <input id='S_ITEM_STD' type='text' class='option_input_fix w300' maxlength=30>
            </div>
        </div><!-- .row -->

        <div class="row">
          <label class='option_label w140'>단위</label>
            <div class='option_input_bg'>
              <input id='S_ITEM_UNIT' type='text' class='option_input_fix w100' maxlength=5>
              <input id='S_ITEM_UNIT2' type='hidden' class='option_input_fix w100' maxlength=5>
            </div>
        </div><!-- .row -->
        <div class="row">
          <label class='option_label w140'>특이사항</label>
            <div class='option_input_bg'>
              <input id='S_REMARKS' type='text' class='option_input_fix w300' maxlength=50>
            </div>
        </div><!-- .row -->
      </div><!-- .modal-body -->
      <div class="modal-footer">
        <div class="text-center">
          <button type="button" id="btn-tempMatrClose" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="btn-tempMatrCreate" class="btn btn-primary">코드 생성</button>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
/*	topsearch.ResizeInfo 	= {init_width: 1000, init_height:33 , width_increase:1, height_increase:0};
	tabs_1.ResizeInfo       = {init_width:1000, init_height:642, width_increase:1, height_increase:1};
	ctab_nav.ResizeInfo 	= {init_width:1000, init_height:25, width_increase:1, height_increase:0};*/
/*    ctab_1.ResizeInfo 		= {init_width:1000, init_height:642, width_increase:1, height_increase:1};*/
/*    grid_3.ResizeInfo 		= {init_width: 1000, init_height:642, width_increase:1, height_increase:1};
    grid_4.ResizeInfo 		= {init_width: 1000, init_height:200, width_increase:1, height_increase:0};
    grid_5.ResizeInfo 		= {init_width: 1000, init_height:440, width_increase:1, height_increase:1};
    grid_6.ResizeInfo 		= {init_width: 1000, init_height:642, width_increase:1, height_increase:1};*/

  topsearch.ResizeInfo = {init_width:1000, init_height:33 , width_increase:1, height_increase:0};
  tabs_1.ResizeInfo    = {init_width:1000, init_height:615, width_increase:1, height_increase:1};

  tabsub_1.ResizeInfo = {init_width:1000, init_height:612, width_increase:1, height_increase:1};
  grid_3.ResizeInfo   = {init_width:1000, init_height:610, width_increase:1, height_increase:1};

  tabsub_2.ResizeInfo = {init_width:1000, init_height:612, width_increase:1, height_increase:1};
  grid_4.ResizeInfo   = {init_width:1000, init_height:260, width_increase:1, height_increase:0};
  grid_5.ResizeInfo   = {init_width:1000, init_height:350, width_increase:1, height_increase:1};

  tabsub_3.ResizeInfo = {init_width:1000, init_height:612, width_increase:1, height_increase:1};
  grid_7.ResizeInfo   = {init_width:1000, init_height:260, width_increase:1, height_increase:0};
  grid_8.ResizeInfo   = {init_width:1000, init_height:350, width_increase:1, height_increase:1};
  grid_9.ResizeInfo   = {init_width:1, init_height:1, width_increase:0, height_increase:0};

  tabsub_4.ResizeInfo = {init_width:1000, init_height:612, width_increase:1, height_increase:1};
  grid_6.ResizeInfo   = {init_width:1000, init_height:610, width_increase:1, height_increase:1};
</script>

<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<jsp:include page="/Mighty/include/COMM_JS.jsp"/>

<style>
	table.summary_total { font-size:9pt; font-family:"굴림"; padding:1px; }
	table.summary_total th { font-weight: bold; text-align: center; padding: 4px 0; color: #666; background-color: #efefef; border-bottom: 1px dashed #999; border-right: 1px dashed #999; }
	table.summary_total td { text-align:right; font-weight: bold; padding: 4px 4px 4px 0; border-bottom: 1px dashed #999; border-right: 1px dashed #999; color: #666; }
	table.summary_total td.total_td { background-color: #efefef; color: #666; }
	table td.htitle { background-color: #efefef; font-weight: bold; color: #666; text-align: center; padding:2px;}
	table td.detldata { color: #666; text-align: center;}
</style>

<%
DecimalFormat df = new DecimalFormat("00");
Calendar calendar = Calendar.getInstance();

String curr_yy = Integer.toString(calendar.get(Calendar.YEAR)); //년도를 구한다
String curr_mm = df.format(calendar.get(Calendar.MONTH) + 1); //달을 구한다
String curr_dd = df.format(calendar.get(Calendar.DATE)); //날짜를 구한다
String curr_dy = "";

int iDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK); //요일을 구한다
switch(iDayOfWeek){
  case 1: curr_dy = "일"; break;
  case 2: curr_dy = "월"; break;
  case 3: curr_dy = "화"; break;
  case 4: curr_dy = "수"; break;
  case 5: curr_dy = "목"; break;
  case 6: curr_dy = "금"; break;
  case 7: curr_dy = "토"; break;
}

	String 	year = "";
	String 	month = "";
	String 	day = "";
	int 	lastday=0;
	int 	rowCnt=0;
	int 	startWeek=0;
	String 	calendarHtml = "";

	Calendar today 		= Calendar.getInstance();
    Calendar firstDay 	= Calendar.getInstance();
    Calendar lastDay  	= Calendar.getInstance();

	StringBuffer sb = new StringBuffer(1024);
	year  = request.getParameter("year");
	month = request.getParameter("month");
	day   = request.getParameter("day");

	if(year==null)
		year=Integer.toString(today.get(Calendar.YEAR));
	if(month==null)
		month=Integer.toString(today.get(Calendar.MONTH)+1);
	if(day==null)
		day=Integer.toString(today.get(Calendar.DATE));

    firstDay.set(Integer.parseInt(year),Integer.parseInt(month)-1,1);  	// 해당 월의 다음 달의 첫 날짜를 구한다.
    lastDay.set(Integer.parseInt(year),Integer.parseInt(month),1);  	// 해당 월의 다음 달의 첫 날짜를 구한다.
    lastDay.add(Calendar.DATE,-1); 				// 해당 월의 마지막날을 구한다.

	lastday = lastDay.get(Calendar.DATE);
	if(Integer.parseInt(day)>lastday)
		day=Integer.toString(lastday);

	startWeek = firstDay.get(Calendar.DAY_OF_WEEK);

	if(firstDay.get(Calendar.DAY_OF_WEEK)!=Calendar.SUNDAY)
	{
		sb.append("<TR align=left valign=top>");
		rowCnt++;
		for(int i=1;i<startWeek;i++)
			sb.append("<TD></TD>");
	}
	for(int i=1;i<=lastday;i++)
	{
		Calendar dt = Calendar.getInstance();
    	dt.set(Integer.parseInt(year),Integer.parseInt(month) -1,i);  		// 해당 월의 다음 달의 첫 날짜를 구한다.
		String font1 = (dt.get(Calendar.DAY_OF_WEEK)==Calendar.SUNDAY?"<font color=red>":dt.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY?"<font color=blue>":"");
		String font2 = (dt.get(Calendar.DAY_OF_WEEK)==Calendar.SUNDAY||dt.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY?"</font>":"");
		if(dt.get(Calendar.DAY_OF_WEEK)==Calendar.SUNDAY)
		{
			sb.append("<TR align=left valign=top>");
			rowCnt++;
		}
		sb.append("<TD id='" + "td_day_" + i + "' onclick='SelectionChanged(" + i + ");'>" + font1 + i + font2 + "</TD>");
		if(dt.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY)
			sb.append("</TR>");
	}
	if(lastDay.get(Calendar.DAY_OF_WEEK)!=Calendar.SATURDAY)
	{
		for(int i=lastDay.get(Calendar.DAY_OF_WEEK);i<6;i++)
			sb.append("<TD></TD>");
		sb.append("</TR>");
	}
	calendarHtml = sb.toString();

%>

<HTML>
	<HEAD>
		<title>일정조회</title>
	</HEAD>
	<body onload="jf_initial()">
	<table width="100%" height="100%" border="1" bordercolor="#c0c0c0" cellspacing="0" cellpadding="2"
		style="FONT-SIZE: 9pt; FONT-FAMILY: Dotum; BORDER-COLLAPSE: collapse">
		<tr>
			<td height="41" colspan=3 align=left valign=top id="td_summary">
			</td>
			<td height="41" style="padding-left:15; line-height: 186%;" colspan=1 align=center>
				<select id="selYear" name="selYear" align="absmiddle" style="width:60px" onchange="MonthChanged()">
          <option value=2007>&nbsp;2007&nbsp;</option>
          <option value=2008>&nbsp;2008&nbsp;</option>
          <option value=2009>&nbsp;2009&nbsp;</option>
          <option value=2010>&nbsp;2010&nbsp;</option>
          <option value=2011>&nbsp;2011&nbsp;</option>
          <option value=2012>&nbsp;2012&nbsp;</option>
          <option value=2013>&nbsp;2013&nbsp;</option>
          <option value=2014>&nbsp;2014&nbsp;</option>
          <option value=2015>&nbsp;2015&nbsp;</option>
          <option value=2016>&nbsp;2016&nbsp;</option>
          <option value=2017>&nbsp;2017&nbsp;</option>
          <option value=2018>&nbsp;2018&nbsp;</option>
          <option value=2019>&nbsp;2019&nbsp;</option>
          <option value=2020>&nbsp;2020&nbsp;</option>
				</select>년
				<select id="selMonth" name="selMonth" align="absmiddle" style="width:45px" onchange="MonthChanged()">
					<option value=1 >&nbsp;01&nbsp;</option>
					<option value=2 >&nbsp;02&nbsp;</option>
					<option value=3 >&nbsp;03&nbsp;</option>
					<option value=4 >&nbsp;04&nbsp;</option>
					<option value=5 >&nbsp;05&nbsp;</option>
					<option value=6 >&nbsp;06&nbsp;</option>
					<option value=7 >&nbsp;07&nbsp;</option>
					<option value=8 >&nbsp;08&nbsp;</option>
					<option value=9 >&nbsp;09&nbsp;</option>
					<option value=10 >&nbsp;10&nbsp;</option>
					<option value=11 >&nbsp;11&nbsp;</option>
					<option value=12 >&nbsp;12&nbsp;</option>
				</select>월
				<br />
				전체 선택/해제 <input type="checkbox" id="chkAll" name="chkAll" onClick="CheckAll(this.checked)">
				<br />
				오늘 :
				<font style="font-size:10pt;color:#FF8040;font-weight:bold"><%=curr_yy%></font>년&nbsp;
				<font style="font-size:10pt;color:#FF8040;font-weight:bold"><%=curr_mm%></font>월&nbsp;
				<font style="font-size:10pt;color:#FF8040;font-weight:bold"><%=curr_dd%></font>일
				(<font style="font-size:10pt;color:#FF8040;font-weight:bold"><%=curr_dy%></font>)
				<br />
				<b>급여한도 : <span id="m_limit_amt">0</span> 원<br />
				잔여금액 : <span id="m_remain_amt">0</span> 원</b>
			</td>
			<td colspan=3 align=center valign=top id="td_remark">
			</td>
		</tr>

		<tr height="30" align="center" valign="middle" bgcolor="#efeffa">
			<td width="14.2%">일 <input type="checkbox" name="chkWeek" value=1 onClick="CheckWeek(this.value,this.checked)">(선택/해제)</td>
			<td width="14.3%">월 <input type="checkbox" name="chkWeek" value=2 onClick="CheckWeek(this.value,this.checked)">(선택/해제)</td>
			<td width="14.3%">화 <input type="checkbox" name="chkWeek" value=3 onClick="CheckWeek(this.value,this.checked)">(선택/해제)</td>
			<td width="14.3%">수 <input type="checkbox" name="chkWeek" value=4 onClick="CheckWeek(this.value,this.checked)">(선택/해제)</td>
			<td width="14.3%">목 <input type="checkbox" name="chkWeek" value=5 onClick="CheckWeek(this.value,this.checked)">(선택/해제)</td>
			<td width="14.3%">금 <input type="checkbox" name="chkWeek" value=6 onClick="CheckWeek(this.value,this.checked)">(선택/해제)</td>
			<td width="14.3%">토 <input type="checkbox" name="chkWeek" value=7 onClick="CheckWeek(this.value,this.checked)">(선택/해제)</td>
		</tr>
		<%=calendarHtml%>
	</table>
	</body>

<script type="text/javascript">
	var curYear	= <%=year%>;
	var curMonth= <%=month%>;
	var curDay	= <%=day%>;
	var lastDay	= <%=lastday%>;
	var rowCnt	= <%=rowCnt%>;
	var startWeek = <%=startWeek%>;

	td_day_1.height  = (80/rowCnt) + '%';
	td_day_8.height  = (80/rowCnt) + '%';
	td_day_15.height = (80/rowCnt) + '%';
	td_day_22.height = (80/rowCnt) + '%';
	td_day_28.height = (80/rowCnt) + '%';
	td_day_<%=lastday%>.height = (80/rowCnt) + '%';

	selYear.value = <%=year%>;
	selMonth.value = <%=month%>;

	var init = "Y";

	function chkAllCheck(val) {
		chkAll.checked = val;
	}


	function CheckWeek(idx,val)
	{
		if(typeof(chkday)=="undefined")
			return;
		for(var i=0; i<=5; i++)
		{
			var no = (idx - startWeek) + (i*7);
			if(no>=0 && no<lastDay)
			{
				var chkObj = chkday[no];
				chkObj.checked = val;

			}
		}
	}

	function CheckAll(val)
	{
		if(typeof(chkday)=="undefined") return;

		for(var i=0; i<lastDay; i++)
		{
			var chkObj = chkday[i];
			chkObj.checked = val;
		}
	}

	function jf_initial(){
		if(parent!=null&&typeof(parent.jf_CalendarLoaded)!="undefined"){
			//parent.jf_CalendarLoaded(curYear,curMonth,lastDay);
			//parent.jf_CalendarLoaded();
			init = "N";
		}
	}

	function SelectionChanged(newDay){ return;
	}

	function SetData(day, data){
		if(document.getElementById("td_day_"+day)!=null)
		{
			document.getElementById("td_day_"+day).innerHTML= GetDayHtml(day) + '<br>' + data;
		}
	}

	function GetDayHtml(day){
		var preHtml = document.getElementById("td_day_"+day).innerHTML;
		if(preHtml.indexOf("<br>")>0)
			return preHtml.substring(0,preHtml.indexOf("<br>"));
		else
			//return preHtml + '<input type="checkbox" id="chkday'+day+'" name="chkday" value="'+selYear.value+'-'+(selMonth.value<=9?'0':'')+selMonth.value+'-'+(day<=9?'0':'')+day+'">';
			return preHtml + '<input type="checkbox" id="chkday" name="chkday" value="'+selYear.value+'-'+(selMonth.value<=9?'0':'')+selMonth.value+'-'+(day<=9?'0':'')+day+'">';
	}

	function MonthChanged(){
		if(parent!=null&&typeof(parent.DateChanged)!="undefined"){
			if(!parent.DateChanged(selYear.value,selMonth.value,'1',init))
				return;
		}

		document.location.href="calendar4.jsp?year="+selYear.value+"&month="+selMonth.value+"&day=01";
	}

	function FrmReload(a_year, a_month){
		document.location.href="calendar4.jsp?year="+a_year+"&month="+a_month+"&day=01";
	}

	function SelectDay(aObj) {
		if(parent!=null&&typeof(parent.jf_select_day)!="undefined"){
			if(!parent.jf_select_day(aObj.value,(aObj.checked?"Y":"N")))
				return;
		}
	}

	function SetRemark(txt)
	{
		td_remark.innerHTML = txt;
	}

	function SetSummary(txt)
	{
		td_summary.innerHTML = txt;
	}

	function SetLimitAmt(amt)
	{ if(typeof(amt)!="undefined") {
			if(typeof(m_limit_amt)=="object") document.getElementById("m_limit_amt").innerHTML = _X.FormatComma(amt[1]);
			if(typeof(m_remain_amt)=="object") document.getElementById("m_remain_amt").innerHTML = _X.FormatComma(amt[2]);
		}
	}

	//배경색 변경
	function uf_bg_color() {
		var ls_rtn = _X.XmlSelect("gs", "GS01040", "SM_CODE_CALENDAR_R01", [selYear.value, selMonth.value], "array");

		if(ls_rtn.length > 0) {
			for(var i=0; i<ls_rtn.length; i++) {
				var li_date = ls_rtn[i][4];//날짜
				if(document.getElementById("td_day_" + li_date.toString())!=null) {
					if(ls_rtn[i][0] == 'Y') {
						document.getElementById("td_day_" + li_date.toString()).bgColor = "#deeeee";
					}
					if(ls_rtn[i][1] == 'Y') {
						document.getElementById("td_day_" + li_date.toString()).bgColor = "#ffe3ee";
					}
					if(ls_rtn[i][2] == 'Y') {
						document.getElementById("td_day_" + li_date.toString()).bgColor = "#ffe3ee";
					}

				}
			}
		}
	}


</script>


</HTML>


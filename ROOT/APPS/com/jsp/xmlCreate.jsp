﻿<%@ page language="java" contentType="text/xml; charset=utf-8"	pageEncoding="utf-8"%>
<%@ page import="java.io.FileOutputStream"%>
<%@ page import="java.io.FileInputStream"%>
<%@ page import="java.io.IOException"%>
<%@ page import="org.jdom.Document"%>
<%@ page import="org.jdom.Namespace"%>
<%@ page import="org.jdom.Element"%>
<%@ page import="org.jdom.Attribute"%>
<%@ page import="org.jdom.Document"%>
<%@ page import="org.jdom.output.Format"%>
<%@ page import="org.jdom.output.XMLOutputter"%>

<%

 String xmlName = request.getParameter("xmlName");
 String pdfName = request.getParameter("pdfName");
 String xmlgbn = request.getParameter("xmlgbn");
 String xmlTitle = request.getParameter("xmlTitle");
 String xmlPath;
 
 if(xmlgbn.equals("1")) {
	 xmlPath = "D:\\uploadfiles\\pmis_file\\xml\\" + xmlName;
 } else if (xmlgbn.equals("2")) {
	 xmlPath = "\\\\10.176.224.197\\pmis_file\\XML\\" + xmlName;
 } else {
	 xmlPath = "\\\\101.1.33.130\\pmis_file\\XML\\" + xmlName;
 }
 
 Document doc = new Document(); 
 Element root = new Element("parameter");
 
 Namespace xsi = Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
 Namespace xsd = Namespace.getNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
 
 root.addNamespaceDeclaration(xsi);
 root.addNamespaceDeclaration(xsd);

 Element title = new Element("title");
 Element file = new Element("file");
  
 title.setText(xmlTitle);
 file.setText(pdfName);
 file.setAttribute("type", "PDF");
 file.setAttribute("path", "");
 file.setAttribute("no", "1");
    
 root.addContent(title); 
 root.addContent(file);
 
 doc.setRootElement(root);
 try {                                                             
       FileOutputStream out2 = new FileOutputStream(xmlPath);
       //xml 파일을 떨구기 위한 경로와 파일 이름 지정해 주기
        XMLOutputter serializer = new XMLOutputter();                 
                                                                     
       Format f = serializer.getFormat();                            
       f.setEncoding("EUC-KR");
       f.setIndent(" ");                                             
       f.setLineSeparator("\r\n");                                   
       f.setTextMode(Format.TextMode.TRIM);                          
       serializer.setFormat(f);                                      
                                                                     
       serializer.output(doc, out2);                                  
       out2.flush();                                                  
       out2.close(); 
       
 } catch (IOException e) {}                                                                 
%>


<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<script type="text/javascript" src="<c:url value='/Mighty/3rd/se2/js/HuskyEZCreator.js'/>" charset="utf-8"></script>

<div id='topsearch' class='option_box'>
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true" pgmcode="HO04010"></html:authbutton>
		</span>
	</div>
	
	<div id='userxbtns'>
		<button type="button" id='btn_file' class="btn btn-success btn-xs" onClick="pf_file();">첨부파일등록</button>
	</div>
	
</div>
	
<div id='freeform' class='detail_box ver2'>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">*제목</label>
		<div class='detail_input_bg' style="width:920px">
			<input type='text' id='BOARDTITLE' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row detail_row_bb'>
		<label class='detail_label' style="width:100px">공지여부</label>
		<div class='detail_input_bg' style="width:100px;padding-left:10px;">
			<input id='POPUP_YN' type='checkbox' >
		</div>

		<label class='detail_label' style="width:100px">공지기간</label>
		<div class='detail_input_bg' style="width:100px">
			<input type='text' id='SDATE' class='detail_date'>
		</div>
		<div class='detail_input_bg noborder' style="width:30px;text-align:center">~</div>
		<div class='detail_input_bg noborder' style="width:100px">
			<input type='text' id='EDATE' class='detail_date'>
		</div>
		<div class='detail_input_bg noborder' style="width:470px;"></div>
	</div>
</div>

<div id='grid_2' class='grid_div' style= "visibility:block">
	<div id="dg_2" class='slick-grid'></div>
</div>

<div id='grid_1' class='grid_div' style= "visibility:hidden">
	<div id="dg_1" class='slick-grid'></div>
</div>


<div id='from_edit' class='grid_div'>
	<div id='S_CONTENT'>
		<textarea id="ir1" name="ir1" style="width:100%; height: 290px;" readonly ></textarea>
	</div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo 		= {init_width:1000, init_height: 33, anchor:{x1:1,y1:1,x2:1,y2:0}};
	freeform.ResizeInfo			= {init_width:1000, init_height: 60, anchor:{x1:1,y1:1,x2:1,y2:0}};

	grid_2.ResizeInfo 			= {init_width:1000, init_height:100, anchor:{x1:1,y1:1,x2:1,y2:0}};
	grid_1.ResizeInfo 			= {init_width:1000, init_height:  4, anchor:{x1:1,y1:1,x2:0,y2:0}};

	from_edit.ResizeInfo		= {init_width:1000, init_height:290, anchor:{x1:1,y1:1,x2:1,y2:1}};
	
</script>
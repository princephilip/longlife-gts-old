<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%      
   String ls_title = java.net.URLDecoder.decode(request.getParameter("msg_title").toString(),"utf-8");	
	 ls_title = java.net.URLDecoder.decode(ls_title, "utf-8");
	
	 String ls_msg_content = java.net.URLDecoder.decode(request.getParameter("msg_content").toString(),"utf-8");	
	 ls_msg_content = java.net.URLDecoder.decode(ls_msg_content, "utf-8");
	 
	 int	li_button  = Integer.parseInt(request.getParameter("msg_button"));
	 int	li_icon 	= Integer.parseInt(request.getParameter("msg_icon"));
	 int	li_default = Integer.parseInt(request.getParameter("msg_default"));	 	 
%>

<HTML>
<HEAD>
<TITLE>MessageBox</TITLE>
	<html:resource enableTheme="true" enableCache="true"/>
</HEAD>


<body onload="jf_initial()" onkeydown="jf_body_keydown(event)" style="background-image:url('<c:url value="/Theme/images/messagebox/msg_body_bg.gif"/>');background-repeat:repeat-x;background-position:top;">


<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td height="78" align="center">
		<div align="center">
		<% if(li_icon==0 || li_icon==1 || li_icon==3 || li_icon==5 ) { %>
				<IMG src="<c:url value='/images/messagebox/icon_information.jpg'/>" border=0>
		<% } else if(li_icon==2) { %>
				<IMG src="<c:url value='/images/messagebox/icon_error.jpg'/>" border=0>
		<% } else if(li_icon==4) { %>
				<IMG src="<c:url value='/images/messagebox/icon_question.jpg'/>" border=0>
		<% } else if(li_icon==6) { %>
				<IMG src="<c:url value='/images/messagebox/icon_ok.jpg'/>" border=0>
		<% } %>
		</div>
		</td>
	</tr>
	<tr style="background-image:url('<c:url value="/Theme/images/messagebox/tb_bg.jpg"/>')">
		<td valign="middle" align="center" style="color:#f0421d;font-family:돋움;font-size:12px;text-decoration:underline;font-weight:bold;">
		<%=ls_title%>
		</td>
	</tr>
	<tr style="background-image:url('<c:url value="/Theme/images/messagebox/tb_bg.jpg"/>');width:240px"> 
		<td id="td_content" valign="top" align="center" style="font-family:돋움;font-size:10px;"> 
		<%=ls_msg_content%>
		</td>
	</tr>
	<tr style="height:50px;background-image:url('<c:url value="/Theme/images/messagebox/tb_bott.jpg"/>');background-repeat:no-repeat;">
		<td >
		<div id="button_div" align="center"></div>
		</td>
	</tr>
</table>


<script language="JavaScript" type="text/javascript">
	function jf_initial() {
		
		<% if(li_button==0 || li_button==1 || li_button==2 ) { %>
				button_div.innerHTML = _X.GetBtn("btn_1", "btn_ok", "${pageContext.request.contextPath}/images/messagebox/", "btnClick(1)", 59,21);
		<% } if(li_button==2) { %>
			  button_div.innerHTML = _X.GetBtn("btn_2", "btn_cancel", "${pageContext.request.contextPath}/images/messagebox/", "btnClick(2)", 59,21);
				
		<% } if(li_button==3 || li_button==4) { %>
			  button_div.innerHTML = _X.GetBtn("btn_1", "btn_yes", "${pageContext.request.contextPath}/images/messagebox/", "btnClick(1)", 59,21)
			                       + _X.GetBtn("btn_2", "btn_no",  "${pageContext.request.contextPath}/images/messagebox/", "btnClick(2)", 59,21);
				
		<% } if(li_button==4) { %>
			  button_div.innerHTML = _X.GetBtn("btn_3", "btn_cancel", "${pageContext.request.contextPath}/images/messagebox/", "btnClick(3)", 59,21);
				
		<% } if(li_button==5) { %>
			  button_div.innerHTML = _X.GetBtn("btn_1", "btn_retry",  "${pageContext.request.contextPath}/images/messagebox/", "btnClick(1)", 59,21)
			                       + _X.GetBtn("btn_2", "btn_cancel", "${pageContext.request.contextPath}/images/messagebox/", "btnClick(2)", 59,21);
			
		<% } if(li_button==6) { %>
			
			  button_div.innerHTML = _X.GetBtn("btn_1", "btn_abort",  "${pageContext.request.contextPath}/images/messagebox/", "btnClick(1)", 59,21)
			                       + _X.GetBtn("btn_2", "btn_retry",  "${pageContext.request.contextPath}/images/messagebox/", "btnClick(2)", 59,21)
			                       + _X.GetBtn("btn_3", "btn_ignore", "${pageContext.request.contextPath}/images/messagebox/", "btnClick(3)", 59,21);
			
		<% } %>
									
	}
	
	var	clientwin = window.dialogArguments;

	function btnClick(idx) {
	    if (clientwin) clientwin.ii_rtvalue = idx;
	    
	    window.close();
	    return clientwin.ii_rtvalue;
	}

	function jf_body_keydown(a_event) {
		if(a_event.keyCode==KEY_y || a_event.keyCode==(KEY_y +32))
			btnClick(1);
		else if(a_event.keyCode==KEY_n || a_event.keyCode==(KEY_n +32))
			btnClick(2);
		else if(a_event.keyCode==KEY_c || a_event.keyCode==(KEY_c +32))
			btnClick(3);
	}

	<% if(li_button==3) { %>
		clientwin.ii_rtvalue = 2;
	<% } else if(li_button==4) {%>
		clientwin.ii_rtvalue = 3;
	<% } else {%>
		clientwin.ii_rtvalue = -1;
	<% } %>
</script>

</BODY>
</HTML>


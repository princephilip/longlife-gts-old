<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<div id='topsearch' class='option_box2'>
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authchildbutton id='buttons' grid='dg_1' append='true' insert='false' duplicate='false' delete='true' excel='false' retrieve='true' ></html:authchildbutton>
		</span>
	</div>
	
	<div id='userxbtns' style="height:21px;">
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:button name='S_SAVE' id='S_SAVE' type='button' icon='ui-icon-check' desc="변경내역 저장" onClick="x_DAO_Save(dg_1)" ></html:button>&nbsp;
		</span>
	</div>
</div>

<div id='freeform' class='detail_box'>
	
	<div class='detail_label w100'>견적서번호</div> 
	<div class='detail_input_bg'>
		<input id='ORDER_DOCNO' type='text' class='detail_input_fix w110' disabled >
	</div> 
	
	<div class='detail_line' ></div> 
	
	<div class='detail_label w100'>제목</div> 
	<div class='detail_input_bg'>
		<input id='ORDER_NAME' type='text' class='detail_input_fix w220' disabled>
	</div> 	
	
	<div class='detail_line' ></div> 
	
	<div class='detail_label w100'>공급가액</div> 
	<div class='detail_input_bg'>
		<input id='SALE_AMT' type='text' class='detail_input_fix w110' disabled>
	</div> 
	
	<div class='detail_label w100'>원가</div> 
	<div class='detail_input_bg'>
		<input id='BUY_AMT'  type='text' class='detail_input_fix w110' disabled>
	</div> 
	
</div>
	
<div id='grid_1' class='grid_div mr5'>
	<div id="dg_1" style="width:100%; height:100%; " class="slick-grid"></div>
</div>


<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};
	freeform.ResizeInfo  = {init_width:1300, init_height:83, width_increase:0, height_increase:0};
		grid_1.ResizeInfo 	= {init_width:1000, init_height:570, width_increase:1, height_increase:1};
</script>
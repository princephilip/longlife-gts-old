<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<style>
* {
  margin: 0;
  padding: 0;
}

body {
  margin: 100px;
}

.pop-layer .pop-container {
  padding: 20px 25px;
}

.pop-layer p.ctxt {
  color: #666;
  line-height: 25px;
}


.pop-layer {
  display: none;
  position: absolute;
  top: 50%;
  left: 50%;
  width: 100%;
  height: 100%;
  background-color: #fff;
  border: 5px solid #3571B5;
  z-index: 10;
}

.dim-layer {
  display: none;
  position: fixed;
  _position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 100;
}

.dim-layer .dimBg {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: #000;
  opacity: .5;
  filter: alpha(opacity=50);
}

.dim-layer .pop-layer {
  display: block;
}


.cont-div {
	width: 100%;
	height: 100%;
}
</style>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2' >
	<div class='option_label iw100'>현장/카드번호</div>
	<div class='option_input_bg' id='div_FIND'>
		<input id='S_FIND' type='text' >
	</div>

	<div id='userxbtns'>
		<button type="button" id='DATA_RETIEVE'  class="btn btn-success btn-xs" onClick="x_DAO_Retrieve();">조회</button>
		<button type="button" id='CLOSE_FROM'  class="btn btn-warning btn-xs" onClick="x_Close();">닫기</button>
	</div>
</div>

<div id='grid_1' class='grid_div' >
	<div id="dg_1" class="slick-grid"></div>
</div>


<div id='grid_2' class='grid_div mt10' style='text-align: center;'>
	<a href="#layer2" class="btn-example">
		<img id="photo_1" class='mb5' src="/Theme/images/noimage.jpg" align="middle;" style="border:0; width:240px; height:160px;cursor:pointer">
	</a>
</div>

<div class="dim-layer">
    <div class="dimBg"></div>
    <div id="layer2" class="pop-layer">
        <div class="pop-container">
            <div class="pop-conts">
                <!--content //-->
                <div class="cont-div"></div>

<!--                 <div class="btn-r">
                    <a href="#" class="btn-layerClose">Close</a>
                </div> -->
                <!--// content-->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width: 1000, init_height:33 , width_increase:1, height_increase:0};
	div_FIND.ResizeInfo 	= {init_width: 700, init_height:33 , width_increase:1, height_increase:0};
	S_FIND.ResizeInfo 		= {init_width: 690, init_height:22 , width_increase:1, height_increase:0};
	grid_1.ResizeInfo 		= {init_width: 1000, init_height:500, width_increase:1, height_increase:1};
	grid_2.ResizeInfo 		= {init_width: 1000, init_height:160, width_increase:1, height_increase:0};
</script>
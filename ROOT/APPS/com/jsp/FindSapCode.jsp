﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>

<div id='topsearch' class='option_box'>
	<div class="detail_label_left w110" style="background-color: #f0f4f7;">업체구분</div>
	<div class="option_input_bg w120">                                                                                                 
		<select id='S_CORPDIVCD'></select>
	</div>

	<div class="detail_label_left w110" style="background-color: #f0f4f7;">검색어</div>
	<div class="option_input_bg" style="width:400px;">
		<input id='FIND_CODE' type='text' class='option_input_fix' style='width:190px' value="">
		<img id='btn_search' src='${pageContext.request.contextPath}/images/btn/search.gif' width='42px' height='19px' style="cursor:pointer;" />
		<img id='btn_confirm' src='${pageContext.request.contextPath}/images/btn/confirm.gif' width='42px' height='19px' style="cursor:pointer;" />
	</div>
</div>

<div id='grid_1' class='grid_div mt5'>
  <div id="dg_1" class="slick-grid"></div>
</div>

<script type='text/javascript' charset='UTF-8'>
	topsearch.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};
	grid_1.ResizeInfo = {init_width:1000, init_height:662, width_increase:1, height_increase:1};
	$('#btn_search').bind('click', function(){x_DAO_Retrieve();});
	$('#btn_confirm').bind('click', function(){x_Confirm();});
</script>

<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>

<style>
#btn_search {
  border: 1px solid transparent;
  border-bottom-left-radius: 0;
  border-top-left-radius: 0;
  height: 25px;
  margin: 0;
  outline: none;
  padding: 0 0;
  width: 50px;
  -webkit-box-shadow: none;
  box-shadow: none;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  background: #0f6ba8;
  vertical-align: top; 
}

.gb_bg {
  background-image: url('/images/button/GroupButton_01.png');
  -webkit-background-size: 25px 2011px;
  background-size: 25px 2011px;
}

.gb_mag {
  background-position: 0px -1625px;
  display: inline-block;
  margin: -1px;
  height: 25px;
  width: 25px;
}

#FIND_CODE {
  border-style: solid;
  border-width: 2px 0px 2px 2px;
  border-color: #0f6ba8;
  /*border-color: #c0dade;*/
  background-color: #ffffff; 
  height:19px;
  margin:0px 0px 0px 0px;
  padding:1px 0px 1px 4px;
  /*margin:0px 0px 2px 1px;  padding:0px 1px 1px 1px;*/
  text-align:left;
  vertical-align:middle;
  color:#000000; 
}

</style>


<div id='div_criteria' style='float:left;margin-bottom:5px;line-height:30px;'>
	<input id='FIND_CODE' type='text'><button id="btn_search" aria-label="검색" name="" ><span class="gb_bg gb_mag"></span></button>
</div>	

<div id='grid_1' style='float:left;'>
	<div id="dg_1" class="slick-grid"></div>
</div>

<script type="text/javascript" charset="UTF-8">
	div_criteria.ResizeInfo = {init_width:1000, init_height:30, width_increase:1, height_increase:0};
	grid_1.ResizeInfo = {init_width:1000, init_height:680, width_increase:1, height_increase:1};
	
	FIND_CODE.ResizeInfo = {init_width:950,  init_height:22, anchor:{x1:1,y1:0,x2:1,y2:0}};
	$("#btn_search").bind("click", function(){x_DAO_Retrieve();});
</script>

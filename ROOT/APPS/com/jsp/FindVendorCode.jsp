﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
/**
 *<pre>
 * Statements
 *</pre>
 
 * @Class Name : FindVendorCode.jsp
 * @Descriptio	  거래처찾기
 * @Modification Information
 * @
 * @  수정일     수정자              수정내용
 * @ -------    --------    ---------------------------
 * @ 2014.12.15    이요한        최초 생성
 *
 */
%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='div_criteria' style='float:left;margin-bottom:5px;line-height:30px;'>
	<!-- div class='detail_label_left w110'>업체구분</div>
	<div id='di_11' class="detail_input_bg w120">                                                                                                 
		<html:select name="S_CORPDIVCD" id="S_CORPDIVCD" comCodeYn="0" codeGroup="rm|SM_COMCODE_D|W_RM0101_01" params="BI|CORPDIVCD|%" dispStyle="0" titleCode="전체"></html:select>
	</div>
	
	<div class='detail_label w110'>업체형태</div>
	<div id='di_12' class='option_input_bg w120' style='vertical-align:middle; border:soild 1px #d2dbbd;'>
		<html:select name="S_CORPTYPECD" id="S_CORPTYPECD" comCodeYn="0" codeGroup="rm|SM_COMCODE_D|W_RM0101_01" params="BI|CORPTYPECD|%" dispStyle="0" titleCode="전체"></html:select>
	</div>
	
	<div class='detail_line'></div -->
	<input id='FIND_CODE' class='FIND_CODE' type='text' style="ime-mode:active;"><button id="btn_search" class="btn_search" aria-label="검색" name="" ><span class="gb_bg gb_mag"></span></button>
<!--	
	<div class='detail_label_left w110'>업체명</div>
	<div id='di_12' class='option_input_bg' style='vertical-align:middle;'>
		<input id='FIND_CODE' type='text' class='detail_input_fix w280' style='margin:4px;'> 
		<img id='btn_search' src='<c:url value="/Theme/images/btn/search.gif"/>' width='42px' height='19px'/>
		<img id='btn_confirm' src='<c:url value="/Theme/images/btn/confirm.gif"/>' width='42px' height='19px'/>
	</div>
-->	
</div>

<div id="grid_1" style='float:left;width:100%;height:655px;'>
	<div id="dg_1" style='float:left;width:100%;height:100%;' class='slick-grid'>
	</div>
</div>

<script type='text/javascript' charset='UTF-8'>
	div_criteria.ResizeInfo = {init_width:1000, init_height:30, width_increase:1, height_increase:0};
	grid_1.ResizeInfo = {init_width:1000, init_height:680, width_increase:1, height_increase:1};
	
	FIND_CODE.ResizeInfo = {init_width:950,  init_height:22, anchor:{x1:1,y1:0,x2:1,y2:0}};
	$("#btn_search").bind("click", function(){x_DAO_Retrieve();});
	//$('#btn_confirm').bind('click', function(){x_Confirm();});
</script>

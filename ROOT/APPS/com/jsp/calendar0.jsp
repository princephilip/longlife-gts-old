<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.text.*" %>
<jsp:include page="/Mighty/include/COMM_JS.jsp"/>

<%
DecimalFormat df = new DecimalFormat("00");
Calendar calendar = Calendar.getInstance();

String curr_yy = Integer.toString(calendar.get(Calendar.YEAR)); //년도를 구한다
String curr_mm = df.format(calendar.get(Calendar.MONTH) + 1); //달을 구한다
String curr_dd = df.format(calendar.get(Calendar.DATE)); //날짜를 구한다
String curr_dy = "";

int iDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK); //요일을 구한다
switch(iDayOfWeek){
  case 1: curr_dy = "일"; break;
  case 2: curr_dy = "월"; break;
  case 3: curr_dy = "화"; break;
  case 4: curr_dy = "수"; break;
  case 5: curr_dy = "목"; break;
  case 6: curr_dy = "금"; break;
  case 7: curr_dy = "토"; break;
}

	String 	year = "";
	String 	month = "";
	String 	day = "";
	int 	lastday=0;
	int 	rowCnt=0;
	int 	startWeek=0;
	String 	calendarHtml = "";

	Calendar today 		= Calendar.getInstance();
    Calendar firstDay 	= Calendar.getInstance();
    Calendar lastDay  	= Calendar.getInstance();

	StringBuffer sb = new StringBuffer(1024);
	year  = request.getParameter("year");
	month = request.getParameter("month");
	day   = request.getParameter("day");

	if(year==null)
		year=Integer.toString(today.get(Calendar.YEAR));
	if(month==null)
		month=Integer.toString(today.get(Calendar.MONTH)+1);
	if(day==null)
		day=Integer.toString(today.get(Calendar.DATE));

    firstDay.set(Integer.parseInt(year),Integer.parseInt(month)-1,1);  	// 해당 월의 다음 달의 첫 날짜를 구한다.
    lastDay.set(Integer.parseInt(year),Integer.parseInt(month),1);  	// 해당 월의 다음 달의 첫 날짜를 구한다.
    lastDay.add(Calendar.DATE,-1); 				// 해당 월의 마지막날을 구한다.

	lastday = lastDay.get(Calendar.DATE);
	if(Integer.parseInt(day)>lastday)
		day=Integer.toString(lastday);

	startWeek = firstDay.get(Calendar.DAY_OF_WEEK);

	if(firstDay.get(Calendar.DAY_OF_WEEK)!=Calendar.SUNDAY)
	{
		sb.append("<TR align=left valign=top>");
		rowCnt++;
		for(int i=1;i<startWeek;i++)
			sb.append("<TD></TD>");
	}
	for(int i=1;i<=lastday;i++)
	{
		Calendar dt = Calendar.getInstance();
    	dt.set(Integer.parseInt(year),Integer.parseInt(month) -1,i);  		// 해당 월의 다음 달의 첫 날짜를 구한다.
		String font1 = (dt.get(Calendar.DAY_OF_WEEK)==Calendar.SUNDAY?"<font color=red>":dt.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY?"<font color=blue>":"");
		String font2 = (dt.get(Calendar.DAY_OF_WEEK)==Calendar.SUNDAY||dt.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY?"</font>":"");
		if(dt.get(Calendar.DAY_OF_WEEK)==Calendar.SUNDAY)
		{
			sb.append("<TR align=left valign=top>");
			rowCnt++;
		}
		sb.append("<TD id='" + "td_day_" + i + "' onclick='SelectionChanged(" + i + ");'>" + font1 + i + font2 + "</TD>");
		if(dt.get(Calendar.DAY_OF_WEEK)==Calendar.SATURDAY)
			sb.append("</TR>");
	}
	if(lastDay.get(Calendar.DAY_OF_WEEK)!=Calendar.SATURDAY)
	{
		for(int i=lastDay.get(Calendar.DAY_OF_WEEK);i<6;i++)
			sb.append("<TD></TD>");
		sb.append("</TR>");
	}
	calendarHtml = sb.toString();

%>

<HTML>
	<HEAD>
		<title>일정조회</title>
	</HEAD>
	<body onload="jf_initial()">
	<table width="100%" height="100%" border="1" bordercolor="#c0c0c0" cellspacing="0" cellpadding="2"
		style="FONT-SIZE: 9pt; FONT-FAMILY: Dotum; BORDER-COLLAPSE: collapse">
		<tr>
			<td height="41" align=left valign=top id="td_remark1">
			</td>
			<td height="41" style="padding-left:15; line-height: 186%;" colspan=5 align=center>
				<select id="selYear" name="selYear" align="absmiddle" style="width:60px" onchange="MonthChanged()">
          <option value=2007>&nbsp;2007&nbsp;</option>
          <option value=2008>&nbsp;2008&nbsp;</option>
          <option value=2009>&nbsp;2009&nbsp;</option>
          <option value=2010>&nbsp;2010&nbsp;</option>
          <option value=2011>&nbsp;2011&nbsp;</option>
          <option value=2012>&nbsp;2012&nbsp;</option>
          <option value=2013>&nbsp;2013&nbsp;</option>
          <option value=2014>&nbsp;2014&nbsp;</option>
          <option value=2015>&nbsp;2015&nbsp;</option>
          <option value=2016>&nbsp;2016&nbsp;</option>
          <option value=2017>&nbsp;2017&nbsp;</option>
          <option value=2018>&nbsp;2018&nbsp;</option>
          <option value=2019>&nbsp;2019&nbsp;</option>
          <option value=2020>&nbsp;2020&nbsp;</option>
				</select>년
				<select id="selMonth" name="selMonth" align="absmiddle" style="width:45px" onchange="MonthChanged()">
					<option value=1 >&nbsp;01&nbsp;</option>
					<option value=2 >&nbsp;02&nbsp;</option>
					<option value=3 >&nbsp;03&nbsp;</option>
					<option value=4 >&nbsp;04&nbsp;</option>
					<option value=5 >&nbsp;05&nbsp;</option>
					<option value=6 >&nbsp;06&nbsp;</option>
					<option value=7 >&nbsp;07&nbsp;</option>
					<option value=8 >&nbsp;08&nbsp;</option>
					<option value=9 >&nbsp;09&nbsp;</option>
					<option value=10 >&nbsp;10&nbsp;</option>
					<option value=11 >&nbsp;11&nbsp;</option>
					<option value=12 >&nbsp;12&nbsp;</option>
				</select>월
				<br />
				오늘날짜 :
				<font style="font-size:10pt;color:#FF8040;font-weight:bold"><%=curr_yy%></font>년&nbsp;
				<font style="font-size:10pt;color:#FF8040;font-weight:bold"><%=curr_mm%></font>월&nbsp;
				<font style="font-size:10pt;color:#FF8040;font-weight:bold"><%=curr_dd%></font>일
				(<font style="font-size:10pt;color:#FF8040;font-weight:bold"><%=curr_dy%></font>)
			</td>
			<td align=center valign=top id="td_remark2">
			</td>
		</tr>

		<tr height="30" align="center" valign="middle" bgcolor="#efeffa">
			<td width="14.2%">일</td>
			<td width="14.3%">월</td>
			<td width="14.3%">화</td>
			<td width="14.3%">수</td>
			<td width="14.3%">목</td>
			<td width="14.3%">금</td>
			<td width="14.3%">토</td>
		</tr>
		<%=calendarHtml%>
	</table>
	</body>

<script type="text/javascript">
	var curYear	= <%=year%>;
	var curMonth= <%=month%>;
	var curDay	= <%=day%>;
	var lastDay	= <%=lastday%>;
	var rowCnt	= <%=rowCnt%>;
	var startWeek = <%=startWeek%>;

	td_day_1.height  = (80/rowCnt) + '%';
	td_day_8.height  = (80/rowCnt) + '%';
	td_day_15.height = (80/rowCnt) + '%';
	td_day_22.height = (80/rowCnt) + '%';
	td_day_28.height = (80/rowCnt) + '%';
	td_day_<%=lastday%>.height = (80/rowCnt) + '%';

	selYear.value = <%=year%>;
	selMonth.value = <%=month%>;

	function jf_initial(){
		if(parent!=null&&typeof(parent.jf_CalendarLoaded)!="undefined"){
			//parent.jf_CalendarLoaded(curYear,curMonth,lastDay);
			parent.jf_CalendarLoaded();
			SelectionChanged(<%=day%>);
		}
	}

	function SelectionChanged(newDay){
		if(parent!=null&&typeof(parent.DateChanged)!="undefined"){
			if(!parent.DateChanged(selYear.value,selMonth.value,newDay,"N"))
				return;
		}
		document.getElementById("td_day_"+curDay).bgColor="";
		document.getElementById("td_day_"+newDay).bgColor="#CCCCCC";   //document.getElementById("td_day_"+newDay).bgColor="#fafaef";
		curDay = newDay;
	}

	function SetData(day, data){
		if(document.getElementById("td_day_"+day)!=null)
		{
			document.getElementById("td_day_"+day).innerHTML= GetDayHtml(day) + '<br>' + data;
		}
	}

	function GetDayHtml(day){
		var preHtml = document.getElementById("td_day_"+day).innerHTML;
		if(preHtml.indexOf("<br>")>0)
			return preHtml.substring(0,preHtml.indexOf("<br>"));
		else
			return preHtml;
	}

	function MonthChanged(){
		if(parent!=null&&typeof(parent.DateChanged)!="undefined"){
			if(!parent.DateChanged(selYear.value,selMonth.value,'1',"Y"))
				return;
		}

		document.location.href="calendar0.jsp?year="+selYear.value+"&month="+selMonth.value+"&day=01";
	}

	function FrmReload(a_year, a_month){
		document.location.href="calendar0.jsp?year="+a_year+"&month="+a_month+"&day=01";
	}

	function SetRemark1(txt)
	{
		td_remark1.innerHTML = txt;
	}

	function SetRemark2(txt)
	{
		td_remark2.innerHTML = txt;
	}


</script>


</HTML>


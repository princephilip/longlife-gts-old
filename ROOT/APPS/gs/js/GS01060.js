//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : 수급자 청구 현황
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
function x_InitForm2(){
	if(!_X.IsHeadoffice())	{
		$("#btn_finddept").hide();
	}

	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "gs", "GS01060", "GS01060|GS01060_R01", false, false);
	_X.InitGrid(grid_2, "dg_2", "100%", "100%", "gs", "GS01060", "GS01060|GS01060_C02", false, true);

	var la_DEPT = _X.XmlSelect("com", "COMMON", "CODE_DEPT_AUTHORITY_R01" , new Array(mytop._UserID), 'json2');
	_X.DDLB_SetData(S_DEPT_ID, la_DEPT, null, true, (_X.IsHeadoffice() ? true : false));
	S_DEPT_ID.value = mytop._DeptCode;

}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {

		var la_FAMILY_YN = [{"code":"Y","label":"예"},{"code":"N","label":"아니오"}];
		dg_2.SetCombo("FAMILY_YN", la_FAMILY_YN);

		var la_CUST_LEVEL_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0125", 'Y'), 'json2');
		dg_2.SetCombo("CUST_LEVEL_DIV", la_CUST_LEVEL_DIV);

		var la_APP_RATE_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0111", 'Y'), 'json2');
		dg_2.SetCombo("APP_RATE_DIV", la_APP_RATE_DIV);

		var la_SERVICE_YN = [{"code":"1","label":" "},{"code":"0","label":"미이용"}];
		dg_2.SetCombo("SERVICE_YN", la_SERVICE_YN);

		//dg_1.SetGroup(["DEPT_NAME"], true, [{sortAsc: true, sortCol: {field: "DEPT_NAME"}},{sortAsc: true, sortCol: {field: "CUST_NAME"}}]);

		setTimeout("x_DAO_Retrieve()",100);
	}
}

function x_DAO_Retrieve2(a_dg){
  uf_Service_Account_Create();

	var ls_ym   = _X.StrPurify(sle_basicmonth.value);
	var ls_cust = "%"+ S_CUST_NAME.value + "%";

 	dg_1.Retrieve([S_DEPT_ID.value, ls_ym, ls_cust]);
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id) {
		case	"S_DEPT_ID":
		case	"sle_basicmonth":
		case	"S_CUST_NAME":
					x_DAO_Retrieve();
					break;
		case "S_RANGE": //비율변경
					var ll_left  = Number(a_val);
					var ll_right = 1000 - Number(a_val);

					grid_1.ResizeInfo = {init_width:ll_left,  init_height:660, width_increase:0, height_increase:1};
					grid_2.ResizeInfo = {init_width:ll_right, init_height:660, width_increase:1, height_increase:1};
					xm_BodyResize(true);
					break;
	}
}


function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

//저장후 호출
function x_DAO_Saved2(){
	var ls_deptid = dg_1.GetItem(dg_1.GetRow(),'DEPT_ID');
	var ls_yyyymm = dg_1.GetItem(dg_1.GetRow(),'YYYYMM');
	var li_custid = dg_1.GetItem(dg_1.GetRow(),'CUST_ID');

  dg_2.Retrieve([ls_deptid, ls_yyyymm, li_custid]);
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	return 100;
}

function x_DAO_Excel2(a_dg){
	dg_1.ExcelExport(true);

	return 0;
	return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
	if(a_dg.id == 'dg_1') {
		var ls_deptid = dg_1.GetItem(a_newrow,'DEPT_ID');
		var ls_yyyymm = dg_1.GetItem(a_newrow,'YYYYMM');
		var li_custid = dg_1.GetItem(a_newrow,'CUST_ID');

	  dg_2.Retrieve([ls_deptid, ls_yyyymm, li_custid]);
	}
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
	if(_FindCode.returnValue == null) return;

	switch(_FindCode.finddiv) {
		case	"S_DEPT_ID" :
					S_DEPT_ID.value = _FindCode.returnValue.DEPT_ID;
					x_DAO_Retrieve();
					break;
	}
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

// 지사찾기
function uf_findDept() {
	_X.FindDeptCode2('S_DEPT_ID','');
}

function uf_Service_Account_Create() {
  var ls_dept = S_DEPT_ID.value;   // 지사구분
  var ls_date = _X.StrPurify(sle_basicmonth.value);

  _X.EP("gs", "PR_SERVICE_CALCULATOR", [ls_dept, ls_date]);
}

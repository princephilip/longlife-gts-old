//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : 월별일정변경(주야간)
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
var is_client_div = 'O';	//I:매입, O:매출, J:지사, E:기타
var is_date = "";
is_psep_div = "S"
var is_yymm = "";  // 조회 년월
var is_deptcust = 'Y';    //Y:지사별고객찾기  , N:전체고객찾기
var is_cust_id ='';
var is_cust_name = '';
var is_manage_no = '';
var is_cust_level = '';
var is_cust_limit_amt = '0'; // 등급별 월 한도금액
var is_app_rate = '';
var is_reduction = '';
var is_dept_id = '';
var is_curYear = "";
var is_curMonth = "";
var is_cont_seq = "0"; // 계약순번
var is_ini_product_div = "4";  // 주야간 코드
//var is_amt_bath = "2300"; // 목욕비
var is_amt_meal = "2500"; // 식대
var is_move_code = "";  // 5km 미만 이동코드
var is_bath_code = "";  // 목욕코드

var ar_move;	//이동서비스명
var ar_bath;	//목욕서비스명

function x_InitForm2(){
	if(!_X.IsHeadoffice())	{
		$("#btn_finddept").hide();
	}

	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "gs", "GS01050", "GS01050|GS01050_C01", false,  true);

	//지사명
	var la_DEPT = _X.XmlSelect("com", "COMMON", "CODE_DEPT_AUTHORITY_R01" , new Array(mytop._UserID), 'json2');
	_X.DDLB_SetData(S_DEPT_ID, la_DEPT, null, true, false);
	S_DEPT_ID.value = mytop._DeptCode;

	is_date=_X.ToString(_X.GetSysDate(),'yyyy-mm-dd');
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {

		var ls_family_ref = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0116", 'Y'), 'json2');
		_X.DDLB_SetData(FAMILY_REF, ls_family_ref, "", false, true, "");

		ar_move = _X.XmlSelect("com", "COMMON", "DDLB_CODE_PRODUCT2" , new Array("7", 'Y', 'Z10%'), 'json2');
		_X.DDLB_SetData(PRODUCT_CODE_MOVE, ar_move, null, false, false);

		ar_bath = _X.XmlSelect("com", "COMMON", "DDLB_CODE_PRODUCT2" , new Array("7", 'Y', 'Z20%'), 'json2');
		_X.DDLB_SetData(PRODUCT_CODE_BATH, ar_bath, null, false, false);

		// 이동, 목욕서비스 초기값 가져오기..
		var ls_move_code = _X.XmlSelect("gs", "GS01050", "GET_CODE_PRODUCT_INI", [], "array");
		is_move_code = ls_move_code[0][0];
		is_bath_code = ls_move_code[0][1];
		is_amt_meal  = ls_move_code[0][2];

		var ls_product_div = _X.XmlSelect("com", "COMMON", "CODE_COMDIV4" , new Array("0128", 'Y', '4', '9', ''), 'json2');
		_X.DDLB_SetData(PRODUCT_DIV, ls_product_div, null, false, false);

		DateChanged(_X.ToString(_X.GetSysDate(),'yyyy'),_X.ToInt(_X.ToString(_X.GetSysDate(),'mm')),_X.ToInt(_X.ToString(_X.GetSysDate(),'dd')),'Y')

		xe_EditChanged(PRODUCT_DIV, PRODUCT_DIV.value);

		if(_params != "null"){
			var ls_par = new Array();
			ls_par = _params.split("@");

		  is_cust_id   = ls_par[0];
		  is_cust_name = ls_par[1];
			is_dept_id   = ls_par[2];
		  S_DEPT_ID.value = is_dept_id;
		  S_CUST_ID.value = is_cust_id;
		  S_CUST_NAME.value = is_cust_name;


			x_DAO_Retrieve();

		  //is_cont_seq = _GS.GetMaxContSeq(is_cust_id, S_DEPT_ID.value, is_yymm);
		  is_cont_seq = _GS.GetMaxContSeq(is_cust_id, '%', is_yymm);
		  
			_params = null;
		}

		//시간 00으로 세팅
		uf_form_schedule_clear();
	}
}

function x_DAO_Retrieve2(a_dg){
	if (!is_cust_id) {
		_X.MsgBox('고객을 검색하세요.');
		uf_findCust();
		return 0;
	}

	var ls_yy = $("#form_calendar").contents().find("#selYear").val();
	var ls_mm = $("#form_calendar").contents().find("#selMonth").val();

	is_yymm = ls_yy + _X.LPad(ls_mm, 2, '0');
	dg_1.Retrieve([S_DEPT_ID.value, is_cust_id, is_yymm]);
	//uf_form_schedule_clear();

	//$('#form_calendar')[0].contentWindow.uf_bg_color();
}

function uf_Set_product_code() {
	var ls_div = PRODUCT_DIV.value;
	var ls_product_code = _X.XmlSelect("com", "COMMON", "DDLB_CODE_PRODUCT2" , new Array(ls_div, 'Y', '%'), 'json2');
	_X.DDLB_SetData(PRODUCT_CODE, ls_product_code, null, false, false);

	if(ls_div == "9") {
		$("#dl_product").hide();
		$("#dl_etcamt1").show();
		$("#dl_etcamt2").show();
		$("#dl_etcamt3").show();
	} else {
		$("#dl_product").show();
		$("#dl_etcamt1").hide();
		$("#dl_etcamt2").hide();
		$("#dl_etcamt3").hide();
	}
}

function xe_EditChanged2(a_obj, a_val){
	if(a_obj.id == "FAMILY_YN") {
		if(FAMILY_YN.checked && FAMILY_REF.value == "") {
			FAMILY_REF.value = "0010";
		}
		if(!FAMILY_YN.checked && FAMILY_REF.value != "") {
			FAMILY_REF.value = "";
		}
	}

	switch(a_obj.id) {
		case	"PRODUCT_DIV":
					uf_Set_product_code();
		case	"START_TIME_DIV":
		case	"START_MIN_DIV":
		case	"END_TIME_DIV":
		case	"END_MIN_DIV":
		case	"MEMBER_ID":
		case	"MEMBER_ID2":
		case	"PRODUCT_CODE":
		case	"SERVICE_YN":
		case	"FAMILY_YN":
		case	"FAMILY_REF":
					var ls_div   = PRODUCT_DIV.value;
					var ls_stime = _X.LPad(_X.ToInt(START_TIME_DIV.value),2,'0');
					var ls_smin  = _X.LPad(_X.ToInt(START_MIN_DIV.value),2,'0');
					var ls_etime = _X.LPad(_X.ToInt(END_TIME_DIV.value),2,'0');
					var ls_emin  = _X.LPad(_X.ToInt(END_MIN_DIV.value),2,'0');

					var ls_service_yn = SERVICE_YN.checked  ? "0" : "1";
					var ls_svc_yn  = SVC_YN.checked  ? "Y" : "N";
					var ls_move_yn = MOVE_YN.checked ? "Y" : "N";
					var ls_bath_yn = BATH_YN.checked ? "Y" : "N";
					var ls_meal_yn = MEAL_YN.checked ? "Y" : "N";

					if(!(ls_stime>=0&&ls_stime<=23)||!(ls_etime>=0&&ls_etime<=23)) {
						_X.MsgBox('시간은 0에서 23사이로 입력해야 합니다.');
						return;
					}

					if(!(ls_smin>=0&&ls_smin<=59)||!(ls_emin>=0&&ls_emin<=59)) {
						_X.MsgBox('분은 0에서 59사이로 입력해야 합니다.');
						return;
					}

  				switch(ls_div) {
  					case '4':		//야간보호일 경우
		  					if(a_obj.id == "PRODUCT_CODE") return;	//서비스코드 바뀔시 아무것도 하지 않음
								if(ls_stime && ls_smin && ls_etime && ls_emin) {
									//가족케어여부
									var ls_family_yn = 'N';
									var ls_member_id = 'N';
									var ll_service_cost = 0;
									var ls_date = "20171228";	//휴일 아닐 날짜 그냥 지정
									var ls_date = is_yymm + "01";
									var ls_product_id = 0;
					    		var ls_product_div = ls_div;
									var ls_cust_id = is_cust_id;

									//기타급여일때 product_div 변경
									if(ls_svc_yn=="Y") {
									  var ls_level_div = _X.XmlSelect("gs", "GS01050", "GET_CUST_LEVEL_DIV", [ls_cust_id], "array")[0][0];	//장기요양등급
						    		if(ls_level_div != '1' && ls_level_div != '2' && ls_level_div != '3' && ls_level_div != '4') ls_level_div = '5';	//1,2,3,4 등급을 제외한 나머지는 5등급 처리

									  var ls_result = _X.XmlSelect("gs", "GS01050", "GET_SERVICEAMT", [ls_date, ls_stime, ls_smin, ls_etime, ls_emin, ll_service_cost, ls_family_yn, ls_product_id, ls_product_div, ls_level_div, ls_service_yn], "array");

									  if (ls_result.length > 0) {
									  	var la_rtn = ls_result[0][0].split('|');
									    var ll_service_amt   = _X.ToInt(la_rtn[0]);
									    var ll_service_min_1 = _X.ToInt(la_rtn[1]);
									    var ll_service_min_2 = _X.ToInt(la_rtn[2]);
									    var ll_service_min_3 = _X.ToInt(la_rtn[3]);
									    var ll_service_amt_1 = _X.ToInt(la_rtn[4]);
									    var ll_service_amt_2 = _X.ToInt(la_rtn[5]);
									    var ll_service_amt_3 = _X.ToInt(la_rtn[6]);

									    MIN_1.value    = ll_service_min_1;
				   						MIN_2.value    = ll_service_min_2;
				   						MIN_3.value    = ll_service_min_3;
									    MIN_TOT.value  = (ll_service_min_1 + ll_service_min_2 + ll_service_min_3)/60;
				   						AMT_1.value    = ll_service_amt_1;
				   						AMT_2.value    = ll_service_amt_2;
				   						AMT_3.value    = ll_service_amt_3;
				   						AMT_TOT.value  = ll_service_amt_1 + ll_service_amt_2 + ll_service_amt_3;

									    PRODUCT_ID.value   = _X.ToInt(la_rtn[7]);
									    PRODUCT_CODE.value = la_rtn[8];
									    PRODUCT_NAME.value = la_rtn[9];

									    if(typeof(la_rtn[8]) == "undefined") {
										    PRODUCT_ID.value   = "";
										    PRODUCT_CODE.value = "";
										    PRODUCT_NAME.value = "";
									    }
									    SERVICE_AMT.value  = ll_service_amt;
									    SERVICE_MIN.value  = ll_service_min_1 + ll_service_min_2 + ll_service_min_3;
				  					}
									} else {
										ls_product_div = '7';

										if(ls_stime && ls_smin && ls_etime && ls_emin) {
											var li_tot_min = 0;
											if((_X.ToInt(ls_etime) * 60 + _X.ToInt(ls_emin)) - (_X.ToInt(ls_stime) * 60 + _X.ToInt(ls_smin)) >= 0) {
												li_tot_min = (_X.ToInt(ls_etime) * 60 + _X.ToInt(ls_emin)) - (_X.ToInt(ls_stime) * 60 + _X.ToInt(ls_smin));
											} else {
												li_tot_min = ((_X.ToInt(ls_etime) + 24) * 60 + _X.ToInt(ls_emin)) - (_X.ToInt(ls_stime) * 60 + _X.ToInt(ls_smin));
											}
									    MIN_1.value    = li_tot_min;
				   						MIN_2.value    = 0;
				   						MIN_3.value    = 0;
									    MIN_TOT.value  = li_tot_min / 60;
				   						AMT_TOT.value  = SERVICE_AMT.value;

				   						AMT_1.value    = SERVICE_AMT.value;
				   						PAY_AMT.value  = SERVICE_AMT.value;
				   						REQ_AMT.value  = 0;

				   						SERVICE_MIN.value = li_tot_min;
				  					}
									}
								}
								break;
						case '5':		//단기보호일 경우
						case '6':		//시설급여
								//서비스 코드가 바뀔대 수가 변경
								if(a_obj.id == "PRODUCT_CODE") {
								  var ls_result = _X.XmlSelect("gs", "GS01050", "CODE_PRODUCT_R01", [ls_div, a_val, _X.ToString(is_date,'YYYYMMDD')], "array");

							    MIN_1.value    = 0;
		   						MIN_2.value    = 0;
		   						MIN_3.value    = 0;
		   						MIN_TOT.value  = 0;

							    AMT_1.value    = _X.ToInt(ls_result[0][3]);
		   						AMT_2.value    = 0;
		   						AMT_3.value    = 0;
		   						AMT_TOT.value  = _X.ToInt(ls_result[0][3]);

							    PRODUCT_ID.value   = _X.ToInt(ls_result[0][0]);
							    PRODUCT_CODE.value = ls_result[0][1];
							    PRODUCT_NAME.value = ls_result[0][2];
							    SERVICE_AMT.value  = _X.ToInt(ls_result[0][3]);
								}
								break;
						case '7':		//기타급여일 경우
		  					if(a_obj.id == "PRODUCT_CODE") return;	//서비스코드 바뀔시 아무것도 하지 않음
								if(ls_stime && ls_smin && ls_etime && ls_emin) {
									var li_tot_min = 0;
									if((_X.ToInt(ls_etime) * 60 + _X.ToInt(ls_emin)) - (_X.ToInt(ls_stime) * 60 + _X.ToInt(ls_smin)) >= 0) {
										li_tot_min = (_X.ToInt(ls_etime) * 60 + _X.ToInt(ls_emin)) - (_X.ToInt(ls_stime) * 60 + _X.ToInt(ls_smin));
									} else {
										li_tot_min = ((_X.ToInt(ls_etime) + 24) * 60 + _X.ToInt(ls_emin)) - (_X.ToInt(ls_stime) * 60 + _X.ToInt(ls_smin));
									}

							    MIN_1.value    = li_tot_min;
		   						MIN_2.value    = 0;
		   						MIN_3.value    = 0;
		   						MIN_TOT.value  = li_tot_min;
		   						AMT_TOT.value  = SERVICE_AMT.value;

		   						AMT_1.value    = SERVICE_AMT.value;
		   						PAY_AMT.value  = SERVICE_AMT.value;
		   						REQ_AMT.value  = 0;

		   						SERVICE_MIN.value = li_tot_min;
		  					}
								break;
					}
					break;
		case	"SERVICE_AMT":
					var ls_div   = PRODUCT_DIV.value;
					if(ls_div == '7') {			//기타급여일 경우
						AMT_1.value   = _X.ToInt(a_val);
						AMT_TOT.value = _X.ToInt(a_val);
						PAY_AMT.value = _X.ToInt(a_val);
						REQ_AMT.value = 0;
					}
					break;
		case	"PRODUCT_CODE_MOVE": 	// 이동서비스
					var ls_move_amt = ar_move[PRODUCT_CODE_MOVE.selectedIndex].sale_amt;
					MOVE_YN.checked = true;
					AMT_MOVE.value  = ls_move_amt;
					break;
		case	"PRODUCT_CODE_BATH": 	// 목욕
					var ls_bath_amt = ar_bath[PRODUCT_CODE_BATH.selectedIndex].sale_amt;
					BATH_YN.checked = true;
					AMT_BATH.value  = ls_bath_amt;
					break;
	}
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

//저장후 호출
function x_DAO_Saved2(){
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
	if(a_dg.id == 'dg_1') {
		dg_1.SetItem(rowIdx, "REQ_DATE",  _X.ToDate(is_date));
	}
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
	if (!is_cust_id) {
		_X.MsgBox('고객을 검색하세요.');
		uf_findCust();
		return 0;
	}

	var ls_month = _X.ToString(is_date,'YYYYMM')

	if( rb_print_0.checked ){
		_X.SetiReport("gs", "GS01040R_A",	"as_dept="   		+ S_DEPT_ID.value
																		+ "&as_yyyymm="  	+ ls_month
																		+ "&as_cust="   	+ is_cust_id
																		+ "&as_printtag="	+ 'CUSTPRINT' );
	}else{
		var ls_memberid = "";
		var as_maxseq = _X.XS("gs", "GS01050", "MAX_SEQ" , [S_DEPT_ID.value, is_cust_id, ls_month], 'Array')[0][0];
		var ls_print  = "CUSTPRINT";
	  var ls_depositediv = "0200";
	  var ls_print_div = "A";			//A:전체일정, 1:청구일정, 2:기타(급여)

	  _X.SetiReport("gs", "GS01040R_SB0",	"as_dept="   		 + S_DEPT_ID.value
																			+ "&as_month="  	 + ls_month
																			+ "&as_cust="   	 + is_cust_id
																			+ "&as_seq="   		 + as_maxseq
																			+ "&as_memberid="  + is_cust_id //dg_1.GetItem(dg_1.GetRow(),'MEMBER_ID')
																			+ "&as_printtag="  + ls_print
																			+ "&as_print_div=" + ls_print_div
																			+ "&as_deposite="	 + ls_depositediv );
	}

	/*if(rb_print_1.checked) {

	  var ls_memberid = "";

	  var ls_seq   = ptdwo.SqlSelect("gs","SELECT Max(Cont_Seq) FROM Service_Schedule WHERE dept_id='" + S_DEPT_ID.value + "' AND cust_id=" + is_cust_id + " AND PLAN_DATE BETWEEN to_date('" + ls_month + "01','YYYYMMDD') AND last_day(to_date('" + ls_month + "01','YYYYMMDD')) ");

	  var ls_print  = "CUSTPRINT";
	  var ls_depositediv = "0200";
	  var ls_print_div = "A";


	  ptdwo.SetReport('gs','dr_service_contract_cal_main',false,true);
	 	ptdwo.RptRetrieve(S_DEPT_ID.value, ls_month, is_cust_id,ls_seq,ls_memberid,ls_print,ls_depositediv, ls_print_div);
	} else {
		ptdwo.SetReport('gs','dr_service_contract_cal_hor',false);
	 	ptdwo.RptRetrieve(S_DEPT_ID.value, ls_month, is_cust_id, 'CUSTPRINT');
	}*/
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
  // if (a_dg.id == 'dg_1') {
		// uf_SetColVisible(dg_1.GetItem(a_newrow, "DEPT_DIV"));

		// ii_form_seq = dg_1.GetItem(a_newrow, "FORM_SEQ");
		// dg_2.Retrieve([ii_form_seq]);
  // }
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
	if (a_dg.id == 'dg_1') {
		var li_rtn = _X.EP('gs','PR_GS_SERVICE_SUMMARY', [is_cust_id, S_DEPT_ID.value, _X.ToString(is_date,'YYYYMM')]);
		if(li_rtn<0){
			_X.MsgBox("서비스 집계 중, 오류가 발생했습니다.");
			return;
		}

		// 일정표 그리기
		setTimeout("jf_CalendarLoaded()", 200);
	}
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
	if(_FindCode.returnValue == null) return;

	switch(_FindCode.finddiv) {
		case	"S_DEPT_ID" :
					// var ls_yy = $("#form_calendar").contents().find("#selYear").val();
					// var ls_mm = $("#form_calendar").contents().find("#selMonth").val();
					// $('#form_calendar')[0].contentWindow.FrmReload(ls_yy, ls_mm);

					S_DEPT_ID.value = _FindCode.returnValue.DEPT_ID;
					S_CUST_NAME.value = "";
					break;
		case	"S_CUST_NAME" :
					$('#form_calendar')[0].contentWindow.chkAllCheck(false);
					$('#form_calendar')[0].contentWindow.CheckAll(false);

				  is_cust_id        = _FindCode.returnValue.CUST_ID;
				  is_cust_name      = _FindCode.returnValue.CUST_NAME;
				  is_cust_level     = _FindCode.returnValue.CUST_LEVEL_NAME;
				  is_cust_limit_amt = _FindCode.returnValue.M_LIMIT_AMT;
				  is_reduction      = _FindCode.returnValue.REDUCTION_NAME;
				  is_app_rate       = _FindCode.returnValue.APP_RATE_NAME;
				  is_manage_no      = _FindCode.returnValue.COPY_MANAGE_NO;
				  S_CUST_ID.value   = is_cust_id;
				  S_CUST_NAME.value = is_cust_name;

					uf_form_schedule_clear();
					//jf_CalendarLoaded();
					x_DAO_Retrieve();

					//is_cont_seq = _GS.GetMaxContSeq(is_cust_id, S_DEPT_ID.value, is_yymm);
					is_cont_seq = _GS.GetMaxContSeq(is_cust_id, '%', is_yymm);
					if(is_cont_seq=="0") {
						_X.MsgBox("등록된 계약이 없습니다. 계약을 먼저 생성해야 합니다.");
					}
					break;
		case	"MEMBER_ID" :
					MEMBER_ID.value    = _FindCode.returnValue.MEMBER_ID;
					MEMBER_NAME.value  = _FindCode.returnValue.MEMBER_NAME;
					break;
		case	"MEMBER_ID2" :
					MEMBER_ID2.value   = _FindCode.returnValue.MEMBER_ID;
					MEMBER_NAME2.value = _FindCode.returnValue.MEMBER_NAME;
					break;
	}
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

// 지사찾기
function uf_findDept() {
	_X.FindDeptCode2('S_DEPT_ID','');
}

// 고객찾기
function uf_findCust() {
	var ls_yy = $("#form_calendar").contents().find("#selYear").val();
	var ls_mm = $("#form_calendar").contents().find("#selMonth").val();

	var ls_yymm = ls_yy + _X.LPad(ls_mm, 2, '0');
	_X.CommonFindCode(window,"지사별 고객 찾기","com",'S_CUST_NAME','',[S_DEPT_ID.value, ls_yymm],"FindCustId|FindCode|FIND_CODE_CUST2",700, 700);
}

// 요양보호사찾기
function uf_findMember(a_div) {
	var as_gubun  = '0205';  //요양보호사
	_X.CommonFindCode(window,"요양보호사 찾기","com",a_div,'',[S_DEPT_ID.value, as_gubun],"FindCodeMember|FindCode|FIND_CODE_MEMBER",700, 700);
}

// 서비스일정 초기화
function uf_form_schedule_clear() {
	START_TIME_DIV.value = '00';
	START_MIN_DIV.value  = '00';
	END_TIME_DIV.value   = '00';
	END_MIN_DIV.value    = '00';
	PRODUCT_DIV.value    = is_ini_product_div;

	AMT_1.value          = '';
	AMT_2.value          = '';
	AMT_3.value          = '';
	AMT_TOT.value        = '';
	MIN_1.value          = '';
	MIN_2.value          = '';
	MIN_3.value          = '';
	MIN_TOT.value        = '';
	MIN_REAL.value       = '';
	MEMBER_ID.value      = '';
	MEMBER_NAME.value    = '';
	MEMBER_ID2.value     = '';
	MEMBER_NAME2.value   = '';
	FAMILY_YN.checked    = false;
	FAMILY_REF.value     = '';
	SERVICE_AMT.value    = '';

	SVC_YN.checked       = true;	// Y/N
	MOVE_YN.checked      = true;	// Y/N
	BATH_YN.checked      = true;	// Y/N
	MEAL_YN.checked      = true;	// Y/N
	SERVICE_YN.checked   = false;	// 0/1

	PRODUCT_CODE_MOVE.value    = is_move_code;
	PRODUCT_CODE_BATH.value    = is_bath_code;
	AMT_MEAL.value       = is_amt_meal;

	AMT_MOVE.value       = ar_move[PRODUCT_CODE_MOVE.selectedIndex].sale_amt;
	AMT_BATH.value       = ar_bath[PRODUCT_CODE_BATH.selectedIndex].sale_amt;

	AMT_ETC.value        = '';
	ONEWAY_YN.checked    = false;
	PAY_AMT.value        = '';
	REQ_AMT.value        = '';
	REMAIN_AMT.value     = '';
	OFFICE_AMT.value     = '';

	uf_Set_product_code();
}

function DateChanged(curYear,curMonth,curDay,curinit){

  // if(jf_dw_updateconfirm()==3) {
  // 	fdw_list.selYear.value = is_curYear;
  // 	fdw_list.selMonth.value = is_curMonth;
  // 	return false;
  // }
  is_date = curYear + "-" + _X.LPad(curMonth,2,'0') + "-" + _X.LPad(curDay,2,'0');
  is_yymm = curYear + _X.LPad(curMonth,2,'0');

  // 고객 선택되어 있을 때, 조회
   //_X.MsgBox(curMonth + " / " +is_cust_id)
	is_curYear  = curYear;
	is_curMonth = curMonth;

	if (is_cust_id) {
		x_DAO_Retrieve();
	}

	return true;
}

// 달력콘트롤에 일자별 자료를 세팅한다.(월전체)
function jf_CalendarLoaded(lastDay){
	if(is_cust_id==null || is_cust_id=="")
		return;

	var ls_result = _X.XmlSelect("gs", "GS01050", "GET_SERVICESCHEDULE", [S_DEPT_ID.value, is_cust_id, is_yymm], "array");

	if (ls_result.length>0) {
		for(var i=1; i<=ls_result.length; i++) {
			$('#form_calendar')[0].contentWindow.SetData(i,ls_result[i-1][1]);
		}
	}

	// 달력우측
	var ls_results = _X.XmlSelect("gs", "GS01050", "SERVICE_SUMMARY_SUM_R01", [S_DEPT_ID.value, is_cust_id, is_yymm], "array");
	var ls_sumtot  = _X.XmlSelect("gs", "GS01050", "SERVICE_SUMMARY_SUM_R02", [S_DEPT_ID.value, is_cust_id, is_yymm], "array");

	var ls_total = [0, 0, 0, 0, 0];
	ls_total[0] = _X.ToInt(ls_sumtot[0][0]);
	ls_total[1] = _X.ToInt(ls_sumtot[0][1]);
	ls_total[2] = _X.ToInt(ls_sumtot[0][2]);
	ls_total[3] = _X.ToInt(ls_sumtot[0][3]);
	ls_total[4] = _X.ToInt(ls_sumtot[0][4]);

	var info = "";
	if(ls_results!=null && ls_results!="")
	{
		var ls_amt1 = ["0", "0", "0", "0", "0"];
		var ls_amt2 = ["0", "0", "0", "0", "0"];
		var ls_amt3 = ["0", "0", "0", "0", "0"];
		var ls_amt4 = ["0", "0", "0", "0", "0"];
		var ls_amt5 = ["0", "0", "0", "0", "0"];
		var ls_amt6 = ["0", "0", "0", "0", "0"];
		//var ls_total = [0, 0, 0, 0, 0];

		for(var i in ls_results) {
			var ls_amt = [_X.FormatComma(ls_results[i][1]), _X.FormatComma(ls_results[i][2]), _X.FormatComma(ls_results[i][3]), _X.FormatComma(ls_results[i][4]), _X.FormatComma(ls_results[i][5])];
			// ls_total[0] += _X.ToInt(ls_results[i][1]);
			// ls_total[1] += _X.ToInt(ls_results[i][2]);
			// ls_total[2] += _X.ToInt(ls_results[i][3]);
			// ls_total[3] += _X.ToInt(ls_results[i][4]);
			// ls_total[4] += _X.ToInt(ls_results[i][5]);

			switch(ls_results[i][0]) {
				case "주야" :
					ls_amt1 = ls_amt;
					break;
				case "주야(미)" :
					ls_amt2 = ls_amt;
					break;
				case "이동" :
					ls_amt3 = ls_amt;
					break;
				case "목욕" :
					ls_amt4 = ls_amt;
					break;
				case "식비" :
					ls_amt5 = ls_amt;
					break;
				case "등급외" :
					ls_amt6 = ls_amt;
					break;
			}
		}

		info = '<table border="0" cellspacing="0" cellpadding="0" width="100%" class="summary_total" >'
				 + '<col width="10%" align="center" /><col width="18%" align="right" style="padding-right: 6px" /><col width="18%" align="right" /><col width="18%" align="right" /><col width="18%" align="right" /><col width="18%" align="right" />'
				 + '<tr><th>구분</th><th>수가합계</th><th>공단청구</th><th>본인부담</th><th>초과+비급여</th><th>본인부담계</th></tr>'
				 + '<tr><td style="text-align:center">주야</td><td>'+ls_amt1[0]+'</td><td>'+ls_amt1[1]+'</td><td>'+ls_amt1[2]+'</td><td>'+ls_amt1[3]+'</td><td>'+ls_amt1[4]+'</td></tr>'
				 + '<tr><td style="text-align:center">주야(미)</td><td>'+ls_amt2[0]+'</td><td>'+ls_amt2[1]+'</td><td>'+ls_amt2[2]+'</td><td>'+ls_amt2[3]+'</td><td>'+ls_amt2[4]+'</td></tr>'
				 + '<tr><td style="text-align:center">이동</td><td>'+ls_amt3[0]+'</td><td>'+ls_amt3[1]+'</td><td>'+ls_amt3[2]+'</td><td>'+ls_amt3[3]+'</td><td>'+ls_amt3[4]+'</td></tr>'
				 + '<tr><td style="text-align:center">목욕</td><td>'+ls_amt4[0]+'</td><td>'+ls_amt4[1]+'</td><td>'+ls_amt4[2]+'</td><td>'+ls_amt4[3]+'</td><td>'+ls_amt4[4]+'</td></tr>'
				 + '<tr><td style="text-align:center">식비</td><td>'+ls_amt5[0]+'</td><td>'+ls_amt5[1]+'</td><td>'+ls_amt5[2]+'</td><td>'+ls_amt5[3]+'</td><td>'+ls_amt5[4]+'</td></tr>'
				 + '<tr><td style="text-align:center">등급외</td><td>'+ls_amt6[0]+'</td><td>'+ls_amt6[1]+'</td><td>'+ls_amt6[2]+'</td><td>'+ls_amt6[3]+'</td><td>'+ls_amt6[4]+'</td></tr>'
				 + '<tr><td class="total_td" style="text-align:center">합계</td><td class="total_td">'+_X.FormatComma(ls_total[0])+'</td><td class="total_td">'+_X.FormatComma(ls_total[1])+'</td><td class="total_td">'+_X.FormatComma(ls_total[2])+'</td><td class="total_td">'+_X.FormatComma(ls_total[3])+'</td><td class="total_td">'+_X.FormatComma(ls_total[4])+'</td></tr>'
				 + '</table>';
	}

	// 달력좌측
	var ls_results = _X.XmlSelect("gs", "GS01050", "SERVICE_SUMMARY_SUM_R03", [S_DEPT_ID.value, is_cust_id, is_yymm], "array");

	var info2 = "";
	if(ls_results!=null && ls_results!="") {
    var li_b     = 0;
    var li_total = 0;
    var li_cnt   = 0;

		info2 = '<table border="0" cellspacing="0" cellpadding="0" class="summary_total" >'
				 + '<tr><td class="htitle">종류</td><td class="htitle">시간</td><td class="htitle">유형</td>	<td class="htitle">수가</td><td class="htitle">횟수</td><td class="htitle">비급여</td>	<td class="htitle">총수가</td></tr>';
		for(var i in ls_results) {
			info2	+=	'<tr><td class="detldata">&nbsp;'+ls_results[i][0]+'&nbsp;</td>'
						+		'<td class="detldata">&nbsp;'+ls_results[i][1]+'&nbsp;</td>'
						+		'<td class="detldata">&nbsp;'+ls_results[i][2]+'&nbsp;</td>'
						+		'<td class="detldata">&nbsp;'+_X.FormatComma(ls_results[i][3])+'&nbsp;</td>'
						+		'<td class="detldata">&nbsp;'+ls_results[i][4]+'&nbsp;</td>'
						+		'<td class="detldata">&nbsp;'+_X.FormatComma(ls_results[i][5])+'&nbsp;</td>'
						+		'<td class="detldata">&nbsp;'+_X.FormatComma(ls_results[i][6])+'&nbsp;</td></tr>';

	    li_cnt   += _X.ToInt(ls_results[i][4]);
	    li_b     += _X.ToInt(ls_results[i][5]);
	    li_total += _X.ToInt(ls_results[i][6]);
		}
		li_b = Math.round(li_b/10) * 10;
		li_total = Math.round(li_total/10) * 10;
		info2 +=	'<tr><td colspan="4" class="htitle">합계</td>'
					+		'<td class="htitle">'+li_cnt+'</td>'
					+		'<td class="htitle">'+_X.FormatComma(li_b)+'</td>'
					+		'<td class="htitle">'+_X.FormatComma(li_total)+'</td>'
					+		'</tr>'
					+		'</table>';
	}

	var ls_limit_amt  = uf_SetLimitAmt();
	var ls_office_amt = uf_SetOfficeAmt();

	$('#form_calendar')[0].contentWindow.SetLimitAmt(ls_limit_amt);
	$('#form_calendar')[0].contentWindow.SetRemark(info);
	$('#form_calendar')[0].contentWindow.SetSummary(info2);
	$('#form_calendar')[0].contentWindow.uf_bg_color();
}

function uf_SetLimitAmt() {
	var ls_result = _X.XmlSelect("gs", "GS01050", "GET_LIMIT_AMT", [S_DEPT_ID.value, is_cust_id, is_yymm], "array");
	return ls_result[0];
}

function uf_SetOfficeAmt() {
	var ls_result = _X.XmlSelect("gs", "GS01050", "GET_OFFICE_AMT", [S_DEPT_ID.value, is_cust_id, is_yymm], "array")[0][0];
	var la_rtn = ls_result.split('@');

	// 고객정보 & 계약정보 설정
	span_contract.innerHTML = '요양인정번호 : ' + is_manage_no + '&nbsp&nbsp|&nbsp&nbsp' + (la_rtn[6]<90?'<font color="#ff0000">':"") + '서비스계약기간 : ' + la_rtn[1] + ' ~ ' + la_rtn[2] + '&nbsp&nbsp|&nbsp&nbsp인정유효기간 : '  + la_rtn[3] + ' ~ ' + la_rtn[4] + '&nbsp&nbsp|&nbsp&nbsp' + is_cust_name + '님 (' + is_reduction.substr(0, 2) + is_app_rate + '%)&nbsp&nbsp|&nbsp&nbsp갱신까지 <b>[ ' + la_rtn[6] + ' ]</b> 일 남았습니다.' + (la_rtn[6]<90?'</font>':"");

	var ls_arr = new Array((ls_result==null || ls_result=="" ? "0" : la_rtn[0]), is_cust_level, is_reduction.substr(0, 2) + is_app_rate + '%');
	return ls_arr;
}

function uf_delete_schedule() {
	var frame = document.getElementById('form_calendar').contentWindow;
	var chkday = frame.document.getElementsByName('chkday');

	var j=0;

	if(dg_1.RowCount()<=0) {
		_X.MsgBox('일정이 없습니다.');
		return;
	}

	if(_X.MsgBoxYesNo("확인", (rd_div_1.checked?"이번달 전체 " : "선택한 날짜의 ") + "일정을 모두 삭제합니다. 계속 진행하시겠습니까?") != 1) return;

	if(rd_div_1.checked) {
		_X.ES("gs", "GS01050", "SERVICE_SCHEDULE_D01", [S_DEPT_ID.value, is_cust_id, is_yymm]);
	} else {
		for(var i = 0 ; i < chkday.length ; i++){
		//for(var i = 1 ; i <= 31 ; i++){
	  	if(chkday[i].checked == true) {
			//if($("#form_calendar").contents().find("#chkday"+i).prop("checked") == true) {
				j++;
  			//var chkDate = is_yymm+_X.LPad(i,2,'0');
	  		var chkDate = _X.strPurify(chkday[i].value);
  			_X.ES("gs", "GS01050", "SERVICE_SCHEDULE_D02", [S_DEPT_ID.value, is_cust_id, chkDate]);
  		}
  	}
	  if(j==0) {
	  	_X.MsgBox('일정을 삭제할 날짜를 선택하세요.');
	  	return;
	  }
  }

	uf_recalc();
}

function uf_append_schedule() {
	var frame = document.getElementById('form_calendar').contentWindow;
	var chkday = frame.document.getElementsByName('chkday');

	var j=0;
	var ll_row=0;
	var ServiceId = "";
	var ls_s_time_div = _X.LPad(_X.ToInt(START_TIME_DIV.value),2,'0');
	var ls_s_min_div  = _X.LPad(_X.ToInt(START_MIN_DIV.value),2,'0');
	var ls_e_time_div = _X.LPad(_X.ToInt(END_TIME_DIV.value),2,'0');
	var ls_e_min_div  = _X.LPad(_X.ToInt(END_MIN_DIV.value),2,'0');
	var ls_member_id  = MEMBER_ID.value;
	var ls_member_id2 = MEMBER_ID2.value;

	//가족케어여부
	var ls_family_yn    = FAMILY_YN.checked ? "Y" : "N";
	var ls_family_ref   = FAMILY_REF.value;

	// 미이용수가
	var ls_service_yn   = SERVICE_YN.checked ? "0" : "1";

	var ls_product_div  = PRODUCT_DIV.value;
	var ls_product_code = PRODUCT_CODE.value;
	var ls_remark       = REMARK.value;

	var ll_service_amt  = _X.ToInt(SERVICE_AMT.value);
	var ll_pay_amt      = _X.ToInt(PAY_AMT.value);
	var ll_req_amt      = _X.ToInt(REQ_AMT.value);
	var ll_remain_amt   = _X.ToInt(REMAIN_AMT.value);

	// product_code, product_div 는 선택값인데, product_id 는 가져와야 함.
	var ll_product_id = _GS.GetProductId(ls_product_code);

	var ll_service_cost  = _X.ToInt(SERVICE_AMT.value);
	var ll_service_min_1 = MIN_1.value;
	var ll_service_min_2 = MIN_2.value;
	var ll_service_min_3 = MIN_3.value;

	// 목욕(Z2000010) , 이동서비스 , 식대(Z3000010)
	var ll_product_id_meal = _GS.GetProductId('Z3000010');
	var ls_product_code_move = PRODUCT_CODE_MOVE.value;
	var ls_product_code_bath = PRODUCT_CODE_BATH.value;
	var ll_product_id_move = _GS.GetProductId(ls_product_code_move);
	var ll_product_id_bath = _GS.GetProductId(ls_product_code_bath);

	var ls_svc_yn    = SVC_YN.checked ? "Y" : "N";
	var ls_move_yn   = MOVE_YN.checked ? "Y" : "N";
	var ls_bath_yn   = BATH_YN.checked ? "Y" : "N";
	var ls_meal_yn   = MEAL_YN.checked ? "Y" : "N";
	var ls_oneway_yn = ONEWAY_YN.checked ? "Y" : "N";
	var ll_bath_amt  = _X.ToInt(AMT_BATH.value);
	var ll_meal_amt  = _X.ToInt(AMT_MEAL.value);
	var ll_move_amt  = _X.ToInt(AMT_MOVE.value);
	var ll_amt_etc   = _X.ToInt(AMT_ETC.value);

	if(!is_cust_id) { _X.MsgBox('고객을 선택하세요.'); return; }

	if(ls_svc_yn=="Y"||ls_bath_yn=="Y") {
		if(!ls_s_time_div) { _X.MsgBox('시작 시간을 선택하세요.'); return;	}
		if(!ls_s_min_div) { _X.MsgBox('시작 분을 선택하세요.'); return;	}
		if(!ls_e_time_div) { _X.MsgBox('종료 시간을 선택하세요.'); return;	}
		if(!ls_e_min_div) { _X.MsgBox('종료 분을 선택하세요.'); return;	}

		if(!(ls_s_time_div>='00'&&ls_s_time_div<='23')||!(ls_e_time_div>='00'&&ls_e_time_div<='23')) {
			_X.MsgBox('시간은 00에서 23사이로 입력해야 합니다.');
			return;
		}

		if(!(ls_s_min_div>='00'&&ls_s_min_div<='59')||!(ls_e_min_div>='00'&&ls_e_min_div<='59')) {
			_X.MsgBox('분은 00에서 59사이로 입력해야 합니다.');
			return;
		}

		if(ls_s_time_div==ls_e_time_div&&ls_s_min_div==ls_e_min_div) {
			_X.MsgBox('시작시간과 종료시간이 같습니다.');
			return;
		}
	}

	if(ls_svc_yn=="Y") {
		if(!ls_product_code) { _X.MsgBox('서비스코드를 선택하세요.'); return;	}
		if(!ls_member_id) { _X.MsgBox('요양보호사를 선택하세요.'); return;	}
	}

	if(ls_product_div == '4') {	// 주야간 보호
		if(ls_move_yn=="Y"&&ls_product_code_move=="") {
			_X.MsgBox("이동서비스 코드를 선택하세요.");
			return;
		}

		if(ls_bath_yn=="Y"&&ls_product_code_bath=="") {
			_X.MsgBox("목욕서비스코드를 입력하세요.");
			return;
		}

		if(ls_meal_yn=="Y"&&ll_meal_amt<=0) {
			_X.MsgBox("식대를 입력하세요.");
			return;
		}
	}

	// 방문에선 중복일정 없음.
	var ls_dupliservice_yn = "N";

	if(!ll_service_amt) ll_service_amt=0;
	if(!ll_pay_amt) ll_pay_amt=0;
	if(!ll_req_amt) ll_req_amt=0;
	if(!ll_remain_amt) ll_remain_amt=0;

	// 시간과 요보사, 고객, 지사 정보만 입력 후, 프로시저에서 재계산
	for(var i = 0 ; i < chkday.length ; i++){
  	if(chkday[i].checked == true) { j++;
  		var chkDate = _X.strPurify(chkday[i].value);
	// for(var i = 1 ; i <= 31 ; i++){
	// 	if($("#form_calendar").contents().find("#chkday"+i).prop("checked") == true) { j++;
 //  		var chkDate = is_yymm+_X.LPad(i,2,'0');

  		// 서비스 넣기
  		if(ls_svc_yn=="Y") {
	  		ll_row = dg_1.InsertRow(0);

	  		ServiceId = _GS.GetDataServiceId();

				dg_1.SetItem(ll_row,"S_ID",       _X.ToInt(ServiceId));
				dg_1.SetItem(ll_row,"PLAN_DATE",  _X.ToString(_X.ToDate(chkDate), 'YYYY-MM-DD'));
				dg_1.SetItem(ll_row,"CUST_ID",    _X.ToInt(is_cust_id));
				dg_1.SetItem(ll_row,"DEPT_ID",    S_DEPT_ID.value);
				dg_1.SetItem(ll_row,"SERVICE_YN", ls_service_yn);
				dg_1.SetItem(ll_row,"ENTER_ID",   mytop.gs_userid);
				dg_1.SetItem(ll_row,"ENTER_DATE", _X.ToString(_X.GetSysDate(), 'YYYY-MM-DD'));
				dg_1.SetItem(ll_row,"EDIT_DATE",  _X.ToString(_X.GetSysDate(), 'YYYY-MM-DD'));

				dg_1.SetItem(ll_row,"START_TIME_DIV", ls_s_time_div);
				dg_1.SetItem(ll_row,"START_MIN_DIV",  ls_s_min_div);
				dg_1.SetItem(ll_row,"END_TIME_DIV",   ls_e_time_div);
				dg_1.SetItem(ll_row,"END_MIN_DIV",    ls_e_min_div);

				dg_1.SetItem(ll_row,"PRODUCT_ID",     (ls_product_div=="9" ? 2560 : _X.ToInt(ll_product_id)));
				dg_1.SetItem(ll_row,"SERVICE_AMT",    (ls_product_div=="9" ? ll_amt_etc : ll_service_amt));
				dg_1.SetItem(ll_row,"PAY_AMT",    	  ll_pay_amt);
				dg_1.SetItem(ll_row,"REQ_AMT",    	  ll_req_amt);
				dg_1.SetItem(ll_row,"REMAIN_AMT",     ll_remain_amt);
				dg_1.SetItem(ll_row,"REMARK",         ls_remark);

				dg_1.SetItem(ll_row,"FAMILY_YN",      ls_family_yn);
				dg_1.SetItem(ll_row,"FAMILY_REF",     ls_family_ref);

				dg_1.SetItem(ll_row,"CONT_SEQ",   		 _X.ToInt(is_cont_seq));
				dg_1.SetItem(ll_row,"DISCOUNT_AMT",   0);

				dg_1.SetItem(ll_row,"MEMBER_ID",      ls_member_id);
				dg_1.SetItem(ll_row,"STATUS",         "T");

				dg_1.SetItem(ll_row,"DUPLISERVICE_YN", ls_dupliservice_yn);
				dg_1.SetItem(ll_row,"DUPLISERVICE_SEQ", 1);
	  	}

			// 목욕 추가로 넣기
			if(ls_bath_yn=="Y") {
	  		ll_row = dg_1.InsertRow(0);

	  		// insert
	  		ServiceId = _GS.GetDataServiceId();

				dg_1.SetItem(ll_row,"S_ID",       _X.ToInt(ServiceId));
				dg_1.SetItem(ll_row,"PLAN_DATE",  _X.ToString(_X.ToDate(chkDate), 'YYYY-MM-DD'));
				dg_1.SetItem(ll_row,"CUST_ID",    _X.ToInt(is_cust_id));
				dg_1.SetItem(ll_row,"DEPT_ID",    S_DEPT_ID.value);
				dg_1.SetItem(ll_row,"SERVICE_YN", "1");
				dg_1.SetItem(ll_row,"ENTER_ID",   mytop.gs_userid);
				dg_1.SetItem(ll_row,"ENTER_DATE", _X.ToString(_X.GetSysDate(), 'YYYY-MM-DD'));
				dg_1.SetItem(ll_row,"EDIT_DATE",  _X.ToString(_X.GetSysDate(), 'YYYY-MM-DD'));

				dg_1.SetItem(ll_row,"START_TIME_DIV", ls_s_time_div);
				dg_1.SetItem(ll_row,"START_MIN_DIV",  ls_s_min_div);
				dg_1.SetItem(ll_row,"END_TIME_DIV",   ls_e_time_div);
				dg_1.SetItem(ll_row,"END_MIN_DIV",    ls_e_min_div);

				dg_1.SetItem(ll_row,"PRODUCT_ID",     _X.ToInt(ll_product_id_bath));
				dg_1.SetItem(ll_row,"SERVICE_AMT",    ll_bath_amt);
				dg_1.SetItem(ll_row,"PAY_AMT",    	  0);
				dg_1.SetItem(ll_row,"REQ_AMT",    	  0);
				dg_1.SetItem(ll_row,"REMAIN_AMT",     0);

				dg_1.SetItem(ll_row,"CONT_SEQ",   		 _X.ToInt(is_cont_seq));
				dg_1.SetItem(ll_row,"DISCOUNT_AMT",   0);

				dg_1.SetItem(ll_row,"MEMBER_ID",      ls_member_id2);
				dg_1.SetItem(ll_row,"STATUS",         "I");

				dg_1.SetItem(ll_row,"DUPLISERVICE_YN", ls_dupliservice_yn);
				dg_1.SetItem(ll_row,"DUPLISERVICE_SEQ", 1);
	  	}

			// 이동서비스 추가로 넣기
			if(ls_move_yn=="Y") {
	  		ll_row = dg_1.InsertRow(0);

	  		// insert
	  		ServiceId = _GS.GetDataServiceId();

				dg_1.SetItem(ll_row,"S_ID",       _X.ToInt(ServiceId));
				dg_1.SetItem(ll_row,"PLAN_DATE",  _X.ToString(_X.ToDate(chkDate), 'YYYY-MM-DD'));
				dg_1.SetItem(ll_row,"CUST_ID",    _X.ToInt(is_cust_id));
				dg_1.SetItem(ll_row,"DEPT_ID",    S_DEPT_ID.value);
				dg_1.SetItem(ll_row,"SERVICE_YN", "1");
				dg_1.SetItem(ll_row,"ENTER_ID",   mytop.gs_userid);
				dg_1.SetItem(ll_row,"ENTER_DATE", _X.ToString(_X.GetSysDate(), 'YYYY-MM-DD'));
				dg_1.SetItem(ll_row,"EDIT_DATE",  _X.ToString(_X.GetSysDate(), 'YYYY-MM-DD'));

				dg_1.SetItem(ll_row,"START_TIME_DIV", '00');
				dg_1.SetItem(ll_row,"START_MIN_DIV",  '00');
				dg_1.SetItem(ll_row,"END_TIME_DIV",   '00');
				dg_1.SetItem(ll_row,"END_MIN_DIV",    '00');

				dg_1.SetItem(ll_row,"PRODUCT_ID",     _X.ToInt(ll_product_id_move));
				dg_1.SetItem(ll_row,"SERVICE_AMT",    ll_move_amt);
				dg_1.SetItem(ll_row,"PAY_AMT",    	  0);
				dg_1.SetItem(ll_row,"REQ_AMT",    	  0);
				dg_1.SetItem(ll_row,"REMAIN_AMT",     0);

				dg_1.SetItem(ll_row,"CONT_SEQ",   		 _X.ToInt(is_cont_seq));
				dg_1.SetItem(ll_row,"DISCOUNT_AMT",   0);

				dg_1.SetItem(ll_row,"MEMBER_ID",      "");
				dg_1.SetItem(ll_row,"STATUS",         "I");

				dg_1.SetItem(ll_row,"DUPLISERVICE_YN", ls_dupliservice_yn);
				dg_1.SetItem(ll_row,"DUPLISERVICE_SEQ", 1);
	  	}

			// 식대 추가로 넣기
			if(ls_meal_yn=="Y") {
				ll_row = dg_1.InsertRow(0);

	  		// insert
	  		ServiceId = _GS.GetDataServiceId();

				dg_1.SetItem(ll_row,"S_ID",       _X.ToInt(ServiceId));
				dg_1.SetItem(ll_row,"PLAN_DATE",  _X.ToString(_X.ToDate(chkDate), 'YYYY-MM-DD'));
				dg_1.SetItem(ll_row,"CUST_ID",    _X.ToInt(is_cust_id));
				dg_1.SetItem(ll_row,"DEPT_ID",    S_DEPT_ID.value);
				dg_1.SetItem(ll_row,"SERVICE_YN", "1");
				dg_1.SetItem(ll_row,"ENTER_ID",   mytop.gs_userid);
				dg_1.SetItem(ll_row,"ENTER_DATE", _X.ToString(_X.GetSysDate(), 'YYYY-MM-DD'));
				dg_1.SetItem(ll_row,"EDIT_DATE",  _X.ToString(_X.GetSysDate(), 'YYYY-MM-DD'));

				dg_1.SetItem(ll_row,"START_TIME_DIV", '00');
				dg_1.SetItem(ll_row,"START_MIN_DIV",  '00');
				dg_1.SetItem(ll_row,"END_TIME_DIV",   '00');
				dg_1.SetItem(ll_row,"END_MIN_DIV",    '00');

				dg_1.SetItem(ll_row,"PRODUCT_ID",     _X.ToInt(ll_product_id_meal));
				dg_1.SetItem(ll_row,"SERVICE_AMT",    ll_meal_amt);
				dg_1.SetItem(ll_row,"PAY_AMT",    	  0);
				dg_1.SetItem(ll_row,"REQ_AMT",    	  0);
				dg_1.SetItem(ll_row,"REMAIN_AMT",     0);

				dg_1.SetItem(ll_row,"CONT_SEQ",   		_X.ToInt(is_cont_seq));
				dg_1.SetItem(ll_row,"DISCOUNT_AMT",   0);

				dg_1.SetItem(ll_row,"MEMBER_ID",      "");
				dg_1.SetItem(ll_row,"STATUS",         "I");

				dg_1.SetItem(ll_row,"DUPLISERVICE_YN", ls_dupliservice_yn);
				dg_1.SetItem(ll_row,"DUPLISERVICE_SEQ", 1);
			}
  	}
  }

 	if(j==0) {
		_X.MsgBox('일정을 추가할 날짜를 선택하세요.');
		return;
	} else {
		x_DAO_Save();

		// 서비스 수가 재계산
		var li_rtn = _X.EP('gs','PR_GS_CALC_SERVICEAMT', [is_cust_id, S_DEPT_ID.value, is_yymm+'01', _X.ToString(_X.LastDate(_X.ToDate(is_date)),'yyyymmdd')]);
		if(li_rtn<0){
			_X.MsgBox("서비스 수가 계산 중, 오류가 발생했습니다.");
			return;
		}

		uf_recalc();
  }
}

function uf_recalc() {
	// 마감 확인
	if(uf_CloseCheck(false)===false) return;

	// 중복센터 입력확인
	var ls_deptcnt = _X.XmlSelect("gs", "GS01050", "CHK_SERVICE_SCHEDULE_R01", [is_cust_id, is_yymm], "array")[0][0];
	if(ls_deptcnt>1) {
		_X.MsgBox(ls_deptcnt+"개 센터에 스케쥴이 등록됐습니다.");
	}

	// 서비스 수가 재계산
	var li_rtn = _X.EP('gs','PR_GS_CALC_SERVICEAMT', [is_cust_id, S_DEPT_ID.value, is_yymm+'01', _X.ToString(_X.LastDate(_X.ToDate(is_date)),'yyyymmdd')]);
	if(li_rtn<0){
		_X.MsgBox("서비스 수가 계산 중, 오류가 발생했습니다.");
		return;
	}

	// 서비스 Summary 집계
	var ls_result = _X.EP('gs','PR_GS_SERVICE_SUMMARY', [is_cust_id, S_DEPT_ID.value, is_yymm]);
	if(ls_result==-1) {
		_X.MsgBox("서비스 집계 중, 오류가 발생했습니다.");
		return;
	}

	var ls_result = _X.EP('gs','PR_GS_CALCAMT_PERIOD', [is_cust_id, is_yymm+'01', _X.ToString(_X.LastDate(_X.ToDate(is_date)),'yyyymmdd')]);
	if(ls_result==-1) {
		_X.MsgBox("본인부담금 계산 중, 오류가 발생했습니다.");
	}
	x_DAO_Retrieve();
}


function uf_delete_one(s_id) {
	if(!confirm('삭제 하시겠습니까?')) {
		return;
	}

	var ls_result = _X.ES("gs", "GS01050", "SERVICE_SCHEDULE_D03", [s_id]);
	if ( li_result < 0){
		_X.MsgBox("저장하지 못했습니다.");
		return;
	}

	uf_recalc();
}

function uf_edit_one(aSid) {
	return;
	// var param = new Object();
	// param.dept_id = S_DEPT_ID.value;
	// param.yymm = is_curYear + ((is_curMonth+"").length<2? "0" : "") + is_curMonth;
	// param.cust_id = is_cust_id;
	// param.sid = aSid;
	// ptmain.SetParam(param);

	// ptmain.OpenSheet('GS01055',1,window,'','서비스 일정 수정',true,0,0,590,120,'default');
}

// function uf_edit_one_end(adate) {
// 	var ls_sql = " SELECT F_GS_GET_SERVICESCHEDULE5('CUST', '" + S_DEPT_ID.value + "', '" + adate.replace("-", "") + "','"+ is_cust_id +"') as info, TO_NUMBER(TO_CHAR(TO_DATE('"+adate+"'), 'DD')) as daynum FROM DUAL ";
// 	var ls_result = ptdwo.SqlSelectsArray("gs",ls_sql);

// 	if (ls_result.length>0) {
// 		$('#form_calendar')[0].contentWindow.SetData(ls_result[0][1], ls_result[0][0]);
// 	}
// }

function uf_show_modify() {
	if (!is_cust_id) {
		_X.MsgBox('고객을 검색하세요.');
		uf_findCust();
		return 0;
	}

	if(document.getElementById("lfdw_cond_tr").style.display == "block") {
		document.getElementById("lfdw_cond_tr").style.display = "none";
	} else {
		if(is_cont_seq=="0") {
			_X.MsgBox("등록된 계약이 없습니다. 계약을 먼저 생성해야 합니다.");
			return 0;
		}
		document.getElementById("lfdw_cond_tr").style.display = "block";
		START_TIME_DIV.focus();
	}
}

// 반복일정 추가/삭제 프로그램 실행
function uf_schedule_batch() {
	if (!is_cust_id) {
		_X.MsgBox('고객을 검색하세요.');
		uf_findCust();
		return 0;
	}

	if(is_cont_seq=="0") {
		_X.MsgBox("등록된 계약이 없습니다. 계약을 먼저 생성해야 합니다.");
		return 0;
	}

	_GS.OpenScheduleCreate(window, S_DEPT_ID.value, is_cust_id, is_cust_name, is_manage_no + ', ' + is_cust_level + ', ' + is_reduction + '(' + is_app_rate + '%)');
}

function uf_CloseCheck(aTag)
{ // aTag  : true 이면 _X.MsgBox 안띄움
  //급여 마감여부 체크(2009.03.31 김양열)
	if(_GS.IsClosed(is_yymm, S_DEPT_ID.value, "service")) {
		if(!aTag) {
			_X.MsgBox("서비스 마감 상태입니다. 마감을 해제해야 금액이 재계산됩니다.");
		}
		return false;
	}

	if(_GS.IsClosed(is_yymm, S_DEPT_ID.value, "gong")) {
		if(!aTag) {
			_X.MsgBox("처우개선비 마감 상태입니다. 마감을 해제해야 금액이 재계산됩니다.");
		}
		return false;
	}

	return true;
}

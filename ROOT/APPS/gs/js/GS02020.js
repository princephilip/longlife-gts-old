//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : 급여관리
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================

function x_InitForm2(){
	if(!_X.IsHeadoffice())	{
		$("#btn_finddept").hide();
	}
	_X.Tabs(tabs_1, 0);

	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "gs", "GS02020", "GS02020|GS02020_C01", false, true);
	_X.InitGrid(grid_2, "dg_2", "100%", "100%", "gs", "GS02020", "GS02020|GS02020_C02", false, true);
	_X.InitGrid(grid_3, "dg_3", "100%", "100%", "gs", "GS02020", "GS02020|GS02020_R03", false, false);

	var la_DEPT = _X.XmlSelect("com", "COMMON", "CODE_DEPT_AUTHORITY_R01" , new Array(mytop._UserID), 'json2');
	_X.DDLB_SetData(S_DEPT_ID, la_DEPT, null, true, (_X.IsHeadoffice() ? true : false));
	S_DEPT_ID.value = mytop._DeptCode;
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {

		// 제공
		var ls_SERVICE_YN = [{"code":"1","label":"제공"},{"code":"0","label":"미제공"}];
		dg_3.SetCombo("SERVICE_YN", ls_SERVICE_YN);

		// 가족케어
		var ls_FAMILY_YN = [{"code":"Y","label":"예"},{"code":"N","label":"아니오"}];
		dg_3.SetCombo("FAMILY_YN", ls_FAMILY_YN);

		// 본사일 때만 본사금액 보이게 하기
		if(_X.IsHeadoffice()) {
			uf_show_comp('show');
		}

		xe_EditChanged(S_RANGE, S_RANGE.value);
		xe_EditChanged(S_DEPT_ID, S_DEPT_ID.value);
		// setTimeout("x_DAO_Retrieve()",100);
	}
}

function x_DAO_Retrieve2(a_dg){
	var ls_yymm = _X.StrPurify(sle_basicmonth.value);  //날짜

	dg_3.Reset();
	dg_1.Retrieve([S_DEPT_ID.value, ls_yymm]);
	dg_2.Retrieve([S_DEPT_ID.value, ls_yymm]);

	uf_SetColtrol();
}

function	uf_Retrieve_dg3() {
	dg_3.Reset();
	var a_dg = dg_1;

	if(_X.GetTabIndex(tabs_1) != 0) a_dg = dg_2;

	var ls_dept     = S_DEPT_ID.value;   // 지사구분
	var ls_yn       =  "1";
	var ls_member   =  a_dg.GetItem(a_dg.GetRow(), "MEMBER_ID") +'%';
	var ls_cust     =  "%";
	var ls_fromdate = _X.StrPurify(sle_basicmonth.value)+'01';
	var ls_todate   = _X.LastDate(ls_fromdate);

	dg_3.Retrieve([ls_dept, ls_yn, ls_member, ls_cust, ls_fromdate, ls_todate]);
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id) {
		case	"S_DEPT_ID":
		case	"sle_basicmonth":
					x_DAO_Retrieve();
					break;
		case "S_RANGE": //비율변경
					var ll_btm = Number(a_val);
					var ll_top  = 665 - Number(a_val);
					var ll_topd = ll_top - 25;

					tabs_1.ResizeInfo 	 		= {init_width:1000, init_height:ll_top, width_increase:1, height_increase:1};
						ctab_1.ResizeInfo 	 	= {init_width:1000, init_height:ll_topd, width_increase:1, height_increase:1};
							grid_1.ResizeInfo		= {init_width:1000, init_height:ll_topd, width_increase:1, height_increase:1};
						ctab_2.ResizeInfo 	 	= {init_width:1000, init_height:ll_topd, width_increase:1, height_increase:1};
							grid_2.ResizeInfo		= {init_width:1000, init_height:ll_topd, width_increase:1, height_increase:1};

					grid_3.ResizeInfo		 		= {init_width:1000, init_height:ll_btm, width_increase:1, height_increase:0};
					xm_BodyResize(true);
					break;
	}
}


function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	if(!uf_CloseCheckSpecial()) return 0;

	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

//저장후 호출
function x_DAO_Saved2(){
	var ls_dept = S_DEPT_ID.value;  // 지사구분
	var ls_yymm = _X.StrPurify(sle_basicmonth.value);  //날짜

	_X.EP("gs", "PR_COLLECT_SERVICEAMT_UPDATE", [ls_dept, ls_yymm, '1']);

	x_DAO_Retrieve();
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
	if(dg_1.RowCount() == 0) {
		_X.MsgBox("출력할 데이터가 없습니다.");
		return;
	}

	if(S_DEPT_ID.value == '%'){
		_X.MsgBox("지사명을 선택해 주세요.");
		return;
	}

	if(rb_print_0.checked){
		_X.SetiReport("gs", "GS02020R_A", 		"as_dept="   		+ S_DEPT_ID.value
																				+ "&as_ym="   		+ _X.StrPurify(sle_basicmonth.value)
																				+ "&as_dept_nm="	+ $('#S_DEPT_ID').children('option:selected').text() );
	} else {
		var ls_yesno = "N";
		if(_X.MsgBoxYesNo("확인", "전체 임금 명세서를 인쇄하시겠습니까?") == 1) ls_yesno = "Y";

		if (x_IsAllDataChanged() > 0) {
			x_DAO_Save();
		}

		var ls_member_id = "";
		if(_X.GetTabIndex(tabs_1) == 0) {
			ls_member_id = dg_1.GetItem(dg_1.GetRow(), "MEMBER_ID");
		} else {
			ls_member_id = dg_2.GetItem(dg_2.GetRow(), "MEMBER_ID");
		}

		_X.SetiReport("gs", "GS02020R_B", 		"as_yn="   				+ ls_yesno
																				+ "&as_member_id="  + ls_member_id
																				+ "&as_yyyymm="   	+ _X.StrPurify(sle_basicmonth.value)
																				+ "&as_dept_id="		+ S_DEPT_ID.value );
	}
}

// function jf_dw_print_loaded2(dwc, dwcf, dwcw) {
// 	if(pthtml.GetRadioValue("rb_print") == '1'){
// 		var lsp_dept  = '[' + S_DEPT_ID.value + '] ' + _X.GetSelectText(S_DEPT_ID);
// 		var lsp_title = sle_paymonth.value.substr(0, 4) + '년 ' + sle_paymonth.value.substr(5, 2) + '월 급여대장'

// 		gs.DwModify(dwc, "t_title", lsp_title);
// 		gs.DwModify(dwc, "t_dept" , lsp_dept);
// 	}

// 	return 100;
// }

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	uf_Retrieve_dg3();
	xm_BodyResize(true);
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
	if(a_dg.id == 'dg_1' || a_dg.id == 'dg_2'){
		uf_Retrieve_dg3();
	}
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
	if(_FindCode.returnValue == null) return;

	switch(_FindCode.finddiv) {
		case	"S_DEPT_ID" :
					S_DEPT_ID.value = _FindCode.returnValue.DEPT_ID;
					x_DAO_Retrieve();
					break;
	}
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

// 지사찾기
function uf_findDept() {
	_X.FindDeptCode2('S_DEPT_ID','');
}

function uf_show_comp(aDiv) {
	var tShow = false;
	if(aDiv=="show") tShow = true;

	dg_2.SetColumnVisible( "C_HEALTHINSURE_AMT, C_PENSION_AMT, C_INDACC_AMT, C_EMPINSU_AMT, C_LONGTERM_AMT, C_RETIRE_AMT, C_EMPINSU2_AMT", tShow);
}

function	uf_SetColtrol() {
	if(!uf_CloseCheckSpecial(true)) {
		$("#btn_paymentresult").hide();
		$("#btn_pay_close").hide();
		$("#btn_special_close").hide();
		$("#xBtnSave,#xBtnDelete").hide();
		msg_txt.innerHTML = "&nbsp;&nbsp;※ 급여,특별수당 마감";

		dg_1.GridReadOnly(true);
		dg_2.GridReadOnly(true);
	} else if(!uf_CloseCheck(true)) {
		$("#btn_paymentresult").hide();
		//HideButton(btn_save);
		$("#btn_pay_close").hide();
		$("#btn_special_close").show();
		$("#xBtnDelete").hide();
		msg_txt.innerHTML = "&nbsp;&nbsp;※ 급여마감(특별수당 수정가능)";

		dg_1.GridReadOnly(false);
		dg_2.GridReadOnly(false);
	} else {
		$("#btn_paymentresult").show();
		$("#btn_pay_close").show();
		$("#btn_special_close").hide();
		$("#xBtnSave,#xBtnDelete").show();
		msg_txt.innerHTML = "";

		dg_1.GridReadOnly(false);
		dg_2.GridReadOnly(false);
	}
}

function uf_PaymentResult(){
	if(!uf_CloseCheck())
	 return;

	var ls_dept = S_DEPT_ID.value;   // 지사구분
	var ls_date = _X.StrPurify(sle_basicmonth.value);  //날짜

	if(_X.MsgBoxYesNo("급여 집계", "현재 저장되어있는 데이터는 삭제됩니다. 급여를 집계 하시겠습니까?") == 1)	{
		if (x_IsAllDataChanged() > 0) {
			x_DAO_Save();
		}

		_X.EP("gs", "PR_COLLECT_SERVICEAMT_UPDATE", [ls_dept, ls_date, '1']);

		x_DAO_Retrieve();
	}
}

function uf_CloseCheck(aTag)
{ // aTag  : true 이면 _X.MsgBox 안띄움
  //급여 마감여부 체크(2009.03.31 김양열)
	if(_GS.IsClosed(_X.StrPurify(sle_basicmonth.value), S_DEPT_ID.value, "pay")) {
		if(!aTag) {
			_X.MsgBox("급여마감 되었습니다.");
		}
		return false;
	}

	return true;
}

function uf_CloseCheckSpecial(aTag) {
	// aTag  : true 이면 _X.MsgBox 안띄움
  //급여 마감여부 체크(2009.03.31 김양열)
	if(_GS.IsClosed(_X.StrPurify(sle_basicmonth.value), S_DEPT_ID.value, "special")) {
		if(!aTag) {
			_X.MsgBox("특별수당마감 되었습니다.");
		}
		return false;
	}

	return true;
}

function uf_pay_closing() {
	if (x_IsAllDataChanged() > 0) {
		if(_X.MsgBoxYesNo("변경된 자료가 있습니다 \r\n 계속 진행 하시겠습니까?")==2) return;
	}

	var ls_yymm = _X.StrPurify(sle_basicmonth.value);

	if(_X.MsgBoxYesNo("급여 마감", ls_yymm.substr(0, 4)+"년 "+ls_yymm.substr(4,2)+"월 급여를 마감합니다.") == 1)	{
		var ls_dept = S_DEPT_ID.value;   // 지사구분
		_X.EP("gs", "PR_COLLECT_SERVICEAMT_UPDATE", [ls_dept, ls_yymm, '1']);

		_GS.ClosingDept(_X.StrPurify(sle_basicmonth.value), S_DEPT_ID.value, "pay");
		x_DAO_Retrieve();
	}
}

function uf_special_closing() {
	if (x_IsAllDataChanged() > 0) {
		if(_X.MsgBoxYesNo("변경된 자료가 있습니다 \r\n 계속 진행 하시겠습니까?")==2) return;
	}

	var ls_yymm = _X.StrPurify(sle_basicmonth.value);

	// 급여 마감 되었을 때만, 특별수당 마감
	if(uf_CloseCheck(true)) {
		_X.MsgBox("확인", "급여마감을 먼저 하신 후, 특별수당을 마감해야 합니다.");
	} else {
		if(_X.MsgBoxYesNo("특별수당 마감", ls_yymm.substr(0, 4)+"년 "+ls_yymm.substr(4,2)+"월 급여를 마감합니다.") == 1)	{
			var ls_dept = S_DEPT_ID.value;   // 지사구분
			_X.EP("gs", "PR_COLLECT_SERVICEAMT_UPDATE", [ls_dept, ls_yymm, '1']);

			_GS.ClosingDept(ls_yymm, S_DEPT_ID.value, "special");
			x_DAO_Retrieve();
		}
	}
}

//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : 이용계약서
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
var is_cust_id    = "",
		is_cust_name  = "",
		is_cust_level = "",
		is_cust_limit_amt = "",
		is_reduction = "",
		is_app_rate  = "",
		is_manage_no = "",
		is_cust_level_div = "",
		is_cust_gubun_div = "",
		is_from_date = "",
		is_to_date   = "",
		is_birth_date = "",
		is_phone      = "",
		is_zip_code   = "",
		is_addr1      = "",
		is_addr2      = "";

var ii_form_seq = 0;

var is_dept_name  = "",
    is_dept_phone = "",
    is_dept_owner = "",
    is_dept_addr  = "";

var is_pro_name  = "",
    is_pro_phone = "",
    is_pro_hp    = "",
    is_pro_zip   = "",
    is_pro_addr1 = "",
    is_pro_addr2 = "";

function x_InitForm2(){
	if(!_X.IsHeadoffice())	{
		$("#btn_finddept").hide();
	}

	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "gs", "GS01030", "GS01030|GS01030_C01", true,  true);
	_X.InitGrid(grid_2, "dg_2", "100%", "100%", "gs", "GS01030", "GS01030|GS01030_C02", false, true);

	//지사명
	var la_DEPT = _X.XmlSelect("com", "COMMON", "CODE_DEPT_AUTHORITY_R01" , new Array(mytop._UserID), 'json2');
	_X.DDLB_SetData(S_DEPT_ID, la_DEPT, null, true, (_X.IsHeadoffice() ? true : false));
	S_DEPT_ID.value = mytop._DeptCode;

	// if(_X.HaveActAuth("시스템관리자")) {
	// 	is_cust = '%';
	// 	is_custname = '';
	// }
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		//구분
		var ls_gubun = [{"code":"일반","label":"일반"},{"code":"50% 경감대상자","label":"50% 경감대상자"},{"code":"국민기초생활보장 수급자","label":"국민기초생활보장 수급자"}];
		_X.DDLB_SetData(CUST_GUBUN_DIV, ls_gubun, null, false, false);

		//var la_CUST_LEVEL_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0125", 'Y'), 'json2');
		var la_CUST_LEVEL_DIV = [{"code":"1등급","label":"1등급"},{"code":"2등급","label":"2등급"},{"code":"3등급","label":"3등급"},{"code":"4등급","label":"4등급"}];
		_X.DDLB_SetData(CUST_LEVEL_DIV, la_CUST_LEVEL_DIV, null, false, false);

		//var la_RELATION_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0116", 'Y'), 'json2');
		var la_RELATION_DIV = [{"code":"본인","label":"본인"},{"code":"가족","label":"가족"},{"code":"가족(배우자)","label":"가족(배우자)"},{"code":"가족(부모)","label":"가족(부모)"}];
		_X.DDLB_SetData(RELATION_DIV, la_RELATION_DIV, null, false, false);

		var ls_DEPT_DIV = [{"code":"방문요양","label":"방문요양"},{"code":"방문목욕","label":"방문목욕"},{"code":"주야간보호","label":"주야간보호"}];
		_X.DDLB_SetData(DEPT_DIV, ls_DEPT_DIV, null, false, false);

		var ls_DAY = [{"code":"1","label":"1일"},{"code":"2","label":"2일"},{"code":"3","label":"3일"},{"code":"4","label":"4일"}];
		_X.DDLB_SetData(WRITE_DAY, ls_DAY, null, false, false);
		_X.DDLB_SetData(CALC_DAY, ls_DAY, null, false, false);
		_X.DDLB_SetData(NOTI_DAY, ls_DAY, null, false, false);
		_X.DDLB_SetData(PAY_DAY, ls_DAY, null, false, false);

		var ls_PAY_METHOD = [{"code":"현금","label":"현금"},{"code":"신용카드","label":"신용카드"},{"code":"지로","label":"지로"},{"code":"무통장입금","label":"무통장입금"}];
		_X.DDLB_SetData(PAY_METHOD, ls_PAY_METHOD, null, false, false);

		xe_EditChanged(S_DEPT_ID, S_DEPT_ID.value);
		// setTimeout("x_DAO_Retrieve()",100);
	}
}

function x_DAO_Retrieve2(a_dg){
	if(!is_cust_id) { _X.MsgBox('고객을 선택하세요.'); return; }

	dg_2.Reset();
	dg_1.Retrieve([S_DEPT_ID.value, is_cust_id]);
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id) {
		case	"S_DEPT_ID":
					uf_get_dept();
					break;
		case	"DEPT_DIV":
					uf_SetColVisible(a_val);
					break;
	}
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	var cData2 	= dg_2.GetChangedData();

	if(cData2.length > 0){
		var li_seq = _X.ToInt(dg_2.GetTotalValue("PLAN_SEQ", "MAX"));
		for (i=0 ; i < cData2.length; i++){
			if(cData2[i].job == 'I'){
				li_seq++;
				dg_2.SetItem(cData2[i].idx, "PLAN_SEQ", li_seq);
			}
		}
	}

	return true;
}

//저장후 호출
function x_DAO_Saved2(){
}

function x_DAO_Insert2(a_dg, row){
	if(a_dg.id == 'dg_1') {
		if(!is_cust_id) { _X.MsgBox('고객을 선택하세요.'); return 0; }
	}

	if(a_dg.id == 'dg_2') {
		if(!is_cust_id) { _X.MsgBox('고객을 선택하세요.'); return 0; }
	}

  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
	if(a_dg.id == 'dg_1') {
		var li_seq = _X.XmlSelect("gs", "GS01030", "SEQ_FORM_CONTRACT", [], "array")[0][0];

		var ls_today = _X.ToString(_X.GetSysDate(), 'YYYYMMDD');

		dg_1.SetItem(rowIdx, "FORM_SEQ",  Number(li_seq));
		dg_1.SetItem(rowIdx, "CUST_ID",   Number(is_cust_id));
		dg_1.SetItem(rowIdx, "CUST_NAME", is_cust_name);
		dg_1.SetItem(rowIdx, "DEPT_ID",   S_DEPT_ID.value);

		dg_1.SetItem(rowIdx, "ZIP_CODE",   is_zip_code);
		dg_1.SetItem(rowIdx, "ADDR1",      is_addr1);
		dg_1.SetItem(rowIdx, "ADDR2",      is_addr2);
		dg_1.SetItem(rowIdx, "BIRTH_DATE", is_birth_date);

		dg_1.SetItem(rowIdx, "CUST_LEVEL_DIV", is_cust_level);
		dg_1.SetItem(rowIdx, "MANAGE_NO",      is_manage_no);
		dg_1.SetItem(rowIdx, "CUST_GUBUN_DIV", is_cust_gubun_div);

		dg_1.SetItem(rowIdx, "WRITE_DAY", 1);
		dg_1.SetItem(rowIdx, "CALC_DAY",  1);
		dg_1.SetItem(rowIdx, "NOTI_DAY",  1);
		dg_1.SetItem(rowIdx, "PAY_DAY",   1);

		dg_1.SetItem(rowIdx, "PAY_METHOD",    "현금");
		dg_1.SetItem(rowIdx, "CONTRACT_DATE", ls_today);
		dg_1.SetItem(rowIdx, "WRITE_DATE",    ls_today);
		dg_1.SetItem(rowIdx, "FROM_DATE",     is_from_date);
		dg_1.SetItem(rowIdx, "TO_DATE",       is_to_date);

		var ptt = /주야간/;
		dg_1.SetItem(rowIdx, "DEPT_NAME",     is_dept_name);
		dg_1.SetItem(rowIdx, "DEPT_DIV",      (ptt.test(is_dept_name) ? "주야간보호" : "방문요양"));
		dg_1.SetItem(rowIdx, "OWNER_NAME",    is_dept_owner);
		dg_1.SetItem(rowIdx, "DEPT_PHONE",    is_dept_phone);
		dg_1.SetItem(rowIdx, "DEPT_ADDR",     is_dept_addr);

		dg_1.SetItem(rowIdx, "MEMBER_ID",     mytop._UserID);
		dg_1.SetItem(rowIdx, "MEMBER_NAME",   mytop._UserName);
	}

	if(a_dg.id == 'dg_2') {
		dg_2.SetItem(rowIdx, "FORM_SEQ", ii_form_seq);
		dg_2.SetItem(rowIdx, "FROM_HOUR", "00");
		dg_2.SetItem(rowIdx, "FROM_MIN",  "00");
		dg_2.SetItem(rowIdx, "TO_HOUR", "00");
		dg_2.SetItem(rowIdx, "TO_MIN",  "00");
	}

}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
  if(dg_2.RowCount()<=0) {
  	_X.MsgBox("확인", "급여계획을 입력 후, '인쇄'해야 합니다.");
		return 0;
  }

  var ls_FileBaseUrl = window.location.origin + mytop._rootFileUpload;
	//ls_FileBaseUrl = ls_FileBaseUrl.replace("\\","\/").replace("\\","\/");
	var li_row 			= dg_1.GetRow();
	var ls_dept_div = dg_1.GetItem(li_row, "DEPT_DIV");
	var ls_form			=	dg_1.GetItem(li_row, "FORM_SEQ");

	if(ls_dept_div=="방문요양") {
		_X.SetiReport("gs",  "GS01030R_S0_A", "as_form=" + ls_form);
	} else if(ls_dept_div=="방문목욕") {
		_X.SetiReport("gs",  "GS01030R_SB0_B", "as_form=" + ls_form);
	} else if(ls_dept_div=="주야간보호") {
		_X.SetiReport("gs",  "GS01030R_SC0_C", "as_form=" + ls_form);
	}
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
  if (a_dg.id == 'dg_1') {
		uf_SetColVisible(dg_1.GetItem(a_newrow, "DEPT_DIV"));

		ii_form_seq = dg_1.GetItem(a_newrow, "FORM_SEQ");
		dg_2.Retrieve([ii_form_seq]);
  }
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
  if (a_dg.id == 'dg_1') {
  	if(dg_1.RowCount()==0) {
  		dg_1.InsertRow();
  	}
  }
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
	switch(_FindCode.finddiv) {
		case	"ZIP_CODE" :
					var ll_row = dg_1.GetRow();
					dg_1.SetItem(ll_row, "ZIP_CODE"   , a_retVal.zipNo);	// 주소찾이만 이런형식으로 받음
					dg_1.SetItem(ll_row, "ADDR1"      , a_retVal.roadAddrPart1 + a_retVal.roadAddrPart2);
					dg_1.SetItem(ll_row, "ADDR2"      , a_retVal.addrDetail);
					ADDR2.focus();
					break;
		case	"PRO_ZIP_CODE" :
					var ll_row = dg_1.GetRow();
					dg_1.SetItem(ll_row, "PRO_ZIP_CODE", a_retVal.zipNo);
					dg_1.SetItem(ll_row, "PRO_ADDR1"   , a_retVal.roadAddrPart1 + a_retVal.roadAddrPart2);
					dg_1.SetItem(ll_row, "PRO_ADDR2"   , a_retVal.addrDetail);
					PRO_ADDR2.focus();
					break;
	}

	if(_FindCode.returnValue == null) return;

	switch(_FindCode.finddiv) {
		case	"S_DEPT_ID" :
					S_DEPT_ID.value = _FindCode.returnValue.DEPT_ID;
					is_cust_id = "";
					is_cust_name = "";
					S_CUST_NAME.value = "";
					//x_DAO_Retrieve();
					break;
		case	"S_CUST_NAME" :
				  is_cust_id        = _FindCode.returnValue.CUST_ID;
				  is_cust_name      = _FindCode.returnValue.CUST_NAME;
				  is_cust_level     = _FindCode.returnValue.CUST_LEVEL_NAME;
				  is_cust_level_div = _FindCode.returnValue.CUST_LEVEL_DIV;
				  is_cust_limit_amt = _FindCode.returnValue.M_LIMIT_AMT;
				  is_reduction      = _FindCode.returnValue.REDUCTION_DIV;
				  is_app_rate       = _FindCode.returnValue.APP_RATE_NAME;
				  is_manage_no      = _FindCode.returnValue.COPY_MANAVE_NO;
				  is_from_date      = _FindCode.returnValue.START_DATE;
				  is_to_date        = _FindCode.returnValue.END_DATE;
				  S_CUST_NAME.value = is_cust_name;

				  switch(_FindCode.returnValue.REDUCTION_DIV) {
				  	case "B" :
				  		is_cust_gubun_div = "국민기초생활보장 수급자";
				  		break;
				  	case "M" :
				  		is_cust_gubun_div = "50% 경감대상자";
				  		break;
				  	default :
				  		is_cust_gubun_div = "일반";
				  }

				  is_birth_date = _FindCode.returnValue.BIRTH_DATE;
				  is_zip_code   = _FindCode.returnValue.ZIP_CODE;
				  is_addr1      = _FindCode.returnValue.ADDR1;
				  is_addr2      = _FindCode.returnValue.ADDR2;

				  x_DAO_Retrieve();
				  uf_get_pro();
					break;
	}
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

// 지사찾기
function uf_findDept() {
	_X.FindDeptCode2('S_DEPT_ID','');
}

// 고객찾기
function uf_findCust() {
	_X.CommonFindCode(window,"지사별 고객 찾기","com",'S_CUST_NAME','',[S_DEPT_ID.value],"FindCustId|FindCode|FIND_CODE_CUST",700, 700);
}

//우편번호찾기
function uf_ZipFind(a_param) {
	if(dg_1.RowCount() == 0) return;
	_X.FindStreetAddr(window,a_param,"", "");
}

function uf_get_dept() {
	if(S_DEPT_ID.value!="%") {
		var ls_dept = _X.XmlSelect("gs", "GS01030", "CODE_DEPT_R01", [S_DEPT_ID.value], "array");

		if(ls_dept.length > 0) {
			is_dept_name  = ls_dept[0][0];
			is_dept_phone = ls_dept[0][1];
			is_dept_owner = ls_dept[0][2];
			is_dept_addr  = ls_dept[0][3];
		}
	}
}

function uf_get_pro() {
	is_pro_name  = "";
	is_pro_phone = "";
	is_pro_hp    = "";
	is_pro_zip   = "";
	is_pro_addr1 = "";
	is_pro_addr2 = "";

	if(is_cust_id!="") {
		var ls_pro = _X.XmlSelect("gs", "GS01030", "CODE_CUST_R01", [is_cust_id], "array");
		if(ls_pro.length > 0) {
			is_pro_name  = ls_pro[0][0];
			is_pro_phone = ls_pro[0][1];
			is_pro_hp    = ls_pro[0][2];
			is_pro_zip   = ls_pro[0][3];
			is_pro_addr1 = ls_pro[0][4];
			is_pro_addr2 = ls_pro[0][5];
		}
	}
}

//  dg_2 컬럽 visible
function uf_SetColVisible(a_data) {
	if(a_data=="방문목욕") {
		dg_2.SetColumnVisible( "WEEK_YN, WEEK2_YN, MONTH_YN", true);
	} else {
		dg_2.SetColumnVisible( "WEEK_YN, WEEK2_YN, MONTH_YN", false);
	}
}



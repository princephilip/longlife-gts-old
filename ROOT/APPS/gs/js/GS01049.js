//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : 반복일정 추가/삭제
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
var is_date = "";
var is_fromdate = "";  // 조회 시작일자
var is_todate = "";    // 조회 종료일자
var is_deptcust = 'Y';    //Y:지사별고객찾기  , N:전체고객찾기
var is_cust_id ='';
var is_cust_name = '';
var is_cust_info = '';
var is_manage_no = '';
var is_cust_level = '';
var is_app_rate = '';
var is_reduction = '';
var is_dept_id = '';
var is_curYear = "";
var is_curMonth = "";
var is_cont_sdate = ""; // 계약시작일자
var is_cont_edate = ""; // 계약종료일자
var is_mon_last_date = "";  // 이번달 마지막날짜
var is_member_name = ""; // 요보사 이름
var is_family_yn = "N"; // 가족케어
var is_cont_seq = "0"; // 계약순번

function x_InitForm2(){
	_X.Tabs(tabs_1, 0);

  if(typeof(_Caller._FindCode.findcode)!="undefined") {
		is_dept_id = _Caller._FindCode.findcode[0];
	  is_cust_id = _Caller._FindCode.findcode[1];
	  is_cust_name = _Caller._FindCode.findcode[2];
	  is_cust_info = _Caller._FindCode.findcode[3];
  } else {
  	x_Close();
  }

	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "gs", "GS01049", "GS01049|GS01049_C01", false, true);

	//지사명
	var la_DEPT = _X.XmlSelect("com", "COMMON", "CODE_DEPT_AUTHORITY_R01" , new Array(mytop._UserID), 'json2');
	_X.DDLB_SetData(S_DEPT_ID, la_DEPT, null, true, (_X.IsHeadoffice() ? true : false));

  S_DEPT_ID.value = is_dept_id;
  S_CUST_ID.value = is_cust_id;
  S_CUST_NAME.value = is_cust_name;

	_X.FormSetDisable([S_DEPT_ID], true);

	is_date=_X.ToString(_X.GetSysDate(),'yyyy-mm-dd');
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		var ls_family_ref = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0116", 'Y'), 'json2');
		_X.DDLB_SetData(FAMILY_REF, ls_family_ref, "", false, true, "");

		var ls_product_div = _X.XmlSelect("com", "COMMON", "CODE_COMDIV4" , new Array("0128", 'Y', '1', '2', '3'), 'json2');
		_X.DDLB_SetData(PRODUCT_DIV, ls_product_div, null, false, false);

		uf_form_schedule_clear();
		xe_EditChanged(PRODUCT_DIV, PRODUCT_DIV.value);

		is_mon_last_date = _X.ToString(_X.LastDate(_X.ToDate(S_INS_TO_DATE.value)),'yyyymmdd');

		//is_cont_seq = _GS.GetMaxContSeq(is_cust_id, S_DEPT_ID.value, _X.ToString(_X.LastDate(_X.ToDate(S_INS_FROM_DATE.value)),'yyyymm'));
		is_cont_seq = _GS.GetMaxContSeq(is_cust_id, '%', _X.ToString(_X.LastDate(_X.ToDate(S_INS_FROM_DATE.value)),'yyyymm'));

		uf_SetOfficeAmt();

		uf_check_closing('ins');
		uf_check_closing('del');
		uf_check_todate('ins', 'C');
		uf_check_todate('del', 'C');

		uf_set_summary();

		//x_DAO_Retrieve();

	}
}

function x_DAO_Retrieve2(a_dg){
	//dg_1.Retrieve([is_custid]);
}

function xe_EditChanged2(a_obj, a_val){
	if(a_obj.id == "FAMILY_YN") {
		if(FAMILY_YN.checked && FAMILY_REF.value == "") {
			FAMILY_REF.value = "0010";
		}
		if(!FAMILY_YN.checked && FAMILY_REF.value != "") {
			FAMILY_REF.value = "";
		}
	}

	switch(a_obj.id) {
		case	"CHK_WEEKALL":
					if(CHK_WEEKALL.checked) {
						CHK_SUN.checked = true;
						CHK_MON.checked = true;
						CHK_TUE.checked = true;
						CHK_WED.checked = true;
						CHK_THU.checked = true;
						CHK_FRI.checked = true;
						CHK_SAT.checked = true;
					} else {
						CHK_SUN.checked = false;
						CHK_MON.checked = false;
						CHK_TUE.checked = false;
						CHK_WED.checked = false;
						CHK_THU.checked = false;
						CHK_FRI.checked = false;
						CHK_SAT.checked = false;
					}
					uf_set_summary();
					break;
		case	"CHK_DWEEKALL":
					if(CHK_DWEEKALL.checked) {
						CHK_DSUN.checked = true;
						CHK_DMON.checked = true;
						CHK_DTUE.checked = true;
						CHK_DWED.checked = true;
						CHK_DTHU.checked = true;
						CHK_DFRI.checked = true;
						CHK_DSAT.checked = true;
					} else {
						CHK_DSUN.checked = false;
						CHK_DMON.checked = false;
						CHK_DTUE.checked = false;
						CHK_DWED.checked = false;
						CHK_DTHU.checked = false;
						CHK_DFRI.checked = false;
						CHK_DSAT.checked = false;
					}
					uf_set_summary();
					break;
		case	"CHK_SUN":
		case	"CHK_MON":
		case	"CHK_TUE":
		case	"CHK_WED":
		case	"CHK_THU":
		case	"CHK_FRI":
		case	"CHK_SAT":
					if(!a_obj.checked) {
						CHK_WEEKALL.checked = false;
					}
					uf_set_summary();
					break;
		case	"CHK_DSUN":
		case	"CHK_DMON":
		case	"CHK_DTUE":
		case	"CHK_DWED":
		case	"CHK_DTHU":
		case	"CHK_DFRI":
		case	"CHK_DSAT":
					if(!a_obj.checked) {
						CHK_DWEEKALL.checked = false;
					}
					uf_set_summary();
					break;
		case	"S_INS_FROM_DATE":
					uf_check_closing('ins');
		case	"S_INS_TO_DATE":
					uf_set_summary();
					break;
		case	"S_DEL_FROM_DATE":
					uf_check_closing('del');
		case	"S_DEL_TO_DATE":
					uf_set_summary();
					break;
		case	"rb_chk_todate":
					uf_check_todate('ins', a_val);
					uf_set_summary();
					break;
		case	"rb_chk_dtodate":
					uf_check_todate('del', a_val);
					uf_set_summary();
					break;
		case	"PRODUCT_DIV":
					var ls_product_code = _X.XmlSelect("com", "COMMON", "DDLB_CODE_PRODUCT" , new Array(a_val, 'Y'), 'json2');
					_X.DDLB_SetData(PRODUCT_CODE, ls_product_code, null, false, false);

					if(a_val == "2") {
						$("#MEMBER_NAME2").show();
						$("#btn_memberfind2").show();
					} else {
						$("#MEMBER_NAME2").hide();
						$("#btn_memberfind2").hide();
					}
		case	"START_TIME_DIV":
		case	"START_MIN_DIV":
		case	"END_TIME_DIV":
		case	"END_MIN_DIV":
		case	"MEMBER_ID":
		case	"MEMBER_ID2":
		case	"PRODUCT_CODE":
		case	"FAMILY_YN":
		case	"FAMILY_REF":
					var ls_div   = PRODUCT_DIV.value;
					var ls_stime = _X.LPad(_X.ToInt(START_TIME_DIV.value),2,'0');
					var ls_smin  = _X.LPad(_X.ToInt(START_MIN_DIV.value),2,'0');
					var ls_etime = _X.LPad(_X.ToInt(END_TIME_DIV.value),2,'0');
					var ls_emin  = _X.LPad(_X.ToInt(END_MIN_DIV.value),2,'0');

					if(!(ls_stime>=0&&ls_stime<=23)||!(ls_etime>=0&&ls_etime<=23)) {
						_X.MsgBox('시간은 0에서 23사이로 입력해야 합니다.');
						return;
					}

					if(!(ls_smin>=0&&ls_smin<=59)||!(ls_emin>=0&&ls_emin<=59)) {
						_X.MsgBox('분은 0에서 59사이로 입력해야 합니다.');
						return;
					}

  				switch(ls_div) {
  					case '1' :		//방문요양일경우
		  					if(a_obj.id == "PRODUCT_CODE") return;	//서비스코드 바뀔시 아무것도 하지 않음
								if(ls_stime && ls_smin && ls_etime && ls_emin) {
									//가족케어여부
									var ls_family_yn    = FAMILY_YN.checked ? "Y" : "N";
									var ls_family_ref   = FAMILY_REF.value;
									var ll_service_cost = 0;
									var ls_date         = "20170614";	//휴일 아닐 날짜 그냥 지정
									var ls_product_id   = 0;
					    		var ls_product_div  = ls_div;
					    		var ls_level_div    = '1';	//장기요양등급 (등급에 따른 급여한도가 달라서 의미 없음)
								  var ls_result = _X.XmlSelect("gs", "GS01040", "GET_SERVICEAMT", [ls_date, ls_stime, ls_smin, ls_etime, ls_emin, ll_service_cost, ls_family_yn, ls_product_id, ls_product_div, ls_level_div, "1"], "array");

								  if (ls_result.length > 0) {
								  	var la_rtn = ls_result[0][0].split('|');
								    var ll_service_amt   = _X.ToInt(la_rtn[0]);
								    var ll_service_min_1 = _X.ToInt(la_rtn[1]);
								    var ll_service_min_2 = _X.ToInt(la_rtn[2]);
								    var ll_service_min_3 = _X.ToInt(la_rtn[3]);
								    var ll_service_amt_1 = _X.ToInt(la_rtn[4]);
								    var ll_service_amt_2 = _X.ToInt(la_rtn[5]);
								    var ll_service_amt_3 = _X.ToInt(la_rtn[6]);

								    MIN_1.value    = ll_service_min_1;
			   						MIN_2.value    = ll_service_min_2;
			   						MIN_3.value    = ll_service_min_3;
								    MIN_TOT.value  = ll_service_min_1 + ll_service_min_2 + ll_service_min_3;
								    MIN_REAL.value = _X.ToInt(la_rtn[10]);
			   						AMT_1.value    = ll_service_amt_1;
			   						AMT_2.value    = ll_service_amt_2;
			   						AMT_3.value    = ll_service_amt_3;
			   						AMT_TOT.value  = ll_service_amt_1 + ll_service_amt_2 + ll_service_amt_3;

								    PRODUCT_ID.value   = _X.ToInt(la_rtn[7]);
								    PRODUCT_CODE.value = la_rtn[8];
								    PRODUCT_NAME.value = la_rtn[9];
								    SERVICE_AMT.value  = ll_service_amt;
			  					}
								}
								break;
						case '2':		//방문목욕일 경우
								//서비스 코드가 바뀔대 수가 변경
								if(a_obj.id == "PRODUCT_CODE") {
								  var ls_result = _X.XmlSelect("gs", "GS01040", "CODE_PRODUCT_R01", [ls_div, a_val, _X.ToString(is_date,'YYYYMMDD')], "array");

							    MIN_1.value    = _X.ToInt(ls_result[0][4]);
		   						MIN_2.value    = 0;
		   						MIN_3.value    = 0;

							    PRODUCT_ID.value   = _X.ToInt(ls_result[0][0]);
							    PRODUCT_CODE.value = ls_result[0][1];
							    PRODUCT_NAME.value = ls_result[0][2];
							    SERVICE_AMT.value  = _X.ToInt(ls_result[0][3]);
							    SERVICE_MIN.value  = ls_result[0][4];
								}
								break;
						case '3':		//방문간호일 경우
		  					if(a_obj.id == "PRODUCT_CODE") return;	//서비스코드 바뀔시 아무것도 하지 않음
								if(ls_stime && ls_smin && ls_etime && ls_emin) {
									//가족케어여부
									var ls_family_yn = 'N';
									//var ls_member_id = 'N';
									var ll_service_cost = 0;
									var ls_date = "20121008";	//휴일 아닐 날짜 그냥 지정
									var ls_product_id = 0;
					    		var ls_product_div = ls_div;
					    		var ls_level_div = '1';	//장기요양등급

								  var ls_result = _X.XmlSelect("gs", "GS01040", "GET_SERVICEAMT", [ls_date, ls_stime, ls_smin, ls_etime, ls_emin, ll_service_cost, ls_family_yn, ls_product_id, ls_product_div, ls_level_div, "1"], "array");

								  if (ls_result.length > 0) {
								  	var la_rtn = ls_result[0][0].split('|');
								    var ll_service_amt   = _X.ToInt(la_rtn[0]);
								    var ll_service_min_1 = _X.ToInt(la_rtn[1]);
								    var ll_service_min_2 = _X.ToInt(la_rtn[2]);
								    var ll_service_min_3 = _X.ToInt(la_rtn[3]);
								    var ll_service_amt_1 = _X.ToInt(la_rtn[4]);
								    var ll_service_amt_2 = _X.ToInt(la_rtn[5]);
								    var ll_service_amt_3 = _X.ToInt(la_rtn[6]);

								    MIN_1.value    = ll_service_min_1;
			   						MIN_2.value    = 0;
			   						MIN_3.value    = 0;
								    MIN_TOT.value  = ll_service_min_1;
			   						AMT_1.value    = ll_service_amt_1;
			   						AMT_2.value    = 0;
			   						AMT_3.value    = 0;
			   						AMT_TOT.value  = ll_service_amt_1;

								    PRODUCT_ID.value   = _X.ToInt(la_rtn[7]);
								    PRODUCT_CODE.value = la_rtn[8];
								    PRODUCT_NAME.value = la_rtn[9];
								    SERVICE_AMT.value  = ll_service_amt;
			  					}
								}
								break;
					}
					break;
		case	"SERVICE_AMT":
					var ls_div   = PRODUCT_DIV.value;
					if(ls_div == '7') {			//기타급여일 경우
						AMT_1.value   = Number(a_val);
						AMT_TOT.value = Number(a_val);
						PAY_AMT.value = Number(a_val);
						REQ_AMT.value = 0;
					}
					break;
	}
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

function x_DAO_Saved2(){
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
	if(_FindCode.returnValue == null) return;

	switch(_FindCode.finddiv) {
		case	"MEMBER_ID" :
					MEMBER_ID.value    = _FindCode.returnValue.MEMBER_ID;
					MEMBER_NAME.value  = _FindCode.returnValue.MEMBER_NAME;
					is_member_name     = _FindCode.returnValue.MEMBER_NAME;
					uf_set_summary();
					break;
		case	"MEMBER_ID2" :
					MEMBER_ID2.value   = _FindCode.returnValue.MEMBER_ID;
					MEMBER_NAME2.value = _FindCode.returnValue.MEMBER_NAME;
					break;
	}
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
  if(_IsModal || _IsPopup) {
    window.close();
  }else{
    if(_Caller.$("#findmodal").length>0?_Caller.$("#findmodal").dialog("close"):window.close());
  }

	_Caller.x_DAO_Retrieve();
}

// 요양보호사찾기
function uf_findMember(a_div) {
	var as_gubun  = '0205';  //요양보호사
	_X.CommonFindCode(window,"요양보호사 찾기","com",a_div,'',[S_DEPT_ID.value, as_gubun],"FindCodeMember|FindCode|FIND_CODE_MEMBER",700, 450);
}

// 서비스일정 초기화
function uf_form_schedule_clear() {
	$("#MEMBER_NAME2").hide();
	$("#btn_memberfind2").hide();

	START_TIME_DIV.value = '00';
	START_MIN_DIV.value  = '00';
	END_TIME_DIV.value   = '00';
	END_MIN_DIV.value    = '00';
	PRODUCT_DIV.value    = '1';
	AMT_1.value          = '';
	AMT_2.value          = '';
	AMT_3.value          = '';
	AMT_TOT.value        = '';
	MIN_1.value          = '';
	MIN_2.value          = '';
	MIN_3.value          = '';
	MIN_TOT.value        = '';
	MIN_REAL.value       = '';
	MEMBER_ID.value      = '';
	MEMBER_NAME.value    = '';
	MEMBER_ID2.value     = '';
	MEMBER_NAME2.value   = '';
	FAMILY_YN.checked    = false;
	FAMILY_REF.value     = '';
	SERVICE_AMT.value    = '';
}

function uf_SetOfficeAmt() {
	var ls_result = _X.XmlSelect("gs", "GS01040", "GET_OFFICE_AMT", [S_DEPT_ID.value, is_cust_id, _X.ToString(is_date,'YYYYMM')], "array")[0][0];
	var la_rtn = ls_result.split('@');

	OFFICE_AMT.value = (ls_result==null || ls_result=="" ? 0 : _X.ToInt(la_rtn[0]));
	cust_info.innerHTML = is_cust_info + '&nbsp&nbsp|&nbsp&nbsp' + (la_rtn[6]<90?'<font color="#ff0000">':"") + '계약기간 : ' + la_rtn[1] + ' ~ ' + la_rtn[2] ;
	is_cont_sdate = la_rtn[1].replace(/\s/g, "").replace(/\./g, "");
	is_cont_edate = la_rtn[2].replace(/\s/g, "").replace(/\./g, "");
}

// 반복일정 삭제하기
function uf_delete_schedule() {
	var ls_frdt = _X.StrPurify(S_DEL_FROM_DATE.value);
	var ls_todt = _X.StrPurify(S_DEL_TO_DATE.value);
	var ls_wk1  = CHK_DSUN.checked ? "1" : "";
	var ls_wk2  = CHK_DMON.checked ? "2" : "";
	var ls_wk3  = CHK_DTUE.checked ? "3" : "";
	var ls_wk4  = CHK_DWED.checked ? "4" : "";
	var ls_wk5  = CHK_DTHU.checked ? "5" : "";
	var ls_wk6  = CHK_DFRI.checked ? "6" : "";
	var ls_wk7  = CHK_DSAT.checked ? "7" : "";

	if(ls_wk1=="" && ls_wk2=="" && ls_wk3=="" && ls_wk4=="" && ls_wk5=="" && ls_wk6=="" && ls_wk7=="") {
		_X.MsgBox('요일을 선택하세요.');
		return;
	}

	if(_X.MsgBoxYesNo("반복일정 삭제", summary_del.innerHTML.replace(/<br>/gi,"\n").replace(/<\/b>/gi,"").replace(/<b>/gi,"") + "\n\n일정을 모두 삭제하시겠습니까?") != 1) return;

	// 격주 처리
	// var ls_add_where = "";
	// if(pthtml.GetRadioValue("chk_week")=="2") {
	// 	ls_add_where = " AND MOD(A.WRANK ,2)=1 ";
	// }

	var ls_result = _X.ES("gs", "GS01049", "SERVICE_SCHEDULE_D01", [S_DEPT_ID.value, is_cust_id, ls_frdt, ls_todt, ls_wk1, ls_wk2, ls_wk3, ls_wk4, ls_wk5, ls_wk6, ls_wk7]);
	if ( li_result < 0){
		_X.MsgBox("반복일정 삭제중 오류가 발생했습니다.");
		return;
	}

	_X.MsgBox("반복일정 삭제", "일정을 삭제했습니다.\n창을 닫으시면 달력에 일정이 조회됩니다.");
}

// 반복일정 생성하기
function uf_append_schedule() {
	var ls_s_time_div = _X.LPad(_X.ToInt(START_TIME_DIV.value),2,'0');
	var ls_s_min_div  = _X.LPad(_X.ToInt(START_MIN_DIV.value),2,'0');
	var ls_e_time_div = _X.LPad(_X.ToInt(END_TIME_DIV.value),2,'0');
	var ls_e_min_div  = _X.LPad(_X.ToInt(END_MIN_DIV.value),2,'0');
	var ls_member_id  = MEMBER_ID.value;
	var ls_member_id2 = MEMBER_ID2.value;

	//가족케어여부
	var ls_family_yn = "N";
	if(FAMILY_YN.checked) ls_family_yn = "Y";
	var ls_family_ref   = FAMILY_REF.value;
	var ls_product_div  = PRODUCT_DIV.value;
	var ls_product_code = PRODUCT_CODE.value;

	// product_code, product_div 는 선택값인데, product_id 는 가져와야 함.
	var ll_product_id = _GS.GetProductId(ls_product_code);

	var ll_service_cost  = SERVICE_AMT.value;
	var ll_service_min_1 = MIN_1.value;
	var ll_service_min_2 = MIN_2.value;
	var ll_service_min_3 = MIN_3.value;

	if(!is_cust_id) { _X.MsgBox('고객을 선택하세요.'); return; }
	if(!ls_s_time_div) { _X.MsgBox('시작 시간을 선택하세요.'); return;	}
	if(!ls_s_min_div) { _X.MsgBox('시작 분을 선택하세요.'); return;	}
	if(!ls_e_time_div) { _X.MsgBox('종료 시간을 선택하세요.'); return;	}
	if(!ls_e_min_div) { _X.MsgBox('종료 분을 선택하세요.'); return;	}
	if(ls_product_code.substr(0,1)!="Z") {
		if(!ls_member_id) { _X.MsgBox('요양보호사를 선택하세요.'); return;	}
	}
	if(!ls_product_code) { _X.MsgBox('서비스코드를 선택하세요.'); return;	}

	if(ls_product_div == '7') {
		if(!ll_service_amt) { _X.MsgBox('서비스금액을 입력하세요.'); return; }
	}

	if(!(ls_s_time_div>=0&&ls_s_time_div<=23)||!(ls_e_time_div>=0&&ls_e_time_div<=23)) {
		_X.MsgBox('시간은 0에서 23사이로 입력해야 합니다.');
		return;
	}

	if(!(ls_s_min_div>=0&&ls_s_min_div<=59)||!(ls_e_min_div>=0&&ls_e_min_div<=59)) {
		_X.MsgBox('분은 0에서 59사이로 입력해야 합니다.');
		return;
	}

	if(ls_s_time_div==ls_e_time_div&&ls_s_min_div==ls_e_min_div) {
		_X.MsgBox('시작시간과 종료시간이 같습니다.');
		return;
	}

	if(ls_member_id!=''&&ls_member_id2!=''&&ls_member_id==ls_member_id2) {
		_X.MsgBox('2인 일정의 경우, 서로 다른 요양보호사를 입력해야 합니다.');
		return;
	}

	// 목욕(product_div:2) 이면서, member_id1 member_id2 있으면 중복일정
	var ls_dupliservice_yn = "N";
	if(ls_product_div=='2'&&ls_member_id!=""&&ls_member_id2!="") {
		ls_dupliservice_yn = "Y";
	}

	var ls_frdt = _X.StrPurify(S_INS_FROM_DATE.value);
	var ls_todt = _X.StrPurify(S_INS_TO_DATE.value);
	var ls_wk1  = CHK_SUN.checked ? "1" : "";
	var ls_wk2  = CHK_MON.checked ? "2" : "";
	var ls_wk3  = CHK_TUE.checked ? "3" : "";
	var ls_wk4  = CHK_WED.checked ? "4" : "";
	var ls_wk5  = CHK_THU.checked ? "5" : "";
	var ls_wk6  = CHK_FRI.checked ? "6" : "";
	var ls_wk7  = CHK_SAT.checked ? "7" : "";

	if(ls_wk1=="" && ls_wk2=="" && ls_wk3=="" && ls_wk4=="" && ls_wk5=="" && ls_wk6=="" && ls_wk7=="") {
		_X.MsgBox('요일을 선택하세요.');
		return;
	}

	// 일정생성 확인
	if(_X.MsgBoxYesNo("반복일정 생성", summary_ins.innerHTML.replace(/<br>/gi,"\n").replace(/<\/b>/gi,"").replace(/<b>/gi,"") + "\n\n일정을 생성하시겠습니까?") != 1) return;

	// 격주 처리
	var ls_dup = "N";
	if(rb_chk_week_1.checked) ls_dup = "Y";

	var ls_result = _X.ES("gs", "GS01049", "SERVICE_SCHEDULE_C01", [S_DEPT_ID.value, is_cust_id, ls_frdt, ls_todt, ls_member_id, ls_member_id2,
									ls_s_time_div, ls_s_min_div, ls_e_time_div, ls_e_min_div,
									ll_product_id, is_cont_seq, ls_family_yn, ls_family_ref, ls_dupliservice_yn,
									ls_wk1, ls_wk2, ls_wk3, ls_wk4, ls_wk5, ls_wk6, ls_wk7, ls_dup, mytop._UserID]);

	if ( li_result < 0){
		_X.MsgBox("반복일정 생성중 오류가 발생했습니다.");
		return;
	}

	// 서비스 수가 재계산
	var ls_result = _X.EP('gs','PR_GS_CALC_SERVICEAMT', [is_cust_id, S_DEPT_ID.value,  _X.ToString(is_date,'YYYYMM')+'01', _X.ToString(_X.LastDate(_X.ToDate(is_date)),'yyyymmdd')]);
	if(ls_result<0){
		_X.MsgBox("서비스 수가 계산 중, 오류가 발생했습니다.");
		return;
	}

	_X.MsgBox("반복일정 생성", "일정을 추가했습니다.\n창을 닫으시면 달력에 일정이 조회됩니다.");
}

// 마감일자 체크해서 서비스 변경 시작일 수정하기
function uf_check_closing(a_act) {
	var ls_return = _X.XmlSelect("gs", "GS01049", "GET_GREATEST_DATE", [S_DEPT_ID.value, is_cont_sdate], "array")[0][0];

	if (a_act=="ins") {
		S_INS_FROM_DATE.value = _X.ToString(ls_return ,'YYYY-MM-DD');
	} else {
		S_DEL_FROM_DATE.value = _X.ToString(ls_return ,'YYYY-MM-DD');
	}
}

// 마감일자 수정
function uf_check_todate(a_act, a_div) {
	if(a_div=="C") { // 계약종료일
		if(a_act=="ins") {
			S_INS_TO_DATE.value = _X.ToString(is_cont_edate ,'YYYY-MM-DD');
		} else {
			S_DEL_TO_DATE.value = _X.ToString(is_cont_edate ,'YYYY-MM-DD');
		}
	} else {
		if(a_act=="ins") {
			S_INS_TO_DATE.value = _X.ToString(is_mon_last_date ,'YYYY-MM-DD');
		} else {
			S_DEL_TO_DATE.value = _X.ToString(is_mon_last_date ,'YYYY-MM-DD');
		}
	}

}

// 추가 및 삭제 요약정보 입력하기
function uf_set_summary() {
	var ls_ins = "";
	var ls_del = "";
	var ls_ins_w = (CHK_SUN.checked? "일," : "") + (CHK_MON.checked? "월," : "") + (CHK_TUE.checked? "화," : "") + (CHK_WED.checked? "수," : "") + (CHK_THU.checked? "목," : "") + (CHK_FRI.checked? "금," : "") + (CHK_SAT.checked? "토," : "");
	var ls_del_w = (CHK_DSUN.checked? "일," : "") + (CHK_DMON.checked? "월," : "") + (CHK_DTUE.checked? "화," : "") + (CHK_DWED.checked? "수," : "") + (CHK_DTHU.checked? "목," : "") + (CHK_DFRI.checked? "금," : "") + (CHK_DSAT.checked? "토," : "");

	ls_ins = "<b>" + S_INS_FROM_DATE.value + "부터 " + S_INS_TO_DATE.value + "까지</b> | "
				+ (rb_chk_week_0.checked ? "매주 " : "격주 " )
				+ "<b>" + ls_ins_w.substr(0, ls_ins_w.length - 1) + "</b><br>"
				+ (is_member_name!="" ? is_member_name + " | " : "")
				+ START_TIME_DIV.value +"시 "+ START_MIN_DIV.value +"분 ~ "+ END_TIME_DIV.value +"시 "+ END_MIN_DIV.value +"분"
				+ (FAMILY_YN.ckecked ? " | 가족케어(" + FAMILY_REF.options[FAMILY_REF.selectedIndex].text + ")": "");
	ls_del = "<b>" + S_DEL_FROM_DATE.value + "부터 " + S_DEL_TO_DATE.value + "까지</b> | "
				+ "<b>" + ls_del_w.substr(0, ls_del_w.length - 1) + "</b><br>" ;
	//fdw_1.SetItem(1, "product_div", '1');

	document.getElementById("summary_ins").innerHTML = ls_ins;
	document.getElementById("summary_del").innerHTML = ls_del;
}

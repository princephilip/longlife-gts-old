//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : [GS02040] 요양서비스 일정조회
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
var is_client_div = 'O';	//I:매입, O:매출, J:지사, E:기타
var is_date = "";
var is_yymm = "";  // 조회 년월
var member_id = "";

function x_InitForm2(){
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "gs", "GS02040", "GS02040|GS02040_R01", false,  false);

	//지사명
	var la_DEPT = _X.XmlSelect("com", "COMMON", "CODE_DEPT_AUTHORITY_R01" , new Array(mytop._UserID), 'json2');
	_X.DDLB_SetData(S_DEPT_ID, la_DEPT, null, true, false);
	S_DEPT_ID.value = mytop._DeptCode;

	is_date = _X.ToString(_X.GetSysDate(),'yyyy-mm-dd');
  is_yymm = _X.ToString(_X.GetSysDate(),'yyyymm');
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		x_DAO_Retrieve();
	}
}

function x_DAO_Retrieve2(a_dg){
	jf_CalendarLoaded();
}

function xe_EditChanged2(a_obj, a_val){
	if(a_obj.id == "S_DEPT_ID") {
		x_DAO_Retrieve();
	}
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

//저장후 호출
function x_DAO_Saved2(){
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
	 _X.SetiReport("gs", "GS02040R",	"as_memberid="  	+ dg_1.GetItem(dg_1.GetRow(), "MEMBER_ID")
																	+ "&as_yyyymm="  	 	+ _X.ToString(is_date, 'YYYYMM')
																	+ "&as_dept="   		+ S_DEPT_ID.value
																	+ "&as_printtag="  	+ 'CDEPTPRINT'
																	+ "&as_dept_nm="  	+ $('#S_DEPT_ID').children('option:selected').text() );
}


function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
	if(_FindCode.returnValue == null) return;

	switch(_FindCode.finddiv) {
		case	"S_DEPT_ID" :
					S_DEPT_ID.value = _FindCode.returnValue.DEPT_ID;
					x_DAO_Retrieve();
					break;
	}
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

// 지사찾기
function uf_findDept() {
	_X.FindDeptCode2('S_DEPT_ID','');
}

function DateChanged(curYear,curMonth,curDay,curinit){
  is_date = curYear + "-" + _X.LPad(curMonth,2,'0') + "-" + _X.LPad(curDay,2,'0');
  is_yymm = curYear + _X.LPad(curMonth,2,'0');

	if (curinit == "Y"){
		x_DAO_Retrieve();
  }

	return true;
}

// 달력콘트롤에 일자별 자료를 세팅한다.(월전체)
function jf_CalendarLoaded(lastDay){
	var ls_result = _X.XmlSelect("gs", "GS02040", "GET_SERVICESCHEDULE", [S_DEPT_ID.value, member_id, is_yymm], "array");

	if (ls_result.length>0) {
		for(var i=1; i<=ls_result.length; i++) {
			$('#form_calendar')[0].contentWindow.SetData(i,ls_result[i-1][1]);
			$('#form_calendar')[0].contentWindow.document.getElementById("td_day_"+i).ondblclick = function () {uf_ondblclick()};
		}
	}

}


function uf_ondblclick(){
 	_GS.FindGS02041(window, "GS02041", "", [S_DEPT_ID.value, is_date]);
}


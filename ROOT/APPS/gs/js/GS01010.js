//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : 대상자 정보관리
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
// var DW_ITEMSTATUS_notmodified 	= 0;
// var DW_ITEMSTATUS_datamodified = 1;
// var DW_ITEMSTATUS_new         	= 2;
// var DW_ITEMSTATUS_newmodified 	= 3;
//========================================================================================
// var p_params; // 팝업으로 떴을때 처리 위한 변수
// var p_target_div = '';
// var p_cust_code = '';
var p_cust_id = '';
var p_dept_id = 0;

var is_client_div = 'O';	//I:매입, O:매출, J:지사, E:기타
var is_new = false;
var _ParmData  = "";

function x_InitForm2(){
	if(!_X.IsHeadoffice())	{
		$("#btn_finddept").hide();
	}

	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "gs", "GS01010", "GS01010|GS01010_C01", true,  true);
	_X.InitGrid(grid_2, "dg_2", "100%", "100%", "gs", "GS01010", "GS01010|GS01010_U02", false,  true);
	_X.InitGrid(grid_excel, "dg_excel", "100%", "100%", "gs", "GS01010", "GS01010|GS01010_R01", false,  false);

	// if(_X.HaveActAuth("시스템관리자")) {
	// 	is_cust = '%';
	// 	is_custname = '';
	// }

  MANAGE_FIX.value = "L";
  S_TO_DATE.value = _X.ToString(_X.LastDate(_X.ToString(_X.GetSysDate(),'yyyymmdd')),"yyyy-mm-dd");

	var la_DEPT = _X.XmlSelect("com", "COMMON", "CODE_DEPT_AUTHORITY_R01" , new Array(mytop._UserID), 'json2');
	_X.DDLB_SetData(S_DEPT_ID, la_DEPT, null, true, (_X.IsHeadoffice() ? true : false));
	S_DEPT_ID.value = mytop._DeptCode;

	if(_params != "null"){
		//$("#top_condition,#grid_1,#xBtnRetrieve,#xBtnDelete,#xBtnExcel").hide();
		$("#xBtnRetrieve,#xBtnAppend,#xBtnDelete,#xBtnDuplicate,#xBtnExcel").hide();
		$("#btn_finddept").hide();
		_X.FormSetDisable([S_DEPT_ID, S_CUST_NAME], true);

		var ls_par = new Array();
		ls_par = _params.split("@");

		p_dept_id = ls_par[0];
		p_cust_id = ls_par[1];

    S_DEPT_ID.value = p_dept_id;
    S_CUST_NAME.value = p_cust_id;
    _X.FormSetDisable([S_DEPT_ID, S_CUST_NAME], true);
	}

  // if(typeof(_Caller._FindCode.findcode)!="undefined") {
		// //$("#top_condition,#grid_1,#xBtnRetrieve,#xBtnDelete,#xBtnExcel").hide();

  //   p_dept_id    = _Caller._FindCode.findcode[0];
  //   p_cust_id    = _Caller._FindCode.findcode[1];

  //   S_DEPT_ID.value = p_dept_id;
  //   S_CUST_NAME.value = p_cust_id;
  //   _X.FormSetDisable([S_DEPT_ID, S_CUST_NAME], true);
  // }

}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {

		_X.DDLB_SetData(SEX, _CB_SEX_DIV, null, false, false);

		var la_GUBUN_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0109", 'Y'), 'json2');
		_X.DDLB_SetData(CUST_GUBUN_DIV, la_GUBUN_DIV, null, false, false);

		var la_REDUCTION_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0110", 'Y'), 'json2');
		_X.DDLB_SetData(REDUCTION_DIV, la_REDUCTION_DIV, null, false, false);

		var la_APP_RATE_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0111", 'Y'), 'json2');
		_X.DDLB_SetData(APP_RATE_DIV, la_APP_RATE_DIV, null, false, false);

		var la_RELATION_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0116", 'Y'), 'json2');
		_X.DDLB_SetData(RELATION_DIV, la_RELATION_DIV, null, false, false);

		var la_CUST_LEVEL_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0125", 'Y'), 'json2');
		_X.DDLB_SetData(CUST_LEVEL_DIV, la_CUST_LEVEL_DIV, null, false, false);

		dg_1.SetAutoFooterValue("CUST_NAME", function(idx, ele) {
			var li_cnt = dg_1.RowCount();
			return "전체 : " + li_cnt.toString() + "명";
		});

		dg_1.SetAutoFooterValue("MANAGE_STATUS", function(idx, ele) {
			var datas = dg_1.GetData();
			var li_a   = 0;
			var li_b   = 0;
			var i, ii;
			for ( i = 0, ii = datas.length; i < ii; i++ ) {
				if(_X.ToInt(datas[i].CARE_EDATE_DDAY) > 0) li_a++;
				if(_X.ToInt(datas[i].CARE_EDATE_DDAY) < 0) li_b++;
			}
			return "기간내 : " + li_a.toString() + "명,  만료 : " + li_b.toString() + "명";
		});

		_X.FormSetDisable([RELATION_DIV], true);

		setTimeout("x_DAO_Retrieve()",100);
	}
}

function x_DAO_Retrieve2(a_dg){
	var ls_allchk = ( CHK_ALLDATE.checked ? "Y" : "N" );
	var ls_custid = p_cust_id || "9999999999999";
	var ls_from = _X.StrPurify(S_FROM_DATE.value);
	var ls_to   = _X.StrPurify(S_TO_DATE.value);

	dg_2.Reset();
	dg_1.Retrieve([S_CUST_NAME.value, S_DEPT_ID.value, ls_allchk, ls_from, ls_to, ls_custid]);
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id) {
		case	"S_DEPT_ID":
					//_X.SetDept();
					//_X.SaveDept(a_val);
		case	"S_FROM_DATE":
		case	"S_TO_DATE":
		case	"CHK_ALLDATE":
					x_DAO_Retrieve();
					break;
		case	"REMARK":
			 		var li_cust = dg_1.GetItem(dg_1.GetRow(), "CUST_ID");
			 		var ls_dept = S_DEPT_ID.value;

			 		if(li_cust>0 && ls_dept) {
			 			_GS.UpdateCustRemarkDept(ls_dept, li_cust, a_val);
			 		}
					break;
		case	"CUST_NAME":
		case	"BIRTH_DATE":
					dg_1.SetItem(dg_1.GetRow(), "JUMIN_CHK_YN","N");
					break;
	}
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	var cData1 	= dg_1.GetChangedData();

	if(cData1.length > 0){
			// 저장후에 계약생성을 위한 파라메터값(신규건)
		_ParmData  = "";

		for (i=0 ; i < cData1.length; i++){
			if(cData1[i].job == 'D') continue;

			if(dg_1.GetItem(cData1[i].idx, "CUST_NAME") == "" || dg_1.GetItem(cData1[i].idx, "CUST_NAME") == null) {
				dg_1.SetRow(cData1[i].idx);
				_X.MsgBox("확인", "이름(고객명)을 입력하여주시기 바랍니다.");
				return false;
			}

			if(dg_1.GetItem(cData1[i].idx, "JUMIN_CHK_YN") == "N"){
				dg_1.SetRow(cData1[i].idx);
				_X.MsgBox("대상자 중복확인을 해주시기 바랍니다.");
				return false;
			}

			if(cData1[i].job == 'I'){
				// 저장후에 계약생성을 위한 파라메터값
				if(_ParmData!="") {_ParmData += "。";}
				_ParmData += dg_1.GetItem(cData1[i].idx, "CUST_ID");
			}
		}
	}

	return true;
}

//저장후 호출
function x_DAO_Saved2(){
  // 계약 없을 때, 계약 생성
	if(_ParmData != "") {
		var paramNum = _X.ToInt(_X.XmlSelect('sm', 'XG_GRID', 'PARAM_NUM', [], 'Array')[0][0]);
		_X.EP("sm", "PROC_XM_PARAM_DATA", [paramNum, "GS01010", mytop._UserID, _ParmData]);

		var li_rtn = _X.ExecProc('gs', 'SP_SERVICE_CONTRACT_CREATE', new Array(S_DEPT_ID.value, paramNum, "GS01010", mytop._UserID, mytop._CliUserID) );
		if(li_rtn > 0){
			_X.Noty("첫번째 계약을 자동으로 생성했습니다.", null, null, 1500);
		}else{
			_X.MsgBox("계약 자동 생성중 오류가 발생했습니다. '서비스 계약관리'에서 계약을 추가하신 후, 일정을 입력하세요.");
		}
	}

	// 담당센터 설정
	_X.EP("gs", "PROC_CODE_CUSTINDEPT", [dg_1.GetItem(dg_1.GetRow(), "CUST_ID"), mytop._UserID, mytop._ClientIP]);

  is_new = false;
}

function x_DAO_Insert2(a_dg, row){
	if(a_dg.id == 'dg_1') {
	  if (is_new) {
	  	_X.MsgBox("추가된 건을 저장 후 작업해 주세요.\n한 건씩만 추가 가능 합니다.");
			return 0;
	  }

	  // //데이타가 변경된 경우 저장
	  // if(dg_1.IsDataChanged()) {
	  // 	x_DAO_Save();
	  // }
	}

  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
	if(a_dg.id == 'dg_1') {
	  dg_1.SetItem(rowIdx, "CUST_ID"     , _GS.GetDataCustId());
	  dg_1.SetItem(rowIdx, "SEX"         , "F");
		dg_1.SetItem(rowIdx, "CLIENT_DIV"  , "O");
		dg_1.SetItem(rowIdx, "CHK_ADDR"    , "Y");
		dg_1.SetItem(rowIdx, "DEPT_ID"     , S_DEPT_ID.value);
		dg_1.SetItem(rowIdx, 'MEMBER_ID'   , mytop._UserID);
		dg_1.SetItem(rowIdx, "JUMIN_CHK_YN", "N");
		var ls_dept = _X.XmlSelect("com", "COMMON", "GET_DEPT_NAME", [S_DEPT_ID.value], "array");
		if(ls_dept.length > 0) {
			dg_1.SetItem(rowIdx,"DEPT_NAME", ls_dept[0][0]);
		}
		is_new = true;
	}

}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
	if(a_dg.id == 'dg_1') {
	  dg_1.SetItem(rowIdx, "CUST_ID"     , _GS.GetDataCustId());
		dg_1.SetItem(rowIdx, 'MEMBER_ID'   , mytop._UserID);
		dg_1.SetItem(rowIdx, "JUMIN_CHK_YN", "N");
		is_new = true;
	}
}

function x_DAO_Delete2(a_dg, row){
	if(a_dg.id == 'dg_1') {
	  if(dg_1.GetItem(row, "job") != "I") {
		  if(_X.MsgBoxYesNo("고객 삭제", "지사에 등록된 고객을 삭제합니다.<br> (고객정보가 삭제되지 않습니다.)") == 1) {
         var li_cust = dg_1.GetItem(dg_1.GetRow(),"CUST_ID");
         var ls_result = _X.ES("gs", "GS01010", "DEL_CODE_CUSTINDEPT", [S_DEPT_ID.value, li_cust]);
         x_DAO_Retrieve();
         return 0;
       }
	  } else {
	  	is_new = false;
	  }
	}

	return 100;
}

function x_DAO_Excel2(a_dg){
	var ls_allchk = ( CHK_ALLDATE.checked ? "Y" : "N" );
	var ls_custid = p_cust_id || "9999999999999";
	var ls_from = _X.StrPurify(S_FROM_DATE.value);
	var ls_to   = _X.StrPurify(S_TO_DATE.value);

	dg_excel.Retrieve([S_CUST_NAME.value, S_DEPT_ID.value, ls_allchk, ls_from, ls_to, ls_custid]);
	dg_excel.ExcelExport(true);
	return 0;
	//return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	if(a_dg.id == "dg_1") {
	  // 경감코드 수정시 비율 자동 세팅
	  if(a_col == 'CUST_GUBUN_DIV' || a_col == 'REDUCTION_DIV'){
	  	uf_set_apprate();
	  }

	  switch(a_col) {
		  // 등급과 경감비율 수정시 월한도금액, 본인부담금 한도금액 재계산
	  	case	"CUST_LEVEL_DIV":
	  	case	"REDUCTION_DIV":
	  	case	"APP_RATE_DIV":
				  	var ls_level = dg_1.GetItem(a_row, "CUST_LEVEL_DIV");
				  	var ls_rate  = dg_1.GetItem(a_row, "APP_RATE_DIV");
				  	if(ls_level!="") {
					  	var li_rtn = _GS.GetMonthLimitAmt(ls_level, ls_rate);
					  	dg_1.SetItem(a_row, "M_LIMIT_AMT", li_rtn.limit_amt);
					  	dg_1.SetItem(a_row, "REQ_LIMIT_AMT", li_rtn.req_limit_amt);
				  	}
	  				break;
	  	case	"ADDR2":
	  				uf_sync_addr(a_row);
	  				break;
	  	case	"MANAGE_NO1":
	  	case	"MANAGE_NO2":
	  				dg_1.SetItem(a_row, 'MANAGE_NO', dg_1.GetItem(a_row, 'MANAGE_NO1') + dg_1.GetItem(a_row, 'MANAGE_NO2'));
	  				break;
	  	case	"INPUT_RRN_NO":
	  				var ls_result = _X.XmlSelect("com", "COMMON", "GET_ENC_RRN", [a_newvalue], "array");
	  				if(ls_result.length > 0) {
	  					dg_1.SetItem(a_row, 'RRN_NO', ls_result[0][0]);
	  				}

				    var ls_birth = '19' + a_newvalue.substr(0,6);
				    dg_1.SetItem(a_row,'BIRTH_DATE',_X.Todate(ls_birth,'yyyymmdd'));

				    dg_1.SetItem(a_row,'CUST_CODE',a_newvalue.substr(0,7)+_X.Lpad( dg_1.GetItem(a_row, "CUST_ID"), 6, '0'));
	  				break;
	  }
	}

	if(a_dg.id == "dg_2" && a_col == "USE_YN" && a_newvalue=="Y") {
		for(var i = 1; i <= dg_2.RowCount(); i++){
			if(i != a_row) dg_2.SetItem(i, "USE_YN", "N");
		}
	}

}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	if(a_dg.id == 'dg_1') {
		if (a_oldrow > 0 && (dg_1.GetItem(a_oldrow, "job") == "I" || dg_1.GetItem(a_oldrow, "job") == "U")) {
			if(_X.MsgBoxYesNo("변경된 자료가 있습니다 \r\n계속 진행 하시겠습니까?")==2) {return false;}
		}
	}
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
	if(a_dg.id == 'dg_1') {
		dg_2.Reset();

    if (a_newrow > 0 && dg_1.GetItem(a_newrow, "job") != "I")    {	// 신규가아닌경우
      // if (jf_dw_updateconfirm() == 3)      {
      //   dwc.SetActionCode(1);
      //   return;
      // }

	    uf_service_block("Y");
			REMARK.value = uf_getremark();

			dg_2.Retrieve([dg_1.GetItem(a_newrow, "CUST_ID")]);
    } else { // 신규입력
    	uf_service_block("N");
			REMARK.value = "";
    }
	}
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
	if(a_dg.id == 'dg_1') {
		is_new = false;

		uf_Set_Color();
	}
}

function uf_Set_Color() {
	var s_red = { "color": "#FF0000" };
	var s_blk = { "color": "#000000" };
	var s_gry = { "color": "#C8C8C8" };

//( li_ord=0 && care_edate_dday <= 90 && care_edate_dday>0) ? s_red : ( li_ord=0 && care_edate_dday > 90 ) ? s_blk : s_gry

	for(var i = 1; i <= dg_1.RowCount(); i++){
		var ll_dday = _X.ToInt(dg_1.GetItem(i, "CARE_EDATE_DDAY"));
		var li_ord  = _X.ToInt(dg_1.GetItem(i, "SORT_ORDER"));
		dg_1.SetCellStyle(i, "CUST_NAME"    , ( ll_dday <= 90 && ll_dday > 0 )  ? s_red : s_blk);

		dg_1.SetCellStyle(i, "BIRTH_DATE"   , ( li_ord==0 && ll_dday <= 90 && ll_dday>0) ? s_red : ( li_ord==0 && ll_dday > 90 ) ? s_blk : s_gry);
		dg_1.SetCellStyle(i, "MANAGE_STATUS", ( li_ord==0 && ll_dday <= 90 && ll_dday>0) ? s_red : ( li_ord==0 && ll_dday > 90 ) ? s_blk : s_gry);
	}
}


function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
	switch(_FindCode.finddiv) {
		case	"ZIP_CODE" :
					var ll_row = dg_1.GetRow();
					dg_1.SetItem(ll_row, "ZIP_CODE"   , a_retVal.zipNo);	// 주소찾이만 이런형식으로 받음
					dg_1.SetItem(ll_row, "ADDR1"      , a_retVal.roadAddrPart1 + a_retVal.roadAddrPart2);
					dg_1.SetItem(ll_row, "ADDR2"      , a_retVal.addrDetail);
					dg_1.SetItem(ll_row, "DM_ZIP_CODE", a_retVal.zipNo);
					dg_1.SetItem(ll_row, "DM_ADDR1"   , a_retVal.roadAddrPart1 + a_retVal.roadAddrPart2);
					dg_1.SetItem(ll_row, "DM_ADDR2"   , a_retVal.addrDetail);
					ADDR2.focus();
					break;
		case	"DM_ZIP_CODE" :
					var ll_row = dg_1.GetRow();
					dg_1.SetItem(ll_row, "DM_ZIP_CODE", a_retVal.zipNo);
					dg_1.SetItem(ll_row, "DM_ADDR1"   , a_retVal.roadAddrPart1 + a_retVal.roadAddrPart2);
					dg_1.SetItem(ll_row, "DM_ADDR2"   , a_retVal.addrDetail);
					dg_1.SetItem(ll_row, "CHK_ADDR", "N");
					dg_1.SetItem(ll_row, "COPY_PRO_ADDR", "N");
					DM_ADDR2.focus();
					break;
	}

	if(_FindCode.returnValue == null) return;

	switch(_FindCode.finddiv) {
		case	"S_DEPT_ID" :
					S_DEPT_ID.value = _FindCode.returnValue.DEPT_ID;
					x_DAO_Retrieve();
					break;
		case	"MANAGE_OFFICE_DIV" :
					var ll_row = dg_1.GetRow();
					dg_1.SetItem(ll_row, "MANAGE_OFFICE_DIV"     , _FindCode.returnValue.MANAGE_OFFICE_DIV);
					dg_1.SetItem(ll_row, "MANAGE_OFFICE_DIV_NAME", _FindCode.returnValue.MANAGE_OFFICE_DIV_NAME);
					break;
	}
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

// 지사찾기
function uf_findDept() {
	_X.FindDeptCode2('S_DEPT_ID','');
}

//우편번호찾기
function uf_ZipFind(a_param) {
	if(dg_1.RowCount() == 0) return;
	_X.FindStreetAddr(window,a_param,"", "");
}

//보장기관
function uf_findOffice() {
	if(dg_1.RowCount() == 0) return;
	_X.FindComdivCode('MANAGE_OFFICE_DIV','');
}

function uf_set_apprate() {
  var ls_row = dg_1.GetRow();
  var ls_gubun = dg_1.GetItem(ls_row,'CUST_GUBUN_DIV');
  var ls_reduc = dg_1.GetItem(ls_row,'REDUCTION_DIV');

  if(ls_gubun == 'A') {       //노인장기수급자
      if(ls_reduc == 'B') {   //경감사유 없음
        dg_1.SetItem(ls_row,'APP_RATE_DIV','0010');    //적용요율 0%
      } else if(ls_reduc == 'M') { // 의료급여
        dg_1.SetItem(ls_row,'APP_RATE_DIV','0100');    //적용요율 7.5%
      } else { // 일반등급 (G)
    		dg_1.SetItem(ls_row,'APP_RATE_DIV','0200');    //적용요율 15%
    	}
  } else if(ls_gubun == 'G') {  //일반
    dg_1.SetItem(ls_row,'APP_RATE_DIV','1000');   // 적용요율 100%
  }
}

function uf_sync_addr(Row){
   var ls_row = Row;
   var ls_check = dg_1.GetItem(ls_row,'CHK_ADDR');
   var ls_check_pro = dg_1.GetItem(ls_row,'COPY_PRO_ADDR');

   if(ls_check == 'Y') {
     var ls_zip   = dg_1.GetItem(ls_row,'ZIP_CODE');
     var ls_addr1 = dg_1.GetItem(ls_row,'ADDR1');
     var ls_addr2 = dg_1.GetItem(ls_row,'ADDR2');
     dg_1.SetItem(ls_row,"ZIP_CODE",ls_zip);
     dg_1.SetItem(ls_row,"DM_ADDR1",ls_addr1);
     dg_1.SetItem(ls_row,"DM_ADDR2",ls_addr2);
   }
}

function uf_service_block(a_Tag){
 	var _ITEMS_I = new Array(MANAGE_NO1, MANAGE_NO2, START_DATE, END_DATE, CUST_LEVEL_DIV, REDUCTION_DIV, APP_RATE_DIV, MANAGE_OFFICE_DIV_NAME, OFFICE_AMT, REQ_LIMIT_AMT);

  if(a_Tag == "Y") {
	  $("#t_service").show();
	  $("#btn_officefind").hide();
	  _X.FormSetDisable(_ITEMS_I, true);
  } else {
	  $("#t_service").hide();
	  $("#btn_officefind").show();
	  _X.FormSetDisable(_ITEMS_I, false);
  }
}

// 중복확인
function uf_chk_jumin(){
	var li_row = dg_1.GetRow();
	// 이름, 생년월일로 변경
	var ls_name  = _X.Trim(dg_1.GetItem(li_row,'CUST_NAME'));
  var ls_birth = _X.ToString(dg_1.GetItem(li_row,'BIRTH_DATE'), 'yyyymmdd');

  if(ls_name!=""&&ls_birth!=""){
  	var ls_results = _X.XmlSelect("gs", "GS01010", "CHK_CODE_CUST", [S_DEPT_ID.value, ls_name, ls_birth], "array");
  	var li_incnt = ls_results[0][0];
  	var li_outcnt = ls_results[0][1];

  	if(li_incnt>0) {
  		_X.MsgBox("센터에 이미 등록된 대상자 입니다. 전체 기간으로 다시 조회하시거나 이름으로 검색하세요.");
  		//x_DAO_Retrieve();
  		return;
  	} else {
  		if(li_outcnt>0) {
  			uf_extend_custdept(ls_name, ls_birth);
  		} else {
  			_X.MsgBox("이름, 생일이 중복된 대상자가 없습니다.");
  			dg_1.SetItem(li_row, "JUMIN_CHK_YN","Y");
  		}
  	}
  }else{
    _X.MsgBox("이름과 생년월일을 입력하세요.");
  }
}

function uf_extend_custdept(as_name, as_birth){
 	var ls_two = _X.XmlSelect("gs", "GS01010", "CODE_CUST_R01", [as_name, as_birth], "array");
	var ls_msg = ls_two[0][1] + " 센터에 "+ls_two[0][2]+"님("+ls_two[0][3]+")("+(ls_two[0][4]!=""?"연락처뒷자리:"+ls_two[0][4]:"")+", 주소:"+ls_two[0][5]+")으로 등록된 대상자 입니다.\n현재 지사에 추가등록(지사이동)을 하시겠습니까?";

	if(_X.MsgBoxYesNo("다른센터에 이미 등록된 고객입니다.",ls_msg)==1) {
		//_X.ES("gs", "GS01010", "CODE_CUSTINDEPT_C01", [S_DEPT_ID.value, ls_two[0], mytop._UserID, mytop._ClientIP]);
		_X.EP("gs", "PROC_CUST_MOVE_DEPT", [S_DEPT_ID.value, ls_two[0][0], mytop._UserID, mytop._ClientIP]);
  }
  x_DAO_Retrieve();
}

// 보호자목록
function uf_protector(){
	if(dg_1.RowCount() <= 0) return;
	var ls_custid = dg_1.GetItem(dg_1.GetRow(), "CUST_ID");
 	_GS.FindGS01012(window, "GS01012","", [ls_custid]);
}

// 보호자주소적용
function uf_set_protector(a_dg){
	var ls_pro_seq = "";
	var ls_pro_name = "";
	var ls_relation_div = "";
	var ls_email = "";
	var ls_phone = "";
	var ls_hp = "";
	var ls_zip_code = "";
	var ls_addr1 = "";
	var ls_addr2 = "";

	for(var i=1; i<=a_dg.RowCount(); i++) {
		if(a_dg.GetItem(i, "SELECTED") == "Y") {
			ls_pro_seq      = a_dg.GetItem(i, "PRO_SEQ");
			ls_pro_name     = a_dg.GetItem(i, "PRO_NAME");
			ls_relation_div = a_dg.GetItem(i, "RELATION_DIV");
			ls_email        = a_dg.GetItem(i, "EMAIL");
			ls_phone        = a_dg.GetItem(i, "PHONE");
			ls_hp           = a_dg.GetItem(i, "HP");
			ls_zip_code     = a_dg.GetItem(i, "ZIP_CODE");
			ls_addr1        = a_dg.GetItem(i, "ADDR1");
			ls_addr2        = a_dg.GetItem(i, "ADDR2");
			break;
		}
	}

	var li_row   = dg_1.GetRow();
	dg_1.SetItem(li_row, "PRO_SEQ"     , ls_pro_seq);
	dg_1.SetItem(li_row, "PRO_NAME"    , ls_pro_name);
	dg_1.SetItem(li_row, "RELATION_DIV", ls_relation_div);
	dg_1.SetItem(li_row, "PRO_EMAIL"   , ls_email);
	dg_1.SetItem(li_row, "PRO_PHONE"   , ls_phone);
	dg_1.SetItem(li_row, "PRO_HP"      , ls_hp);
	dg_1.SetItem(li_row, "PRO_ZIP_CODE", ls_zip_code);
	dg_1.SetItem(li_row, "PRO_ADDR1"   , ls_addr1);
	dg_1.SetItem(li_row, "PRO_ADDR2"   , ls_addr2);
}

// 주소복사
function uf_copy_addr(a_flag) {
  if(a_flag == 'CHK_ADDR'){
  	var ls_row   = dg_1.GetRow();
    var ls_zip   = dg_1.GetItem(ls_row, 'ZIP_CODE');
    var ls_addr1 = dg_1.GetItem(ls_row, 'ADDR1');
    var ls_addr2 = dg_1.GetItem(ls_row, 'ADDR2');
    dg_1.SetItem(ls_row, 'DM_ZIP_CODE', ls_zip);
    dg_1.SetItem(ls_row, 'DM_ADDR1', ls_addr1);
    dg_1.SetItem(ls_row, 'DM_ADDR2', ls_addr2);
    dg_1.SetItem(ls_row, 'COPY_PRO_ADDR', 'N');
  }
  if(a_flag == 'COPY_PRO_ADDR'){
  	var ls_row = dg_1.GetRow();
    var ls_zip = dg_1.GetItem(ls_row, 'PRO_ZIP_CODE');
    var ls_addr1 = dg_1.GetItem(ls_row, 'PRO_ADDR1');
    var ls_addr2 = dg_1.GetItem(ls_row, 'PRO_ADDR2');
    dg_1.SetItem(ls_row, 'DM_ZIP_CODE', ls_zip);
    dg_1.SetItem(ls_row, 'DM_ADDR1', ls_addr1);
    dg_1.SetItem(ls_row, 'DM_ADDR2', ls_addr2);
    dg_1.SetItem(ls_row, 'CHK_ADDR', 'N');
  }
}

// 요보사등록
function uf_open_member() {
	mytop._X.MDI_Open('GS02010', mytop._MyPgmList['GS02010'].PGM_NAME, '/Mighty/template/XG_Slick1.jsp?pcd=GS02010');
}

// 서비스계약관리
function uf_service_contract() {
	var ls_custid   = "";
	var ls_custname = "";
	var ls_deptid   = "";

	if(dg_1.GetRow() > 0) {
		ls_custid   = dg_1.GetItem(dg_1.GetRow(), "CUST_ID");
		ls_custname = dg_1.GetItem(dg_1.GetRow(), "CUST_NAME");
		ls_deptid   = S_DEPT_ID.value;
	}

	mytop._X.MDI_Open('GS01020', mytop._MyPgmList['GS01020'].PGM_NAME, '/Mighty/template/XG_Slick1.jsp?pcd=GS01020&params='+ls_custid+'@'+ls_custname+'@'+ls_deptid);
}

// 월별일정변경
function uf_month_change() {
	var ls_custid   = "";
	var ls_custname = "";
	var ls_deptid   = "";

	if(dg_1.GetRow() > 0) {
		ls_custid   = dg_1.GetItem(dg_1.GetRow(), "CUST_ID");
		ls_custname = dg_1.GetItem(dg_1.GetRow(), "CUST_NAME");
		ls_deptid   = S_DEPT_ID.value;
	}

	var ls_dept_name = S_DEPT_ID.options[S_DEPT_ID.selectedIndex].text ;

	if(ls_dept_name.indexOf("주야간")>0) {
		mytop._X.MDI_Open('GS01050', mytop._MyPgmList['GS01050'].PGM_NAME, '/Mighty/template/XG_Slick1.jsp?pcd=GS01050&params='+ls_custid+'@'+ls_custname+'@'+ls_deptid);
	} else {
		mytop._X.MDI_Open('GS01040', mytop._MyPgmList['GS01040'].PGM_NAME, '/Mighty/template/XG_Slick1.jsp?pcd=GS01040&params='+ls_custid+'@'+ls_custname+'@'+ls_deptid);
	}
}

// 비고조회
function uf_getremark(){
	var ls_dept = S_DEPT_ID.value;
	var li_cust = dg_1.GetItem(dg_1.GetRow(), "CUST_ID");

	if(ls_dept && li_cust) {
		var ls_result = _X.XmlSelect("gs", "GS_COMMON", "CODE_CUSTINDEPT_R01", [ls_dept, li_cust], "array");

		if(ls_result.length > 0) {
			return ls_result[0][0];
		} else {
			return '';
		}
	} else {
		return '';
	}
}

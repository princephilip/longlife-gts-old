//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : [GS01070] 고객별 서비스 이용내역
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
function x_InitForm2(){
	if(!_X.IsHeadoffice())	{
		$("#btn_finddept").hide();
	}

	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "gs", "GS01070", "GS01070|GS01070_R01", false, false);

	var la_DEPT = _X.XmlSelect("com", "COMMON", "CODE_DEPT_AUTHORITY_R01" , new Array(mytop._UserID), 'json2');
	_X.DDLB_SetData(S_DEPT_ID, la_DEPT, null, true, (_X.IsHeadoffice() ? true : false));
	S_DEPT_ID.value = mytop._DeptCode;

}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {

		var la_chk = [{"code":"1","label":"제공"},{"code":"0","label":"미제공"}];
		_X.DDLB_SetData(S_CHECK_YN, la_chk, null, false, true);

		var la_FAMILY_YN = [{"code":"Y","label":"예"},{"code":"N","label":"아니오"}];
		dg_1.SetCombo("FAMILY_YN", la_FAMILY_YN);

		var la_CUST_LEVEL_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0125", 'Y'), 'json2');
		dg_1.SetCombo("CUST_LEVEL_DIV", la_CUST_LEVEL_DIV);

		var la_REDUCTION_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0110", 'Y'), 'json2');
		dg_1.SetCombo("REDUCTION_DIV", la_REDUCTION_DIV);

		var la_SERVICE_YN = [{"code":"1","label":" "},{"code":"0","label":"미이용"}];
		dg_1.SetCombo("SERVICE_YN", la_SERVICE_YN);

		//dg_1.SetGroup(["DEPT_NAME"], true, [{sortAsc: true, sortCol: {field: "DEPT_NAME"}},{sortAsc: true, sortCol: {field: "CUST_NAME"}}]);

		dg_1.SetAutoFooterValue("CUST_CODE", function(idx, ele) {
			return "합계 : " + dg_1.RowCount().toString() + " 건 ";
		});

		dg_1.SetAutoFooterValue("PRODUCT_NAME", function(idx, ele) {
			var datas = dg_1.GetData();
			var li_a   = 0;
			var li_b   = 0;
			var i, ii;
			for ( i = 0, ii = datas.length; i < ii; i++ ) {
				if(datas[i].SERVICE_YN == "1") {
					li_a++;
				} else {
					li_b++;
				}
			}
			return "(제공 : " + li_a.toString() + "건,  미제공 : " + li_b.toString() + "건)";
		});

		dg_1.SetAutoFooterValue("PAY_AMT", function(idx, ele) {
			var datas = dg_1.GetData();
			var li_a   = 0;
			var i, ii;
			for ( i = 0, ii = datas.length; i < ii; i++ ) {
				if(datas[i].SERVICE_YN == "1") {
					li_a += _X.ToInt(datas[i].PAY_AMT);
				}
			}
			return _X.FormatComma(li_a);
		});

		dg_1.SetAutoFooterValue("REQ_AMT", function(idx, ele) {
			var datas = dg_1.GetData();
			var li_a   = 0;
			var i, ii;
			for ( i = 0, ii = datas.length; i < ii; i++ ) {
				if(datas[i].SERVICE_YN == "1") {
					li_a += _X.ToInt(datas[i].REQ_AMT);
				}
			}
			return _X.FormatComma(li_a);
		});

		setTimeout("x_DAO_Retrieve()",100);
	}
}

function x_DAO_Retrieve2(a_dg){
	var ls_frdt   = _X.StrPurify(S_FROM_DATE.value);
	var ls_todt   = _X.StrPurify(S_TO_DATE.value);
 	dg_1.Retrieve([S_DEPT_ID.value, S_CHECK_YN.value, '%', S_CUST_FIND.value+'%', ls_frdt, ls_todt, S_EMP_FIND.value+'%']);
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id) {
		case	"S_DEPT_ID":
		case	"S_FROM_DATE":
		case	"S_TO_DATE":
		case	"S_EMP_FIND":
		case	"S_CHECK_YN":
		case	"S_CUST_FIND":
					x_DAO_Retrieve();
					break;
	}
}


function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

//저장후 호출
function x_DAO_Saved2(){
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
	if(dg_1.RowCount()<=0) {
		_X.MsgBox("출력할 데이터가 없습니다.");
		return 0;
  }

	var ls_frdt   = _X.StrPurify(S_FROM_DATE.value);
	var ls_todt   = _X.StrPurify(S_TO_DATE.value);



	 _X.SetiReport("gs", "GS01070R",	"as_deptid="  		+ S_DEPT_ID.value
																	+ "&as_serviceyn="  + S_CHECK_YN.value
																	+ "&as_member="   	+ S_EMP_FIND.value +'%'
																	+ "&as_cust="  			+ S_CUST_FIND.value +'%'
																	+ "&as_fromdate="  	+ ls_frdt
																	+ "&as_todate="  		+ ls_todt );
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
	if(_FindCode.returnValue == null) return;

	switch(_FindCode.finddiv) {
		case	"S_DEPT_ID" :
					S_DEPT_ID.value = _FindCode.returnValue.DEPT_ID;
					x_DAO_Retrieve();
					break;
	}
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

// 지사찾기
function uf_findDept() {
	_X.FindDeptCode2('S_DEPT_ID','');
}

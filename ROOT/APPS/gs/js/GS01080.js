//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : 본인부담금 수금 등록
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
function x_InitForm2(){
	if(!_X.IsHeadoffice())	{
		$("#btn_finddept").hide();
	}

	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "gs", "GS01080", "GS01080|GS01080_R01", false, false);
	_X.InitGrid(grid_2, "dg_2", "100%", "100%", "gs", "GS01080", "GS01080|GS01080_R02", false, false);
	_X.InitGrid(grid_3, "dg_3", "100%", "100%", "gs", "GS01080", "GS01080|GS01080_C03", false, true);

	var la_DEPT = _X.XmlSelect("com", "COMMON", "CODE_DEPT_AUTHORITY_R01" , new Array(mytop._UserID), 'json2');
	_X.DDLB_SetData(S_DEPT_ID, la_DEPT, null, true, false);
	S_DEPT_ID.value = mytop._DeptCode;

}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {

		var la_CUSTDIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0110", 'Y'), 'json2');
		_X.DDLB_SetData(S_CUST_DIV, la_CUSTDIV, null, false, true);

		dg_3.SetAutoFooterValue("DEPOSITNO", function(idx, ele) {
			var datas = dg_3.GetData();
			var ll_sum   = 0;
			for ( i = 0, ii = datas.length; i < ii; i++ ) {
				ll_sum += _X.ToInt(datas[i].CASH_AMT) + _X.ToInt(datas[i].CARD_AMT) + _X.ToInt(datas[i].BANK_AMT) + _X.ToInt(datas[i].VBANK_AMT) + _X.ToInt(datas[i].JIRO_AMT);
			}
			return " 합계 :  " + _X.FormatComma(ll_sum);
		});

		//본사관리 권한 이상의 사용자만 체크란 활성화(2009.08.17)
		var ls_rtn = _X.XmlSelect("com", "COMMON", "GET_AUTHORITY_DIV", [mytop._UserID], "array")[0][0];
		if(ls_rtn >= "0400") {
			//fdw_1.Modify("Check_YN.Protect=1")
		}

		xe_EditChanged(S_DEPT_ID, S_DEPT_ID.value);

		//setTimeout("x_DAO_Retrieve()",100);
	}
}

function x_DAO_Retrieve2(a_dg){
  uf_Service_Account_Create();

	var ls_ym   = _X.StrPurify(sle_basicmonth.value);
	var ls_cust = "%"+ S_CUST_NAME.value + "%";

	var li_crow = 0;
	if(dg_1.RowCount() > 0) li_crow = dg_1.GetRow();

 	dg_1.Retrieve([S_DEPT_ID.value, ls_ym, ls_cust, S_CUST_DIV.value]);

 	if(li_crow > 0) {
 		dg_1.SetRow(li_crow);
 	}
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id) {
		case	"S_DEPT_ID":
					var la_DEPOSITNO = _X.XmlSelect("com", "COMMON", "DDLB_DEPT_DEPOSITNO" , [S_DEPT_ID.value, "%"], 'json2');
					dg_3.SetCombo("DEPOSITNO", la_DEPOSITNO);
		case	"sle_basicmonth":
		case	"S_CUST_NAME":
		case	"S_CUST_DIV":
					x_DAO_Retrieve();
					break;
	}
}


function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	var cData 	= dg_3.GetChangedData();

	if(cData.length > 0){
		//var ls_dept = S_DEPT_ID.value;

		for (i=0 ; i < cData.length; i++){
			if(cData[i].job == 'D') continue;

			var ls_yymm = dg_3.GetItem(cData[i].idx, "REQ_YYMM");
			if(ls_yymm == null || ls_yymm == "" || ls_yymm.length < 6) {
				_X.MsgBox("확인", "청구년월을 입력 하십시요.");
				return false;
			}

			// if(cData[i].job == 'I') {

   //  		if(ptdwo.GetRowStatus(fdw_1.dw, i) == 1 || ptdwo.GetRowStatus(fdw_1.dw, i) == 3){ 
   //  		  // 같은 날짜에 중복날짜 체크
   //  		  var ll_findrow = fdw_1.dw.find("string(pay_date,'YYYY-MM-DD') = '" + ptcomm.ToString(fdw_1.GetItemDate(i, "pay_date"),'YYYY-MM-DD') + "'",0,9999999);
   //  		  if(ll_findrow>0&&ll_findrow!=i) { 
		 //      	if(!confirm("하루에 한껀의 반제만 입력 가능합니다.\r\n다음 날짜로 저장하시겠습니까?")) {
		 //      		return false;
		 //      	}
		 //      	//ptcomm.MsgBox("확인", "하루에 한껀의 반제만 입력 가능합니다. 다음 날짜로 저장하시겠습니까?");
		 //      	ld_today = ptcomm.GetNextDate(fdw_1.Evaluate(1,"string(Max(pay_date),'YYYY-MM-DD')"));
		 //      	fdw_1.SetItem(i, "pay_date", ptcomm.Todate(ptcomm.ToString(ld_today,'YYYY-MM-DD')));
		 //      }
   //  		  ls_scrollrow = fdw_list.GetRow();
   // 		  }
			// }

		}
	}

	return true;
}

//저장후 호출
function x_DAO_Saved2(){
  var ls_dept = dg_1.GetItem(dg_1.GetRow(), "DEPT_ID");
  var ls_cust = dg_1.GetItem(dg_1.GetRow(), "CUST_ID");

  _X.EP("gs", "PROC_GS_SERVICE_PAY_RECEIPT", [mytop._UserID, ls_dept, ls_cust]);

	setTimeout("x_DAO_Retrieve()",100);
}

function x_DAO_Insert2(a_dg, row){
	if(dg_1.RowCount() <= 0) return 0;

  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
	if(a_dg.id == 'dg_3') {
		dg_3.SetItem(rowIdx, "DEPT_ID" , S_DEPT_ID.value);
		dg_3.SetItem(rowIdx, "CUST_ID" , dg_1.GetItem(dg_1.GetRow(), "CUST_ID"));
		dg_3.SetItem(rowIdx, "REQ_YYMM", _X.StrPurify(sle_basicmonth.value));

		var ls_date = _X.ToString(_X.GetSysDate(), 'YYYY-MM-DD');
		for(var i=1; i<=dg_3.RowCount(); i++) {
			if(ls_date == _X.ToString(dg_3.GetItem(i, "PAY_DATE"), 'YYYY-MM-DD')) {
				return;
			}
		}
		dg_3.SetItem(rowIdx, "PAY_DATE", ls_date);
	}
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	return 100;
}

function x_DAO_Excel2(a_dg){
	dg_1.ExcelExport(true);

	return 0;
	//return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	if(a_dg.id == 'dg_3') {
		switch(a_col) {
			case	"CARD_AMT":
			case	"BANK_AMT":
			case	"VBANK_AMT":
			case	"JIRO_AMT":
						var ll_amt = _X.ToInt(dg_3.GetItem(a_row, "CARD_AMT")) + _X.ToInt(dg_3.GetItem(a_row, "BANK_AMT")) + _X.ToInt(dg_3.GetItem(a_row, "VBANK_AMT")) + _X.ToInt(dg_3.GetItem(a_row, "JIRO_AMT"));
						dg_3.SetItem(a_row, "PAY_AMT", ll_amt);
						dg_3.SetAllFooterData();
						break;
			case	"REQ_YYMM":
						var ls_yymm = dg_3.GetItem(a_row, "REQ_YYMM");
						var ls_date = _X.ToString(dg_3.GetItem(a_row, "PAY_DATE"),"YYYYMMDD");
						for(var i=1; i<=dg_3.RowCount(); i++) {
							if(i == a_row) continue;
							if(ls_yymm == dg_3.GetItem(i, "REQ_YYMM") && ls_date == _X.ToString(dg_3.GetItem(i, "PAY_DATE"),"YYYYMMDD")) {
								_X.MsgBox("확인", "결재일자를 확인하세요.\n동일한 청구년월에 결재일자를 같은날로 중복해서 등록 할수 없습니다.");
								dg_3.SetItem(a_row, "REQ_YYMM", "");
								return;
							}
						}
						break;
			case	"PAY_DATE":
						var ls_yymm = dg_3.GetItem(a_row, "REQ_YYMM");
						var ls_date = _X.ToString(dg_3.GetItem(a_row, "PAY_DATE"),"YYYYMMDD");
						for(var i=1; i<=dg_3.RowCount(); i++) {
							if(i == a_row) continue;
							if(ls_yymm == dg_3.GetItem(i, "REQ_YYMM") && ls_date == _X.ToString(dg_3.GetItem(i, "PAY_DATE"),"YYYYMMDD")) {
								_X.MsgBox("확인", "결재일자를 확인하세요.\n동일한 청구년월에 결재일자를 같은날로 중복해서 등록 할수 없습니다.");
								dg_3.SetItem(a_row, "PAY_DATE", "");
								return;
							}
						}
						break;
		}
	}
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
	if(a_dg.id == 'dg_1') {
		var ls_deptid = dg_1.GetItem(a_newrow,'DEPT_ID');
		var ls_ym     = _X.StrPurify(sle_basicmonth.value);
		var li_custid = dg_1.GetItem(a_newrow,'CUST_ID');

	  dg_2.Retrieve([ls_deptid, ls_ym, li_custid]);
	  dg_3.Retrieve([ls_deptid, ls_ym, li_custid]);

		var la_DEPOSIT = _X.XmlSelect("com", "COMMON", "DDLB_CUST_DEPOSITNO" , [li_custid], 'json2');
		dg_3.SetCombo("VBANK_DEPOSIT", la_DEPOSIT);
	}
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
	if(_FindCode.returnValue == null) return;

	switch(_FindCode.finddiv) {
		case	"S_DEPT_ID" :
					S_DEPT_ID.value = _FindCode.returnValue.DEPT_ID;
					x_DAO_Retrieve();
					break;
	}
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
	if(a_dg.id == 'dg_1' && a_row > 0) {
    var li_misu = dg_1.GetItem(a_row,"MISU_AMT");
    if(li_misu > 0){
      var li_nRow = dg_3.InsertRow(0);
      dg_3.SetItem(li_nRow, "BANK_AMT", li_misu);
    }
	}
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

// 지사찾기
function uf_findDept() {
	_X.FindDeptCode2('S_DEPT_ID','');
}

function uf_Service_Account_Create() {
  var ls_dept = S_DEPT_ID.value;   // 지사구분
  var ls_date = _X.StrPurify(sle_basicmonth.value);

  _X.EP("gs", "PR_SERVICE_CALCULATOR", [ls_dept, ls_date]);
}

// 본인부담금수납대장
function uf_boninsunab() { 
	_X.SetiReport("gs", "GS01080R1",	"AS_DEPT="		+ S_DEPT_ID.value
																	+ "&AS_YYMM="	+ _X.StrPurify(sle_basicmonth.value)
																	+ "&AS_CUST="	+ "%"+S_CUST_NAME.value+ "%" );
}

// 급여비용명세서출력
function uf_gubyebiyong() {
	if(dg_1.RowCount()<=0) {
		return 0;
	}

	_X.SetiReport("gs", "GS01080R2",	"AS_CUST="		+ dg_1.GetItem(dg_1.GetRow(), "CUST_ID")
																	+ "&AS_YYMM="	+ _X.StrPurify(sle_basicmonth.value)
																	+ "&AS_DATE="	+ _X.StrPurify(sle_basicdate.value) );
}

// 급여비용명세서엑셀
function uf_gubyebiyongxls() {
	var ls_dept = S_DEPT_ID.value;
	var ls_yymm = _X.StrPurify(sle_basicmonth.value);
  var ls_cust = dg_1.GetItem(dg_1.GetRow(), "CUST_ID");

	location.href = "/Mighty/jsp/down_report3.jsp?rep=rpt_00030.xml&fname=급여비용명세서&cust="+ls_cust+"&dept="+ls_dept+"&ym="+ls_yymm;

}

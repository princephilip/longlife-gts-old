//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : 대상자 주별별일정/변경
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
var is_client_div = 'O';	//I:매입, O:매출, J:지사, E:기타
var is_date = "";
is_psep_div = "S"
var is_deptcust = 'Y';    //Y:지사별고객찾기  , N:전체고객찾기
var is_index = "";
var ia_changed_rows = new Array();

function x_InitForm2(){
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "gs", "GS01090", "GS01090|GS01090_C01", false,  true);

	//지사명
	var la_DEPT = _X.XmlSelect("com", "COMMON", "CODE_DEPT_AUTHORITY_R01" , new Array(mytop._UserID), 'json2');
	_X.DDLB_SetData(S_DEPT_ID, la_DEPT, null, true, false);
	S_DEPT_ID.value = mytop._DeptCode;

	is_date = _X.ToString(_X.GetSysDate(),'yyyy-mm-dd');
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		var ls_service_yn = [{"code":"1", "label":"제공"}, {"code":"0", "label":"미제공"}];
		dg_1.SetCombo("SERVICE_YN", ls_service_yn);

		var ls_product_div = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0128", 'Y'), 'json2');
		dg_1.SetCombo("PRODUCT_DIV", ls_product_div);

		//var ls_product_code = _X.XmlSelect("com", "COMMON", "DDLB_CODE_PRODUCT" , new Array('%', '%'), 'json2');
		//dg_1.SetCombo("PRODUCT_CODE", ls_product_code);

		var ls_time_div = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0123", 'Y'), 'json2');
		var ls_min_div  = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0124", 'Y'), 'json2');
		dg_1.SetCombo("START_TIME_DIV", ls_time_div);
		dg_1.SetCombo("START_MIN_DIV" , ls_min_div);
		dg_1.SetCombo("END_TIME_DIV"  , ls_time_div);
		dg_1.SetCombo("END_MIN_DIV"   , ls_min_div);

		xe_EditChanged(sle_basicdate, sle_basicdate.value);
	}
}

function x_DAO_Retrieve2(a_dg){
	//uf_dg1_Retrieve();
	// 일정표 그리기
	setTimeout("jf_CalendarLoaded()", 200);
}

function uf_dg1_Retrieve(){
	dg_1.Retrieve([S_DEPT_ID.value, is_date]);
	if(dg_1.RowCount() > 0) {
		var lb_close = uf_CloseCheck(S_DEPT_ID.value, _X.ToString(is_date,'YYYYMM'));
		dg_1.GridReadOnly(lb_close);
	}
}


function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id) {
		case	"S_DEPT_ID":
					x_DAO_Retrieve();
					break;
		case	"sle_basicdate":
					is_date = sle_basicdate.value;
					x_DAO_Retrieve();
					break;
	}
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	var cData 	= dg_1.GetChangedData();

	var j = 0;
	if(cData.length > 0){
		for (i=0 ; i < cData.length; i++){
			if(cData[i].job == 'U' && dg_1.GetItem(cData[i].idx, "PRODUCT_CODE") != "Z9999990") {
				ia_changed_rows[j] = cData[i].idx;
				j++;
			}
		}
	}

	return true;
}

//저장후 호출
function x_DAO_Saved2(){
	var ls_frdt = _X.ToString(is_date,'YYYYMM')+'01';
	var ls_todt = _X.ToString(_X.LastDate(ls_frdt),'YYYYMMDD');

	for(var i=0; i<ia_changed_rows.length; i++) {
		var ls_cust = dg_1.GetItem(ia_changed_rows[i], "CUST_ID");

		// 아래로변경
		// var ls_result = _X.EP('gs','PROC_GS_CALCAMT_PERIOD', [ls_cust, ls_frdt, ls_todt]);
		// if(ls_result == -1) {
		// 	_X.MsgBox("본인부담금 계산 중, 오류가 발생했습니다.");
		// }

		//alert([ls_cust, S_DEPT_ID.value, ls_frdt, ls_todt]);
		uf_recalc(ls_cust, S_DEPT_ID.value, ls_frdt, ls_todt);
	}
	
	ia_changed_rows = new Array();

  // 달력콘트롤에 일 자료를 세팅한다.
  jf_CalendarSetDataDay(); 
  uf_dg1_Retrieve();
}

// 수가재계산
function uf_recalc(a_cust, a_dept, a_frdt, a_todt) {
	// 서비스 수가 재계산
	var li_rtn = _X.EP('gs','PR_GS_CALC_SERVICEAMT', [a_cust, a_dept, a_frdt, a_todt]);
	if(li_rtn<0){
		_X.MsgBox("서비스 수가 계산 중, 오류가 발생했습니다.");
		return;
	}

	// 서비스 Summary 집계
	var ls_result = _X.EP('gs','PR_GS_SERVICE_SUMMARY', [a_cust, a_dept, a_todt.substr(0,6)]);
	if(ls_result==-1) {
		_X.MsgBox("서비스 집계 중, 오류가 발생했습니다.");
		return;
	}

	var ls_result = _X.EP('gs','PR_GS_CALCAMT_PERIOD', [a_cust, a_frdt, a_todt]);
	if(ls_result==-1) {
		_X.MsgBox("본인부담금 계산 중, 오류가 발생했습니다.");
	}
}



function x_DAO_Insert2(a_dg, row){
  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
	// var ls_month = _X.ToString(is_date,'YYYYMM')

	// if( rb_print_0.checked ){
	// 	_X.SetiReport("gs", "GS01040R_A",	"as_dept="   		+ S_DEPT_ID.value
	// 																	+ "&as_yyyymm="  	+ ls_month
	// 																	+ "&as_cust="   	+ is_cust_id
	// 																	+ "&as_printtag="	+ 'CUSTPRINT' );
	// }else{
	// 	var ls_memberid = "";
	// 	var as_maxseq = _X.XS("gs", "GS01090", "MAX_SEQ" , [S_DEPT_ID.value, is_cust_id, ls_month], 'Array')[0][0];
	// 	var ls_print  = "CUSTPRINT";
	//   var ls_depositediv = "0200";
	//   var ls_print_div = "A";			//A:전체일정, 1:청구일정, 2:기타(급여)

	//   _X.SetiReport("gs", "GS01040R_SB0",	"as_dept="   		 + S_DEPT_ID.value
	// 																		+ "&as_month="  	 + ls_month
	// 																		+ "&as_cust="   	 + is_cust_id
	// 																		+ "&as_seq="   		 + as_maxseq
	// 																		+ "&as_memberid="  + is_cust_id //dg_1.GetItem(dg_1.GetRow(),'MEMBER_ID')
	// 																		+ "&as_printtag="  + ls_print
	// 																		+ "&as_print_div=" + ls_print_div
	// 																		+ "&as_deposite="	 + ls_depositediv );
	// }

}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	if(a_dg.id == 'dg_1') {
		switch(a_col) {
  		case "START_TIME_DIV":
  		case "START_MIN_DIV":
  		case "END_TIME_DIV":
  		case "END_MIN_DIV":
  		case "PRODUCT_DIV":
  		case "MEMBER_ID":
  		case "PRODUCT_CODE":
					var ls_div   = dg_1.GetItem(a_row, "PRODUCT_DIV");
					var ls_stime = dg_1.GetItem(a_row, "START_TIME_DIV");
					var ls_smin  = dg_1.GetItem(a_row, "START_MIN_DIV");
					var ls_etime = dg_1.GetItem(a_row, "END_TIME_DIV");
					var ls_emin  = dg_1.GetItem(a_row, "END_MIN_DIV");

					//서비스분류가 바꼈을때 기본으로 세팅	화면에서 수정못함(막아놈) YSKIM
					// if(a_col =="PRODUCT_DIV") {		
					// 	//uf_chg_product_div();
					// 	switch(Data) {
					// 		case '1':			//방문요양
					// 				var ls_rtn = ptdwo.SqlSelect("gs", " SELECT PRODUCT_ID, PRODUCT_CODE, PRODUCT_NAME, SALE_AMT, SERVICE_MIN FROM CODE_PRODUCT WHERE PSEP_DIV = 'S' AND USE_YN = 'Y' "
     //                     + " AND PRODUCT_DIV = '" + ls_div + "' AND ROWNUM = 1" ).split('@');
                  
	    //             dg_1.SetItem(Row, "amt_1", Number(ls_rtn[3]));
		   // 						dg_1.SetItem(Row, "amt_2", 0);
		   // 						dg_1.SetItem(Row, "amt_3", 0);
		   // 						dg_1.SetItem(Row, "min_1", Number(ls_rtn[4]));
		   // 						dg_1.SetItem(Row, "min_2", 0);
		   // 						dg_1.SetItem(Row, "min_3", 0);

					// 		    dg_1.SetItem(Row, "product_id", Number(ls_rtn[0]));
					// 		    dg_1.SetItem(Row, "product_code", ls_rtn[1]);
					// 		    dg_1.SetItem(Row, "product_name", ls_rtn[2]);
					// 		    dg_1.SetItem(Row, "service_amt", Number(ls_rtn[3]));
						    
					// 	    	uf_set_viewcost();
                  
					// 				break;
					// 		case '2':			//방문목욕
					// 				var ls_rtn = ptdwo.SqlSelect("gs", " SELECT PRODUCT_ID, PRODUCT_CODE, PRODUCT_NAME, SALE_AMT, SERVICE_MIN FROM CODE_PRODUCT WHERE PSEP_DIV = 'S' AND USE_YN = 'Y' "
     //                     + " AND PRODUCT_DIV = '" + ls_div + "' AND ROWNUM = 1" ).split('@');
                  
	    //             dg_1.SetItem(Row, "amt_1", Number(ls_rtn[3]));
		   // 						dg_1.SetItem(Row, "amt_2", 0);
		   // 						dg_1.SetItem(Row, "amt_3", 0);
		   // 						dg_1.SetItem(Row, "min_1", Number(ls_rtn[4]));
		   // 						dg_1.SetItem(Row, "min_2", 0);
		   // 						dg_1.SetItem(Row, "min_3", 0);

					// 		    dg_1.SetItem(Row, "product_id", Number(ls_rtn[0]));
					// 		    dg_1.SetItem(Row, "product_code", ls_rtn[1]);
					// 		    dg_1.SetItem(Row, "product_name", ls_rtn[2]);
					// 		    dg_1.SetItem(Row, "service_amt", Number(ls_rtn[3]));
					// 		    dg_1.SetItem(Row, "service_min", ls_rtn[4]);
						    
					// 	    	uf_set_viewcost();
                  
					// 				break;
					// 		case '3':			//방문간호
					// 				var ls_rtn = ptdwo.SqlSelect("gs", " SELECT PRODUCT_ID, PRODUCT_CODE, PRODUCT_NAME, SALE_AMT, SERVICE_MIN FROM CODE_PRODUCT WHERE PSEP_DIV = 'S' AND USE_YN = 'Y' "
     //                     + " AND PRODUCT_DIV = '" + ls_div + "' AND ROWNUM = 1" ).split('@');
                  
	    //             dg_1.SetItem(Row, "amt_1", Number(ls_rtn[3]));
		   // 						dg_1.SetItem(Row, "amt_2", 0);
		   // 						dg_1.SetItem(Row, "amt_3", 0);
		   // 						dg_1.SetItem(Row, "min_1", Number(ls_rtn[4]));
		   // 						dg_1.SetItem(Row, "min_2", 0);
		   // 						dg_1.SetItem(Row, "min_3", 0);

					// 		    dg_1.SetItem(Row, "product_id", Number(ls_rtn[0]));
					// 		    dg_1.SetItem(Row, "product_code", ls_rtn[1]);
					// 		    dg_1.SetItem(Row, "product_name", ls_rtn[2]);
					// 		    dg_1.SetItem(Row, "service_amt", Number(ls_rtn[3]));
						    
					// 	    	uf_set_viewcost();
                  
					// 				break;
					// 		case '4':			//야간보호
					// 				var ls_cust_id = dg_1.GetItem(Row, "cust_id");
					// 				var ls_cont_seq = dg_1.GetItem(Row, "cont_seq");
					// 				var ls_level_div = ptdwo.SqlSelect("gs", "select cust_level_div from service_contract where cust_id = " + ls_cust_id + " and cont_seq = " + ls_cont_seq );	//장기요양등급
					//     		if(ls_level_div != '1' && ls_level_div != '2') ls_level_div = '3';	//1,2 등급을 제외한 나머지는 3등급 처리
					    		
					//     		var ls_rtn = ptdwo.SqlSelect("gs", " SELECT PRODUCT_ID, PRODUCT_CODE, PRODUCT_NAME, SALE_AMT, SERVICE_MIN FROM CODE_PRODUCT WHERE PSEP_DIV = 'S' AND USE_YN = 'Y' "
     //                     + " AND PRODUCT_DIV = '" + ls_div + "' AND CUST_LEVEL_DIV = '" + ls_level_div + "' AND ROWNUM = 1" ).split('@');
                  
     //              dg_1.SetItem(Row, "amt_1", 0);
		   // 						dg_1.SetItem(Row, "amt_2", 0);
		   // 						dg_1.SetItem(Row, "amt_3", 0);
		   // 						dg_1.SetItem(Row, "min_1", 0);
		   // 						dg_1.SetItem(Row, "min_2", 0);
		   // 						dg_1.SetItem(Row, "min_3", 0);

					// 		    dg_1.SetItem(Row, "product_id", Number(ls_rtn[0]));
					// 		    dg_1.SetItem(Row, "product_code", ls_rtn[1]);
					// 		    dg_1.SetItem(Row, "product_name", ls_rtn[2]);
					// 		    dg_1.SetItem(Row, "service_amt", Number(ls_rtn[3]));
							    
					// 	    	uf_set_viewcost();
									
					// 				break;
					// 		case '5':			//단기보호
					// 				var ls_cust_id = dg_1.GetItem(Row, "cust_id");
					// 				var ls_cont_seq = dg_1.GetItem(Row, "cont_seq");
					// 				var ls_level_div = ptdwo.SqlSelect("gs", "select cust_level_div from service_contract where cust_id = " + ls_cust_id + " and cont_seq = " + ls_cont_seq );	//장기요양등급
					//     		if(ls_level_div != '1' && ls_level_div != '2') ls_level_div = '3';	//1,2 등급을 제외한 나머지는 3등급 처리

					//     		var ls_rtn = ptdwo.SqlSelect("gs", " SELECT PRODUCT_ID, PRODUCT_CODE, PRODUCT_NAME, SALE_AMT, SERVICE_MIN FROM CODE_PRODUCT WHERE PSEP_DIV = 'S' AND USE_YN = 'Y' "
     //                     + " AND PRODUCT_DIV = '" + ls_div + "' AND CUST_LEVEL_DIV = '" + ls_level_div + "' " ).split('@');
                  
		   // 						dg_1.SetItem(Row, "service_min_1", 0);
		   // 						dg_1.SetItem(Row, "service_min_2", 0);
		   // 						dg_1.SetItem(Row, "service_min_3", 0);

					// 		    dg_1.SetItem(Row, "product_id", Number(ls_rtn[0]));
					// 		    dg_1.SetItem(Row, "product_code", ls_rtn[1]);
					// 		    dg_1.SetItem(Row, "product_name", ls_rtn[2]);
					// 		    dg_1.SetItem(Row, "service_amt", Number(ls_rtn[3]));
							    
					// 				dg_1.SetItem(Row, 'start_time_div', '00');   
					//         dg_1.SetItem(Row, 'start_min_div',  '00');   
					//         dg_1.SetItem(Row, 'end_time_div',   '00');   
					//         dg_1.SetItem(Row, 'end_min_div',    '00');  
					        
					//         dg_1.SetItem(Row, "service_min", '0');

					// 	    	uf_set_viewcost();
						    	
					// 	    	break;
					// 		case '6':			//시설급여
					// 				var ls_cust_id = dg_1.GetItem(Row, "cust_id");
					// 				var ls_cont_seq = dg_1.GetItem(Row, "cont_seq");
					// 				var ls_level_div = ptdwo.SqlSelect("gs", "select cust_level_div from service_contract where cust_id = " + ls_cust_id + " and cont_seq = " + ls_cont_seq );	//장기요양등급
					//     		if(ls_level_div != '1' && ls_level_div != '2') ls_level_div = '3';	//1,2 등급을 제외한 나머지는 3등급 처리

					//     		var ls_rtn = ptdwo.SqlSelect("gs", " SELECT PRODUCT_ID, PRODUCT_CODE, PRODUCT_NAME, SALE_AMT, SERVICE_MIN FROM CODE_PRODUCT WHERE PSEP_DIV = 'S' AND USE_YN = 'Y' "
     //                     + " AND PRODUCT_DIV = '" + ls_div + "' AND CUST_LEVEL_DIV = '" + ls_level_div + "' AND ROWNUM = 1 " ).split('@');

		   // 						dg_1.SetItem(Row, "service_min_1", 0);
		   // 						dg_1.SetItem(Row, "service_min_2", 0);
		   // 						dg_1.SetItem(Row, "service_min_3", 0);

					// 		    dg_1.SetItem(Row, "product_id", Number(ls_rtn[0]));
					// 		    dg_1.SetItem(Row, "product_code", ls_rtn[1]);
					// 		    dg_1.SetItem(Row, "product_name", ls_rtn[2]);
					// 		    dg_1.SetItem(Row, "service_amt", Number(ls_rtn[3]));
							    
					// 				dg_1.SetItem(Row, 'start_time_div', '00');   
					//         dg_1.SetItem(Row, 'start_min_div',  '00');   
					//         dg_1.SetItem(Row, 'end_time_div',   '00');   
					//         dg_1.SetItem(Row, 'end_min_div',    '00');  
					        
					//         dg_1.SetItem(Row, "service_min", '0');

					// 	    	uf_set_viewcost();
						    	
					// 	    	break;
					// 		case '7':			//기타급여
					//     		var ls_rtn = ptdwo.SqlSelect("gs", " SELECT PRODUCT_ID, PRODUCT_CODE, PRODUCT_NAME, SALE_AMT, SERVICE_MIN FROM CODE_PRODUCT WHERE PSEP_DIV = 'S' AND USE_YN = 'Y' "
     //                     + " AND PRODUCT_DIV = '" + ls_div + "' AND PRODUCT_CODE = 'Z9999990' " ).split('@');
                  
     //              dg_1.SetItem(Row, "amt_1", 0);
		   // 						dg_1.SetItem(Row, "amt_2", 0);
		   // 						dg_1.SetItem(Row, "amt_3", 0);
		   // 						dg_1.SetItem(Row, "min_1", 0);
		   // 						dg_1.SetItem(Row, "min_2", 0);
		   // 						dg_1.SetItem(Row, "min_3", 0);

					// 		    dg_1.SetItem(Row, "product_id", Number(ls_rtn[0]));
					// 		    dg_1.SetItem(Row, "product_code", ls_rtn[1]);
					// 		    dg_1.SetItem(Row, "product_name", ls_rtn[2]);
					// 		    dg_1.SetItem(Row, "service_amt", 0);
					// 		    dg_1.SetItem(Row, "pay_amt", 0);
					// 		    dg_1.SetItem(Row, "req_amt", 0);
							    
					// 	    	uf_set_viewcost();
						    	
					// 	    	break;
					// 	}
					// }
					
  				switch(ls_div) {
  					case '1' :		//방문요양일경우
		  					if(a_col == "PRODUCT_CODE") return;	//서비스코드 바뀔시 아무것도 하지 않음
								if(ls_stime && ls_smin && ls_etime && ls_emin) {
									var ls_member_id = dg_1.GetItem(a_row, "MEMBER_ID");
									var ls_service_yn = dg_1.GetItem(a_row, "SERVICE_YN");
									//가족케어여부
									var ls_family_yn = _X.XmlSelect("gs", "GS01090", "FAMILY_YN", [ls_member_id], "array")[0][0];
									var ll_service_cost = 0;
									var ls_date = _X.ToString(_X.ToDate(dg_1.GetItem(a_row, "PLAN_DATE")), 'yyyymmdd');	
									var ls_product_id = 0;
					    		var ls_product_div = ls_div;
					    		var ls_level_div = '1';	//장기요양등급

								  var ls_result = _X.XmlSelect("gs", "GS01040", "GET_SERVICEAMT", [ls_date, ls_stime, ls_smin, ls_etime, ls_emin, ll_service_cost, ls_family_yn, ls_product_id, ls_product_div, ls_level_div, ls_service_yn], "array");
								  if (ls_result.length > 0) {
								  	var la_rtn = ls_result[0][0].split('|');
								    var ll_service_amt   = _X.ToInt(la_rtn[0]);
								    var ll_service_min_1 = _X.ToInt(la_rtn[1]);
								    var ll_service_min_2 = _X.ToInt(la_rtn[2]);
								    var ll_service_min_3 = _X.ToInt(la_rtn[3]);
								    var ll_service_amt_1 = _X.ToInt(la_rtn[4]);
								    var ll_service_amt_2 = _X.ToInt(la_rtn[5]);
								    var ll_service_amt_3 = _X.ToInt(la_rtn[6]);
								  // var ls_result = _X.XmlSelect("gs", "GS01090", "GET_SERVICEAMT", [ls_date, ls_stime, ls_smin, ls_etime, ls_emin, ll_service_cost, ls_family_yn, ls_product_id, ls_product_div, ls_level_div], "array")[0][0];
								  // if (!(ls_result == null || ls_result == "" || typeof(ls_result) == "undefined" )) {
								  //   var la_rtn = ls_result.split('|');
								  //   var ll_service_amt = Number(la_rtn[0]);
								  //   var ll_service_min_1 = Number(la_rtn[1]);
								  //   var ll_service_min_2 = Number(la_rtn[2]);
								  //   var ll_service_min_3 = Number(la_rtn[3]);
								  //   var ll_service_amt_1 = Number(la_rtn[4]);
								  //   var ll_service_amt_2 = Number(la_rtn[5]);
								  //   var ll_service_amt_3 = Number(la_rtn[6]);
								    
								    dg_1.SetItem(a_row, "SERVICE_MIN_1", ll_service_min_1);
			   						dg_1.SetItem(a_row, "SERVICE_MIN_2", ll_service_min_2);
			   						dg_1.SetItem(a_row, "SERVICE_MIN_3", ll_service_min_3);
			
								    dg_1.SetItem(a_row, "PRODUCT_ID"   , Number(la_rtn[7]));
								    dg_1.SetItem(a_row, "PRODUCT_CODE" , la_rtn[8]);
								    dg_1.SetItem(a_row, "PRODUCT_NAME" , la_rtn[9]);
								    dg_1.SetItem(a_row, "SERVICE_AMT"  , ll_service_amt);
								    
								    dg_1.SetItem(a_row, "SERVICE_MIN"  , (ll_service_min_1 + ll_service_min_2 + ll_service_min_3).toString());
								    
								    uf_set_viewcost();
			  					}
								}
								break;
						case '2':		//방문목욕일 경우
								//서비스 코드가 바뀔대 수가 변경
								if(a_col == "PRODUCT_CODE") {
									// var ls_rtn = ptdwo.SqlSelect("gs", " SELECT PRODUCT_ID, PRODUCT_CODE, PRODUCT_NAME, SALE_AMT, SERVICE_MIN FROM CODE_PRODUCT WHERE PSEP_DIV = 'S' AND USE_YN = 'Y' "
		       //               + " AND PRODUCT_DIV = '" + ls_div + "' AND PRODUCT_CODE = '" + a_newvalue + "' " ).split('@');
		              
		   				// 		dg_1.SetItem(a_row, "SERVICE_MIN_1", Number(ls_rtn[4]));
		   				// 		dg_1.SetItem(a_row, "SERVICE_MIN_2", 0);
		   				// 		dg_1.SetItem(a_row, "SERVICE_MIN_3", 0);
		
							  //   dg_1.SetItem(a_row, "PRODUCT_ID"   , Number(ls_rtn[0]));
							  //   dg_1.SetItem(a_row, "PRODUCT_CODE" , ls_rtn[1]);
							  //   dg_1.SetItem(a_row, "PRODUCT_NAME" , ls_rtn[2]);
							  //   dg_1.SetItem(a_row, "SERVICE_AMT"  , Number(ls_rtn[3]));
							    
							  //   dg_1.SetItem(a_row, "SERVICE_MIN"  , ls_rtn[4]);
						    
						   //  	uf_set_viewcost();
								}
								break;
						case '3':		//방문간호일 경우
		  					if(a_col == "PRODUCT_CODE") return;	//서비스코드 바뀔시 아무것도 하지 않음
								if(ls_stime && ls_smin && ls_etime && ls_emin) {
									
									var ls_service_yn = dg_1.GetItem(a_row, "SERVICE_YN");
									var ls_family_yn = 'N';	//가족케어여부
									var ll_service_cost = 0;
									var ls_date = _X.ToString(_X.ToDate(dg_1.GetItem(a_row, "PLAN_DATE")), 'yyyymmdd');	
									var ls_product_id = 0;
					    		var ls_product_div = ls_div;
					    		var ls_level_div = '1';	//장기요양등급

								  var ls_result = _X.XmlSelect("gs", "GS01040", "GET_SERVICEAMT", [ls_date, ls_stime, ls_smin, ls_etime, ls_emin, ll_service_cost, ls_family_yn, ls_product_id, ls_product_div, ls_level_div, ls_service_yn], "array");
								  if (ls_result.length > 0) {
								  	var la_rtn = ls_result[0][0].split('|');
								    var ll_service_amt   = _X.ToInt(la_rtn[0]);
								    var ll_service_min_1 = _X.ToInt(la_rtn[1]);
								    var ll_service_min_2 = _X.ToInt(la_rtn[2]);
								    var ll_service_min_3 = _X.ToInt(la_rtn[3]);
								    var ll_service_amt_1 = _X.ToInt(la_rtn[4]);
								    var ll_service_amt_2 = _X.ToInt(la_rtn[5]);
								    var ll_service_amt_3 = _X.ToInt(la_rtn[6]);
								  // var ls_result = _X.XmlSelect("gs", "GS01090", "GET_SERVICEAMT", [ls_date, ls_stime, ls_smin, ls_etime, ls_emin, ll_service_cost, ls_family_yn, ls_product_id, ls_product_div, ls_level_div], "array")[0][0];
								  // if (!(ls_result == null || ls_result == "" || typeof(ls_result) == "undefined" )) {
								  //   var la_rtn = ls_result.split('|');
								  //   var ll_service_amt = Number(la_rtn[0]);
								  //   var ll_service_min_1 = Number(la_rtn[1]);
								  //   var ll_service_min_2 = Number(la_rtn[2]);
								  //   var ll_service_min_3 = Number(la_rtn[3]);
								  //   var ll_service_amt_1 = Number(la_rtn[4]);
								  //   var ll_service_amt_2 = Number(la_rtn[5]);
								  //   var ll_service_amt_3 = Number(la_rtn[6]);
								    
								    dg_1.SetItem(a_row, "SERVICE_MIN_1", ll_service_min_1);
			   						dg_1.SetItem(a_row, "SERVICE_MIN_2", 0);
			   						dg_1.SetItem(a_row, "SERVICE_MIN_3", 0);
			
								    dg_1.SetItem(a_row, "PRODUCT_ID"   , Number(la_rtn[7]));
								    dg_1.SetItem(a_row, "PRODUCT_CODE" , la_rtn[8]);
								    dg_1.SetItem(a_row, "PRODUCT_NAME" , la_rtn[9]);
								    dg_1.SetItem(a_row, "SERVICE_AMT"  , ll_service_amt);
								    
								    dg_1.SetItem(a_row, "SERVICE_MIN"  , la_rtn[1]);
								    
								    uf_set_viewcost();
			  					}
								}								
								break;
						case '4':		//야간보호일 경우
		  					if(a_col == "PRODUCT_CODE") return;	//서비스코드 바뀔시 아무것도 하지 않음
								if(ls_stime && ls_smin && ls_etime && ls_emin) {
									
									var ls_service_yn = dg_1.GetItem(a_row, "SERVICE_YN");
									var ls_family_yn = 'N';	//가족케어여부
									var ll_service_cost = 0;
									var ls_date = _X.ToString(_X.ToDate(dg_1.GetItem(a_row, "PLAN_DATE")), 'yyyymmdd');	
									var ls_product_id = 0;
					    		var ls_product_div = ls_div;			
									var ls_cust_id = dg_1.GetItem(a_row, "CUST_ID");
									var ls_cont_seq = dg_1.GetItem(a_row, "CONT_SEQ");
									var ls_level_div = _X.XmlSelect("gs", "GS01090", "CUST_LEVEL_DIV", [ls_cust_id, ls_cont_seq], "array")[0][0];	//장기요양등급
					    		if(ls_level_div != '1' && ls_level_div != '2') ls_level_div = '3';	//1,2 등급을 제외한 나머지는 3등급 처리

								  var ls_result = _X.XmlSelect("gs", "GS01040", "GET_SERVICEAMT", [ls_date, ls_stime, ls_smin, ls_etime, ls_emin, ll_service_cost, ls_family_yn, ls_product_id, ls_product_div, ls_level_div, ls_service_yn], "array");
								  if (ls_result.length > 0) {
								  	var la_rtn = ls_result[0][0].split('|');
								    var ll_service_amt   = _X.ToInt(la_rtn[0]);
								    var ll_service_min_1 = _X.ToInt(la_rtn[1]);
								    var ll_service_min_2 = _X.ToInt(la_rtn[2]);
								    var ll_service_min_3 = _X.ToInt(la_rtn[3]);
								    var ll_service_amt_1 = _X.ToInt(la_rtn[4]);
								    var ll_service_amt_2 = _X.ToInt(la_rtn[5]);
								    var ll_service_amt_3 = _X.ToInt(la_rtn[6]);
								  // var ls_result = _X.XmlSelect("gs", "GS01090", "GET_SERVICEAMT", [ls_date, ls_stime, ls_smin, ls_etime, ls_emin, ll_service_cost, ls_family_yn, ls_product_id, ls_product_div, ls_level_div], "array")[0][0];
								  // if (!(ls_result == null || ls_result == "" || typeof(ls_result) == "undefined" )) {
								  //   var la_rtn = ls_result.split('|');
								  //   var ll_service_amt = Number(la_rtn[0]);
								  //   var ll_service_min_1 = Number(la_rtn[1]);
								  //   var ll_service_min_2 = Number(la_rtn[2]);
								  //   var ll_service_min_3 = Number(la_rtn[3]);
								  //   var ll_service_amt_1 = Number(la_rtn[4]);
								  //   var ll_service_amt_2 = Number(la_rtn[5]);
								  //   var ll_service_amt_3 = Number(la_rtn[6]);
								    
								    dg_1.SetItem(a_row, "SERVICE_MIN_1", ll_service_min_1);
			   						dg_1.SetItem(a_row, "SERVICE_MIN_2", 0);
			   						dg_1.SetItem(a_row, "SERVICE_MIN_3", 0);
			
										dg_1.SetItem(a_row, "PRODUCT_ID"   , Number(la_rtn[7]));
								    dg_1.SetItem(a_row, "PRODUCT_CODE" , la_rtn[8]);
								    dg_1.SetItem(a_row, "PRODUCT_NAME" , la_rtn[9]);
								    if(typeof(la_rtn[8]) == "undefined") {
											dg_1.SetItem(a_row, "PRODUCT_ID"  , null);
									    dg_1.SetItem(a_row, "PRODUCT_CODE", null);
									    dg_1.SetItem(a_row, "PRODUCT_NAME", null);
								    }
								    dg_1.SetItem(a_row, "SERVICE_AMT"  , ll_service_amt);
								    
 								    dg_1.SetItem(a_row, "SERVICE_MIN"  , la_rtn[1]);
								    
								    uf_set_viewcost();
			  					}
								}								
								break;
						case '5':		//방문목욕일 경우
						case '6':		//시설급여
								//서비스 코드가 바뀔대 수가 변경
								if(a_col == "PRODUCT_CODE") {
									// var ls_rtn = ptdwo.SqlSelect("gs", " SELECT PRODUCT_ID, PRODUCT_CODE, PRODUCT_NAME, SALE_AMT, SERVICE_MIN FROM CODE_PRODUCT WHERE PSEP_DIV = 'S' AND USE_YN = 'Y' "
		       //               + " AND PRODUCT_DIV = '" + ls_div + "' AND PRODUCT_CODE = '" + a_newvalue + "' " ).split('@');
		              
		   				// 		dg_1.SetItem(a_row, "MIN_1", 0);
		   				// 		dg_1.SetItem(a_row, "MIN_2", 0);
		   				// 		dg_1.SetItem(a_row, "MIN_3", 0);
		
							  //   dg_1.SetItem(a_row, "PRODUCT_ID"  , Number(ls_rtn[0]));
							  //   dg_1.SetItem(a_row, "PRODUCT_CODE", ls_rtn[1]);
							  //   dg_1.SetItem(a_row, "PRODUCT_NAME", ls_rtn[2]);
							  //   dg_1.SetItem(a_row, "SERVICE_AMT" , Number(ls_rtn[3]));
						    	
						   //  	uf_set_viewcost();
								}
								break;
						case '7':		//기타급여일 경우
		  					if(a_col == "PRODUCT_CODE") return;	//서비스코드 바뀔시 아무것도 하지 않음
								if(ls_stime && ls_smin && ls_etime && ls_emin) {
									var li_tot_min = 0;
									if((Number(ls_etime) * 60 + Number(ls_emin)) - (Number(ls_stime) * 60 + Number(ls_smin)) >= 0) {
										li_tot_min = (Number(ls_etime) * 60 + Number(ls_emin)) - (Number(ls_stime) * 60 + Number(ls_smin));
									} else {
										li_tot_min = ((Number(ls_etime) + 24) * 60 + Number(ls_emin)) - (Number(ls_stime) * 60 + Number(ls_smin));
									}
		   						dg_1.SetItem(a_row, "SERVICE_MIN_1", li_tot_min);
		   						dg_1.SetItem(a_row, "SERVICE_MIN_2", 0);
		   						dg_1.SetItem(a_row, "SERVICE_MIN_3", 0);
		   						
							    dg_1.SetItem(a_row, "AMT_1"  , Number(dg_1.GetItem(a_row, "SERVICE_AMT")));
							    dg_1.SetItem(a_row, "PAY_AMT", Number(dg_1.GetItem(a_row, "SERVICE_AMT")));
							    dg_1.SetItem(a_row, "REQ_AMT", 0);
							    
 						    	dg_1.SetItem(a_row, "SERVICE_MIN", li_tot_min.toString());
							    
							    uf_set_viewcost();
		  					}
								break;
					
					}
					break;
			case "SERVICE_AMT":
					var ls_div = dg_1.GetItem(a_row, "PRODUCT_DIV");
					if(ls_div == '7') {			//기타급여일 경우
				    dg_1.SetItem(a_row, "AMT_1"  , Number(a_newvalue));
				    dg_1.SetItem(a_row, "PAY_AMT", Number(a_newvalue));
				    dg_1.SetItem(a_row, "REQ_AMT", 0);
				    uf_set_viewcost();
					}
					break;
		}
	}
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
  // if (a_dg.id == 'dg_1') {
		// uf_SetColVisible(dg_1.GetItem(a_newrow, "DEPT_DIV"));

		// ii_form_seq = dg_1.GetItem(a_newrow, "FORM_SEQ");
		// dg_2.Retrieve([ii_form_seq]);
  // }
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
	if (a_dg.id == 'dg_1') {
		// var li_rtn = _X.EP('gs','PR_GS_SERVICE_SUMMARY', [is_cust_id, S_DEPT_ID.value, _X.ToString(is_date,'YYYYMM')]);
		// if(li_rtn<0){
		// 	_X.MsgBox("서비스 집계 중, 오류가 발생했습니다.");
		// 	return;
		// }

	}
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
	// if(_FindCode.returnValue == null) return;

	// switch(_FindCode.finddiv) {
	// 	case	"S_DEPT_ID" :
	// 				// var ls_yy = $("#form_calendar").contents().find("#selYear").val();
	// 				// var ls_mm = $("#form_calendar").contents().find("#selMonth").val();
	// 				// $('#form_calendar')[0].contentWindow.FrmReload(ls_yy, ls_mm);

	// 				S_DEPT_ID.value = _FindCode.returnValue.DEPT_ID;
	// 				S_CUST_NAME.value = "";
	// 				break;
	// 	case	"S_CUST_NAME" :
	// 				$('#form_calendar')[0].contentWindow.chkAllCheck(false);
	// 				$('#form_calendar')[0].contentWindow.CheckAll(false);

	// 			  is_cust_id        = _FindCode.returnValue.CUST_ID;
	// 			  is_cust_name      = _FindCode.returnValue.CUST_NAME;
	// 			  is_cust_level     = _FindCode.returnValue.CUST_LEVEL_NAME;
	// 			  is_cust_limit_amt = _FindCode.returnValue.M_LIMIT_AMT;
	// 			  is_reduction      = _FindCode.returnValue.REDUCTION_NAME;
	// 			  is_app_rate       = _FindCode.returnValue.APP_RATE_NAME;
	// 			  is_manage_no      = _FindCode.returnValue.COPY_MANAGE_NO;
	// 			  S_CUST_ID.value   = is_cust_id;
	// 			  S_CUST_NAME.value = is_cust_name;

	// 				//jf_CalendarLoaded();
	// 				x_DAO_Retrieve();

	// 				//is_cont_seq = _GS.GetMaxContSeq(is_cust_id, S_DEPT_ID.value, is_yymm);
	// 				is_cont_seq = _GS.GetMaxContSeq(is_cust_id, '%', is_yymm);
	// 				if(is_cont_seq=="0") {
	// 					_X.MsgBox("등록된 계약이 없습니다. 계약을 먼저 생성해야 합니다.");
	// 				}
	// 				break;
	// 	case	"MEMBER_ID" :
	// 				MEMBER_ID.value    = _FindCode.returnValue.MEMBER_ID;
	// 				MEMBER_NAME.value  = _FindCode.returnValue.MEMBER_NAME;
	// 				break;
	// 	case	"MEMBER_ID2" :
	// 				MEMBER_ID2.value   = _FindCode.returnValue.MEMBER_ID;
	// 				MEMBER_NAME2.value = _FindCode.returnValue.MEMBER_NAME;
	// 				break;
	// }
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

// 지사찾기
function uf_findDept() {
	_X.FindDeptCode2('S_DEPT_ID','');
}

function DateChanged(newIndex,curinit){   //선택된 주의 index    초기여부
  //jf_dw_updateconfirm();
	if (curinit == "N"){
		is_date = $("#form_week").contents().find("#td_day_"+newIndex)[0].tdData;
	  sle_basicdate.value = _X.ToString(is_date, 'yyyy-mm-dd');
	  is_index = newIndex;
  	uf_dg1_Retrieve();
  }
	return true;
}


// 달력콘트롤에 일자별 자료를 세팅한다.(월전체)
function jf_CalendarLoaded(){
	var ls_result = _X.XmlSelect("gs", "GS01090", "GET_SERVICESCHEDULE", [S_DEPT_ID.value, is_date], "array");

	if (ls_result.length>0) {
		for(var i=1; i<=ls_result.length; i++) {
			var ls_record = ls_result[i-1];

			//alert(ls_record[0]+":"+ls_record[1]+":"+ls_record[2]+":"+ls_record[3]+":"+ls_record[4]);
			ls_title = _X.ToString(ls_record[1],'MM-DD') + "("+ ls_record[3] +")";
			if(ls_record[2]=="1") {
				ls_title = "<font color='red'>" + ls_title + "</font>";
			}
			if(ls_record[2]=="7") {
				ls_title = "<font color='blue'>" + ls_title + "</font>";
			}
			
			//공휴일 색깔 설정
			var li_holicnt = _X.XmlSelect("gs", "GS01090", "SM_CODE_CALENDAR_R01", [_X.ToString(ls_record[1], 'yyyymmdd')], "array")[0][0];
			if(li_holicnt > 0) {
				ls_title = "<font color='red'>" + ls_title + "</font>";
			}

			$('#form_week')[0].contentWindow.SetTitle(i,ls_title);
			$('#form_week')[0].contentWindow.SetData(i,ls_record[4],_X.ToString(ls_record[1],"YYYY-MM-DD"));
		}
	}

	// 투번째꺼 click
	$('#form_week')[0].contentWindow.SelectionChanged(2);
}

// 달력콘트롤에 일 자료를 세팅한다.
function jf_CalendarSetDataDay(){
	var ls_result = _X.XmlSelect("gs", "GS01090", "GET_SERVICESCHEDULE_R01", [S_DEPT_ID.value, _X.ToString(is_date,"YYYY-MM-DD")], "array");
  if (!(ls_result == null || ls_result == "" || typeof(ls_result) == "undefined" )){
    $('#form_week')[0].contentWindow.SetData(is_index,ls_result,_X.ToString(is_date,"YYYY-MM-DD"));
  }
}


function uf_call_FindRetrieveAfter(){
  var Row = dg_1.GetRow();
  var lst_cust_id = dg_1.GetItem(Row,"CUST_ID");
  _GS.SetRemainAmtCalc(dg_1, lst_cust_id, is_date, 'care');
}

function uf_get_intervalmin(){
}

function uf_get_service_amt() {
	if(dg_1.GetItem(dg_1.GetRow(),"PRODUCT_CODE") == "" || dg_1.GetItem(dg_1.GetRow(),"PRODUCT_CODE") == null) return;
	var ls_stime        = dg_1.GetItem(dg_1.GetRow(), "START_TIME_DIV");
	var ls_smin         = dg_1.GetItem(dg_1.GetRow(), "START_MIN_DIV");
	var ls_etime        = dg_1.GetItem(dg_1.GetRow(), "END_TIME_DIV");
	var ls_emin         = dg_1.GetItem(dg_1.GetRow(), "END_MIN_DIV");
	var li_service_cost = dg_1.GetItem(dg_1.GetRow(), "SERVICE_COST");

	

	/*20120926 김태호 */
		var ll_service_amt = li_service_cost;
		var ll_service_min_1 = 0;
		var ll_service_min_2 = 0;
		var ll_service_min_3 = 0;
  	//가족케어여부
  	var ls_family_yn = _X.XmlSelect("gs", "GS01090", "FAMILY_YN", [dg_1.GetItem(dg_1.GetRow(), "MEMBER_ID")], "array")[0][0];
		if(dg_1.GetItem(dg_1.GetRow(),"PRODUCT_CODE") == "Z9999990") { //기타급여일때는 그냥 단가가 그냥 입력 됨
	  	ls_family_yn = 'Y';
	  }


  	var ls_result = _X.XmlSelect("gs", "GS01090", "GET_SERVICEAMT_EXTRA3", [_X.ToString(is_date, 'YYYY-MM-DD'), ls_stime, ls_smin, ls_etime, ls_emin, li_service_cost, ls_family_yn], "array")[0][0];
	  if (!(ls_result == null || ls_result == "" || typeof(ls_result) == "undefined" )){
	    var la_rtn = ls_result.split('|');
	    ll_service_amt = Number(la_rtn[0]);
	    ll_service_min_1 = Number(la_rtn[1]);
	    ll_service_min_2 = Number(la_rtn[2]);
	    ll_service_min_3 = Number(la_rtn[3]);
		} 


    dg_1.SetItem(dg_1.GetRow(), "SERVICE_AMT", ll_service_amt);
    dg_1.SetItem(dg_1.GetRow(), "SERVICE_MIN_1", ll_service_min_1);
    dg_1.SetItem(dg_1.GetRow(), "SERVICE_MIN_2", ll_service_min_2);
    dg_1.SetItem(dg_1.GetRow(), "SERVICE_MIN_3", ll_service_min_3);
    dg_1.SetItem(dg_1.GetRow(), "SERVICE_MIN", (ll_service_min_1+ll_service_min_2+ll_service_min_3).toString());

		if(dg_1.GetItem(dg_1.GetRow(),"PRODUCT_CODE") == "Z9999990") { //기타급여일때는 그냥 단가가 그냥 입력 됨
	  	dg_1.SetItem(dg_1.GetRow(), "PAY_AMT", ll_service_amt);
	  	dg_1.SetItem(dg_1.GetRow(), "REQ_AMT", 0);
	  }
}

//서비스코드 차일드리트리브 변경
function uf_chg_product_div(a_div, a_row) {
	// if(a_div == "" || a_div == null) {
	// 	a_div = '%';
	// }
	// dg_1.ChildRetrieve4("gs", "product_code",   a_div,"Y","%");			  //서비스명
}

function uf_set_viewcost() {
}


// 마감체크
function uf_CloseCheck(a_dept, a_yymm) {
	if(_GS.IsClosed(a_yymm, a_dept, "service")) {
		return true;
	}

	if(_GS.IsClosed(a_yymm, a_dept, "gong")) {
		return true;
	}
	return false;
}


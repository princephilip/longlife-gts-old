//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : 요보사관리
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================

function x_InitForm2(){
	if(!_X.IsHeadoffice())	{
		$("#btn_finddept").hide();
	}

	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "gs", "GS02010", "GS02010|GS02010_C01", true, true);

	var la_DEPT = _X.XmlSelect("com", "COMMON", "CODE_DEPT_AUTHORITY_R01" , new Array(mytop._UserID), 'json2');
	_X.DDLB_SetData(S_DEPT_ID, la_DEPT, null, true, (_X.IsHeadoffice() ? true : false));
	S_DEPT_ID.value = mytop._DeptCode;
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		// 직급
		var ls_GUBUN_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0103", 'Y'), 'json2');
		_X.DDLB_SetData(GUBUN_DIV, ls_GUBUN_DIV, null, false, false);
		dg_1.SetCombo("GUBUN_DIV", ls_GUBUN_DIV);

		//재직여부
		var ls_WORK_YN = [{"code":"Y","label":"재직중"},{"code":"N","label":"퇴직"}];
		_X.DDLB_SetData(S_WORK_YN, ls_WORK_YN, "Y", false, true);
		_X.DDLB_SetData(WORK_YN, ls_WORK_YN, null, false, false);

		//장애유무
		var ls_HANDI_YN = [{"code":"Y","label":"있음"},{"code":"N","label":"없음"}];
		_X.DDLB_SetData(HANDICAP_YN, ls_HANDI_YN, null, false, false);

		// 장애등급
		var ls_HANDICAP_LEVEL = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0145", 'Y'), 'json2');
		_X.DDLB_SetData(HANDICAP_LEVEL, ls_HANDICAP_LEVEL, null, false, true, "없음");
		dg_1.SetCombo("HANDICAP_LEVEL", ls_HANDICAP_LEVEL);

		// 자격등급
		var ls_LICENSELEVEL_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0104", 'Y'), 'json2');
		_X.DDLB_SetData(LICENSELEVEL_DIV, ls_LICENSELEVEL_DIV, null, false, false);

		// 급여종류
		var ls_PAYTYPE = [{"code":"1","label":"1.시급제"},{"code":"2","label":"2.연봉제"},{"code":"3","label":"3.사업소득"}];
		_X.DDLB_SetData(PAYTYPE, ls_PAYTYPE, null, false, false);

		// 급여계좌
		var ls_BANK_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0100", 'Y'), 'json2');
		_X.DDLB_SetData(BANK_DIV, ls_BANK_DIV, null, false, false);

		// 건강보험
		var ls_HEALTHINSURETAG = [{"code":"1","label":"1.직장가입자자격"},{"code":"2","label":"2.의료급여자"},{"code":"3","label":"3.의료급여수급자"},{"code":"4","label":"4.기초생활수급자"}];
		_X.DDLB_SetData(HEALTHINSURETAG, ls_HEALTHINSURETAG, null, false, false);

		// 부양가족
		var ls_DEDUCT_CNT = [{"code":"1","label":"1명"},{"code":"2","label":"2명"},{"code":"3","label":"3명"},{"code":"4","label":"4명"}];
		_X.DDLB_SetData(DEDUCT_CNT, ls_DEDUCT_CNT, null, false, false);

		// 국민연금
		var ls_NATIONALPENSIONTAG = [{"code":"1","label":"1.가입대상"},{"code":"2","label":"2.60세 이상자"},{"code":"3","label":"3.기초생활수급자"},{"code":"4","label":"4.공무원,군인,사학연금가입자"}];
		_X.DDLB_SetData(NATIONALPENSIONTAG, ls_NATIONALPENSIONTAG, null, false, false);

		// 고용보험
		var ls_GOYONGTAG = [{"code":"1","label":"1.가입대상"},{"code":"2","label":"2.가입대상아님"},{"code":"3","label":"3.이중가입"}];
		_X.DDLB_SetData(GOYONGTAG, ls_GOYONGTAG, null, false, false);

		// 사용기간
		var ls_TRAINEE_YN = [{"code":"Y","label":"적용"},{"code":"N","label":"미적용"}];
		_X.DDLB_SetData(TRAINEE_YN, ls_TRAINEE_YN, null, false, false);

		xe_EditChanged(S_DEPT_ID, S_DEPT_ID.value);

		// setTimeout("x_DAO_Retrieve()",100);
	}
}

function x_DAO_Retrieve2(a_dg){
  dg_1.Retrieve([S_DEPT_ID.value, "%", S_WORK_YN.value]);
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id) {
		case	"S_DEPT_ID":
		case	"S_WORK_YN":
					x_DAO_Retrieve();
					break;
		case	"NAME":
					dg_1.SetItem(dg_1.GetRow(),"DEPOSITOWNER",a_val);
					break;
		case	"LICENSELEVEL_DIV":
			  	if(a_val == '' || a_val == '0000') {
			  		dg_1.SetItem(dg_1.GetRow(), 'LICENSE_NO', "");
			  	}
					break;
		case	"INPUT_RRN_NO":
					var ls_result = _X.XmlSelect("gs", "GS02010", "GET_ENC_RRN", [a_val], "array")[0][0];

			    dg_1.SetItem(dg_1.GetRow(), 'RRN_NO', ls_result);
			    dg_1.SetItem(dg_1.GetRow(), 'REG_CODE', a_val.substr(0,7)+dg_1.GetItem(dg_1.GetRow(), "MEMBER_ID").substr(4, 6));//, 6, '0').substr(4, 6);
					break;
		case	"WORK_YN":
			  	var ls_date = _X.ToString( dg_1.GetItem(dg_1.GetRow(), "RETIRE_DATE"), 'YYYY-MM-DD' );
			  	var ls_today = _X.ToString(_X.GetSysDate(),"YYYY-MM-DD");

			  	if(ls_date=='2999-12-31') {
			  		dg_1.SetItem(dg_1.GetRow(), 'RETIRE_DATE', ls_today );
			  	}
					break;
		case	"RETIRE_DATE":
					// 퇴사일자 있으면 입사일자 수정 못하게
				  _X.FormSetDisable([IN_DATE], (a_val == '') ? false : true);

			  	var ls_today = _X.ToString(_X.GetSysDate(),"YYYY-MM-DD");
			  	// 과거시점이면 '퇴직'자동으로 입력
			  	if(a_val.substr(0, 10)<ls_today) {
			  		dg_1.SetItem(dg_1.GetRow(), 'WORK_YN', 'N' );
			  	}
					break;
		case	"TRAINEE_YN":
					// 사용기간 적용이면 종료일자 입력
				  _X.FormSetDisable([TRAINEE_DATE], (a_val == 'Y') ? false : true);
				  if(a_val != "Y") {
				  	dg_1.SetItem(dg_1.GetRow(), 'TRAINEE_DATE', '');
				  }
					break;
	}
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	switch(a_obj.id) {
		case	"S_EMP_NO":
					if(S_EMP_NO.value != ""){
						var li_fRow =  dg_1.SetFocusByElement('S_EMP_NO',['EMP_NO'], false);
						if(li_fRow > 0) {
							setTimeout("dg_1.SetRow(" + li_fRow.toString() + ")",100);
						}
					}
					break;
		case	"S_EMP_NAME":
					if(S_EMP_NAME.value != ""){
						var li_fRow =  dg_1.SetFocusByElement('S_EMP_NAME',['NAME'], false);
						if(li_fRow > 0) {
							setTimeout("dg_1.SetRow(" + li_fRow.toString() + ")",100);
						}
					}
					break;
	}
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	var cData1 	= dg_1.GetChangedData();

	if(cData1.length > 0){
		var ls_dept = S_DEPT_ID.value;

		for (i=0 ; i < cData1.length; i++){
			if(cData1[i].job == 'D') continue;

			var ls_member = dg_1.GetItem(cData1[i].idx, "MEMBER_ID");
			var ls_date   = dg_1.GetItem(cData1[i].idx, "RETIRE_DATE");

			if(ls_date != "") {
				var li_cnt = _X.XmlSelect("gs", "GS02010", "CHK_SERVICE_SCHEDULE", [ls_member, ls_dept, ls_date], "array")[0][0];

				if(li_cnt>0) {
					_X.MsgBox("확인", cData1[i].idx.toString() + "라인\n퇴사일자 이후에 해당 요보사에 등록된 스케쥴이 있습니다.<br>스케쥴 삭제하시고 퇴사처리 해야 합니다.");
					dg_1.SetRow(cData1[i].idx);
					return false;
				}
			}

			if( dg_1.GetItem(cData1[i].idx, "NAME") == ""){
				dg_1.SetRow(cData1[i].idx);
				_X.MsgBox("확인", " 사원명를 입력하여주시기 바랍니다.");
				return false;
			}

			if( dg_1.GetItem(cData1[i].idx, "IN_DATE") == ""){
				dg_1.SetRow(cData1[i].idx);
				_X.MsgBox("확인", " 입사일자를 입력하여주시기 바랍니다.");
				return false;
			}

			if( dg_1.GetItem(cData1[i].idx, "BANK_DIV") == "999" ){
				dg_1.SetRow(cData1[i].idx);
				_X.MsgBox("확인", " 급여계좌 은행을 선택해야 합니다.");
				return false;
			}

			if( dg_1.GetItem(cData1[i].idx, "NAME") != dg_1.GetItem(cData1[i].idx, "DEPOSITOWNER") ){
				dg_1.SetRow(cData1[i].idx);
				_X.MsgBox("확인", " 이름과 예금주명이 일치해야 합니다.");
				return false;
			}

		}
	}

	return true;
}

//저장후 호출
function x_DAO_Saved2(){
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
	if (a_dg.id == 'dg_1')	{
		var ls_member_id = _X.XmlSelect("gs", "GS02010", "SEQ_MEMBER_ID", [], "array")[0][0];

		dg_1.SetItem(rowIdx, "MEMBER_ID" , ls_member_id);
		dg_1.SetItem(rowIdx, "MEMBER_PWD", "greencare-gts-2016!");  // 요보사 로그인 하지 않음 (비번 임의로 세팅)
		dg_1.SetItem(rowIdx, "MEMBER_DIV", "Y");  // 정임직 구분 사용안함(임금계산에서 사용할까봐 default 'Y' 로 세팅)
		dg_1.SetItem(rowIdx, "WORK_YN"   , "Y");
		dg_1.SetItem(rowIdx, "GUBUN_DIV" , "0205");
		if(S_DEPT_ID.value !== '%'){
    		dg_1.SetItem(rowIdx, "DEPT_ID"  , S_DEPT_ID.value);
        dg_1.SetItem(rowIdx, 'DEPT_NAME', S_DEPT_ID.options[S_DEPT_ID.selectedIndex].text);
    }
		dg_1.SetItem(rowIdx, 'REG_DATE'        , _X.ToString(_X.GetSysDate(), "yyyy-mm-dd"));
		dg_1.SetItem(rowIdx, 'IN_DATE'         , _X.ToString(_X.GetSysDate(), "yyyy-mm-dd"));
		dg_1.SetItem(rowIdx, 'LIINSURANCEDATE' , _X.ToString(_X.GetSysDate(), "yyyy-mm-dd"));
		dg_1.SetItem(rowIdx, 'HANDICAP_YN'     , "N");
		dg_1.SetItem(rowIdx, 'LICENSELEVEL_DIV', "0200");
	}
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
	if(a_dg.id == 'dg_1') {
		// 퇴사일자 있으면 입사일자 수정 못하게
	  _X.FormSetDisable([IN_DATE], (dg_1.GetItem(a_newrow, "RETIRE_DATE") == '') ? false : true);
		// 사용기간 적용이면 종료일자 입력
	  _X.FormSetDisable([TRAINEE_DATE], (dg_1.GetItem(a_newrow, "TRAINEE_YN") == 'Y') ? false : true);
	}
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
	switch(_FindCode.finddiv) {
		case	"ZIP_CODE" :
					var ll_row = dg_1.GetRow();
					dg_1.SetItem(ll_row, "ZIP_CODE"   , a_retVal.zipNo);	// 주소찾이만 이런형식으로 받음
					dg_1.SetItem(ll_row, "ADDR1"      , a_retVal.roadAddrPart1 + a_retVal.roadAddrPart2);
					dg_1.SetItem(ll_row, "ADDR2"      , a_retVal.addrDetail);
					ADDR2.focus();
					break;
	}

	if(_FindCode.returnValue == null) return;

	switch(_FindCode.finddiv) {
		case	"S_DEPT_ID" :
					S_DEPT_ID.value = _FindCode.returnValue.DEPT_ID;
					x_DAO_Retrieve();
					break;
		case	"DEPT_ID" :
					var ll_row = dg_1.GetRow();
					dg_1.SetItem(ll_row, "DEPT_ID"      , _FindCode.returnValue.DEPT_ID);
					dg_1.SetItem(ll_row, "DEPT_NAME"    , _FindCode.returnValue.DEPT_NAME);
					break;
	}
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

// 지사찾기
function uf_findDept() {
	_X.FindDeptCode2('S_DEPT_ID','');
}

//우편번호찾기
function uf_ZipFind(a_param) {
	if(dg_1.RowCount() == 0) return;
	_X.FindStreetAddr(window,a_param,"", "");
}

// 주민번호중복확인
function uf_chk_jumin() {
	if(dg_1.RowCount() == 0) return;
	var li_row = dg_1.GetRow();

  var ls_memberid = _X.Trim(dg_1.GetItem(li_row,'MEMBER_ID'));
  var ls_jumin    = _X.StrPurify(_X.Trim(dg_1.GetItem(li_row,'INPUT_RRN_NO')));;
  var ls_mem_yn   = (ls_memberid == "") ? "N" : "Y";

  var ls_results = _X.XmlSelect("gs", "GS02010", "CHK_JUMIN", [ls_jumin, ls_mem_yn, ls_memberid], "array")[0][0];

  if(ls_jumin.length == 13 || ls_jumin.length == 10)
  {
	  if(ls_results!=null && ls_results!="" && ls_results.length>0)
	  {
	  	var ls_result = "";
	  	for(var i=0; i<ls_results.length; i++)
	  	{
	  		ls_result += ls_results[i][2] + " " + ls_results[i][1] + "\n"
	  	}
	  	_X.MsgBox("중복된 데이타가 존재합니다.\n" + ls_result);
	  }
	  else
	  {
	    _X.MsgBox("중복된 주민번호가 없습니다.");
 	  }
  }
  else
  {
    _X.MsgBox("주민번호 자릿수를 확인해 주시기 바랍니다.");
  }
}

function uf_down_doc(akey, afname) {
	var ls_member = dg_1.GetItem(dg_1.GetRow(), "MEMBER_ID");

	location.href = "/Mighty/jsp/down_report2.jsp?rep="+akey+"&fname="+afname+"&member="+ls_member+"&dept="+S_DEPT_ID.value+"&ym=";
}

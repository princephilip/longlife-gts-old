//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : 서비스 계약관리
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
var p_dept_id 	= '';
var p_cust_name = '';

var is_client_div = 'O';	//I:매입, O:매출, J:지사, E:기타
var is_psep_div 	= "S"
var is_deptcust 	= 'Y';   //Y:지사별고객찾기  , N:전체고객찾기
var is_new = false;
var is_newrow = 0;
//var is_working 		= false;
var ls_print_type = 0;


function x_InitForm2(){
	if(!_X.IsHeadoffice())	{
		$("#btn_finddept").hide();
	}
	_X.Tabs(tabs_1, 0);

	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "gs", "GS01020", "GS01020|GS01020_R01", false, false);
	_X.InitGrid(grid_2, "dg_2", "100%", "100%", "gs", "GS01020", "GS01020|GS01020_C02", true, true);
	_X.InitGrid(grid_3, "dg_3", "100%", "100%", "gs", "GS01020", "GS01020|GS01020_R03", false, false);
	_X.InitGrid(grid_4, "dg_4", "100%", "100%", "gs", "GS01020", "GS01020|GS01020_C04", false, true);

	S_TO_DATE.value = _X.ToString(_X.LastDate(_X.ToString(_X.GetSysDate(),'yyyymmdd')),"yyyy-mm-dd");

	var la_DEPT = _X.XmlSelect("com", "COMMON", "CODE_DEPT_AUTHORITY_R01" , new Array(mytop._UserID), 'json2');
	_X.DDLB_SetData(S_DEPT_ID, la_DEPT, null, true, (_X.IsHeadoffice() ? true : false));
	S_DEPT_ID.value = mytop._DeptCode;

 	var _ITEMS_I = new Array(BIRTH_DATE);
  _X.FormSetDisable(_ITEMS_I, true);

	if(_params != "null"){
		$("#btn_finddept").hide();

		var ls_par = new Array();
		ls_par = _params.split("@");

		p_cust_id = ls_par[0];
		p_dept_id = ls_par[2];

    S_DEPT_ID.value = p_dept_id;
    S_CUST_NAME.value = p_cust_id;
    _X.FormSetDisable([S_DEPT_ID, S_CUST_NAME], true);
	}

}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {

		var la_CUST_LEVEL_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0125", 'Y'), 'json2');
		_X.DDLB_SetData(CUST_LEVEL_DIV, la_CUST_LEVEL_DIV, null, false, false);

		var la_REDUCTION_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0110", 'Y'), 'json2');
		_X.DDLB_SetData(REDUCTION_DIV, la_REDUCTION_DIV, null, false, false);

		var la_APP_RATE_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0111", 'Y'), 'json2');
		_X.DDLB_SetData(APP_RATE_DIV, la_APP_RATE_DIV, null, false, false);

		var la_RELATION_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0116", 'Y'), 'json2');

		dg_2.SetCombo("CUST_LEVEL_DIV", la_CUST_LEVEL_DIV);
		dg_2.SetCombo("RELATION_DIV", la_RELATION_DIV);

		dg_1.SetAutoFooterValue("CUST_NAME", function(idx, ele) {
			var li_cnt = dg_1.RowCount();
			return "전체 : " + li_cnt.toString() + "명";
		});

		dg_2.SetAutoFooterValue("CONT_SEQ", function(idx, ele) {
			var li_cnt = dg_2.RowCount();
			return "총 " + li_cnt.toString() + "회";
		});

		// 계약종류
		var ls_CONT_DEPT_DIV = [{"code":"","label":"선택"},{"code":"방문요양","label":"방문요양"},{"code":"방문목욕","label":"방문목욕"},{"code":"주야간보호","label":"주야간보호"}];
		_X.DDLB_SetData(CONT_DEPT_DIV, ls_CONT_DEPT_DIV, null, false, false);

		//var ls_DAY = [{"code":"1","label":"1일"},{"code":"2","label":"2일"},{"code":"3","label":"3일"},{"code":"4","label":"4일"}];
		var ls_DAY = _X.XmlSelect("com", "COMMON", "SM_COMCODE_D" , new Array("SM", 'DAY'), 'json2');
		_X.DDLB_SetData(CONT_WRITE_DAY, ls_DAY, null, false, false);
		_X.DDLB_SetData(CONT_CALC_DAY, ls_DAY, null, false, false);
		_X.DDLB_SetData(CONT_NOTI_DAY, ls_DAY, null, false, false);
		_X.DDLB_SetData(CONT_PAY_DAY, ls_DAY, null, false, false);

		var ls_PAY_METHOD = [{"code":"현금","label":"현금"},{"code":"신용카드","label":"신용카드"},{"code":"지로","label":"지로"},{"code":"무통장입금","label":"무통장입금"}];
		_X.DDLB_SetData(CONT_PAY_METHOD, ls_PAY_METHOD, null, false, false);

		setTimeout("x_DAO_Retrieve()",100);
	}
}

function x_DAO_Retrieve2(a_dg){
	var ls_allchk = ( CHK_ALLDATE.checked ? "Y" : "N" );
	var ls_from = _X.StrPurify(S_FROM_DATE.value);
	var ls_to   = _X.StrPurify(S_TO_DATE.value);

	dg_4.Reset();
	dg_3.Reset();
	dg_2.Reset();
	dg_1.Retrieve([S_DEPT_ID.value, S_CUST_NAME.value+'%', ls_allchk, ls_from, ls_to]);
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id) {
		case	"S_DEPT_ID":
		case	"S_FROM_DATE":
		case	"S_TO_DATE":
		case	"CHK_ALLDATE":
					x_DAO_Retrieve();
					break;
		case	"CUST_LEVEL_DIV":
		case	"CUST_GUBUN_DIV":
		case	"REDUCTION_DIV":
		case	"APP_RATE_DIV":
					// 경감코드 수정시 비율 자동 세팅
				  if(a_obj.id == 'CUST_GUBUN_DIV' || a_obj.id == 'REDUCTION_DIV'){
				  	uf_set_apprate(dg_2.GetRow());
				  }

					// 등급과 경감비율 수정시 월한도금액, 본인부담금 한도금액 재계산
				  if(a_obj.id == 'CUST_LEVEL_DIV' || a_obj.id == 'REDUCTION_DIV' || a_obj.id == 'APP_RATE_DIV') {
				  	uf_reset_month_amt(dg_2.GetRow());
				  }
					break;
		case	"MANAGE_NO1":
					dg_2.SetItem(dg_2.GetRow(), "MANAGE_NO", a_val+dg_2.GetItem(dg_2.GetRow(), "MANAGE_NO2"));
					break;
		case	"MANAGE_NO2": dg_2.SetItem(dg_2.GetRow(), "MANAGE_NO", dg_2.GetItem(dg_2.GetRow(), "MANAGE_NO1")+a_val);
					break;
	}
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	is_newrow = 0;
	var cData1 	= dg_2.GetChangedData();

	if(cData1.length > 0){
			// 저장후에 계약생성을 위한 파라메터값(신규건)
		_ParmData  = "";

		for (i=0 ; i < cData1.length; i++){
			if(cData1[i].job == 'D') continue;

	    if(dg_2.GetItem(cData1[i].idx, 'CUST_ID') == ""||dg_2.GetItem(cData1[i].idx, 'CUST_ID') == null){
				dg_2.SetRow(cData1[i].idx);
	      _X.MsgBox('확인', "계약고객을 조회하시기 바랍니다.");
	      return false;
	    }
	    if (dg_2.GetItem(cData1[i].idx,'CONT_SDATE') == null || dg_2.GetItem(cData1[i].idx,'CONT_SDATE') == ""
	  		    || _X.ToString(dg_2.GetItem(cData1[i].idx,'CONT_SDATE'), 'YYYY-MM-DD')=='1928-10-28') {
				dg_2.SetRow(cData1[i].idx);
	  		_X.MsgBox('확인', "계약시작일을 입력하시기 바랍니다.");
	      return false;
	    }
	    if (dg_2.GetItem(cData1[i].idx,'CUST_LEVEL_DIV') == null || dg_2.GetItem(cData1[i].idx,'CUST_LEVEL_DIV') == ""){
				dg_2.SetRow(cData1[i].idx);
	  		_X.MsgBox('확인', "장기요양등급을 입력하시기 바랍니다.");
	      return false;
	    }

	    if (dg_2.GetItem(cData1[i].idx,'REDUCTION_DIV') == null || dg_2.GetItem(cData1[i].idx,'REDUCTION_DIV') == ""){
				dg_2.SetRow(cData1[i].idx);
	  		_X.MsgBox('확인', "경감구분코드를 입력하시기 바랍니다.");
	      return false;
	    }

			if (dg_2.GetItem(cData1[i].idx,'APP_RATE_DIV') == null || dg_2.GetItem(cData1[i].idx,'APP_RATE_DIV') == ""){
				dg_2.SetRow(cData1[i].idx);
	  		_X.MsgBox('확인', "경감비율을 입력하시기 바랍니다.");
	      return false;
	    }

			if(dg_2.GetItem(cData1[i].idx,'CUST_LEVEL_DIV') != '6') {
		    if (dg_2.GetItem(cData1[i].idx,'M_LIMIT_AMT') == 0 || dg_2.GetItem(cData1[i].idx,'M_LIMIT_AMT') == null) {
					dg_2.SetRow(cData1[i].idx);
		  		_X.MsgBox('확인', "월 한도금액을 입력하시기 바랍니다.");
		      return false;
		    }
		  }

			if(cData1[i].job == 'I'){
				is_newrow = cData1[i].idx;
			}
		}
	}

	var cData4 	= dg_4.GetChangedData();

	if(cData4.length > 0){
		var li_seq = _X.ToInt(dg_4.GetTotalValue("PLAN_SEQ", "MAX"));
		for (i=0 ; i < cData4.length; i++){
			if(cData4[i].job == 'I'){
				li_seq++;
				dg_4.SetItem(cData4[i].idx, "PLAN_SEQ", li_seq);
			}
		}
	}

	return true;
}

//저장후 호출
function x_DAO_Saved2(){
	var ls_cust = dg_1.GetItem(dg_1.GetRow(), "CUST_ID");
	if(is_new) {
		var li_seq  = dg_2.GetItem(is_newrow, "CONT_SEQ");

		// 이전계약 종료일 수정
		if(li_seq>1) {
			_X.ES("gs", "GS01020", "SERVICE_CONTRACT_U01", [ls_cust, li_seq]);
		}
	}

	var li_result = _X.ExecProc('gs', 'PR_GS_CUST_UPDATE', [ls_cust] );
  if ( li_result < 0){
  	_X.MsgBox('확인', "고객정보 수정 중 에러가 발생했습니다.");
  }

	is_new = false;
	xe_GridRowFocusChanged(dg_1, dg_1.GetRow());
}

function x_DAO_Insert2(a_dg, row){
	// if(a_dg.id == 'dg_2') {
	//   if (is_new) {
	//   	_X.MsgBox("추가된 건을 저장 후 작업해 주세요.\n한 건씩만 추가 가능 합니다.");
	// 		return 0;
	//   }
	// }

  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
	if(a_dg.id == 'dg_2') {
		var ls_custid = dg_1.GetItem(dg_1.GetRow(),"CUST_ID");
		var li_cont_seq = _X.XmlSelect("gs", "GS01020", "MAX_CONT_SEQ", [ls_custid], "array")[0][0];

    if (!(li_cont_seq == null || li_cont_seq == "" || typeof(li_cont_seq) == "undefined" )){
      dg_2.SetItem(rowIdx, 'CONT_SEQ', Number(li_cont_seq));
    }

    dg_2.SetItem(rowIdx, 'DEPT_ID'         , S_DEPT_ID.value);
    dg_2.SetItem(rowIdx, 'DEPT_NAME'       , S_DEPT_ID.options[S_DEPT_ID.selectedIndex].text);
    
    dg_2.SetItem(rowIdx, 'SERVICE_CONT_DIV', 'I');  //신규계약
    dg_2.SetItem(rowIdx, 'REG_DATE'        , _X.ToString(_X.GetSysDate(), "yyyy-mm-dd"));

    var ls_rtn = _X.XmlSelect("gs", "GS01020", "CODE_CUST_R01", [ls_custid], "array")[0];

    dg_2.SetItem(rowIdx, 'CUST_CODE'     , ls_rtn[0]);
    dg_2.SetItem(rowIdx, 'CUST_NAME'     , ls_rtn[1]);
    dg_2.SetItem(rowIdx, 'CUST_ID'       , ls_rtn[2]);
    dg_2.SetItem(rowIdx, 'HP'            , ls_rtn[3]);
    dg_2.SetItem(rowIdx, 'PHONE'         , ls_rtn[4]);
    dg_2.SetItem(rowIdx, 'ZIP_CODE'      , ls_rtn[5]);
    dg_2.SetItem(rowIdx, 'ADDR1'         , ls_rtn[6]);
    dg_2.SetItem(rowIdx, 'ADDR2'         , ls_rtn[7]);
    dg_2.SetItem(rowIdx, 'CUST_LEVEL_DIV', ls_rtn[8]);
    dg_2.SetItem(rowIdx, 'APP_RATE_DIV'  , ls_rtn[9]);
    dg_2.SetItem(rowIdx, 'REDUCTION_DIV' , ls_rtn[10]);
    dg_2.SetItem(rowIdx, 'MANAGE_NO'     , ls_rtn[11]);
    dg_2.SetItem(rowIdx, 'MANAGE_NO1'    , ls_rtn[12]);
    dg_2.SetItem(rowIdx, 'MANAGE_NO2'    , ls_rtn[13]);
    dg_2.SetItem(rowIdx, 'CARE_SDATE'    , ls_rtn[14]);
    dg_2.SetItem(rowIdx, 'CARE_EDATE'    , ls_rtn[15]);
    dg_2.SetItem(rowIdx, 'CONT_SDATE'    , _X.ToString(_X.GetSysDate(), "yyyy-mm-dd"));
    dg_2.SetItem(rowIdx, 'CONT_EDATE'    , ls_rtn[15]);
    dg_2.SetItem(rowIdx, 'PRO_SEQ'       , ls_rtn[16]);
    dg_2.SetItem(rowIdx, 'PRO_NAME'      , ls_rtn[17]);
    dg_2.SetItem(rowIdx, 'PRO_PHONE'     , ls_rtn[18]);
    dg_2.SetItem(rowIdx, 'PRO_HP'        , ls_rtn[19]);
    dg_2.SetItem(rowIdx, 'RELATION_DIV'  , ls_rtn[20]);
    dg_2.SetItem(rowIdx, 'RELATION_DIV_NAME', ls_rtn[21]);

    if(uf_set_apprate(rowIdx)) {
    	uf_reset_month_amt(rowIdx);
    }

	 	is_new = true;
	}


	if(a_dg.id == 'dg_4') {
		dg_4.SetItem(rowIdx, "CUST_ID", dg_2.GetItem(dg_2.GetRow(), "CUST_ID"));
		dg_4.SetItem(rowIdx, "CONT_SEQ", dg_2.GetItem(dg_2.GetRow(), "CONT_SEQ"));
		dg_4.SetItem(rowIdx, "FROM_HOUR", "00");
		dg_4.SetItem(rowIdx, "FROM_MIN",  "00");
		dg_4.SetItem(rowIdx, "TO_HOUR", "00");
		dg_4.SetItem(rowIdx, "TO_MIN",  "00");
	}

}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
	if(a_dg.id == 'dg_2') {
		var ls_custid = dg_1.GetItem(dg_1.GetRow(),"CUST_ID");
		var li_cont_seq = _X.XmlSelect("gs", "GS01020", "MAX_CONT_SEQ", [ls_custid], "array")[0][0];

    if (!(li_cont_seq == null || li_cont_seq == "" || typeof(li_cont_seq) == "undefined" )){
      dg_2.SetItem(rowIdx, 'CONT_SEQ', Number(li_cont_seq));
    }

    dg_2.SetItem(rowIdx, 'DEPT_ID'         , S_DEPT_ID.value);
    dg_2.SetItem(rowIdx, 'DEPT_NAME'       , S_DEPT_ID.options[S_DEPT_ID.selectedIndex].text);
    
    dg_2.SetItem(rowIdx, 'SERVICE_CONT_DIV', 'I');  //신규계약
    dg_2.SetItem(rowIdx, 'REG_DATE'        , _X.ToString(_X.GetSysDate(), "yyyy-mm-dd"));

    var ls_rtn = _X.XmlSelect("gs", "GS01020", "CODE_CUST_R01", [ls_custid], "array")[0];

    dg_2.SetItem(rowIdx, 'CUST_CODE'     , ls_rtn[0]);
    dg_2.SetItem(rowIdx, 'CUST_NAME'     , ls_rtn[1]);
    dg_2.SetItem(rowIdx, 'CUST_ID'       , ls_rtn[2]);
    dg_2.SetItem(rowIdx, 'HP'            , ls_rtn[3]);
    dg_2.SetItem(rowIdx, 'PHONE'         , ls_rtn[4]);
    dg_2.SetItem(rowIdx, 'ZIP_CODE'      , ls_rtn[5]);
    dg_2.SetItem(rowIdx, 'ADDR1'         , ls_rtn[6]);
    dg_2.SetItem(rowIdx, 'ADDR2'         , ls_rtn[7]);
    dg_2.SetItem(rowIdx, 'CUST_LEVEL_DIV', ls_rtn[8]);
    dg_2.SetItem(rowIdx, 'APP_RATE_DIV'  , ls_rtn[9]);
    dg_2.SetItem(rowIdx, 'REDUCTION_DIV' , ls_rtn[10]);
    dg_2.SetItem(rowIdx, 'MANAGE_NO'     , ls_rtn[11]);
    dg_2.SetItem(rowIdx, 'MANAGE_NO1'    , ls_rtn[12]);
    dg_2.SetItem(rowIdx, 'MANAGE_NO2'    , ls_rtn[13]);
    dg_2.SetItem(rowIdx, 'CARE_SDATE'    , ls_rtn[14]);
    dg_2.SetItem(rowIdx, 'CARE_EDATE'    , ls_rtn[15]);
    dg_2.SetItem(rowIdx, 'CONT_SDATE'    , _X.ToString(_X.GetSysDate(), "yyyy-mm-dd"));
    dg_2.SetItem(rowIdx, 'CONT_EDATE'    , ls_rtn[15]);
    dg_2.SetItem(rowIdx, 'PRO_SEQ'       , ls_rtn[16]);
    dg_2.SetItem(rowIdx, 'PRO_NAME'      , ls_rtn[17]);
    dg_2.SetItem(rowIdx, 'PRO_PHONE'     , ls_rtn[18]);
    dg_2.SetItem(rowIdx, 'PRO_HP'        , ls_rtn[19]);
    dg_2.SetItem(rowIdx, 'RELATION_DIV'  , ls_rtn[20]);
    dg_2.SetItem(rowIdx, 'RELATION_DIV_NAME', ls_rtn[21]);

    if(uf_set_apprate(rowIdx)) {
    	uf_reset_month_amt(rowIdx);
    }

	 	is_new = true;
	}
}

function x_DAO_Delete2(a_dg, row){
	if(a_dg.id == 'dg_2') {
	  if(dg_2.GetItem(row, "job") == "I") {
	  	is_new = false;
	  } else {
	  	if(is_new) {
	  		_X.MsgBox("계약 변경건을 작업후에 삭제해 주세요.");
	  		return 0;
	  	}
	  }
	}

	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
	return 0;
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	xe_BodyResize();
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
	if(a_dg.id == 'dg_1') {
		dg_4.Reset();
		dg_3.Reset();
	  dg_2.Retrieve([dg_1.GetItem(a_newrow, 'CUST_ID')]);
  	uf_set_doc_link();
	}

	if(a_dg.id == 'dg_2') {
		if (a_newrow > 0 && dg_2.GetItem(a_newrow, "job") != "I")    {	// 신규가아닌경우
			// if (jf_dw_updateconfirm() == 3)	{
			// 	dwc.SetActionCode(1);
			// 	return;
			// }

			var ls_from = _X.StrPurify(S_FROM_DATE.value);
			var ls_to   = _X.StrPurify(S_TO_DATE.value);
		  var li_cust_id  = dg_2.GetItem(a_newrow, 'CUST_ID');
		  var li_cont_seq = dg_2.GetItem(a_newrow, 'CONT_SEQ');
		  if(li_cust_id == '' || li_cust_id == null ) { return 0;}
		  else if(li_cont_seq == '' || li_cont_seq == null ) { return 0;}

		  dg_3.Retrieve([S_DEPT_ID.value, li_cust_id, ls_from, ls_to]);
		  dg_4.Retrieve([li_cust_id, li_cont_seq]);
		}
	}
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
	if(a_dg.id == 'dg_1') {
		is_working = false;

		uf_Set_Color();
	}
}

function uf_Set_Color() {
	var s_red = { "color": "#FF0000" };
	var s_blk = { "color": "#000000" };
	var s_gry = { "color": "#C8C8C8" };

	for(var i = 1; i <= dg_1.RowCount(); i++){
		var ll_dday = _X.ToInt(dg_1.GetItem(i, "CARE_EDATE_DDAY"));
		var li_ord  = _X.ToInt(dg_1.GetItem(i, "SORT_ORDER"));
		dg_1.SetCellStyle(i, "CUST_NAME"    , ( ll_dday <= 90 && ll_dday > 0 )  ? s_red : s_blk);

		dg_1.SetCellStyle(i, "BIRTH_DATE"   , ( li_ord==0 && ll_dday <= 90 && ll_dday>0) ? s_red : ( li_ord==0 && ll_dday > 90 ) ? s_blk : s_gry);
		dg_1.SetCellStyle(i, "CONT_SEQ"     , ( li_ord==0 && ll_dday <= 90 && ll_dday>0) ? s_red : ( li_ord==0 && ll_dday > 90 ) ? s_blk : s_gry);
		dg_1.SetCellStyle(i, "MANAGE_STATUS", ( li_ord==0 && ll_dday <= 90 && ll_dday>0) ? s_red : ( li_ord==0 && ll_dday > 90 ) ? s_blk : s_gry);
		dg_1.SetCellStyle(i, "CUST_ID"      , ( li_ord==0 && ll_dday <= 90 && ll_dday>0) ? s_red : ( li_ord==0 && ll_dday > 90 ) ? s_blk : s_gry);
	}
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
	if(_FindCode.returnValue == null) return;

	switch(_FindCode.finddiv) {
		case	"S_DEPT_ID" :
					S_DEPT_ID.value = _FindCode.returnValue.DEPT_ID;
					x_DAO_Retrieve();
					break;
		case	"PRO_SEQ" :
					var li_row = dg_2.GetRow();
					dg_2.SetItem(li_row, "PRO_SEQ", _FindCode.returnValue.PRO_SEQ);
					dg_2.SetItem(li_row, "PRO_NAME", _FindCode.returnValue.PRO_NAME);
					dg_2.SetItem(li_row, "RELATION_DIV_NAME", _FindCode.returnValue.RELATION_DIV_NAME);
					dg_2.SetItem(li_row, "PRO_PHONE", _FindCode.returnValue.PHONE);
					dg_2.SetItem(li_row, "PRO_HP", _FindCode.returnValue.HP);
					break;
	}
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

// 지사찾기
function uf_findDept() {
	_X.FindDeptCode2('S_DEPT_ID','');
}

function uf_set_apprate(a_row) {
  var ls_gubun = "A"; // 대상자 관리 복사부분 dg_2.GetItem(a_row,'cust_gubun_div');
  var ls_reduc = dg_2.GetItem(a_row,'REDUCTION_DIV');

  if(ls_gubun == 'A') {       //노인장기수급자
      if(ls_reduc == 'B') {   //경감사유 없음
        dg_2.SetItem(a_row,'APP_RATE_DIV','0010');    //적용요율 0%
      } else if(ls_reduc == 'M') { // 의료급여
        dg_2.SetItem(a_row,'APP_RATE_DIV','0100');    //적용요율 7.5%
      } else { // 일반등급 (G)
    		dg_2.SetItem(a_row,'APP_RATE_DIV','0200');    //적용요율 15%
    	}
  } else if(ls_gubun == 'G') {  //일반
    dg_2.SetItem(a_row,'APP_RATE_DIV','1000');   // 적용요율 100%
  }

  return true;
}

function uf_reset_month_amt(a_row) {
	var ls_level = dg_2.GetItem(a_row, "CUST_LEVEL_DIV");
	var ls_rate  = dg_2.GetItem(a_row, "APP_RATE_DIV");

	if(ls_level!="") {
  	var li_rtn = _GS.GetMonthLimitAmt(ls_level, ls_rate);
  	dg_2.SetItem(a_row, "M_LIMIT_AMT", _X.ToInt(li_rtn.limit_amt));
  	dg_2.SetItem(a_row, "REQ_LIMIT_AMT", _X.ToInt(li_rtn.req_limit_amt));
	}
}

function jf_cust_modify() {
	if(dg_1.RowCount() > 0) {
		_GS.UpdateCustomer(S_DEPT_ID.value, dg_1.GetItem(dg_1.GetRow(), "CUST_ID"));
	}
}

function uf_set_doc_link() {
	var ls_cust = dg_2.GetItem(dg_2.GetRow(), "CUST_ID") + "";
	var ls_html = "";
	ls_html = "<ul style='list-style: none; padding: 0; margin: 10px 0 ;'>"
					+ "<li style='padding: 10px; border-bottom: 1px dotted #ccc'>" + '<img align="absMiddle" src="/Theme/images/ico_hwp.gif" border="0" hspace="0"></img>' + " <a href='javascript:uf_down_doc(\"rpt_00010.hml\", \"제공기록지\", \""+ls_cust+"\", \""+S_DEPT_ID.value+"\", \"201601\")'>제공기록지(방문요양)</a>" + "</li>"
					+ "<li style='padding: 10px; border-bottom: 1px dotted #ccc'>" + '<img align="absMiddle" src="/Theme/images/ico_hwp.gif" border="0" hspace="0"></img>' + " <a href='javascript:uf_down_doc(\"rpt_00020.hml\", \"납부확인서\", \""+ls_cust+"\", \""+S_DEPT_ID.value+"\", \"201601\")'>장기요양급여비 납부확인서</a>" + "</li>"
					+ "<ul>";
	form_doc.innerHTML = ls_html;
}

function uf_down_doc(akey, afname, acust, adept, aym) {
	location.href = "/Mighty/jsp/down_report.jsp?rep="+akey+"&fname="+afname+"&cust="+acust+"&dept="+adept+"&ym="+aym;
}

function uf_append_contract() {
  //데이타가 변경된 경우 저장
  if(dg_2.IsDataChanged()) {
  	_X.MsgBox("변경된 자료가 있습니다.\n저장 후 작업해 주세요.");
		return;
  }

	dg_2.DuplicateRow(dg_2.GetRow());
}

// 보호자목록
function uf_protector(){
	if(dg_2.RowCount() <= 0) return;
	var ls_custid = dg_2.GetItem(dg_2.GetRow(), "CUST_ID");

	_X.CommonFindCode(window,"보호자 찾기","com",'PRO_SEQ','',[ls_custid],"FindCustProtector|FindCode|FIND_CUSTPROTECTOR",700, 700);
}

// 월별일정변경
function uf_month_change() {
	var ls_custid   = "";
	var ls_custname = "";
	var ls_deptid   = "";

	if(dg_1.GetRow() > 0) {
		ls_custid   = dg_2.GetItem(dg_2.GetRow(), "CUST_ID");
		ls_custname = dg_2.GetItem(dg_2.GetRow(), "CUST_NAME");
		ls_deptid   = S_DEPT_ID.value;
	}

	var ls_dept_name = S_DEPT_ID.options[S_DEPT_ID.selectedIndex].text ;

	if(ls_dept_name.indexOf("주야간")>0) {
		mytop._X.MDI_Open('GS01050', mytop._MyPgmList['GS01050'].PGM_NAME, '/Mighty/template/XG_Slick1.jsp?pcd=GS01050&params='+ls_custid+'@'+ls_custname+'@'+ls_deptid);
	} else {
		mytop._X.MDI_Open('GS01040', mytop._MyPgmList['GS01040'].PGM_NAME, '/Mighty/template/XG_Slick1.jsp?pcd=GS01040&params='+ls_custid+'@'+ls_custname+'@'+ls_deptid);
	}
}

// 이용계약서
function uf_print_cont(){
  if(dg_4.RowCount()<=0) {
  	_X.MsgBox("확인", "급여계획을 입력 후, '인쇄'해야 합니다.");
		return 0;
  }

  var ls_FileBaseUrl = window.location.origin + mytop._rootFileUpload;
	//ls_FileBaseUrl = ls_FileBaseUrl.replace("\\","\/").replace("\\","\/");
	var li_row 			= dg_2.GetRow();
	var ls_dept_div = dg_2.GetItem(li_row, "CONT_DEPT_DIV");
	var ls_cust_id	=	dg_2.GetItem(li_row, "CUST_ID");
	var ls_cont_seq	=	dg_2.GetItem(li_row, "CONT_SEQ");

  if(ls_dept_div == "") {
  	_X.MsgBox("확인", "계약종류를 입력 해야 합니다.");
		return 0;
  }

	if(ls_dept_div=="방문요양") {
		_X.SetiReport("gs",  "GS01020R_S0_A",  "AS_CUST_ID=" + ls_cust_id + "&AS_CONT_SEQ=" + ls_cont_seq);
	} else if(ls_dept_div=="방문목욕") {
		_X.SetiReport("gs",  "GS01020R_SB0_B", "AS_CUST_ID=" + ls_cust_id + "&AS_CONT_SEQ=" + ls_cont_seq);
	} else if(ls_dept_div=="주야간보호") {
		_X.SetiReport("gs",  "GS01020R_SC0_C", "AS_CUST_ID=" + ls_cust_id + "&AS_CONT_SEQ=" + ls_cont_seq);
	}
}


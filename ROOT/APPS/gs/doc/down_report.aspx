<%@Page language="C#" Debug="true" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="Mighty.DB" %>
<%@ Import Namespace="Mighty.Util" %>

<script language="C#" runat="server">
    private void Page_Load(object sender, System.EventArgs e)
    {
        // rep=greentest&fname=제공계획서&cust=13249&dept=1038&ym=201601
        string ls_rep = Request["rep"];
        string ls_fname = Request["fname"];
        string ls_cust = Request["cust"];
        string ls_dept = Request["dept"];
        //string ls_member = Request["member"];
        string ls_ym = Request["ym"];
        
        string ls_fpath = @"E:\Mighty\Net\Mighty.Net.GTS2016\APPS\gs\doc\"+ls_rep;
        string ls_src = File.ReadAllText(ls_fpath, Encoding.UTF8);
        
        string ls_cust_code = "";
        string ls_cust_name = "";
        string ls_birth = "";
        string ls_manage_no = "";
        string ls_level = "";
        
        string ls_sql = "";
        string ts_sql = null;
        
        ts_sql = @"
        SELECT 
				  A.CUST_ID, 
				  '19' || SUBSTR(A.CUST_CODE, 1, 2) ||'-'||SUBSTR(A.CUST_CODE, 3, 2)||'-'||SUBSTR(A.CUST_CODE, 5, 2) AS BIRTH,
				  A.CUST_CODE,
				  A.CUST_NAME, 
				  A.DEPT_ID, 
				  A.ZIP_CODE ||') '|| A.ADDR1 ||' '|| A.ADDR2 AS ADDRESS, 
				  A.HP, 
				  A.PHONE,
				  A.MANAGE_NO,
				  (SELECT COMDIV_NAME FROM CODE_COMDIV B WHERE A.CUST_LEVEL_DIV=B.COMDIV_CODE AND B.COMSEC_CODE='0125') AS CUST_LEVEL_NAME
				FROM 
				  CODE_CUST A
				WHERE A.CUST_ID=#as_cust_id# ";
              
        ls_sql = ts_sql.Replace("#as_cust_id#", ls_cust);
        //ls_sql = ls_sql.Replace("#as_sbj_type#", sbj_type);
              
        using (Mighty.DB.Ora.XSQLRun ptsql_a = new Mighty.DB.Ora.XSQLRun("comm"))
        {
            ptsql_a.executeSQL(ls_sql);
            if (ptsql_a.rs.Read())
            {
                ls_cust_code = Mighty.Util.XUtility.toString(ptsql_a.rs["CUST_CODE"]);
                ls_cust_name = Mighty.Util.XUtility.toString(ptsql_a.rs["CUST_NAME"]);
                ls_birth = Mighty.Util.XUtility.toString(ptsql_a.rs["BIRTH"]);
                ls_manage_no = Mighty.Util.XUtility.toString(ptsql_a.rs["MANAGE_NO"]);
                ls_level = Mighty.Util.XUtility.toString(ptsql_a.rs["CUST_LEVEL_NAME"]);
            }
            
            ptsql_a.rs.Close();
        }
        
        // 텍스트 변환
        ls_src = ls_src.Replace("{{cust_code}}", ls_cust_code);
        ls_src = ls_src.Replace("{{name}}", ls_cust_name);
        ls_src = ls_src.Replace("{{birth}}", ls_birth);
        ls_src = ls_src.Replace("{{level}}", ls_level);
        ls_src = ls_src.Replace("{{manage_no}}", ls_manage_no);
        
        
        Response.Clear();
				Response.ContentType = "application/hml";
				Response.ContentEncoding = Encoding.UTF8;
				Response.AppendHeader("Content-Disposition", "attachment; filename="+Server.UrlEncode(ls_fname+'_'+ls_cust_name)+".hml");
				//Response.TransmitFile(filePath);
				Response.Write(ls_src);
				Response.End();
        
        //Response.Write(ls_cust_name);
        
        
    }
    
    public static string uf_td(string vStr) 
    {
        if(vStr==null||vStr=="") {
            vStr="&nbsp;";
        }
        
        return "<td align='center'>" + vStr + "</td>";
    }
    
    public static string uf_trtd(string vStr) 
    {
        if(vStr==null||vStr=="") {
            vStr="&nbsp;";
        }
        
        return "<tr><td>" + vStr + "</td></tr>\r";
    }
</script>


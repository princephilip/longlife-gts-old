<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_3' gridCall="true" pgmcode="GS01080"></html:authbutton>
		</span>
	</div>

	<div class='option_label w100'>지사명</div>
	<div class='option_input_bg'>
		<select id="S_DEPT_ID" style="width:260px"></select>
		<img id="btn_finddept" src="/Theme/images/btn/btn_find.gif" class="search_find_img" onClick="uf_findDept()"/>
	</div>

	<div class='option_label w100'>구분</div>
	<div class='option_input_bg'>
		<select id="S_CUST_DIV" class="option_input_fix" style="width:130px"></select>
	</div>

	<div class='option_label w100'>고객</div>
	<div class='option_input_bg'>
		<input id='S_CUST_NAME' type='text' class='option_input_fix' style="width:80px">
	</div>

	<div class='option_label'>전표년월</div>
	<div class='option_input_bg'>
		<html:basicdate id="dateimages" classType="search" dateType="month" moveBtn="true" readOnly="false"></html:basicdate>
	</div>

	<div class='grid_div' style="float:right;">
		<div class='option_label'>출력일자</div>
		<div class='option_input_bg'>
			<html:basicdate id="dateimages" classType="search" dateType="date" moveBtn="true" readOnly="true"></html:basicdate>
		</div>
	</div>

	<div class="option_line"></div>

	<div id='userxbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<button type="button" id='btn_boninsunab' class="btn btn-success btn-xs" onClick="uf_boninsunab();">본인부담금수납대장</button>
			<button type="button" id='btn_gubyebiyong' class="btn btn-success btn-xs" onClick="uf_gubyebiyong();">급여비용명세서출력</button>
			<button type="button" id='btn_gubyebiyongxls' class="btn btn-success btn-xs" onClick="uf_gubyebiyongxls();">급여비용명세서엑셀</button>
		</span>
	</div>

</div>


<div id='form_left' class='grid_div'>
	<div id='grid_1' class='grid_div mr5 has_title'>
		<label class='sub_title'>미수금현황</label>
		<div id="dg_1" class="slick-grid"></div>
	</div>

	<div id='grid_2' class='grid_div mr5 mt5 has_title'>
		<label class='sub_title'>월별정산내역</label>
		<div id="dg_2" class="slick-grid"></div>
	</div>
</div>

<div id='grid_3' class='grid_div has_title'>
	<label class='sub_title'>입금내역</label>
	<div id="dg_3" class="slick-grid"></div>
</div>



<script type="text/javascript">
	topsearch.ResizeInfo			= {init_width:1000, init_height: 60, width_increase:1, height_increase:0};
	form_left.ResizeInfo			= {init_width: 520, init_height:635, width_increase:0, height_increase:1};
		grid_1.ResizeInfo				= {init_width: 520, init_height:310, width_increase:0, height_increase:1};
		grid_2.ResizeInfo				= {init_width: 520, init_height:325, width_increase:0, height_increase:0};
	grid_3.ResizeInfo					= {init_width: 480, init_height:635, width_increase:1, height_increase:1};
</script>


<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<jsp:include page="/Mighty/jsp/UploadExcelForm.jsp"/>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true" pgmcode="GS01030"></html:authbutton>
		</span>
	</div>

	<div class='option_label w100'>지사명</div>
	<div class='option_input_bg'>
		<select id="S_DEPT_ID" style="width:260px"></select>
		<img id="btn_finddept" src="/Theme/images/btn/btn_find.gif" class="search_find_img" onClick="uf_findDept()"/>
	</div>

	<div class='option_label w100'>고객</div>
	<div class='option_input_bg'>
		<input id='S_CUST_NAME' type='text' class='option_input_fix' style="width:100px">
		<img id="btn_findcust" src="/Theme/images/btn/btn_find.gif" class="search_find_img" onClick="uf_findCust()"/>
	</div>

</div>

<div id='grid_1' class='grid_div mr5'>
	<div id="dg_1" class="slick-grid"></div>
</div>

<div id='form_right' class='grid_div'>

	<div id='form_free1' class='detail_box ver2 has_title'>
		<label class='sub_title'>이용자(갑)</label>

		<div class='detail_row'>
			<label class='detail_label' style="width:110px">성명</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='CUST_NAME' class='detail_input_fix'>
			</div>

			<label class='detail_label' style="width:110px">고객번호</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='CUST_ID' class='detail_input_fix'>
			</div>

			<label class='detail_label' style="width:110px">장기요양등급</label>
			<div class='detail_input_bg' style="width:150px">
				<select id="CUST_LEVEL_DIV" class='detail_input_fix'></select>
			</div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:110px">장기요양관리번호</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='MANAGE_NO' class='detail_input_fix'>
			</div>
			<label class='detail_label' style="width:110px">생년월일</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='BIRTH_DATE' class='detail_date'>
			</div>
			<label class='detail_label' style="width:110px">연락처(자택)</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='PHONE' class='detail_input_fix'>
			</div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:110px">주소</label>
			<div class='detail_input_bg has_button' style="width:120px">
				<input type='text' id='ZIP_CODE' class='detail_input_fix'>
				<button id="btn_zipfind" class="search_find_img" onClick="uf_ZipFind('ZIP_CODE')"></button>
			</div>
			<div class='detail_input_bg noborder' style="width:300px">
				<input type='text' id='ADDR1' class='detail_input_fix'>
			</div>
			<div class='detail_input_bg noborder' style="width:250px">
				<input type='text' id='ADDR2' class='detail_input_fix'>
			</div>
		</div>

		<div class='detail_row detail_row_bb'>
			<label class='detail_label' style="width:110px">기타</label>
			<div class='detail_input_bg' style="width:150px">
				<select id="CUST_GUBUN_DIV" class='detail_input_fix'></select>
			</div>
			<div class='detail_input_bg noborder' style="width:260px">
			</div>
			<div class='detail_input_bg noborder' style="width:260px">
			</div>
		</div>
	</div>

	<div id='form_free2' class='detail_box ver2 has_title mt5' >
	<label class='sub_title'>제공자(을)</label>
		<div class='detail_row'>
			<label class='detail_label' style="width:110px">시설명</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='DEPT_NAME' class='detail_input_fix'>
			</div>
			<label class='detail_label' style="width:110px">시설종류</label>
			<div class='detail_input_bg' style="width:150px">
				<select id="DEPT_DIV" class='detail_input_fix'></select>
			</div>
			<label class='detail_label' style="width:110px">대표자 성명</label>
			<div class='detail_input_bg noborder' style="width:150px">
				<input type='text' id='OWNER_NAME' class='detail_input_fix'>
			</div>
		</div>

		<div class='detail_row detail_row_bb'>
			<label class='detail_label' style="width:110px">연락처</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='DEPT_PHONE' class='detail_input_fix'>
			</div>
			<label class='detail_label' style="width:110px">주소</label>
			<div class='detail_input_bg' style="width:410px">
				<input type='text' id='DEPT_ADDR' class='detail_input_fix'>
			</div>
		</div>
  </div>

	<div id='form_free3' class='detail_box ver2 has_title mt5'>
		<label class='sub_title'>대리인 또는 보호자(병)</label>
		<div class='detail_row'>
			<label class='detail_label' style="width:110px">성명</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='PRO_NAME' class='detail_input_fix'>
			</div>

			<label class='detail_label' style="width:110px">관계</label>
			<div class='detail_input_bg' style="width:150px">
				<select id="RELATION_DIV" class='detail_input_fix'></select>
			</div>

			<label class='detail_label' style="width:110px">생년월일</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='PRO_BIRTH_DATE' class='detail_date'>
			</div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:110px">연락처(자택)</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='PRO_PHONE' class='detail_input_fix'>
			</div>

			<label class='detail_label' style="width:110px">휴대전화</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='PRO_HP' class='detail_input_fix'>
			</div>
			<div class='detail_input_bg noborder' style="width:260px">
			</div>
		</div>

		<div class='detail_row detail_row_bb'>
			<label class='detail_label' style="width:110px">주소</label>
			<div class='detail_input_bg has_button' style="width:120px">
				<input type='text' id='PRO_ZIP_CODE' class='detail_input_fix'>
				<button id="btn_zipfind2" class="search_find_img" onClick="uf_ZipFind('PRO_ZIP_CODE')"></button>
			</div>
			<div class='detail_input_bg noborder' style="width:300px">
				<input type='text' id='PRO_ADDR1' class='detail_input_fix'>
			</div>
			<div class='detail_input_bg noborder' style="width:250px">
				<input type='text' id='PRO_ADDR2' class='detail_input_fix'>
			</div>
		</div>

	</div>

	<div id='form_free4' class='detail_box ver2 has_title mt5'>
		<label class='sub_title'>계약내용</label>

		<div class='detail_row'>

	    <label class='detail_label' style="width:110px">계약기간</label>
			<div class='detail_input_bg' style="width:90px">
				<input type='text' id='FROM_DATE' class='detail_date' readonly>
			</div>
			<div class='detail_input_bg noborder' style="width:20px;text-align:center">~</div>
			<div class='detail_input_bg noborder' style="width:90px">
				<input type='text' id='TO_DATE' class='detail_date' readonly>
			</div>
			<div class='detail_input_bg noborder' style="width:210px;"></div>

			<label class='detail_label' style="width:110px">통보서작성일자</label>
			<div class='detail_input_bg' style="width:40px;text-align:right">당월</div>
	    <div class="detail_input_bg noborder" style="width:70px">
	    	<select id="WRITE_DAY" class='detail_input_fix'></select>
	    </div>
	    <div class='detail_input_bgnoborder' style="width:40px;"></div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:110px">급여비용정산일자</label>
			<div class='detail_input_bg' style="width:40px;text-align:right">매월</div>
	    <div class="detail_input_bg noborder" style="width:70px">
	    	<select id="CALC_DAY" class='detail_input_fix'></select>
	    </div>
			<div class='detail_input_bgnoborder' style="width:40px;">까지</div>

	    <label class='detail_label' style="width:110px">급여비용통보일자</label>
			<div class='detail_input_bg' style="width:40px;text-align:right">매월</div>
	    <div class="detail_input_bg noborder" style="width:70px">
	    	<select id="NOTI_DAY" class='detail_input_fix'></select>
	    </div>
			<div class='detail_input_bgnoborder' style="width:40px;">까지</div>

	    <label class='detail_label' style="width:110px">급여비용납부일자</label>
			<div class='detail_input_bg' style="width:40px;text-align:right">매월</div>
	    <div class="detail_input_bg noborder" style="width:70px">
	    	<select id="PAY_DAY" class='detail_input_fix'></select>
	    </div>
			<div class='detail_input_bgnoborder' style="width:40px;">까지</div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:110px">급여비용납부방법</label>
	    <div class="detail_input_bg" style="width:150px">
	    	<select id="PAY_METHOD" class='detail_input_fix'></select>
	    </div>
	    <label class='detail_label' style="width:110px">계약일자</label>
	    <div class="detail_input_bg" style="width:150px">
	    	 <input type="text" id="CONTRACT_DATE" class="detail_date">
	    </div>
			<div class='detail_input_bgnoborder' style="width:260px;"></div>
		</div>

		<div class='detail_row detail_row_bb'>
	    <label class='detail_label' style="width:110px">작성자</label>
	    <div class="detail_input_bg" style="width:150px">
	    	 <input type="text" id="MEMBER_NAME" class="detail_input_fix">
	    </div>
	    <label class='detail_label' style="width:110px">작성일자</label>
	    <div class="detail_input_bg" style="width:150px">
	    	<input type="text" id="WRITE_DATE" class="detail_date">
	    </div>
			<div class='detail_input_bgnoborder' style="width:260px;"></div>
		</div>

	</div>
</div>


<div id='grid_2' class='grid_div has_title has_button'>
	<label class='sub_title'>급여계획	</label>
	<div class='child_button float_right'>
		<html:authchildbutton id='buttons' grid='dg_2' append='true' insert='false' duplicate='false' delete='true' excel='false' retrieve='false' ></html:authchildbutton>
	</div>
	<div id="dg_2" class="slick-grid"></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo			= {init_width:1000, init_height: 33, width_increase:1, height_increase:0};
	grid_1.ResizeInfo					= {init_width: 220, init_height:665, width_increase:1, height_increase:1};
	form_right.ResizeInfo			= {init_width: 780, init_height:500, width_increase:0, height_increase:0};
	grid_2.ResizeInfo					= {init_width: 780, init_height:160, width_increase:0, height_increase:1};
</script>


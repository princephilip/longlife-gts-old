<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true" pgmcode="GS01070"></html:authbutton>
		</span>
	</div>

	<div class='option_label w100'>지사명</div>
	<div class='option_input_bg'>
		<select id="S_DEPT_ID" style="width:260px"></select>
		<img id="btn_finddept" src="/Theme/images/btn/btn_find.gif" class="search_find_img" onClick="uf_findDept()"/>
	</div>

	<div class='option_line'></div>

	<div class='option_label'>조회기간</div>
	<div class='option_input_bg'>
		<html:fromtodate id="dateimages" classType="option" dateType="date" moveBtn="true" readOnly="false"></html:fromtodate>
	</div>

	<div class='option_label w100'>사번/성명</div>
	<div class='option_input_bg'>
		<input id='S_EMP_FIND' type='text' class='option_input_fix' style="width:100px">
	</div>

	<div class='option_label w100'>구분</div>
	<div class='option_input_bg'>
		<select id="S_CHECK_YN" style="width:100px"></select>
	</div>

	<div class='option_label w100'>고객</div>
	<div class='option_input_bg'>
		<input id='S_CUST_FIND' type='text' class='option_input_fix' style="width:100px">
	</div>

</div>

<div id='grid_1' class='grid_div'>
	<div id="dg_1" class="slick-grid"></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo			= {init_width:1000, init_height: 60, width_increase:1, height_increase:0};
	grid_1.ResizeInfo					= {init_width:1000, init_height:640, width_increase:1, height_increase:1};
</script>


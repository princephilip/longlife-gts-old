<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<%
	String params = request.getParameter("params");
%>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true" pgmcode="GS01010"></html:authbutton>
		</span>
	</div>

	<div id="top_condition">
		<div class='option_label w100'>지사명</div>
		<div class='option_input_bg'>
			<select id="S_DEPT_ID" style="width:260px"></select>
			<img id="btn_finddept" src="/Theme/images/btn/btn_find.gif" class="search_find_img" onClick="uf_findDept()"/>
		</div>

		<div class='option_label'>기준일자</div>
		<div class="option_input_bg" >
			<html:fromtodate id="dateimages"  classType="option" dateType="date" moveBtn="true" readOnly="false"></html:fromtodate>
		</div>

		<div class="option_label">
			<A href="javascript:if(!CHK_ALLDATE.disabled){CHK_ALLDATE.checked=!CHK_ALLDATE.checked;xe_EditChanged(CHK_ALLDATE,CHK_ALLDATE.value)}" target=_self><INPUT onclick=xe_EditChanged(this,this.value) id=CHK_ALLDATE type=checkbox value="" name=CHK_ALLDATE>전체기간</A>
		</div>

		<div class='option_label w100'>고객</div>
		<div class='option_input_bg'>
			<input id='S_CUST_NAME' type='text' class='option_input_fix'>
		</div>
	</div>

</div>

<div id='grid_1' class='grid_div mr5'>
	<div id="dg_1" class="slick-grid"></div>
</div>

<div id='form_right' class='grid_div'>

	<div id='form_free1' class='detail_box ver2 has_title'>
		<label class='sub_title'>기본정보</label>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">*이름</label>
			<div class='detail_input_bg' style="width:90px">
				<input type='text' id='CUST_NAME' class='detail_input_fix'>
			</div>
	    <div class="detail_input_bg noborder" style="width:60px">
				<select id="SEX" class='detail_input_fix'></select>
	    </div>

			<label class='detail_label' style="width:100px">고객번호</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='CUST_ID' class='detail_input_fix' readonly>
			</div>

			<label class='detail_label' style="width:100px">*생일</label>
			<div class='detail_input_bg' style="width:84px">
				<input type='text' id='BIRTH_DATE' class='detail_date'>
			</div>
	    <div class="detail_input_bg noborder" style="width:66px">
				<button type="button" id='btn_jumin' class="btn btn-success btn-xs" onClick="uf_chk_jumin();" title="이름/생일로 중복 확인을 합니다.">중복확인</button>
	    </div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">핸드폰</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='HP' class='detail_input_fix'>
			</div>
			<label class='detail_label' style="width:100px">전화번호</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='PHONE' class='detail_input_fix'>
			</div>
			<label class='detail_label' style="width:100px">고객구분</label>
			<div class='detail_input_bg' style="width:150px">
				<select id="CUST_GUBUN_DIV" class='detail_input_fix'></select>
			</div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">담당센터</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='DEPT_NAME' class='detail_input_fix' readonly>
			</div>
			<label class='detail_label' style="width:100px">현금영수증TEL</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='CASH_RECEIPT_NO' class='detail_input_fix'>
			</div>
			<label class='detail_label' style="width:100px">주요질환</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='MAIN_DIS' class='detail_input_fix'>
			</div>
		</div>

		<div class='detail_row detail_row_bb'>
			<label class='detail_label' style="width:100px">가상계좌</label>
			<div class='detail_input_bg' style="width:400px">
				<input type='text' id='VIRTUAL_DEPOSIT' class='detail_input_fix' readonly>
			</div>
			<label class='detail_label' style="width:100px">지로번호</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='JIRO_NO' class='detail_input_fix' readonly>
			</div>
		</div>

	</div>

	<div id='form_free2' class='grid_div has_title has_button' >
		<label class='sub_title'>주소</label>
	  <div class='child_button float_right' style="margin-right:3px;">
      <button type="button" class="btn btn-success btn-xs" id="btn_copy_addr" onclick="uf_copy_addr('CHK_ADDR')" title="고객주소를 지로주소로 복사 합니다.">고객주소복사</button>
      <button type="button" class="btn btn-success btn-xs" id="btn_copy_proaddr" onclick="uf_copy_addr('COPY_PRO_ADDR')" title="보호자주소를 지로주소로 복사 합니다.">보호자주소복사</button>
	  </div>

		<div id='form_free2d' class='detail_box ver2'>
			<div class='detail_row'>
				<label class='detail_label' style="width:100px">주소</label>
		    <div class="detail_input_bg has_button" style="width:120px">
		      <input type="text" id="ZIP_CODE">
		      <button id="btn_zipfind" class="search_find_img" onClick="uf_ZipFind('ZIP_CODE')"></button>
		    </div>
		    <div class="detail_input_bg noborder" style="width:280px">
		      <input type="text" id="ADDR1">
		    </div>
		    <div class="detail_input_bg noborder" style="width:250px">
		      <input type="text" id="ADDR2">
		    </div>
			</div>

			<div class='detail_row detail_row_bb'>
				<label class='detail_label' style="width:100px">지로주소</label>
		    <div class="detail_input_bg has_button" style="width:120px">
		      <input type="text" id="DM_ZIP_CODE">
		      <button id="btn_zipfind2" class="search_find_img" onClick="uf_ZipFind('DM_ZIP_CODE')"></button>
		    </div>
		    <div class="detail_input_bg noborder" style="width:280px">
		      <input type="text" id="DM_ADDR1">
		    </div>
		    <div class="detail_input_bg noborder" style="width:250px">
		      <input type="text" id="DM_ADDR2">
		    </div>
			</div>
		</div>
  </div>

	<div id='form_free3' class='grid_div has_title has_button' >
		<label class='sub_title'>보호자</label>
	  <div class='child_button float_right' style="margin-right:3px;">
      <button type="button" class="btn btn-success btn-xs" id="btn_protector" onclick="uf_protector()" title="보호자 목록을 관리 합니다.">보호자 목록</button>
	  </div>

		<div id='form_free3d' class='detail_box ver2'>
			<div class='detail_row'>
				<label class='detail_label' style="width:100px">보호자이름</label>
				<div class='detail_input_bg' style="width:150px">
					<input type='text' id='PRO_NAME' class='detail_input_fix' readonly>
				</div>
		    <div class="detail_input_bg noborder" style="width:160px">
					<select id="RELATION_DIV" class='detail_input_fix'></select>
		    </div>
		    <div class="detail_input_bg noborder" style="width:90px">
					<input type='text' id='PRO_BIRTH' class='detail_input_fix' style="text-align:center" readonly>
		    </div>

				<label class='detail_label' style="width:100px">핸드폰</label>
				<div class='detail_input_bg' style="width:150px">
					<input type='text' id='PRO_HP' class='detail_input_fix' readonly>
				</div>
			</div>

			<div class='detail_row'>
				<label class='detail_label' style="width:100px">이메일</label>
				<div class='detail_input_bg' style="width:400px">
					<input type='text' id='PRO_EMAIL' class='detail_input_fix' readonly>
				</div>
				<label class='detail_label' style="width:100px">전화</label>
				<div class='detail_input_bg' style="width:150px">
					<input type='text' id='PRO_PHONE' class='detail_input_fix' readonly>
				</div>
			</div>

			<div class='detail_row detail_row_bb'>
				<label class='detail_label' style="width:100px">주소</label>
		    <div class="detail_input_bg" style="width:80px">
		      <input type="text" id="PRO_ZIP_CODE" readOnly>
		    </div>
		    <div class="detail_input_bg noborder" style="width:320px">
		      <input type="text" id="PRO_ADDR1" readonly>
		    </div>
		    <div class="detail_input_bg noborder" style="width:250px">
		      <input type="text" id="PRO_ADDR2" readonly>
		    </div>
			</div>

		</div>
	</div>

	<div id='form_free4' class='grid_div has_title has_button' >
		<label class='sub_title'>요양서비스 관련</label>
	  <div class='child_button float_right' style="margin-right:3px;">
      <span id="t_service" style="color:blue;" >※ 대상자 등록 후, 요양서비스 관련 정보는 "서비스 계약관리"에서 수정하실 수 있습니다.</span>
	  </div>

		<div id='form_free4d' class='detail_box ver2'>
			<div class='detail_row'>
				<label class='detail_label' style="width:100px">요양인정번호</label>
				<div class='detail_input_bg' style="width:105px">
					<input type='text' id='MANAGE_FIX' class='detail_input_fix' style="width:10px" readonly><input type='text' id='MANAGE_NO1' class='detail_input_fix' style="width:83px">
				</div>
				<div class='detail_input_bg noborder' style="width:45px">
					<input type='text' id='MANAGE_NO2' class='detail_input_fix'>
				</div>
				<div class='detail_input_bg noborder' style="width:250px">
					(예, L1234567890-123)
				</div>
				<div class='detail_input_bg noborder' style="width:250px">
					<input type='text' id='COPY_MANAGE_NO' class='detail_input_fix' readonly>
				</div>
			</div>

			<div class='detail_row'>
				<label class='detail_label' style="width:100px">인정유효기간</label>
				<div class='detail_input_bg' style="width:90px">
					<input type='text' id='START_DATE' class='detail_date' readonly>
				</div>
				<div class='detail_input_bg noborder' style="width:20px;text-align:center">~</div>
				<div class='detail_input_bg noborder' style="width:90px">
					<input type='text' id='END_DATE' class='detail_date' readonly>
				</div>
				<div class='detail_input_bg noborder' style="width:200px;"></div>
				<label class='detail_label' style="width:100px">장기요양등급</label>
				<div class='detail_input_bg' style="width:150px">
					<select id="CUST_LEVEL_DIV" class='detail_input_fix'></select>
				</div>
			</div>

			<div class='detail_row'>
				<label class='detail_label' style="width:100px">경감구분</label>
				<div class='detail_input_bg' style="width:150px">
					<select id="REDUCTION_DIV" class='detail_input_fix'></select>
				</div>
				<label class='detail_label' style="width:100px">적용요율</label>
				<div class='detail_input_bg' style="width:150px">
					<select id="APP_RATE_DIV" class='detail_input_fix'></select>
				</div>
				<label class='detail_label' style="width:100px">월한도금액</label>
				<div class='detail_input_bg' style="width:150px">
					<input type='text' id='M_LIMIT_AMT' class='detail_amt' readonly>
				</div>
			</div>

			<div class='detail_row detail_row_bb'>
				<label class='detail_label' style="width:100px">보장기관</label>
				<div class='detail_input_bg has_button' style="width:150px">
					<input type='text' id='MANAGE_OFFICE_DIV_NAME' class='detail_input_fix' readonly>
					<button id="btn_officefind" class="search_find_img" onclick="uf_findOffice()"></button>
				</div>
				<label class='detail_label' style="width:100px">기관월한도액</label>
				<div class='detail_input_bg' style="width:150px">
					<input type='text' id='OFFICE_AMT' class='detail_amt' readonly>
				</div>
				<label class='detail_label' style="width:100px">본인부담금한도</label>
				<div class='detail_input_bg' style="width:150px">
					<input type='text' id='REQ_LIMIT_AMT' class='detail_amt' readonly>
				</div>
			</div>
		</div>
	</div>

	<div id='form_free5' class='detail_box ver2 has_title'>
		<label class='sub_title'>비고</label>
		<div class='detail_row detail_row_bb'>
			<div class='detail_input_bg' style="width:750px">
				<textarea id="REMARK" style="height:75px;"></textarea>
			</div>
		</div>
	</div>

	<div id='form_free6' class='grid_div has_button' >
	  <div class='child_button'>
      <button type="button" class="btn btn-success btn-xs" id="btn_open_member" onclick="uf_open_member()">요보사 등록</button>
      <button type="button" class="btn btn-success btn-xs" id="btn_service_contract" onclick="uf_service_contract()">서비스 계약관리</button>
      <button type="button" class="btn btn-success btn-xs" id="btn_schedule_change_m" onclick="uf_month_change()">월별일정변경</button>
	  </div>

		<div id='form_free6d' class='detail_box ver2' style="margin-top:5px">
			<span style="color:red;" >※ 장기요양보험 인정유효기간이 3달(90일)이하로 남은 고객이 빨간색으로 표시합니다.</span><br>
			<span style="color:red;" >※ 기준일자를 설정하면, 해당 기간에 서비스 일정이 등록된 고객만 조회합니다.</span>
		</div>
  </div>

	<div id='grid_2' class='grid_div has_title'>
		<label class='sub_title'>담당 센터</label>
		<div id="dg_2" class="slick-grid"></div>
	</div>

</div>

<div id='grid_excel' class='grid_div' style='visibility:hidden'>
	<div id="dg_excel" class="slick-grid"></div>
</div>


<script type="text/javascript">
	var _params = "<%=params%>";

	topsearch.ResizeInfo			= {init_width:1000, init_height: 35, width_increase:1, height_increase:0};
	grid_1.ResizeInfo					= {init_width: 250, init_height:665, width_increase:1, height_increase:1};

	form_right.ResizeInfo			= {init_width: 750, init_height:665, width_increase:0, height_increase:1};
		form_free1.ResizeInfo		= {init_width: 750, init_height:150, width_increase:0, height_increase:0};
		form_free2.ResizeInfo		= {init_width: 750, init_height: 95, width_increase:0, height_increase:0};
		form_free2d.ResizeInfo	= {init_width: 750, init_height: 70, width_increase:0, height_increase:0};
		form_free3.ResizeInfo		= {init_width: 750, init_height:120, width_increase:0, height_increase:0};
		form_free3d.ResizeInfo	= {init_width: 750, init_height: 95, width_increase:0, height_increase:0};
		form_free4.ResizeInfo		= {init_width: 750, init_height:145, width_increase:0, height_increase:0};
		form_free4d.ResizeInfo	= {init_width: 750, init_height:120, width_increase:0, height_increase:0};
		form_free5.ResizeInfo		= {init_width: 750, init_height:125, width_increase:0, height_increase:0};
		form_free6.ResizeInfo		= {init_width: 750, init_height: 80, width_increase:0, height_increase:0};
		form_free6d.ResizeInfo	= {init_width: 750, init_height: 60, width_increase:0, height_increase:0};
		grid_2.ResizeInfo				= {init_width: 750, init_height:150, width_increase:0, height_increase:0};

	grid_excel.ResizeInfo			= {init_width: 100, init_height: 10, width_increase:0, height_increase:0};


</script>


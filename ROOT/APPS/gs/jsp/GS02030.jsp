<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true" pgmcode="GS02030"></html:authbutton>
		</span>
	</div>

	<div class='option_label w100'>지사명</div>
	<div class='option_input_bg'>
		<select id="S_DEPT_ID" style="width:260px"></select>
	</div>

</div>

<div id='grid_1' class='grid_div'>
	<div id="dg_1" class='slick-grid'></div>
</div>

<div id='from_1' class='grid_div ml5'>
	<IFRAME id="form_calendar" src="/APPS/com/jsp/calendar0.jsp" width="100%" height="100%" marginwidth="0" marginheight="0" frameborder="0" scrolling="yes"></IFRAME>
</div>

<div id='grid_2' class='grid_div mt5'>
	<div id="dg_2" class='slick-grid'></div>
</div>


<script type="text/javascript">
	topsearch.ResizeInfo			= {init_width:1000, init_height: 33, width_increase:1, height_increase:0};
	grid_1.ResizeInfo					= {init_width: 210, init_height:495, width_increase:0, height_increase:0.75};
	from_1.ResizeInfo					= {init_width: 790, init_height:495, width_increase:1, height_increase:0.75};
	grid_2.ResizeInfo					= {init_width:1000, init_height:165, width_increase:1, height_increase:0.25};
</script>


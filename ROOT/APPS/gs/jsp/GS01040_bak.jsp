<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.Calendar" %>
<%@ page import="java.text.DecimalFormat"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<%
DecimalFormat df = new DecimalFormat("00");
Calendar calendar = Calendar.getInstance();

String curr_yy = Integer.toString(calendar.get(Calendar.YEAR)); //년도를 구한다
String curr_mm = df.format(calendar.get(Calendar.MONTH) + 1); //달을 구한다
String curr_dd = df.format(calendar.get(Calendar.DATE)); //날짜를 구한다
String curr_dy = "";

int iDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK); //요일을 구한다
switch(iDayOfWeek){
  case 1: curr_dy = "일"; break;
  case 2: curr_dy = "월"; break;
  case 3: curr_dy = "화"; break;
  case 4: curr_dy = "수"; break;
  case 5: curr_dy = "목"; break;
  case 6: curr_dy = "금"; break;
  case 7: curr_dy = "토"; break;
}
%>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true"></html:authbutton>
		</span>
	</div>

	<div class='option_label w100'>지사명</div>
	<div class='option_input_bg'>
		<select id="S_DEPT_ID" style="width:260px"></select>
		<img id="btn_finddept" src="/Theme/images/btn/btn_find.gif" class="search_find_img" onClick="uf_findDept()"/>
	</div>

	<div class='option_label w100'>고객</div>
	<div class='option_input_bg'>
		<input id='S_CUST_NAME' type='text' class='option_input_fix' style="width:100px">
		<img id="btn_findcust" src="/Theme/images/btn/btn_find.gif" class="search_find_img" onClick="uf_findCust()"/>
	</div>

	<div class='option_input_bg' style="border-width: 0px 0px 0px 0px;">
		<button type="button" id='btn_add_calendar' class="btn btn-success btn-xs" onClick="uf_show_modify();">달력에 추가/삭제</button>
		<button type="button" id='btn_add_schedule' class="btn btn-success btn-xs" onClick="uf_schedule_batch();">반복일정 추가/삭제</button>
	</div>

	<div class='option_input_bg' style="float:right;background-color: #f8f8f8;border-width: 0px 0px 0px 0px;">
		<div class="option_input_bg" style="padding:3px 5px 0px 5px;background-color: #f8f8f8;border-width: 0px 0px 0px 0px;">
			<span style="color:black;">인쇄구분 :</span>
		</div>
		<INPUT onclick=xe_EditChanged(rb_print,this.value) id=rb_print_0 type=radio value='H' name=rb_print CHECKED>
		<A href="javascript:if(!rb_print_0.disabled){rb_print_0.checked=true;xe_EditChanged(rb_print,rb_print_0.value);}" target=_self>가로</A>
		<INPUT onclick=xe_EditChanged(rb_print,this.value) id=rb_print_1 type=radio value='V' name=rb_print>
		<A href="javascript:if(!rb_print_1.disabled){rb_print_1.checked=true;xe_EditChanged(rb_print,rb_print_1.value);}" target=_self>세로</A>
	</div>

	<div id='userxbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<button type="button" id='btn_recalc' class="btn btn-success btn-xs" onClick="uf_recalc();">재계산</button>
		</span>
	</div>

</div>


<div id='form_calendar' class='grid_div'>

	<table id='calendarHtml' width="100%" height="100%" border="1" bordercolor="#c0c0c0" cellspacing="0" cellpadding="2"
		style="FONT-SIZE: 9pt; FONT-FAMILY: Dotum; BORDER-COLLAPSE: collapse">
		<tr>
			<td height="41" colspan=3 align=center id="td_summary">
				<iframe src="about:blank" id="if_summary" frameborder="0" style="width: 100%; height: 100%; overflow-x:hidden; "></iframe>
			</td>
			<td height="41" style="padding-left:15; line-height: 186%;" colspan=1 align=center>
				<select id="selYear" name="selYear" align="absmiddle" style="width:60px" onchange="uf_setCalendar()">
          <option value=2007>&nbsp;2007&nbsp;</option>
          <option value=2008>&nbsp;2008&nbsp;</option>
          <option value=2009>&nbsp;2009&nbsp;</option>
          <option value=2010>&nbsp;2010&nbsp;</option>
          <option value=2011>&nbsp;2011&nbsp;</option>
          <option value=2012>&nbsp;2012&nbsp;</option>
          <option value=2013>&nbsp;2013&nbsp;</option>
          <option value=2014>&nbsp;2014&nbsp;</option>
          <option value=2015>&nbsp;2015&nbsp;</option>
          <option value=2016>&nbsp;2016&nbsp;</option>
          <option value=2017>&nbsp;2017&nbsp;</option>
          <option value=2018>&nbsp;2018&nbsp;</option>
          <option value=2019>&nbsp;2019&nbsp;</option>
				</select>년
				<select id="selMonth" name="selMonth" align="absmiddle" style="width:45px" onchange="uf_setCalendar()">
				<option value=01 >&nbsp;01&nbsp;</option><option value=02 >&nbsp;02&nbsp;</option><option value=03 >&nbsp;03&nbsp;</option><option value=04 >&nbsp;04&nbsp;</option><option value=05 >&nbsp;05&nbsp;</option><option value=06 >&nbsp;06&nbsp;</option><option value=07 >&nbsp;07&nbsp;</option><option value=08 >&nbsp;08&nbsp;</option><option value=09 >&nbsp;09&nbsp;</option><option value=10 >&nbsp;10&nbsp;</option><option value=11 >&nbsp;11&nbsp;</option><option value=12 >&nbsp;12&nbsp;</option>
				</select>월
				<br />
				전체 선택/해제 <input type="checkbox" name="chkAll" onClick="CheckAll(this.checked)">
				<br />
				오늘 :
				<font style="font-size:10pt;color:#FF8040;font-weight:bold"><%=curr_yy%></font>년&nbsp;
				<font style="font-size:10pt;color:#FF8040;font-weight:bold"><%=curr_mm%></font>월&nbsp;
				<font style="font-size:10pt;color:#FF8040;font-weight:bold"><%=curr_dd%></font>일
				(<font style="font-size:10pt;color:#FF8040;font-weight:bold"><%=curr_dy%></font>)
				<br />
				<b>급여한도 : <span id="m_limit_amt">0</span> 원<br />
				잔여금액 : <span id="m_remain_amt">0</span> 원</b>
			</td>
			<td colspan=3 align=center id="td_remark">
			</td>
		</tr>

		<tr height="30" align="center" valign="middle" bgcolor="#efeffa">
			<td width="14.2%">일 <input type="checkbox" name="chkWeek" value=0 onClick="CheckWeek(this.value,this.checked)">(선택/해제)</td>
			<td width="14.3%">월 <input type="checkbox" name="chkWeek" value=1 onClick="CheckWeek(this.value,this.checked)">(선택/해제)</td>
			<td width="14.3%">화 <input type="checkbox" name="chkWeek" value=2 onClick="CheckWeek(this.value,this.checked)">(선택/해제)</td>
			<td width="14.3%">수 <input type="checkbox" name="chkWeek" value=3 onClick="CheckWeek(this.value,this.checked)">(선택/해제)</td>
			<td width="14.3%">목 <input type="checkbox" name="chkWeek" value=4 onClick="CheckWeek(this.value,this.checked)">(선택/해제)</td>
			<td width="14.3%">금 <input type="checkbox" name="chkWeek" value=5 onClick="CheckWeek(this.value,this.checked)">(선택/해제)</td>
			<td width="14.3%">토 <input type="checkbox" name="chkWeek" value=6 onClick="CheckWeek(this.value,this.checked)">(선택/해제)</td>
		</tr>
		<!-- <div id='calendarHtml'> -->
	</table>

</div>

<div id='grid_1' class='grid_div' style= "visibility:hidden">
	<div id="dg_1" class='slick-grid'></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo			= {init_width:1000, init_height: 33, width_increase:1, height_increase:0};
	form_calendar.ResizeInfo	= {init_width:1000, init_height:660, width_increase:1, height_increase:1};
	grid_1.ResizeInfo					= {init_width:1000, init_height: 10, width_increase:0, height_increase:0};
</script>


<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true" pgmcode="GS02020"></html:authbutton>
		</span>
	</div>

	<div class='option_label w100'>지사명</div>
	<div class='option_input_bg'>
		<select id="S_DEPT_ID" style="width:260px"></select>
		<img id="btn_finddept" src="/Theme/images/btn/btn_find.gif" class="search_find_img" onClick="uf_findDept()"/>
	</div>

	<div class='option_label'>기준년월</div>
	<div class='option_input_bg'>
		<html:basicdate id="dateimages" classType="search" dateType="month" moveBtn="true" readOnly="true"></html:basicdate>
	</div>

	<div class='option_input_bg' style="background-color: #f8f8f8;border-width: 0px 0px 0px 0px;">
		<div class="option_input_bg" style="padding:3px 5px 0px 5px;background-color: #f8f8f8;border-width: 0px 0px 0px 0px;color:#666633;">
			<span id="msg_txt" style="color:red;"></span>
		</div>
	</div>

	<!-- 두번째줄 -->
	<div class="option_line"></div>

	<div class='option_input_bg D_RANGE' style='float: right;'>
		<input id='S_RANGE' type="range" class="S_RANGE_BAR" min="0" max="665" step="10" name="range" value="320" style='width: 100px;'>
	</div>
	<div class='option_label D_RANGE' style='float: right;'>화면비율</div>

	<div class='option_input_bg' style="float:right;background-color: #f8f8f8;border-width: 0px 0px 0px 0px;">
		<div class="option_input_bg" style="padding:3px 5px 0px 5px;background-color: #f8f8f8;border-width: 0px 0px 0px 0px;">
			<span style="color:black;">인쇄구분 :</span>
		</div>
		<INPUT onclick=xe_EditChanged(rb_print,this.value) id=rb_print_0 type=radio value=1 name=rb_print CHECKED>
		<A href="javascript:if(!rb_print_0.disabled){rb_print_0.checked=true;xe_EditChanged(rb_print,rb_print_0.value);}" target=_self>급여대장</A>
		<INPUT onclick=xe_EditChanged(rb_print,this.value) id=rb_print_1 type=radio value=2 name=rb_print>
		<A href="javascript:if(!rb_print_1.disabled){rb_print_1.checked=true;xe_EditChanged(rb_print,rb_print_1.value);}" target=_self>급여명세서</A>
	</div>

	<div id='userxbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<button type="button" id='btn_paymentresult' class="btn btn-success btn-xs" onClick="uf_PaymentResult();">급여집계</button>
			<button type="button" id='btn_pay_close'     class="btn btn-success btn-xs" onClick="uf_pay_closing();">급여마감</button>
			<button type="button" id='btn_special_close' class="btn btn-success btn-xs" onClick="uf_special_closing();">특별수당마감</button>
		</span>
	</div>
</div>

<div id="tabs_1" class='tabs_div'>
	<div id="ctab_nav" class="tabs_nav">
		<ul>
			<li><a href="#ctab_1">급여계산표</a></li>
			<li><a href="#ctab_2">월급여대장</a></li>
		</ul>
	</div>

	<div id="ctab_1" class='tabc_div'>
		<div id='grid_1' class='grid_div'>
			<div id="dg_1" class='slick-grid'></div>
		</div>
	</div>
	<div id="ctab_2" class='tabc_div'>
		<div id='grid_2' class='grid_div'>
			<div id="dg_2" class='slick-grid'></div>
		</div>
	</div>
</div>

<div id='grid_3' class='grid_div has_title mt5'>
	<label class='sub_title'>서비스 내역</label>
	<div id="dg_3" class="slick-grid"></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo 		= {init_width:1000, init_height: 58, width_increase:1, height_increase:0};
	tabs_1.ResizeInfo 	 		= {init_width:1000, init_height:320, width_increase:1, height_increase:1};
		ctab_nav.ResizeInfo 	= {init_width:1000, init_height: 25, width_increase:1, height_increase:0};
		ctab_1.ResizeInfo 	 	= {init_width:1000, init_height:295, width_increase:1, height_increase:1};
			grid_1.ResizeInfo		= {init_width:1000, init_height:295, width_increase:1, height_increase:1};
		ctab_2.ResizeInfo 	 	= {init_width:1000, init_height:295, width_increase:1, height_increase:1};
			grid_2.ResizeInfo		= {init_width:1000, init_height:295, width_increase:1, height_increase:1};

	grid_3.ResizeInfo		 		= {init_width:1000, init_height:320, width_increase:1, height_increase:0};
</script>


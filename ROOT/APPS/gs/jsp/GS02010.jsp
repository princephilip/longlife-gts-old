<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<jsp:include page="/Mighty/jsp/UploadExcelForm.jsp"/>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true" pgmcode="GS02010"></html:authbutton>
		</span>
	</div>

	<div class='option_label w100'>지사명</div>
	<div class='option_input_bg'>
		<select id="S_DEPT_ID" style="width:260px"></select>
		<img id="btn_finddept" src="/Theme/images/btn/btn_find.gif" class="search_find_img" onClick="uf_findDept()"/>
	</div>

	<div class='option_label w100'>구분</div>
	<div class='option_input_bg'>
		<select id="S_WORK_YN" class="option_input_fix" style="width:100px"></select>
	</div>

	<div class='option_label w100'>사번/성명</div>
	<div class='option_input_bg'>
		<input id='S_EMP_NO' type='text' class='option_input_fix noborder' style="width:70px"><input id='S_EMP_NAME' type='text' class='option_input_fix' style="width:100px">
	</div>

	<div style='padding: 4px; float:right; color: black; font-weight: bold;'>
		<img align="absMiddle" src="/Theme/images/ico_doc.gif" border="0" hspace="0"></img>
 		<a href='javascript:uf_down_doc("rpt_01000.xml", "근로계약서")'>근로계약서 다운로드</a>
 	</div>

</div>

<div id='grid_1' class='grid_div mr5'>
	<div id="dg_1" class="slick-grid"></div>
</div>

<div id='form_right' class='grid_div'>

	<div id='form_free1' class='detail_box ver2 has_title'>
		<label class='sub_title'>기본정보</label>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">이름</label>
			<div class='detail_input_bg' style="width:130px">
				<input type='text' id='NAME' class='detail_input_fix'>
			</div>

			<label class='detail_label' style="width:100px">사번</label>
			<div class='detail_input_bg' style="width:130px">
				<input type='text' id='EMP_NO' class='detail_input_fix' readonly>
			</div>

			<label class='detail_label' style="width:100px">주민번호</label>
			<div class='detail_input_bg' style="width:124px">
				<input type='text' id='INPUT_RRN_NO' class='detail_input_fix'>
			</div>
	    <div class="detail_input_bg noborder" style="width:66px">
				<button type="button" id='btn_jumin' class="btn btn-success btn-xs" onClick="uf_chk_jumin();" title="이름/생일로 중복 확인을 합니다.">중복확인</button>
	    </div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">핸드폰</label>
			<div class='detail_input_bg' style="width:130px">
				<input type='text' id='HP' class='detail_input_fix'>
			</div>
			<label class='detail_label' style="width:100px">전화번호</label>
			<div class='detail_input_bg' style="width:130px">
				<input type='text' id='PHONE' class='detail_input_fix'>
			</div>
			<label class='detail_label' style="width:100px">이메일</label>
			<div class='detail_input_bg' style="width:190px">
				<input type='text' id='EMAIL' class='detail_input_fix'>
			</div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">직급</label>
			<div class='detail_input_bg' style="width:130px">
				<select id="GUBUN_DIV" class="detail_input_fix"></select>
			</div>
			<label class='detail_label' style="width:100px">담당센터</label>
			<div class='detail_input_bg has_button' style="width:170px">
				<input type='text' id='DEPT_NAME' class='detail_input_fix'>
				<button id="btn_deptfind" class="search_find_img" onClick="_X.FindDeptCode2('DEPT_ID','')"></button>
			</div>
	    <div class='detail_input_bg noborder' style="width:80px;text-align:right">복수근무지사</div>
	    <div class="detail_input_bg noborder" style="width:170px">
	      <input type="text" id="DUP_DEPT_NAME" class='detail_input_fix' readonly>
	    </div>
		</div>

		<div class='detail_row detail_row_bb'>
			<label class='detail_label' style="width:100px">주소</label>
			<div class='detail_input_bg has_button' style="width:100px">
				<input type='text' id='ZIP_CODE' class='detail_input_fix'>
				<button id="btn_zipfind" class="search_find_img" onClick="uf_ZipFind('ZIP_CODE')"></button>
			</div>
			<div class='detail_input_bg noborder' style="width:300px">
				<input type='text' id='ADDR1' class='detail_input_fix'>
			</div>
			<div class='detail_input_bg noborder' style="width:250px">
				<input type='text' id='ADDR2' class='detail_input_fix'>
			</div>
		</div>
	</div>

	<div id='form_free2' class='detail_box ver2 has_title has_button mt5' >
	<label class='sub_title'>업무관련</label>
		<div class='detail_row'>
			<label class='detail_label' style="width:100px">입사일</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='IN_DATE' class='detail_date'>
			</div>
			<label class='detail_label' style="width:100px">퇴사일</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='RETIRE_DATE' class='detail_date'>
			</div>
			<label class='detail_label' style="width:100px">연차일수</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='YEAR_VACATION' class='detail_amt'>
			</div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">배상보험가입일</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='LIINSURANCEDATE' class='detail_date'>
			</div>
			<label class='detail_label' style="width:100px">재직여부</label>
			<div class='detail_input_bg' style="width:150px">
				<select id="WORK_YN" class="detail_input_fix"></select>
			</div>
			<label class='detail_label' style="width:100px">장애유무</label>
			<div class='detail_input_bg' style="width:150px">
				<select id="HANDICAP_YN" class="detail_input_fix"></select>
			</div>
		</div>

		<div class='detail_row detail_row_bb'>
			<label class='detail_label' style="width:100px">장애등급</label>
			<div class='detail_input_bg' style="width:150px">
				<select id="HANDICAP_LEVEL" class="detail_input_fix"></select>
			</div>
			<label class='detail_label' style="width:100px">자격등급</label>
			<div class='detail_input_bg' style="width:150px">
				<select id="LICENSELEVEL_DIV" class="detail_input_fix"></select>
			</div>
			<label class='detail_label' style="width:100px">자격번호</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='LICENSE_NO' class='detail_input_fix'>
			</div>
		</div>
  </div>

	<div id='form_free3' class='detail_box ver2 has_title mt5'>
		<label class='sub_title'>급여관련</label>
		<div class='detail_row'>
			<label class='detail_label' style="width:100px">급여종류</label>
			<div class='detail_input_bg' style="width:150px">
				<select id="PAYTYPE" class="detail_input_fix"></select>
			</div>

			<label class='detail_label' style="width:120px">(세후)최소보장급여</label>
			<div class='detail_input_bg' style="width:130px">
				<input type='text' id='MIN_PAYAMT' class='detail_amt'>
			</div>

			<div class='detail_input_bg noborder' style="width:250px;"></div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">계약시급</label>
			<div class='detail_input_bg' style="width:212px">
				<input type='text' id='HOURPAY' class='detail_amt' style="width:165px">&nbsp;(요양)
			</div>
			<div class='detail_input_bg noborder' style="width:227px">
				<input type='text' id='HOURPAY_2' class='detail_amt' style="width:160px">&nbsp;(가족요양)
			</div>
			<div class='detail_input_bg noborder' style="width:211px">
				<input type='text' id='HOURPAY_3' class='detail_amt' style="width:165px">&nbsp;(목욕)
			</div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">급여계좌</label>
			<div class='detail_input_bg' style="width:150px">
				<select id="BANK_DIV" class="detail_input_fix"></select>
			</div>
			<div class='detail_input_bg noborder' style="width:250px">
				<input type='text' id='DEPOSITNO' class='detail_input_fix'>
			</div>
			<label class='detail_label' style="width:120px">예금주</label>
			<div class='detail_input_bg' style="width:130px">
				<input type='text' id='DEPOSITOWNER' class='detail_input_fix'>
			</div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">건강보험</label>
			<div class='detail_input_bg' style="width:150px">
				<select id="HEALTHINSURETAG" class="detail_input_fix"></select>
			</div>
			<label class='detail_label' style="width:120px">부양가족</label>
			<div class='detail_input_bg' style="width:130px">
				<select id="DEDUCT_CNT" class="detail_input_fix"></select>
			</div>
			<div class='detail_input_bg noborder' style="width:250px;"></div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">국민연금</label>
			<div class='detail_input_bg' style="width:150px">
				<select id="NATIONALPENSIONTAG" class="detail_input_fix"></select>
			</div>
			<label class='detail_label' style="width:120px">국민연금최초금액</label>
			<div class='detail_input_bg' style="width:130px">
				<input type='text' id='FIRST_AMT' class='detail_amt'>
			</div>
			<div class='detail_input_bg noborder' style="width:250px;"></div>
		</div>

		<div class='detail_row detail_row_bb'>
			<label class='detail_label' style="width:100px">고용보험</label>
			<div class='detail_input_bg' style="width:150px">
				<select id="GOYONGTAG" class="detail_input_fix"></select>
			</div>
			<label class='detail_label' style="width:120px">사용기간</label>
			<div class='detail_input_bg' style="width:130px">
				<select id="TRAINEE_YN" class="detail_input_fix"></select>
			</div>
			<!-- 사용기간  적용일 경우 종료일자 활성화 -->
			<label class='detail_label' style="width:120px">종료일자</label>
			<div class='detail_input_bg' style="width:130px">
				<input type='text' id='TRAINEE_DATE' class='detail_date'>
			</div>
		</div>

	</div>

	<div id='form_free4' class='detail_box ver2 has_title mt5'>
		<label class='sub_title'>메모</label>

		<div class='detail_row detail_row_bb'>
	    <div class="detail_input_bg" style="width:750px; height: 90px;">
	    	<textarea id="REMARK" class="detail_input_fix"></textarea>
	    </div>
		</div>

	</div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo			= {init_width:1000, init_height: 35, width_increase:1, height_increase:0};
	grid_1.ResizeInfo					= {init_width: 250, init_height:665, width_increase:1, height_increase:1};
	form_right.ResizeInfo			= {init_width: 750, init_height:665, width_increase:0, height_increase:1};
</script>


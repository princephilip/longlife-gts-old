<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2' >

	<div class='option_label w100'>지사명</div>
	<div class='option_input_bg'>
		<select id="S_DEPT_ID" style="width:260px"></select>
	</div>

	<div class='option_label w100'>고객</div>
	<div class='option_input_bg'>
		<input id='S_CUST_ID' type='text' class='option_input_fix' style="display:none">
		<input id='S_CUST_NAME' type='text' class='option_input_fix' style="width:70px" readonly>
		<!-- <img id="btn_findcust" src="/Theme/images/btn/btn_find.gif" class="search_find_img" onClick="uf_findCust()"/> -->
	</div>

	<div id='xbtns' >
		<span style='vertical-align:middle;'>
			<html:button name='S_CLOSE' 		id='S_CLOSE' 		type='button' icon='ui-icon-circle-close' desc="닫기" onClick="x_Close();"></html:button>
		</span>
	</div>

</div>

<div id='from_1' class='grid_div'>
	고객정보 : <span id="cust_info">L00000000-000, 1등급, 일반 15%</span>
</div>

<div id="tabs_1" class='tabs_div'>
	<div id="ctab_nav" class="tabs_nav">
		<ul>
			<li><a href="#ctab_1">일정 추가하기</a></li>
			<li><a href="#ctab_2">일정 삭제하기</a></li>
		</ul>
	</div>

	<!-- 일정 추가하기 -->
	<div id="ctab_1" class='tabc_div'>
		<div id='form_schedule' class='detail_box ver2'>
			<div class='detail_row'>
				<label class='detail_label' style="width:150px;text-align:center;background-color:#ecf1ff;">서비스시간</label>
				<label class='detail_label' style="width:150px;text-align:center;background-color:#ecf1ff;">제공서비스</label>
				<label class='detail_label' style="width:150px;text-align:center;background-color:#ecf1ff;">작업수가(원)</label>
				<label class='detail_label' style="width:150px;text-align:center;background-color:#ecf1ff;">시간(분)</label>
				<label class='detail_label' style="width:150px;text-align:center;background-color:#ecf1ff;">요양보호사</label>
			</div>
			<div class='detail_row'>
		    <div class='detail_input_bg' style="width:50px;text-align:right"><span>시작 : </span></div>
		    <div class="detail_input_bg noborder" style="width:30px">
		      <input type="text" id="START_TIME_DIV" class='detail_input_fix' maxLength="2">
		    </div>
		    <div class='detail_input_bg noborder' style="width:20px;text-align:right"><span>시</span></div>
		    <div class="detail_input_bg noborder" style="width:30px">
		      <input type="text" id="START_MIN_DIV" class='detail_input_fix' maxLength="2">
		    </div>
		    <div class='detail_input_bg noborder' style="width:20px;text-align:right"><span>분</span></div>

		    <div class="detail_input_bg" style="width:150px">
		    	<select id="PRODUCT_DIV" class='detail_input_fix'></select>
		    </div>

		    <div class='detail_input_bg' style="width:70px;text-align:right">수가 : </div>
		    <div class="detail_input_bg noborder" style="width:80px">
		      <input type="text" id="AMT_1" class='detail_amt' readonly>
		    </div>

		    <div class='detail_input_bg' style="width:70px;text-align:right">주간 : </div>
		    <div class="detail_input_bg noborder" style="width:80px">
		      <input type="text" id="MIN_1" class='detail_amt' readonly>
		    </div>

				<div class='detail_input_bg has_button' style="width:150px">
					<input type='text' id='MEMBER_ID' class='detail_input_fix' style="display:none">
					<input type='text' id='MEMBER_NAME' class='detail_input_fix'>
					<button id="btn_memberfind" class="search_find_img" onclick="uf_findMember('MEMBER_ID')"></button>
				</div>

			</div>
			<div class='detail_row no_line'>
		    <div class='detail_input_bg' style="width:50px;text-align:right"><span>종료 : </span></div>
		    <div class="detail_input_bg noborder" style="width:30px">
		      <input type="text" id="END_TIME_DIV" class='detail_input_fix' maxLength="2">
		    </div>
		    <div class='detail_input_bg noborder' style="width:20px;text-align:right"><span>시</span></div>
		    <div class="detail_input_bg noborder" style="width:30px">
		      <input type="text" id="END_MIN_DIV" class='detail_input_fix' maxLength="2">
		    </div>
		    <div class='detail_input_bg noborder' style="width:20px;text-align:right"><span>분</span></div>

		    <div class="detail_input_bg" style="width:150px">
		    	<select id="PRODUCT_CODE" class='detail_input_fix'></select>
		    </div>

		    <div class='detail_input_bg' style="width:70px;text-align:right">야간 : </div>
		    <div class="detail_input_bg noborder" style="width:80px">
		      <input type="text" id="AMT_2" class='detail_amt' readonly>
		    </div>

		    <div class='detail_input_bg' style="width:70px;text-align:right">야간 : </div>
		    <div class="detail_input_bg noborder" style="width:80px">
		      <input type="text" id="MIN_2" class='detail_amt' readonly>
		    </div>

				<div class='detail_input_bg' style="width:150px"></div>
			</div>

			<div class='detail_row no_line'>
		    <div class='detail_input_bg' style="width:70px;text-align:right">배정시간 : </div>
		    <div class="detail_input_bg noborder" style="width:60px">
		      <input type="text" id="MIN_TOT" class='detail_input_fix'>
		    </div>
		    <div class='detail_input_bg noborder' style="width:20px;text-align:right">분</div>

				<div class='detail_input_bg' style="width:50px;text-align:right"><span>가족 : </span></div>
		    <div class="detail_input_bg noborder" style="width:100px">
		    	<input id='FAMILY_YN' type='checkbox'>
		    </div>

		    <div class='detail_input_bg' style="width:70px;text-align:right">심야 : </div>
		    <div class="detail_input_bg noborder" style="width:80px">
		      <input type="text" id="AMT_3" class='detail_amt' readonly>
		    </div>

		    <div class='detail_input_bg' style="width:70px;text-align:right">심야 : </div>
		    <div class="detail_input_bg noborder" style="width:80px">
		      <input type="text" id="MIN_3" class='detail_amt' readonly>
		    </div>

				<div class='detail_input_bg has_button' style="width:150px">
					<input type='text' id='MEMBER_ID2' class='detail_input_fix' style="display:none">
					<input type='text' id='MEMBER_NAME2' class='detail_input_fix' style="display:none">
					<button id="btn_memberfind2" class="search_find_img" onclick="uf_findMember('MEMBER_ID2')" style="display:none"></button>
				</div>

			</div>

			<div class='detail_row detail_row_bb no_line'>
				<div class='detail_input_bg' style="width:150px">
					<input type='text' id='PAY_AMT' class='detail_amt' style="display:none">
					<input type='text' id='REQ_AMT' class='detail_amt' style="display:none">
					<input type='text' id='SERVICE_MIN' class='detail_amt' style="display:none">
					<input type='text' id='REMAIN_AMT' class='detail_amt' style="display:none">
					<input type='text' id='OFFICE_AMT' class='detail_amt' style="display:none">
					<input type='text' id='PRODUCT_ID' class='detail_input_fix' style="display:none">
					<input type='text' id='PRODUCT_NAME' class='detail_input_fix' style="display:none">
					<input type='text' id='REMARK' class='detail_input_fix' style="display:none">
				</div>

				<!-- if( product_div='7', '금액', '관계') -->
				<div class='detail_input_bg' style="width:50px;text-align:right"><span>관계 : </span></div>
		    <div class="detail_input_bg noborder" style="width:100px">
		    	<select id="FAMILY_REF" class='detail_input_fix'></select>
		    	<input type="text" id="SERVICE_AMT" class='detail_amt' style='display:none' readonly>
		    </div>

		    <div class='detail_input_bg' style="width:70px;text-align:right"><span>합계 : </span></div>
		    <div class="detail_input_bg noborder" style="width:80px">
		      <input type="text" id="AMT_TOT" class='detail_amt' readonly>
		    </div>

		    <div class='detail_input_bg' style="width:70px;text-align:right"><span>수가인정 : </span></div>
		    <div class="detail_input_bg noborder" style="width:80px">
		      <input type="text" id="MIN_REAL" class='detail_amt' readonly>
		    </div>
				<div class='detail_input_bg' style="width:150px"></div>
			</div>
  	</div>

		<div id='form_ins' class='detail_box ver2'>
			<div class='detail_row'>
				<label class='detail_label' style="width:80px">빈도</label>
				<div class='detail_input_bg' style="width:670px">
					<INPUT id=rb_chk_week style="display:none">
					<INPUT onclick=xe_EditChanged(rb_chk_week,this.value) id=rb_chk_week_0 type=radio value='1' name=rb_chk_week CHECKED>
					<A href="javascript:if(!rb_chk_week_0.disabled){rb_chk_week_0.checked=true;xe_EditChanged(rb_chk_week,rb_chk_week_0.value);}" target=_self>매주</A>
					<INPUT onclick=xe_EditChanged(rb_chk_week,this.value) id=rb_chk_week_1 type=radio value='2' name=rb_chk_week>
					<A href="javascript:if(!rb_chk_week_1.disabled){rb_chk_week_1.checked=true;xe_EditChanged(rb_chk_week,rb_chk_week_1.value);}" target=_self>격주(2주마다)</A>
				</div>
			</div>
			<div class='detail_row'>
				<label class='detail_label' style="width:80px">요일</label>
				<div class='detail_input_bg' style="width:670px">
					<input id='CHK_SUN' type='checkbox' checked=true>일&nbsp;&nbsp;
					<input id='CHK_MON' type='checkbox' checked=true>월&nbsp;&nbsp;
					<input id='CHK_TUE' type='checkbox' checked=true>화&nbsp;&nbsp;
					<input id='CHK_WED' type='checkbox' checked=true>수&nbsp;&nbsp;
					<input id='CHK_THU' type='checkbox' checked=true>목&nbsp;&nbsp;
					<input id='CHK_FRI' type='checkbox' checked=true>금&nbsp;&nbsp;
					<input id='CHK_SAT' type='checkbox' checked=true>토&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input id='CHK_WEEKALL' type='checkbox' checked=true>전체선택/해제
				</div>
			</div>
			<div class='detail_row detail_row_bb'>
				<label class='detail_label' style="width:80px">기간</label>
				<div class='detail_input_bg' style="width:670px">
					<html:fromtodate id="dateimages" idHead="INS" classType="option" dateType="date" moveBtn="true" readOnly="false"></html:fromtodate>&nbsp;&nbsp;&nbsp;
					<INPUT id=rb_chk_todate style="display:none">
					<INPUT onclick=xe_EditChanged(rb_chk_todate,this.value) id=rb_chk_todate_0 type=radio value='T' name=rb_chk_todate>
					<A href="javascript:if(!rb_chk_todate_0.disabled){rb_chk_todate_0.checked=true;xe_EditChanged(rb_chk_todate,rb_chk_todate_0.value);}" target=_self>이번달까지</A>
					<INPUT onclick=xe_EditChanged(rb_chk_todate,this.value) id=rb_chk_todate_1 type=radio value='C' name=rb_chk_todate CHECKED>
					<A href="javascript:if(!rb_chk_todate_1.disabled){rb_chk_todate_1.checked=true;xe_EditChanged(rb_chk_todate,rb_chk_todate_1.value);}" target=_self>계약종료까지</A>
				</div>
			</div>
  	</div>

		<div id='remk_ins' class='grid_div'>
			<span style="margin-left:80px">(마감일자 이후로 일정을 추가할 수 있습니다.)</span>
		</div>

		<div id="summary_ins" class='grid_div' style="background-color: #f1f1f1; border:1px solid #ddd; padding: 10px 20px 10px 20px;">
		</div>
		<div id="btn_ins" class='grid_div' style="padding-top: 10px; text-align: center;">
			<button type="button" id='btn_schedule_ins' class="btn btn-success btn-xs" onClick="uf_append_schedule();">추가하기</button>
		</div>
	</div>

	<!-- 일정 삭제하기 -->
	<div id="ctab_2" class='tabc_div'>
		<br>
		<div id='form_del' class='detail_box ver2'>
			<div class='detail_row'>
				<label class='detail_label' style="width:80px">요일</label>
				<div class='detail_input_bg' style="width:670px">
					<input id='CHK_DSUN' type='checkbox' checked=true>일&nbsp;&nbsp;
					<input id='CHK_DMON' type='checkbox' checked=true>월&nbsp;&nbsp;
					<input id='CHK_DTUE' type='checkbox' checked=true>화&nbsp;&nbsp;
					<input id='CHK_DWED' type='checkbox' checked=true>수&nbsp;&nbsp;
					<input id='CHK_DTHU' type='checkbox' checked=true>목&nbsp;&nbsp;
					<input id='CHK_DFRI' type='checkbox' checked=true>금&nbsp;&nbsp;
					<input id='CHK_DSAT' type='checkbox' checked=true>토&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input id='CHK_DWEEKALL' type='checkbox' checked=true>전체선택/해제
				</div>
			</div>
			<div class='detail_row detail_row_bb'>
				<label class='detail_label' style="width:80px">기간</label>
				<div class='detail_input_bg' style="width:670px">
					<html:fromtodate id="dateimages" idHead="DEL" classType="option" dateType="date" moveBtn="true" readOnly="false"></html:fromtodate>&nbsp;&nbsp;&nbsp;
					<INPUT id=rb_chk_dtodate style="display:none">
					<INPUT onclick=xe_EditChanged(rb_chk_dtodate,this.value) id=rb_chk_dtodate_0 type=radio value='T' name=rb_chk_dtodate>
					<A href="javascript:if(!rb_chk_dtodate_0.disabled){rb_chk_dtodate_0.checked=true;xe_EditChanged(rb_chk_dtodate,rb_chk_dtodate_0.value);}" target=_self>이번달까지</A>
					<INPUT onclick=xe_EditChanged(rb_chk_dtodate,this.value) id=rb_chk_dtodate_1 type=radio value='C' name=rb_chk_dtodate CHECKED>
					<A href="javascript:if(!rb_chk_dtodate_1.disabled){rb_chk_dtodate_1.checked=true;xe_EditChanged(rb_chk_dtodate,rb_chk_dtodate_1.value);}" target=_self>계약종료까지</A>
				</div>
			</div>
  	</div>

		<div id='remk_del' class='grid_div'>
			<span style="margin-left:80px">(마감일자 이후로 일정을 삭제할 수 있습니다.)</span>
		</div>

		<div id="summary_del" class='grid_div' style="background-color: #f1f1f1; border:1px solid #ddd; padding: 10px 20px 10px 20px;">
		</div>
		<div id="btn_del" class='grid_div' style="padding-top: 10px; text-align: center;">
			<button type="button" id='btn_schedule_del' class="btn btn-success btn-xs" onClick="uf_delete_schedule();">삭제하기</button>
		</div>
	</div>
</div>

<div id='grid_1' class='grid_div' style= "visibility:hidden">
	<div id="dg_1" class='slick-grid'></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo		= {init_width:1000, init_height: 35, width_increase:1, height_increase:0};
	from_1.ResizeInfo				= {init_width:1000, init_height: 30, width_increase:1, height_increase:1};
	tabs_1.ResizeInfo 	 		= {init_width:1000, init_height:645, width_increase:1, height_increase:1};
		ctab_nav.ResizeInfo 	= {init_width:1000, init_height: 25, width_increase:1, height_increase:0};
		ctab_1.ResizeInfo 	 	= {init_width:1000, init_height:620, width_increase:1, height_increase:1};
			form_schedule.ResizeInfo 	= {init_width:750, init_height:140, width_increase:0, height_increase:0};
			form_ins.ResizeInfo 	 		= {init_width:750, init_height: 90, width_increase:0, height_increase:0};
			remk_ins.ResizeInfo 	 		= {init_width:750, init_height: 30, width_increase:0, height_increase:0};
			summary_ins.ResizeInfo 	 	= {init_width:750, init_height: 60, width_increase:0, height_increase:0};
			btn_ins.ResizeInfo 	 			= {init_width:750, init_height: 30, width_increase:0, height_increase:0};

		ctab_2.ResizeInfo 	 	= {init_width:1000, init_height:620, width_increase:1, height_increase:1};
			form_del.ResizeInfo 	 		= {init_width:750, init_height: 63, width_increase:0, height_increase:0};
			remk_del.ResizeInfo 	 		= {init_width:750, init_height: 30, width_increase:0, height_increase:0};
			summary_del.ResizeInfo 	 	= {init_width:750, init_height: 60, width_increase:0, height_increase:0};
			btn_del.ResizeInfo 	 			= {init_width:750, init_height: 30, width_increase:0, height_increase:0};
	grid_1.ResizeInfo				= {init_width:1000, init_height: 10, width_increase:1, height_increase:0};

</script>


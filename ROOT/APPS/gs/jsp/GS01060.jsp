<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<jsp:include page="/Mighty/jsp/UploadExcelForm.jsp"/>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_2' gridCall="true" pgmcode="GS01060"></html:authbutton>
		</span>
	</div>

	<div class='option_label w100'>지사명</div>
	<div class='option_input_bg'>
		<select id="S_DEPT_ID" style="width:260px"></select>
		<img id="btn_finddept" src="/Theme/images/btn/btn_find.gif" class="search_find_img" onClick="uf_findDept()"/>
	</div>

	<div class='option_label'>기준년월</div>
	<div class='option_input_bg'>
		<html:basicdate id="dateimages" classType="search" dateType="month" moveBtn="true" readOnly="true"></html:basicdate>
	</div>

	<div class='option_label w100'>고객</div>
	<div class='option_input_bg'>
		<input id='S_CUST_NAME' type='text' class='option_input_fix' style="width:120px">
	</div>

	<div class='option_input_bg D_RANGE' style='float: right;'>
		<input id='S_RANGE' type="range" class="S_RANGE_BAR" min="400" max="900" step="10" name="range" value="900" style='width: 100px;'>
	</div>
	<div class='option_label D_RANGE' style='float: right;'>화면비율</div>

</div>

<div id='grid_1' class='grid_div mr5 has_title'>
	<label class='sub_title'>수급자청구현황</label>
	<div id="dg_1" class="slick-grid"></div>
</div>

<div id='grid_2' class='grid_div has_title'>
	<label class='sub_title'>서비스내역</label>
	<div id="dg_2" class="slick-grid"></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo			= {init_width:1000, init_height: 35, width_increase:1, height_increase:0};
	grid_1.ResizeInfo					= {init_width: 900, init_height:660, width_increase:0, height_increase:1};
	grid_2.ResizeInfo					= {init_width: 100, init_height:660, width_increase:1, height_increase:1};
</script>


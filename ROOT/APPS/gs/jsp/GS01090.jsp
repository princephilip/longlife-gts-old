<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<%
	String params = request.getParameter("params");
%>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true" pgmcode="GS01090"></html:authbutton>
		</span>
	</div>

	<div class='option_label w100'>지사명</div>
	<div class='option_input_bg'>
		<select id="S_DEPT_ID" style="width:260px"></select>
		<img id="btn_finddept" src="/Theme/images/btn/btn_find.gif" class="search_find_img" onClick="uf_findDept()"/>
	</div>

	<div class='option_label'>기준일자</div>
	<div class='option_input_bg'>
		<html:basicdate id="dateimages" classType="search" dateType="date" moveBtn="true" readOnly="false"></html:basicdate>
	</div>

</div>

<div id='from_1' class='grid_div'>
	<IFRAME id="form_week" src="/APPS/com/jsp/calendar_week.jsp" width="100%" height="100%" marginwidth="0" marginheight="0" frameborder="0" scrolling="yes"></IFRAME>
</div>

<div id='grid_1' class='grid_div mt5'>
	<div id="dg_1" class='slick-grid'></div>
</div>

<script type="text/javascript">
	var _params = "<%=params%>";

	topsearch.ResizeInfo			= {init_width:1000, init_height: 33, width_increase:1, height_increase:0};
	from_1.ResizeInfo					= {init_width:1000, init_height:435, width_increase:1, height_increase:0.65};
	grid_1.ResizeInfo					= {init_width:1000, init_height:235, width_increase:1, height_increase:0.35};
</script>


<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<jsp:include page="/Mighty/jsp/UploadExcelForm.jsp"/>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true" pgmcode='GS02041'></html:authbutton>
		</span>
	</div>

	<div class='option_label'>조회일자</div>
	<div class="option_input_bg" >
		<input id='S_PLAN_DATE' type='text' class='option_input_fix' style="text-align:center" readonly/>
	</div>

</div>

<div id='grid_1' class='grid_div'>
	<div id="dg_1" class='slick-grid'></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo	= {init_width:1000, init_height: 35, width_increase:1, height_increase:0};
	grid_1.ResizeInfo			= {init_width:1000, init_height:660, width_increase:1, height_increase:1};

</script>


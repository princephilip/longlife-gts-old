<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<%
	String params = request.getParameter("params");
%>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_2' gridCall="true" pgmcode='GS01020'></html:authbutton>
		</span>
	</div>

	<div class='option_label w100'>지사명</div>
	<div class='option_input_bg'>
		<select id="S_DEPT_ID" style="width:260px"></select>
		<img id="btn_finddept" src="/Theme/images/btn/btn_find.gif" class="search_find_img" onClick="uf_findDept()"/>
	</div>

	<div class='option_label'>기준일자</div>
	<div class="option_input_bg" >
		<html:fromtodate id="dateimages"  classType="option" dateType="date" moveBtn="true" readOnly="false"></html:fromtodate>
	</div>

	<div class="option_label">
		<A href="javascript:if(!CHK_ALLDATE.disabled){CHK_ALLDATE.checked=!CHK_ALLDATE.checked;xe_EditChanged(CHK_ALLDATE,CHK_ALLDATE.value)}" target=_self><INPUT onclick=xe_EditChanged(this,this.value) id=CHK_ALLDATE type=checkbox value="" name=CHK_ALLDATE>전체기간</A>
	</div>

	<!-- 두번째줄 -->
	<div class="option_line"></div>

	<div class='option_label w100'>고객</div>
	<div class='option_input_bg'>
		<input id='S_CUST_NAME' type='text' class='option_input_fix'>
	</div>

	<div id='userxbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<button type="button" id='btn_change'      class="btn btn-success btn-xs" onClick="uf_append_contract();">계약변경</button>
			<button type="button" id='btn_cust_modify' class="btn btn-success btn-xs" onClick="jf_cust_modify();">대상자 정보수정</button>
		</span>
	</div>
</div>

<div id='grid_1' class='grid_div mr5 has_title'>
	<label class='sub_title'>대상자 목록</label>
	<div id="dg_1" class="slick-grid"></div>
</div>

<div id='grid_2' class='grid_div mr5 has_title'>
	<label class='sub_title'>계약 히스토리</label>
	<div id="dg_2" class="slick-grid"></div>
</div>

<div id='form_right' class='grid_div'>

	<div id='form_free1' class='detail_box ver2 has_title'>
		<label class='sub_title'>기본 정보</label>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">이름</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='CUST_NAME' class='detail_input_fix' readonly>
			</div>

			<label class='detail_label' style="width:100px">고객번호</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='CUST_ID' class='detail_input_fix' readonly>
			</div>

			<label class='detail_label' style="width:100px">생년월일</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='BIRTH_DATE' class='detail_date' readonly>
			</div>
		</div>

		<div class='detail_row detail_row_bb'>
			<label class='detail_label' style="width:100px">핸드폰</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='HP' class='detail_input_fix' readonly>
			</div>
			<label class='detail_label' style="width:100px">전화번호</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='PHONE' class='detail_input_fix' readonly>
			</div>
			<div class='detail_input_bg noborder' style="width:250px">
			</div>
		</div>
	</div>

	<div id='form_free2' class='detail_box ver2 has_title has_button' >
		<label class='sub_title'>계약 정보</label>
		<div class='detail_row'>
			<label class='detail_label' style="width:100px">*요양인정번호</label>
	    <div class="detail_input_bg" style="width:100px">
	      <input type="text" id="MANAGE_NO1" class='detail_input_fix'>
	    </div>
	    <div class="detail_input_bg noborder" style="width:50px">
	      <input type="text" id="MANAGE_NO2" class='detail_input_fix'>
	    </div>
			<div class='detail_input_bg noborder' style="width:120px;"></div>

	    <label class='detail_label' style="width:100px">*인정유효기간</label>
			<div class='detail_input_bg' style="width:90px">
				<input type='text' id='CARE_SDATE' class='detail_date'>
			</div>
			<div class='detail_input_bg noborder' style="width:20px;text-align:center">~</div>
			<div class='detail_input_bg noborder' style="width:90px">
				<input type='text' id='CARE_EDATE' class='detail_date'>
			</div>
			<div class='detail_input_bg noborder' style="width:80px;"></div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">*장기요양등급</label>
	    <div class="detail_input_bg" style="width:110px">
	    	<select id="CUST_LEVEL_DIV" class='detail_input_fix'></select>
	    </div>
	    <div class='detail_input_bg noborder' style="width:60px;text-align:right">월한도액</div>
	    <div class="detail_input_bg noborder" style="width:100px">
	      <input type="text" id="M_LIMIT_AMT" class='detail_amt' readonly>
	    </div>
			<label class='detail_label' style="width:100px">*경감구분</label>
	    <div class="detail_input_bg" style="width:170px">
	      <select id="REDUCTION_DIV" class='detail_input_fix'></select>
	    </div>
	    <div class="detail_input_bg noborder" style="width:80px">
	      <select id="APP_RATE_DIV" class='detail_input_fix'></select>
	    </div>
	    <div class='detail_input_bg noborder' style="width:30px;">%</div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">기관월한도액</label>
	    <div class="detail_input_bg" style="width:270px">
	      <input type="text" id="OFFICE_AMT" class='detail_amt'>
	    </div>

			<label class='detail_label' style="width:100px">본인부담금한도</label>
	    <div class="detail_input_bg" style="width:280px">
	    	<input type="text" id="REQ_LIMIT_AMT" class='detail_amt'>
	    </div>

		</div>

		<div class='detail_row detail_row_bb'>
	    <label class='detail_label' style="width:100px">*계약기간</label>
			<div class='detail_input_bg' style="width:90px">
				<input type='text' id='CONT_SDATE' class='detail_date'>
			</div>
			<div class='detail_input_bg noborder' style="width:20px;text-align:center">~</div>
			<div class='detail_input_bg noborder' style="width:90px">
				<input type='text' id='CONT_EDATE' class='detail_date'>
			</div>
			<div class='detail_input_bg noborder' style="width:450px;"></div>

		</div>

  </div>

	<div id='form_free3' class='grid_div has_title has_button'>
		<label class='sub_title'>보호자 정보</label>
	  <div class='child_button float_right' style="margin-right:3px;">
      <button type="button" class="btn btn-success btn-xs" id="btn_protector" onclick="uf_protector()" title="보호자 정보를 변경 합니다.">보호자 목록</button>
	  </div>

		<div id='form_free3d' class='detail_box ver2'>

			<div class='detail_row'>
				<label class='detail_label' style="width:100px">이름</label>
				<div class='detail_input_bg' style="width:150px">
					<input type='text' id='PRO_NAME' class='detail_input_fix' readonly>
				</div>

				<label class='detail_label' style="width:100px">관계</label>
		    <div class="detail_input_bg" style="width:150px">
		      <input type="text" id="RELATION_DIV_NAME" class='detail_input_fix' readOnly>
		    </div>

		    <div class="detail_input_bg noborder" style="width:250px"></div>
			</div>

			<div class='detail_row detail_row_bb'>
				<label class='detail_label' style="width:100px">연락처</label>
				<div class='detail_input_bg' style="width:150px">
					<input type='text' id='PRO_PHONE' class='detail_input_fix' readonly>
				</div>

				<label class='detail_label' style="width:100px">핸드폰</label>
				<div class='detail_input_bg' style="width:150px">
					<input type='text' id='PRO_HP' class='detail_input_fix' readonly>
				</div>

		    <div class="detail_input_bg noborder" style="width:250px"></div>
			</div>
	  </div>
	</div>

	<div id='form_free4' class='detail_box ver2 has_title'>
		<label class='sub_title'>계약 해지</label>
		<div class='detail_row'>
	    <label class='detail_label' style="width:100px">해지일자</label>
	    <div class="detail_input_bg" style="width:150px">
	      <input type="text" id="CANCEL_DATE" class="detail_date">
	    </div>

	    <div class="detail_input_bg noborder" style="width:500px"></div>
		</div>

		<div class='detail_row detail_row_bb'>
			<label class='detail_label' style="width:100px">해지사유</label>
	    <div class="detail_input_bg" style="width:650px">
	    	<textarea id="CANCEL_REASON" style="height:57px;"></textarea>
	    </div>
	    <!-- <div class="detail_input_bg noborder" style="width:250px"></div> -->
		</div>
	</div>

</div>

<div id="tabs_1" class='tabs_div'>
	<div id="ctab_nav" class="tabs_nav">
		<ul>
			<li><a href="#ctab_1">서류/서식</a></li>
			<li><a href="#ctab_2">서비스 일정</a></li>
			<li><a href="#ctab_3">이용계약서</a></li>
		</ul>
	</div>
	<button type="button" class="btn btn-success btn-xs" onClick="uf_month_change();" style="float:right; margin-top: -30px;">월별일정변경</button>

	<div id="ctab_1" class='tabc_div'>
		<div id='form_doc' class='grid_div'>
		</div>
	</div>

	<div id="ctab_2" class='tabc_div'>
		<div id='grid_3' class='grid_div'>
			<div id="dg_3" class='slick-grid'></div>
		</div>
	</div>

	<div id="ctab_3" class='tabc_div'>
		<!-- 계약내용 -->
		<div id='form_free5' class='detail_box ver2 has_title has_button'>
			<label class='sub_title'>계약내용</label>
			<button type="button" class="btn btn-warning btn-xs" onClick="uf_print_cont();" style="float:right; margin-top: -27px;">계약서인쇄</button>

			<div class='detail_row'>
		    <label class='detail_label' style="width:100px">계약종류</label>
		    <div class="detail_input_bg" style="width:150px">
		    	<select id="CONT_DEPT_DIV" class='detail_input_fix'></select>
		    </div>

				<label class='detail_label' style="width:100px">통보서작성일자</label>
				<div class='detail_input_bg' style="width:40px;text-align:right">당월</div>
		    <div class="detail_input_bg noborder" style="width:70px">
		    	<select id="CONT_WRITE_DAY" class='detail_input_fix'></select>
		    </div>
		    <div class='detail_input_bgnoborder' style="width:40px;"></div>

				<div class='detail_input_bgnoborder' style="width:250px;"></div>
			</div>


			<div class='detail_row'>
				<label class='detail_label' style="width:100px">비용정산일자</label>
				<div class='detail_input_bg' style="width:40px;text-align:right">매월</div>
		    <div class="detail_input_bg noborder" style="width:70px">
		    	<select id="CONT_CALC_DAY" class='detail_input_fix'></select>
		    </div>
				<div class='detail_input_bgnoborder' style="width:40px;">까지</div>

		    <label class='detail_label' style="width:100px">비용통보일자</label>
				<div class='detail_input_bg' style="width:40px;text-align:right">매월</div>
		    <div class="detail_input_bg noborder" style="width:70px">
		    	<select id="CONT_NOTI_DAY" class='detail_input_fix'></select>
		    </div>
				<div class='detail_input_bgnoborder' style="width:40px;">까지</div>

		    <label class='detail_label' style="width:100px">비용납부일자</label>
				<div class='detail_input_bg' style="width:40px;text-align:right">매월</div>
		    <div class="detail_input_bg noborder" style="width:70px">
		    	<select id="CONT_PAY_DAY" class='detail_input_fix'></select>
		    </div>
				<div class='detail_input_bgnoborder' style="width:40px;">까지</div>
			</div>

			<div class='detail_row'>
				<label class='detail_label' style="width:100px">비용납부방법</label>
		    <div class="detail_input_bg" style="width:150px">
		    	<select id="CONT_PAY_METHOD" class='detail_input_fix'></select>
		    </div>
		    <label class='detail_label' style="width:100px">계약일자</label>
		    <div class="detail_input_bg" style="width:150px">
		    	 <input type="text" id="CONT_DATE" class="detail_date">
		    </div>
				<div class='detail_input_bgnoborder' style="width:250px;"></div>
			</div>

			<div class='detail_row detail_row_bb'>
		    <label class='detail_label' style="width:100px">작성자</label>
		    <div class="detail_input_bg" style="width:150px">
		    	 <input type="text" id="CONT_MEMBER_NAME" class="detail_input_fix">
		    </div>
		    <label class='detail_label' style="width:100px">작성일자</label>
		    <div class="detail_input_bg" style="width:150px">
		    	<input type="text" id="CONT_WRITE_DATE" class="detail_date">
		    </div>
				<div class='detail_input_bgnoborder' style="width:250px;"></div>
			</div>

		</div>

		<div id='grid_4' class='grid_div has_title has_button'>
			<label class='sub_title'>급여계획	</label>
			<div class='child_button float_right'>
				<html:authchildbutton id='buttons' grid='dg_4' append='true' insert='false' duplicate='false' delete='true' excel='false' retrieve='false' ></html:authchildbutton>
			</div>
			
			<div id="dg_4" class="slick-grid"></div>
		</div>

	</div>

</div>



<script type="text/javascript">
	var _params = "<%=params%>";

	topsearch.ResizeInfo			= {init_width:1000, init_height: 58, width_increase:1,   height_increase:0};
	grid_1.ResizeInfo					= {init_width:  70, init_height:635, width_increase:0.35, height_increase:1};
	grid_2.ResizeInfo					= {init_width: 180, init_height:635, width_increase:0.65, height_increase:1};
	form_right.ResizeInfo			= {init_width: 750, init_height:460, width_increase:0, 	 height_increase:0};
		form_free1.ResizeInfo		= {init_width: 750, init_height: 90, width_increase:0, height_increase:0};
		form_free2.ResizeInfo		= {init_width: 750, init_height:145, width_increase:0, height_increase:0};
		form_free3.ResizeInfo		= {init_width: 750, init_height: 90, width_increase:0, height_increase:0};
		form_free3d.ResizeInfo	= {init_width: 750, init_height: 65, width_increase:0, height_increase:0};
		form_free4.ResizeInfo		= {init_width: 750, init_height:150, width_increase:0, height_increase:0};


	tabs_1.ResizeInfo 				= {init_width: 750, init_height:205, anchor:{x1:1,y1:1,x2:0,y2:1}};
		form_doc.ResizeInfo 		= {init_width: 750, init_height:155, anchor:{x1:1,y1:1,x2:0,y2:1}};
		grid_3.ResizeInfo 			= {init_width: 750, init_height:155, anchor:{x1:1,y1:1,x2:0,y2:1}};

		ctab_3.ResizeInfo 			= {init_width: 750, init_height:155, anchor:{x1:1,y1:1,x2:0,y2:1}};
			form_free5.ResizeInfo = {init_width: 750, init_height:140, anchor:{x1:1,y1:1,x2:0,y2:0}};
			grid_4.ResizeInfo 		= {init_width: 750, init_height: 10, anchor:{x1:1,y1:1,x2:0,y2:1}};

</script>


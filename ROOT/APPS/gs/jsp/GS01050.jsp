<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<%
	String params = request.getParameter("params");
%>

<style>

#form_schedule.detail_box.ver2 div.detail_row {
  background-color:#ffffff;
}

#form_schedule.detail_box.ver2 div.detail_row span {
  color:#000000;
}

#form_schedule.detail_box.ver2 div.detail_row.no_line {
  border-top: 0px;
}

</style>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true" pgmcode="GS01050"></html:authbutton>
		</span>
	</div>

	<div class='option_label w100'>지사명</div>
	<div class='option_input_bg'>
		<select id="S_DEPT_ID" style="width:260px"></select>
		<img id="btn_finddept" src="/Theme/images/btn/btn_find.gif" class="search_find_img" onClick="uf_findDept()"/>
	</div>

	<div class='option_label w100'>고객</div>
	<div class='option_input_bg'>
		<input id='S_CUST_ID' type='text' class='option_input_fix' style="display:none">
		<input id='S_CUST_NAME' type='text' class='option_input_fix' style="width:70px" readonly>
		<img id="btn_findcust" src="/Theme/images/btn/btn_find.gif" class="search_find_img" onClick="uf_findCust()"/>
	</div>

	<div class='option_input_bg' style="border-width: 0px 0px 0px 0px;">
		<button type="button" id='btn_add_calendar' class="btn btn-success btn-xs" onClick="uf_show_modify();">달력에 추가/삭제</button>
		<!-- <button type="button" id='btn_add_schedule' class="btn btn-success btn-xs" onClick="uf_schedule_batch();">반복일정 추가/삭제</button> -->
	</div>

	<div class='option_input_bg' style="float:right;background-color: #f8f8f8;border-width: 0px 0px 0px 0px;">
		<div class="option_input_bg" style="padding:3px 5px 0px 5px;background-color: #f8f8f8;border-width: 0px 0px 0px 0px;">
			<span style="color:black;">인쇄구분 :</span>
		</div>
		<INPUT onclick=xe_EditChanged(rb_print,this.value) id=rb_print_0 type=radio value='H' name=rb_print CHECKED>
		<A href="javascript:if(!rb_print_0.disabled){rb_print_0.checked=true;xe_EditChanged(rb_print,rb_print_0.value);}" target=_self>가로</A>
		<INPUT onclick=xe_EditChanged(rb_print,this.value) id=rb_print_1 type=radio value='V' name=rb_print>
		<A href="javascript:if(!rb_print_1.disabled){rb_print_1.checked=true;xe_EditChanged(rb_print,rb_print_1.value);}" target=_self>세로</A>
	</div>

	<div id='userxbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<button type="button" id='btn_recalc' class="btn btn-success btn-xs" onClick="uf_recalc();">재계산</button>
		</span>
	</div>

</div>

<div id='from_1' class='grid_div'>
	<table width="100%" height=100%>
		<tr id=lfdw_cond_tr height="135px" style="display:none">
			<td valign="top" align="center">
				<table cellpadding="0" cellcpacing="0" border="0" style="border:2px solid #ccc;background-color: #f6f6f6;">
					<tr>
						<td height="113px" style="padding: 10px;" align="center">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td width="750px" height="113px">

										<div id='form_schedule' class='detail_box ver2'>
											<div class='detail_row'>
												<label class='detail_label' style="width:150px;text-align:center;background-color:#ecf1ff;">서비스시간</label>
												<label class='detail_label' style="width:340px;text-align:center;background-color:#ecf1ff;">제공서비스</label>
												<label class='detail_label' style="width:130px;text-align:center;background-color:#ecf1ff;">적용수가(원)</label>
												<label class='detail_label' style="width:130px;text-align:center;background-color:#ecf1ff;">시간(분)</label>
											</div>
											<div class='detail_row'>
										    <div class='detail_input_bg' style="width:50px;text-align:right"><span>시작 : </span></div>
										    <div class="detail_input_bg noborder" style="width:30px">
										      <input type="text" id="START_TIME_DIV" class='detail_input_fix' maxLength="2" tabindex=1>
										    </div>
										    <div class='detail_input_bg noborder' style="width:20px;text-align:right"><span>시</span></div>
										    <div class="detail_input_bg noborder" style="width:30px">
										      <input type="text" id="START_MIN_DIV" class='detail_input_fix' maxLength="2" tabindex=2>
										    </div>
										    <div class='detail_input_bg noborder' style="width:20px;text-align:right"><span>분</span></div>

										    <div class="detail_input_bg" style="width:25px">
										    	<input id='SVC_YN' type='checkbox'>
										    </div>
												<div class='detail_input_bg noborder' style="width:35px"><span>주야:</span></div>
										    <div class="detail_input_bg noborder" style="width:120px">
										    	<select id="PRODUCT_DIV" class='detail_input_fix'></select>
										    </div>
												<div class='detail_input_bg noborder' style="width:45px;text-align:right">요보사</div>
												<div class='detail_input_bg noborder has_button' style="width:115px">
													<input type='text' id='MEMBER_ID' class='detail_input_fix' style="display:none">
													<input type='text' id='MEMBER_NAME' class='detail_input_fix' readonly>
													<button id="btn_memberfind" class="search_find_img" onclick="uf_findMember('MEMBER_ID')"></button>
												</div>

										    <div class='detail_input_bg' style="width:60px;text-align:right">주간 : </div>
										    <div class="detail_input_bg noborder" style="width:70px">
										      <input type="text" id="AMT_1" class='detail_amt' readonly>
										    </div>

										    <div class='detail_input_bg' style="width:60px;text-align:right">주간 : </div>
										    <div class="detail_input_bg noborder" style="width:70px">
										      <input type="text" id="MIN_1" class='detail_amt' readonly>
										    </div>
											</div>

											<div class='detail_row no_line'>
										    <div class='detail_input_bg' style="width:50px;text-align:right"><span>종료 : </span></div>
										    <div class="detail_input_bg noborder" style="width:30px">
										      <input type="text" id="END_TIME_DIV" class='detail_input_fix' maxLength="2" tabindex=3>
										    </div>
										    <div class='detail_input_bg noborder' style="width:20px;text-align:right"><span>시</span></div>
										    <div class="detail_input_bg noborder" style="width:30px">
										      <input type="text" id="END_MIN_DIV" class='detail_input_fix' maxLength="2" tabindex=4>
										    </div>
										    <div class='detail_input_bg noborder' style="width:20px;text-align:right"><span>분</span></div>

										    <div class="detail_input_bg" style="width:60px"></div>
										    <div id="dl_product" class="detail_input_bg noborder" style="width:280px">
										    	<select id="PRODUCT_CODE" class='detail_input_fix'></select>
										    </div>
										    <div id="dl_etcamt1" class='detail_input_bg noborder' style="width:35px;display:none"><span>금액</span></div>
										    <div id="dl_etcamt2" class="detail_input_bg noborder" style="width:140px;display:none">
										      <input type="text" id="AMT_ETC" class='detail_amt'>
										    </div>
										    <div id="dl_etcamt3" class='detail_input_bg noborder' style="width:105px;display:none"><span>원</span></div>

										    <div class='detail_input_bg' style="width:60px;text-align:right">야간,토 : </div>
										    <div class="detail_input_bg noborder" style="width:70px">
										      <input type="text" id="AMT_2" class='detail_amt' readonly>
										    </div>

										    <div class='detail_input_bg' style="width:60px;text-align:right">야간,토 : </div>
										    <div class="detail_input_bg noborder" style="width:70px">
										      <input type="text" id="MIN_2" class='detail_amt' readonly>
										    </div>
											</div>

											<div class='detail_row no_line'>
										    <div class='detail_input_bg' style="width:70px;text-align:right">배정시간 : </div>
										    <div class="detail_input_bg noborder" style="width:50px">
										      <input type="text" id="MIN_TOT" class='detail_input_fix' readonly>
										    </div>
										    <div class='detail_input_bg noborder' style="width:30px;text-align:right">시간</div>

										    <div class="detail_input_bg" style="width:25px;border-width: 1px 0px 0px 1px;">
										    	<input id='MOVE_YN' type='checkbox'>
										    </div>
												<div class='detail_input_bg noborder' style="width:35px;border-width: 1px 0px 0px 1px;"><span>이동:</span></div>
										    <div class="detail_input_bg noborder" style="width:180px;border-width: 1px 0px 0px 1px;">
										    	<select id="PRODUCT_CODE_MOVE" class='detail_input_fix'></select>
										    </div>
										    <div class="detail_input_bg noborder" style="width:100px;border-width: 1px 0px 0px 1px;"></div>
										    <!-- <div class="detail_input_bg noborder" style="width:25">
										    	<input id='ONEWAY_YN' type='checkbox'>
										    </div>
												<div class='detail_input_bg noborder' style="width:35;"><span>편도:</span></div> -->

										    <div class='detail_input_bg' style="width:60px;text-align:right">공휴일 : </div>
										    <div class="detail_input_bg noborder" style="width:70px">
										      <input type="text" id="AMT_3" class='detail_amt' readonly>
										    </div>

										    <div class='detail_input_bg' style="width:60px;text-align:right">공휴일 : </div>
										    <div class="detail_input_bg noborder" style="width:70px">
										      <input type="text" id="MIN_3" class='detail_amt' readonly>
										    </div>

											</div>

											<div class='detail_row no_line'>
												<div class='detail_input_bg' style="width:150px"></div>

										    <div class="detail_input_bg" style="width:25px">
										    	<input id='MEAL_YN' type='checkbox'>
										    </div>
												<div class='detail_input_bg noborder' style="width:35px;"><span>식대:</span></div>
										    <div class="detail_input_bg noborder" style="width:120px">
										    	<input type="text" id="AMT_MEAL" class='detail_amt'>
										    </div>
												<div class='detail_input_bg noborder' style="width:30px;">원</div>
												<div class='detail_input_bg noborder' style="width:130px"></div>

										    <div class='detail_input_bg' style="width:60px;text-align:right"><span>합계 : </span></div>
										    <div class="detail_input_bg noborder" style="width:70px">
										      <input type="text" id="AMT_TOT" class='detail_amt' readonly>
										    </div>

										    <!-- <div class='detail_input_bg' style="width:70px;text-align:right"><span>수가인정 : </span></div> -->
												<div class='detail_input_bg' style="width:130px">
										      <input type="text" id="MIN_REAL" class='detail_amt' style="display:none" readonly>
												</div>
											</div>

											<div class='detail_row no_line'>
										    <div class="detail_input_bg" style="width:25px">
										    	<input id='SERVICE_YN' type='checkbox'>
										    </div>
												<div class='detail_input_bg noborder' style="width:125px;"><span>미이용수가</span></div>

										    <div class="detail_input_bg" style="width:25px">
										    	<input id='BATH_YN' type='checkbox'>
										    </div>
												<div class='detail_input_bg noborder' style="width:35px;"><span>목욕:</span></div>
										    <div class="detail_input_bg noborder" style="width:120px">
										    	<select id="PRODUCT_CODE_BATH" class='detail_input_fix'></select>
										    </div>
												<div class='detail_input_bg noborder' style="width:45px;text-align:right">요보사</div>
												<div class='detail_input_bg noborder has_button' style="width:115px">
													<input type='text' id='MEMBER_ID2' class='detail_input_fix' style="display:none">
													<input type='text' id='MEMBER_NAME2' class='detail_input_fix' readonly>
													<button id="btn_memberfind2" class="search_find_img" onclick="uf_findMember('MEMBER_ID2')"></button>
												</div>

										    <div class='detail_input_bg' style="width:60px;text-align:right;border-width: 1px 0px 0px 1px;">이동 : </div>
										    <div class="detail_input_bg noborder" style="width:70px;border-width: 1px 0px 0px 1px;">
										      <input type="text" id="AMT_MOVE" class='detail_amt' readonly>
										    </div>

												<div class='detail_input_bg' style="width:130px"></div>
											</div>

											<div class='detail_row detail_row_bb no_line'>
												<div class='detail_input_bg' style="width:150px"></div>

												<div class='detail_input_bg' style="width:340px"></div>

										    <div class='detail_input_bg' style="width:60px;text-align:right">목욕 : </div>
										    <div class="detail_input_bg noborder" style="width:70px">
										      <input type="text" id="AMT_BATH" class='detail_amt' readonly>
										    </div>

												<div class='detail_input_bg' style="width:130px">
													<input id='ONEWAY_YN' type='checkbox' style="display:none">
													<input id='FAMILY_YN' type='checkbox' style="display:none">
													<select id="FAMILY_REF" class='detail_input_fix' style="display:none"></select>
													<input type='text' id='SERVICE_AMT' class='detail_amt' style="display:none">
													<input type='text' id='SERVICE_MIN' class='detail_amt' style="display:none">
													<input type='text' id='PAY_AMT' class='detail_amt' style="display:none">
													<input type='text' id='REQ_AMT' class='detail_amt' style="display:none">
													<input type='text' id='REMAIN_AMT' class='detail_amt' style="display:none">
													<input type='text' id='OFFICE_AMT' class='detail_amt' style="display:none">
													<input type='text' id='PRODUCT_ID' class='detail_input_fix' style="display:none">
													<input type='text' id='PRODUCT_NAME' class='detail_input_fix' style="display:none">
													<input type='text' id='REMARK' class='detail_input_fix' style="display:none">
												</div>
											</div>

							    	</div>

										<!-- pthtml.Dw('fdw_cond') -->
									</td>
									<td style="padding-left: 10px; padding-right: 30px;">
										<button type="button" id='btn_schedule_insert' class="btn btn-info btn-xs" onClick="uf_append_schedule()">달력에 일정 추가하기</button>
									</td>
									<td style="padding-left: 30px; border-left: 1px dashed #666 " align="center">
										<INPUT onclick=xe_EditChanged(rd_div,this.value) id=rd_div_0 CHECKED type=radio value=S name=rd_div>
										<A href="javascript:if(!rd_div_0.disabled){rd_div_0.checked=true;xe_EditChanged(rd_div,rd_div_0.value);}" target=_self>달력에 선택한 일정</A>
										<INPUT onclick=xe_EditChanged(rd_div,this.value) id=rd_div_1 type=radio value=A name=rd_div>
										<A href="javascript:if(!rd_div_1.disabled){rd_div_1.checked=true;xe_EditChanged(rd_div,rd_div_1.value);}" target=_self>이번달 모든 일정</A>
										<br><br>
										<button type="button" id='btn_schedule_del' class="btn btn-info btn-xs" onClick="uf_delete_schedule()">삭제하기</button>
									</td>
								</tr>
							</table>
						</td>
						<td width="40px" valign="top" style="padding:6px 6px 0 0">
							<button type="button" id='btn_close' class="btn btn-warning btn-xs" onClick="uf_show_modify()">닫기</button>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr height="17px">
			<td>
				<span id="span_contract" style="margin-left:3px;"></sapn>
			</td>
		</tr>
		<tr height="100%" id=lfdw_list_tr>
			<td>
				<IFRAME id="form_calendar" src="/APPS/com/jsp/calendar4.jsp" width="100%" height="100%" marginwidth="0" marginheight="0" frameborder="0" scrolling="yes"></IFRAME>
			</td>
		</tr>
	</table>
</div>

<div id='grid_1' class='grid_div' style= "visibility:hidden">
	<div id="dg_1" class='slick-grid'></div>
</div>

<script type="text/javascript">
	var _params = "<%=params%>";

	topsearch.ResizeInfo			= {init_width:1000, init_height: 33, width_increase:1, height_increase:0};
	from_1.ResizeInfo					= {init_width:1000, init_height:660, width_increase:1, height_increase:1};
	grid_1.ResizeInfo					= {init_width:1000, init_height: 10, width_increase:1, height_increase:0};
</script>


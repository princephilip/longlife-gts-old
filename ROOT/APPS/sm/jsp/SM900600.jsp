<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script src="/Mighty/3rd/underscore/underscore-min.js"></script>
<script src="/Mighty/3rd/codemirror/lib/codemirror.js"></script>
<link rel="stylesheet" href="/Mighty/3rd/codemirror/lib/codemirror.css">
<script src="/Mighty/3rd/codemirror/mode/sql/sql.js"></script>
<link rel="stylesheet" href="/Mighty/3rd/codemirror/addon/hint/show-hint.css" />
<script src="/Mighty/3rd/codemirror/addon/hint/show-hint.js"></script>
<script src="/Mighty/3rd/codemirror/addon/hint/sql-hint.js"></script>

<link rel="stylesheet" href="/Mighty/3rd/font-awesome/css/all.css">
<link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap-theme.min.css">
<script type="text/javaScript" src="/Mighty/3rd/bootstrap/js/bootstrap.min.js"></script>

<style type="text/css">
.parameter-wrap { width: 100%; height: 128px; overflow-y: scroll; overflow-x: hidden; }
.updcol-wrap { width: 100%; height: 358px; overflow-y: scroll; overflow-x: hidden; margin-bottom: 10px; }
.property-wrap { width: 100%; height: 300px; overflow-y: scroll; overflow-x: hidden; margin-bottom: 10px; }
</style>

<div class="row">
  <div class="col-xs-12">
    <!-- Nav tabs -->
    <ul id="tab-1" class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#sql" aria-controls="sql" role="tab" data-toggle="tab">SQL</a></li>
      <li role="presentation"><a href="#grid" aria-controls="grid" role="tab" data-toggle="tab">Grid</a></li>
    </ul>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <!-- Tab panes -->
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="sql">
        <div class="row">
          <div class="col-xs-6">
            
            <div id="process-1" class="panel panel-default">
              <div class="panel-heading">
                1. 오라클 쿼리(toad 버전)를 넣으세요.
              </div>
              <div class="panel-body">
                <textarea id="qry-before" class="form-control" rows="24"></textarea>
              </div>
            </div>
          </div><!-- col-xs6 -->

          <div class="col-xs-6">
            <div id="process-2" class="panel panel-default">
              <div class="panel-heading">
                2. SELECT ID를 입력하세요.
              </div>
              <div class="panel-body">
                <div class="form-horizontal">
                  <div class="form-group">
                    <label for="select-id" class="col-sm-3 control-label">SELECT ID</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control input-sm" id="select-id" placeholder="">
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div id="process-3" class="panel panel-default">
              <div class="panel-heading">
                3. 파라미터 속성을 입력 하신 후, "COLUMN 속성 가져오기" 버튼을 누르세요.
              </div>
              <div class="panel-body">
                <div class="parameter-wrap">
                  <table id="sql-params" class="table"></table>
                </div>

                <div class="col-xs-12 text-center">
                  <button id="btn-get-colinfo" onclick="setUpdateColunms()" class="btn btn-primary">COLUMN 속성 가져오기</button>
                </div>
              </div>
            </div>
          </div><!-- col-xs-6 -->
        </div><!-- row -->

        <div class="row">
          <div class="col-xs-6">
            <div id="process-updatecols" class="panel panel-default">
              <div class="panel-heading">
                4. 업데이트 칼럼을 설정하신 후, "XML 생성" 버튼을 누르세요.
              </div>
              <div class="panel-body">
                <div class="form-horizontal">
                  <div class="form-group">
                    <label for="update-table" class="col-sm-3 control-label">UPDATE TABLE</label>
                    <div class="col-sm-9">
                      <select class="form-control input-sm" id="update-table">
                      </select>
                    </div>
                  </div>
                </div>


                <div class="updcol-wrap">
                  <table id="update-columns" class="table"></table>
                </div>
                
                <div class="col-xs-6 text-center">
                  <button onclick="convertXmlQuery(true)" class="btn btn-primary btn-block">XML 생성 & 클립보드 복사</button>
                </div>
                <div class="col-xs-6 text-center">
                  <button onclick="convertXmlQuery()" class="btn btn-primary btn-block">XML 생성만</button>
                </div>
              </div>
            </div>

          </div><!-- col-xs-6 -->
          
          <div class="col-xs-6">
            <div id="process-xml" class="panel panel-default">
              <div class="panel-heading">
                5. XML이 생성됩니다.
              </div>
              <div class="panel-body">
                <textarea id="qry-xml" class="form-control" rows="20"></textarea>
                <button onclick="createGrid()" class="btn btn-primary btn-block">GRID 생성</button>
              </div>
            </div>

          </div><!-- col-xs-6 -->
        </div><!-- row -->
      </div><!-- tab-pane -->

      <div role="tabpanel" class="tab-pane" id="grid">
        <div class="panel panel-default">
          <div class="panel-heading">
            1. 컬럼명 입력 (, 로 구분 예, " pgm_name, pgm_code, title, retrieve_yn " )
          </div>
          <div class="panel-body">
            <textarea id="grid-cols" class="form-control" rows="2" placeholder="컬럼명을 , 로 구분해서 입력하세요." ></textarea>
          </div>
        </div>
        
        <div class="panel panel-default">
          <div class="panel-heading">
            2. 컬럼의 속성을 설정
          </div>
          <div class="panel-body">
            <div class="property-wrap">
              <table id="grid-cols-setting" class="table">
              </table>
            </div>

            <div class="col-sm-6">
              <button id="btn-generategrid" class="btn btn-danger btn-block" onclick="setGridCode(true)">코드 생성하고, 클립보드 복사</button>
            </div>
            <div class="col-sm-6">
              <button id="btn-generategrid" class="btn btn-warning btn-block" onclick="setGridCode()">코드만 생성</button>
            </div>

          </div>
        </div>

        <div class="row">
          <div class="col-xs-6">
            <div class="panel panel-default">
              <div class="panel-heading">
                3. 아래를 복사해서 [pgmcode].Grid.js 파일로 저장
              </div>
              <div class="panel-body">
                <textarea id="grid-code" class="form-control" rows="20" ></textarea>
              </div>
            </div>
          </div><!-- col-xs-6 -->

          <div class="col-xs-6">
            <div class="panel panel-default">
              <div class="panel-heading">
                4. FreeForm 코딩생성
              </div>
              <div class="panel-body">
                <textarea id="freeform-code" class="form-control" rows="20" ></textarea>
              </div>
            </div>
          </div><!-- col-xs-6 -->
        </div><!-- row -->

      </div>
    </div>
  </div>
</div>


<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%
/**
 *<pre>
 * Statements
 *</pre>

 * @Class Name : SM900002.jsp
 * @Descriptio    Grid 정보관리
 * @Modification Information
 * @
 * @  수정일            수정자                   수정내용
 * @ -------    --------    ---------------------------
 * @ 2013.03.03    박영찬        최초 생성
 * @
 */
%>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap-theme.min.css">
<script type="text/javaScript" src="/Mighty/3rd/bootstrap/js/bootstrap.min.js"></script>

<style>
  .wrap-btn-grids { padding-top: 2px; min-width: 100px; }
  .wrap-btn-grids .btn { margin-left: 6px; }
  #ta-sql { border: 1px solid #ccc; }
  .modal-lg { width: 90% !important; }
  #grid-sql { height: 400px; }
  select.input-sm { height: 22px !important; padding: 1px 5px !important; font-size: 12px !important; }

  .nav-tabs>li.ffgrid.active>a, .nav-tabs>li.ffgrid.active>a:focus, .nav-tabs>li.ffgrid.active>a:hover { background-color: #f2dede; border-color: #ebccd1; border-bottom: 1px solid #f2dede; color: #a94442; }
  ul.tab-title { margin-left: 6px; }
  ul.tab-title.nav>li>a { padding: 2px 15px; }
  ul.tab-title.nav>li.active>a { font-weight: 700; }

  #ff-array .panel-body { background-color: #f2dede; }

  .connectedSortable { background-color: #fff; width: 100%; height: 28px; margin-bottom: 6px; list-style: none; clear: both; }
  .connectedSortable li { float: left; margin: 5px; }
  .connectedSortable li .label { font-size: 12px; }

  .table-condensed td { padding: 2px !important; }
  .table-condensed td .input-xs { padding: 2px 6px !important; height: 24px; }

  .ffgrid.active a, .wrap-ff-array span.label { cursor: pointer !important; }

  #freeform_panel { overflow-y: scroll; }

  .wrap-ff-array { max-height: 330px; overflow-y: auto; }
  .wrap-ff-preview .selected { background-color: #ccc !important; }

  #ff-preview .panel-body  { position: relative; }
  #ff-preview .panel-body table { margin-bottom: 5px; }

  .wrap-ff-preview { padding-right: 200px; width: 100%; }
  #freeform_setting { width: 180px; float: right; height: 120px; padding-right: 10px; padding-left: 10px; margin-top: 6px; border-left: 1px dashed #ccc; overflow-y: auto; }

</style>

<div id='topsearch' class='option_box'>

  <div id='xbtns'>
    <html:authbutton id='buttons' grid='dg_1' pgmcode="SM900040"></html:authbutton>
  </div>
  <div id='userxbtns'>
  </div>

  <div class='option_label'>시스템</div>
  <div class='option_input_bg'>
    <html:select name="S_SYS_ID" id = "S_SYS_ID" comCodeYn="0" codeGroup="sm|SM_AUTH_SYS|W_SM0915_01" params="${companyCode}" dispStyle="1" selected="20" showTitle="true" titleCode="전체" titleValue='%'></html:select>
  </div>

  <label class='option_label w160'>프로그램 코드/이름</label>
  <div class='option_input_bg'>
    <input id='S_PGM_CODE' type='text' class='option_input_fix w200'>
  </div>

  <label class='option_label w100'>작업중인 Grid</label>
  <div class="option_input_bg wrap-btn-grids text-danger">

  </div>

  <div class='option_input_bg'>
  <button type="button" id="btn-history" class="btn btn-success btn-xs" data-toggle="modal" data-target="#historyModal">History에서 불러오기</button>
  </div>

  <!-- label class='option_label w90'>New Freeform</label>
  <div class='option_input_bg new-grid'>
    <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#sqlModal">SQL 또는 TEXT에서 시작</button>
  </div -->

</div>

<div id="panel_left" class='grid_div'>
  <div id='grid_2' class='wr100 hr30'>
    <div id="dg_2" class="slick-grid"></div>
  </div>

  <div id='grid_1' class='wr100 hr70 pt5'>
    <div id="dg_1" class="slick-grid"></div>
  </div>
</div>


<div id='topoption' class='option_box2 ml5' style='width: 100%; height: 29px; '>
  <label class='option_label w80'>Width</label>
  <div class='option_input_bg'>
    <input id='ff-width' type='text' value='700' class='option_input_fix w60'> (100은 %)
  </div>

  <label class='option_label w80'>Col 수</label>
  <div class='option_input_bg'>
    <select class="form-control input-sm" id="ff-colcnt">
      <option valule="1">1</option>
      <option valule="2">2</option>
      <option valule="3" selected>3</option>
      <option valule="4">4</option>
      <option valule="5">5</option>
      <option valule="6">6</option>
      <option valule="7">7</option>
      <option valule="8">8</option>
      <option valule="9">9</option>
      <option valule="10">10</option>
      <option valule="11">11</option>
      <option valule="12">12</option>
      <option valule="13">13</option>
      <option valule="14">14</option>
      <option valule="15">15</option>
    </select>
  </div>

  <label class='option_label w120'>Label Width</label>
  <div class='option_input_bg'>
    <input id='ff-label-width' type='text' value='100' class='option_input_fix w60'>
  </div>

  <label class='option_label w80'>Title</label>
  <div class='option_input_bg'>
    <input id='ff-title' type='text' class='option_input_fix w220' placeholder="프리폼 제목 (입력 안하면 제목 없음)">
  </div>

  <div class='option_input_bg'>
    <button type="button" id="btn-reset" class="btn btn-warning btn-xs" style="font-weight: 700;">Grid DB에서 초기화</button>

    <!--
    <button class='btn btn-xs btn-default'>작업2</button>
    <button class='btn btn-xs btn-default'>btn3</button>
    -->
  </div>
</div><!-- #topoption -->



<div id="freeform_panel" class="grid_div ml5">

  <ul class="nav nav-tabs tab-title" role="tablist">
    <li role="presentation" class="ffgrid active">
      <a href="#ff-array" aria-controls="ff-array" role="tab" data-toggle="tab">Layout ▲</a>
    </li>
  </ul>

  <div class="tab-content">

    <div role="tabpanel" class="tab-pane active" id="ff-array">
      <div class="panel panel-danger">
        <div class="panel-body">

          <div class="wrap-ff-array">
          designer
          </div>


        </div><!-- panel-body -->
      </div><!-- panel -->
    </div><!-- tab-pane -->

  </div><!-- tab-content -->




  <ul class="nav nav-tabs tab-title" role="tablist">
    <li role="presentation" class="ffgrid active">
      <a href="#ff-preview" aria-controls="ff-preview" role="tab" data-toggle="tab">Preview ▲</a>
    </li>
  </ul>

  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="ff-preview">
      <div class="panel panel-danger">
        <div class="panel-body">

          <div class="wrap-ff-preview">
          </div>

          <div id="freeform_setting" class="grid_div wrap-ff-setting">

            <div id="setting_label" class="grid_div grid_div has_title">
              <label class="sub_title" style="margin-bottom: 2px;">Label 설정</label>
              <table class="table table-condensed table-bordered">
                <tr>
                  <th>Title</th>
                  <td><input type="text" id="setting-l-title" class="form-control input-sm input-xs" /></td>
                </tr>
                <tr>
                  <th>Width</th>
                  <td><input type="text" id="setting-l-width" class="form-control input-sm input-xs" /></td>
                </tr>
                <tr>
                  <th>Visible</th>
                  <td><input type="checkbox" id="setting-l-visible" value="Y" /></td>
                </tr>
              </table>
            </div>

            <div id="setting_input" class="grid_div has_title ">
              <label class="sub_title" >Input 설정</label>
              <table class="table table-condensed table-bordered">
                <tr>
                  <th>Width</th>
                  <td><input type="text" id="setting-i-width" class="form-control input-sm input-xs" /></td>
                </tr>
                <tr>
                  <th>Type</th>
                  <td>
                    <select id="setting-i-type" class="form-control input-sm">
                      <option value="checkbox">Checkbox</option>
                      <option value="date">Date</option>
                      <option value="select">Dropdown</option>
                      <option value="text">Text</option>
                      <option value="textarea">Textarea</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <th>Border</th>
                  <td><input type="checkbox" id="setting-i-border" value="Y" /> Left</td>
                </tr>
                <tr>
                  <th>Button</th>
                  <td><input type="checkbox" id="setting-i-button" value="Y" /></td>
                </tr>
                <!-- tr>
                  <th>Style</th>
                  <td><input type="text" id="setting-i-style" class="form-control input-sm input-xs" /></td>
                </tr>
                <tr>
                  <th>Blank</th>
                  <td><input type="checkbox" value="Y" /></td>
                </tr -->
              </table>
            </div>
          </div>


        </div><!-- panel-body -->
      </div><!-- panel -->
    </div><!-- tab-pane -->
  </div><!-- tab-content -->




  <ul class="nav nav-tabs tab-title" role="tablist">
    <li role="presentation" class="ffgrid active">
      <a href="#ff-property" aria-controls="ff-property" role="tab" data-toggle="tab">Property</a>
    </li>
    <li role="presentation" class="ffgrid">
      <a href="#ff-source" aria-controls="ff-source" role="tab" data-toggle="tab">Html</a>
    </li>
  </ul>

  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="ff-property">
      <div class="panel panel-danger">
        <div class="panel-body" style="height: 360px;">
          <div id="grid_3" class="grid_div wr100 hr100">
            <!-- div style="margin-top: -30px; padding-bottom: 5px; text-align: right">
              <button class="btn btn-xs btn-default" onclick="pf_add_col()" >추가</button>
              <button class="btn btn-xs btn-default" onclick="pf_ins_col()" >삽입</button>
            </div -->
            <div id="dg_3" class="slick-grid"></div>
          </div>

        </div>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="ff-source">
      <div class="panel panel-danger">
        <div class="panel-body">
          <button class="btn btn-xs btn-warning pull-right" id="copybtn-code-source" style="margin-bottom: 5px;">Copy To Clipboard</button>
          <div class="wrap-ff-source">
            <textarea id="code-html" class="form-control" rows="10" placeholder=""></textarea>
          </div>


        </div><!-- panel-body -->
      </div><!-- panel -->
    </div><!-- tab-pane -->

  </div><!-- tab-content -->
</div><!-- #freeform_panel -->





</div> <!-- #pageLayout 밖에 Modal 놓기 위한 Trick -->

<!-- Modal -->
<div class="modal fade" id="sqlModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="sqlModalLabel">SQL & PARAMS</h4>
      </div>
      <div class="modal-body">

        <ul class="nav nav-tabs tab-title" role="tablist">
          <li role="presentation" class="ffgrid active">
            <a href="#ff-from-sql" aria-controls="ff-from-sql" role="tab" data-toggle="tab">SQL에서 시작</a>
          </li>
          <li role="presentation" class="ffgrid">
            <a href="#ff-from-json" aria-controls="ff-from-json" role="tab" data-toggle="tab">Freeform JSON에서 시작</a>
          </li>
          <li role="presentation" class="ffgrid">
            <a href="#ff-from-text" aria-controls="ff-from-text" role="tab" data-toggle="tab">TEXT 컬럼명에서 시작</a>
          </li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="ff-from-sql">

            <div class="row">
              <div class="col-xs-9">
                <div class="col-xs-12 sql-label">
                  <label class="control-label">SQL & Params</label>
                  <span class="">( SQL 을 수정하면 하단에 "수정 SQL 적용" 버튼이 활성화 됩니다.)</span>
                </div>CREATE OR REPLACE FUNCTION SBTEC.X_SEARCHC
                <div class="col-xs-12">
                  <div class="tab-content wrap-textarea">
                    <div class="tab-pane active" id="ta-sql">
                      <textarea id="grid-sql" class="form-control" placeholder="SQL 쿼리를 입력하세요."></textarea>
                    </div>
                    <div class="tab-pane" id="ta-grid">
                      <textarea id="grid-txt" readonly class="form-control" placeholder="Grid 구문이 표시됩니다."></textarea>
                    </div>
                    <div class="tab-pane" id="ta-js">
                      <textarea id="js-txt" readonly class="form-control" placeholder="js 구문이 표시됩니다."></textarea>
                    </div>
                    <div class="tab-pane" id="ta-xmls">
                      <textarea id="sql-xmlx" readonly class="form-control" placeholder="Sql 구문이 표시됩니다."></textarea>
                    </div>
                  </div>
                </div>
              </div><!-- .col-xs-9 -->
              <div class="col-xs-3 wrap-updtables">
                <div class="row mt5">
                  <div class="col-xs-12">
                    <label class="control-label">Params</label>
                  </div>
                  <div class="col-xs-12 wrap-params">
                    <table id="sql-params" class="table">
                    </table>
                  </div>
                </div>

              </div><!-- .col-xs-3 -->
            </div><!-- .row -->

          </div><!-- tab-pane -->

          <div role="tabpanel" class="tab-pane" id="ff-from-json">
            <div class="sql-label">
              <label class="control-label">Freeform JSON String</label><br />
              <span class="">( Freeform 디자이너에 의해 만들어진 Json문자열을 입력합니다. )</span>
            </div>
            <textarea id="start-json" rows="15" class="form-control" placeholder=""></textarea>
          </div>

          <div role="tabpanel" class="tab-pane" id="ff-from-text">
            <div class="sql-label">
              <label class="control-label">Column명칭 문자열</label><br />
              <span class="">( 콤마, 엔터, 등으로 구분합니다. )</span>
            </div>
            <textarea id="column-list" rows="15" class="form-control" placeholder=""></textarea>
          </div>

        </div><!-- tab-content -->





      </div><!-- .modal-body -->
      <div class="modal-footer">
        <div class="text-center">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="btn-modified-sql" class="btn btn-primary">수정 SQL 적용</button>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="historyModal" tabindex="-1" role="dialog" aria-labelledby="historyModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="historyModalLabel">HISTORY</h4>
      </div>
      <div class="modal-body">

        <div id="grid_4" class="grid_div wr100" style="height: 300px;">
          <div id="dg_4" class="slick-grid wr100 hr100"></div>
        </div>

        <div class="clearfix"></div>

      </div><!-- .modal-body -->
      <div class="modal-footer">
        <div class="text-center">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="btn-modified-sql" class="btn btn-primary">수정 SQL 적용</button>
        </div>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  topsearch.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};
  panel_left.ResizeInfo = {init_width:320, init_height:665, width_increase:0, height_increase:1};
  topoption.ResizeInfo = {init_width:680, init_height:33, width_increase:1, height_increase:0};
  freeform_panel.ResizeInfo = {init_width:680, init_height:632, width_increase:1, height_increase:1};
  //freeform_setting.ResizeInfo = {init_width:240, init_height:632, width_increase:0, height_increase:1};
  //grid_3.ResizeInfo         = {init_width:240, init_height:400, width_increase:0, height_increase:0};
  //grid_4.ResizeInfo    = {init_width:500, init_height:300, width_increase:0, height_increase:0};
  //grid_1.ResizeInfo = {init_width:360, init_height:465, width_increase:0, height_increase:1};
</script>
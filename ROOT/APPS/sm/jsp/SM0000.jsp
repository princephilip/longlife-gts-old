﻿
<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<html:authbutton id='buttons' grid='dg_1' pgmcode="SM0000"></html:authbutton>
	</div>
</div>

<div id='grid_1' class='grid_div' >
	<div id="dg_1" class="slick-grid"></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo 	= {init_width: 1000, init_height:33 , width_increase:1, height_increase:0};
	grid_1.ResizeInfo 		= {init_width: 1000, init_height:665, width_increase:1, height_increase:1};
</script>

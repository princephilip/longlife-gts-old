<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2'>
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true" pgmcode="SM0103"></html:authbutton>
		</span>
	</div>

	<div class='option_label w100'>코드찾기</div>
	<div class='option_input_bg w200'>
		<input id='S_HNAME' type='text' class='option_input_fix' style="width:190px">
	</div>
</div>

<div id='grid_1' class='grid_div mr5'>
	<span style="float:right;margin-bottom:3px">
		<html:authchildbutton id='buttons' grid='dg_1' append='true' insert='false' duplicate='false' delete='true' excel='false' retrieve='false' ></html:authchildbutton>
	</span>
	<div id="dg_1" style='float:left; width:100%; height:100%;' class='slick-grid'></div>
</div>

<div id='grid_2' class='grid_div'>
	<span style="float:right;margin-bottom:3px">
		<html:authchildbutton id='buttons' grid='dg_2' append='true' insert='false' duplicate='false' delete='true' excel='false' retrieve='false' ></html:authchildbutton>
	</span>
	<div id="dg_2" style='float:left; width:100%; height:100%;' class='slick-grid'></div>
</div>



<script type="text/javascript">

	topsearch.ResizeInfo = {init_width:1000, init_height: 33, width_increase:1, height_increase:0};
	grid_1.ResizeInfo    = {init_width: 400, init_height:645, width_increase:0, height_increase:1};
	grid_2.ResizeInfo    = {init_width: 600, init_height:645, width_increase:1, height_increase:1};

</script>



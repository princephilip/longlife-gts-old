<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="<c:url value='/Mighty/template/js/XG_GRID1_template.js'/>"></script>

<div id='topsearch' class='option_box2' >
  <div id='xbtns'>
    <html:authbutton id='buttons' grid='dg_1' pgmcode="SM0945"></html:authbutton>
  </div>

  <div class='option_label w120'>승인상태</div>
  <div class='option_input_bg'>
    <select id='S_STATUS_DIV' class='option_input_fix w80'></select>
  </div>

  <div class='option_label_left'>사번성명</div>
  <div class='option_input_bg w220'>
    <input id='S_EMP_NAME' type='text' class='option_input_fix'>
  </div>

</div>

<div id='grid_1' class='grid_div has_title' >
  <label class='sub_title'>메뉴권한 신청</label>
  <div id="dg_1" class="slick-grid"></div>
</div>

 <script type="text/javascript">
  topsearch.ResizeInfo = {init_width:1000, init_height:33 , width_increase:1, height_increase:0};
  grid_1.ResizeInfo    = {init_width: 1000, init_height:665, width_increase:1, height_increase:1};
</script>

<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2'>
	<div class='option_label w100'>통합검색</div>
	<div id='di_11' class='option_input_bg'>
		<input id='S_FIND' type='text' class='option_input_fix w200'>
	</div>
	<div class='option_label w100'>사용여부</div>
	<div style="position:relative;"  id="di_12" class="option_input_bg">
		<select id='S_USE_YESNO' class="w70">
			<option value='%'>전체</option>
			<option value='Y' selected>사용</option>
			<option value='N'>중지</option>
		</select>
	</div>
<!-- 	<div class='option_label w100'>사용자구분</div>
	<div style="position:relative;"  id="di_12" class="option_input_bg">
		<select id='S_USER_TAG'  class="w80"></select>
	</div>  -->

	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' pgmcode="SM0905"></html:authbutton>
		</span>
	</div>
	<div id='userxbtns' style='height:23px;'>
			<span style="width:100%;height:100%;vertical-align:middle;">
				<button type="button" id='pass_new' class="btn btn-warning btn-xs" onClick="pf_resetPassword();">비밀번호초기화</button>
			</span>
	</div>
</div>

<div id='grid_top' class='grid_div mt5'>
	<div id='tabs_1' class='grid_div'>
		<div id="ctab_nav" class="xtabs_nav">
		  <ul>
		    <li><a href="#ctab_1">정직원</a></li>
		    <li><a href="#ctab_1">계약직</a></li>
		    <li><a href="#ctab_1">협력업체</a></li>
		  </ul>
		</div>

		<div id="ctab_1" class='tabc_div'>
			<div id='tabsub_1' class='grid_div'>
				<div id='grid_1' class='grid_div'>
					<div id="dg_1" class='slick-grid'></div>
				</div>
			</div>

			<div id='tabsub_2' class='xtabc_div' >
		      <div id='grid_3' class='grid_div' >
		        <div id="dg_3" class="slick-grid"></div>
		      </div>
		    </div><!-- #tabsub_1 -->

		    <div id='tabsub_3' class='xtabc_div' >
		      <div id='grid_4' class='grid_div' >
		        <div id="dg_4" class="slick-grid"></div>
		      </div>
		    </div><!-- #tabsub_1 -->


		</div>

	</div>
</div>

<!-- <div id='grid_1' class='grid_div mr5'>
	<div id="dg_1" style="width:100%; height:100%; " class="slick-grid"></div>
</div> -->

<!--
<div id='grid_2' class='grid_div' style=''>
	<div id="dg_2" style="width:100%; height:100%; " class="slick-grid"></div>
</div>
-->

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:33 , width_increase:1, height_increase:0};
	//grid_1.ResizeInfo 	 = {init_width:1000, init_height:665, width_increase:1, height_increase:1};
	//grid_2.ResizeInfo 	 = {init_width:50  , init_height:665, width_increase:1, height_increase:1};

	grid_top.ResizeInfo = {init_width:1000, init_height:225, width_increase:1, height_increase:1};
	tabs_1.ResizeInfo    = {init_width:1000, init_height:640, width_increase:1, height_increase:1};	// 그리드와 탭엮기위함
	ctab_nav.ResizeInfo  = {init_width:1000, init_height: 25, width_increase:1, height_increase:1};	// 탭 틀
	ctab_1.ResizeInfo    = {init_width:1000, init_height:640, width_increase:1, height_increase:1};	// 탭 뿌려줌 및  체인지 작동
	tabsub_1.ResizeInfo  = {init_width:1000, init_height:640, width_increase:1, height_increase:1};
	grid_1.ResizeInfo   = {init_width:1000, init_height:640	, width_increase:1, height_increase:1};
	tabsub_2.ResizeInfo  = {init_width:1000, init_height:640, width_increase:1, height_increase:1};
	grid_3.ResizeInfo   = {init_width:1000, init_height:640	, width_increase:1, height_increase:1};
	tabsub_3.ResizeInfo  = {init_width:1000, init_height:640, width_increase:1, height_increase:1};
	grid_4.ResizeInfo   = {init_width:1000, init_height:640	, width_increase:1, height_increase:1};
</script>

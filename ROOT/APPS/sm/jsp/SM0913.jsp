<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
/**
 *<pre>
 * Statements
 *</pre>

 * @Class Name : SM0913.jsp
 * @Descriptio	  시스템 사용통계
 * @Modification Information
 * @
 * @  수정일        		 수정자                   수정내용
 * @ -------    --------    ---------------------------
 * @ 2013.03.03    박영찬        최초 생성
 * @
 */
%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<div id='topsearch' class='option_box2'>
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_parent' pgmcode="SM0913"></html:authbutton>
		</span>
	</div>
	<div class='option_label'>사용자</div>
	<div id='di_11' class='option_input_bg w120'>
		<input id='S_USER' type='text' class='option_input'>
	</div>
	<div class='option_label'>조회일자</div>
	<div class='option_input_bg w220'>
		<html:fromtodate id="dateimages" idHead='CON' classType="search" dateType="date" moveBtn="true" readOnly="true"></html:fromtodate>&nbsp;
	</div>
	<div id='userxbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			  <html:button name='log_sum' id='log_sum' type='button' icon='ui-icon-transfer-e-w' desc="로그집계" onClick="pf_syslog_summary();"></html:button>
		</span>
	</div>
</div>


<div id='grid_parent' class='grid_div'>
	<%= xgov.core.util.XHtmlUtil.RealGrid("dg_parent", request.getHeader("User-Agent"),"0","N","N") %>
</div>

<div id='grid_child' class='grid_div'>
	<%= xgov.core.util.XHtmlUtil.RealGrid("dg_child", request.getHeader("User-Agent"),"1","N","Y") %>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};
	grid_parent.ResizeInfo = {init_width:370, init_height:664, width_increase:0, height_increase:1};
	grid_child.ResizeInfo = {init_width:630, init_height:664, width_increase:1, height_increase:1};
</script>
<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<script type="text/javascript" src="/Theme/js/temp_common.js"></script>


<div id='topbar'>
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' pgmcode="SM900700"></html:authbutton>
		</span>
	</div>
</div>


<div class="x5-row">
	<button class="btn btn-default btn-sm" onClick="pf_CollapseAll();">
     <i class="fa fa-save"></i> 전체 축소
	</button>

	<button class="btn btn-default btn-sm" onClick="pf_ExpandAll();">
     <i class="fa fa-save"></i> 전체 확장
	</button>

	<button class="btn btn-default btn-sm" onClick="pf_ExpandLevel_2();">
     <i class="fa fa-save"></i> 2레벨 까지 확장
	</button>

	<button class="btn btn-default btn-sm" onClick="pf_Collapse();">
     <i class="fa fa-save"></i> 선택 행 축소
	</button>

	<button class="btn btn-default btn-sm" onClick="pf_Expand1();">
     <i class="fa fa-save"></i> 선택 행 확장 (자식 행 축소 상태 유지)
	</button>

	<button class="btn btn-default btn-sm" onClick="pf_Expand2();">
     <i class="fa fa-save"></i> 선택 행 확장 (자식 행도 전체 확장)
	</button>
</div>


<div class="x5-row">
	<button class="btn btn-default btn-sm" onClick="pf_GetParent();">
     <i class="fa fa-save"></i> 선택 행의 부모 행번호
	</button>

	<button class="btn btn-default btn-sm" onClick="pf_GetChildCount();">
     <i class="fa fa-save"></i> 선택 행의 자식 수
	</button>

	<button class="btn btn-default btn-sm" onClick="pf_GetChildren1();">
     <i class="fa fa-save"></i> 선택 행의 자식 행 번호
	</button>

	<button class="btn btn-default btn-sm" onClick="pf_GetChildren2();">
     <i class="fa fa-save"></i> 선택 행의 자식 데이타
	</button>

	<button class="btn btn-default btn-sm" onClick="pf_GetDescendants1();">
     <i class="fa fa-save"></i> 선택 행의 모든 자식 행 번호
	</button>

	<button class="btn btn-default btn-sm" onClick="pf_GetDescendants2();">
     <i class="fa fa-save"></i> 선택 행의 모든 자식 행 데이타
	</button>
</div>


<div class="x5-row">
	<button class="btn btn-default btn-sm" onClick="pf_ChangeParent();">
     <i class="fa fa-save"></i> 10번 행의 부모를 3번 행으로 변경
	</button>

	<button class="btn btn-default btn-sm" onClick="pf_TreeLevelUp();">
     <i class="fa fa-save"></i> 선택 행의 레벨을 1단계 올림
	</button>
</div>


<div class="x5-row">
	<button class="btn btn-default btn-sm" onClick="pf_AddCurrent();">
     <i class="fa fa-save"></i> 동일 레벨의 행을 추가
	</button>

	<button class="btn btn-default btn-sm" onClick="pf_AddChild();">
     <i class="fa fa-save"></i> 선택 행의 자식 행을 추가
	</button>
</div>


<div id='grid_1' class='grid_div'>
	<div id="dg_1" style="width:100%; height:100%; " class="slick-grid"></div>
</div>


<script type="text/javascript">
	topbar.ResizeInfo = {init_width:1000, init_height:35, anchor:{x1:1,y1:0,x2:1,y2:0}};
	grid_1.ResizeInfo = {init_width:1000, init_height:565, width_increase:1, height_increase:1};
</script>
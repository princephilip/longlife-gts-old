<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%
/**
 *<pre>
 * Statements
 *</pre>

 * @Class Name : SM0201.jsp
 * @Descriptio	  시스템코드 관리
 * @Modification Information
 * @
 * @  수정일        		 수정자                   수정내용
 * @ -------    --------    ---------------------------
 * @ 2013.03.03    박영찬        최초 생성
 * @
 */
%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<div id='topbar'>
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' pgmcode="SM90100"></html:authbutton>
		</span>
	</div>
</div>


<div id='grid_1'>
	<div id='dg_1' style="float:left; width: 100%; height: 100%;"></div>
</div>


<script type="text/javascript">
	topbar.ResizeInfo = {init_width:1000, init_height:33, anchor:{x1:1,y1:0,x2:1,y2:0}};
	grid_1.ResizeInfo = {init_width:1000, init_height:667, width_increase:1, height_increase:1};
</script>
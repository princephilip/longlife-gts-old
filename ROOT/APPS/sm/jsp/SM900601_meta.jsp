<%@ page language="java" contentType="application/json; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.sql.ResultSetMetaData"%>
<%
String sql_query = request.getParameter("query");
String table_name = request.getParameter("tables");
String sql_tables[] = table_name.split(",");

xgov.core.dao.XSQLRun xsql = null;

try {
  xsql = new xgov.core.dao.XSQLRun("sm");
  xsql.executeSQL(sql_query);

  // 칼럼 타입 읽기..
  int colcount = xsql.getColCount();

  ResultSetMetaData rsMetaData = xsql.rs.getMetaData();
  int numberOfColumns = rsMetaData.getColumnCount();


  // 테이블별 칼럼정보는 나중에 합시다.
  out.print( "{\"columns_info\":[" );

  for(int i = 1; i <= colcount; i++) {
    if(i>1) {
      out.print( "," );
    }
    out.print( "{" );
    out.print( "\"TableName\":" );
    out.print( "\""+rsMetaData.getTableName(i)+"\"" );
    out.print( ",\"columnName\":" );
    out.print( "\""+rsMetaData.getColumnName(i)+"\"" );
    out.print( ",\"columnType\":" );
    out.print( "\""+rsMetaData.getColumnTypeName(i)+"\"" );
    out.print( "}" );
  }

  out.print( "]}" );
} 
catch (Exception ex) {
  throw ex;
}
finally {
  if (xsql != null) {xsql.close();}  
  xsql = null;
}



%>


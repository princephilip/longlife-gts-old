<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link rel="stylesheet" href="/Mighty/3rd/font-awesome/css/all.css">
<link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap-theme.min.css">
<script type="text/javaScript" src="/Mighty/3rd/bootstrap/js/bootstrap.min.js"></script>

<!-- script type="text/javascript" src="https://xinternet.github.io/mx5/dev_apps/sm/js/SM900003.js"></script -->
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<style type="text/css">
  #pageLayout { overflow-x: hidden; overflow-y: auto; }
  #jsp-src  { height: 300px; }
  #tag_buttons { padding: 10px 0; }
  #tag_buttons .btn { margin-left: 5px; margin-right: 5px; font-size: 12px !important; } /* color: #fff; font-weight: 700;  */
  #tag_buttons .btn-info i, #tag_buttons .btn-danger i { color: #fff; font-weight: 700; }
  #tag_buttons .btn-danger { margin-left: 30px; }
</style>

<div class="row">
  <div class="col-md-12">
    <div class='option_label w100'>테마</div>
    <div id='di_12' class='option_input_bg w200'>
      <select name="S_THEME_ID" id="S_THEME_ID" size="1">
      </select>
    </div>

    <div class='option_label w100'>검색</div>
    <div class='option_input_bg w200'>
      <input id='S_FIND' type='text' class='option_input_fix' style="width:190px">
    </div>

    <div id='xbtns'>
      <html:authbutton id='buttons' grid='dg_1' pgmcode="SM900003"></html:authbutton>
    </div>

    <div class="clearfix"></div>
  </div>

</div>

<div class="row-fluid" style="clear: both; padding-top: 6px;">
  <div id="tag_buttons" class="col-xs-12">
    <strong>Tags : </strong>
  </div>
</div>

<div class="row-fluid clearfix" >
  <div id="layouts_list" class="col-xs-12">

  </div>
</div>


</div> <!-- #pageLayout 밖에 Modal 놓기 위한 Trick -->


<!-- Modal -->
<div class="modal fade" id="srcModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="srcModalLabel">SRC</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12">

            <textarea id="jsp-src" class="form-control" placeholder="JSP 구문이 표시됩니다."></textarea>

          </div><!-- .col-xs-12 -->

        </div><!-- .row -->
      </div><!-- .modal-body -->
      <div class="modal-footer">
        <div class="text-center">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <!-- button type="button" id="btn-modified-sql" class="btn btn-primary">수정 SQL 적용</button -->
        </div>
      </div>
    </div>
  </div>


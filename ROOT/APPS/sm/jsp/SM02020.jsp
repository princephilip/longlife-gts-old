<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<jsp:include page="/Mighty/jsp/UploadExcelForm.jsp"/>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true" pgmcode="SM02020"></html:authbutton>
		</span>
	</div>

  <div class='option_label'>기준년도</div>
  <div class='option_input_bg'>
    <html:basicdate id="dateimages" classType="option" dateType="year" moveBtn="true" readOnly="false"></html:basicdate>
  </div>


  <div id='userxbtns' >
    <span style="width:100%;height:100%;vertical-align:middle;">
      <button type="button" id='btn_create' class="btn btn-success btn-xs" onClick="CreateCalendar();">카렌다생성</button>
    </span>
  </div>

</div>

<div id='grid_1' class='grid_div'>
	<div id="dg_1" class="slick-grid"></div>
</div>


<script type="text/javascript">
	topsearch.ResizeInfo			= {init_width:1000, init_height: 35, width_increase:1, height_increase:0};
	grid_1.ResizeInfo					= {init_width:1000, init_height:660, width_increase:1, height_increase:1};
</script>


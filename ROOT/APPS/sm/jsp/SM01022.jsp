<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<div id='topsearch' class='option_box2'>
	<div class='option_label w80'>거래처검색</div>
	<div id='di_11' class='option_input_bg' style='width:110px;'>
		<input id='S_VENDOR_NAME' type='text' class='option_input_fix' style='width:100px;'>
	</div>
	<div class='option_label w80' style='width:70px;'>사용여부</div>
	<div style="position:relative;width:100px"  id="di_12" class="option_input_bg">
		<select id='S_USE_YESNO' style="width:90px">
			<option value='Y'>사용</option>
			<option value='N'>미사용</option>
			<option value='%'>전체</option>
		</select>
	</div>
	<div class='option_label w80'>사용자구분</div>
	<div style="position:relative;width:55px"  id="di_12" class="option_input_bg">
		<!-- <html:select name="CATEGORY" id="CATEGORY" comCodeYn="1" codeGroup="SM|USER_TAG" dispStyle="0" showTitle="false" titleCode="선택"></html:select>--> <!-- 밑에 쿼리없는 select 추후에 쿼리랑 연결할 예정 -->
		<select id='S_DIV'></select>
	</div>

	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' pgmcode="SM01022"></html:authbutton>
		</span>
	</div>

	<div id='userxbtns' >
		<div class='option_input_bg' >
			<span style="width:100%;height:100%;vertical-align:middle;">
				<button type="button" id='btn_create_id' 					class="btn btn-success btn-xs" 	onClick="pf_create_id();">거래처_계정생성</button>
			</span>
		</div>
	</div>

</div>

<div id='freeform' class='detail_box ver2'>
	<div class='detail_row'>
		<label class='detail_label w100'>거래처구분</label>
		<div class='detail_input_bg w150'>
			<select id='CUST_TAG'></select>
		</div>
		<label class='detail_label w100'>*사업자번호</label>
		<div class='detail_input_bg w150'>
			<input id='CUST_CODE' type='text' class='detail_cust tac' >
		</div>
		<label class='detail_label w100'>상호명</label>
		<div class='detail_input_bg w190'>
			<input id='CUST_NAME' type='text'>
		</div>
		<label class='detail_label w60'>업태</label>
		<div class='detail_input_bg w150'>
			<input id='CONDITION' 	type='text'>
		</div>
		<label class='detail_label w100'>업종</label>
		<div class='detail_input_bg w200'>
			<input id='CATEGORY' 	type='text'>
		</div>
		<div class='row-closing'></div>
	</div>

	<div class='detail_row'>
		<label class='detail_label w100'>대표자</label>
		<div class='detail_input_bg w150'>
			<input id='OWNER' type='text' >
		</div>
		<label class='detail_label w100'>법인등록번호</label>
		<div class='detail_input_bg w150'>
			<input id='REGISTER_NO' type='text' >
		</div>
		<label class='detail_label w100'>우편번호</label>
		<div class='detail_input_bg w150'>
			<input id='ZIP' type='text' >
			<!-- button id='btn_addrfind' class="search_find_img" / -->
		</div>

		<label class='detail_label w100'>주소</label>
		<div class='detail_input_bg'>
			<input id='ADDR' type='text' >
		</div>

		<!--div class='detail_input_bg noborder'>
			<input id='ADDR2' type='text' >
		</div-->
	</div>

	<div class='detail_row'>
		<label class='detail_label w100'>거래유형</label>
		<div class='detail_input_bg w150'>
			<select id='TRADE_TYPE' ></select>
		</div>
		<label class='detail_label w100'>계산서담당자</label>
		<div class='detail_input_bg w150'>
			<input id='TAXEMP_NAME' type='text'  >
		</div>
		<label class='detail_label w100'>담당자</label>
		<div class='detail_input_bg w150'>
			<input id='CHARGE_NAME' type='text'  >
		</div>
		<label class='detail_label w100'>전화번호</label>
		<div class='detail_input_bg w150'>
			<input id='TEL' 		type='text' >
		</div>
		<div class='detail_input_bg noborder'>
		</div>
		<!-- <label class='detail_label w100'>은행</label>
		<div class='detail_input_bg w150 has_button'>
			<input id='BANK_CODE' 		type='text'  >
			<button id='btn_bankcode' class="search_find_img" />
		</div>
		<div class='detail_input_bg'>
			<input id='BANK_NAME' type='text'  >
		</div> -->
	</div>

	<div class='detail_row'>
		<label class='detail_label w100'>과세정보</label>
		<div class='detail_input_bg w150'>
			<select id='BUSINESS_TYPE'></select>
		</div>
		<label class='detail_label w100'>담당부서</label>
		<div class='detail_input_bg w150'>
			<input id='TAXEMP_DEPT_NAME' 		type='text' >
		</div>
		<label class='detail_label w100'>팩스번호</label>
		<div class='detail_input_bg w150'>
			<input id='FAX' type='text' >
		</div>
		<label class='detail_label w100'>휴대폰</label>
		<div class='detail_input_bg w150'>
			<input id='TEL2' type='text'  >
		</div>
		<div class='detail_input_bg noborder'>
		</div>

		<!-- <label class='detail_label w100'>예금주</label>
		<div class='detail_input_bg'>
			<input id='DEPOSIT_OWNER' type='text' >
		</div> -->

	</div>

	<div class='detail_row'>
		<label class='detail_label w100'>지급그룹</label>
		<div class='detail_input_bg w150'>
			<select id='PAY_GROUP' ></select>
		</div>
		<label class='detail_label w100'>휴대폰(세금)</label>
		<div class='detail_input_bg w150'>
			<input id='TAXEMP_TEL' type='text' >
		</div>
		<label class='detail_label w100'>이메일</label>
		<div class='detail_input_bg w390'>
			<input id='TAXEMP_EMAIL' type='text' >
		</div>

<!-- 		<label class='detail_label w100'>계좌번호</label>
		<div class='detail_input_bg'>
		 ※ 계좌번호 등록 및 사용여부 변경은 재경팀에 문의 바랍니다.
 -->			<!-- <input id='DEPOSIT_NO' type='text' > -->
<!-- 		</div> -->
		<label class='detail_label w100'>대표계좌</label>
		<div class='detail_input_bg'>
			<input id='MAIN_BANK_INFO' type='text' readonly>
		</div>

	</div>

	<div class='detail_row'>
		<label class='detail_label w100'>승인일</label>
		<div class='detail_input_bg w150'>
			<input id='BUSINESS_CHK_DATE' type='text' class='detail_date'>
		</div>
		<label class='detail_label w100'>폐업일</label>
		<div class='detail_input_bg w150'>
			<input id='CLOSING_DATE' type='text' class='detail_date'>
		</div>
		<label class='detail_label w100'>유효일자</label>
		<div class='detail_input_bg w85'>
			<input id='DEPOSIT_FROMDATE' type='text' class='detail_date'>
		</div>
		<div class='detail_input_bg w20 text_center noborder'>~</div>
		<div class='detail_input_bg w85 noborder'>
		  <input id='DEPOSIT_TODATE' type='text' class='detail_date'>
		</div>
		<div class='detail_input_bg w200 noborder'></div>
		<label class='detail_label w100'>사용여부</label>
		<div class='detail_input_bg w80'>
			<select id='USE_YN'>
				<option value='Y'>사용</option>
				<option value='N'>미사용</option>
			</select>
		</div>
		<div class='detail_input_bg noborder'>
			<input id='USE_REMARK' type='text' style="color: red;">
		</div>

		<div class='row-closing'></div>
	</div>

	<div class='detail_row'>
		<label class='detail_label w100'>채권가압류</label>
		<div class='detail_input_bg w150'>
			<input id='ATTACH_YN' type='checkbox' />
		</div>
		<label class='detail_label w100'>가압류첨부</label>
		<div class='detail_input_bg has_button' style="width:390px">
			<input id='ATTACH_FILE_NAME' type='text' readonly >
			<button id='btn_atchfile' class="search_find_img"  onClick="pf_atch_file();"/>
		</div>
		<label class='detail_label w100'>지불구분</label>
		<div class='detail_input_bg w150'>
			<select id='PAY_TAG' ></select>
		</div>
		<div class='detail_input_bg noborder'>
		</div>
	</div>

	<div class='detail_row detail_row_bb'>
		<label class='detail_label w100'>등록자</label>
		<div class='detail_input_bg w150'>
			<input id='EMP_NO' type='text' >
		</div>

		<label class='detail_label w100'>등록일</label>
		<div class='detail_input_bg w150'>
			<input id='INPUT_DATE' type='text' class='detail_date' >
		</div>

		<label class='detail_label w100'>최종 수정자</label>
		<div class='detail_input_bg w150'>
			<input id='UP_EMP_NAME' type='text' readonly>
		</div>

		<label class='detail_label w100'>최종 수정일</label>
		<div class='detail_input_bg w150'>
			<input id='UP_DATE' type='text' class='detail_date' readonly>
		</div>

		<label class='detail_label w100'>적요</label>
		<div class='detail_input_bg'>
			<input id='REMARK' type='text' ><!-- 632px -->
		</div>
	</div>

</div>

<div id="tabs_1" class='tabs_div ml5 has_title'> <!-- 그리드와 탭 엮게 하기위함. -->
<label class='sub_title'>거래처 상세내역</label>
	<div id="ctab_nav" class="tabs_nav"> <!-- 탭 나눔 지정. -->
	  <ul>
	    <li><a href="#ctab_1">전체</a></li>
	    <li><a href="#ctab_1">일반사업자</a></li>
	    <li><a href="#ctab_1">폐업자</a></li>
	    <li><a href="#ctab_1">개인</a></li>
	    <li><a href="#ctab_1">공공기관</a></li>
	    <li><a href="#ctab_1">금융기관</a></li>
	    <li><a href="#ctab_1">기타</a></li>
	    <li><a href="#ctab_1">사원</a></li>
	    <li><a href="#ctab_1">부서</a></li>
	    <li><a href="#ctab_1">건설사</a></li>
	  </ul>
	</div>

  <div id="ctab_1" class='tabc_div'> <!-- 탭체인지에 따른 이벤트 발생 코딩가능하게 해줌. -->
		<div id='grid_1' class='grid_div mr5'>
			<div id="dg_1" style="width:100%; height:100%; " class="slick-grid"></div>
		</div>
	</div>

</div>

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};
	freeform.ResizeInfo  = {init_width:1000, init_height:255, width_increase:1, height_increase:0};

	tabs_1.ResizeInfo 	 = {init_width:994, init_height:410, width_increase:1, height_increase:1}; // 그리드와 탭엮기위함
		ctab_nav.ResizeInfo = {init_width:980, init_height:25, width_increase:1, height_increase:1}; //탭 틀
		ctab_1.ResizeInfo 	= {init_width:1000, init_height:385, width_increase:0, height_increase:1}; // 탭 뿌려줌 및  체인지 작동
		grid_1.ResizeInfo 	= {init_width:1000, init_height:385, width_increase:1, height_increase:1};
</script>

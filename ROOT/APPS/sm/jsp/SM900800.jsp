<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%
/**
 *<pre>
 * Statements
 *</pre>

 * @Class Name : SM0201.jsp
 * @Descriptio    시스템코드 관리
 * @Modification Information
 * @
 * @  수정일            수정자                   수정내용
 * @ -------    --------    ---------------------------
 * @ 2013.03.03    박영찬        최초 생성
 * @
 */
%>

<%@ include file="/Theme/jsp/comm_inc_chart_morris.jsp"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2'>
  <div id='xbtns'>
    <span style="width:100%;height:100%;vertical-align:middle;">
      <html:authbutton id='buttons' grid='dg_parent' pgmcode="SM900800"></html:authbutton>
    </span>
  </div>

  <div class='option_label'>chart 종류</div>
  <div id='di_11' class='option_input_bg'>
    <select name="S_CHART_TYPE" id="S_CHART_TYPE">
      <option value="bar">Bar</option>
      <option value="line" selected>Line</option>
      <option value="area">Area</option>
      <option value="stacked">Stacked</option>
      <option value="donut">Donut</option>
    </select>
    <button class="btn btn-default btn-sm" onClick="pf_chart();">
      <i class="fa fa-refresh"></i> Chart 보기
    </button>
  </div>
</div>


<div id='grid_1' class='grid_div'>
  <div id="dg_1" style="width:100%; height:100%; " class="slick-grid"></div>
</div>

<div id='chart_1' class='grid_div ml5'>
</div>


<script type="text/javascript">
  topsearch.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};
  grid_1.ResizeInfo = {init_width:380, init_height:664, width_increase:0, height_increase:1};
  chart_1.ResizeInfo = {init_width:620, init_height:664, width_increase:1, height_increase:1};
</script>



<%@ page language="java" import="java.util.*, java.io.*, java.net.*, javax.xml.parsers.*, javax.xml.xpath.*, org.w3c.dom.*, org.xml.sax.*" %>
<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>

<% 
/**
  * @Class Name : WebService.jsp
  * @Description : SAP IF 웹서비스
  * @Modification Information
  * @
  * @  수정일        		 수정자                   수정내용
  * @ -------    --------    ---------------------------
  * @ 2015.04.06    김성한        최초 생성
  * @
  */

	// 변수 선언
	response.setCharacterEncoding("utf-8");
	String wstype = request.getParameter("WSTYPE");
	String eai = "";
	String url = "";
	String params = "";

	// EAI 주소 체크
	String domain = javax.servlet.http.HttpUtils.getRequestURL(request).toString();
	String[] sqlData = xgov.core.dao.XDAO.XmlSelect(request, "array", "sm", "SM_CODE_IFINFO", "R_SM_CODE_IFINFO_01", "EAI", "all", "˛", "¸").split("˛");
	if(domain.indexOf("epms.hec.co.kr") > 0)
		eai = sqlData[0];
	else
		eai = sqlData[1];

	// 거래처 등록 IF
	if(wstype.equals("1")) {
		url		= eai + "/ws/SAP_FI_01.EPMS.EPMS_FI_0002.webservice.provider:EPMS_FI_0002_WS_PROVIDER";
		params	= "<Input>"
				+ "<INDATA>" + request.getParameter("INDATA") + "</INDATA>"
				+ "</Input>";
	}
	// 프로젝트 WBS 등록 IF
	else if(wstype.equals("2")) {
		url		= eai + "/ws/SAP_CO_01.EPMS.CO_EPMS_0003.webservice.provider:CO_EPMS_0003_WS_PROVIDER";
		params	= "<CO_EPMS_0003>"
				+ "<BUSSNO>" + request.getParameter("BUSSNO") + "</BUSSNO>"
				+ "</CO_EPMS_0003>";
	}
	// 프로젝트 WBS 갱신 IF
	else if(wstype.equals("3")) {
		url		= eai + "/ws/SAP_CO_01.EPMS.CO_EPMS_0005.webservice.provider:CO_EPMS_0005_WS_PROVIDER";
		params	= "<CO_EPMS_0005>"
				+ "<BUSSNO>" + request.getParameter("BUSSNO") + "</BUSSNO>" 
				+ "<PSPID>" + request.getParameter("PSPID") + "</PSPID>"
				+ "</CO_EPMS_0005>";
	}
	// 프로젝트 WBS 계약관리 IF
	else if(wstype.equals("4")) {
		url		= eai + "/ws/SAP_CO_01.EPMS.CO_EPMS_0006.webservice.provider:CO_EPMS_0006_WS_PROVIDER";
		params	= "<CO_EPMS_0006>"
				+ "<BUSSNO>" + request.getParameter("BUSSNO") + "</BUSSNO>" 
				+ "<PSPID>" + request.getParameter("PSPID") + "</PSPID>" 
				+ "<POSID>" + request.getParameter("POSID") + "</POSID>" 
				+ "<ZVERN>" + request.getParameter("ZVERN") + "</ZVERN>"
				+ "</CO_EPMS_0006>";
	}
	// 전표 생성 IF
	else if(wstype.equals("5")) {
		url		= eai + "/ws/SAP_FI_01.EPMS.EPMS_FI_0004.webservice.provider:EPMS_FI_0004_WS_PROVIDER";
		params	= "<EPMS_FI_0004>"
				+ "<companyID>" + request.getParameter("companyID") + "</companyID>" 
				+ "<transDt>" + request.getParameter("transDt") + "</transDt>" 
				+ "<transNo>" + request.getParameter("transNo") + "</transNo>"
				+ "</EPMS_FI_0004>";
	}
	// 세금계산서 발행 IF
	else if(wstype.equals("6")) {
		url		= eai + "/ws/LEGACY_EPS_01.EPS_EPMS.EPS_EPMS_0001.webservice.provider:EPMS_EPS_0001_WS_PROVIDER";
		params	= "<EPS_EPMS_0001>"
				+ "<etaxNo>" + request.getParameter("etaxNo") + "</etaxNo>" 
				+ "<etaxSeq>" + request.getParameter("etaxSeq") + "</etaxSeq>" 
				+ "</EPS_EPMS_0001>";
	}
	// 세금계산서 조회 IF
	else if(wstype.equals("7")) {
		url		= eai + "/ws/LEGACY_EPS_01.EPS_EPMS.EPS_EPMS_0002.webservice.provider:EPMS_EPS_0002_WS_PROVIDER";
		params	= "<EPS_EPMS_0002>"
				+ "<TaxlnvoiceCode>" + request.getParameter("TaxlnvoiceCode") + "</TaxlnvoiceCode>" 
				+ "</EPS_EPMS_0002>";
	}
	// 세금계산서 삭제 IF
	else if(wstype.equals("8")) {
		url		= eai + "/ws/LEGACY_EPS_01.EPS_EPMS.EPS_EPMS_0003.webservice.provider:EPS_EPS_0002_WS_PROVIDER";
		params	= "<IF_EPS_PMIS_03>"
				+ "<TI_NO>" + request.getParameter("TI_NO") + "</TI_NO>" 
				+ "<EMP_ID>" + request.getParameter("EMP_ID") + "</EMP_ID>" 
				+ "</IF_EPS_PMIS_03>";
	}
	// 복수계좌 처리 IF
	else if(wstype.equals("9")) {
		url		= eai + "/ws/SAP_TR_01.EPMS.TR_EPMS_0004.ws:TR_EPMS_0004_WS_PROVIDER";
		params	= "<TR_EPMS_0004>"
				+ "<BUKRS>" + request.getParameter("BUKRS") + "</BUKRS>" 
				+ "<LIFNR>" + request.getParameter("LIFNR") + "</LIFNR>" 
				+ "<GJAHR>" + request.getParameter("GJAHR") + "</GJAHR>" 
				+ "<BELNR>" + request.getParameter("BELNR") + "</BELNR>" 
				+ "<BUZEI>" + request.getParameter("BUZEI") + "</BUZEI>" 
				+ "<BANKS>" + request.getParameter("BANKS") + "</BANKS>" 
				+ "<BANKL>" + request.getParameter("BANKL") + "</BANKL>" 
				+ "<ZZBANKN>" + request.getParameter("ZZBANKN") + "</ZZBANKN>" 
				+ "<KOINH>" + request.getParameter("KOINH") + "</KOINH>" 
				+ "<ERDAT>" + request.getParameter("ERDAT") + "</ERDAT>" 
				+ "<ERZET>" + request.getParameter("ERZET") + "</ERZET>" 
				+ "<ERNAM>" + request.getParameter("ERNAM") + "</ERNAM>" 
				+ "<AEDAT>" + request.getParameter("AEDAT") + "</AEDAT>" 
				+ "<AEZET>" + request.getParameter("AEZET") + "</AEZET>" 
				+ "<AENAM>" + request.getParameter("AENAM") + "</AENAM>" 
				+ "</TR_EPMS_0004>";
	}
	// MMS IF
	else if(wstype.equals("10")) {
		url		= eai + "/ws/IF_MMS_EPMS_01.webservice.provider:IF_MMS_EPMS_01_WS_PROVIDER";
		params	= "<IN_DATA>"
				+ "<MT_REFKEY>" + request.getParameter("MT_REFKEY") + "</MT_REFKEY>" 
				+ "<DATE_CLIENT_REQ>" + request.getParameter("DATE_CLIENT_REQ") + "</DATE_CLIENT_REQ>" 
				+ "<SUBJECT>" + request.getParameter("SUBJECT") + "</SUBJECT>" 
				+ "<CONTENT>" + request.getParameter("CONTENT") + "</CONTENT>" 
				+ "<CALLBACK>" + request.getParameter("CALLBACK") + "</CALLBACK>" 
				+ "<RECIPIENT_NUM>" + request.getParameter("RECIPIENT_NUM") + "</RECIPIENT_NUM>" 
				+ "</IN_DATA>";
	}
	// SMS IF
	else if(wstype.equals("11")) {
		url		= eai + "/ws/IF_SMS_EPMS_01.webservice.provider:IF_SMS_EPMS_01_WS_PROVIDER";
		params	= "<IN_DATA>"
				+ "<TRAN_PHONE>" + request.getParameter("TRAN_PHONE") + "</TRAN_PHONE>" 
				+ "<TRAN_CALLBACK>" + request.getParameter("TRAN_CALLBACK") + "</TRAN_CALLBACK>" 
				+ "<TRAN_STATUS>" + request.getParameter("TRAN_STATUS") + "</TRAN_STATUS>" 
				+ "<TRAN_DATE>" + request.getParameter("TRAN_DATE") + "</TRAN_DATE>" 
				+ "<TRAN_MSG>" + request.getParameter("TRAN_MSG") + "</TRAN_MSG>" 
				+ "<TRAN_REFKEY>" + request.getParameter("TRAN_REFKEY") + "</TRAN_REFKEY>" 
				+ "<TRAN_ETC1>" + request.getParameter("TRAN_ETC1") + "</TRAN_ETC1>" 
				+ "<TRAN_ETC2>" + request.getParameter("TRAN_ETC2") + "</TRAN_ETC2>" 
				+ "<TRAN_ETC3>" + request.getParameter("TRAN_ETC3") + "</TRAN_ETC3>" 
				+ "<TRAN_ETC4>" + request.getParameter("TRAN_ETC4") + "</TRAN_ETC4>" 
				+ "</IN_DATA>";
	}

	String xml	= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
				+ "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
				+ "<SOAP-ENV:Header xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"></SOAP-ENV:Header>"
				+ "<SOAP-ENV:Body>"
				+ params
				+ "</SOAP-ENV:Body>"
				+ "</SOAP-ENV:Envelope>";

	URL wsurl = new URL(url);
	HttpURLConnection hurlc = (HttpURLConnection) wsurl.openConnection();
	hurlc.setRequestMethod("POST");
	hurlc.setRequestProperty("Content-type", "text/xml; charset=utf-8");
	hurlc.setDoOutput(true);
	hurlc.setDoInput(true);
	hurlc.setUseCaches(false);
	hurlc.setDefaultUseCaches(false);
	hurlc.setConnectTimeout(300*1000);
	hurlc.setReadTimeout(300*1000);

	OutputStream os = hurlc.getOutputStream(); 
	os.write(xml.getBytes("utf-8"));
	os.flush();
	os.close();

	BufferedReader in = null;
	String buffer = "";
	String result = "";
	String value[] = new String[10];

	if(hurlc.getResponseCode() == 200) {
		in = new BufferedReader(new InputStreamReader(hurlc.getInputStream(), "utf-8"));
	} 
	else {
		in = new BufferedReader(new InputStreamReader(hurlc.getErrorStream(), "utf-8"));
	}
	while((buffer=in.readLine())!=null) { 
		result += buffer;
	}

	in.close();

	InputSource is = new InputSource(new StringReader(result));
	Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(is);
	XPath xpath = XPathFactory.newInstance().newXPath();

	if(wstype.equals("1")) {
		value[0] = xpath.compile("//EAI_STAT").evaluate(document);
		value[1] = xpath.compile("//EAI_MSG").evaluate(document);
		value[2] = xpath.compile("//E_ZRLET").evaluate(document);
		value[3] = xpath.compile("//E_ZMSEG").evaluate(document);
		out.println(value[0] + "\t" + value[1] + "\t" + value[2] + "\t" + value[3]);
	}
	else if(wstype.equals("2") || wstype.equals("3") || wstype.equals("4")) {
		value[0] = xpath.compile("//EAI_STAT").evaluate(document);
		value[1] = xpath.compile("//EAI_MSG").evaluate(document);
		value[2] = xpath.compile("//RTNFLAG").evaluate(document);
		value[3] = xpath.compile("//RTNMSG").evaluate(document);
		out.println(value[0] + "\t" + value[1] + "\t" + value[2] + "\t" + value[3]);
	}
	else if(wstype.equals("5")) {
		value[0] = xpath.compile("//BELNR").evaluate(document);
		value[1] = xpath.compile("//GJAHR").evaluate(document);
		value[2] = xpath.compile("//BUKRS").evaluate(document);
		value[3] = xpath.compile("//ZRLET").evaluate(document);
		value[4] = xpath.compile("//ZMSEG").evaluate(document);
		value[5] = xpath.compile("//EAI_STAT").evaluate(document);
		value[6] = xpath.compile("//EAI_MSG").evaluate(document);
		out.println(value[0] + "\t" + value[1] + "\t" + value[2] + "\t" + value[3] + "\t" + value[4] + "\t" + value[5] + "\t" + value[6]);
	}
	else if(wstype.equals("6")) {
		value[0] = xpath.compile("//EAI_STAT").evaluate(document);
		value[1] = xpath.compile("//EAI_MSG").evaluate(document);
		value[2] = xpath.compile("//ZRLET").evaluate(document);
		value[3] = xpath.compile("//ZMSEG").evaluate(document);
		value[4] = xpath.compile("//TI_NO").evaluate(document);
		value[5] = xpath.compile("//corp_cd").evaluate(document);
		value[6] = xpath.compile("//Issue_sys_id").evaluate(document);
		value[7] = xpath.compile("//IssueSystemKey").evaluate(document);
		out.println(value[0] + "\t" + value[1] + "\t" + value[2] + "\t" + value[3] + "\t" + value[4] + "\t" + value[5] + "\t" + value[6] + "\t" + value[7]);
	}
	else if(wstype.equals("7")) {
		value[0] = xpath.compile("//EAI_STAT").evaluate(document);
		value[1] = xpath.compile("//EAI_MSG").evaluate(document);
		value[2] = xpath.compile("//EAI_TRAN_ID").evaluate(document);
		value[3] = xpath.compile("//MasterID").evaluate(document);
		out.println(value[0] + "\t" + value[1] + "\t" + value[2] + "\t" + value[3]);
	}
	else if(wstype.equals("8") || wstype.equals("9") || wstype.equals("10") || wstype.equals("11")) {
		value[0] = xpath.compile("//EAI_STAT").evaluate(document);
		value[1] = xpath.compile("//EAI_MSG").evaluate(document);
		out.println(value[0] + "\t" + value[1]);
	}

	// 디버그 로그
	// out.println("\n\n\n"+xml+"\n\n\n"+result+"\n\n\n"+url);
%>

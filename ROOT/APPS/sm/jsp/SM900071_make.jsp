<%@ page language="java" contentType="application/json; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.sql.ResultSetMetaData"%>
<%@ page import="java.io.*"%>
<%

//프로그램 타입 구분
String r_code = request.getParameter("code");
String[] jspData ;
String[] htmlData ;

try {
  //jsp_src = xgov.core.dao.XDAO.XmlSelect3(request, "array", "sm", "SM900071", "GET_HTML", r_code, "all", _Row_Separate,_Col_Separate);
  jspData = xgov.core.dao.XDAO.XmlSelect(request, "array", "sm", "SM900071", "GET_HTML", r_code, "all", "˛", "¸").split("˛");
  htmlData = jspData[0].split("¸");

  String fileName = r_code+".jsp"; //생성할 파일명
  String fileDir = "/APPS/sm/jsp_dev"; //파일을 생성할 디렉토리
  String filePath = request.getRealPath(fileDir) + "/"; //파일을 생성할 전체경로
  filePath += fileName; //생성할 파일명을 전체경로에 결합

  // out.print(filePath);

  try{
    File f = new File(filePath); // 파일객체생성
    f.createNewFile(); //파일생성
    
    BufferedWriter output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f.getPath()), "UTF8"));

    output.write("<%");
    output.write(htmlData[0]);
    output.write("<%");
    output.write(htmlData[1]);

    //output.write(" ");

    output.write(htmlData[2]);
    output.close();

    //// 파일쓰기
    //FileWriter fw = new FileWriter(filePath); //파일쓰기객체생성
    //String data = new String(jsp_src.getBytes("8859_1"), "KSC5601");
    ////fw.write("< page language=\"java\" contentType=\"application/json; charset=utf-8\" pageEncoding=\"utf-8\"\>");
    ////fw.write("<@ taglib prefix=\"html\"  uri=\"/WEB-INF/tlds/mighty.tld\" >"); //파일에다 작성

    //fw.write(data); //파일에다 작성
    //fw.close(); //파일핸들 닫기

  }catch (IOException e) { 
    System.out.println(e.toString()); //에러 발생시 메시지 출력
  }
} catch (Exception e) {
  e.printStackTrace();
}

//out.print(jsp_src);

%>


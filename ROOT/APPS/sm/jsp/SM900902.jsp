<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<script type="text/javascript" src="/Theme/js/temp_common.js"></script>


<div id='topbar'>
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' pgmcode="SM900902"></html:authbutton>
		</span>
	</div>
</div>


<div class="x5-row">
  <div class="pull-left">
  	<button class="btn btn-default btn-sm" onClick="pf_setGroup1();">
	     <i class="fa fa-save"></i> COLS4, COLS1 그룹생성( 컬럼 순서 유지 )
		</button>

    <button class="btn btn-default btn-sm" onClick="pf_setGroup2();">
	     <i class="fa fa-save"></i> COLS4, COLS1 그룹생성( 컬럼 순서 재설정 )
		</button>

  	<button class="btn btn-default btn-sm" onClick="pf_resetGroup();">
	     <i class="fa fa-save"></i> 리셋그룹
		</button>

		<button class="btn btn-default btn-sm" onClick="pf_isGroup();">
	     <i class="fa fa-save"></i> 그룹 여부
		</button>

		<br>

		<button class="btn btn-default btn-sm" onClick="pf_CollapseGroup1();">
	     <i class="fa fa-save"></i> 그룹 축소
		</button>

		<button class="btn btn-default btn-sm" onClick="pf_ExpandGroup1();">
	     <i class="fa fa-save"></i> 그룹 확장
		</button>

		<button class="btn btn-default btn-sm" onClick="pf_CollapseGroup2();">
	     <i class="fa fa-save"></i> 1레벨 그룹 축소
		</button>

		<button class="btn btn-default btn-sm" onClick="pf_ExpandGroup2();">
	     <i class="fa fa-save"></i> 1레벨 그룹 확장
		</button>

		<br>

		<button class="btn btn-default btn-sm" onClick="pf_SetGroupFootVisible1();">
	     <i class="fa fa-save"></i> 그룹 합계 닫기
		</button>

		<button class="btn btn-default btn-sm" onClick="pf_SetGroupFootVisible2();">
	     <i class="fa fa-save"></i> 그룹 합계 열기
		</button>

		<button class="btn btn-default btn-sm" onClick="pf_SetGroupFootVisible3();">
	     <i class="fa fa-save"></i> COLS5 컬럼 그룹 합계 닫기
		</button>

		<button class="btn btn-default btn-sm" onClick="pf_SetGroupFootVisible4();">
	     <i class="fa fa-save"></i> COLS5 컬럼 그룹 합계 열기
		</button>

		<br>

		<button class="btn btn-default btn-sm" onClick="pf_SetCountIf1();">
	     <i class="fa fa-save"></i> COLS9 컬럼 값이 N 일때만 count 적용
		</button>

		<button class="btn btn-default btn-sm" onClick="pf_SetCountIf2();">
	     <i class="fa fa-save"></i> COLS9 컬럼 값이 Y 일때만 count 적용
		</button>

		<br>

		<button class="btn btn-default btn-sm" onClick="pf_SetGroupCustom1();">
	     <i class="fa fa-save"></i> COLS7 컬럼 사용자 정의 합계 ( COLS2 + COLS7 )
		</button>

		<button class="btn btn-default btn-sm" onClick="pf_SetGroupMaskValue1();">
	     <i class="fa fa-save"></i> COLS7 컬럼 그룹 합계 마스크 적용
		</button>

		<button class="btn btn-default btn-sm" onClick="pf_SetGroupMaskValue2();">
	     <i class="fa fa-save"></i> COLS2 컬럼 그룹 합계 마스크 적용
		</button>

		<br>

	  <button class="btn btn-default btn-sm" onClick="pf_copyToClipboard();">
	     <i class="fa fa-save"></i> 선택행 엑셀 클립보드
		</button>

	  <button class="btn btn-default btn-sm" onClick="pf_Fixed();">
	     <i class="fa fa-save"></i> Fixed
		</button>

	  <button class="btn btn-default btn-sm" onClick="pf_SetFooterRowVisibility(true);">
	     <i class="fa fa-save"></i> ShowFooterRowVisibility
		</button>

		<button class="btn btn-default btn-sm" onClick="pf_SetFooterRowVisibility(false);">
	     <i class="fa fa-save"></i> HideFooterRowVisibility
		</button>

  </div>
</div>

<div id='grid_1' class='grid_div'>
	<div id="dg_1" style="width:100%; height:100%; " class="slick-grid"></div>
</div>

<script type="text/javascript">
	topbar.ResizeInfo = {init_width:1000, init_height:35, anchor:{x1:1,y1:0,x2:1,y2:0}};
	grid_1.ResizeInfo = {init_width:1000, init_height:565, width_increase:1, height_increase:1};
</script>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
/**
 *<pre>
 * Statements
 *</pre>

 * @Class Name : SM900002.jsp
 * @Descriptio    Grid 정보관리
 * @Modification Information
 * @
 * @  수정일            수정자                   수정내용
 * @ -------    --------    ---------------------------
 * @ 2013.03.03    박영찬        최초 생성
 * @
 */
%>
<style>
  .wrap-btn-grids { padding-top: 2px; min-width: 100px; }
  .wrap-btn-grids .btn { margin-left: 6px; }
  #ta-sql { border: 1px solid #ccc; }
  .modal-lg { width: 90% !important; }
  .wr30 { width: 30% !important; }
  #grid-sql { height: 400px; }
  select.input-sm { height: 22px !important; padding: 1px 5px !important; font-size: 12px !important; }
</style>

<script src="/Mighty/3rd/underscore/underscore-min.js"></script>
<script src="/Mighty/3rd/codemirror/lib/codemirror.js"></script>
<link rel="stylesheet" href="/Mighty/3rd/codemirror/lib/codemirror.css">
<script src="/Mighty/3rd/codemirror/mode/sql/sql.js"></script>
<link rel="stylesheet" href="/Mighty/3rd/codemirror/addon/hint/show-hint.css" />
<script src="/Mighty/3rd/codemirror/addon/hint/show-hint.js"></script>
<script src="/Mighty/3rd/codemirror/addon/hint/sql-hint.js"></script>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap-theme.min.css">
<script type="text/javaScript" src="/Mighty/3rd/bootstrap/js/bootstrap.min.js"></script>

<div id='topsearch' class='option_box'>

  <div id='xbtns'>
    <html:authbutton id='buttons' grid='dg_1' pgmcode="SM900002"></html:authbutton>
  </div>
  <div id='userxbtns'>
  </div>

  <div class='option_label'>시스템</div>
  <div class='option_input_bg'>
    <html:select name="S_SYS_ID" id = "S_SYS_ID" comCodeYn="0" codeGroup="sm|SM_AUTH_SYS|W_SM0915_01" params="${companyCode}" dispStyle="1" selected="20" showTitle="true" titleCode="전체" titleValue='%'></html:select>
  </div>

  <label class='option_label w160'>프로그램 코드/이름</label>
  <div class='option_input_bg'>
    <input id='S_PGM_CODE' type='text' class='option_input_fix cache w200'>
  </div>

  <label class='option_label w90 new-grid'>New Grid Id</label>
  <div class='option_input_bg new-grid'>
    <input id='S_GRID_ID' type='text' class='option_input_fix w100'>
  </div>
  <div class='option_input_bg new-grid'>
    <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#sqlModal">SQL 에서 입력</button>
  </div>

  <label class='option_label w120 new-grid'>Update Tables</label>
  <div class='option_input_bg new-grid'>
    <select class="form-control input-sm" id="update-table"></select>
  </div>

</div>
<div id='topsearch2' class='option_box'>
  <label class='option_label w90' style='height: 78px;'>Grids</label>
  <div class="option_input_bg wrap-btn-grids" style='width: calc(100% - 150px); height: 78px;'>

  </div>
</div>


<div id='grid_2' class='grid_div'>
  <div id="dg_2" class="slick-grid"></div>
</div>

<div id='topoption' class='option_box2 ml5' >
  <label class='option_label w90'>SQL FILE</label>
  <div class='option_input_bg'>
    <input id='S_SQL_FILE' type='text' class='option_input_fix w120'>
  </div>

  <label class='option_label w90'>SQL ID</label>
  <div class='option_input_bg'>
    <input id='S_SQL_ID' type='text' class='option_input_fix w120'>
  </div>

  <label class='option_label w140'>UPDATE TABLE</label>
  <div class='option_input_bg'>
    <input id='S_UPDATE_TABLE' type='text' class='option_input_fix w200'>
  </div>

  <div class='option_input_bg pull-right'>
    <button type="button" id="btn-ff-modal" class="btn btn-success btn-xs" data-toggle="modal" data-target="#freeformModal">프리폼</button>
    <button type="button" id="btn-copy-modal" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#copyModal">Grid 복사</button>
    <button type="button" id="btn-del-modal" class="btn btn-danger btn-xs" >Grid 삭제</button>
    <button type="button" id="btn-upd-modal" class="btn btn-info btn-xs" data-toggle="modal" data-target="#updModal">Update Xml 생성</button>


    <!--
    <button class='btn btn-xs btn-default'>btn3</button>
    <button class='btn btn-xs btn-default'>btn4</button>
    <button class='btn btn-xs btn-default'>btn5</button>
    <button class='btn btn-xs btn-default'>btn6</button>
    <button class='btn btn-xs btn-default'>btn7</button>
    <button class='btn btn-xs btn-default'>btn8</button>
    <button class='btn btn-xs btn-default'>btn9</button>
    <button class='btn btn-xs btn-default'>btn10</button>
    -->
  </div>
</div>

<div id='grid_1' class='grid_div pl5'>
  <div id="dg_1" class="slick-grid"></div>
</div>

<div id="grid_devpropsrc" class="grid_div hide">
  <div id="dg_devpropsrc" class="slick-grid"></div>
</div>



</div> <!-- #pageLayout 밖에 Modal 놓기 위한 Trick -->

<!-- Modal -->
<div class="modal fade" id="sqlModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="sqlModalLabel">SQL & PARAMS</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-9">
            <div class="col-xs-12 sql-label">
              <label class="control-label">SQL & Params</label>
              <span class="">( SQL 을 수정하면 하단에 "수정 SQL 적용" 버튼이 활성화 됩니다.)</span>
            </div>CREATE OR REPLACE FUNCTION SBTEC.X_SEARCHC
            <div class="col-xs-12">
              <div class="tab-content wrap-textarea">
                <div class="tab-pane active" id="ta-sql">
                  <textarea id="grid-sql" class="form-control" placeholder="SQL 쿼리를 입력하세요."></textarea>
                </div>
                <div class="tab-pane" id="ta-grid">
                  <textarea id="grid-txt" readonly class="form-control" placeholder="Grid 구문이 표시됩니다."></textarea>
                </div>
                <div class="tab-pane" id="ta-js">
                  <textarea id="js-txt" readonly class="form-control" placeholder="js 구문이 표시됩니다."></textarea>
                </div>
                <div class="tab-pane" id="ta-xmls">
                  <textarea id="sql-xmlx" readonly class="form-control" placeholder="Sql 구문이 표시됩니다."></textarea>
                </div>
              </div>
            </div>
          </div><!-- .col-xs-9 -->
          <div class="col-xs-3 wrap-updtables">
            <div class="row mt5">
              <div class="col-xs-12">
                <label class="control-label">Params</label>
              </div>
              <div class="col-xs-12 wrap-params">
                <table id="sql-params" class="table">
                </table>
              </div>
            </div>

          </div><!-- .col-xs-3 -->
        </div><!-- .row -->
      </div><!-- .modal-body -->
      <div class="modal-footer">
        <div class="text-center">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="btn-modified-sql" class="btn btn-primary">수정 SQL 적용</button>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="freeformModal" tabindex="-1" role="dialog" aria-labelledby="freeformModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="freeformModalLabel">프리폼 HTML</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <label class='option_label w140'>FREEFORM SIZE</label>
            <div class='option_input_bg'>
              <input id='S_FREEFORM_SIZE' type='text' class='option_input_fix w100'>
            </div>
        </div><!-- .row -->

        <div class="row">
          <div class="col-xs-12">
              <div class="tab-content wrap-textarea">
                <div class="tab-pane active" id="ta-freeform">
                  <textarea id="freeform-html" class="form-control" style="height:400px;"></textarea>
                </div>
              </div>
            </div>
        </div><!-- .row -->
      </div><!-- .modal-body -->
      <div class="modal-footer">
        <div class="text-center">
          <button type="button" id="btn-freeformClose" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="btn-freeformModal" class="btn btn-primary">HTML 생성</button>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="copyModal" tabindex="-1" role="dialog" aria-labelledby="copyModalLabel">
  <div class="modal-dialog modal-lg wr30" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="freeformModalLabel">그리드 복사</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <label class='option_label w140'>프로그램 코드</label>
          <div class='option_input_bg'>
            <input id='S_TO_PGMCODE' type='text' class='option_input_fix w100'>
          </div>
        </div><!-- .row -->
        <div class="row">
          <label class='option_label w140'>그리드 ID</label>
          <div class='option_input_bg'>
            <input id='S_TO_GRIDID' type='text' class='option_input_fix w100'>
          </div>
        </div><!-- .row -->
      </div><!-- .modal-body -->
      <div class="modal-footer">
        <div class="text-center">
          <button type="button" id="btn-copyClose" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="btn-copyModal" class="btn btn-primary">복사</button>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="updModal" tabindex="-1" role="dialog" aria-labelledby="copyModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="freeformModalLabel">Xml Update 정보 생성</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-xs-12">
              <div class="tab-content wrap-textarea">
                <div class="tab-pane active" id="ta-freeform">
                  <textarea id="upd-xml-txt" class="form-control" style="height:400px;"></textarea>
                </div>
              </div>
            </div>
        </div><!-- .row -->

      </div><!-- .modal-body -->
      <div class="modal-footer">
        <div class="text-center">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="btn-updModal" class="btn btn-primary">복사</button>
        </div>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  topsearch.ResizeInfo  = {init_width:1000, init_height: 33, width_increase:1, height_increase:0};
  topsearch2.ResizeInfo = {init_width:1000, init_height: 78, width_increase:1, height_increase:0};
  grid_2.ResizeInfo     = {init_width: 360, init_height:590, width_increase:0, height_increase:1};
  topoption.ResizeInfo  = {init_width: 640, init_height: 33, width_increase:1, height_increase:0};
  grid_1.ResizeInfo     = {init_width: 640, init_height:557, width_increase:1, height_increase:1};
  grid_devpropsrc.ResizeInfo = {init_width:640, init_height:665, width_increase:1, height_increase:1};
</script>

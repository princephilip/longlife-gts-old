
<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2'>
	<div class='option_label w100'>통합검색</div>
	<div id='di_11' class='option_input_bg'>
		<input id='S_FIND' type='text' class='option_input_fix w200'>
	</div>
	<div class='option_label w100'>사용여부</div>
	<div style="position:relative;"  id="di_12" class="option_input_bg">
		<select id='S_USE_YESNO' class="w70">
			<option value='%'>전체</option>
			<option value='Y' selected>사용</option>
			<option value='N'>중지</option>
		</select>
	</div>
	<div class='option_label w100'>사용자구분</div>
	<div style="position:relative;"  id="di_12" class="option_input_bg">
		<select id='S_USER_TAG'  class="w80"></select>
	</div>

	<div class='option_label w100'>개인권한</div>
	<div style="position:relative;"  id="di_12" class="option_input_bg">
		<select id='S_TYPE' class="w70">
			<option value='1'>전체</option>
			<option value='2'>대상</option>
		</select>
	</div>

	<div id='xbtns'>
		<html:authbutton id='buttons' grid='dg_1' pgmcode="SM0915"></html:authbutton>
	</div>

	<div id='userxbtns' style='height:23px;'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<button type="button" id='auth_copy' class="btn btn-warning btn-xs" onClick="pf_UserFind();">권한 복사</button>
		</span>
	</div>
</div>

<div id='grid_1' class='grid_div mr5 has_title' >
	<label class='sub_title'>사용자 리스트</label>
	<div id="dg_1" class="slick-grid"></div>
</div>

<div id='grid_2' class='grid_div mr5 has_title' >
	<label class='sub_title'>프로그램 권한 설정
			<div style="float:right;margin-top:4px;">
				<div class='option_label w100'>메뉴명</div>
				<div id='di_11' class='option_input_bg'>
					<input id='S_FIND2' type='text' class='option_input_fix w150'>
				</div>
				<div class='option_label'>시스템</div>
				<div class='option_input_bg'>
					<html:select name="S_SYS_ID" id = "S_SYS_ID" comCodeYn="0" codeGroup="sm|SM_AUTH_SYS|W_SM0915_01" params="${companyCode}" dispStyle="1" selected="20" showTitle="true" ></html:select>
				</div>
			</div>
<!--
		<div id='xbtns' class='child_button float_right'>
			<html:authchildbutton id='buttons' grid='dg_2' append='true' insert='false' duplicate='false' delete='true' excel='false' retrieve='false' ></html:authchildbutton>
		</div>
		<div style="float:right;margin-top:4px;">
			<button type="button" id='btn_deselect' class="btn btn-danger btn-xs" onClick="uf_proj_del();">삭제</button>
		</div>
-->
	</label>





	<div id="dg_2" class="slick-grid"></div>
</div>

<!--
<div id='grid_3' class='grid_div has_title ml5'>
	<label class='sub_title'>프로그램 선택

		<div style="float:right;margin-top:4px;">
            <button type="button" id='btn_select' class="btn btn-success btn-xs" onClick="uf_proj_add();">추가</button>
		</div>
		<div style="float:right;margin-top:0px;">
			<div class='option_label' >프로그램 검색</div>
			<div class="option_input_bg" >
				<input type="text" id="S_PROJ_FIND" class="detail_input_fix W250">
			</div>
		</div>
    </label>

	<div id="dg_3" style="width:100%; height:100%; " class="slick-grid"></div>
</div>
-->

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:33 , width_increase:1, height_increase:0};
	grid_1.ResizeInfo 	 = {init_width:330, init_height:655, width_increase:1, height_increase:1};
	grid_2.ResizeInfo  	 = {init_width:670, init_height:655,  width_increase:0, height_increase:1};
	//grid_3.ResizeInfo  	 = {init_width:610, init_height:655,  width_increase:0, height_increase:1};
</script>
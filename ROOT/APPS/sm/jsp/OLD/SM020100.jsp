<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>

<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<div id='topbar' class='option_box1'>
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1'></html:authbutton>
		</span>
	</div>
</div>


<div id='grid_1'>	
	<div id="dg_1" class="slick-grid"></div>
</div>

<script type="text/javascript">
	topbar.ResizeInfo = {init_width:1000, init_height:33, anchor:{x1:1,y1:0,x2:1,y2:0}};
	grid_1.ResizeInfo = {init_width:1000, init_height:667, width_increase:1, height_increase:1};
</script>
﻿<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>               
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>                                          
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>                                      
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box'>
	
	<div id='xbtns' style='height:23px;'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true"></html:authbutton>
		</span>
	</div>
	
	<div class='option_label_left w120'>부서검색</div> 
	<div id='di_11' class='option_input_bg w120'>
		<input id='S_DEPTCODE' type='text' class='option_input'>
	</div> 
	
	<div class='option_label w120'>사용여부</div> 
	<div id='di_12' class='option_input_bg w30'>
		<select id='S_USEYN' ></select>
	</div> 
</div>

<div id='grid_1' class='grid_div mr5'>
	<div id="dg_1" class="slick-grid"></div>
</div>

<div id='freeform' class='detail_box'>
		<div class='detail_label_left_check w120'>부서코드</div> 
		<div id='di_DEPT_CODE' class='detail_input_bg w120'>
			<input id='DEPT_CODE' type='text' class='detail_input'>
		</div> 
		<div class='detail_label_check w120'>부서명</div> 
		<div id='di_DEPT_NAME' class='detail_input_bg w120'>
			<input id='DEPT_NAME' type='text' class='detail_input'>
		</div> 	
		<div class='detail_label w120'>부서약어명</div> 
		<div id='di_DEPT_SNAME' class='detail_input_bg w120'>
			<input id='DEPT_SNAME' type='text' class='detail_input'>
		</div> 	
	<div class='detail_line'></div> 
		<div class='detail_label_left w120'>부서영문명</div> 
			<div id='di_DEPT_ENAME' class='detail_input_bg w120'>
		<input id='DEPT_ENAME' type='text' class='detail_input'>
		</div> 
		<div class='detail_label w120'>부서영문약어명</div> 
		<div id='di_DEPT_ESNAME' class='detail_input_bg w120'>
			<input id='DEPT_ESNAME' type='text' class='detail_input'>
		</div> 
		<div class='detail_label w120'>조직표기구</div> 
		<div id='di_ORG_DISP_YN' class='detail_input_bg w30'>
			<input id='ORG_DISP_YN' type='checkbox' checked data-labelPosition="left"  data-label="" />
			<!--<select id='ORG_DISP_YN' ></select>-->
		</div> 
	<div class='detail_line'></div> 
		<div class='detail_label_left w120'>부서구분</div> 
		<div id='di_DEPT_DIV' class='detail_input_bg w120'>
			<select id='DEPT_DIV' ></select>
		</div> 	
		<div class='detail_label w120'>상위부서</div> 
		<div id='di_UPPER_DEPT_CODE' class='detail_input_bg w250'>
			<input id='UPPER_DEPT_CODE' type='text' class='detail_input_fix w60'><img id='btn_upperdeptcodefind' src='/Theme/images/btn/btn_find.gif'/>
			<input id='UPPER_DEPT_NAME' type='text' class='detail_input_fix w120'>
		</div> 	
	<div class='detail_line'></div> 
		<div class='detail_label_left w120'>본부실구분</div> 
		<div id='di_BONBOO_CODE' class='detail_input_bg w120'>
			<select id='BONBOO_CODE' ></select>
		</div> 
		<div class='detail_label w120'>부팀구분</div> 
		<div id='di_SIL_CODE' class='detail_input_bg w120'>
			<select id='SIL_CODE' ></select>
		</div> 
	<div class='detail_line'></div> 
		<div class='detail_label_left w120'>공사종류</div> 
		<div id='di_DEPT_TYPE_DIV' class='detail_input_bg w120'>
			<select id='DEPT_TYPE_DIV' ></select>
		</div> 
		<div class='detail_label w120'>공사차수</div> 
		<div id='di_PROJ_DEGREE_NUM' class='detail_input_bg w120'>
			<input id='PROJ_DEGREE_NUM' type='text' class='detail_amt'>
		</div> 
		<div class='detail_label w120'>현장순서</div> 
		<div id='di_PROJ_SORT_ORDER' class='detail_input_bg w120'>
			<input id='PROJ_SORT_ORDER' type='text' class='detail_amt'>
		</div> 
	<div class='detail_line'></div> 
		<div class='detail_label_left w120'>원가구분</div> 
		<div id='di_COST_TAG' class='detail_input_bg w120'>
			<select id='COST_TAG' ></select>
		</div> 		
		<div class='detail_label w120'>원가코드</div> 
		<div id='di_COST_CODE' class='detail_input_bg w120'>
			<select id='COST_CODE' ></select>
		</div> 
	<div class='detail_line'></div> 
		<div class='detail_label_left w120'>원가부서</div> 
		<div id='di_COST_DEPT_CODE' class='detail_input_bg w250'>
			<input id='COST_DEPT_CODE' type='text' class='detail_input_fix w60'><img id='btn_acntcostdeptfind' src='/Theme/images/btn/btn_find.gif'/>
			<input id='COST_DEPT_NAME' type='text' class='detail_input_fix w120-'>
		</div> 
		<div class='detail_label w120'>회계원가부서코드</div> 
		<div id='di_COSTDEPT_CODE' class='detail_input_bg w250'>
			<input id='COSTDEPT_CODE' type='text' class='detail_input_fix w60' ><img id='btn_acntcostdeptfind' src='/Theme/images/btn/btn_find.gif'/>
			<input id='COSTDEPT_NAME' type='text' class='detail_input_fix w120' >
		</div>
	<div class='detail_line'></div> 
		<div class='detail_label_left w120'>총괄사업코드</div> 
		<div id='di_BUSINESS_CODE' class='detail_input_bg w120'>
			<select id='BUSINESS_CODE'></select>
		</div> 
		<div class='detail_label w120'>사업부코드</div> 
		<div id='di_DEPT_GROUP' class='detail_input_bg w120'>
			<select id='DEPT_GROUP'></select>
		</div> 
		<div class='detail_label w120'>제조구분</div> 
		<div id='di_MANUFAC_DIV' class='detail_input_bg w120'>
			<select id='MANUFAC_DIV' ></select>
		</div> 
	<div class='detail_line'></div> 
		<div class='detail_label_left w120'>사업장코드</div> 
		<div id='di_OFFICE_CODE' class='detail_input_bg w120'>
			<select id='OFFICE_CODE' ></select>
		</div> 
		<div class='detail_label w120'>세무사업장코드</div> 
		<div id='di_TAX_COMPANY_CODE' class='detail_input_bg w120'>
			<select id='TAX_COMPANY_CODE' ></select>
		</div> 
		<div class='detail_label w120'>관할세무서</div> 
		<div id='di_TAX_OFFICE_CODE' class='detail_input_bg w120'>
			<select id='TAX_OFFICE_CODE' ></select>
		</div> 	
	<div class='detail_line'></div> 
		<div class='detail_label_left w120'>예산유무</div> 
		<div id='di_BUDGET_CONTROLTAG' class='detail_input_bg w120'>
			<select id='BUDGET_CONTROLTAG' ></select>
		</div> 
		<div class='detail_label w120'>과세구분</div> 
		<div id='di_TAX_DIV' class='detail_input_bg w120'>
			<select id='TAX_DIV' ></select>
		</div> 
		<div class='detail_label w120'>회계처리종료일</div> 
		<div id='di_ACC_END_DATE' class='detail_input_bg w120'>
			<input id='ACC_END_DATE' type='text' class='detail_date'>
		</div> 
	<div class='detail_line'></div> 	
		<div class='detail_label_left w120'>급여계정</div> 
		<div id='di_PAY_ACNT_CODE' class='detail_input_bg w250'>
			<input id='PAY_ACNT_CODE' type='text' class='detail_input_fix w60'><img id='btn_payacntcodefind' src='/Theme/images/btn/btn_find.gif'/>
			<input id='PAY_ACNT_NAME' type='text' class='detail_input_fix w120-'>
		</div> 
		<div class='detail_label w120'>상여계정</div> 
		<div id='di_BONUS_ACNT_CODE' class='detail_input_bg w250'>
			<input id='BONUS_ACNT_CODE' type='text' class='detail_input_fix w60'><img id='btn_bonusacntcodefind' src='/Theme/images/btn/btn_find.gif'/>
			<input id='BONUS_ACNT_NAME' type='text' class='detail_input_fix w120-'>
		</div> 
	<div class='detail_line'></div> 
		<div class='detail_label_left w120'>전도금계좌</div> 
		<div id='di_PROJ_DEPOSIT_NO' class='detail_input_bg w300'>
			<input id='PROJ_DEPOSIT_NO' type='text' class='detail_input_fix w120'><img id='btn_payacntcodefind' src='/Theme/images/btn/btn_find.gif'/>
			<input id='PROJ_DEPOSIT_NAME' type='text' class='detail_input_fix w120'>
		</div> 		
	<div class='detail_line'></div> 
		<div class='detail_label_left w120'>지역구분</div> 
		<div id='di_AREA_DIV' class='detail_input_bg w150'>
			<select id='AREA_DIV' ></select>
		</div> 
		<div class='detail_label w120'>해외현장구분</div> 
		<div id='di_FC_DEPT' class='detail_input_bg w120'>
			<select id='FC_DEPT' ></select>
		</div> 
		<div class='detail_label w120'>국가코드</div> 
		<div id='di_NATION_CD' class='detail_input_bg w120'>
			<select id='NATION_CD' ></select>
		</div> 
	<div class='detail_line'></div> 
		<div class='detail_label_left w120'>4대보험EDI번호</div> 
		<div id='di_EDI_MANAGE_NO' class='detail_input_bg w120'>
			<input id='EDI_MANAGE_NO' type='text' class='detail_input'>
		</div> 
		<div class='detail_label w120'>주민세납부처</div> 
		<div id='di_RESIDENT_PAY_PLACE' class='detail_input_bg w120'>
			<input id='RESIDENT_PAY_PLACE' type='text' class='detail_input'>
		</div> 
		<div class='detail_label w120'>식대통장별도구분</div> 
		<div id='di_FOOD_DEPOSIT_YN' class='detail_input_bg w120'>
			<select id='FOOD_DEPOSIT_YN'></select>
		</div> 
	<div class='detail_line'></div> 
		<div class='detail_label_left w120'>사용유무</div> 
		<div id='di_USING_TAG' class='detail_input_bg w120'>
			<select id='USING_TAG' ></select>
		</div> 
		<div class='detail_label w120'>정렬순서</div> 
		<div id='di_SORT_ORDER' class='detail_input_bg w120'>
			<input id='SORT_ORDER' type='text' class='detail_amt w120'>
		</div> 
		<div class='detail_label w120'>인원일보순서</div> 
		<div id='di_TO_SORT_ORDER' class='detail_input_bg w120'>
			<input id='TO_SORT_ORDER' type='text' class='detail_amt'>
		</div> 
	<div class='detail_line'></div> 
		<div class='detail_label_left w120'>최초등록일</div> 
		<div id='di_FIRST_REGIST_DATE' class='detail_input_bg w120'>
			<input id='FIRST_REGIST_DATE' type='text' class='detail_date'>
		</div> 		
		<div class='detail_label w120'>사용종료일</div> 
		<div id='di_TERMINATE_DATE' class='detail_input_bg w120'>
			<input id='TERMINATE_DATE' type='text' class='detail_date'>
		</div> 
</div>

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:35, width_increase:1, height_increase:0};
	grid_1.ResizeInfo = {init_width:150, init_height:630, width_increase:1, height_increase:1};
	freeform.ResizeInfo = {init_width:850, init_height:419, width_increase:0, height_increase:0};

	//700-35
	//폭은 1000 기준 높이는 700 기준
	//25*21 + 1*20 + 2+2(상하단)
</script>
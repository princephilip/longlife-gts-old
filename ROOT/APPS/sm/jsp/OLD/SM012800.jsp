﻿<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>


<div id='topsearch' class='option_box2'>      
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons'></html:authbutton>		
		</span>
	</div>

	<div id='userxbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			  <html:button name='sourcecopy' id='sourcecopy' type='button' icon='ui-icon-plus' desc="자료생성" onClick="pf_create_HolyDate();"></html:button>
		</span>
	</div>

	<div class='option_label'>기준년도</div>
	<div class='option_input_bg' id='S_SEARCH_DATE'>
		<html:basicdate id="dateimages" classType="search" dateType="year" moveBtn="true" readOnly="true"></html:basicdate>
	</div>
</div>


<div id='grid_1' class='grid_div mr5'>
	<div id="dg_1" class="slick-grid"></div>
</div>

<div id='grid_2' class='grid_div'>
	<div id="dg_2" class="slick-grid"></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:33, anchor:{x1:1,y1:0,x2:1,y2:0}};
	
	grid_1.ResizeInfo = {init_width:500, init_height:667, width_increase:0.5, height_increase:1};
	grid_2.ResizeInfo = {init_width:500, init_height:667, width_increase:0.5, height_increase:1};
	

//this is another way
function itemHasFocus(id)
{
    var output = false;
    console.log(id);
    //loop for all fields in the form
    $.each($('#'+id+' :input'), function(i,v)
    {
       console.log($(this).is(":focus")); //check item has focus
       if ($(this).is(":focus"))
       {
         output = true;
         return false; //return false skips out of the loop
       }
    });
    return output;
}
</script>
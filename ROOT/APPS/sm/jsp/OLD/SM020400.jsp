<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>

<div id='topsearch' class='option_box2'>
	<div class='option_label w120'>사용자검색</div> 
	<div id='di_11' class='option_input_bg w300'>
		<input id='S_MENU_GROUP' type='text' class='option_input'> 
	</div> 
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1'></html:authbutton>
		</span>
	</div>
</div>

<div id='grid_1'>	
	<div id="dg_1" class="slick-grid"></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};
	grid_1.ResizeInfo = {init_width:1000, init_height:667, width_increase:1, height_increase:1};
</script>
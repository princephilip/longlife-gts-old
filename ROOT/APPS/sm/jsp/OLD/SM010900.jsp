<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box'>
	<div id='xbtns' style='height:23px;'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true"></html:authbutton>
		</span>
	</div>
	
	<div class='option_label_left'>찾 기</div>
	<div class='detail_input_bg'>
		<input type="text" id='S_SEARCH' class='option_input_fix' size="15"/>
	</div>
</div>

<div id='freeform' class='option_box mr5'>
	<div class='detail_label_left_check w100'>회사코드</div>
		<div id='' class='option_input_bg'>
			<input id='COMPANY_CODE' type='text' class='option_input_fix' style='width:130px'/>
		</div>
	<div class='detail_label w100'>사업장코드</div>
		<div id='' class='option_input_bg'>
			<input id='OFFICE_CODE' type='text' class='option_input_fix' style='width:130px'/>
		</div>
	<div class='detail_label w100'>사업장명</div>
		<div id='' class='option_input_bg'>
			<input id='OFFICE_NAME' type='text' class='option_input_fix' style='width:140px'/>
		</div>
	<div class='detail_line'></div>
	<div class='detail_label_left w100'>사업자번호</div>
		<div id='' class='option_input_bg'>
			<input id='COMP_NO' type='text' class='option_input_fix' style='width:130px'/>
		</div>
	<div class='detail_label w100'>대표자명</div>
		<div id='' class='option_input_bg'>
			<input id='OWNER' type='text' class='option_input_fix' style='width:130px'/>
		</div>
	<div class='detail_label w100'>사업장 단축명</div>
		<div id='' class='option_input_bg'>
			<input id='OFFICES_NAME' type='text' class='option_input_fix' style='width:140px'/>
		</div>
	<div class='detail_line'></div>
	<div class='detail_label_left w100'>업태</div>
		<div id='' class='option_input_bg'>
			<input id='CONDITION' type='text' class='option_input_fix' style='width:130px'/>
		</div>
	<div class='detail_label w100'>업종</div>
		<div id='' class='option_input_bg'>
			<input id='CATEGORY' type='text' class='option_input_fix' style='width:130px'/>
		</div>
	<div class='detail_label w100'>법인번호</div>
		<div id='' class='option_input_bg'>
			<input id='COMPANY_REGNO' type='text' class='option_input_fix' style='width:140px'/>
		</div>
	<div class='detail_line'></div>
	<div class='detail_label_left w100'>우편번호</div>
		<div id='' class='option_input_bg'>
			<input id='ZIP' type='text' class='option_input_fix' style='width:130px'/>
		</div>
	<div class='detail_label w100'>주소</div>
		<div id='' class='option_input_bg'>
			<input id='ADDR' type='text' class='option_input_fix' style='width:184px'/>
			<input id='ADDR2' type='text' class='option_input_fix' style='width:184px'/>
		</div>
	<div class='detail_line'></div>
		<div class='detail_label_left w100'>회기</div>
		<div id='' class='option_input_bg'>
			<input id='TERM' type='text' class='option_input_fix' style='width:130px'/>
		</div>
	<div class='detail_label w100'>전표마감일</div>
		<div id='' class='option_input_bg'>
			<input id='CLOSE_DATE' type='text' class='option_input_fix' style='width:130px'/>
		</div>
	<div class='detail_label w100'>관할세무서</div>
		<div id='' class='option_input_bg'>
			<input id='TAX_OFFICE_NAME' type='text' class='option_input_fix' style='width:140px'/>
		</div>
	<div class='detail_line'></div>
	<div class='detail_label_left w100'>전화</div>
		<div id='' class='option_input_bg'>
			<input id='TEL' type='text' class='option_input_fix' style='width:130px'/>
		</div>
	<div class='detail_label w100'>회계최초일</div>
		<div id='' class='option_input_bg'>
			<input id='START_DATE' type='text' class='option_input_fix' style='width:130px'/>
		</div>
	<div class='detail_label w100'>폐업일자</div>
		<div id='' class='option_input_bg'>
			<input id='END_DATE' type='text' class='option_input_fix' style='width:140px'/>
		</div>
	<div class='detail_line'></div>
	<div class='detail_label_left w100'>은행코드</div>
		<div id='' class='option_input_bg'>
			<input id='BANK_CODE' type='text' class='option_input_fix' style='width:40px'/>
			<input id='S_DEPT_CODE' type='text' class='option_input_fix' style='width:78px'/>
		</div>
	<div class='detail_label w100'>계좌번호</div>
		<div id='' class='option_input_bg'>
			<input id='DEPOSIT_NO' type='text' class='option_input_fix' style='width:130px'/>
		</div>
	<div class='detail_label w100'>설립일자</div>
		<div id='' class='option_input_bg'>
			<input id='ESTAB_DATE' type='text' class='option_input_fix' style='width:140px'/>
		</div>
	<div class='detail_line'></div>
	<div class='detail_label_left w100'>대표자주민번호</div>
		<div id='' class='option_input_bg'>
			<input id='OWNER_NO' type='text' class='option_input_fix' style='width:130px'/>
		</div>
	<div class='detail_label w100'>집계구분Y/N</div>
		<div id='' class='option_input_bg'>
			<input id='SUM_TAG' type='text' class='option_input_fix' style='width:130px'/>
		</div>
	<div class='detail_label w100'></div>
		<div id='' class='option_input_bg'>
			<input id='' type='text' class='option_input_fix' style='width:140px'/>
		</div>
	<div class='detail_line'></div>
</div>

<div id='grid_2' class='option_box' >
	<div id='grid_21' style='width:20px;height:206px;border:2px solid #EAEAEA;border-radius:5px;text-align:center;float:left;line-height:90px;background-color:#E6FFFF'>비고</div>
	
	<div id='grid_22' class='grid_div' style='line-height:210px;'>
		<!-- <textarea id='s_remarks' rows="14" cols="40" name="s_remarks"></textarea> -->
		<input type='text' id='REMARK' class='detail_input' style='width:200px'/>
	</div>
</div>

<div id='grid_1' class='grid_div'>
	<div id="dg_1" class="slick-grid"></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:35, width_increase:1, height_increase:0};
	freeform.ResizeInfo = {init_width:755, init_height:217, width_increase:0, height_increase:0};
	grid_1.ResizeInfo = {init_width:1000, init_height:413, width_increase:1, height_increase:1};
	grid_2.ResizeInfo = {init_width:245, init_height:217, width_increase:1, height_increase:0};
	grid_22.ResizeInfo = {init_width:221, init_height:206, width_increase:1, height_increase:0};
	REMARK.ResizeInfo = {init_width:330, init_height:40, width_increase:1, height_increase:0};
	 
</script>
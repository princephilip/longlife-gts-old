<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>

<div id='grid'>
	<div id='topbar' style='height:35px;'>
		<div id='xsearch'></div>
		<div id='xbtns'></div>
	</div>
	<div id='grid_default' style='width:100%;height:645px;'></div>
</div>

<script type="text/javascript">
	grid.ResizeInfo = {init_width:1000, init_height:680, width_increase:1, height_increase:1};
	topbar.ResizeInfo = {init_width:1000, init_height:35, anchor:{x1:0,y1:0,x2:0,y2:0}};
	grid_default.ResizeInfo = {init_width:1000, init_height:645, width_increase:1, height_increase:1};
</script>
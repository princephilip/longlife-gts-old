<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
/**
 *<pre>
 * Statements
 *</pre>
 
 * @Class Name : SM0815.jsp
 * @Descriptio	 게시판권한관리
 * @Modification Information
 * @
 * @  수정일        		 수정자                   수정내용
 * @ -------    --------    ---------------------------
 * @ 2014.12.28    박영찬        최초 생성
 * @
 */
%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2'>
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1'></html:authbutton>		
		</span>	
	</div>
</div>

<div id='grid_list' class='grid_div'>
	<%= xgov.core.util.XHtmlUtil.RealGrid("dg_list", request.getHeader("User-Agent"),"0","N","N") %>
</div>

<div id='grid_1' class='grid_div ml5'>
	<%= xgov.core.util.XHtmlUtil.RealGrid("dg_1", request.getHeader("User-Agent"),"1","N","Y") %>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};
	grid_list.ResizeInfo = {init_width:680, init_height:664, width_increase:0, height_increase:1};
	grid_1.ResizeInfo = {init_width:320, init_height:664, width_increase:1, height_increase:1};
</script>
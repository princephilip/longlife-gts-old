<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>

<div id='topbar'>
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1'></html:authbutton>
		</span>
	</div>
	<div id='userxbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			  <html:button name='sourcecopy' id='sourcecopy' type='button' icon='ui-icon-copy' desc="소스복사" onClick="pf_PgmFind();"></html:button>
		</span>
	</div>
</div>


<div id='topsearch' class='option_box'>
	<div class='option_label_left'>시스템</div>
	<div id='di_11' class='option_input_bg'>
		<html:select name="S_SYS_ID" id = "S_SYS_ID" comCodeYn="0" codeGroup="sm|CodeCom|SM_AUTH_SYS_R01" params = "" dispStyle="1" selected="20" titleCode="선택"></html:select>
	</div>
	<div class='option_label'>프로그램</div>
	<div id='di_12' class='option_input_bg'>
		<input id='S_PGM_CODE' type='text' class='option_input'>
	</div>
	<div class='option_label'>일괄설정</div>
	<div id='di_13' class='option_input_bg'>
	  <input id='chk_SHOW_TAG' type='checkbox' data-label='보이기' />
	  <input id='chk_MOVE_BTN_TAG' type='checkbox' data-label='이동버튼' />
	  <input id='chk_UPDATE_TAG' type='checkbox' data-label='저장' />
	  <input id='chk_RETRIEVE_TAG' type='checkbox' data-label='조회' />
	  <input id='chk_INSERT_TAG' type='checkbox' data-label='삽입' />
	  <input id='chk_APPEND_TAG' type='checkbox' data-label='추가' />
	  <input id='chk_DUPLICATE_TAG' type='checkbox' data-label='복제' />
	  <input id='chk_DELETE_TAG' type='checkbox' data-label='삭제' />
	  <input id='chk_PRINT_TAG' type='checkbox' data-label='출력' />
	  <input id='chk_EXCEL_TAG' type='checkbox' data-label='엑셀' />
	  <input id='chk_CLOSE_TAG' type='checkbox' data-label='닫기' />
	  <input id='chk_WEB_TAG' type='checkbox' data-label='웹' />
	  <input id='chk_DEVELOPED_TAG' type='checkbox' data-label='개발완료' />
	  <input id='chk_HELP_TAG' type='checkbox' data-label='도움말' />
	</div>
</div>

<div id='grid_1'>	
	<div id="dg_1" class="slick-grid"></div>
</div>

<script type="text/javascript">
	topbar.ResizeInfo = {init_width:1000, init_height:33, anchor:{x1:1,y1:0,x2:1,y2:0}};
	topsearch.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};
	grid_1.ResizeInfo = {init_width:1000, init_height:630, width_increase:1, height_increase:1};
	di_11.ResizeInfo = {init_width:135,  init_height:0, anchor:{x1:0,y1:0,x2:0,y2:0}};
	di_12.ResizeInfo = {init_width:150,  init_height:0, anchor:{x1:0,y1:0,x2:0,y2:0}};
	di_13.ResizeInfo = {init_width:350,  init_height:0, anchor:{x1:1,y1:0,x2:1,y2:0}};
	//S_SYS_ID.ResizeInfo = {init_width:100,  init_height:0, anchor:{x1:0,y1:0,x2:0,y2:0}};
</script>
﻿<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2'>
	기준년도, 년도찾기, 실행버튼
	<div id='xbtns' style='height:23px;'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true"></html:authbutton>
		</span>
	</div>
</div>

<div id='grid' style='border:0px solid blue;'>
	<div id='grid_1_out' style='float:left;width:200px;height:100%;border:0px solid green;'>
		<div id='grid_1' class='grid_div'>
	<div id="dg_1" class="slick-grid"></div>
</div>

	</div>
	<div id='grid_2_out' style='float:left;width:800px;height:100%;border:0px solid red;padding-left:0px;'>
		<div id='grid_2' style='border:0px solid blue;padding-bottom:0px;'>grid_2</div>
		<div id='dgtab1' style='border:0px solid green;padding-top:0px;'>TAB</div>
		<div id='grid_3' style='border:0px solid green;padding-top:0px;'>grid_3</div>
		<div id='grid_4' style='display:none'>grid_4</div>
	</div>
</div>


<script type="text/javascript">
	grid.ResizeInfo = {init_width:1000, init_height:659, width_increase:1, height_increase:1};
		grid_1_out.ResizeInfo = {init_width:300, init_height:659, anchor:{x1:0,y1:1,x2:0,y2:1}};
		grid_2_out.ResizeInfo = {init_width:700, init_height:659, width_increase:1, height_increase:1};
		grid_2.ResizeInfo = {init_width:700, init_height:250, anchor:{x1:1,y1:0,x2:1,y2:0}};
		dgtab1.ResizeInfo = {init_width:700, init_height:30, anchor:{x1:1,y1:0,x2:1,y2:0}};
		grid_3.ResizeInfo = {init_width:700, init_height:377, width_increase:1, height_increase:1};
		grid_4.ResizeInfo = {init_width:700, init_height:377, width_increase:1, height_increase:1};
	//689-30 = 
	//689-30-100 = 
	//폭은 1000 기준 높이는 689 기준
</script>
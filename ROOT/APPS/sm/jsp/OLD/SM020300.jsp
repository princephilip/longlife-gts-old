<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<div id='topsearch' class='option_box2'>
	<div class='detail_label w120'>사용자검색</div> 
	<div id='di_11' class='option_input_bg'>
		<input id='S_USERID' type='text' class='option_input_fix w100'> 
	</div> 
	<div class='option_label w200'>사용여부</div> 
	<div style="position:relative;width:80px"  id="di_12" class="option_input_bg w160">
		<select id='S_USEYN'>
			<option value='%'>전체</option>
			<option value='Y'>사용</option>
			<option value='N'>중지</option>
		</select>
	</div> 
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1'></html:authbutton>
		</span>
	</div>
</div>

<div id='grid_1' class='grid_div mr5'>
	<div id="dg_1" class="slick-grid"></div>
</div>

<div id='freeform' class='detail_box'>
	<div class='detail_label w120'>사용자아이디</div> 
	<div id='di_USER_ID' class='detail_input_bg'>
		<input id='USER_ID' type='text' class='detail_input_fix w110'>
	</div> 
	<div class='detail_line'></div> 
	<div class='detail_label w120'>사용자명</div> 
	<div id='di_USER_NAME' class='detail_input_bg'>
		<input id='USER_NAME' type='text' class='detail_input_fix w110'>
	</div> 	
	<div class='detail_line'></div> 
	<div class='detail_label w120'>사용자설명</div> 
	<div id='di_USER_DESC' class='detail_input_bg'>
		<input id='USER_DESC' type='text' class='detail_input_fix w260'>
	</div> 	
	<div class='detail_line'></div> 
	<div class='detail_label w120'>비밀번호</div> 
	<div id='di_USER_PASSWORD' class='detail_input_bg'>
		<input id='USER_PASSWORD' type='password' class='detail_input_fix w110' readonly>
	</div> 
	<div class='detail_line'></div> 
	<div class='detail_label w120'>메뉴권한</div> 
	<div id='di_MENU_AUTH' class='detail_input_bg w280' >
		<!-- <select id='MENU_AUTH' ></select> -->
		<html:select name="MENU_AUTH" id = "MENU_AUTH" comCodeYn="0" codeGroup="sm|SM_AUTH_MENU_GROUP|SM_AUTH_MENU_GROUP_W01" params = "" dispStyle="1" selected="20" titleCode="선택" showTitle="false"></html:select>
	</div> 
	<div class='detail_line'></div> 
	<div class='detail_label w120'>부서코드</div>
	<div id='di_DEPT_CODE' class='detail_input_bg'>
		<input id='DEPT_CODE' type='text' class='detail_input_fix w60' readonly>
		<input id='DEPT_NAME' type='text' class='detail_input_fix w140'>
		<img id='btn_deptcodefind' src='/Theme/images/btn/btn_find.gif' class="search_find_img"/>
	</div> 	
	<div class='detail_line'></div> 
	<div class='detail_label_check w120'>사용유무</div> 
	<div id='di_USE_YN' class='detail_input_bg w60'>
		<input id='USE_YN' type='checkbox' checked data-labelPosition="left"  data-label="" />
	</div> 
	<div class='detail_line'></div> 
	<div class='detail_label w120'>권한등급</div> 
	<div id='di_USER_ROLE' class='detail_input_bg w200'>
		<html:select name="USER_ROLE" id="USER_ROLE" comCodeYn="1" codeGroup="SM|USER_ROLE" dispStyle="0" showTitle="false"></html:select>
	</div> 
	<div class='detail_line'></div> 
	<div class='detail_label w120'>보안등급</div> 
	<div id='di_DATA_ROLE' class='detail_input_bg w200'>
		<html:select name="DATA_ROLE" id="DATA_ROLE"  comCodeYn="1" codeGroup="SM|DATA_ROLE" dispStyle="0" showTitle="false"></html:select>
	</div> 
	<div class='detail_line'></div> 
	<div class='detail_label w120'>전화번호</div> 
	<div id='di_MOBILE_NO' class='detail_input_bg'>
		<input id='MOBILE_NO' type='text' class='detail_input_fix w140'>
	</div> 
	<div class='detail_line'></div> 
	<div class='detail_label w120'>이메일</div> 
	<div id='di_E_MAIL' class='detail_input_bg'>
		<input id='E_MAIL' type='text' class='detail_input_fix w240'>
	</div> 		
	<div class='detail_line'></div> 
	<div class='detail_label w120'>로그인횟수</div> 
	<div id='di_LOGIN_COUNT' class='detail_input_bg'>
		<input id='LOGIN_COUNT' type='text' class='detail_input_fix w60'>
	</div> 
	<div class='detail_line'></div> 
	<div class='detail_label_check w120'>로그인락여부</div> 
	<div id='di_LOGIN_LOCK_YN' class='detail_input_bg w60'>
		<input id='LOGIN_LOCK_YN' type='checkbox' checked data-labelPosition="left"  data-label="" />
	</div> 
	<div class='detail_line'></div> 
	<div class='detail_label w120'>최종로그인시간</div> 
	<div id='di_LOGIN_TIME' class='detail_input_bg'>
		<input id='LOGIN_TIME' type='text' class='detail_input_fix w160' readonly>
	</div>
	<div class='detail_line'></div> 
	<div class='detail_label w120'>최종로그아웃시간</div> 
	<div id='di_LOGOUT_TIME' class='detail_input_bg'>
		<input id='LOGOUT_TIME' type='text' class='detail_input_fix w160' readonly/>
	</div>
	<div class='detail_line'></div> 
	<div class='detail_label w120'>접속아이피</div> 
	<div id='di_IP_ADDR' class='detail_input_bg'>
		<span><input id='IP_ADDR' type='text' class='detail_input_fix w200' readonly/></span>
	</div> 
</div>

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};
	grid_1.ResizeInfo = {init_width:600, init_height:665, width_increase:1, height_increase:1};
	freeform.ResizeInfo = {init_width:400, init_height:420, width_increase:0, height_increase:0};
</script>
<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
/**
 *<pre>
 * Statements
 *</pre>
 
 * @Class Name : SM0907.jsp
 * @Descriptio	  게시판공통코드관리
 * @Modification Information
 * @
 * @  수정일        		 수정자                   수정내용
 * @ -------    --------    ---------------------------
 * @ 2014.12.28    박영찬        최초 생성
 * @
 */
%>
<script type="text/javascript" src="<c:url value='/Mighty/template/XG_PCHILD_template.js'/>"></script>

<div id='topsearch' class='option_box2'>
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_parent'></html:authbutton>		
		</span>	
	</div>
	<div class='option_label'>코드명</div>
	<div class='option_input_bg w200'><input id='S_FIND' type='text' class='option_input_fix w180'></div>
</div>

<div id='grid_cbutton' class='grid_div'>
	<span style="float:right;margin-top:1px;">
		<html:authchildbutton id='buttons' grid='dg_child' eventHead="_Child" append='true' insert='false' duplicate='false' delete='true' excel='false' retrieve='true'></html:authchildbutton>
	</span>
</div>

<div id='grid_parent' class='grid_div'>
	<%= xgov.core.util.XHtmlUtil.RealGrid("dg_parent", request.getHeader("User-Agent"),"0","N","Y") %>
</div>

<div id='grid_child' class='grid_div ml5'>
	<%= xgov.core.util.XHtmlUtil.RealGrid("dg_child", request.getHeader("User-Agent"),"1","N","Y") %>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};
	grid_cbutton.ResizeInfo = {init_width:1000, init_height:24, width_increase:1, height_increase:0};
	grid_parent.ResizeInfo = {init_width:400, init_height:642, width_increase:0, height_increase:1};
	grid_child.ResizeInfo = {init_width:600, init_height:642, width_increase:1, height_increase:1};
</script>
<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>

<div id='topbar'>
	<div id='xsearch'></div>
	<div id='xbtns'></div>
</div>

<div id='grid'>
	<div id='grid_col_1' style='width:60%;'></div>
	<div class='grid_col_space' style='width:1%;'></div>
	<div id='grid_col_2' style='width:39%;'></div>
	
</div>

<div id='freefrom' class='detail_box'>
	<div class='detail_label_left_check'>센터번호</div><div id='di_11' class='detail_input_bg'><input id='CENTER_CODE' type='text' class='detail_input'  ></div>
	<div class='detail_label_check'>센터명</div>       <div id='di_12' class='detail_input_bg'><input id='CENTER_NAME' type='text' class='detail_input'  ></div>
	<div class='detail_label'>사업자번호</div>         <div id='di_13' class='detail_input_bg'><input id='VENDOR_NO'   type='text' class='detail_input'  ></div>
	<div class='detail_label'>대표자</div>             <div id='di_14' class='detail_input_bg'><input id='REPRE_NAME' type='text' class='detail_input'  ></div>
	
	<div class='detail_line'></div>
	
	<div class='detail_label_left'>신고유형</div>     <div id='di_21' class='detail_input_bg'><select id='REPORT_DIV' class='detail_input'></select></div>
	<div class='detail_label'>종사업장번호</div>      <div id='di_22' class='detail_input_bg'><input id='TAX_MANAGE_NO' type='text' class='detail_input'></div>
	<div class='detail_label'>대표전화번호</div>      <div id='di_23' class='detail_input_bg'><input id='MAIN_PHONENO' type='text' class='detail_input'></div>
	<div class='detail_label'>대표팩스번호</div>      <div id='di_24' class='detail_input_bg'><input id='MAIN_FAXNO' type='text' class='detail_input'></div>
	
  <div class='detail_line'></div>
  
	<div class='detail_label_left'>주소</div>         <div id='di_31' class='detail_input_bg'>
		<input id='POST_CODE' type='text'  class='detail_input_fix' readonly style='width:60px;'>
		<img id='btn_zipfind' src='/images/btn/zip_find.gif'/> 
	  <input id='ADDRESS1'  type='text' class='detail_input_fix' style='width:250px;'>
	  <input id='ADDRESS2'  type='text' class='detail_input_fix' style='width:250px;'>
	</div>
	
	<div class='detail_line'></div>
	
	<div class='detail_label_left'>설립년도</div>   <div id='di_41' class='detail_input_bg'><input id='ESTAB_YEAR' type='text' class='detail_input'></div>
	<div class='detail_label'>대표Email</div>       <div id='di_42' class='detail_input_bg'><input id='MAIN_EMAIL' type='text' class='detail_input'></div>
	<div class='detail_label'>홈페이지</div>        <div id='di_43' class='detail_input_bg'><input id='HOMEPAGE_URL' type='text' class='detail_input'></div>
	
	<div class='detail_line'></div>
	<div class='detail_label_left'>계좌번호</div><div id='di_51' class='detail_input_bg'><input id='DEPOSIT_NO' type='text' class='detail_input' ></div>
	<div class='detail_label'>은행명</div>     <div id='di_52' class='detail_input_bg'>
		<input id='BANK_CODE' type='text' class='detail_input_fix' >
		<img id='btn_bankfind' src='/images/btn/btn_find.gif'/>
	  <input id='BANK_NAME' type='text' class='detail_input' >
	</div>  
	<div class='detail_label'>예금주명</div>     <div id='di_53' class='detail_input_bg'><input id='DEPOSITOR' type='text' class='detail_input' ></div>
	
	<div class='detail_line'></div><div class='detail_line'></div>
	<div class='detail_label_left'>계산시작월</div> <div id='di_61' class='detail_input_bg'><select id='CALC_SM_DIV' class='detail_input' style = 'width:20px;'></select></div>
	<div class='detail_label'>계산시작일</div>      <div id='di_62' class='detail_input_bg'><input  id='CALC_S_DATE' type='text' class='detail_input'></div>
	<div class='detail_label'>계산종료월</div>      <div id='di_63' class='detail_input_bg'><select id='CALC_EM_DIV' class='detail_input'></select></div>
	<div class='detail_label'>계산종료일</div>      <div id='di_64' class='detail_input_bg'><input  id='CALC_E_DATE' type='text' class='detail_input'></div>
	<div class='detail_label'>납부기한월</div>      <div id='di_65' class='detail_input_bg'><select id='ACCPAY_M_DIV' class='detail_input'></select></div>
	<div class='detail_label'>납부기한</div>        <div id='di_66' class='detail_input_bg'><input  id='ACCPAY_DATE' type='text' class='detail_input'></div>

	
</div>

<script type="text/javascript">
	grid.ResizeInfo = {init_width:1000, init_height:490, width_increase:1, height_increase:1};
	freefrom.ResizeInfo = {init_width:1000, init_height:160, anchor:{x1:1,y1:0,x2:1,y2:0}};
	
	di_11.ResizeInfo = {init_width:590,  init_height:0, anchor:{x1:1,y1:0,x2:0.2,y2:0}};
	di_12.ResizeInfo = {init_width:590,  init_height:0, anchor:{x1:1,y1:0,x2:0.4,y2:0}};
	di_13.ResizeInfo = {init_width:590,  init_height:0, anchor:{x1:1,y1:0,x2:0.2,y2:0}};
	di_14.ResizeInfo = {init_width:590,  init_height:0, anchor:{x1:1,y1:0,x2:0.2,y2:0}};

	di_21.ResizeInfo = {init_width:590,  init_height:0, anchor:{x1:1,y1:0,x2:0.2,y2:0}};
	di_22.ResizeInfo = {init_width:590,  init_height:0, anchor:{x1:1,y1:0,x2:0.4,y2:0}};
	di_23.ResizeInfo = {init_width:590,  init_height:0, anchor:{x1:1,y1:0,x2:0.2,y2:0}};
	di_24.ResizeInfo = {init_width:590,  init_height:0, anchor:{x1:1,y1:0,x2:0.2,y2:0}};
	
	di_31.ResizeInfo = {init_width:1000,  init_height:0, anchor:{x1:1,y1:0,x2:0.9,y2:0}};
	
	di_41.ResizeInfo = {init_width:690,  init_height:0, anchor:{x1:1,y1:0,x2:0.3,y2:0}};
	di_42.ResizeInfo = {init_width:690,  init_height:0, anchor:{x1:1,y1:0,x2:0.3,y2:0}};
	di_43.ResizeInfo = {init_width:690,  init_height:0, anchor:{x1:1,y1:0,x2:0.4,y2:0}};
	
	di_51.ResizeInfo = {init_width:570,  init_height:0, anchor:{x1:1,y1:0,x2:0.2,y2:0}};
	di_52.ResizeInfo = {init_width:570,  init_height:0, anchor:{x1:1,y1:0,x2:0.6,y2:0}};
	di_53.ResizeInfo = {init_width:570,  init_height:0, anchor:{x1:1,y1:0,x2:0.2,y2:0}};
	
	di_61.ResizeInfo = {init_width:390,  init_height:0, anchor:{x1:1,y1:0,x2:0.16,y2:0}};
	di_62.ResizeInfo = {init_width:390,  init_height:0, anchor:{x1:1,y1:0,x2:0.16,y2:0}};
	di_63.ResizeInfo = {init_width:390,  init_height:0, anchor:{x1:1,y1:0,x2:0.16,y2:0}};
	di_64.ResizeInfo = {init_width:390,  init_height:0, anchor:{x1:1,y1:0,x2:0.16,y2:0}};
	di_65.ResizeInfo = {init_width:390,  init_height:0, anchor:{x1:1,y1:0,x2:0.16,y2:0}};
	di_66.ResizeInfo = {init_width:390,  init_height:0, anchor:{x1:1,y1:0,x2:0.16,y2:0}};
	
	
	ADDRESS1.ResizeInfo = {init_width:400,  init_height:0, anchor:{x1:1,y1:0,x2:0.5,y2:0}};
	ADDRESS2.ResizeInfo = {init_width:430,  init_height:0, anchor:{x1:1,y1:0,x2:0.5,y2:0}};
	BANK_NAME.ResizeInfo = {init_width:300,  init_height:0, anchor:{x1:1,y1:0,x2:0.5,y2:0}};


	//찾기 이벤트 처리
	$("#btn_zipfind").bind("click", function(){_X.FindPostCode(window,"POST_CODE","")});
	$("#ADDRESS1").bind("dblclick", function(){_X.FindPostCode(window,"POST_CODE",this.value)});
	
	
	$("#btn_bankfind").bind("click", function(){_X.FindBankCode(window,"BANK_CODE","")});
	$("#BANK_CODE").bind("dblclick", function(){_X.FindBankCode(window,"BANK_CODE",this.value)});
</script>
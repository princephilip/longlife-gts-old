<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>

<div id='topsearch' class='option_box2'>
	<div class='option_label_left w40'>사용자</div>
	<div id='di_11' class='option_input_bg w60'>
		<input id='S_USER_ID' type='text' class='option_input'>
	</div>
	<div class='option_label w40'>시스템</div>
	<div id='di_12' class='option_input_bg'>
		<html:select name="S_SYS_ID" id = "S_SYS_ID" comCodeYn="0" codeGroup="sm|CodeCom|SM_AUTH_SYS_R01" params = "" dispStyle="1" titleCode="선택"></html:select>
	</div>
	<div class='option_label'>프로그램</div>
	<div id='di_13' class='option_input_bg w60'>
		<input id='S_PGM_CODE' type='text' class='option_input'>
	</div>	
	<div class='option_label'>권한설정</div>
	<div id='di_14' class='option_input_bg'>
	  <input id='chk_AUTH_I' type='checkbox' data-label='입력' />
	  <input id='chk_AUTH_R' type='checkbox' data-label='조회' />
	  <input id='chk_AUTH_D' type='checkbox' data-label='삭제' />
	  <input id='chk_AUTH_P' type='checkbox' data-label='인쇄' />
	</div>
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1'></html:authbutton>
		</span>
	</div>
	<div id='userxbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
		  <html:button name='S_CALC_BTN' id='S_CALC_BTN' type='button' icon='ui-icon-copy' desc="권한복사" onClick="pf_AuthFind();"></html:button>
		</span>
	</div>
</div>

<div id='grids' class='grid_div'>
	<div id='grid_list' class='grid_div mr10'>
		<%= xgov.core.util.XHtmlUtil.RealGrid("dg_list", request.getHeader("User-Agent")) %>
	</div>
	<div id='grid_1' class='grid_div'>
		<div id="dg_1" class="slick-grid"></div>
	</div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};
	grids.ResizeInfo = {init_width:1000, init_height:667, width_increase:1, height_increase:1};
	grid_list.ResizeInfo = {init_width:500, init_height:667, width_increase:0, height_increase:1};
	grid_1.ResizeInfo = {init_width:500, init_height:667, width_increase:1, height_increase:1};
	//di_11.ResizeInfo = {init_width:120,  init_height:0, anchor:{x1:0,y1:0,x2:0,y2:0}};
	//di_12.ResizeInfo = {init_width:135,  init_height:0, anchor:{x1:0,y1:0,x2:0,y2:0}};
	//di_13.ResizeInfo = {init_width:165,  init_height:0, anchor:{x1:0,y1:0,x2:0,y2:0}};
	//di_14.ResizeInfo = {init_width:180,  init_height:0, anchor:{x1:1,y1:0,x2:1,y2:0}};
</script>
<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2'>
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true" pgmcode="SM01050"></html:authbutton>
		</span>
	</div>

	<div class='option_label w70'>부서명검색</div>
	<div class='option_input_bg w200'>
		<input id='S_FIND' type='text' class='option_input_fix' style="width:190px">
	</div>

	<!--
	<div class='option_label w70'>부서구분</div>
	<div style="position:relative;width:70px" class='option_input_bg w150'>
		<select id='S_OBJ_TYPE'></select>
	</div>
	-->

	<div class='option_label w70'>사용여부</div>
	<div style="position:relative;width:70px" class="option_input_bg">
		<select id='S_USE_YESNO'>
			<option value='%'>전체</option>
			<option value='Y'>사용</option>
			<option value='N'>미사용</option>
		</select>
	</div>

</div>

<div id='grid_1' class='grid_div'>
	<div id="dg_1" style='float:left; width:100%; height:100%;' class='slick-grid'></div>
</div>

<script type="text/javascript">

	topsearch.ResizeInfo = {init_width:1000, init_height:33, anchor:{x1:1,y1:1,x2:1,y2:0}};
	grid_1.ResizeInfo    = {init_width:1000, init_height:671, anchor:{x1:1,y1:1,x2:1,y2:1}};

</script>



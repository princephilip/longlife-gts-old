<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<jsp:include page="/Mighty/jsp/UploadExcelForm.jsp"/>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true" pgmcode="SM02010"></html:authbutton>
		</span>
	</div>

	<!-- <div class='option_label w100'>회사명</div>
	<div class='option_input_bg'>
		<input id='S_COMPANY' type='text' class='option_input_fix' style="width:100px">
	</div> -->


</div>

<div id='grid_1' class='grid_div mr5'>
	<div id="dg_1" class="slick-grid"></div>
</div>


<div id='form_free1' class='detail_box ver2'>
	<div class='detail_row'>
		<label class='detail_label' style="width:100px">*회사코드</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='COMPANY_CODE' class='detail_input_fix'>
		</div>

		<label class='detail_label' style="width:100px">*그룹구분</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='GROUP_DIV' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">*회사명칭</label>
		<div class='detail_input_bg' style="width:400px">
			<input type='text' id='COMPANY_NAME' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">*사업자명칭</label>
		<div class='detail_input_bg' style="width:400px">
			<input type='text' id='VENDOR_NAME' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">*사업자번호</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='VENDOR_NO' class='detail_input_fix'>
		</div>

		<label class='detail_label' style="width:100px">전화번호</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='PHONE' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">*대표자</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='REPRESENT_NAME' class='detail_input_fix'>
		</div>

		<label class='detail_label' style="width:100px">대표자영문명</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='REPRESENT_ENAME' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">주민번호</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='REPRESENT_RRN' class='detail_input_fix'>
		</div>

		<label class='detail_label' style="width:100px">법인번호</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='LEGAL_NO' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">업태</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='BIZ_STATUS' class='detail_input_fix'>
		</div>

		<label class='detail_label' style="width:100px">업종</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='BIZ_TYPE' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">우편번호</label>
		<div class="detail_input_bg has_button" style="width:150px">
      <input type="text" id="ZIP_CODE">
      <button id="btn_zipfind" class="search_find_img" onClick="uf_ZipFind('ZIP_CODE')"></button>
		</div>

		<label class='detail_label' style="width:100px"></label>
		<div class='detail_input_bg' style="width:150px">
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">주소1</label>
		<div class='detail_input_bg' style="width:400px">
			<input type='text' id='ADDR1' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">주소2</label>
		<div class='detail_input_bg' style="width:400px">
			<input type='text' id='ADDR2' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">회사영문명</label>
		<div class='detail_input_bg' style="width:400px">
			<input type='text' id='COMP_ENAME' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">영문주소1</label>
		<div class='detail_input_bg' style="width:400px">
			<input type='text' id='ADDR1_ENG' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">영문주소2</label>
		<div class='detail_input_bg' style="width:400px">
			<input type='text' id='ADDR2_ENG' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">홈페이지</label>
		<div class='detail_input_bg' style="width:400px">
			<input type='text' id='SITE_ADDR' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">회계최초시작</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='FIRST_S_DATE' class='detail_date'>
		</div>

		<label class='detail_label' style="width:100px">회계게시월</label>
		<div class='detail_input_bg' style="width:150px">
			<select id="AM_START_MONTH" class='detail_input_fix'></select>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">당기회계기간</label>
		<div class='detail_input_bg' style="width:400px">
			<input type='text' id='AM_S_DATE' class='detail_date' style='width:90px'>&nbsp;&nbsp;~&nbsp;&nbsp;
			<input type='text' id='AM_E_DATE' class='detail_date' style='width:90px'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">전표출력명칭</label>
		<div class='detail_input_bg' style="width:400px">
			<input type='text' id='SLIP_PRINT_NAME' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">그룹정렬</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='GROUP_ORDER' class='detail_input_fix'>
		</div>

		<label class='detail_label' style="width:100px">회사정렬</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='COMP_ORDER' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<div class='detail_input_bg' style="width:500px">
			<div style='width:170px'><input id='SLIP_EVID_CNT_YN' type='checkbox' disabled=true /> 전표:증빙1건여부</div>
			<div style='width:170px'><input id='TAX_EVID_ISSUE_YN' type='checkbox' disabled=true /> 매출증빙발행관리여부</div>
			<div style='width:160px'><input id='OUTSOURCE_YN' type='checkbox' disabled=true /> 급여위탁</div>
		</div>
	</div>

	<div class='detail_row'>
		<div class='detail_input_bg' style="width:500px">
			<div style='width:170px'><input id='BDGT_RESULT_YN' type='checkbox' disabled=true /> 예산실적관리</div>
			<div style='width:170px'><input id='HR_USE_YN' type='checkbox' disabled=true /> 인사사용</div>
			<div style='width:160px'><input id='USE_YN' type='checkbox' disabled=true /> 사용여부</div>
		</div>
	</div>


	<div class='detail_row detail_row_bb'>
		<div class='detail_input_bg' style="width:500px">
			<div style='width:250px'><input id='GENERAL_ACNT_USE_YN' type='checkbox' disabled=true /> 원가부서,판관리 사용여부</div>
			<div style='width:250px'><input id='CREDITCARD_VAT_YN' type='checkbox' disabled=true /> 신용카드 부가세신고</div>
		</div>
	</div>

</div>


<script type="text/javascript">
	topsearch.ResizeInfo			= {init_width:1000, init_height: 35, width_increase:1, height_increase:0};
	grid_1.ResizeInfo					= {init_width: 500, init_height:660, width_increase:1, height_increase:1};
	form_free1.ResizeInfo			= {init_width: 500, init_height:660, width_increase:0, height_increase:1};
</script>


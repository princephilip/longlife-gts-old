<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
/**
 *<pre>
 * Statements
 *</pre>

 * @Class Name : SM0903.jsp
 * @Descriptio    메뉴목록관리
 * @Modification Information
 * @
 * @  수정일            수정자                   수정내용
 * @ -------    --------    ---------------------------
 * @ 2013.03.03    박영찬        최초 생성
 * @
 */
%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box'>

  <div id='xbtns'>
    <html:authbutton id='buttons' grid='dg_1' pgmcode="SM900050"></html:authbutton>
  </div>
  <div id='userxbtns'>
  </div>

  <div class='option_label'>시스템</div>
  <div class='option_input_bg'>
    <html:select name="S_SYS_ID" id = "S_SYS_ID" comCodeYn="0" codeGroup="sm|SM_AUTH_SYS|W_SM0915_01" params="${companyCode}" dispStyle="1" selected="20" showTitle="true" ></html:select>
  </div>
  <div class='option_label'>프로그램</div>
  <div class='option_input_bg'>
    <input id='S_PGM_CODE' type='text' class='option_input_fix w140'>
  </div>

  <div class='option_label'>선택</div>
  <div class='option_input_bg'>
    <html:button name='select_all' id='select_all' type='button' icon='ui-icon-check' desc="전체선택" onClick="pf_SelectAll('SELECTED', true);"></html:button>
    <html:button name='deselect_all' id='deselect_all' type='button' icon='ui-icon-closethick' desc="선택해제" onClick="pf_SelectAll('SELECTED', false);"></html:button>
    <html:button name='select_new' id='select_new' type='button' icon='ui-icon-star' desc="DB없는것만 선택" onClick="pf_SelectNew('SELECTED', true);"></html:button>
  </div>

  <div class='option_label'>작업</div>
  <div class='option_input_bg'>
    <html:button name='btn_import' id='btn_import' type='button' icon='ui-icon-transferthick-e-w' desc="IMP Grid.js & .js & .xmlx" onClick="getStringFromJs()"></html:button>
  </div>

</div>

<div id='grid_1' class='grid_div has_title'>
  <label class="sub_title">프로그램코드</label>
  <div id="dg_1" class="slick-grid"></div>
</div>

<div id='grid_2' class='grid_div has_title ml10'>
  <label class="sub_title">Grid.js 파일 속 칼럼정보</label>
  <div id="dg_2" class="slick-grid"></div>
</div>

<div id='grid_3' class='grid_div has_title ml10'>
  <label class="sub_title">SQL FILE</label>
  <div id="dg_3" class="slick-grid"></div>
</div>

<script type="text/javascript">
  topsearch.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};
  grid_1.ResizeInfo = {init_width:490, init_height:665, width_increase:0.5, height_increase:1};
  grid_2.ResizeInfo = {init_width:510, init_height:500, width_increase:0.5, height_increase:1};
  grid_3.ResizeInfo = {init_width:510, init_height:165, anchor:{x1:1,y1:1,x2:0.5,y2:0}};
</script>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="<c:url value='/Mighty/template/js/XG_GRID1_template.js'/>"></script>

<div id='topsearch' class='option_box2' >
  <div id='xbtns'>
    <html:authbutton id='buttons' grid='dg_1' pgmcode="SM9010"></html:authbutton>
  </div>


  <div class='option_label'>시스템</div>
  <div class='option_input_bg'>
    <html:select name="S_SYS_ID" id = "S_SYS_ID" comCodeYn="0" codeGroup="sm|SM9010|SM9010_SYSID" params="${companyCode}" dispStyle="1" selected="20" showTitle="true" titleCode="전체" titleValue="%"></html:select>
  </div>

  <div class='option_label' >수정승인</div>
  <div class="option_input_bg" >
    <select id="S_DEV_TAG" class="w70">
      <option value="%">전체</option>
      <option value="Y">승인</option>
      <option value="N">미승인</option>
    </select>
  </div>

  <div class='option_label' >수정완료</div>
  <div class="option_input_bg" >
    <select id="S_END_YN" class="w70">
      <option value="%">전체</option>
      <option value="Y">완료</option>
      <option value="N" selected>미완료</option>
    </select>
  </div>

  <div class='option_label' >완료확인</div>
  <div class="option_input_bg" >
    <select id="S_DEV_END" class="w70">
      <option value="%">전체</option>
      <option value="Y">확인</option>
      <option value="N" selected>미확인</option>
    </select>
  </div>

  <div class='option_label_left'>검색</div>
  <div class='option_input_bg w150'>
    <input id='S_KEYWORD' type='text' class='option_input_fix'>
  </div>

</div>

<div id='grid_1' class='grid_div' >
  <div id="dg_1" class="slick-grid"></div>
</div>

<div id='freeform' class='detail_box ver2 pl5 has_title'>
	<label class='sub_title'>수정요청 내용</label>
  <div class='detail_row detail_row_bb'>
    <div class='detail_input_bg'>
      <textarea id="MAIN_CONTENT" style='height: 360px'></textarea>
    </div>
  </div>
</div>

<div id='freeform2' class='detail_box ml5 has_title'>
	<label class='sub_title'>메세지</label>
  <div id='MSG_CONTENT'></div>
</div>

<div id='freeform3' class='detail_box ver2 pl5'>
  <div class='detail_row detail_row_bb'>
  	<label class='detail_label w100'>입력(ctrl+Enter)</label>
    <div class='detail_input_bg'>
      <textarea id="MSG_INPUT" style='height: 70px'></textarea>
    </div>
  </div>
</div>

<div id='grid_2' class='grid_div' style='opacity: 0;' >
  <div id="dg_2" class="slick-grid"></div>
</div>

 <script type="text/javascript">
  topsearch.ResizeInfo   = {init_width:1000, init_height:33 , width_increase:1, height_increase:0};
  grid_1.ResizeInfo      = {init_width: 950, init_height:665, width_increase:0, height_increase:1};
  freeform.ResizeInfo    = {init_width: 50, init_height:430, width_increase:1, height_increase:0};

  freeform2.ResizeInfo   = {init_width: 50, init_height:300, width_increase:1, height_increase:0};
  MSG_CONTENT.ResizeInfo = {init_width: 50, init_height:293, width_increase:1, height_increase:0};

  freeform3.ResizeInfo   = {init_width: 50, init_height:100, width_increase:1, height_increase:0};

  grid_2.ResizeInfo      = {init_width:1000, init_height:  1, width_increase:0, height_increase:0};
</script>

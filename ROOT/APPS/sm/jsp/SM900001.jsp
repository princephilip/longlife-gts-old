<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link rel="stylesheet" href="/Mighty/3rd/font-awesome/css/all.css">
<link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap-theme.min.css">
<script type="text/javaScript" src="/Mighty/3rd/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<style type="text/css">
  #pageLayout { overflow-x: hidden; overflow-y: auto; }
  ul { margin-left: 20px; }
  li { margin-bottom: 6px; font-size: 1.2em; }
</style>

<div class="container-fluid">
	<div class="row">
	  <div class="col-md-8">
	    <div class="panel panel-default">
	      <div class="panel-heading"><strong>프로젝트 기본정보</strong></div>
	      <div class="panel-body">
	        <ul>
	        	<li><strong>회사명</strong> : 주식회사 세방TEC</li>
	        	<li><strong>홈페이지</strong> : <a href="http://www.sebangtec.co.kr/" target="_blank">http://www.sebangtec.co.kr/</a></li>
	        	<li><strong>기 간</strong> : 2016. 10. 10 ~</li>
	        	<li><strong>본 사</strong> : 서울시 서초구 양재천로 135-4 솔빌딩</li>
	        	<li><strong>TEL</strong> : 02-579-5551</li>
	        </ul>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="row">
	  <div class="col-md-8">
	    <div class="panel panel-default">
	      <div class="panel-heading"><strong>개발, 운영 관련 문서</strong></div>
	      <div class="panel-body">
	        <ul>
	        	<li><strong>신한 InsideBank 연동</strong> : <a href="https://docs.google.com/document/d/1oha9Xm6qBxMZ7SzeLLEFcMMj4DoDJ2m-G9C7GsRj8Qk/edit#heading=h.2auj325vyoja" target="_blank">신한 InsideBank 연동 문서 바로가기</a></li>
	        	
	        </ul>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="row">
	  <div class="col-md-8">
	    <div class="panel panel-default">
	      <div class="panel-heading"><strong>개발관련 정보</strong></div>
	      <div class="panel-body">
	        <ul>
	        	<li><strong>PM(redmine)</strong> : <a href="http://pm.xinternet.co.kr/redmine/projects/hitecj" target="_blank">http://pm.xinternet.co.kr/redmine/projects/hitecj</a></li>
	        	<li><strong>PM(redmine) wiki</strong> : <a href="http://pm.xinternet.co.kr/redmine/projects/hitecj/wiki" target="_blank">http://pm.xinternet.co.kr/redmine/projects/hitecj/wiki</a></li>
	        	<li><strong>개발규칙문서</strong> : <a href="https://docs.google.com/document/d/1eiFeGmML_VjmxrJm8-bAj8AnoegSc8asuhhf5MZN5es/edit?usp=sharing" target="_blank">개발규칙문서 바로가기</a></li>
	        	
	        </ul>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="row">
	  <div class="col-md-8">
	    <div class="panel panel-default">
	      <div class="panel-heading"><strong>참고사이트</strong></div>
	      <div class="panel-body">
	        <ul>
	        	<li><strong>Jquery UI ICON</strong> : <a href="https://api.jqueryui.com/theming/icons/" target="_blank">https://api.jqueryui.com/theming/icons/</a></li>
	        	
	        </ul>
	      </div>
	    </div>
	  </div>
	</div>


</div>

﻿<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2' >
  <div id='xbtns'>
    <html:authbutton id='buttons' grid='dg_1' pgmcode="SM01140"></html:authbutton>
  </div>

  <div class='option_label'>현장명(<a onclick='_X.ProjIngToggle(this)'>진행</a>)</div>
  <div class='option_input_bg'>
    <html:select name="S_PROJ_CD" id = "S_PROJ_CD" comCodeYn="0" codeGroup="cm|CM_COMMON|PROJ_CODE_ING" params = "${companyCode}|${userId}|Y" dispStyle="1" titleCode="전체" titleValue="%" class="w340 cache"></html:select>
    <img id="btn_sprojfind" src="/Theme/images/btn/btn_find.gif" class="search_find_img"/>
  </div>

  <div class='option_label_left'>통합검색</div>
  <div class='option_input_bg w200'>
    <input id='S_FIND' type='text' class='option_input_fix'>
  </div>

  <div id='userxbtns'>
    <button type="button" id='btn_init' class="btn btn-success btn-xs" style="margin-right: 20px;" onClick="pf_sensor_init();">출역에서 가져오기</button>
  </div>

</div>


<!-- <div id='grid_top' class='grid_div mt5'>
  <div id='tabs_1' class='grid_div'>
    <div id="ctab_nav" class="xtabs_nav">
      <ul>
        <li><a href="#ctab_1">현장배포</a></li>
        <li><a href="#ctab_1">본사보관</a></li>
      </ul>
    </div>

    <div id="ctab_1" class='tabc_div'>
      <div id='tabsub_1' class='grid_div'>
        <div id='grid_1' class='grid_div'>
          <div id="dg_1" class='slick-grid'></div>
        </div>
      </div>
    </div>
  </div>
</div> -->

<div id="tabs_1" class='tabs_div' style="margin-top: 5px;">
  <div id="ctab_nav" class="xtabs_nav" style='margin-right: 250px;'>
    <ul>
    <li><a href="#ctab_1">현장배포</a></li>
    <li><a href="#ctab_1">본사보관</a></li>
    <li><a href="#ctab_1">폐기 or 테스트용</a></li>
    </ul>
  </div>

  <div id="ctab_1">
    <div id='grid_1' class='grid_div ' >
      <div id="dg_1" class='slick-grid'></div>
    </div>
  </div>
</div>

<div id='freeform' class='detail_box ver2 has_title pl5'>
  <label class='sub_title'>장비등록
    <div style="float:right;margin-top:4px;">
      <button type="button" id='btn_return' class="btn btn-danger btn-xs" onClick="pf_return();">장비반납
    </div>
  </label>

  <div class='detail_row'>
    <label class='detail_label w100'>장비명</label>
    <div class='detail_input_bg w200'>
      <input id='SENSOR_NAME' type='text' >
    </div>
    <label class='detail_label w100'>모델명</label>
    <div class='detail_input_bg '>
      <input id='SENSOR_MODEL' type='text' >
    </div>
  </div>

  <div class='detail_row'>
    <label class='detail_label w100'>*장비번호</label>
    <div class='detail_input_bg w100'>
      <input id='SENSORID' type='text' >
    </div>
    <label class='detail_label w70'>타이머</label>
    <div class='detail_input_bg w40'>
      <input type='checkbox' id='TIMER_YN' />
    </div>
    <label class='detail_label w100'>본사보관</label>
    <div class='detail_input_bg w40'>
      <input type='checkbox' id='KEEP_YN' />
    </div>
    <label class='detail_label w90'>폐기여부</label>
    <div class='detail_input_bg '>
      <input type='checkbox' id='DISCARD_YN' />
    </div>
  </div>

  <div class='detail_row'>
    <label class='detail_label w100'>등록일</label>
    <div class='detail_input_bg w200'>
      <input id='INSTALL_DATE' type='text' class='detail_date'>
    </div>
    <label class='detail_label w100'>인수자</label>
    <div class='detail_input_bg has_button'>
      <input id='INSTALL_EMP_NAME' type='text'>
      <button id='btn_empfind' class="search_find_img" />
    </div>
  </div>

   <div class='detail_row'>
    <label class='detail_label w100'>현장명</label>
     <div class='detail_input_bg has_button'>
      <input id='PROJ_NAME' type='text' >
      <button id='btn_projfind' class="search_find_img" />
    </div>
  </div>

  <div class='detail_row'>
    <label class='detail_label w100'>설치장소</label>
    <div class='detail_input_bg'>
      <input id = 'INSTALL_PLACE' type='text' >
    </div>
  </div>

  <div class='detail_row'>
    <label class='detail_label w100'>반납일</label>
    <div class='detail_input_bg w200'>
      <input id='RETURN_DATE' type='text' class='detail_date'>
    </div>
    <label class='detail_label w100'>반납자</label>
    <div class='detail_input_bg has_button'>
      <input id='RETURN_EMP_NAME' type='text'>
      <button id='btn_empfind2' class="search_find_img" />
    </div>
  </div>

  <div class='detail_row'>
    <label class='detail_label w100'>관리부서</label>
    <div class='detail_input_bg has_button'>
      <input id='DEPT_NAME' type='text'>
      <button id='btn_deptfind' class="search_find_img" />
    </div>
  </div>

  <div class='detail_row detail_row_bb'>
    <label class='detail_label w100'>비고</label>
    <div class='detail_input_bg'>
      <textarea id="REMARKS" style="height: 120px;"></textarea>
    </div>
  </div>
  <!-- .detail_row -->
</div><!-- #freeform3 -->


<div id='grid_2' class='grid_div has_title pl5'>
  <label class='sub_title'>장비 반납이력
<!--     <div class='child_button float_right'>
      <html:authchildbutton id='buttons' grid='dg_2' append='true' insert='false' duplicate='false' delete='true' excel='false' retrieve='false' ></html:authchildbutton>
    </div> -->
  </label>
  <div id="dg_2" class='slick-grid'></div>
</div>


<script type="text/javascript">
  topsearch.ResizeInfo  = {init_width:1000, init_height:33 , width_increase:1, height_increase:0};

/*  grid_top.ResizeInfo = {init_width:450, init_height:25, width_increase:1, height_increase:1};
  tabs_1.ResizeInfo    = {init_width:450, init_height:640, width_increase:1, height_increase:1}; // 그리드와 탭엮기위함
  ctab_nav.ResizeInfo  = {init_width:450, init_height: 25, width_increase:1, height_increase:1}; // 탭 틀
  ctab_1.ResizeInfo    = {init_width:450, init_height:640, width_increase:1, height_increase:1}; // 탭 뿌려줌 및  체인지 작동
  tabsub_1.ResizeInfo  = {init_width:450, init_height:640, width_increase:1, height_increase:1};
  grid_1.ResizeInfo   = {init_width:450, init_height:640 , width_increase:1, height_increase:1};
*/
  tabs_1.ResizeInfo    = {init_width:450, init_height:640, width_increase:1, height_increase:1};
  grid_1.ResizeInfo    = {init_width:450, init_height:640, width_increase:1, height_increase:1};

  freeform.ResizeInfo  = {init_width:550, init_height:360, width_increase:0, height_increase:0};
  grid_2.ResizeInfo    = {init_width:550, init_height:305, width_increase:0, height_increase:1};
</script>
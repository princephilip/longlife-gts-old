﻿<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2' >
	<div id='xbtns' style='height:23px;'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' pgmcode="SM0940"></html:authbutton>
		</span>
	</div>

	<div class='option_label'>시스템</div>
	<div class='option_input_bg'>
		<html:select name="S_SYS_ID" id = "S_SYS_ID" comCodeYn="0" codeGroup="sm|SM_AUTH_SYS|W_SM0915_01" params="${companyCode}" dispStyle="1" selected="20" showTitle="true" ></html:select>
	</div>

</div>

<div id='grid_1' class='grid_div' >
	<div id="dg_1" style='width:100%; height:100%;' class='slick-grid'></div>
</div>

<script type="text/javascript">

	topsearch.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};
	grid_1.ResizeInfo = {init_width:1000, init_height:667, width_increase:1, height_increase:1};

</script>
<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<div id='topsearch' class='option_box2'>
	<div class='option_label w80'>거래처검색</div>
	<div id='di_11' class='option_input_bg' style='width:110px;'>
		<input id='S_CUST_NAME' type='text' class='option_input_fix' style='width:100px;'>
	</div>

	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' pgmcode="SM01030"></html:authbutton>
		</span>
	</div>

</div>

<div id='grid_1' class='grid_div w70 pr5'>
	<div id="dg_1" style="width:100%; height:100%; " class="slick-grid"></div>
</div>

<div id='freeform' class='detail_box ver2 w30 has_title'>
	<label class='sub_title'>선택된 회사코드 등록/수정란</label>
	<div class='detail_row'>
		<label class='detail_label w110'>회사코드/명칭</label>
		<div class='detail_input_bg w150'>
			<input id='COMPANY_CODE' type='text'>
		</div>
		<div class='detail_input_bg'>
			<input id='COMPANY_NAME' type='text'>
		</div>
	</div>
	<div class='detail_row'>
		<label class='detail_label w110'>사업자번호</label>
		<div class='detail_input_bg'>
			<input id='VENDOR_NO' type='text' >
		</div>
	</div>
	<div class='detail_row'>
		<label class='detail_label w110'>사업자명칭</label>
		<div class='detail_input_bg'>
			<input id='VENDOR_NAME' type='text' >
		</div>
	</div>
	<div class='detail_row'>
		<label class='detail_label w110'>대표자명</label>
		<div class='detail_input_bg'>
			<input id='REPRESENT_NAME' type='text' >
		</div>
	</div>
	<div class='detail_row'>
		<label class='detail_label w110'>대표자영문명</label>
		<div class='detail_input_bg'>
			<input id='REPRESENT_ENAME' type='text' >
		</div>
	</div>
	<div class='detail_row'>
		<label class='detail_label w110'>주민번호</label>
		<div class='detail_input_bg'>
			<input id='REPRESENT_RRN' type='text' >
		</div>
	</div>
	<div class='detail_row'>
		<label class='detail_label w110'>법인번호</label>
		<div class='detail_input_bg'>
			<input id='LEGAL_NO' type='text'>
		</div>
	</div>
	<div class='detail_row'>
		<label class='detail_label w110'>업태</label>
		<div class='detail_input_bg'>
			<input id='BIZ_STATUS' type='text'>
		</div>
	</div>
	<div class='detail_row'>
		<label class='detail_label w110'>업종</label>
		<div class='detail_input_bg'>
			<input id='BIZ_TYPE' type='text' >
		</div>
	</div>
	<div class='detail_row'>
		<label class='detail_label w110'>전화번호</label>
		<div class='detail_input_bg'>
			<input id='PHONE' 		type='text' >
		</div>
	</div>
	<div class='detail_row'>
		<label class='detail_label w110'>우편번호</label>
		<div class='detail_input_bg'>
			<input id='ZIP_CODE' 		type='text'  >
		</div>
	</div>
	<div class='detail_row'>
		<label class='detail_label w110'>주소1</label>
		<div class='detail_input_bg'>
			<input id='ADDR1' type='text'>
		</div>
	</div>
	<div class='detail_row'>
		<label class='detail_label w110'>주소2</label>
		<div class='detail_input_bg'>
			<input id='ADDR2' type='text'>
		</div>
	</div>
	<div class='detail_row'>
		<label class='detail_label w110'>회사영문명</label>
		<div class='detail_input_bg'>
			<input id='COMP_ENAME' type='text' >
		</div>
	</div>
	<div class='detail_row'>
		<label class='detail_label w110'>영문주소1</label>
		<div class='detail_input_bg'>
			<input id='ADDR1_ENG' type='text'  >
		</div>
	</div>
	<div class='detail_row'>
		<label class='detail_label w110'>영문주소2</label>
		<div class='detail_input_bg'>
			<input id='ADDR1_ENG' type='text'  >
		</div>
	</div>
	<div class='detail_row'>
		<label class='detail_label w110'>홈사이트</label>
		<div class='detail_input_bg'>
			<input id='SITE_ADDR' type='text' >
		</div>
	</div>
	<div class='detail_row'>
		<label class='detail_label w110'>회계최초시작</label>
		<div class='detail_input_bg'>
			<input id='FIRST_S_DATE' type='text' class='detail_date'>
		</div>
	</div>
	<div class='detail_row'>
		<label class='detail_label w110'>당기회계기간</label>
		<div class='detail_input_bg w150'>
			<input id='AM_S_DATE' type='text' class='detail_date'>
		</div>
		~
		<div class='detail_input_bg'>
			<input id='AM_E_DATE' type='text' class='detail_date'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label w110'>전표출력명칭</label>
		<div class='detail_input_bg w100'>
			<input id='SLIP_PRINT_NAME' type='text'>
		</div>
		<label class='detail_label w110'>회계개시월</label>
		<div class='detail_input_bg'>
			<input id='AM_STRAT_MONTH' type='text'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label w110'>전표:증빙1건여부</label>
		<div class='detail_input_bg'>
			<input id='SLIP_EVID_CNT_YN' type='checkbox'>
		</div>

		<label class='detail_label w110'>예산실적관리</label>
		<div class='detail_input_bg'>
			<input id='BDGT_RESULT_YN' type='checkbox'>
		</div>

	</div>

	<div class='detail_row'>
		<label class='detail_label w110'>급여위탁</label>
		<div class='detail_input_bg'>
			<input id='OUTSOURCE_YN' type='checkbox'>
		</div>
		<label class='detail_label w110'>매출증빙발행여부</label>
		<div class='detail_input_bg'>
			<input id='TAX_EVID_ISSUE_YN' type='checkbox'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label w150'>원가부서,판관비 사용여부</label>
		<div class='detail_input_bg'>
			<input id='GENERAL_ACNT_USE_YN' type='checkbox'>
		</div>
		<label class='detail_label w110'>인사사용</label>
		<div class='detail_input_bg'>
			<input id='HR_USE_YN' type='checkbox'>
		</div>
		<label class='detail_label w110'>사용여부</label>
		<div class='detail_input_bg'>
			<input id='USE_YN' type='checkbox'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label w150'>프로젝트별 원가관리</label>
		<div class='detail_input_bg'>
			<input id='PROJECT_COST_YN' type='checkbox'>
		</div>
		<label class='detail_label w110'>그룸정렬</label>
		<div class='detail_input_bg'>
			<input id='GROUP_ORDER' type='checkbox'>
		</div>
		<label class='detail_label w110'>회사정렬</label>
		<div class='detail_input_bg'>
			<input id='COMP_ORDER' type='checkbox'>
		</div>
	</div>

	<div class='detail_row detail_row_bb'>
		<label class='detail_label w150'>신용카드 부가세신고</label>
		<div class='detail_input_bg'>
			<input id='CREDITCARD_VAT_YN' type='checkbox'>
		</div>
	</div>

</div>

</div>

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};
	grid_1.ResizeInfo 	 = {init_width:550, init_height:655, width_increase:1, height_increase:1}; // 그리드와 탭엮기위함
	freeform.ResizeInfo  = {init_width:450, init_height:655, width_increase:0, height_increase:0};
</script>
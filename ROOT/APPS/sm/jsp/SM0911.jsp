<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
/**
 *<pre>
 * Statements
 *</pre>

 * @Class Name : SM0911.jsp
 * @Descriptio	  메뉴권한관리
 * @Modification Information
 * @
 * @  수정일        		 수정자                   수정내용
 * @ -------    --------    ---------------------------
 * @ 2013.03.03    박영찬        최초 생성
 * @
 */
%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<div id='topbar'>
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' pgmcode="SM0911"></html:authbutton>
		</span>
	</div>
	<div id='userxbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			  <html:button name='sourcecopy' id='sourcecopy' type='button' icon='ui-icon-copy' desc="소스복사" onClick="pf_PgmFind();"></html:button>
		</span>
	</div>
</div>


<div id='topsearch' class='option_box'>
	<div class='option_label_left'>시스템</div>
	<div id='di_11' class='option_input_bg'>
		<html:select name="S_SYS_ID" id = "S_SYS_ID" comCodeYn="0" codeGroup="sm|SM_AUTH_SYS|W_SM0915_01" params="${companyCode}" dispStyle="1" selected="20" showTitle="true" titleCode="선택"></html:select>
	</div>
	<div class='option_label'>프로그램</div>
	<div id='di_12' class='option_input_bg'>
		<input id='S_PGM_CODE' type='text' class='option_input'>
	</div>
	<div class='option_label'>일괄설정</div>
	<div id='di_13' class='option_input_bg'>
	  <input id='chk_SHOW_YESNO' type='checkbox' data-label='보이기' />
	  <input id='chk_UPDATE_YESNO' type='checkbox' data-label='저장' />
	  <input id='chk_RETRIEVE_YESNO' type='checkbox' data-label='조회' />
	  <input id='chk_INSERT_YESNO' type='checkbox' data-label='삽입' />
	  <input id='chk_APPEND_YESNO' type='checkbox' data-label='추가' />
	  <input id='chk_DUPLICATE_YESNO' type='checkbox' data-label='복제' />
	  <input id='chk_DELETE_YESNO' type='checkbox' data-label='삭제' />
	  <input id='chk_PRINT_YESNO' type='checkbox' data-label='출력' />
	  <input id='chk_EXCEL_YESNO' type='checkbox' data-label='엑셀' />
	  <input id='chk_CLOSE_YESNO' type='checkbox' data-label='닫기' />
	  <input id='chk_DEVELOPED_YESNO' type='checkbox' data-label='개발완료' />
	  <input id='chk_HELP_YESNO' type='checkbox' data-label='도움말' />
	</div>
</div>

<div id='grid_1'>
	<%= xgov.core.util.XHtmlUtil.RealGrid("dg_1", request.getHeader("User-Agent") ,"0","99","Y") %>
</div>

<script type="text/javascript">
	topbar.ResizeInfo = {init_width:1000, init_height:33, anchor:{x1:1,y1:0,x2:1,y2:0}};
	topsearch.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};
	grid_1.ResizeInfo = {init_width:1000, init_height:630, width_increase:1, height_increase:1};
	di_11.ResizeInfo = {init_width:135,  init_height:0, anchor:{x1:0,y1:0,x2:0,y2:0}};
	di_12.ResizeInfo = {init_width:150,  init_height:0, anchor:{x1:0,y1:0,x2:0,y2:0}};
	di_13.ResizeInfo = {init_width:350,  init_height:0, anchor:{x1:1,y1:0,x2:1,y2:0}};
	//S_SYS_ID.ResizeInfo = {init_width:100,  init_height:0, anchor:{x1:0,y1:0,x2:0,y2:0}};
</script>
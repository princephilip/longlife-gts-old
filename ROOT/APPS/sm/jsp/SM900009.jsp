<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link rel="stylesheet" href="/Mighty/3rd/font-awesome/css/all.css">
<link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap-theme.min.css">
<script type="text/javaScript" src="/Mighty/3rd/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="https://xinternet.github.io/mx5/dev/apps/sm/js/SM900009.js?v=<%=new java.util.Date().getTime()%>"></script>

<style type="text/css">
  #pageLayout { overflow-x: hidden; }
  ul { margin-left: 20px; }
  li { margin-bottom: 6px; font-size: 1.2em; }

  .nav>li>a { padding: 4px 10px; }
  .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover { font-weight: 700; }
  .nav-tabs>li.modgrid.active>a, .nav-tabs>li.modgrid.active>a:focus, .nav-tabs>li.modgrid.active>a:hover { background-color: #d9edf7; border-color: #bce8f1; border-bottom: 1px solid #d9edf7; color: #31708f; }
  .nav-tabs>li.newgrid.active>a, .nav-tabs>li.newgrid.active>a:focus, .nav-tabs>li.newgrid.active>a:hover { background-color: #fcf8e3; border-color: #faebcc; border-bottom: 1px solid #fcf8e3; color: #8a6d3b; }
  .nav-tabs>li.textgrid.active>a, .nav-tabs>li.textgrid.active>a:focus, .nav-tabs>li.textgrid.active>a:hover { background-color: #f2dede; border-color: #ebccd1; border-bottom: 1px solid #f2dede; color: #a94442; }
  .nav-tabs>li.sqlgrid.active>a, .nav-tabs>li.sqlgrid.active>a:focus, .nav-tabs>li.sqlgrid.active>a:hover { background-color: #dff0d8; border-color: #d6e9c6; border-bottom: 1px solid #dff0d8; color: #3c763d; }

</style>

<div class="row row-1 mb10">
  <div class="col-xs-12">
    <ul id="tab-guide" class="nav nav-tabs tab-title" role="tablist">
      <li role="presentation" class="modgrid active">
        <a href="#guide-content" id="guide-index" aria-controls="guide-content" role="tab" data-toggle="tab">목차</a>
      </li>
    </ul>

    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="guide-content">
        <iframe src="https://xinternet.github.io/mx5/docs/index.html?v=<%=new java.util.Date().getTime()%>" frameborder="0" width="100%" height="100%"></iframe>
      </div>
    </div>
  </div>
</div>

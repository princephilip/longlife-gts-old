<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap-theme.min.css">
<script type="text/javaScript" src="/Mighty/3rd/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<style type="text/css">
  #pageLayout { overflow-x: hidden; overflow-y: auto; }
  #jsp-src  { height: 200px; }
  #script  { height: 200px; }
</style>

<div class="row">
  <div class="col-md-12">
    <div class='option_label w50'>시스템</div>
    <div id='di_12' class='option_input_bg w130'>
      <html:select name="S_SYS_ID" id = "S_SYS_ID" comCodeYn="0" codeGroup="sm|CodeCom|SM_AUTH_SYS_R01" params = "" dispStyle="1" titleCode="전체" titleValue="%"></html:select>
    </div>

    <div class='option_label w100'>검색</div>
    <div class='option_input_bg w200'>
      <input id='S_FIND' type='text' class='option_input_fix' style="width:190px">
    </div>

    <div class="option_input_bg" >
      <button class='btn btn-warning btn-xs' >새로고침</button>
    </div>

    <div id='xbtns'>
      <html:authbutton id='buttons' grid='dg_1' pgmcode="SM900070"></html:authbutton>
    </div>

    <div class="clearfix"></div>
  </div>

</div>

<div id="pre_view" class="row-fluid" style="clear: both; padding-top: 6px;">
</div>


</div> <!-- #pageLayout 밖에 Modal 놓기 위한 Trick -->


<!-- Modal -->
<div class="modal fade" id="srcModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="srcModalLabel">SRC</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12">

            <textarea id="jsp-src" class="form-control" placeholder="JSP 구문이 표시됩니다."></textarea>

          </div><!-- .col-xs-12 -->

        </div><!-- .row -->
        <div class="row">
          <div class="col-xs-12">

            <textarea id="script" class="form-control" placeholder="SCRIPT 구문이 표시됩니다."></textarea>

          </div><!-- .col-xs-12 -->

        </div><!-- .row -->
      </div><!-- .modal-body -->
      <div class="modal-footer">
        <div class="text-center">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <!-- button type="button" id="btn-modified-sql" class="btn btn-primary">수정 SQL 적용</button -->
        </div>
      </div>
    </div>
  </div>


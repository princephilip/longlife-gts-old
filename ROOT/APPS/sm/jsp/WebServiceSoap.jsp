<%@ page language="java" import="java.io.*, java.net.*, java.util.*, org.apache.soap.*, org.apache.soap.encoding.*, org.apache.soap.encoding.soapenc.*, org.apache.soap.rpc.*, org.apache.soap.util.xml.*" %>
<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>

<%
/**
  * @Class Name : WebService.jsp
  * @Description : SAP IF 웹서비스
  * @Modification Information
  * @
  * @  수정일        		 수정자                   수정내용
  * @ -------    --------    ---------------------------
  * @ 2015.04.06    김성한        최초 생성
  * @
  */

	String wstype = request.getParameter("WSTYPE");
	String url = "";
	String name = "";
	Vector params = new Vector();

	if(wstype.equals("1")) {
		name = "EPMS_FI_0003_WS_PROVIDER";
		url = "http://10.10.148.45:7777/ws/SAP_FI_01.EPMS.EPMS_FI_0003.webservice.provider:EPMS_FI_0003_WS_PROVIDER";
		params.addElement(new Parameter("KUNNR", String.class, request.getParameter("KUNNR"), null));
		params.addElement(new Parameter("NAME1", String.class, request.getParameter("NAME1"), null));
		params.addElement(new Parameter("KTOKD", String.class, request.getParameter("KTOKD"), null));
		params.addElement(new Parameter("SORTL", String.class, request.getParameter("SORTL"), null));
		params.addElement(new Parameter("PSTLZ", String.class, request.getParameter("PSTLZ"), null));
		params.addElement(new Parameter("ORT01", String.class, request.getParameter("ORT01"), null));
		params.addElement(new Parameter("STRAS", String.class, request.getParameter("STRAS"), null));
		params.addElement(new Parameter("TELF1", String.class, request.getParameter("TELF1"), null));
		params.addElement(new Parameter("TELFX", String.class, request.getParameter("TELFX"), null));
		params.addElement(new Parameter("SMTP_ADDR", String.class, request.getParameter("SMTP_ADDR"), null));
		params.addElement(new Parameter("STCD4", String.class, request.getParameter("STCD4"), null));
		params.addElement(new Parameter("J_1KFREPRE", String.class, request.getParameter("J_1KFREPRE"), null));
		params.addElement(new Parameter("J_1KFTIND", String.class, request.getParameter("J_1KFTIND"), null));
		params.addElement(new Parameter("J_1KFTBUS", String.class, request.getParameter("J_1KFTBUS"), null));
		params.addElement(new Parameter("STCD1", String.class, request.getParameter("STCD1"), null));
		params.addElement(new Parameter("STCD2", String.class, request.getParameter("STCD2"), null));
		params.addElement(new Parameter("STCD5", String.class, request.getParameter("STCD5"), null));
		params.addElement(new Parameter("STCD3", String.class, request.getParameter("STCD3"), null));
		params.addElement(new Parameter("REMARK", String.class, request.getParameter("REMARK"), null));
		params.addElement(new Parameter("BRSCH", String.class, request.getParameter("BRSCH"), null));
		params.addElement(new Parameter("SPERR", String.class, request.getParameter("SPERR"), null));
		params.addElement(new Parameter("MOBNO", String.class, request.getParameter("MOBNO"), null));
		params.addElement(new Parameter("MOBNM", String.class, request.getParameter("MOBNM"), null));
		params.addElement(new Parameter("PERNR", String.class, request.getParameter("PERNR"), null));
		params.addElement(new Parameter("FNMHG", String.class, request.getParameter("FNMHG"), null));
		params.addElement(new Parameter("CRDAT", String.class, request.getParameter("CRDAT"), null));
		params.addElement(new Parameter("BANKL1", String.class, request.getParameter("BANKL1"), null));
		params.addElement(new Parameter("BANKN1", String.class, request.getParameter("BANKN1"), null));
		params.addElement(new Parameter("KOINH1", String.class, request.getParameter("KOINH1"), null));
		params.addElement(new Parameter("BVTYP1", String.class, request.getParameter("BVTYP1"), null));
		params.addElement(new Parameter("BKREF1", String.class, request.getParameter("BKREF1"), null));
		params.addElement(new Parameter("BANKL2", String.class, request.getParameter("BANKL2"), null));
		params.addElement(new Parameter("BANKN2", String.class, request.getParameter("BANKN2"), null));
		params.addElement(new Parameter("KOINH2", String.class, request.getParameter("KOINH2"), null));
		params.addElement(new Parameter("BVTYP2", String.class, request.getParameter("BVTYP2"), null));
		params.addElement(new Parameter("BKREF2", String.class, request.getParameter("BKREF2"), null));
		params.addElement(new Parameter("BANKL3", String.class, request.getParameter("BANKL3"), null));
		params.addElement(new Parameter("BANKN3", String.class, request.getParameter("BANKN3"), null));
		params.addElement(new Parameter("KOINH3", String.class, request.getParameter("KOINH3"), null));
		params.addElement(new Parameter("BVTYP3", String.class, request.getParameter("BVTYP3"), null));
		params.addElement(new Parameter("BKREF3", String.class, request.getParameter("BKREF3"), null));
		params.addElement(new Parameter("CHGDATA", String.class, request.getParameter("CHGDATA"), null));
	}
	else if(wstype.equals("2")) {
		name = "CO_EPMS_0003_WS_PROVIDER";
		url = "http://10.10.148.45:7777/ws/SAP_CO_01.EPMS.CO_EPMS_0003.webservice.provider:CO_EPMS_0003_WS_PROVIDER";
		params.addElement(new Parameter("BUSSNO", String.class, request.getParameter("BUSSNO"), null));
	}
	else if(wstype.equals("3")) {
		name = "CO_EPMS_0005_WS_PROVIDER";
		url = "http://10.10.148.45:7777/ws/SAP_CO_01.EPMS.CO_EPMS_0005.webservice.provider:CO_EPMS_0005_WS_PROVIDER";
		params.addElement(new Parameter("BUSSNO", String.class, request.getParameter("BUSSNO"), null));
		params.addElement(new Parameter("PSPID", String.class, request.getParameter("PSPID"), null));
	}
	else if(wstype.equals("4")) {
		name = "CO_EPMS_0006_WS_PROVIDER";
		url = "http://10.10.148.45:7777/ws/SAP_CO_01.EPMS.CO_EPMS_0006.webservice.provider:CO_EPMS_0006_WS_PROVIDER";
		params.addElement(new Parameter("BUSSNO", String.class, request.getParameter("BUSSNO"), null));
		params.addElement(new Parameter("PSPID", String.class, request.getParameter("PSPID"), null));
		params.addElement(new Parameter("POSID", String.class, request.getParameter("POSID"), null));
		params.addElement(new Parameter("ZVERN", String.class, request.getParameter("ZVERN"), null));
	}

	URL webServiceUrl = new URL(url);
	SOAPMappingRegistry smr = new SOAPMappingRegistry();
	StringDeserializer sd = new StringDeserializer();
	smr.mapTypes(Constants.NS_URI_SOAP_ENC, new QName("", "return"), null, null, sd);
	Call webServiceCall = new Call();
	webServiceCall.setSOAPMappingRegistry(smr);
	webServiceCall.setMethodName(name);
	webServiceCall.setTargetObjectURI(url);
	webServiceCall.setEncodingStyleURI(Constants.NS_URI_SOAP_ENC);
	webServiceCall.setParams(params);

	try{
		Response resp = webServiceCall.invoke(webServiceUrl, "");

		if(!resp.generatedFault()) {
			Parameter ret = resp.getReturnValue();
			Object value = ret.getValue();
			out.println(value);
		}
		else {
			Fault fault = resp.getFault();
			out.println(fault.getFaultCode());
			out.println(fault.getFaultString());
		}
	}
	catch(SOAPException e) {
		out.println("(" + e.getFaultCode() + ") " + e.getMessage());
	}
%>

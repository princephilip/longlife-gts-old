<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>

<div id='topsearch' class='option_box2'>
	<div class='option_label_left'>시스템</div>
	<div id='oi_11' class='option_input_bg'>
		<html:select name="S_SYS_ID" id = "S_SYS_ID" comCodeYn="0" codeGroup="sm|CodeCom|SM_AUTH_SYS_R01" params = "" dispStyle="1" titleCode="선택"></html:select>
	</div>
	<div class='option_label'>칼럼명</div>
	<div id='oi_12' class='option_input_bg w200'>
		<input id='S_PGM_CODE' type='text' class='option_input'>
	</div>
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true" pgmcode="SM900200"></html:authbutton>
		</span>
	</div>
</div>

<div id='grid_1' class='grid_div'>
	<div id="dg_1" class="slick-grid"></div>
</div>
<div style='float:left;width:100%;height:23px;margin-top:2px;margin-bottom:2px;line-height:23px;text-align:right;'>
	<div id='grid_chbtn' style='float:right;'>
		<html:authchildbutton id='buttons' grid='dg_2' append='true' insert='true' duplicate='true' delete='true' excel='false' retrieve='true'></html:authchildbutton>
	</div>
	<div style='float:right;margin-right:10px;'>
		<input id='S_COL_CNT' type='text' style='width:16px; text-align:center;' value='4'>
		<html:button name='creategrid' id='creategrid' type='button' desc="Grid파일생성" onClick="pf_creategrid();"></html:button>
		<html:button name='createdetail' id='createdetail' type='button' desc="Detail구문생성" onClick="pf_createdetail();"></html:button>
		<html:button name='createdetail2' id='createdetail2' type='button' desc="Detail구문생성2" onClick="pf_createdetail2();"></html:button>
		<html:button name='gridpreview' id='gridpreview' type='button' desc="Grid미리보기" onClick="pf_gridpreview();"></html:button>
		<html:button name='applytobase' id='applytobase' type='button' desc="표준으로적용" onClick="pf_applytobase();"></html:button>
	</div>
</div>

<div id='grid_2' class='grid_div'>
	<div id="dg_2" class="slick-grid"></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};
	grid_1.ResizeInfo = {init_width:1000, init_height:237, anchor:{x1:1,y1:1,x2:1,y2:0.5}};
	grid_2.ResizeInfo = {init_width:1000, init_height:390, anchor:{x1:1,y1:1,x2:1,y2:0.5}};

	//di_11.ResizeInfo = {init_width:160,  init_height:0, anchor:{x1:0,y1:0,x2:0,y2:0}};
	//di_12.ResizeInfo = {init_width:640,  init_height:0, anchor:{x1:1,y1:0,x2:1,y2:0}};
	S_SYS_ID.ResizeInfo = {init_width:150,  init_height:0, anchor:{x1:0,y1:0,x2:0,y2:0}};
</script>
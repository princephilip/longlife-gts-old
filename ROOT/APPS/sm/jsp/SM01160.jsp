<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<script>
	var vPgm = "<%= request.getParameter("pgm")%>";
	var vSys = "<%= request.getParameter("sys")%>";
</script>

<style>
  .wrap-btn-grids { padding-top: 2px; min-width: 100px; }
  .wrap-btn-grids .btn { margin-left: 6px; }
  .modal-newEmp { width: 280px !important; }
</style>


<script type="text/javaScript" src="/Mighty/3rd/bootstrap/js/bootstrap.min.js"></script>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<html:authbutton id='buttons' grid='dg_1' pgmcode="SM01160"></html:authbutton>
	</div>


  <div class='option_label'>시스템</div>
  <div class='option_input_bg'>
    <html:select name="S_SYS_ID" id = "S_SYS_ID" comCodeYn="0" codeGroup="sm|SM_AUTH_SYS|W_SM0915_01" params="${companyCode}" dispStyle="1" selected="20" showTitle="true" ></html:select>
  </div>

  <label class='option_label'>메뉴</label>
  <div class='option_input_bg'>
    <select id="S_PGM_LEV"></select>
  </div>

</div>


<div id='grid_1' class='grid_div mr5 has_title'>
	<label class='sub_title'>메뉴명</label>
	<div id="dg_1" class="slick-grid"></div>
</div>

<div id='freeform_01' class='detail_box ver2 has_title mr5'>
	<label class='sub_title'>메뉴 상세</label>
	<div class='detail_row'>
		<label class='detail_label w90'>메뉴설명</label>
		<div class='detail_input_bg'><input type='text' id='MENU_CONTENT' ></div>
	</div>
	<div class='detail_row'>
		<label class='detail_label w90'>메뉴위치</label>
		<div class='detail_input_bg'><input type='text' id='MENU_PATH' ></div>
	</div>
	<div class='detail_row detail_row_bb'>
		<label class='detail_label w90'>작성순서</label>
		<div class='detail_input_bg'><input type='text' id='MENU_ORDER' ></div>
	</div>
</div>

<div id='grid_2' class='grid_div has_title has_button' >
	<label class='sub_title'>항목별 작성법</label>
	<div class='child_button float_right'>
		<html:authchildbutton id='buttons' grid='dg_2' append='true' insert='false' duplicate='false' delete='true' excel='false' retrieve='false' ></html:authchildbutton>
	</div>
	<div id="dg_2" class='slick-grid'></div>
</div>

<div id='freeform_02' class='detail_box ver2 has_title mt10'>
	<label class='sub_title'>화면 구성</label>
	<div class='grid_div' style='border:1px solid #b4d5e8;'>
		<img id="photo_1" src="/Theme/images/noimage.jpg" onclick="pf_ImageUpload(this)" align="middle;" style="border:0; width:713px; height:408px;cursor:pointer">
	</div>
</div>

<div id='grid_3' class='grid_div has_title has_button mt10' >
	<label class='sub_title'>특기사항</label>
	<div class='child_button float_right'>
		<html:authchildbutton id='buttons' grid='dg_3' append='true' insert='false' duplicate='false' delete='true' excel='false' retrieve='false' ></html:authchildbutton>
	</div>
	<div id="dg_3" class='slick-grid'></div>
</div>




</div>




<script type="text/javascript">

		topsearch.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};

		grid_1.ResizeInfo = {init_width:340, init_height:665, width_increase:0, height_increase:1};

		freeform_01.ResizeInfo = {init_width:660, init_height:115, width_increase:1, height_increase:0};

		grid_2.ResizeInfo = {init_width:660, init_height:245, width_increase:1, height_increase:0};

		freeform_02.ResizeInfo = {init_width:725, init_height:445, width_increase:0, height_increase:0};
		grid_3.ResizeInfo = {init_width:-65, init_height:445, width_increase:1, height_increase:0};



/*		topsearch.ResizeInfo = {init_width:1000, init_height:33, anchor:{x1:1,y1:0,x2:1,y2:0}};

		grid_1.ResizeInfo = {init_width:-400, init_height:665, width_increase:1, height_increase:1};

		freeform_01.ResizeInfo = {init_width:1388, init_height:115, width_increase:0, height_increase:0};

		grid_2.ResizeInfo = {init_width:1388, init_height:365, width_increase:0, height_increase:0};

		freeform_02.ResizeInfo = {init_width:623, init_height:185, width_increase:0, height_increase:0};
		grid_3.ResizeInfo = {init_width:765, init_height:185, width_increase:0, height_increase:1};*/



</script>
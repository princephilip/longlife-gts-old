
<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2'>
	<div class='option_label w100'>그룹명</div>
	<div id='di_11' class='option_input_bg'>
		<input id='S_FIND' type='text' class='option_input_fix w200'>
	</div>
	<div id='xbtns'>
		<html:authbutton id='buttons' grid='dg_1' pgmcode="SM0930"></html:authbutton>
	</div>

	<div id='userxbtns' style='height:23px;'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<button type="button" id='auth_copy' class="btn btn-warning btn-xs" onClick="pf_UserFind();">권한 복사</button>
		</span>
	</div>
</div>

<div id='grid_1' class='grid_div mr5 has_title' >
	<label class='sub_title'>그룹권한 리스트</label>
	<div id="dg_1" class="slick-grid"></div>
</div>

<div id='grid_2' class='grid_div mr5 has_title' >
	<label class='sub_title'>프로그램 권한 설정
			<div style="float:right;margin-top:4px;">
				<div class='option_label'>시스템</div>
				<div class='option_input_bg'>
					<html:select name="S_SYS_ID" id = "S_SYS_ID" comCodeYn="0" codeGroup="sm|SM_AUTH_SYS|W_SM0915_01" params="${companyCode}" dispStyle="1" selected="20" showTitle="true" ></html:select>
				</div>
			</div>
	</label>
	<div id="dg_2" class="slick-grid"></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:33 , width_increase:1, height_increase:0};
	grid_1.ResizeInfo 	 = {init_width:600, init_height:655,  width_increase:0, height_increase:1};
	grid_2.ResizeInfo  	 = {init_width:400, init_height:655,  width_increase:1, height_increase:1};
</script>
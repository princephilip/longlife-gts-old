﻿
<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<html:authbutton id='buttons' grid='dg_1' pgmcode="SM0920"></html:authbutton>
	</div>
</div>


<div id='grid_1' class='grid_div has_title'>
	<label class="sub_title">실행권한 목록</label>
	<div id="dg_1" style="width:100%; height:100%; " class="slick-grid"></div>
</div>

<div id='grid_2' class='grid_div has_title ml5'>
	<label class="sub_title">권한이 부여된 사용자
		<div style="float:right;margin-top:4px;">
			<button type="button" id='btn_deselect' class="btn btn-danger btn-xs" onClick="uf_user_del();">삭제</button>
		</div>
	</label>
	<div id="dg_2" style="width:100%; height:100%; " class="slick-grid"></div>
</div>

<div id='grid_3' class='grid_div has_title ml5'>
	<label class='sub_title'>사용자 선택

		<div style="float:right;margin-top:4px;">
            <button type="button" id='btn_select' class="btn btn-success btn-xs" onClick="uf_user_add();">추가</button>
		</div>
		<div style="float:right;margin-top:0px;">
			<div class='option_label' >통합검색</div>
			<div class="option_input_bg" >
				<input type="text" id="S_FIND" class="detail_input_fix W200">
			</div>
		</div>
    </label>

	<div id="dg_3" style="width:100%; height:100%; " class="slick-grid"></div>
</div>

<script type="text/javascript">

	topsearch.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};
	grid_1.ResizeInfo = {init_width:400, init_height:664, width_increase:0, height_increase:1};
	grid_2.ResizeInfo = {init_width:500, init_height:664, width_increase:0, height_increase:1};
	grid_3.ResizeInfo = {init_width:100, init_height:664, width_increase:1, height_increase:1};

</script>

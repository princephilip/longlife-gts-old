<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box'>
  <div class='option_label w50'>시스템</div>
  <div id='di_12' class='option_input_bg w130'>
    <html:select name="S_SYS_ID" id = "S_SYS_ID" comCodeYn="0" codeGroup="sm|CodeCom|SM_AUTH_SYS_R01" params = "" dispStyle="1" titleCode="전체" titleValue="%"></html:select>
  </div>

	<div class='option_label w100'>검색</div>
	<div class='option_input_bg w200'>
		<input id='S_FIND' type='text' class='option_input_fix' style="width:190px">
	</div>

	<div id='xbtns'>
		<html:authbutton id='buttons' grid='dg_1' pgmcode="SM900071"></html:authbutton>
	</div>

</div>

<div id='grid_1' class='grid_div mr5'>
	<div id="dg_1" style="width:100%; height:100%;" class="slick-grid"></div>
</div>

<div id='freeform1' class='detail_box'>
	<div class='detail_label w100'>시스템</div>
	<div class='detail_input_bg'>
		<input id='SYS_ID' type='text' class='detail_input_fix' style='width:385px;' maxlength="255" >
	</div>

	<div class='detail_label w100'>코드명</div>
	<div class='detail_input_bg'>
		<input id='NAME' type='text' class='detail_input_fix' style='width:385px;' maxlength="255" >
	</div>
	<div class='detail_line'></div>
	<div class='detail_label' style='width:500px;' >Html_Tag_Source 입력란 (공통이니 수정주의요망!)</div>
	<textarea id="HTML_STRING" class='detail_input_fix'  style="width:100%; height:120;"></textarea>
	<div class='detail_line'></div>
	<div class='detail_label' style='width:500px;' >실제사용 스크립트</div>
	<textarea id="SCRIPT_STRING" class='detail_input_fix'  style="width:100%; height:120;"></textarea>
</div>

<h1>※ 미리보기</h1>
<div id='pre_view' class='detail_box'>

</div>

<script type="text/javascript">
	topsearch.ResizeInfo 	= {init_width:1000, init_height:33, width_increase:1, height_increase:0};
	grid_1.ResizeInfo 		= {init_width:500, init_height:665, width_increase:1, height_increase:1};
	freeform1.ResizeInfo = {init_width:500, init_height:365, width_increase:0, height_increase:0};
	pre_view.ResizeInfo = {init_width:500, init_height:300, width_increase:0, height_increase:0};
</script>


<!--
CREATE TABLE SBTEC.SM_DEV_RETR_T1
(
  CODE               VARCHAR2(5 BYTE)           NOT NULL,
  SYS_ID             VARCHAR2(4 BYTE),
  NAME               VARCHAR2(255 BYTE),
  HTML_STRING        VARCHAR2(1000 BYTE),
  HTML_STRING_VIEW   VARCHAR2(1000 BYTE),
  USE_YN             VARCHAR2(1 BYTE),
  REMK               VARCHAR2(1000 BYTE),
  ROW_INPUT_DATE     DATE,
  ROW_INPUT_EMP_NO   VARCHAR2(12 BYTE),
  ROW_INPUT_IP       VARCHAR2(20 BYTE),
  ROW_UPDATE_DATE    DATE,
  ROW_UPDATE_EMP_NO  VARCHAR2(12 BYTE),
  ROW_UPDATE_IP      VARCHAR2(20 BYTE),
  PRIMARY KEY
  (CODE)
);

COMMENT ON COLUMN SM_DEV_RETR_T1.CODE IS '키코드';
COMMENT ON COLUMN SM_DEV_RETR_T1.SYS_ID IS '시스템코드';
COMMENT ON COLUMN SM_DEV_RETR_T1.NAME IS '사용명칭';
COMMENT ON COLUMN SM_DEV_RETR_T1.HTML_STRING IS 'HTML소스_스트링';
COMMENT ON COLUMN SM_DEV_RETR_T1.HTML_STRING_VIEW IS 'HTML소스_스트링_뷰(임의컬럼)';
COMMENT ON COLUMN SM_DEV_RETR_T1.USE_YN IS '사용여부';
COMMENT ON COLUMN SM_DEV_RETR_T1.REMK IS '비고';
-->
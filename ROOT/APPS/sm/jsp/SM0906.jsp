<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2'>
	<div id='xbtns' style='height:1px;'>
		<span style='width:100%;height:100%;vertical-align:middle;'>
			<html:authbutton id='buttons' grid='dg_1' gridCall='true' pgmcode="SM0906"></html:authbutton>
		</span>
	</div>

	<!-- <div id='userxbtns' style='height:23px;'>
			<span style="width:100%;height:100%;vertical-align:middle;">
			  <button type="button" id="pass_new" class="btn btn-warning btn-xs" onClick="pf_resetPassword();">비밀번호 초기화</button>
			</span>
	</div> -->

	<div class='option_label' style="width:120px;">찾기(이름/아이디)</div>
	<div class='option_input_bg w150'>
		<input id='S_USER_ID' type='text' class='option_input_fix w140'>
	</div>

	<div class="option_label w100">구분</div>
	<div class="option_input_bg">
		<select id="S_CUST_CODE" style="width:100px"></select>
		<button type="button" id="pass_new" class="btn btn-warning btn-xs" onClick="uf_changeuser();">선택변경</button>
	</div>

</div>

<div id='grid_1' class='grid_div mr5'>
	<div id="dg_1" style="width:100%; height:100%; " class="slick-grid"></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:33, anchor:{x1:1,y1:1,x2:1,y2:0}};
	grid_1.ResizeInfo    = {init_width:1000, init_height:665, anchor:{x1:1,y1:1,x2:1,y2:1}};
</script>
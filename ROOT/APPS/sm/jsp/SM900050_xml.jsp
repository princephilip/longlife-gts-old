<%@ page language="java" contentType="text/xml; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.io.*"%>
<%@ page import="javax.xml.parsers.*"%>

<%
request.setCharacterEncoding("utf-8");

String sys_id = request.getParameter("sys_id");
String fileName = request.getParameter("fname");
String qryId = request.getParameter("qid");

String fileDir = "APPS\\" + sys_id + "\\query\\" ;  //파일경로
String filePath = request.getRealPath("/")+fileDir+fileName.toString()+".xmlx";

//out.println(filePath);

try{
  // 파일읽기
  FileReader fr = new FileReader(filePath); //파일읽기객체생성
  BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "utf-8")); //new BufferedReader(fr); //버퍼리더객체생성
  String line = null;
  String line2 = null;

  while((line=br.readLine())!=null){ //라인단위 읽기
    line2 = line.trim(); // remove leading and trailing whitespace
    if (!line2.equals("")) // don't write out blank lines
    {
      out.println(line);
      //out.println(new String(line.getBytes(),"utf-8"));
    }
  }
}catch (IOException e) { 
  System.out.println(e.toString()); //에러 발생시 메시지 출력
}

%>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<style>
.sms-phone {
  background: url(/Theme/images/mobile.jpg) top left no-repeat;
  width: 300px;
  height: 590px;
  margin: 20px 50px 0 50px;
  padding: 85px 40px;
}
.btn-favorite {
  margin-bottom: 6px;
  text-align: right;
}
.sender-no {
  margin-bottom: 15px;
}
#txtSmsMsg {
  border: 1px dashed #ccc;
  width: 215px;
  height: 260px;
}
.msg-length {
  text-align: center;
  padding: 6px 0 12px 0;
}
.btn-sms {
  text-align: center;
}
#sms-title {
  width: 134px;
}

#append {
	  background-image: url(/theme/images/btn/buttons_hi3.png) !important;
    font-size: 11px;
    color: #454545;
    border: 0;
    border-radius: 0;
    text-indent: 0;
    text-align: center;
    background: none;
    background-color: #fff;
    width: 54px;
    height: 23px !important;
    opacity: 1 !important;
}

#delete {
	  background-image: url(/theme/images/btn/buttons_hi3.png) !important;
    font-size: 11px;
    color: #454545;
    border: 0;
    border-radius: 0;
    text-indent: 0;
    text-align: center;
    background: none;
    background-color: #fff;
    width: 54px;
    height: 23px !important;
    opacity: 1 !important;
}
#append:focus {
  outline: none;
}
#append { background-position: 0 0;  }
#append:hover { background-position: -60px 0;  }
#append:active { background-position:  0 -30px;  }

#delete {	background-position: 0 -90px; }
#delete:hover {	background-position: -120px 0; }
#delete:active {	background-position: -60px -90px; }


</style>

<div id='topsearch' >
  <div id='xbtns'>
    <span style="width:100%;height:100%;vertical-align:middle;">
      <html:authbutton id='buttons' grid='dg_1' gridCall="true" pgmcode="SM900400"></html:authbutton>
    </span>
  </div>

</div>

<div id='grid_1' class='grid_div mr5'>
  <div style="height: 20px; color: #4374D9; font-weight: bold; line-height: 20px;">
    <img src='${pageContext.request.contextPath}/Theme/images/btn/tit_icon_pop.gif' style="margin-top: -2px;">&nbsp; 문자전송 대상자1
    <span style="float:right; margin-bottom: 0px;">
     <!-- <html:button name='btn-favorite-msg' id='btn-favorite-msg' type='button' icon='ui-icon-arrowthickstop-1-s' desc="직원" onClick="pf_get_sawon();"></html:button>
      <html:button name='btn-favorite-msg' id='btn-favorite-msg' type='button' icon='ui-icon-arrowthickstop-1-s' desc="하자신청자" onClick="pf_get_haja();"></html:button> -->
      <html:button name='btn-favorite-msg' id='append' type='button'   onClick="pf_get_insert();"></html:button>
      <html:button name='btn-favorite-msg' id='delete' type='button' onClick="pf_get_delete();"></html:button>
    </span>
  </div>
  <div id="dg_1" style='float:left; width:100%; height:100%;' class='slick-grid'></div>
</div>

<div id='grid_2' class='grid_div'>
  <div class='sms-phone'>
    <div class='btn-favorite'>
      <!-- <html:button name='btn-favorite-msg' id='btn-favorite-msg' type='button' icon='ui-icon-arrowthickstop-1-s' desc="자주사용하는 문자열" onClick="pf_get_message();"></html:button> -->
    </div>
    <textarea id='txtSmsMsg' onkeydown='pf_chk_len(this.value)' maxLength='999'></textarea>
    <div class='msg-length'>총 0 / 80 byte</div>
    <div class='sender-no'>
      받는 번호 :
      <input id='sms-title' type='text' class='option_input_fix'>
    </div>
    <div class='sender-no'>
      보내는 번호 :
      <input id='sender-phone-no' type='text' class='option_input_fix' readonly>
    </div>
    <div class='btn-sms'>
      <html:button name='btn-send-sms' id='btn-send-sms' type='button' icon='ui-icon-mail-closed' desc="전송" onClick="pf_send_sms();"></html:button>
      <html:button name='btn-cancel-sms' id='btn-cancel-sms' type='button' icon='ui-icon-close' desc="취소" onClick="pf_cancel_sms();"></html:button>
    </div>
  </div>
</div>

<script type="text/javascript">
  topsearch.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};
  grid_1.ResizeInfo = {init_width:600, init_height:641, width_increase:1, height_increase:1};
  grid_2.ResizeInfo = {init_width:400, init_height:153, width_increase:0, height_increase:0};
</script>


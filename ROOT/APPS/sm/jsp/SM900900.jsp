<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%
/**
 *<pre>
 * Statements
 *</pre>

 * @Class Name : SM0201.jsp
 * @Descriptio    시스템코드 관리
 * @Modification Information
 * @
 * @  수정일            수정자                   수정내용
 * @ -------    --------    ---------------------------
 * @ 2013.03.03    박영찬        최초 생성
 * @
 */
%>
<jsp:include page="/Mighty/jsp/UploadExcelForm.jsp"/>


<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<script type="text/javascript" src="/Theme/js/temp_common.js"></script>

<script>

</script>

<div class="x5-row">
  <div class="pull-right">
    <html:authbutton id='buttons' grid='dg_1' pgmcode="SM900900"></html:authbutton>
  </div>

  <div class="pull-right">
    <html:button name='sourcecopy' id='sourcecopy' type='button' desc="<i class='fa fa-save'></i> excel upload" onClick="pf_ExcelExport();"></html:button>
    <button class="btn btn-default btn-sm" onClick="pf_excelupload();">
	     <i class="fa fa-save"></i> pf_excelImport
		</button>
  </div>
</div>
<!--
<div class='x5-h-fill'>
	<div class='x5-w-505'>
		<div id='grid_1' class='x5-w-500'>
		  <div id="dg_1" class='slick-grid'></div>
		</div>
	</div>

	<div id='grid_2' class='x5-w-fill'>
	  <div id="dg_2" class='slick-grid'></div>
	</div>
</div>
-->

<div class='x5-h-fill'>
	<div class='x5-w-500'>
		<div id='grid_1' class='x5-margin-r x5-margin-b'>
		  <div id="dg_1" class='slick-grid'></div>
		</div>
	</div>

	<div class='x5-w-fill'>
		<div id='grid_2' class='x5-margin-b'>
		  <div id="dg_2" class='slick-grid'></div>
		</div>
	</div>
</div>

<div class='x5-h-fill'>
	<div class='x5-w-fill'>
		<div id='grid_3' class='x5-margin-b'>

	  	<div id="dg_3" class='slick-grid'></div>
	  </div>
	</div>
</div>
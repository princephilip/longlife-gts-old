<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script src="/Mighty/3rd/underscore/underscore-min.js"></script>
<script src="/Mighty/3rd/codemirror/lib/codemirror.js"></script>
<link rel="stylesheet" href="/Mighty/3rd/codemirror/lib/codemirror.css">
<script src="/Mighty/3rd/codemirror/mode/sql/sql.js"></script>
<link rel="stylesheet" href="/Mighty/3rd/codemirror/addon/hint/show-hint.css" />
<script src="/Mighty/3rd/codemirror/addon/hint/show-hint.js"></script>
<script src="/Mighty/3rd/codemirror/addon/hint/sql-hint.js"></script>

<link rel="stylesheet" href="/Mighty/3rd/font-awesome/css/all.css">
<link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/Mighty/3rd/bootstrap/css/bootstrap-theme.min.css">
<script type="text/javaScript" src="/Mighty/3rd/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="/Mighty/3rd/typeahead/handlebars.js"></script>
<link rel="stylesheet" href="/Mighty/3rd/typeahead/typeahead.css" />
<script type="text/javascript" src="/Mighty/3rd/typeahead/bloodhound.js"></script>
<script type="text/javascript" src="/Mighty/3rd/typeahead/typeahead.bundle.min.js"></script>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<style type="text/css">
  #pageLayout { overflow-x: hidden; }
  #search-pgmcode { padding-top: 0px; }
  #search-pgmcode .twitter-typeahead { width: 100%; }
  #search-pgmcode .typeahead { padding: 6px 12px; height: 28px; line-height: 20px; width: 100%; }
  .tab-content textarea { font-size: 12px; white-space:pre-wrap; }
  .row-1 { }
  .row-2 { } /* 72% */
  .row-3 { margin-bottom: 10px; }
  .row-4 { height: 34%; padding-top: 30px; }
  .form-group { margin-bottom: 10px; }

  .typeahead, .tt-query, .tt-hint { font-size: 12px; }

  .nav>li>a { padding: 4px 10px; }
  .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover { font-weight: 700; }
  .nav-tabs>li.modgrid.active>a, .nav-tabs>li.modgrid.active>a:focus, .nav-tabs>li.modgrid.active>a:hover { background-color: #d9edf7; border-color: #bce8f1; border-bottom: 1px solid #d9edf7; color: #31708f; }
  .nav-tabs>li.newgrid.active>a, .nav-tabs>li.newgrid.active>a:focus, .nav-tabs>li.newgrid.active>a:hover { background-color: #fcf8e3; border-color: #faebcc; border-bottom: 1px solid #fcf8e3; color: #8a6d3b; }
  .nav-tabs>li.textgrid.active>a, .nav-tabs>li.textgrid.active>a:focus, .nav-tabs>li.textgrid.active>a:hover { background-color: #f2dede; border-color: #ebccd1; border-bottom: 1px solid #f2dede; color: #a94442; }
  .nav-tabs>li.sqlgrid.active>a, .nav-tabs>li.sqlgrid.active>a:focus, .nav-tabs>li.sqlgrid.active>a:hover { background-color: #dff0d8; border-color: #d6e9c6; border-bottom: 1px solid #dff0d8; color: #3c763d; }

  ul#tab-mode li:first-child { margin-left: 20px !important; }
  ul.nav-tabs { border-bottom: 0px;}
  ul#tab-mode .panel { margin-bottom: 10px; }

  .panel-info .panel-body { background-color: #d9edf7; padding-bottom: 5px; }
  .panel-warning .panel-body { background-color: #fcf8e3; padding-bottom: 5px; }
  .panel-success .panel-body { background-color: #dff0d8; padding-bottom: 5px; }

  ul.tab-title { margin-left: 6px; }
  ul.tab-title.nav>li>a { padding: 2px 5px; }

  #grid-cols-setting input, #grid-cols-setting select { font-size: 11px; padding: 3px 6px; height: 23px; }
  #grid-cols-setting input[type=checkbox] { height: 13px; -webkit-box-shadow: none; box-shadow: none; }
  #grid-cols-setting th { padding: 5px; }
  #grid-cols-setting td { padding: 3px; }
  #grid-cols-setting tr.matched td { background-color: #f5f5f5; }
  #grid-cols-setting tr.notmatched td { background-color: #ffffff; }
  #grid-cols-setting tr.matched td.bg-update, #grid-cols-setting tr.notmatched td.bg-update { background-color: #f2dede; }
  #btn-modified-sql { margin-top: -3px; }

  .wrap-textarea { height: 500px; overflow: hidden; }
  .CodeMirror { border: 1px solid #eee; height: 480px; }
  .fc_false { background-color: #efefef; color: #cdcdcd; }
  .property-wrap .tab-content { height: 200px; overflow-y: auto; }
  .bg-update { background-color: #f2dede; }

  .row-2 .btn-danger { margin-top: -3px; }
  .row-2 #update-table { height: 24px; padding: 2px 6px; margin-top: -3px; }
  .row-4 textarea { height: 100%; }
  .row-4 .nav-tabs { margin-top: -15px; }
  .row-4 .btn-xs { margin-top: -17px; }

  .row-1 label, .row-1 .sql-label, .wrap-updtables .col-xs-2 { padding-right: 0; padding-left: 0; }
  .wrap-updtables input.form-control, .wrap-updtables select.form-control { font-size: 11px; padding: 3px 6px; height: 23px; }
  .row-1 .checkbox.col-xs-1 { margin-right: -60px; z-index: 1; }
  .wrap-updtables select.form-control { font-size: 11px; padding: 3px 0; height: 23px; }
  .wrap-updtables .table td { padding: 0; padding-bottom: 1px; }
  .wrap-params { height: 450px; overflow-y: scroll; }
  #sql-params td { padding: 3px 0; }
  #sql-params td:first-child { padding: 6px 3px 0 3px; }
  .modal-lg { width: 90% !important; }
  .sql-label span { color: #a94442; font-weight: 700; }

  .msg-ver { margin-top: -18px; margin-right: 10px; }
</style>

<div class="row row-1 mb10">
  <div class="col-xs-12">
    <ul id="tab-mode" class="nav nav-tabs tab-title" role="tablist">
      <li role="presentation" class="modgrid active"><a href="#modgrid" id="tab-pgm-name" aria-controls="modgrid" role="tab" data-toggle="tab">프로그램 코드 선택</a></li>
    </ul>
    <div class="msg-ver pull-right">Ver.1.0.0</div>

    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="modgrid">
        <!-- 수정 -->
        <div class="panel panel-info">
          <div class="panel-body">
            <div class="form-horizontal">
              <div class="form-group">
                <label for="pgm-code" class="col-xs-1 control-label">프로그램코드</label>
                <div id="search-pgmcode" class="col-xs-2">
                  <input type="text" class="typeahead" placeholder="프로그램코드 또는 이름">
                </div>
                <div class="col-xs-1">
                  <h5>.Grid.js</h5>
                </div>

                <div class="checkbox col-xs-1 text-right">
                  <label>
                    <input id="check-new-grid" type="checkbox"> 신규
                  </label>
                </div>
                <label for="pgm-code" class="col-xs-1 control-label">Grid Id</label>
                <div id="wrap-grid-ids" class="col-xs-2">
                  <select class="form-control input-sm" id="grid-objs">
                  </select>
                </div>
                <div id="wrap-grid-id" class="col-xs-2">
                  <input id="new-grid-id" type="text" class="form-control input-sm" placeholder="새로운 Grid Id명 예) dg_1 ">
                </div>
                <div id="grid-ids" class="col-xs-4">
                </div>
              </div>
            </div>
            
          </div><!-- panel-body -->          
        </div><!-- panel -->

      </div><!-- tab-pane -->
    </div>

  </div>
</div>


<div class="row row-15 mb10">
  <div class="col-xs-12">
    <ul id="tab-mode" class="nav nav-tabs tab-title" role="tablist">
      <li role="presentation" class="sqlgrid active">
        <a href="#sqlgrid" id="tab-pgm-name" aria-controls="sqlgrid" role="tab" data-toggle="tab">SQL & PARAMS</a>
      </li>
    </ul>

    <div role="tabpanel" class="tab-pane" id="newgrid">
      <div class="panel panel-success">
        <div class="panel-body">

          <div class="row row-3" >
            <div class="col-xs-1 col-xs-offset-1">
              <!-- Button trigger modal -->
              <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#sqlModal">
                SQL 수정
              </button>
            </div>

            <label for="select-id" class="col-xs-3 control-label text-right">Xmlx Select Id</label>
            <div class="col-xs-3">
              <input id="select-id" type="text" class="form-control input-sm" placeholder="SQL ID(xmlx 파일내)">
            </div>
          </div>

        </div><!-- panel-body -->
      </div><!-- panel -->
    </div><!-- tab-pane -->
  </div>

</div>

<div class="row row-2 mb10">
  <div class="col-xs-3">
    <ul class="nav nav-tabs tab-title" role="tablist">
      <li role="presentation" class="textgrid active"><a href="#" >Update 정보 & Grid 칼럼 속성</a></li>
    </ul>
  </div>

  <div class="col-xs-2 text-right">
    <strong>UPDATE TABLE</strong>
  </div>
  <div class="col-xs-2">
    <select class="form-control input-sm" id="update-table">
    </select>
  </div>
  <div class="col-xs-5">
    <button id="beta-go" class="btn btn-xs btn-danger disabled">CODE GENERATE & SAVE</button>
  </div>
  
  <div class="property-wrap col-xs-12">
    <div class="tab-content">
      <table id="grid-cols-setting" class="table">
      </table>
    </div>
  </div>


</div><!-- row-2 -->

<div class="row row-4 mb10">
  <div class="col-xs-3">
    <ul class="nav nav-tabs tab-title pull-left" role="tablist">
      <li role="presentation" class="newgrid active"><a href="#" >QUERY(xmlx)</a></li>
    </ul>
    <button class="btn btn-xs btn-warning pull-right" id="copybtn-code-xmlx">Copy To Clipboard</button>
    <textarea id="code-xmlx" class="form-control" placeholder=""></textarea>
  </div>

  <div class="col-xs-3">
    <ul class="nav nav-tabs tab-title pull-left" role="tablist">
      <li role="presentation" class="newgrid active"><a href="#" >Grid.js</a></li>
    </ul>
    <button class="btn btn-xs btn-warning pull-right" id="copybtn-code-grid">Copy To Clipboard</button>
    <textarea id="code-grid" class="form-control" placeholder=""></textarea>
  </div>

  <div class="col-xs-3">
    <ul class="nav nav-tabs tab-title pull-left" role="tablist">
      <li role="presentation" class="newgrid active"><a href="#" >FreeForm(Old)</a></li>
    </ul>
    <button class="btn btn-xs btn-warning pull-right" id="copybtn-code-freeform">Copy To Clipboard</button>
    <textarea id="code-freeform" class="form-control" placeholder=""></textarea>
  </div>

  <div class="col-xs-3">
    <ul class="nav nav-tabs tab-title pull-left" role="tablist">
      <li role="presentation" class="newgrid active"><a href="#" >FreeForm(New)</a></li>
    </ul>
    <button class="btn btn-xs btn-warning pull-right" id="copybtn-code-freeform2">Copy To Clipboard</button>
    <textarea id="code-freeform2" class="form-control" placeholder=""></textarea>
  </div>

</div><!-- row-2 -->

<div class="row hide"> <!--  hide -->
  <div id="grid_devprop" class="col-xs-6">
    <div id="dg_devprop" style="width:100%; height:100px;" class="slick-grid"></div>
  </div>
  <div id="grid_devpropsrc" class="col-xs-6">
    <div id="dg_devpropsrc" style="width:100%; height:100px;" class="slick-grid"></div>
  </div>
</div>




<!-- 
** TABLE CREATE QUERY 추가 **


CREATE TABLE SM_DEV_GRID(
  SYS_ID              varchar2(4) NOT NULL,
  PGM_CODE         varchar2(20) NOT NULL,
  GRID_ID             varchar2(50) NOT NULL,
  SQL_FILE           varchar2(200) NULL,
  SQL_ID              varchar2(200) NULL,
  UPDATE_TABLE  varchar2(200) NULL,
  ROW_INPUT_DATE     DATE,
  ROW_INPUT_EMP_NO   VARCHAR2(20 BYTE),
  ROW_INPUT_IP       VARCHAR2(20 BYTE),
  ROW_UPDATE_DAT     DATE,
  ROW_UPDATE_EMP_NO  VARCHAR2(20 BYTE),
  ROW_UPDATE_IP      VARCHAR2(20 BYTE)
) ;
    
ALTER TABLE SM_DEV_GRID ADD CONSTRAINT PK_SM_DEV_GRID
 PRIMARY KEY (SYS_ID, PGM_CODE, GRID_ID);





CREATE TABLE SBTEC.SM_DEV_GRID_COLS
(
  SYS_ID             VARCHAR2(4 BYTE)           NOT NULL,
  PGM_CODE           VARCHAR2(20 BYTE)          NOT NULL,
  GRID_ID            VARCHAR2(50 BYTE)          NOT NULL,
  SEQ                INTEGER                    DEFAULT 0,
  FIELD_SEQ          INTEGER                    DEFAULT 0,
  FIELDNAME          VARCHAR2(50 BYTE)          NOT NULL,
  TABLE_NAME         VARCHAR2(50 BYTE),
  WIDTH              VARCHAR2(10 BYTE),   
  MUST_INPUT         VARCHAR2(10 BYTE),
  VISIBLE            VARCHAR2(10 BYTE), 
  READONLY           VARCHAR2(10 BYTE),
  EDITABLE           VARCHAR2(10 BYTE),
  SORTABLE           VARCHAR2(10 BYTE),
  HEADER             VARCHAR2(1000 BYTE),
  HEADER_TEXT        VARCHAR2(100 BYTE),
  STYLES             VARCHAR2(50 BYTE),
  EDITOR             VARCHAR2(50 BYTE),
  LOOKUPDISPLAY      VARCHAR2(10 BYTE),
  RENDERER           VARCHAR2(600 BYTE),
  BUTTON             VARCHAR2(50 BYTE),
  ALWAYSSHOWBUTTON   VARCHAR2(10 BYTE),
  HEADERTOOLTIP      VARCHAR2(2000 BYTE),
  MERGE              VARCHAR2(10 BYTE),
  MAXLENGTH          VARCHAR2(10 BYTE),
  FOOTEREXPR         VARCHAR2(1000 BYTE),
  FOOTERALIGN        VARCHAR2(10 BYTE),
  FOOTERSTYLE        VARCHAR2(2000 BYTE),
  FOOTERPRE          VARCHAR2(1000 BYTE),
  FOOTERPOST         VARCHAR2(1000 BYTE),
  RESIZABLE          VARCHAR2(10 BYTE),
  ROW_INPUT_DATE     DATE,
  ROW_INPUT_EMP_NO   VARCHAR2(20 BYTE),
  ROW_INPUT_IP       VARCHAR2(20 BYTE),
  ROW_UPDATE_DAT     DATE,
  ROW_UPDATE_EMP_NO  VARCHAR2(20 BYTE),
  ROW_UPDATE_IP      VARCHAR2(20 BYTE)
);


    
    
ALTER TABLE SM_DEV_GRID_COLS ADD CONSTRAINT PK_SM_DEV_GRID_COLS
  PRIMARY KEY (SYS_ID, PGM_CODE, GRID_ID, SEQ);



COMMENT ON COLUMN SBTEC.SM_DEV_GRID_COLS.VISIBLE IS 'BOOL';
COMMENT ON COLUMN SBTEC.SM_DEV_GRID_COLS.READONLY IS 'BOOL';
COMMENT ON COLUMN SBTEC.SM_DEV_GRID_COLS.EDITABLE IS 'BOOL';
COMMENT ON COLUMN SBTEC.SM_DEV_GRID_COLS.SORTABLE IS 'BOOL';
COMMENT ON COLUMN SBTEC.SM_DEV_GRID_COLS.LOOKUPDISPLAY IS 'BOOL';
COMMENT ON COLUMN SBTEC.SM_DEV_GRID_COLS.ALWAYSSHOWBUTTON IS 'BOOL';
COMMENT ON COLUMN SBTEC.SM_DEV_GRID_COLS.MERGE IS 'BOOL';
COMMENT ON COLUMN SBTEC.SM_DEV_GRID_COLS.RESIZABLE IS 'BOOL';


-->




</div> <!-- #pageLayout 밖에 Modal 놓기 위한 Trick -->




<!-- Modal -->
<div class="modal fade" id="sqlModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="sqlModalLabel">SQL & PARAMS</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-9">
            <div class="col-xs-12 sql-label">
              <label class="control-label">SQL & Params</label>
              <span class="">( SQL 을 수정하면 하단에 "수정 SQL 적용" 버튼이 활성화 됩니다.)</span>
            </div>
            <div class="col-xs-12">
              <div class="tab-content wrap-textarea">
                <div class="tab-pane active" id="ta-sql">
                  <textarea id="grid-sql" class="form-control" placeholder="SQL 쿼리를 입력하세요."></textarea>
                </div>
                <div class="tab-pane" id="ta-grid">
                  <textarea id="grid-txt" readonly class="form-control" placeholder="Grid 구문이 표시됩니다."></textarea>
                </div>
                <div class="tab-pane" id="ta-js">
                  <textarea id="js-txt" readonly class="form-control" placeholder="js 구문이 표시됩니다."></textarea>
                </div>
                <div class="tab-pane" id="ta-xmls">
                  <textarea id="sql-xmlx" readonly class="form-control" placeholder="Sql 구문이 표시됩니다."></textarea>
                </div>
              </div>
            </div>
          </div><!-- .col-xs-9 -->
          <div class="col-xs-3 wrap-updtables">
            <div class="row mt5">
              <div class="col-xs-12">
                <label class="control-label">Params</label>
              </div>
              <div class="col-xs-12 wrap-params">
                <table id="sql-params" class="table">
                </table>
              </div>
            </div>

          </div><!-- .col-xs-3 -->
        </div><!-- .row -->
      </div><!-- .modal-body -->
      <div class="modal-footer">
        <div class="text-center">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="btn-modified-sql" class="btn btn-primary">수정 SQL 적용</button>
        </div>
      </div>
    </div>
  </div>


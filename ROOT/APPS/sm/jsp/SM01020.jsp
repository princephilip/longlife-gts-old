<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<div id='topsearch' class='option_box2'>
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' pgmcode="SM01020"></html:authbutton>
		</span>
	</div>

	<div id="userxbtns" style="padding-right: 20px;">
    <button type="button" class="btn btn-success btn-xs" id="btn_find_cust" onclick="pf_find_cust()">거래처 검색</button>
  </div>

</div>

<div id='freeform' class='detail_box ver2 has_title'>
	<label class='sub_title'>거래처 기본정보</label>

	<div class='detail_row'>
		<label class='detail_label w120'>거래처구분</label>
		<div class='detail_input_bg w200'>
			<select id='CUST_TAG'></select>
		</div>

		<div class='row-closing'></div>

	</div>

	<div class='detail_row'>
		<label class='detail_label w120'>*사업자등록번호</label>
		<div class='detail_input_bg w200'>
			<input id='CUST_CODE' type='text' class='detail_cust tac'>
		</div>

		<label class='detail_label w120'>법인등록번호</label>
		<div class='detail_input_bg w200'>
			<input id='REGISTER_NO' type='text' >
		</div>

		<label class='detail_label w120'>거래유형</label>
		<div class='detail_input_bg w200'>
			<select id='TRADE_TYPE' ></select>
		</div>

		<div class='row-closing'></div>

	</div>

	<div class='detail_row'>
		<label class='detail_label w120'>*상호명</label>
		<div class='detail_input_bg w200'>
			<input id='CUST_NAME' type='text'>
		</div>

		<label class='detail_label w120'>업태</label>
		<div class='detail_input_bg w200'>
			<input id='CONDITION' 	type='text'>
		</div>

		<label class='detail_label w120'>과세정보</label>
		<div class='detail_input_bg w200'>
			<select id='BUSINESS_TYPE'></select>
		</div>

		<div class='row-closing'></div>
	</div>

	<div class='detail_row'>
		<label class='detail_label w120'>대표자</label>
		<div class='detail_input_bg w200'>
			<input id='OWNER' type='text' >
		</div>

		<label class='detail_label w120'>업종</label>
		<div class='detail_input_bg w200'>
			<input id='CATEGORY' 	type='text'>
		</div>

		<label class='detail_label w120'>지급그룹</label>
		<div class='detail_input_bg w200'>
			<select id='PAY_GROUP' ></select>
		</div>

		<div class='row-closing'></div>
	</div>

	<div class='detail_row detail_row_bb'>
		<label class='detail_label w120'>비 고</label>
		<div class='detail_input_bg w820'>
			<input id='REMARK' type='text' ><!-- 632px -->
		</div>

		<div class='row-closing'></div>
	</div>
</div>


<div id='freeform2' class='detail_box ver2 has_title'>
	<label class='sub_title'>세부정보</label>

	<div class='detail_row'>
		<label class='detail_label w100'>담당자</label>
		<div class='detail_input_bg w200'>
			<input id='CHARGE_NAME' type='text' >
		</div>

		<label class='detail_label w100'>전화번호</label>
		<div class='detail_input_bg'>
			<input id='TEL' 		type='text' >
		</div>

	</div>

	<div class='detail_row'>
		<label class='detail_label w100'>휴대폰</label>
		<div class='detail_input_bg w200'>
			<input id='TEL2' type='text'  >
		</div>

		<label class='detail_label w100'>팩스</label>
		<div class='detail_input_bg'>
			<input id='FAX' type='text' >
		</div>

	</div>

	<div class='detail_row'>
		<label class='detail_label w100' style='border-bottom: 0px;'>주소</label>
		<div class='detail_input_bg has_button w130'>
			<input id='ZIP' type='text' >
			<button id='btn_addrfind' class="search_find_img" />
		</div>

		<div class='detail_input_bg noborder'>
			<input id='ADDR' type='text' >
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label w100'></label>
		<div class='detail_input_bg has_button w130'>
		</div>
		<div class='detail_input_bg noborder'>
			<input id='ADDR2' type='text' >
		</div>

	</div>

	<div class='detail_row detail_row_bb'>
		<label class='detail_label w100'>등록자</label>
		<div class='detail_input_bg'>
			<input id='EMP_NO' type='text' readonly disabled />
		</div>

		<label class='detail_label w80'>등록일</label>
		<div class='detail_input_bg'>
			<input id='INPUT_DATE' type='text' readonly disabled/>
		</div>

		<label class='detail_label w80'>승인일</label>
		<div class='detail_input_bg'>
			<input id='BUSINESS_CHK_DATE' type='text' readonly disabled />
		</div>

		<label class='detail_label w80'>폐업일</label>
		<div class='detail_input_bg'>
			<input id='CLOSING_DATE' type='text' class='detail_date'>
		</div>


	</div>

</div>


<div id='freeform3' class='detail_box ver2 has_title ml10'>
	<label class='sub_title'>세금계산서 정보</label>

	<div class='detail_row'>
		<label class='detail_label w100'>담당부서</label>
		<div class='detail_input_bg w180'>
			<input id='TAXEMP_DEPT_NAME' 		type='text' >
		</div>

		<label class='detail_label w100'>담당자</label>
		<div class='detail_input_bg w180'>
			<input id='TAXEMP_NAME' type='text'  >
		</div>

		<div class='row-closing'></div>
	</div>

	<div class='detail_row detail_row_bb'>
		<label class='detail_label w100'>이메일</label>
		<div class='detail_input_bg w180'>
			<input id='TAXEMP_EMAIL' type='text' >
		</div>

		<label class='detail_label w100'>전화</label>
		<div class='detail_input_bg w180'>
			<input id='TAXEMP_TEL' type='text' >
		</div>

		<div class='row-closing'></div>
	</div>

</div>



<div id='freeform4' class='detail_box ver2 has_title ml10'>
	<label class='sub_title'>계좌정보</label>

	<div class='detail_row'>
		<label class='detail_label w100'>은행</label>
		<div class='detail_input_bg w180 has_button'>
			<input id='BANK_CODE' 		type='text'  >
			<button id='btn_bankcode' class="search_find_img" />
		</div>
		<div class='detail_input_bg w270 noborder'>
			<input id='BANK_NAME' type='text'  >
		</div>

		<div class='row-closing'></div>
	</div>

	<div class='detail_row'>
		<label class='detail_label w100'>예금주</label>
		<div class='detail_input_bg w180'>
			<input id='DEPOSIT_OWNER' type='text' >
		</div>

		<label class='detail_label w100'>계좌번호</label>
		<div class='detail_input_bg w180'>
			<input id='DEPOSIT_NO' type='text' >
		</div>

		<div class='row-closing'></div>
	</div>

	<div class='detail_row detail_row_bb'>
		<label class='detail_label w100'>계좌유효일자</label>
		<div class='detail_input_bg w85'>
			<input id='DEPOSIT_FROMDATE' type='text' class='detail_date'>
		</div>

		<div class='detail_input_bg w20 text_center noborder'>~</div>

		<div class='detail_input_bg w85 noborder'>
		  <input id='DEPOSIT_TODATE' type='text' class='detail_date'>
		</div>

		<div class='row-closing'></div>
	</div>


</div>

<div id='grid_1' class='grid_div mr5'>
	<div id="dg_1" style="width:100%; height:100%; " class="slick-grid"></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:33,  width_increase:1, height_increase:0};
	freeform.ResizeInfo  = {init_width:1000, init_height:175, width_increase:1, height_increase:0};
	freeform2.ResizeInfo = {init_width:620,  init_height:150, width_increase:0, height_increase:0};
	freeform3.ResizeInfo = {init_width:380,  init_height:90,  width_increase:1, height_increase:0};
	freeform4.ResizeInfo = {init_width:380,  init_height:120, width_increase:1, height_increase:0};

	grid_1.ResizeInfo 	 = {init_width:1000, init_height:80, width_increase:1, height_increase:0}; // 그리드와 탭엮기위함
</script>
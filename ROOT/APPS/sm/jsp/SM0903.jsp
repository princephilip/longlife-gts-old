<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
/**
 *<pre>
 * Statements
 *</pre>

 * @Class Name : SM0903.jsp
 * @Descriptio	  메뉴목록관리
 * @Modification Information
 * @
 * @  수정일        		 수정자                   수정내용
 * @ -------    --------    ---------------------------
 * @ 2013.03.03    박영찬        최초 생성
 * @
 */
%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box'>

	<div id='xbtns'>
		<html:authbutton id='buttons' grid='dg_1' pgmcode="SM0903"></html:authbutton>
	</div>
	<div id='userxbtns'>
		<button type="button" id='sourcecopy'  class="btn btn-warning btn-xs" onClick="pf_PgmFind();">소스복사</button>
		<button type="button" id='sourcecopy'  class="btn btn-danger btn-xs" onClick="pf_SourceCopy('SM0000','표준 Template');">표준복사</button>
		<button type="button" id='gridconv'  class="btn btn-warning btn-xs" onClick="pf_GridConv();">Real2Slick</button>
		<button type="button" id='gridconv'  class="btn btn-success btn-xs" onClick="pf_Real2DB();">Real2DB</button>
		<button type="button" id='gridconv'  class="btn btn-danger btn-xs" onClick="pf_Slick2DB();">Slick2DB</button>
	</div>

	<div class='option_label'>시스템</div>
	<div class='option_input_bg'>
		<html:select name="S_SYS_ID" id = "S_SYS_ID" comCodeYn="0" codeGroup="sm|SM_AUTH_SYS|W_SM0915_01" params="${companyCode}" dispStyle="1" selected="20" showTitle="true" titleCode="전체" titleValue='%'></html:select>
	</div>
	<div class='option_label'>프로그램</div>
	<div class='option_input_bg'>
		<input id='S_PGM_CODE' type='text' class='option_input_fix w140'>
	</div>

</div>

<div id='grid_1' class='grid_div'>
	<div id="dg_1" style="width:100%; height:100%;" class="slick-grid"></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};
	grid_1.ResizeInfo = {init_width:1000, init_height:665, width_increase:1, height_increase:1};
</script>
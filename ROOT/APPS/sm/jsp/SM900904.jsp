<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="<c:url value='/Mighty/template/js/XG_GRID1_template.js' />"></script>

<style>
	#func_demo_box {
		float: left;
		border: 0px;
		width: calc(100% - 669px);
		height: calc(100% - 40px);
		margin-left: 5px;
	}
</style>

<div id='topsearch' class='option_box2'>
	<div id='xbtns' style='height:23px;'>
		<html:authbutton id='buttons' grid='dg_1' gridCall="true" pgmcode="SM900904"></html:authbutton>
	</div>

	<div class='option_label'>함수명/설명</div>
	<div class='option_input_bg'>
		<input type='text' id='S_OPTION' class='option_input_fix w400'/>
	</div>
</div>


<div id='grid_1' class='grid_div'>
	<div id="dg_1" style='width:100%; height:100%;' class='slick-grid'></div>
</div>


<iframe id='func_demo_box' src=''></iframe>


<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height: 33, anchor:{x1:1,y1:1,x2:1,y2:0}};
	grid_1.ResizeInfo    = {init_width: 664, init_height:670, anchor:{x1:1,y1:1,x2:0,y2:1}};
</script>
<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2'>
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='grid_user' pgmcode="SM0918"></html:authbutton>
		</span>
	</div>

	<div class='option_label'>사용자</div>
	<div class='option_input_bg w200'><input id='S_FIND' type='text' class='option_input_fix w180'></div>
	<div class='option_label'>프로그램</div>
	<div class='option_input_bg w200'><input id='S_FIND2' type='text' class='option_input_fix w180'></div>
	<div class='option_label_left'>시스템</div>
	<div id='di_11' class='option_input_bg'>
		<html:select name="S_SYS_ID" id = "S_SYS_ID" comCodeYn="0" codeGroup="sm|SM_AUTH_SYS|W_SM0915_01" params="${companyCode}" dispStyle="1" showTitle="true" titleCode="전체"></html:select>
	</div>
</div>

<div id='grid_user' class='grid_div'>
	<%= xgov.core.util.XHtmlUtil.RealGrid("dg_user", request.getHeader("User-Agent"),"0","N","N") %>
</div>

<div id='grid_menu' class='grid_div ml5'>
	<%= xgov.core.util.XHtmlUtil.RealGrid("dg_menu", request.getHeader("User-Agent"),"1","N","N") %>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};
	grid_user.ResizeInfo = {init_width:320, init_height:665, width_increase:0.5, height_increase:1};
	grid_menu.ResizeInfo = {init_width:680, init_height:665, width_increase:0.5, height_increase:1};
</script>
<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
/**
 *<pre>
 * Statements
 *</pre>
 
 * @Class Name : SM0991.jsp
 * @Descriptio	  그룹미등록 프로그램 찾기
 * @Modification Information
 * @
 * @  수정일        		 수정자                   수정내용
 * @ -------    --------    ---------------------------
 * @ 2013.03.03    박영찬        최초 생성
 * @
 */
%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box'>
	<div class='option_label_left w100'>시스템</div>
	<div id='di_11' class='option_input_bg'>
		<html:select name="S_SYS_ID" id = "S_SYS_ID" comCodeYn="0" codeGroup="sm|SM_AUTH_SYS|W_SM0915_01" params="${companyCode}" dispStyle="1" selected="20" showTitle="true" titleCode="선택"></html:select>
	</div>
	<div class='option_label w100'>프로그램</div>
	<div id='di_12' class='option_input_bg w160'>
		<input id='S_PGM_CODE' type='text' class='option_input' style="margin-top:3px;">
	</div>
	<span style="float:right;vertical-align:middle;margin-top:3px;margin-right:3px;">
		<img id='btn_search' src='<c:url value="/Theme/images/btn/search.gif"/> '/>
		<img id='btn_confirm' src='<c:url value="/Theme/images/btn/confirm.gif"/> '/>
	</span>
</div>

<div id='grid_1'>	
	<div id="dg_1" style="width:100%; height:100%; " class="slick-grid"></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};
	grid_1.ResizeInfo = {init_width:1000, init_height:662, width_increase:1, height_increase:1};
</script>
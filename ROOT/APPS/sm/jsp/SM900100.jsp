<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<div id='topsearch' class='option_box2'>
	<div class='option_label_left'>시스템</div>
	<div id='oi_11' class='option_input_bg'>
		<html:select name="S_SYS_ID" id = "S_SYS_ID" comCodeYn="0" codeGroup="sm|CodeCom|SM_AUTH_SYS_R01" params = "" dispStyle="1" titleCode="선택"></html:select>
	</div>
	<div class='option_label'>칼럼명</div>
	<div id='oi_12' class='option_input_bg w200'>
		<input id='S_FIELD_NAME' type='text' class='option_input'>
	</div>
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' pgmcode="SM900100"></html:authbutton>
		</span>
	</div>
</div>

<div id='grid_1'>
	<div id="dg_1" class="slick-grid"></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo = {init_width:1000, init_height:33, width_increase:1, height_increase:0};
	grid_1.ResizeInfo = {init_width:1000, init_height:666, width_increase:1, height_increase:1};
	//oi_11.ResizeInfo = {init_width:160,  init_height:0, anchor:{x1:0,y1:0,x2:0,y2:0}};
	//oi_12.ResizeInfo = {init_width:640,  init_height:0, anchor:{x1:1,y1:0,x2:1,y2:0}};
	//S_SYS_ID.ResizeInfo = {init_width:150,  init_height:0, anchor:{x1:0,y1:0,x2:0,y2:0}};
	//S_FIELD_NAME.ResizeInfo = {init_width:600,  init_height:0, anchor:{x1:1,y1:0,x2:1,y2:0}};

</script>
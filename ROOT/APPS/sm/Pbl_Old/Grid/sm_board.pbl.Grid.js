var sm_q_board_list = [
	{fieldName : 'BOARD_SEQ'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "BOARD_SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BOARD_TITLE'   , width: 249, visible: true, readonly: false, editable: false, sortable: true, header: {text : "BOARD_TITLE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REGI_DATE'     , width: 74, visible: true, readonly: false, editable: false, sortable: true, header: {text : "등록일자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPANY_CODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_ID'       , width: 93, visible: true, readonly: false, editable: false, sortable: true, header: {text : "USER_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_NAME'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "USER_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BOARD_DT_FT'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "BOARD_DT_FT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'OPEN_TAG'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "OPEN_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TOP_TAG'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "TOP_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ALWAYS_BOARD'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ALWAYS_BOARD"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BOARD_CONTENTS'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "BOARD_CONTENTS"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_board_detail = [
	{fieldName : 'BOARD_SEQ'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "BOARD_SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INPUT_DUTY_ID' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "INPUT_DUTY_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INPUT_DATE'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "INPUT_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHG_DUTY_ID'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHG_DUTY_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHG_DATE'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHG_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TITLE'         , width: 480, visible: true, readonly: false, editable: false, sortable: true, header: {text : "TITLE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REGI_DATE'     , width: 72, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Regi Date"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPANY_CODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "Company Code"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_ID'       , width: 228, visible: true, readonly: false, editable: false, sortable: true, header: {text : "User Id"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BOARD_DT_F'    , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Board Dt F"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BOARD_DT_T'    , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Board Dt T"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'OPEN_TAG'      , width: 66, visible: false, readonly: false, editable: false, sortable: true, header: {text : "Open Tag"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TOP_TAG'       , width: 81, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Top Tag"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ALWAYS_BOARD'  , width: 69, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Always Board"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BOARD_CONTENTS'  , width: 480, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Board Contents"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_q_board_list = [
	{fieldName : 'BOARD_SEQ'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "BOARD_SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BOARD_TITLE'   , width: 249, visible: true, readonly: false, editable: false, sortable: true, header: {text : "BOARD_TITLE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REGI_DATE'     , width: 74, visible: true, readonly: false, editable: false, sortable: true, header: {text : "등록일자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPANY_CODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_ID'       , width: 93, visible: true, readonly: false, editable: false, sortable: true, header: {text : "USER_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_NAME'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "USER_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BOARD_DT_FT'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "BOARD_DT_FT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'OPEN_TAG'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "OPEN_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TOP_TAG'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "TOP_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ALWAYS_BOARD'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ALWAYS_BOARD"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BOARD_CONTENTS'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "BOARD_CONTENTS"}, styles: _Styles_textc, 		editor: _Editor_text } 
];


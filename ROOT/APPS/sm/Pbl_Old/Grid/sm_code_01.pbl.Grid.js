var sm_f_zipcode_doro_daejeon = [
	{fieldName : 'ADDR'    , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR2'   , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_CODE'   , width: 407, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ZIP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_zipcode_doro_ulsan = [
	{fieldName : 'ADDR'    , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR2'   , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_CODE'   , width: 407, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ZIP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_zipcode_doro_seoul = [
	{fieldName : 'ADDR'    , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR2'   , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_CODE'   , width: 407, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ZIP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_zipcode_doro_sejong = [
	{fieldName : 'ADDR'    , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR2'   , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_CODE'   , width: 407, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ZIP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_zipcode_doro_jeonbuk = [
	{fieldName : 'ADDR'    , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR2'   , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_CODE'   , width: 407, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ZIP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_zipcode_doro_jeju = [
	{fieldName : 'ADDR'    , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR2'   , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_CODE'   , width: 407, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ZIP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_zipcode_doro_gyeongnam = [
	{fieldName : 'ADDR'    , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR2'   , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_CODE'   , width: 407, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ZIP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_zipcode_doro_gangwon = [
	{fieldName : 'ADDR'    , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR2'   , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_CODE'   , width: 407, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ZIP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_zipcode_doro_gwangju = [
	{fieldName : 'ADDR'    , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR2'   , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_CODE'   , width: 407, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ZIP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_zipcode_doro_incheon = [
	{fieldName : 'ADDR'    , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR2'   , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_CODE'   , width: 407, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ZIP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_zipcode_doro_gyeongbuk = [
	{fieldName : 'ADDR'    , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR2'   , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_CODE'   , width: 407, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ZIP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_zipcode_doro_jeonnam = [
	{fieldName : 'ADDR'    , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR2'   , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_CODE'   , width: 407, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ZIP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_zipcode_doro_gyeonggi = [
	{fieldName : 'ADDR'    , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR2'   , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_CODE'   , width: 407, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ZIP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_zipcode_doro_daegu = [
	{fieldName : 'ADDR'    , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR2'   , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_CODE'   , width: 407, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ZIP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_zipcode_doro_busan = [
	{fieldName : 'ADDR'    , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR2'   , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_CODE'   , width: 407, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ZIP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_zipcode_doro_chungnam = [
	{fieldName : 'ADDR'    , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR2'   , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_CODE'   , width: 407, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ZIP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_zipcode_doro_chungbuk = [
	{fieldName : 'ADDR'    , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR2'   , width: 1271, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ADDR2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_CODE'   , width: 407, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ZIP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_zipcode_doro = [
	{fieldName : 'ADDR'    , width: 244, visible: true, readonly: false, editable: false, sortable: true, header: {text : "주소"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR2'   , width: 186, visible: true, readonly: false, editable: false, sortable: true, header: {text : "상세주소"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_CODE'   , width: 90, visible: true, readonly: false, editable: false, sortable: true, header: {text : "우편번호"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_d_acnt_connect = [
	{fieldName : 'ACNT_CODE'     , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "계정코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ACNT_NAME'     , width: 163, visible: true, readonly: false, editable: false, sortable: true, header: {text : "계정과목명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ACNT_LEVEL'    , width: 41, visible: true, readonly: false, editable: false, sortable: true, header: {text : "레벨"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ACNT_GROUP'    , width: 47, visible: true, readonly: false, editable: false, sortable: true, header: {text : "그룹"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'MM_USE_TAG'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "MM_USE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CM_USE_TAG'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CM_USE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COST_ACNT_CODE'    , width: 121, visible: true, readonly: false, editable: false, sortable: true, header: {text : "원가계정 연결"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_CODE'    , width: 125, visible: true, readonly: false, editable: false, sortable: true, header: {text : "표준공종 연결"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SALE_ACNT_CODE'    , width: 121, visible: true, readonly: false, editable: false, sortable: true, header: {text : "영업원가계정 연결"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_d_acnt_connect_gugong = [
	{fieldName : 'ACNT_CODE'     , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "계정코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ACNT_NAME'     , width: 163, visible: true, readonly: false, editable: false, sortable: true, header: {text : "계정과목명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ACNT_LEVEL'    , width: 41, visible: true, readonly: false, editable: false, sortable: true, header: {text : "레벨"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ACNT_GROUP'    , width: 47, visible: true, readonly: false, editable: false, sortable: true, header: {text : "그룹"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'MM_USE_TAG'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "MM_USE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CM_USE_TAG'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CM_USE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COST_ACNT_CODE'    , width: 121, visible: true, readonly: false, editable: false, sortable: true, header: {text : "원가계정 연결"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_CODE'    , width: 125, visible: true, readonly: false, editable: false, sortable: true, header: {text : "표준공종 연결"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SALE_ACNT_CODE'    , width: 121, visible: true, readonly: false, editable: false, sortable: true, header: {text : "영업원가계정 연결"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_user_project_total = [
	{fieldName : 'PROJ_CODE', width: 70, visible: true, readonly: false, editable: false, sortable: true, header: {text : "현장코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROJ_NAME', width: 306, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PROJ_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_user_project_all = [
	{fieldName : 'PROJ_CODE', width: 63, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PROJ_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROJ_NAME', width: 306, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PROJ_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var ws_kechange = [
	{fieldName : 'SEQ'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'KEDIV' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "KEDIV"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'KNAME' , width: 253, visible: true, readonly: false, editable: false, sortable: true, header: {text : "한글"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ENAME' , width: 264, visible: true, readonly: false, editable: false, sortable: true, header: {text : "영문"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARK' , width: 200, visible: true, readonly: false, editable: false, sortable: true, header: {text : "비고"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'KEYN'  , width: 69, visible: true, readonly: false, editable: false, sortable: true, header: {text : "요청여부"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NNAME' , width: 239, visible: true, readonly: false, editable: false, sortable: true, header: {text : "영문2"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_code_dept = [
	{fieldName : 'COMPANY_CODE'          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_CODE'             , width: 59, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'             , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DEPT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL_DEPT_NAME'       , width: 168, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_ENG'              , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DEPT_ENG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TAX_COMPANY_CODE'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "TAX_COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COST_TAG'              , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COST_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COST_CODE'             , width: 126, visible: true, readonly: false, editable: false, sortable: true, header: {text : "원가코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVELS'                , width: 50, visible: true, readonly: false, editable: false, sortable: true, header: {text : "레벨"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL1'                , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "LEVEL1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL2'                , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "LEVEL2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL3'                , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "LEVEL3"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SORT_ORDER'            , width: 72, visible: true, readonly: false, editable: false, sortable: true, header: {text : "정렬순서"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USING_TAG'             , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "USING_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'OFFICE_CODE'           , width: 178, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사업장코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BUDGET_CONTROLTAG'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "BUDGET_CONTROLTAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TAX_OFFICE_CODE'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "TAX_OFFICE_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COST_DEPT_CODE'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COST_DEPT_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COST_DEPT_NAME'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COST_DEPT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'OFFICE_NAME'           , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "OFFICE_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TAX_COMPANY_NAME'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "TAX_COMPANY_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_DIV_CODE'         , width: 66, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서구분"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROJ_DIV_CODE'         , width: 61, visible: true, readonly: false, editable: false, sortable: true, header: {text : "해외구분"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_CODE_DEPT_DEPT_NAME'         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_CODE_DEPT_DEPT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_CODE_DEPT_DEPT_NAME'         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_CODE_DEPT_DEPT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_CODE_DEPT_DEPT_NAME'         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_CODE_DEPT_DEPT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FC_DEPT'               , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "FC_DEPT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'MANUFAC_DIV'           , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "MANUFAC_DIV"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPOSIT_NO'            , width: 115, visible: true, readonly: false, editable: false, sortable: true, header: {text : "계좌번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPOSIT_NAME'          , width: 121, visible: true, readonly: false, editable: false, sortable: true, header: {text : "계좌명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GROUP_CODE'            , width: 89, visible: true, readonly: false, editable: false, sortable: true, header: {text : "GROUP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GROUP_CODE1'           , width: 89, visible: true, readonly: false, editable: false, sortable: true, header: {text : "GROUP_CODE1"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_code_dept_detail = [
	{fieldName : 'COMPANY_CODE'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_CODE'        , width: 72, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'        , width: 236, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL_DEPT_NAME'  , width: 478, visible: false, readonly: false, editable: false, sortable: true, header: {text : "LEVEL_DEPT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_ENG'         , width: 236, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_ENG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TAX_COMPANY_CODE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "TAX_COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COST_TAG'         , width: 134, visible: true, readonly: false, editable: false, sortable: true, header: {text : "COST_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COST_CODE'        , width: 134, visible: true, readonly: false, editable: false, sortable: true, header: {text : "COST_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVELS'           , width: 134, visible: true, readonly: false, editable: false, sortable: true, header: {text : "LEVELS"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL1'           , width: 43, visible: true, readonly: false, editable: false, sortable: true, header: {text : "LEVEL1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL2'           , width: 43, visible: true, readonly: false, editable: false, sortable: true, header: {text : "LEVEL2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL3'           , width: 43, visible: true, readonly: false, editable: false, sortable: true, header: {text : "LEVEL3"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SORT_ORDER'       , width: 134, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SORT_ORDER"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USING_TAG'        , width: 15, visible: true, readonly: false, editable: false, sortable: true, header: {text : "USING_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'OFFICE_CODE'      , width: 134, visible: true, readonly: false, editable: false, sortable: true, header: {text : "OFFICE_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BUDGET_CONTROLTAG'      , width: 17, visible: true, readonly: false, editable: false, sortable: true, header: {text : "BUDGET_CONTROLTAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TAX_OFFICE_CODE'  , width: 129, visible: true, readonly: false, editable: false, sortable: true, header: {text : "TAX_OFFICE_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COST_DEPT_CODE'   , width: 59, visible: true, readonly: false, editable: false, sortable: true, header: {text : "COST_DEPT_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COST_DEPT_NAME'   , width: 283, visible: true, readonly: false, editable: false, sortable: true, header: {text : "COST_DEPT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'OFFICE_NAME'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "OFFICE_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TAX_COMPANY_NAME' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "TAX_COMPANY_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_DIV_CODE'    , width: 71, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_DIV_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROJ_DIV_CODE'    , width: 134, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PROJ_DIV_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME1'       , width: 86, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_NAME1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME2'       , width: 86, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_NAME2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME3'       , width: 86, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_NAME3"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_FC_DEPT'     , width: 134, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_FC_DEPT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'MANUFAC_DIV'      , width: 129, visible: true, readonly: false, editable: false, sortable: true, header: {text : "MANUFAC_DIV"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPOSIT_NO'       , width: 134, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPOSIT_NO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPOSIT_NAME'     , width: 235, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPOSIT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GROUP_CODE'       , width: 134, visible: true, readonly: false, editable: false, sortable: true, header: {text : "GROUP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GROUP_CODE1'      , width: 134, visible: true, readonly: false, editable: false, sortable: true, header: {text : "GROUP_CODE1"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_code_dept_detail_bk = [
	{fieldName : 'COMPANY_CODE'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_CODE'        , width: 72, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'        , width: 236, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL_DEPT_NAME'  , width: 478, visible: false, readonly: false, editable: false, sortable: true, header: {text : "LEVEL_DEPT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_ENG'         , width: 236, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_ENG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TAX_COMPANY_CODE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "TAX_COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COST_TAG'         , width: 134, visible: true, readonly: false, editable: false, sortable: true, header: {text : "COST_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COST_CODE'        , width: 134, visible: true, readonly: false, editable: false, sortable: true, header: {text : "COST_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVELS'           , width: 134, visible: true, readonly: false, editable: false, sortable: true, header: {text : "LEVELS"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL1'           , width: 43, visible: true, readonly: false, editable: false, sortable: true, header: {text : "LEVEL1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL2'           , width: 43, visible: true, readonly: false, editable: false, sortable: true, header: {text : "LEVEL2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL3'           , width: 43, visible: true, readonly: false, editable: false, sortable: true, header: {text : "LEVEL3"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SORT_ORDER'       , width: 134, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SORT_ORDER"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USING_TAG'        , width: 15, visible: true, readonly: false, editable: false, sortable: true, header: {text : "USING_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'OFFICE_CODE'      , width: 134, visible: true, readonly: false, editable: false, sortable: true, header: {text : "OFFICE_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BUDGET_CONTROLTAG'      , width: 17, visible: true, readonly: false, editable: false, sortable: true, header: {text : "BUDGET_CONTROLTAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TAX_OFFICE_CODE'  , width: 129, visible: true, readonly: false, editable: false, sortable: true, header: {text : "TAX_OFFICE_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COST_DEPT_CODE'   , width: 59, visible: true, readonly: false, editable: false, sortable: true, header: {text : "COST_DEPT_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COST_DEPT_NAME'   , width: 283, visible: true, readonly: false, editable: false, sortable: true, header: {text : "COST_DEPT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'OFFICE_NAME'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "OFFICE_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TAX_COMPANY_NAME' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "TAX_COMPANY_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_DIV_CODE'    , width: 71, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_DIV_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROJ_DIV_CODE'    , width: 134, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PROJ_DIV_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME1'       , width: 86, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_NAME1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME2'       , width: 86, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_NAME2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME3'       , width: 86, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_NAME3"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_FC_DEPT'     , width: 134, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_FC_DEPT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'MANUFAC_DIV'      , width: 129, visible: true, readonly: false, editable: false, sortable: true, header: {text : "MANUFAC_DIV"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPOSIT_NO'       , width: 134, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPOSIT_NO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPOSIT_NAME'     , width: 235, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPOSIT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GROUP_CODE'       , width: 134, visible: true, readonly: false, editable: false, sortable: true, header: {text : "GROUP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_d_const_acnt_code = [
	{fieldName : 'COMPANY_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COSTCODE'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COSTCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_ACNT_SEQ' , width: 104, visible: false, readonly: false, editable: false, sortable: true, header: {text : "배열순서"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_ACNT'     , width: 110, visible: true, readonly: false, editable: false, sortable: true, header: {text : "계정구분"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_ACNT_CODE'     , width: 81, visible: true, readonly: false, editable: false, sortable: true, header: {text : "계정코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_ACNT_NAME'     , width: 188, visible: true, readonly: false, editable: false, sortable: true, header: {text : "계정명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SUM_TAG'        , width: 157, visible: false, readonly: false, editable: false, sortable: true, header: {text : "집계"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQ_YN'         , width: 85, visible: true, readonly: false, editable: false, sortable: true, header: {text : "전도금사용여부"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_l_mcps_status_ym = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'YYMM'        , width: 58, visible: true, readonly: false, editable: false, sortable: true, header: {text : "년월"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CLOSE_TAG'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CLOSE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var d_sm01210_1 = [
	{fieldName : 'HCODE'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "HCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DCODE'       , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DNAME'       , width: 139, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부코드명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DNAME1'      , width: 237, visible: true, readonly: false, editable: false, sortable: true, header: {text : "명칭1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DNAME2'      , width: 141, visible: true, readonly: false, editable: false, sortable: true, header: {text : "명칭2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DVALUE'      , width: 98, visible: true, readonly: false, editable: false, sortable: true, header: {text : "정렬 OR 숫자값"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HDCODE'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "HDCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RELATE_CODE1'      , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "관련코드1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RELATE_CODE2'      , width: 74, visible: true, readonly: false, editable: false, sortable: true, header: {text : "관련코드2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RELATE_CODE3'      , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "관련코드3"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RELATE_CODE4'      , width: 74, visible: true, readonly: false, editable: false, sortable: true, header: {text : "관련코드4"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RELATE_CODE5'      , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "관련코드5"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FIX_CODE1'   , width: 74, visible: true, readonly: false, editable: false, sortable: true, header: {text : "고정코드1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FIX_CODE2'   , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "고정코드2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FIX_CODE3'   , width: 74, visible: true, readonly: false, editable: false, sortable: true, header: {text : "고정코드3"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FIX_CODE4'   , width: 74, visible: true, readonly: false, editable: false, sortable: true, header: {text : "고정코드4"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FIX_CODE5'   , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "고정코드5"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYSTEM_CODE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYSTEM_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CODE_TYPE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CODE_TYPE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DVALUE2'     , width: 102, visible: true, readonly: false, editable: false, sortable: true, header: {text : "문자값"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USE_YN'      , width: 129, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사용여부"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CODE_GU'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CODE_GU"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RMK'         , width: 243, visible: true, readonly: false, editable: false, sortable: true, header: {text : "비고"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_ID'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dddw_office_code = [
	{fieldName : 'OFFICE_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "OFFICE_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'OFFICE_NAME', width: 203, visible: true, readonly: false, editable: false, sortable: true, header: {text : "OFFICE_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dddw_taxoffice = [
	{fieldName : 'TAX_OFFICE_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "TAX_OFFICE_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TAX_OFFICE_NAME', width: 521, visible: true, readonly: false, editable: false, sortable: true, header: {text : "TAX_OFFICE_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dddw_com_code_cc2 = [
	{fieldName : 'HCODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "HCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DCODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DNAME', width: 262, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DNAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dddw_vw_c_sm_stdconst = [
	{fieldName : 'STD_DIV'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "STD_DIV"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CUSTOMER_CODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CUSTOMER_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_DIV'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CONST_DIV"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_CODE'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CONST_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL_NUM'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "LEVEL_NUM"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'QUERY_SEQ'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "QUERY_SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HIGH_CONST_CODE'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "HIGH_CONST_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_SEQ'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CONST_SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HIGH_CONST_SEQ' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "HIGH_CONST_SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_NAME'     , width: 261, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Const Name"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DIRECT_DIV'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DIRECT_DIV"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dddw_hr_dept_div = [
	{fieldName : 'DEPT_DIV_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DEPT_DIV_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_DIV_NAME', width: 111, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서구분명칭"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_c_dpw_caps = [
	{fieldName : 'USER_PASS' , width: 1385, visible: true, readonly: false, editable: false, sortable: true, header: {text : "USER_PASS"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHNG_PASS1' , width: 1097, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHNG_PASS1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHNG_PASS2' , width: 1097, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHNG_PASS2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_KEY'  , width: 1385, visible: true, readonly: false, editable: false, sortable: true, header: {text : "AUTH_KEY"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_CHNG1'  , width: 1097, visible: false, readonly: false, editable: false, sortable: true, header: {text : "AUTH_CHNG1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_CHNG2'  , width: 1097, visible: false, readonly: false, editable: false, sortable: true, header: {text : "AUTH_CHNG2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DIV'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DIV"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dddw_const_acnt_code = [
	{fieldName : 'CONST_ACNT_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CONST_ACNT_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_ACNT_NAME', width: 264, visible: true, readonly: false, editable: false, sortable: true, header: {text : "계정명"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dddw_comcode = [
	{fieldName : 'HCODE', width: 65, visible: false, readonly: false, editable: false, sortable: true, header: {text : "HCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DCODE', width: 108, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DNAME', width: 119, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DNAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dddw_com_code_ccgb = [
	{fieldName : 'RELATE_CODE_NAME', width: 138, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Relate Code Name"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RELATE_CODE'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "RELATE_CODE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dddw_com_code_cc = [
	{fieldName : 'HCODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "HCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DCODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DNAME', width: 132, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DNAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dddw_com_code = [
	{fieldName : 'HCODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "HCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DCODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DNAME', width: 132, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DNAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var d_sm01210_list = [
	{fieldName : 'HCODE'     , width: 132, visible: true, readonly: false, editable: false, sortable: true, header: {text : "주코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HNAME'     , width: 183, visible: true, readonly: false, editable: false, sortable: true, header: {text : "주코드명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DCODE_SIZE'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DCODE_SIZE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HNAME1'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "HNAME1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DATA_TYPE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DATA_TYPE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CODE_SIZE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CODE_SIZE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NAME_SIZE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "NAME_SIZE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FLOAT_POS' , width: 51, visible: true, readonly: false, editable: false, sortable: true, header: {text : "정렬순서"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TABLE_NAME' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "TABLE_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CODE_TYPE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CODE_TYPE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RMK'       , width: 481, visible: true, readonly: false, editable: false, sortable: true, header: {text : "비        고"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_ID'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_NAME'  , width: 62, visible: true, readonly: false, editable: false, sortable: true, header: {text : "업무"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_mcps_user = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_ID'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEQ'         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NO'      , width: 58, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사번"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NAME'    , width: 145, visible: true, readonly: false, editable: false, sortable: true, header: {text : "성명"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_mcps_table = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_ID'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEQ'         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TABLE_NAME'  , width: 205, visible: true, readonly: false, editable: false, sortable: true, header: {text : "테이블명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COLUMN_NAME' , width: 158, visible: true, readonly: false, editable: false, sortable: true, header: {text : "칼럼명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'WHERE_TAG'   , width: 199, visible: true, readonly: false, editable: false, sortable: true, header: {text : "조건"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_stdconst = [
	{fieldName : 'STD_DIV'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "STD_DIV"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CUSTOMER_CODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CUSTOMER_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_DIV'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CONST_DIV"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_CODE'     , width: 72, visible: true, readonly: false, editable: false, sortable: true, header: {text : "공종코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL_NUM'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "LEVEL_NUM"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'QUERY_SEQ'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "QUERY_SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HIGH_CONST_CODE'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "HIGH_CONST_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_SEQ'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CONST_SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HIGH_CONST_SEQ' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "HIGH_CONST_SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_NAME'     , width: 344, visible: true, readonly: false, editable: false, sortable: true, header: {text : "공종명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DIRECT_DIV'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DIRECT_DIV"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_user_project = [
	{fieldName : 'PROJ_CODE', width: 63, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PROJ_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROJ_NAME', width: 306, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PROJ_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_ddlb_estcode = [
	{fieldName : 'EST_CODE', width: 69, visible: true, readonly: false, editable: false, sortable: true, header: {text : "현설코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EST_NAME', width: 366, visible: true, readonly: false, editable: false, sortable: true, header: {text : "현설명"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_mcps_status_item3 = [
	{fieldName : 'COMPANY_CODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'YYMM'          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "YYMM"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_ID'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEQ'           , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHECK_TAG'     , width: 29, visible: true, readonly: false, editable: false, sortable: true, header: {text : "마감"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CLOSE_TAG'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CLOSE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARK'        , width: 330, visible: true, readonly: false, editable: false, sortable: true, header: {text : "비고"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SORT_ORDER'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SORT_ORDER"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ITEM_NAME'     , width: 210, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ITEM_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REAL_CLOSE_TAG'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REAL_CLOSE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_mcps_status_item2 = [
	{fieldName : 'COMPANY_CODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'YYMM'          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "YYMM"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_ID'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEQ'           , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHECK_TAG'     , width: 29, visible: true, readonly: false, editable: false, sortable: true, header: {text : "마감"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CLOSE_TAG'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CLOSE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARK'        , width: 330, visible: true, readonly: false, editable: false, sortable: true, header: {text : "비고"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SORT_ORDER'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SORT_ORDER"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ITEM_NAME'     , width: 210, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ITEM_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REAL_CLOSE_TAG'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REAL_CLOSE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_mcps_status_item1 = [
	{fieldName : 'COMPANY_CODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'YYMM'          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "YYMM"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_ID'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEQ'           , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHECK_TAG'     , width: 29, visible: true, readonly: false, editable: false, sortable: true, header: {text : "마감"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CLOSE_TAG'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CLOSE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARK'        , width: 330, visible: true, readonly: false, editable: false, sortable: true, header: {text : "비고"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SORT_ORDER'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SORT_ORDER"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ITEM_NAME'     , width: 210, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ITEM_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REAL_CLOSE_TAG'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REAL_CLOSE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_mcps_status_level3 = [
	{fieldName : 'COMPANY_CODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'YYMM'          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "YYMM"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_ID'        , width: 40, visible: true, readonly: false, editable: false, sortable: true, header: {text : "코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEQ'           , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHECK_TAG'     , width: 29, visible: true, readonly: false, editable: false, sortable: true, header: {text : "마감"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CLOSE_TAG'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CLOSE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARK'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SORT_ORDER'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SORT_ORDER"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DNAME'         , width: 179, visible: true, readonly: false, editable: false, sortable: true, header: {text : "명칭"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REAL_CLOSE_TAG'         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REAL_CLOSE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_mcps_status_level2 = [
	{fieldName : 'COMPANY_CODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'YYMM'          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "YYMM"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_ID'        , width: 40, visible: true, readonly: false, editable: false, sortable: true, header: {text : "코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEQ'           , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHECK_TAG'     , width: 29, visible: true, readonly: false, editable: false, sortable: true, header: {text : "마감"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CLOSE_TAG'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CLOSE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARK'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SORT_ORDER'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SORT_ORDER"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DNAME'         , width: 179, visible: true, readonly: false, editable: false, sortable: true, header: {text : "명칭"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REAL_CLOSE_TAG'         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REAL_CLOSE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_mcps_status_level1 = [
	{fieldName : 'COMPANY_CODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'YYMM'          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "YYMM"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_ID'        , width: 40, visible: true, readonly: false, editable: false, sortable: true, header: {text : "코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEQ'           , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHECK_TAG'     , width: 30, visible: true, readonly: false, editable: false, sortable: true, header: {text : "마감"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CLOSE_TAG'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CLOSE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARK'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SORT_ORDER'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SORT_ORDER"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DNAME'         , width: 179, visible: true, readonly: false, editable: false, sortable: true, header: {text : "명칭"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REAL_CLOSE_TAG'         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REAL_CLOSE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_l_mcpauth = [
	{fieldName : 'DCODE'  , width: 51, visible: true, readonly: false, editable: false, sortable: true, header: {text : "코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DNAME'  , width: 208, visible: true, readonly: false, editable: false, sortable: true, header: {text : "명칭"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DVALUE2'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DVALUE2"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_mcps_item = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_ID'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEQ'         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ITEM_NAME'   , width: 324, visible: true, readonly: false, editable: false, sortable: true, header: {text : "명칭"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SORT_ORDER'  , width: 59, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SORT_ORDER"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RELATE_CODE' , width: 149, visible: true, readonly: false, editable: false, sortable: true, header: {text : "RELATE_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CLOSE_MM'    , width: 51, visible: true, readonly: false, editable: false, sortable: true, header: {text : "마감월"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CLOSE_DD'    , width: 46, visible: true, readonly: false, editable: false, sortable: true, header: {text : "마감일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CLOSE_EMP_NO'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CLOSE_EMP_NO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NAME'    , width: 98, visible: true, readonly: true, editable: false, sortable: true, header: {text : "사원명"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_d_code_constacnt = [
	{fieldName : 'CONST_ACNT_CODE', width: 94, visible: true, readonly: false, editable: false, sortable: true, header: {text : "코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_ACNT_SEQ' , width: 84, visible: false, readonly: false, editable: false, sortable: true, header: {text : "계정순번"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_ACNT_NAME' , width: 171, visible: true, readonly: false, editable: false, sortable: true, header: {text : "원가 계정명"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_code_group1 = [
	{fieldName : 'COMPANY_CODE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GROUP_CODE'   , width: 55, visible: true, readonly: false, editable: false, sortable: true, header: {text : "그룹코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GROUP_NAME'   , width: 174, visible: true, readonly: false, editable: false, sortable: true, header: {text : "그룹명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'UP_GROUP_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "UP_GROUP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GROUP_LEVEL'  , width: 74, visible: true, readonly: false, editable: false, sortable: true, header: {text : "그룹레벨"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ORDER_SEQ'    , width: 67, visible: true, readonly: false, editable: false, sortable: true, header: {text : "정렬순서"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARK'       , width: 317, visible: true, readonly: false, editable: false, sortable: true, header: {text : "비고"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USE_YN'       , width: 60, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사용여부"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_code_group = [
	{fieldName : 'GROUP_CODE'   , width: 55, visible: true, readonly: false, editable: false, sortable: true, header: {text : "그룹코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GROUP_NAME'   , width: 174, visible: true, readonly: false, editable: false, sortable: true, header: {text : "그룹명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'UP_GROUP_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "UP_GROUP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GROUP_LEVEL'  , width: 74, visible: true, readonly: false, editable: false, sortable: true, header: {text : "그룹레벨"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ORDER_SEQ'    , width: 67, visible: true, readonly: false, editable: false, sortable: true, header: {text : "정렬순서"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARK'       , width: 317, visible: true, readonly: false, editable: false, sortable: true, header: {text : "비고"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USE_YN'       , width: 60, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사용여부"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_code_dept2 = [
	{fieldName : 'DEPT_CODE'     , width: 59, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'     , width: 169, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPANY_CODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TAX_COMP_CODE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "TAX_COMP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_DIV'      , width: 63, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서구분"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COST_CODE'     , width: 89, visible: true, readonly: false, editable: false, sortable: true, header: {text : "원가코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USE_YN'        , width: 52, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사용유무"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TERMINATE_DATE'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "TERMINATE_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GROUP_CODE'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "GROUP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SORT_ORDER'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SORT_ORDER"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COST_DEPT_CODE'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COST_DEPT_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BUSINESS_CODE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "BUSINESS_CODE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_d_company = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPANY_NAME', width: 155, visible: true, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_d_code_ref = [
	{fieldName : 'COM_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COM_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COM_NAME', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COM_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_d_systemcode = [
	{fieldName : 'SYS_ID'  , width: 52, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_NAME'  , width: 287, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SYS_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_code_group = [
	{fieldName : 'COMPANY_CODE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GROUP_CODE'   , width: 55, visible: true, readonly: false, editable: false, sortable: true, header: {text : "그룹코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GROUP_NAME'   , width: 174, visible: true, readonly: false, editable: false, sortable: true, header: {text : "그룹명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'UP_GROUP_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "UP_GROUP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GROUP_LEVEL'  , width: 74, visible: true, readonly: false, editable: false, sortable: true, header: {text : "그룹레벨"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ORDER_SEQ'    , width: 67, visible: true, readonly: false, editable: false, sortable: true, header: {text : "정렬순서"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARK'       , width: 317, visible: true, readonly: false, editable: false, sortable: true, header: {text : "비고"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USE_YN'       , width: 60, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사용여부"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_code_typed = [
	{fieldName : 'AUTH_TYPE_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "AUTH_TYPE_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_CODE'     , width: 77, visible: true, readonly: false, editable: false, sortable: true, header: {text : "권한코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_NAME'     , width: 255, visible: true, readonly: false, editable: false, sortable: true, header: {text : "권한명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SORT_ORDER'    , width: 70, visible: true, readonly: false, editable: false, sortable: true, header: {text : "정열순서"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_code_typem = [
	{fieldName : 'AUTH_TYPE_CODE', width: 87, visible: true, readonly: false, editable: false, sortable: true, header: {text : "권한분류"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_TYPE_NAME', width: 149, visible: true, readonly: false, editable: false, sortable: true, header: {text : "권한분류명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SORT_ORDER'    , width: 65, visible: true, readonly: false, editable: false, sortable: true, header: {text : "정열순서"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_code_ref_detail = [
	{fieldName : 'REF_CODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REF_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COM_CODE'  , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COM_NAME'  , width: 176, visible: true, readonly: false, editable: false, sortable: true, header: {text : "코드명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COM_VALUE1'  , width: 98, visible: true, readonly: false, editable: false, sortable: true, header: {text : "참조값1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COM_VALUE2'  , width: 98, visible: true, readonly: false, editable: false, sortable: true, header: {text : "참조값2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COM_VALUE3'  , width: 85, visible: true, readonly: false, editable: false, sortable: true, header: {text : "참조값3"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SORT_ORDER'  , width: 64, visible: true, readonly: false, editable: false, sortable: true, header: {text : "정열순서"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_code_ref = [
	{fieldName : 'REF_CODE', width: 63, visible: true, readonly: false, editable: false, sortable: true, header: {text : "목차코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REF_NAME', width: 225, visible: true, readonly: false, editable: false, sortable: true, header: {text : "목차명"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_zipcode = [
	{fieldName : 'ZIP_CODE', width: 52, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ZIP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SIDO'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SIDO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GUGUN'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "GUGUN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DONG'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DONG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BUNJI'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "BUNJI"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_NAME'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ZIP_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_pagecode = [
	{fieldName : 'PAGE_ID'    , width: 176, visible: true, readonly: false, editable: false, sortable: true, header: {text : "프로그램 코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PAGE_NAME'  , width: 197, visible: true, readonly: false, editable: false, sortable: true, header: {text : "프로그램 명칭"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PAGE_URL'   , width: 277, visible: true, readonly: false, editable: false, sortable: true, header: {text : "프로그램 URL"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PAGE_TYPE'  , width: 37, visible: true, readonly: false, editable: false, sortable: true, header: {text : "유형"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_ID'     , width: 66, visible: true, readonly: false, editable: false, sortable: true, header: {text : "업무"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BASIC_YN'   , width: 35, visible: true, readonly: false, editable: false, sortable: true, header: {text : "기본"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BASIC_ORDER'   , width: 53, visible: true, readonly: false, editable: false, sortable: true, header: {text : "기본순번"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_systemcode = [
	{fieldName : 'SYS_ID'  , width: 52, visible: true, readonly: false, editable: false, sortable: true, header: {text : "코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_NAME'  , width: 287, visible: true, readonly: false, editable: false, sortable: true, header: {text : "시 스 템 명"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_systemcode = [
	{fieldName : 'SYS_ID'    , width: 52, visible: true, readonly: false, editable: false, sortable: true, header: {text : "코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_NAME'  , width: 287, visible: true, readonly: false, editable: false, sortable: true, header: {text : "시 스 템 명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USE_YN'    , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "구분"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SORT_ORDER'    , width: 63, visible: true, readonly: false, editable: false, sortable: true, header: {text : "정열순서"}, styles: _Styles_textc, 		editor: _Editor_text } 
];


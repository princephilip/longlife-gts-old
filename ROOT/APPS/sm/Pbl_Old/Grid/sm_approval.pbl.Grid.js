var sm_l_appro_detail = [
	{fieldName : 'COMPANY_CODE', width: 67, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Company Code"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NDOCNO'      , width: 57, visible: true, readonly: false, editable: false, sortable: true, header: {text : "문서번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NSEQ'        , width: 69, visible: true, readonly: false, editable: false, sortable: true, header: {text : "결재순서"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STRTYPE'     , width: 62, visible: true, readonly: false, editable: false, sortable: true, header: {text : "결재타입"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STRPERSONNO' , width: 86, visible: true, readonly: false, editable: false, sortable: true, header: {text : "결재자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYNC_APP'    , width: 71, visible: true, readonly: false, editable: false, sortable: true, header: {text : "결재구분"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STRLINE'     , width: 50, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Strline"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STRSUBTITLE' , width: 147, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Strsubtitle"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_l_appro_list = [
	{fieldName : 'STRCOMPANY', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "STRCOMPANY"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NDOCNO'    , width: 77, visible: true, readonly: false, editable: false, sortable: true, header: {text : "문서번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STRTITLE'  , width: 240, visible: true, readonly: false, editable: false, sortable: true, header: {text : "문서명"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dl_sw_pers_list = [
	{fieldName : 'COMPANY_CODE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_CODE'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DEPT_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'    , width: 120, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NO'       , width: 55, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사번"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NAME'     , width: 78, visible: true, readonly: false, editable: false, sortable: true, header: {text : "성명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GRADE_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "GRADE_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHOICE'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHOICE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_DIV_CODE'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DEPT_DIV_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PAYSTEP'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PAYSTEP"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GRADE_NAME'   , width: 47, visible: true, readonly: false, editable: false, sortable: true, header: {text : "직위"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL_NAME'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "LEVEL_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_find_detail = [
	{fieldName : 'COMPANY_CODE', width: 24, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HCODE'       , width: 22, visible: false, readonly: false, editable: false, sortable: true, header: {text : "HCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HNAME'       , width: 91, visible: true, readonly: false, editable: false, sortable: true, header: {text : "HNAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DCODE'       , width: 20, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SCODE'       , width: 74, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SERIAL_NO'   , width: 276, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SERIAL_NO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GUBUN'       , width: 69, visible: true, readonly: false, editable: false, sortable: true, header: {text : "GUBUN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SERIAL_CNT'  , width: 34, visible: true, readonly: false, editable: false, sortable: true, header: {text : "보유"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USE_CNT'     , width: 35, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사용"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARK'      , width: 175, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DNAME'       , width: 127, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DNAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_sw_master = [
	{fieldName : 'COMPANY_CODE', width: 12, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NO'      , width: 50, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사번"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NAME'    , width: 76, visible: true, readonly: false, editable: false, sortable: true, header: {text : "성명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL_CODE'  , width: 24, visible: false, readonly: false, editable: false, sortable: true, header: {text : "LEVEL_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL_NAME'  , width: 41, visible: true, readonly: false, editable: false, sortable: true, header: {text : "직급"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_CODE'   , width: 12, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DEPT_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'   , width: 192, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'IP_ADDR'     , width: 108, visible: true, readonly: false, editable: false, sortable: true, header: {text : "IP 주소"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_sw_detail = [
	{fieldName : 'COMPANY_CODE', width: 15, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NO'      , width: 12, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EMP_NO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NAME'    , width: 13, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EMP_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'IP_ADDR'     , width: 16, visible: false, readonly: false, editable: false, sortable: true, header: {text : "IP_ADDR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HCODE'       , width: 10, visible: false, readonly: false, editable: false, sortable: true, header: {text : "HCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HNAME'       , width: 78, visible: true, readonly: false, editable: false, sortable: true, header: {text : "HNAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DCODE'       , width: 16, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DNAME'       , width: 146, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DNAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SCODE'       , width: 75, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SERIAL_NO'   , width: 234, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SERIAL_NO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GUBUN'       , width: 58, visible: true, readonly: false, editable: false, sortable: true, header: {text : "GUBUN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARK'      , width: 172, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REMARK"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_dname_detail = [
	{fieldName : 'COMPANY_CODE', width: 24, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HCODE'       , width: 22, visible: false, readonly: false, editable: false, sortable: true, header: {text : "HCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HNAME'       , width: 91, visible: true, readonly: false, editable: false, sortable: true, header: {text : "HNAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DCODE'       , width: 20, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SCODE'       , width: 74, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SERIAL_NO'   , width: 276, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SERIAL_NO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GUBUN'       , width: 82, visible: true, readonly: false, editable: false, sortable: true, header: {text : "GUBUN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARK'      , width: 175, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DNAME'       , width: 127, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DNAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_d_sw_master = [
	{fieldName : 'DCODE', width: 40, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DNAME', width: 210, visible: true, readonly: false, editable: false, sortable: true, header: {text : "항목명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HCODE', width: 13, visible: false, readonly: false, editable: false, sortable: true, header: {text : "주코드"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_d_sw_gubun = [
	{fieldName : 'HCODE', width: 49, visible: true, readonly: false, editable: false, sortable: true, header: {text : "코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HNAME', width: 129, visible: true, readonly: false, editable: false, sortable: true, header: {text : "분류명"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_d_sw_detail = [
	{fieldName : 'COMPANY_CODE', width: 13, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HCODE'       , width: 13, visible: false, readonly: false, editable: false, sortable: true, header: {text : "HCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'H_HNAME'     , width: 10, visible: false, readonly: false, editable: false, sortable: true, header: {text : "H_HNAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DCODE'       , width: 11, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SCODE'       , width: 102, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SERIAL_NO'   , width: 316, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SERIAL_NO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SERIAL_CNT'  , width: 88, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SERIAL_CNT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GUBUN'       , width: 74, visible: true, readonly: false, editable: false, sortable: true, header: {text : "GUBUN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARK'      , width: 278, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REMARK"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_appro_pass = [
	{fieldName : 'STRCOMPANYNO'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "STRCOMPANYNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NDOCNO'          , width: 72, visible: true, readonly: false, editable: false, sortable: true, header: {text : "문서번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STRPERSONNO'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "STRPERSONNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STRALTERPERSONNO'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "STRALTERPERSONNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARK'          , width: 437, visible: true, readonly: false, editable: false, sortable: true, header: {text : "비고"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STRTITLE'        , width: 240, visible: true, readonly: false, editable: false, sortable: true, header: {text : "문서명"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dl_gw_appro_process = [
	{fieldName : 'NAPPRONO'        , width: 49, visible: true, readonly: false, editable: false, sortable: true, header: {text : "일련번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NLINESEQUENCE'   , width: 52, visible: true, readonly: false, editable: false, sortable: true, header: {text : "순번"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STRPERSONNO'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "STRPERSONNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STRDEPARTMENTNO' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "STRDEPARTMENTNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STRJOBLEVEL'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "STRJOBLEVEL"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ISDEPARTMENT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ISDEPARTMENT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ISVIEW'          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ISVIEW"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ISTYPE'          , width: 33, visible: true, readonly: false, editable: false, sortable: true, header: {text : "구분"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ISABSENCE'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ISABSENCE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ISAPPROCHECK'    , width: 45, visible: true, readonly: false, editable: false, sortable: true, header: {text : "결재"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STRALTERPERSONNO'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "STRALTERPERSONNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DATEAPPRO'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DATEAPPRO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYNC_APP'        , width: 56, visible: true, readonly: false, editable: false, sortable: true, header: {text : "동시"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NAME'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EMP_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NDOCNO'          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "NDOCNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONTENT'         , width: 310, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CONTENT"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_l_appro_doc = [
	{fieldName : 'NDOCNO'      , width: 67, visible: true, readonly: false, editable: false, sortable: true, header: {text : "문서번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STRCOMPANY'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "STRCOMPANY"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STRTITLE'    , width: 240, visible: true, readonly: false, editable: false, sortable: true, header: {text : "문서명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STRAPPROTYPE'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "STRAPPROTYPE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PATH_INFO'   , width: 38, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PASS"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dl_gw_appro_pers_list = [
	{fieldName : 'COMPANY_CODE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_CODE'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DEPT_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'    , width: 120, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NO'       , width: 55, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사번"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NAME'     , width: 78, visible: true, readonly: false, editable: false, sortable: true, header: {text : "성명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GRADE_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "GRADE_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHOICE'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHOICE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_DIV_CODE'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DEPT_DIV_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PAYSTEP'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PAYSTEP"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GRADE_NAME'   , width: 47, visible: true, readonly: false, editable: false, sortable: true, header: {text : "직위"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL_NAME'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "LEVEL_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dl_gw_appro_path_1 = [
	{fieldName : 'ID'    , width: 471, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Id"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NAME'  , width: 626, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Name"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PARENT'  , width: 471, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Parent"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL' , width: 361, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Level"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ISLEAF' , width: 357, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Isleaf"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EXTEND' , width: 219, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Extend"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TYPE'  , width: 155, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Type"}, styles: _Styles_textc, 		editor: _Editor_text } 
];


var dl_code_condo_detail = [
	{fieldName : 'REF_CODE'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REF_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REF_CODE_S'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REF_CODE_S"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REF_NAME'      , width: 178, visible: true, readonly: false, editable: false, sortable: true, header: {text : "콘도명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REF_LOC_NAME'  , width: 166, visible: true, readonly: false, editable: false, sortable: true, header: {text : "콘도위치"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REF_SORT_ORDER'  , width: 70, visible: true, readonly: false, editable: false, sortable: true, header: {text : "정렬순서"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REF_COM_NAME'  , width: 126, visible: true, readonly: false, editable: false, sortable: true, header: {text : "소유회사"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REF_USE_TAG'   , width: 61, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사용유무"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HOMEPAGE_URL'  , width: 208, visible: true, readonly: false, editable: false, sortable: true, header: {text : "홈페이지"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dl_code_condo = [
	{fieldName : 'REF_CODE'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REF_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REF_CODE_S'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REF_CODE_S"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REF_NAME'      , width: 227, visible: true, readonly: false, editable: false, sortable: true, header: {text : "콘도명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REF_LOC_NAME'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REF_LOC_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REF_SORT_ORDER'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REF_SORT_ORDER"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REF_COM_NAME'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REF_COM_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REF_USE_TAG'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REF_USE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dl_code_stationery = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FIX_NUMBER'  , width: 66, visible: true, readonly: false, editable: false, sortable: true, header: {text : "일련번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ITEM_NAME'   , width: 186, visible: true, readonly: false, editable: false, sortable: true, header: {text : "품명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FIND_NAME'   , width: 413, visible: true, readonly: false, editable: false, sortable: true, header: {text : "검색어 명칭"}, styles: _Styles_textc, 		editor: _Editor_text } 
];


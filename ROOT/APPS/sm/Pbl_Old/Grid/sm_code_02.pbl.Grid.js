var sm_f_deptcode_1 = [
	{fieldName : 'COMPANY_CODE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_CODE'    , width: 77, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'    , width: 263, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SORT_ORDER'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SORT_ORDER"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USING_TAG'    , width: 48, visible: true, readonly: false, editable: false, sortable: true, header: {text : "USING_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'OFFICE_CODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "OFFICE_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GROUP_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "GROUP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GROUP_NAME'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "GROUP_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_DIV_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DEPT_DIV_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_DIV_NAME'   , width: 46, visible: true, readonly: false, editable: false, sortable: true, header: {text : "구분"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROJ_DIV_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PROJ_DIV_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROJ_DIV_NAME'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PROJ_DIV_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_d_validationcode = [
	{fieldName : 'VALID_VALUE_CODE', width: 32, visible: true, readonly: false, editable: false, sortable: true, header: {text : "VALID_VALUE_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'VALID_VALUE_NAME', width: 134, visible: true, readonly: false, editable: false, sortable: true, header: {text : "VALID_VALUE_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_programcode = [
	{fieldName : 'SM_AUTH_PGMAUTH_PGMCODE', width: 75, visible: true, readonly: false, editable: false, sortable: true, header: {text : "프로그램코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMNAME'                , width: 161, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PGMNAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'MENU_NAME'              , width: 281, visible: true, readonly: false, editable: false, sortable: true, header: {text : "메뉴위치"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_AUTH_PGMCODE_REMARK' , width: 232, visible: true, readonly: false, editable: false, sortable: true, header: {text : "검색어"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_d_code_ref = [
	{fieldName : 'COM_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COM_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COM_NAME', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COM_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_deptcode2 = [
	{fieldName : 'CHK'         , width: 35, visible: true, readonly: false, editable: false, sortable: true, header: {text : "선택"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPANY_CODE'         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_CODE'   , width: 66, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'   , width: 283, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COST_CODE'   , width: 110, visible: true, readonly: false, editable: false, sortable: true, header: {text : "COST_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GROUP_CODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "GROUP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPANY_NAME'  , width: 178, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사업자명"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_groupcode = [
	{fieldName : 'GROUP_CODE'   , width: 48, visible: true, readonly: false, editable: false, sortable: true, header: {text : "GROUP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GROUP_NAME'   , width: 328, visible: true, readonly: false, editable: false, sortable: true, header: {text : "GROUP_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SE_GROUP_NAME'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SE_GROUP_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVELS'       , width: 39, visible: true, readonly: false, editable: false, sortable: true, header: {text : "LEVELS"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_deptcode = [
	{fieldName : 'DEPT_CODE', width: 86, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME', width: 328, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_businesscode = [
	{fieldName : 'BUSINESS_CODE', width: 86, visible: true, readonly: false, editable: false, sortable: true, header: {text : "BUSINESS_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BUSINESS_NAME', width: 328, visible: true, readonly: false, editable: false, sortable: true, header: {text : "BUSINESS_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_d_codedept = [
	{fieldName : 'DEPT_CODE'   , width: 58, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'   , width: 255, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPANY_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_d_company = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPANY_NAME', width: 155, visible: true, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_codeheadquater = [
	{fieldName : 'COMPANY_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HEADQUATER_CODE'   , width: 110, visible: true, readonly: false, editable: false, sortable: true, header: {text : "본부코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HEADQUATER_NAME'   , width: 276, visible: true, readonly: false, editable: false, sortable: true, header: {text : "본부명칭"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USE_YN'         , width: 87, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사용여부"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPANY_NAME'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_codecust_list = [
	{fieldName : 'CUST_CODE'       , width: 115, visible: true, readonly: false, editable: false, sortable: true, header: {text : "거래처코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'APPROVE_YN'      , width: 50, visible: true, readonly: false, editable: false, sortable: true, header: {text : "APPROVE_YN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USE_YN'          , width: 49, visible: true, readonly: false, editable: false, sortable: true, header: {text : "USE_YN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CUST_DIV'        , width: 89, visible: true, readonly: false, editable: false, sortable: true, header: {text : "거래처구분"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CUST_NAME'       , width: 131, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CUST_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CUST_ABBR'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CUST_ABBR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CUST_ENG'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CUST_ENG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CUST_LEGAL_NO'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CUST_LEGAL_NO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REPRESENT_NAME'  , width: 82, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REPRESENT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REPRESENT_RRN'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REPRESENT_RRN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BIZ_STATUS'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "BIZ_STATUS"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BIZ_TYPE'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "BIZ_TYPE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PHONE'           , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PHONE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FAX'             , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "FAX"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_CODE'        , width: 69, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ZIP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR1'           , width: 412, visible: true, readonly: false, editable: false, sortable: true, header: {text : "주소  1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR2'           , width: 339, visible: true, readonly: false, editable: false, sortable: true, header: {text : "주소  2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CUST_MATCHING'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CUST_MATCHING"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CUST_KIND'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CUST_KIND"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INOUT_TAG'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "INOUT_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PARTNER_TAG'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PARTNER_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INPUT_COMP_CODE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "INPUT_COMP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INPUT_DUTY_ID'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "INPUT_DUTY_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INPUT_DATE'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "INPUT_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHG_COMP_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHG_COMP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHG_DUTY_ID'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHG_DUTY_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHG_DATE'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHG_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_ID'          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMP_CODE'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STATUS_CODE'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "STATUS_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CLOSING_DATE'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CLOSING_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'MAIN_CUST_CODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "MAIN_CUST_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CUST_NAME'       , width: 131, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CUST_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INPUT_DUTY_ID_NM'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "INPUT_DUTY_ID_NM"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHG_DUTY_ID_NM'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHG_DUTY_ID_NM"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_businesscode = [
	{fieldName : 'COMPANY_CODE'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BUSINESS_CODE'      , width: 86, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사업코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BUSINESS_NAME'      , width: 263, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사업명칭"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BUSINESS_SHORT_NAME'      , width: 118, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사업약칭"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USE_YN'             , width: 63, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사용여부"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TERMINATE_DATE'     , width: 82, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사용종료일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INPUT_DUTY_ID'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "INPUT_DUTY_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INPUT_DATE'         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "INPUT_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHG_DUTY_ID'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHG_DUTY_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHG_DATE'           , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHG_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SORT_ORDER'         , width: 79, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SORT_ORDER"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPANY_NAME'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_codecalender = [
	{fieldName : 'WORK_DATE'   , width: 72, visible: true, readonly: false, editable: false, sortable: true, header: {text : "일자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SATURDAY_YN' , width: 58, visible: true, readonly: false, editable: false, sortable: true, header: {text : "토요일여부"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SUNDAY_YN'   , width: 65, visible: true, readonly: false, editable: false, sortable: true, header: {text : "일요일여부"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HOLIDAY_YN'  , width: 64, visible: true, readonly: false, editable: false, sortable: true, header: {text : "공휴일여부"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HOLIDAY_NAME'  , width: 205, visible: true, readonly: false, editable: false, sortable: true, header: {text : "공휴일명칭"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'WORK_YN'     , width: 63, visible: true, readonly: false, editable: false, sortable: true, header: {text : "평일여부"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARK'      , width: 255, visible: true, readonly: false, editable: false, sortable: true, header: {text : "비고"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_codecust_edit = [
	{fieldName : 'CUST_CODE_2'     , width: 196, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CUST_CODE_2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'APPROVE_YN'      , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "APPROVE_YN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USE_YN'          , width: 59, visible: true, readonly: false, editable: false, sortable: true, header: {text : "USE_YN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CUST_DIV'        , width: 196, visible: true, readonly: false, editable: false, sortable: true, header: {text : "거래처구분"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CUST_NAME'       , width: 196, visible: true, readonly: false, editable: false, sortable: true, header: {text : "거래처명칭"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CUST_ABBR'       , width: 196, visible: true, readonly: false, editable: false, sortable: true, header: {text : "거래처약명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CUST_ENG'        , width: 196, visible: true, readonly: false, editable: false, sortable: true, header: {text : "거래처영문"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CUST_LEGAL_NO'   , width: 196, visible: true, readonly: false, editable: false, sortable: true, header: {text : "법인번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REPRESENT_NAME'  , width: 196, visible: true, readonly: false, editable: false, sortable: true, header: {text : "대표자명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REPRESENT_RRN'   , width: 196, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REPRESENT_RRN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BIZ_STATUS'      , width: 196, visible: true, readonly: false, editable: false, sortable: true, header: {text : "업태"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BIZ_TYPE'        , width: 196, visible: true, readonly: false, editable: false, sortable: true, header: {text : "업종"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PHONE'           , width: 196, visible: true, readonly: false, editable: false, sortable: true, header: {text : "전화번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FAX'             , width: 196, visible: true, readonly: false, editable: false, sortable: true, header: {text : "팩스번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_CODE'        , width: 59, visible: true, readonly: false, editable: false, sortable: true, header: {text : "우편번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR1'           , width: 268, visible: true, readonly: false, editable: false, sortable: true, header: {text : "주소"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR2'           , width: 268, visible: true, readonly: false, editable: false, sortable: true, header: {text : "나머지주소"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CUST_MATCHING'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CUST_MATCHING"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CUST_KIND'       , width: 268, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CUST_KIND"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INOUT_TAG'       , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "INOUT_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PARTNER_TAG'     , width: 126, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PARTNER_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INPUT_COMP_CODE' , width: 196, visible: true, readonly: false, editable: false, sortable: true, header: {text : "INPUT_COMP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INPUT_DUTY_ID'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "INPUT_DUTY_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INPUT_DATE'      , width: 126, visible: true, readonly: false, editable: false, sortable: true, header: {text : "INPUT_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHG_COMP_CODE'   , width: 196, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHG_COMP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHG_DUTY_ID'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHG_DUTY_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHG_DATE'        , width: 126, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHG_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_ID'          , width: 196, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMP_CODE'       , width: 196, visible: true, readonly: false, editable: false, sortable: true, header: {text : "COMP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STATUS_CODE'     , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "STATUS_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CLOSING_DATE'    , width: 83, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CLOSING_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'MAIN_CUST_CODE'  , width: 77, visible: true, readonly: false, editable: false, sortable: true, header: {text : "MAIN_CUST_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'MAIN_CUST_NAME'  , width: 201, visible: true, readonly: false, editable: false, sortable: true, header: {text : "MAIN_CUST_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INPUT_DUTY_ID_NM'  , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "INPUT_DUTY_ID_NM"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHG_DUTY_ID_NM'  , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHG_DUTY_ID_NM"}, styles: _Styles_textc, 		editor: _Editor_text } 
];


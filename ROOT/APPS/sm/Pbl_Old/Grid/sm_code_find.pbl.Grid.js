var dl_sm_find_proj = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROJ_CODE'   , width: 302, visible: true, readonly: false, editable: false, sortable: true, header: {text : "현장코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROJ_NAME'   , width: 1330, visible: true, readonly: false, editable: false, sortable: true, header: {text : "현장명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PM_EMPNO'    , width: 302, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PM"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HEAD_EMPNO'  , width: 302, visible: true, readonly: false, editable: false, sortable: true, header: {text : "현장소장"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'MANAGE_TAG'  , width: 229, visible: true, readonly: false, editable: false, sortable: true, header: {text : "관리여부"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_CODE'   , width: 206, visible: true, readonly: false, editable: false, sortable: true, header: {text : "관리부서"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_CODE'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ZIP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR1'       , width: 649, visible: true, readonly: false, editable: false, sortable: true, header: {text : "주소"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PHONE_NO1'   , width: 576, visible: true, readonly: false, editable: false, sortable: true, header: {text : "현장전화번호1"}, styles: _Styles_textc, 		editor: _Editor_text } 
];


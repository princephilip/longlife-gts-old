var de_standard_row_8 = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAY_YEAR'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PLAY_YEAR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EST_CODE'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EST_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EST_NAME'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EST_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ENT_DEPT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ENT_DEPT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAY_YYMM'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀4"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CNST_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_DIV'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀3"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PM_EMPNO'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PM_EMPNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EXPT_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GOAL_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAN_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ACHV_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'THIS_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "THIS_AMT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NEXT_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "가나다라마"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RECV_DIV'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STATUS_DIV'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀4"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARKS'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REMARKS"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dl_standard = [
	{fieldName : 'COLUMN1', width: 274, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Column1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COLUMN2', width: 274, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Column2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COLUMN3', width: 274, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Column3"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COLUMN4', width: 274, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Column4"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COLUMN5', width: 274, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Column5"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COLUMN6', width: 274, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Column6"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COLUMN7', width: 274, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Column7"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COLUMN8', width: 274, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Column8"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COLUMN9', width: 274, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Column9"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var de_standard_row_6 = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAY_YEAR'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PLAY_YEAR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EST_CODE'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EST_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EST_NAME'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EST_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ENT_DEPT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ENT_DEPT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAY_YYMM'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀4"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CNST_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_DIV'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀3"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PM_EMPNO'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PM_EMPNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EXPT_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GOAL_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAN_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ACHV_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'THIS_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "THIS_AMT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NEXT_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "가나다라마"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RECV_DIV'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STATUS_DIV'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀4"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARKS'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REMARKS"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var de_standard_row_1 = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAY_YEAR'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PLAY_YEAR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EST_CODE'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EST_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EST_NAME'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EST_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ENT_DEPT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ENT_DEPT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAY_YYMM'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PLAY_YYMM"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CNST_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CNST_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_DIV'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CONST_DIV"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PM_EMPNO'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PM_EMPNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EXPT_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GOAL_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "GOAL_AMT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAN_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PLAN_AMT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ACHV_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ACHV_AMT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'THIS_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "THIS_AMT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NEXT_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "가나다라마"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RECV_DIV'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "RECV_DIV"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STATUS_DIV'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "STATUS_DIV"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARKS'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REMARKS"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var de_standard_row_2 = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAY_YEAR'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PLAY_YEAR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EST_CODE'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EST_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EST_NAME'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EST_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ENT_DEPT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ENT_DEPT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAY_YYMM'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PLAY_YYMM"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CNST_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_DIV'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CONST_DIV"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PM_EMPNO'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PM_EMPNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EXPT_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GOAL_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAN_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PLAN_AMT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ACHV_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ACHV_AMT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'THIS_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "THIS_AMT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NEXT_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "가나다라마"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RECV_DIV'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STATUS_DIV'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "STATUS_DIV"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARKS'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REMARKS"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var de_standard_row_7 = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAY_YEAR'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PLAY_YEAR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EST_CODE'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EST_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EST_NAME'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EST_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ENT_DEPT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ENT_DEPT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAY_YYMM'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀4"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CNST_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_DIV'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀3"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PM_EMPNO'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PM_EMPNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EXPT_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GOAL_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAN_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ACHV_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'THIS_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "THIS_AMT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NEXT_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "가나다라마"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RECV_DIV'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STATUS_DIV'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀4"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARKS'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REMARKS"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var de_standard_row_5 = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAY_YEAR'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PLAY_YEAR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EST_CODE'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EST_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EST_NAME'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EST_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ENT_DEPT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ENT_DEPT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAY_YYMM'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀4"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CNST_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_DIV'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀3"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PM_EMPNO'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PM_EMPNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EXPT_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GOAL_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAN_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ACHV_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'THIS_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "THIS_AMT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NEXT_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "가나다라마"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RECV_DIV'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STATUS_DIV'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀4"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARKS'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REMARKS"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var de_standard_row_4 = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAY_YEAR'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PLAY_YEAR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EST_CODE'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EST_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EST_NAME'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EST_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ENT_DEPT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ENT_DEPT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAY_YYMM'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀4"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CNST_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_DIV'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀3"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PM_EMPNO'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PM_EMPNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EXPT_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GOAL_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAN_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ACHV_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'THIS_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "THIS_AMT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NEXT_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "가나다라마"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RECV_DIV'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STATUS_DIV'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀4"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARKS'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REMARKS"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var de_standard_row_3 = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAY_YEAR'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PLAY_YEAR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EST_CODE'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EST_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EST_NAME'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EST_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ENT_DEPT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ENT_DEPT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAY_YYMM'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PLAY_YYMM"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CNST_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONST_DIV'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀3"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PM_EMPNO'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PM_EMPNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EXPT_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GOAL_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAN_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ACHV_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ACHV_AMT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'THIS_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "THIS_AMT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NEXT_AMT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "가나다라마"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RECV_DIV'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "타이틀2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STATUS_DIV'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "STATUS_DIV"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARKS'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REMARKS"}, styles: _Styles_textc, 		editor: _Editor_text } 
];


var sm_dev_schedule_list_old = [
	{fieldName : 'SYS_NAME' , width: 49, visible: true, readonly: false, editable: false, sortable: true, header: {text : "시스템"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SUB_NAME' , width: 68, visible: true, readonly: false, editable: false, sortable: true, header: {text : "중분류"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMNAME'  , width: 135, visible: true, readonly: false, editable: false, sortable: true, header: {text : "프로그램명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_ID'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SUBCODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SUBCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMCODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PGMCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TOTAL_COU'  , width: 33, visible: true, readonly: false, editable: false, sortable: true, header: {text : ""등록
건수""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_COU'  , width: 35, visible: true, readonly: false, editable: false, sortable: true, header: {text : ""개발
완료""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_COU'  , width: 37, visible: true, readonly: false, editable: false, sortable: true, header: {text : ""점검
완료""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_COU' , width: 34, visible: true, readonly: false, editable: false, sortable: true, header: {text : ""전산
완료""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHA_COU'  , width: 34, visible: true, readonly: false, editable: false, sortable: true, header: {text : ""현업
완료""}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_dev_unit_test1 = [
	{fieldName : 'SYS_ID'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMCODE'       , width: 78, visible: true, readonly: false, editable: false, sortable: true, header: {text : "프로그램코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEVELOPER'     , width: 103, visible: true, readonly: false, editable: false, sortable: true, header: {text : "개발자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'WRITE_DATE'    , width: 78, visible: true, readonly: false, editable: false, sortable: true, header: {text : "작성일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TYPE_I'        , width: 36, visible: true, readonly: false, editable: false, sortable: true, header: {text : "입력"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TYPE_R'        , width: 41, visible: true, readonly: false, editable: false, sortable: true, header: {text : "조회"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TYPE_P'        , width: 41, visible: true, readonly: false, editable: false, sortable: true, header: {text : "출력"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TESTER_D1'     , width: 64, visible: true, readonly: false, editable: false, sortable: true, header: {text : "담당자(PL)"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TESTER_D2'     , width: 72, visible: true, readonly: false, editable: false, sortable: true, header: {text : "담당자(PM)"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TESTER_C1'     , width: 82, visible: true, readonly: false, editable: false, sortable: true, header: {text : "담당자(고객1)"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TESTER_C2'     , width: 83, visible: true, readonly: false, editable: false, sortable: true, header: {text : "담당자(고객2)"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TEST_CONDITION'     , width: 437, visible: true, readonly: false, editable: false, sortable: true, header: {text : "테스트 조건"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMNAME'       , width: 149, visible: true, readonly: false, editable: false, sortable: true, header: {text : "프로그램명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_NAME'      , width: 72, visible: true, readonly: false, editable: false, sortable: true, header: {text : "단위 시스템"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_dev_unit_test_result = [
	{fieldName : 'SYS_ID'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMCODE'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PGMCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEQ'         , width: 25, visible: true, readonly: false, editable: false, sortable: true, header: {text : "순번"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GUBUN'       , width: 61, visible: true, readonly: false, editable: false, sortable: true, header: {text : "구 분"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TEST_ITEM'   , width: 436, visible: true, readonly: false, editable: false, sortable: true, header: {text : "테 스 트  항 목"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_DEV'     , width: 41, visible: true, readonly: false, editable: false, sortable: true, header: {text : "개발자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_DEV_DATE'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHK_DEV_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_D1'      , width: 40, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PL"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_D1_DATE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHK_D1_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_D2'      , width: 40, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PM"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_D2_DATE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHK_D2_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_C1'      , width: 41, visible: true, readonly: false, editable: false, sortable: true, header: {text : "고객1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_C1_DATE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHK_C1_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_C2'      , width: 41, visible: true, readonly: false, editable: false, sortable: true, header: {text : "고객2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_C2_DATE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHK_C2_DATE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_dev_unit_test_make = [
	{fieldName : 'RN'  , width: 85, visible: true, readonly: false, editable: false, sortable: true, header: {text : "테스트 사람"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CNT' , width: 54, visible: true, readonly: false, editable: false, sortable: true, header: {text : "총 개수"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'YCNT' , width: 49, visible: true, readonly: false, editable: false, sortable: true, header: {text : "완 료"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NCNT' , width: 49, visible: true, readonly: false, editable: false, sortable: true, header: {text : "미완료"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_dev_unit_test_detail = [
	{fieldName : 'SYS_ID'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMCODE'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PGMCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEVELOPER'     , width: 222, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEVELOPER"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'WRITE_DATE'    , width: 83, visible: true, readonly: false, editable: false, sortable: true, header: {text : "WRITE_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TYPE_I'        , width: 17, visible: true, readonly: false, editable: false, sortable: true, header: {text : "TYPE_I"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TYPE_R'        , width: 15, visible: true, readonly: false, editable: false, sortable: true, header: {text : "TYPE_R"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TYPE_P'        , width: 17, visible: true, readonly: false, editable: false, sortable: true, header: {text : "TYPE_P"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TESTER_D1'     , width: 102, visible: true, readonly: false, editable: false, sortable: true, header: {text : "TESTER_D1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TESTER_D2'     , width: 102, visible: true, readonly: false, editable: false, sortable: true, header: {text : "TESTER_D2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TESTER_C1'     , width: 138, visible: true, readonly: false, editable: false, sortable: true, header: {text : "TESTER_C1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TESTER_C2'     , width: 138, visible: true, readonly: false, editable: false, sortable: true, header: {text : "TESTER_C2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TEST_CONDITION'     , width: 669, visible: true, readonly: false, editable: false, sortable: true, header: {text : "TEST_CONDITION"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMNAME'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PGMNAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_NAME'      , width: 222, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SYS_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_dev_unit_test = [
	{fieldName : 'SYS_ID'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMCODE'       , width: 78, visible: true, readonly: false, editable: false, sortable: true, header: {text : "프로그램코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEVELOPER'     , width: 103, visible: true, readonly: false, editable: false, sortable: true, header: {text : "개발자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'WRITE_DATE'    , width: 78, visible: true, readonly: false, editable: false, sortable: true, header: {text : "작성일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TYPE_I'        , width: 36, visible: true, readonly: false, editable: false, sortable: true, header: {text : "입력"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TYPE_R'        , width: 41, visible: true, readonly: false, editable: false, sortable: true, header: {text : "조회"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TYPE_P'        , width: 41, visible: true, readonly: false, editable: false, sortable: true, header: {text : "출력"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TESTER_D1'     , width: 117, visible: true, readonly: false, editable: false, sortable: true, header: {text : "테스트담당자1(PL)"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TESTER_D2'     , width: 121, visible: true, readonly: false, editable: false, sortable: true, header: {text : "테스트담당자2(PM)"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TESTER_C1'     , width: 130, visible: true, readonly: false, editable: false, sortable: true, header: {text : "테스트담당자(고객1)"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TESTER_C2'     , width: 125, visible: true, readonly: false, editable: false, sortable: true, header: {text : "테트스담당자(고객2)"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TEST_CONDITION'     , width: 437, visible: true, readonly: false, editable: false, sortable: true, header: {text : "테스트 조건"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMNAME'       , width: 149, visible: true, readonly: false, editable: false, sortable: true, header: {text : "프로그램명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_NAME'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'MENULEVEL'     , width: 256, visible: true, readonly: false, editable: false, sortable: true, header: {text : "MENULEVEL"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_dev_unit_test_item = [
	{fieldName : 'SEQ'      , width: 40, visible: true, readonly: false, editable: false, sortable: true, header: {text : "순번"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GUBUN'    , width: 90, visible: true, readonly: false, editable: false, sortable: true, header: {text : "구  분"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TEST_ITEM'    , width: 778, visible: true, readonly: false, editable: false, sortable: true, header: {text : "테스트 항목"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_dev_schedule_view = [
	{fieldName : 'SYS_ID'           , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SUBCODE'          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SUBCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMCODE'          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PGMCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEQ'              , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_REVIEW'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DEPT_REVIEW"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_REVIEW'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHARGE_REVIEW"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TITLE'            , width: 245, visible: true, readonly: false, editable: false, sortable: true, header: {text : "TITLE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQ_NAME'         , width: 115, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REQ_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQ_DATE'         , width: 68, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REQ_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQ_REMARK'       , width: 356, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REQ_REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REG_NAME'         , width: 113, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REG_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REG_DATE'         , width: 152, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REG_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REG_REMARK'       , width: 356, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REG_REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'WEIGHT_DIV'       , width: 36, visible: true, readonly: false, editable: false, sortable: true, header: {text : "WEIGHT_DIV"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_TAG'          , width: 47, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEV_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_NAME'         , width: 115, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEV_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_TERM'         , width: 86, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEV_TERM"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_ENDPLANDATE'  , width: 118, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEV_ENDPLANDATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_ENDDATE'      , width: 114, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEV_ENDDATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_REMARK'       , width: 356, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEV_REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_ENDYN'        , width: 23, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEV_ENDYN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_ENDCHKDATE'   , width: 127, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEV_ENDCHKDATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_NAME'         , width: 116, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHK_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_DATE'         , width: 127, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHK_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_REMARK'       , width: 397, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHK_REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_ENDYN'        , width: 68, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHK_ENDYN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_ENDCHKDATE'   , width: 130, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHK_ENDCHKDATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'        , width: 95, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_DATE'        , width: 82, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_REMARK'      , width: 394, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_ENDYN'       , width: 98, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_ENDYN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_ENDCHK_DATE' , width: 119, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_ENDCHK_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_ID'        , width: 95, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHARGE_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_NAME'      , width: 46, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHARGE_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_DATE'      , width: 62, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHARGE_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_REMARK'    , width: 395, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHARGE_REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_ENDYN'     , width: 64, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHARGE_ENDYN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_ENDCHKDATE'     , width: 122, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHARGE_ENDCHKDATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DIVISION_TAG'     , width: 47, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DIVISION_TAG"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_dev_schedule_main3 = [
	{fieldName : 'SYS_ID'           , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SUBCODE'          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SUBCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMCODE'          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PGMCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEQ'              , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_REVIEW'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DEPT_REVIEW"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_REVIEW'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHARGE_REVIEW"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TITLE'            , width: 240, visible: true, readonly: false, editable: false, sortable: true, header: {text : "제목"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQ_NAME'         , width: 115, visible: true, readonly: false, editable: false, sortable: true, header: {text : "요청자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQ_DATE'         , width: 68, visible: true, readonly: false, editable: false, sortable: true, header: {text : "요청일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQ_REMARK'       , width: 356, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REQ_REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REG_NAME'         , width: 113, visible: true, readonly: false, editable: false, sortable: true, header: {text : "등록자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REG_DATE'         , width: 152, visible: true, readonly: false, editable: false, sortable: true, header: {text : "등록일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REG_REMARK'       , width: 356, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REG_REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'WEIGHT_DIV'       , width: 35, visible: true, readonly: false, editable: false, sortable: true, header: {text : "중요"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_TAG'          , width: 47, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEV_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_NAME'         , width: 115, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEV_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_TERM'         , width: 86, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEV_TERM"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_ENDPLANDATE'  , width: 118, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEV_ENDPLANDATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_ENDDATE'      , width: 114, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEV_ENDDATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_REMARK'       , width: 356, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEV_REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_ENDYN'        , width: 23, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEV_ENDYN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_ENDCHKDATE'   , width: 127, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEV_ENDCHKDATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_NAME'         , width: 116, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHK_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_DATE'         , width: 127, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHK_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_REMARK'       , width: 397, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHK_REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_ENDYN'        , width: 68, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHK_ENDYN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_ENDCHKDATE'   , width: 130, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHK_ENDCHKDATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'        , width: 95, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_DATE'        , width: 82, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_REMARK'      , width: 394, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_ENDYN'       , width: 98, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_ENDYN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_ENDCHK_DATE' , width: 119, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_ENDCHK_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_ID'        , width: 62, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHARGE_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_NAME'      , width: 46, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHARGE_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_DATE'      , width: 62, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHARGE_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_REMARK'    , width: 395, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHARGE_REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_ENDYN'     , width: 64, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHARGE_ENDYN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_ENDCHKDATE'     , width: 122, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHARGE_ENDCHKDATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DIVISION_TAG'     , width: 47, visible: true, readonly: false, editable: false, sortable: true, header: {text : "대상"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_dev_schedule_main_list3 = [
	{fieldName : 'SYS_ID'           , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SUBCODE'          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SUBCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMCODE'          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PGMCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEQ'              , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_REVIEW'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DEPT_REVIEW"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_REVIEW'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHARGE_REVIEW"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TITLE'            , width: 198, visible: true, readonly: false, editable: false, sortable: true, header: {text : "제목"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQ_NAME'         , width: 43, visible: true, readonly: false, editable: false, sortable: true, header: {text : "요청자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQ_DATE'         , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "요청일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQ_REMARK'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REQ_REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REG_NAME'         , width: 65, visible: true, readonly: false, editable: false, sortable: true, header: {text : "등록자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REG_DATE'         , width: 71, visible: true, readonly: false, editable: false, sortable: true, header: {text : "등록일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REG_REMARK'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REG_REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'WEIGHT_DIV'       , width: 43, visible: true, readonly: false, editable: false, sortable: true, header: {text : "중요도"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_TAG'          , width: 60, visible: true, readonly: false, editable: false, sortable: true, header: {text : "진행구분"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_NAME'         , width: 42, visible: true, readonly: false, editable: false, sortable: true, header: {text : "개발자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_TERM'         , width: 94, visible: true, readonly: false, editable: false, sortable: true, header: {text : "개발소요일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_ENDPLANDATE'  , width: 96, visible: true, readonly: false, editable: false, sortable: true, header: {text : "개발완료예정일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_ENDDATE'      , width: 71, visible: true, readonly: false, editable: false, sortable: true, header: {text : "개발완료일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_REMARK'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DEV_REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_ENDYN'        , width: 51, visible: true, readonly: false, editable: false, sortable: true, header: {text : "개발완료"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_ENDCHKDATE'   , width: 116, visible: true, readonly: false, editable: false, sortable: true, header: {text : "개발완료체크일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_NAME'         , width: 61, visible: true, readonly: false, editable: false, sortable: true, header: {text : "개발점검자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_DATE'         , width: 69, visible: true, readonly: false, editable: false, sortable: true, header: {text : "개발점검일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_REMARK'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHK_REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_ENDYN'        , width: 56, visible: true, readonly: false, editable: false, sortable: true, header: {text : "점검완료"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_ENDCHKDATE'   , width: 115, visible: true, readonly: false, editable: false, sortable: true, header: {text : "개발점검완료체크일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'        , width: 76, visible: true, readonly: false, editable: false, sortable: true, header: {text : "전산팀점검자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_DATE'        , width: 69, visible: true, readonly: false, editable: false, sortable: true, header: {text : "전산팀점검일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_REMARK'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DEPT_REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_ENDYN'       , width: 54, visible: true, readonly: false, editable: false, sortable: true, header: {text : "점검완료"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_ENDCHK_DATE' , width: 128, visible: true, readonly: false, editable: false, sortable: true, header: {text : "전산팀점검완료체크일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_ID'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHARGE_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_NAME'      , width: 67, visible: true, readonly: false, editable: false, sortable: true, header: {text : "현업담당자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_DATE'      , width: 69, visible: true, readonly: false, editable: false, sortable: true, header: {text : "현업점검일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_REMARK'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHARGE_REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_ENDYN'     , width: 53, visible: true, readonly: false, editable: false, sortable: true, header: {text : "점검완료"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_ENDCHKDATE'     , width: 130, visible: true, readonly: false, editable: false, sortable: true, header: {text : "현업점검완료체크일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DIVISION_TAG'     , width: 60, visible: true, readonly: false, editable: false, sortable: true, header: {text : "대상구분"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_dev_detail_search = [
	{fieldName : 'SYSNAME'          , width: 53, visible: true, readonly: false, editable: false, sortable: true, header: {text : "시스템"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SUBNAME'          , width: 88, visible: true, readonly: false, editable: false, sortable: true, header: {text : "중분류"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMNAME'          , width: 117, visible: true, readonly: false, editable: false, sortable: true, header: {text : "프로그램명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TITLE'            , width: 194, visible: true, readonly: false, editable: false, sortable: true, header: {text : "제목"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQ_NAME'         , width: 49, visible: true, readonly: false, editable: false, sortable: true, header: {text : "요청자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQ_DATE'         , width: 63, visible: true, readonly: false, editable: false, sortable: true, header: {text : "요청일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REG_NAME'         , width: 51, visible: true, readonly: false, editable: false, sortable: true, header: {text : "등록자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REG_DATE'         , width: 62, visible: true, readonly: false, editable: false, sortable: true, header: {text : "등록일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'WEIGHT_DIV'       , width: 55, visible: true, readonly: false, editable: false, sortable: true, header: {text : "중요도"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_TAG'          , width: 61, visible: true, readonly: false, editable: false, sortable: true, header: {text : "진행구분"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_NAME'         , width: 72, visible: true, readonly: false, editable: false, sortable: true, header: {text : "개발자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_TERM'         , width: 60, visible: true, readonly: false, editable: false, sortable: true, header: {text : "개발소요일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_ENDPLANDATE'  , width: 70, visible: false, readonly: false, editable: false, sortable: true, header: {text : ""개발완료
 예정일""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_ENDDATE'      , width: 67, visible: true, readonly: false, editable: false, sortable: true, header: {text : "개발완료일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_ENDYN'        , width: 33, visible: true, readonly: false, editable: false, sortable: true, header: {text : ""개발
완료""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_ENDCHKDATE'   , width: 113, visible: false, readonly: false, editable: false, sortable: true, header: {text : "개발완료체크일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_NAME'         , width: 51, visible: false, readonly: false, editable: false, sortable: true, header: {text : "" 개발
점검자""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_DATE'         , width: 68, visible: false, readonly: false, editable: false, sortable: true, header: {text : "" 개발
점검일""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_ENDYN'        , width: 33, visible: false, readonly: false, editable: false, sortable: true, header: {text : ""점검
완료""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_ENDCHKDATE'   , width: 108, visible: false, readonly: false, editable: false, sortable: true, header: {text : "개발점검완료체크일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'        , width: 54, visible: true, readonly: false, editable: false, sortable: true, header: {text : ""전산팀
점검자""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_DATE'        , width: 76, visible: true, readonly: false, editable: false, sortable: true, header: {text : ""전산팀
점검일""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_ENDYN'       , width: 35, visible: true, readonly: false, editable: false, sortable: true, header: {text : ""점검
완료""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_ENDCHK_DATE' , width: 115, visible: false, readonly: false, editable: false, sortable: true, header: {text : "전산팀점검체크일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_ID'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHARGE_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_NAME'      , width: 58, visible: false, readonly: false, editable: false, sortable: true, header: {text : "" 현업
담당자""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_DATE'      , width: 71, visible: false, readonly: false, editable: false, sortable: true, header: {text : "현업점검일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_ENDYN'     , width: 35, visible: false, readonly: false, editable: false, sortable: true, header: {text : ""점검
완료""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_ENDCHKDATE'     , width: 126, visible: false, readonly: false, editable: false, sortable: true, header: {text : "현업점검완료체크일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_ID'           , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_SORT'         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_SORT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SUB_SORT'         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SUB_SORT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGM_SORT'         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PGM_SORT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SUBCODE'          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SUBCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMCODE'          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PGMCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEQ'              , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_REVIEW'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DEPT_REVIEW"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHARGE_REVIEW'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHARGE_REVIEW"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DIVISION_TAG'     , width: 52, visible: true, readonly: false, editable: false, sortable: true, header: {text : "대상구분"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_dev_schedule_list = [
	{fieldName : 'SYS_NAME' , width: 49, visible: true, readonly: false, editable: false, sortable: true, header: {text : "시스템"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SUB_NAME' , width: 68, visible: true, readonly: false, editable: false, sortable: true, header: {text : "중분류"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMNAME'  , width: 135, visible: true, readonly: false, editable: false, sortable: true, header: {text : "프로그램명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_ID'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SUBCODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SUBCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMCODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PGMCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TOTAL_COU'  , width: 33, visible: true, readonly: false, editable: false, sortable: true, header: {text : ""등록
건수""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_COU'  , width: 35, visible: true, readonly: false, editable: false, sortable: true, header: {text : ""개발
완료""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_COU'  , width: 37, visible: true, readonly: false, editable: false, sortable: true, header: {text : ""점검
완료""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_COU' , width: 34, visible: true, readonly: false, editable: false, sortable: true, header: {text : ""전산
완료""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHA_COU'  , width: 34, visible: true, readonly: false, editable: false, sortable: true, header: {text : ""현업
완료""}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_dev_code_lv3 = [
	{fieldName : 'SYS_ID'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SUBCODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SUBCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMCODE'   , width: 77, visible: true, readonly: false, editable: false, sortable: true, header: {text : "코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMNAME'   , width: 168, visible: true, readonly: false, editable: false, sortable: true, header: {text : "프로그램명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SORT_ORDER'   , width: 51, visible: true, readonly: false, editable: false, sortable: true, header: {text : "정렬순서"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMLEVEL'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PGMLEVEL"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_dev_code_lv2 = [
	{fieldName : 'SYS_ID'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SUBCODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SUBCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMCODE'   , width: 58, visible: true, readonly: false, editable: false, sortable: true, header: {text : "코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMNAME'   , width: 144, visible: true, readonly: false, editable: false, sortable: true, header: {text : "프로그램명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SORT_ORDER'   , width: 56, visible: true, readonly: false, editable: false, sortable: true, header: {text : "정렬순서"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMLEVEL'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PGMLEVEL"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_dev_code_lv1 = [
	{fieldName : 'SYS_ID'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SUBCODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SUBCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMCODE'   , width: 46, visible: true, readonly: false, editable: false, sortable: true, header: {text : "코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMNAME'   , width: 87, visible: true, readonly: false, editable: false, sortable: true, header: {text : "프로그램명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SORT_ORDER'   , width: 55, visible: true, readonly: false, editable: false, sortable: true, header: {text : "정렬순서"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMLEVEL'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PGMLEVEL"}, styles: _Styles_textc, 		editor: _Editor_text } 
];


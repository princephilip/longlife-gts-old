var detail_test = [
	{fieldName : 'OBJNAME'    , width: 126, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Objname:"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FCATEGORY'  , width: 186, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Fcategory:"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FNAME'      , width: 186, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Fname:"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FDECRIPTION'      , width: 500, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Fdecription:"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FSYNTAX'    , width: 500, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Fsyntax:"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FPARAMETERS'    , width: 1080, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Fparameters:"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FRETURN'    , width: 606, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Freturn:"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FEXAMPLES'  , width: 1080, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Fexamples:"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'VERSION'    , width: 306, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Version:"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var list_function = [
	{fieldName : 'OBJNAME'    , width: 111, visible: true, readonly: false, editable: false, sortable: true, header: {text : "오브젝트"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FCATEGORY'  , width: 117, visible: true, readonly: false, editable: false, sortable: true, header: {text : "카테고리"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FNAME'      , width: 118, visible: true, readonly: false, editable: false, sortable: true, header: {text : "함수명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FDECRIPTION'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "FDECRIPTION"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FSYNTAX'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "FSYNTAX"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FPARAMETERS'    , width: 136, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Argument"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FRETURN'    , width: 163, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Return"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FEXAMPLES'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "FEXAMPLES"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'VERSION'    , width: 54, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Version"}, styles: _Styles_textc, 		editor: _Editor_text } 
];


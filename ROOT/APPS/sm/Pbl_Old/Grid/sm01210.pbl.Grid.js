var d_sm01210_list = [
	{fieldName : 'HCODE'     , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "주코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HNAME'     , width: 187, visible: true, readonly: false, editable: false, sortable: true, header: {text : "주코드명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DCODE_SIZE'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DCODE_SIZE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HNAME1'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "HNAME1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DATA_TYPE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DATA_TYPE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CODE_SIZE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CODE_SIZE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NAME_SIZE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "NAME_SIZE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FLOAT_POS' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "FLOAT_POS"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TABLE_NAME' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "TABLE_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CODE_TYPE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CODE_TYPE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RMK'       , width: 676, visible: true, readonly: false, editable: false, sortable: true, header: {text : "비        고"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var d_sm01210_1 = [
	{fieldName : 'HCODE'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "HCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DCODE'       , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DNAME'       , width: 139, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부코드명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DNAME1'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DNAME1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DNAME2'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DNAME2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DVALUE'      , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "자료값(숫자)"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HDCODE'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "HDCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RELATE_CODE1'      , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "관련코드1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RELATE_CODE2'      , width: 74, visible: true, readonly: false, editable: false, sortable: true, header: {text : "관련코드2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RELATE_CODE3'      , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "관련코드3"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RELATE_CODE4'      , width: 74, visible: true, readonly: false, editable: false, sortable: true, header: {text : "관련코드4"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RELATE_CODE5'      , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "관련코드5"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FIX_CODE1'   , width: 74, visible: true, readonly: false, editable: false, sortable: true, header: {text : "고정코드1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FIX_CODE2'   , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "고정코드2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FIX_CODE3'   , width: 74, visible: true, readonly: false, editable: false, sortable: true, header: {text : "고정코드3"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FIX_CODE4'   , width: 74, visible: true, readonly: false, editable: false, sortable: true, header: {text : "고정코드4"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FIX_CODE5'   , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "고정코드5"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYSTEM_CODE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYSTEM_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CODE_TYPE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CODE_TYPE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DVALUE2'     , width: 102, visible: true, readonly: false, editable: false, sortable: true, header: {text : "자료값(문자)"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USE_YN'      , width: 129, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사용여부"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CODE_GU'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CODE_GU"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RMK'         , width: 243, visible: true, readonly: false, editable: false, sortable: true, header: {text : "비고"}, styles: _Styles_textc, 		editor: _Editor_text } 
];


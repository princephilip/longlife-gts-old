var de_sm_cyberaudit = [
	{fieldName : 'AUDITNO'          , width: 36, visible: true, readonly: false, editable: false, sortable: true, header: {text : "No."}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TITLE'            , width: 243, visible: true, readonly: false, editable: false, sortable: true, header: {text : "제    목"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONTENTS'         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CONTENTS"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NAME'             , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'JUMINNO1'         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "JUMINNO1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'JUMINNO2'         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "JUMINNO2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TELNO'            , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "TELNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMAIL'            , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EMAIL"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FILENAME'         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "FILENAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FILEURL'          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "FILEURL"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHECKDATE'        , width: 120, visible: true, readonly: false, editable: false, sortable: true, header: {text : "확인일자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REPLYDATE'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REPLYDATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'MANAGEMENT'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "MANAGEMENT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ROW_INPUT_DATE'   , width: 119, visible: true, readonly: false, editable: false, sortable: true, header: {text : "등록일시"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ROW_INPUT_EMP_NO' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ROW_INPUT_EMP_NO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ROW_INPUT_IP'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ROW_INPUT_IP"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ROW_UPDATE_DATE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ROW_UPDATE_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ROW_UPDATE_EMP_NO'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ROW_UPDATE_EMP_NO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ROW_UPDATE_IP'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ROW_UPDATE_IP"}, styles: _Styles_textc, 		editor: _Editor_text } 
];


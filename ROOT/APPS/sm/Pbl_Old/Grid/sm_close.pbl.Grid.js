var dl_sm_close_remark = [
	{fieldName : 'REMARK', width: 338, visible: true, readonly: false, editable: false, sortable: true, header: {text : "마감 취소 사유"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dl_sm_close_hist = [
	{fieldName : 'SM_CLOSE_YYMM'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_CLOSE_YYMM"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CLOSE_CODE'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CLOSE_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DML_DATE'         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DML_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ITEM_NAME'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ITEM_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CLOSE_YN'         , width: 54, visible: true, readonly: false, editable: false, sortable: true, header: {text : "마감여부"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARK'           , width: 339, visible: true, readonly: false, editable: false, sortable: true, header: {text : "마감취소 사유"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ROW_INPUT_DATE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ROW_INPUT_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ROW_INPUT_EMP_NO' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ROW_INPUT_EMP_NO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INPUT_EMP_NAME'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "INPUT_EMP_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ROW_INPUT_IP'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ROW_INPUT_IP"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ROW_UPDATE_DATE'  , width: 111, visible: true, readonly: false, editable: false, sortable: true, header: {text : "이력일시"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ROW_UPDATE_EMP_NO'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ROW_UPDATE_EMP_NO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'UPDATE_EMP_NAME'  , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "작업자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ROW_UPDATE_IP'    , width: 92, visible: true, readonly: false, editable: false, sortable: true, header: {text : "IP"}, styles: _Styles_textc, 		editor: _Editor_text } 
];


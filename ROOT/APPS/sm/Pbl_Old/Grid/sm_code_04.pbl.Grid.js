var dl_dictionary_2 = [
	{fieldName : 'WORD'   , width: 134, visible: true, readonly: false, editable: false, sortable: true, header: {text : "용어"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ENGLISH'   , width: 265, visible: true, readonly: false, editable: false, sortable: true, header: {text : "영문"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SHORT'  , width: 120, visible: true, readonly: false, editable: false, sortable: true, header: {text : "약어"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARK' , width: 155, visible: true, readonly: false, editable: false, sortable: true, header: {text : "비고"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_code_cust = [
	{fieldName : 'CUST_CODE'   , width: 96, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사업자번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CUST_TAG'    , width: 73, visible: false, readonly: false, editable: false, sortable: true, header: {text : "거래처구분"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CUST_NAME'   , width: 217, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사업자명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'OWNER'       , width: 57, visible: true, readonly: false, editable: false, sortable: true, header: {text : "대표자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CONDITION'   , width: 217, visible: false, readonly: false, editable: false, sortable: true, header: {text : "업태"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CATEGORY'    , width: 217, visible: false, readonly: false, editable: false, sortable: true, header: {text : "업종"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TEL'         , width: 147, visible: false, readonly: false, editable: false, sortable: true, header: {text : "전화번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TEL2'        , width: 147, visible: false, readonly: false, editable: false, sortable: true, header: {text : "전화번호2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FAX'         , width: 147, visible: false, readonly: false, editable: false, sortable: true, header: {text : "팩스번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP'         , width: 66, visible: false, readonly: false, editable: false, sortable: true, header: {text : "우편번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR'        , width: 427, visible: false, readonly: false, editable: false, sortable: true, header: {text : "주소1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR2'       , width: 427, visible: false, readonly: false, editable: false, sortable: true, header: {text : "주소2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REGISTER_NO' , width: 120, visible: false, readonly: false, editable: false, sortable: true, header: {text : "주민(법인)등록번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'STATUS_CODE' , width: 101, visible: false, readonly: false, editable: false, sortable: true, header: {text : "사업자상태"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'OPEN_DATE'   , width: 69, visible: false, readonly: false, editable: false, sortable: true, header: {text : "개업일자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CLOSING_DATE'   , width: 83, visible: false, readonly: false, editable: false, sortable: true, header: {text : "폐업일자"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_table_list = [
	{fieldName : 'DBA_TABLES_OWNER'           , width: 56, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DB"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DBA_TABLES_TABLE_NAME'      , width: 160, visible: true, readonly: false, editable: false, sortable: true, header: {text : "테이블"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DBA_TAB_COMMENTS_COMMENTS'  , width: 175, visible: true, readonly: false, editable: false, sortable: true, header: {text : "테이블명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DBA_TAB_COMMENTS_TABLE_TYPE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DBA_TAB_COMMENTS_TABLE_TYPE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DBA_TABLES_TABLESPACE_NAME' , width: 175, visible: false, readonly: false, editable: false, sortable: true, header: {text : "Dba Tables Tablespace Name"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_projectcode_update = [
	{fieldName : 'ROWCHECK'       , width: 13, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ROWCHECK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPANY_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROJ_CODE'      , width: 45, visible: true, readonly: false, editable: false, sortable: true, header: {text : "차수사업"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROJ_NAME'      , width: 236, visible: true, readonly: false, editable: false, sortable: true, header: {text : "차수사업명칭"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROJ_SHORT_NAME'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PROJ_SHORT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROJ_DIV'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PROJ_DIV"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BUSINESS_CODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "BUSINESS_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BUSINESS_NAME'  , width: 187, visible: true, readonly: false, editable: false, sortable: true, header: {text : "총괄사업명칭"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COST_PROJ_CODE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COST_PROJ_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COST_PROJ_NAME' , width: 143, visible: true, readonly: false, editable: false, sortable: true, header: {text : "원가사업명칭"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PM_DEPT_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PM_DEPT_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'      , width: 78, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USE_YN'         , width: 75, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사용여부"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TERMINATE_DATE' , width: 66, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사용종료일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INPUT_DUTY_ID'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "INPUT_DUTY_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INPUT_DATE'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "INPUT_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHG_DUTY_ID'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHG_DUTY_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHG_DATE'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHG_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPLETION_DATE'       , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "준공일"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_businesscode_update = [
	{fieldName : 'ROWCHECK'           , width: 14, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ROWCHECK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPANY_CODE'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BUSINESS_CODE'      , width: 78, visible: true, readonly: false, editable: false, sortable: true, header: {text : "총괄사업코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BUSINESS_NAME'      , width: 231, visible: true, readonly: false, editable: false, sortable: true, header: {text : "총괄사업명칭"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BUSINESS_SHORT_NAME'      , width: 118, visible: true, readonly: false, editable: false, sortable: true, header: {text : "총괄사업약칭"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PM_DEPT_CODE'       , width: 86, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PM부서코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'          , width: 159, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PM부서명칭"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USE_YN'             , width: 63, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사용여부"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TERMINATE_DATE'     , width: 80, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사용종료일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INPUT_DUTY_ID'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "INPUT_DUTY_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INPUT_DATE'         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "INPUT_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHG_DUTY_ID'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHG_DUTY_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHG_DATE'           , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHG_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPLETION_DATE'    , width: 82, visible: true, readonly: false, editable: false, sortable: true, header: {text : "준공일"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_d_company = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPANY_NAME', width: 155, visible: true, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_c_businesscode_update = [
	{fieldName : 'DEPT_KIND'   , width: 162, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_KIND"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPANY_CODE'   , width: 160, visible: true, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'F_DATE'      , width: 79, visible: true, readonly: false, editable: false, sortable: true, header: {text : "F_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'T_DATE'      , width: 85, visible: true, readonly: false, editable: false, sortable: true, header: {text : "T_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHANGE_TAG'  , width: 108, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHANGE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text } 
];


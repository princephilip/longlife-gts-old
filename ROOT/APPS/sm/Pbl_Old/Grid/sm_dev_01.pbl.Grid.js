var dr_dev_request_mst = [
	{fieldName : 'SM_DEV_REQUEST_MST_SEQ'            , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_COMPANY_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_SYS_CODE'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_SYS_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_DATE'                      , width: 60, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_TITLE'                     , width: 240, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_TITLE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_REQUEST_EMPNO'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_REQUEST_EMPNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_REQUEST_DEPT'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_REQUEST_DEPT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_DATE'                      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PROCESS_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_PROCESS_EMPNO'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_PROCESS_EMPNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TRANS_DATE'                        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "TRANS_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_COMPLETE_TAG'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_COMPLETE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_REQUEST_CONTENT'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_REQUEST_CONTENT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_PROCESS_CONTENT'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_PROCESS_CONTENT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_EMPNAME'                   , width: 42, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_EMPNAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_DEPTNAME'                  , width: 110, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_EMPNAME'                   , width: 36, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PROCESS_EMPNAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_NAME'                          , width: 48, visible: true, readonly: false, editable: false, sortable: true, header: {text : "업무구분"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_TYPE'                          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_TYPE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TYPE_NAME'                         , width: 25, visible: true, readonly: false, editable: false, sortable: true, header: {text : "형태"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BET_DATE'                          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "소요"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_CHOICE'                        , width: 24, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SYS_CHOICE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FROM_DATE'                         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "FROM_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TO_DATE'                           , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "TO_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROC_TAG'                          , width: 52, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PROC_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'IT_DATE'                           , width: 60, visible: true, readonly: false, editable: false, sortable: true, header: {text : "IT_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQ_END_DATE'                      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REQ_END_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_YOUL'                      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PROCESS_YOUL"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_FDATE'                     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PROCESS_FDATE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dl_dev_request_mst_ver1 = [
	{fieldName : 'SEQ'             , width: 16, visible: false, readonly: false, editable: false, sortable: true, header: {text : "Seq"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPANY_CODE'    , width: 15, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_CODE'        , width: 14, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_DATE'    , width: 66, visible: true, readonly: false, editable: false, sortable: true, header: {text : "요청일자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_TITLE'   , width: 243, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_TITLE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_EMPNO'   , width: 12, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_EMPNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_DEPT'    , width: 11, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_DEPT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_DATE'    , width: 65, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PROCESS_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_EMPNO'   , width: 16, visible: false, readonly: false, editable: false, sortable: true, header: {text : "처리자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TRANS_DATE'      , width: 74, visible: true, readonly: false, editable: false, sortable: true, header: {text : "이관요청일자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPLETE_TAG'    , width: 32, visible: true, readonly: false, editable: false, sortable: true, header: {text : "COMPLETE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_CONTENT' , width: 15, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_CONTENT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_CONTENT' , width: 22, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PROCESS_CONTENT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_EMPNAME' , width: 51, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_EMPNAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_DEPTNAME' , width: 147, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_DEPTNAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_EMPNAME' , width: 51, visible: true, readonly: false, editable: false, sortable: true, header: {text : "담당자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_NAME'        , width: 62, visible: true, readonly: false, editable: false, sortable: true, header: {text : "업무구분"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_TYPE'        , width: 38, visible: true, readonly: false, editable: false, sortable: true, header: {text : "형태"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TYPE_NAME'       , width: 38, visible: false, readonly: false, editable: false, sortable: true, header: {text : "형태"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BET_DATE'        , width: 41, visible: true, readonly: false, editable: false, sortable: true, header: {text : "소요일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_CHOICE'      , width: 31, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SYS_CHOICE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_FDATE'   , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "개발시작일자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_TDATE'   , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "개발종료일자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_RATE'    , width: 47, visible: true, readonly: false, editable: false, sortable: true, header: {text : "진행율"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TRANS_OKDATE'    , width: 74, visible: true, readonly: false, editable: false, sortable: true, header: {text : "이관완료일자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NAPPRONO'        , width: 68, visible: true, readonly: false, editable: false, sortable: true, header: {text : "결제번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TRANS_NAPPRONO'  , width: 68, visible: true, readonly: false, editable: false, sortable: true, header: {text : "이관번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEVPGM_CNT'      , width: 51, visible: true, readonly: false, editable: false, sortable: true, header: {text : "개발본수"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'IT_DATE'         , width: 82, visible: true, readonly: false, editable: false, sortable: true, header: {text : "요청예정일자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQ_END_DATE'    , width: 83, visible: true, readonly: false, editable: false, sortable: true, header: {text : "확정예정일자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NFILEID'         , width: 88, visible: true, readonly: false, editable: false, sortable: true, header: {text : "이관파일번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHG_YN'          , width: 67, visible: true, readonly: false, editable: false, sortable: true, header: {text : "js변경여부"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dr_dev_request_mst_dongjin = [
	{fieldName : 'SM_DEV_REQUEST_MST_SEQ'            , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_COMPANY_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_SYS_CODE'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_SYS_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_DATE'                      , width: 61, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_TITLE'                     , width: 240, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_TITLE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_REQUEST_EMPNO'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_REQUEST_EMPNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_REQUEST_DEPT'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_REQUEST_DEPT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_PROCESS_DATE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_PROCESS_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_PROCESS_EMPNO'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_PROCESS_EMPNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TRANS_DATE'                        , width: 65, visible: true, readonly: false, editable: false, sortable: true, header: {text : "TRANS_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_COMPLETE_TAG'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_COMPLETE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_REQUEST_CONTENT'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_REQUEST_CONTENT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_PROCESS_CONTENT'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_PROCESS_CONTENT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_EMPNAME'                   , width: 42, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_EMPNAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_DEPTNAME'                  , width: 98, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_EMPNAME'                   , width: 40, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PROCESS_EMPNAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_NAME'                          , width: 48, visible: true, readonly: false, editable: false, sortable: true, header: {text : "업무구분"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_TYPE'                          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_TYPE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TYPE_NAME'                         , width: 25, visible: true, readonly: false, editable: false, sortable: true, header: {text : "형태"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BET_DATE'                          , width: 36, visible: true, readonly: false, editable: false, sortable: true, header: {text : "소요일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_CHOICE'                        , width: 24, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SYS_CHOICE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FROM_DATE'                         , width: 70, visible: true, readonly: false, editable: false, sortable: true, header: {text : "FROM_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TO_DATE'                           , width: 70, visible: true, readonly: false, editable: false, sortable: true, header: {text : "TO_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROC_TAG'                          , width: 32, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PROC_TAG"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dr_dev_request_mst_bak = [
	{fieldName : 'SM_DEV_REQUEST_MST_SEQ'            , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_COMPANY_CODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_SYS_CODE'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_SYS_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_DATE'                      , width: 61, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_TITLE'                     , width: 240, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_TITLE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_REQUEST_EMPNO'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_REQUEST_EMPNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_REQUEST_DEPT'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_REQUEST_DEPT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_PROCESS_DATE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_PROCESS_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_PROCESS_EMPNO'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_PROCESS_EMPNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TRANS_DATE'                        , width: 65, visible: true, readonly: false, editable: false, sortable: true, header: {text : "TRANS_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_COMPLETE_TAG'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_COMPLETE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_REQUEST_CONTENT'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_REQUEST_CONTENT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_DEV_REQUEST_MST_PROCESS_CONTENT'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_DEV_REQUEST_MST_PROCESS_CONTENT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_EMPNAME'                   , width: 42, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_EMPNAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_DEPTNAME'                  , width: 125, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_EMPNAME'                   , width: 40, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PROCESS_EMPNAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_NAME'                          , width: 48, visible: true, readonly: false, editable: false, sortable: true, header: {text : "업무구분"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_TYPE'                          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_TYPE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TYPE_NAME'                         , width: 25, visible: true, readonly: false, editable: false, sortable: true, header: {text : "형태"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BET_DATE'                          , width: 53, visible: true, readonly: false, editable: false, sortable: true, header: {text : "소요기간"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_CHOICE'                        , width: 24, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SYS_CHOICE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FROM_DATE'                         , width: 70, visible: true, readonly: false, editable: false, sortable: true, header: {text : "FROM_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TO_DATE'                           , width: 70, visible: true, readonly: false, editable: false, sortable: true, header: {text : "TO_DATE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_dev_emp = [
	{fieldName : 'COMPANY_CODE'  , width: 112, visible: true, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NO'        , width: 66, visible: true, readonly: false, editable: false, sortable: true, header: {text : "EMP_NO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NAME'      , width: 78, visible: true, readonly: false, editable: false, sortable: true, header: {text : "EMP_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'     , width: 270, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL_NAME'    , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "LEVEL_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REAL_DEPT_CODE'    , width: 29, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REAL_DEPT_CODE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dl_dev_request_mst = [
	{fieldName : 'SEQ'             , width: 73, visible: false, readonly: false, editable: false, sortable: true, header: {text : "Seq"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPANY_CODE'    , width: 69, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_CODE'        , width: 64, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_DATE'    , width: 343, visible: true, readonly: false, editable: false, sortable: true, header: {text : "요청일자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_TITLE'   , width: 1513, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_TITLE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_EMPNO'   , width: 55, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_EMPNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_DEPT'    , width: 50, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_DEPT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_DATE'    , width: 361, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PROCESS_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_EMPNO'   , width: 73, visible: false, readonly: false, editable: false, sortable: true, header: {text : "처리자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TRANS_DATE'      , width: 411, visible: true, readonly: false, editable: false, sortable: true, header: {text : "TRANS_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPLETE_TAG'    , width: 293, visible: true, readonly: false, editable: false, sortable: true, header: {text : "COMPLETE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_CONTENT' , width: 69, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_CONTENT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_CONTENT' , width: 101, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PROCESS_CONTENT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_EMPNAME' , width: 297, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_EMPNAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_DEPTNAME' , width: 1006, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_DEPTNAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_EMPNAME' , width: 347, visible: true, readonly: false, editable: false, sortable: true, header: {text : "처리자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_NAME'        , width: 366, visible: true, readonly: false, editable: false, sortable: true, header: {text : "업무구분"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_TYPE'        , width: 192, visible: true, readonly: false, editable: false, sortable: true, header: {text : "형태"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TYPE_NAME'       , width: 174, visible: false, readonly: false, editable: false, sortable: true, header: {text : "형태"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BET_DATE'        , width: 375, visible: true, readonly: false, editable: false, sortable: true, header: {text : "소요기간[일]"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_CHOICE'      , width: 201, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SYS_CHOICE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_dev_object_detail = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'OBJECT_OWNER', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "OBJECT_OWNER"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'OBJECT_TYPE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "OBJECT_TYPE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'OBJECT_NAME' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "OBJECT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHANGE_DATE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHANGE_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TITLE'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "TITLE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NO'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EMP_NO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NAME'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EMP_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARK'      , width: 3799, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARK2'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REMARK2"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_dev_object_master = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'OBJECT_OWNER', width: 89, visible: true, readonly: false, editable: false, sortable: true, header: {text : "계정명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'OBJECT_TYPE' , width: 108, visible: true, readonly: false, editable: false, sortable: true, header: {text : "오프젝트 타입"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'OBJECT_NAME' , width: 213, visible: true, readonly: false, editable: false, sortable: true, header: {text : "오브젝트명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHANGE_DATE' , width: 63, visible: true, readonly: false, editable: false, sortable: true, header: {text : "수정일자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TITLE'       , width: 267, visible: true, readonly: false, editable: false, sortable: true, header: {text : "변경내용"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NO'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EMP_NO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NAME'    , width: 62, visible: true, readonly: false, editable: false, sortable: true, header: {text : "작업자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REMARK'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REMARK"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHANGE_TIME' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHANGE_TIME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_dev_object_list = [
	{fieldName : 'OWNER'      , width: 90, visible: false, readonly: false, editable: false, sortable: true, header: {text : "계정명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'OBJECT_TYPE'      , width: 90, visible: true, readonly: false, editable: false, sortable: true, header: {text : "항목유형"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'OBJECT_NAME'      , width: 258, visible: true, readonly: false, editable: false, sortable: true, header: {text : "항목이름"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CNT'        , width: 48, visible: true, readonly: false, editable: false, sortable: true, header: {text : "작업수"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dl_issue = [
	{fieldName : 'DOC_SEQ'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DOC_SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SUBJECT'     , width: 270, visible: true, readonly: false, editable: false, sortable: true, header: {text : "제목"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'MAIN_CONTENT'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "MAIN_CONTENT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'READ_CNT'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "READ_CNT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQ_NAME'    , width: 53, visible: true, readonly: false, editable: false, sortable: true, header: {text : "요청자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_NAME'    , width: 53, visible: true, readonly: false, editable: false, sortable: true, header: {text : "개발자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REG_DATE'    , width: 63, visible: true, readonly: false, editable: false, sortable: true, header: {text : "등록일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAN_DATE'   , width: 67, visible: true, readonly: false, editable: false, sortable: true, header: {text : "완료예정일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'END_DATE'    , width: 61, visible: true, readonly: false, editable: false, sortable: true, header: {text : "완료일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'END_TAG'     , width: 31, visible: true, readonly: false, editable: false, sortable: true, header: {text : "완료"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'WORK_ORDER'  , width: 61, visible: true, readonly: false, editable: false, sortable: true, header: {text : "작업순서"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REG_TIME'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REG_TIME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLAN_TIME'   , width: 51, visible: true, readonly: false, editable: false, sortable: true, header: {text : "예정시간"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'END_TIME'    , width: 52, visible: true, readonly: false, editable: false, sortable: true, header: {text : "완료시간"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEV_END_TAG' , width: 49, visible: false, readonly: false, editable: false, sortable: true, header: {text : "개발완료"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dl_process_time = [
	{fieldName : 'COMPCODE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ACTIONTIME' , width: 127, visible: true, readonly: false, editable: false, sortable: true, header: {text : "실행시간"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMPNO'      , width: 66, visible: true, readonly: false, editable: false, sortable: true, header: {text : "실행자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYSCODE'    , width: 65, visible: true, readonly: false, editable: false, sortable: true, header: {text : "단위업무"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DWOBJECT'   , width: 427, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DW Object"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ARGUMENTS'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ARGUMENTS"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMCODE'    , width: 93, visible: true, readonly: false, editable: false, sortable: true, header: {text : "프로그램코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'IP'         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "IP"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESSTIME'         , width: 85, visible: true, readonly: false, editable: false, sortable: true, header: {text : "실행시간(ms)"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PACKET_SIZE'         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PACKET_SIZE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMNAME'    , width: 192, visible: true, readonly: false, editable: false, sortable: true, header: {text : "프로그램명"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var image_search_temp-grid2 = [
	{fieldName : 'GUBUN'    , width: 45, visible: true, readonly: false, editable: false, sortable: true, header: {text : "구분"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEQ'      , width: 44, visible: true, readonly: false, editable: false, sortable: true, header: {text : "시퀀스"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LENGTH'   , width: 84, visible: true, readonly: false, editable: false, sortable: true, header: {text : "길이"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NAME'     , width: 107, visible: true, readonly: false, editable: false, sortable: true, header: {text : "한글명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'IMG_NAME' , width: 114, visible: true, readonly: false, editable: false, sortable: true, header: {text : "영문명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLACE'    , width: 181, visible: true, readonly: false, editable: false, sortable: true, header: {text : "경로"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BIGO'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "BIGO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EXTENSION'     , width: 68, visible: true, readonly: false, editable: false, sortable: true, header: {text : "확장자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'WIDTH'    , width: 51, visible: true, readonly: false, editable: false, sortable: true, header: {text : "가로"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HEIGHT'   , width: 52, visible: true, readonly: false, editable: false, sortable: true, header: {text : "세로"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var image_search_temp-grid = [
	{fieldName : 'GUBUN'    , width: 45, visible: true, readonly: false, editable: false, sortable: true, header: {text : "구분"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEQ'      , width: 44, visible: true, readonly: false, editable: false, sortable: true, header: {text : "시퀀스"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LENGTH'   , width: 84, visible: true, readonly: false, editable: false, sortable: true, header: {text : "길이"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NAME'     , width: 107, visible: true, readonly: false, editable: false, sortable: true, header: {text : "한글명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'IMG_NAME' , width: 114, visible: true, readonly: false, editable: false, sortable: true, header: {text : "영문명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLACE'    , width: 181, visible: true, readonly: false, editable: false, sortable: true, header: {text : "경로"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BIGO'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "BIGO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EXTENSION'     , width: 68, visible: true, readonly: false, editable: false, sortable: true, header: {text : "확장자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'WIDTH'    , width: 51, visible: true, readonly: false, editable: false, sortable: true, header: {text : "가로"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HEIGHT'   , width: 52, visible: true, readonly: false, editable: false, sortable: true, header: {text : "세로"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var image_search3 = [
	{fieldName : 'SEQ'      , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GUBUN'    , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "GUBUN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LENGTH'   , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "LENGTH"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NAME'     , width: 103, visible: true, readonly: false, editable: false, sortable: true, header: {text : "NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'IMG_NAME' , width: 119, visible: true, readonly: false, editable: false, sortable: true, header: {text : "IMG_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLACE'    , width: 151, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PLACE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BIGO'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "BIGO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EXTENSION'     , width: 64, visible: true, readonly: false, editable: false, sortable: true, header: {text : "EXTENSION"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'WIDTH'    , width: 69, visible: true, readonly: false, editable: false, sortable: true, header: {text : "WIDTH"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HEIGHT'   , width: 67, visible: true, readonly: false, editable: false, sortable: true, header: {text : "HEIGHT"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var image_search_temp = [
	{fieldName : 'SEQ'      , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Seq"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GUBUN'    , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Gubun"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LENGTH'   , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Length"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NAME'     , width: 103, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Name"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'IMG_NAME' , width: 131, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Img Name"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLACE'    , width: 192, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Place"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BIGO'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "BIGO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EXTENSION'     , width: 64, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Extension"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'WIDTH'    , width: 69, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Width"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HEIGHT'   , width: 67, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Height"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var image_search2 = [
	{fieldName : 'SEQ'      , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GUBUN'    , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "GUBUN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LENGTH'   , width: 73, visible: true, readonly: false, editable: false, sortable: true, header: {text : "LENGTH"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NAME'     , width: 103, visible: true, readonly: false, editable: false, sortable: true, header: {text : "NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'IMG_NAME' , width: 119, visible: true, readonly: false, editable: false, sortable: true, header: {text : "IMG_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLACE'    , width: 151, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PLACE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BIGO'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "BIGO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EXTENSION'     , width: 64, visible: true, readonly: false, editable: false, sortable: true, header: {text : "EXTENSION"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'WIDTH'    , width: 69, visible: true, readonly: false, editable: false, sortable: true, header: {text : "WIDTH"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HEIGHT'   , width: 67, visible: true, readonly: false, editable: false, sortable: true, header: {text : "HEIGHT"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var image_search = [
	{fieldName : 'FOLDER'    , width: 48, visible: true, readonly: false, editable: false, sortable: true, header: {text : "FOLDER"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NAME'      , width: 103, visible: true, readonly: false, editable: false, sortable: true, header: {text : "한글명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'IMG_NAME'  , width: 141, visible: true, readonly: false, editable: false, sortable: true, header: {text : "영문명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PLACE'     , width: 176, visible: true, readonly: false, editable: false, sortable: true, header: {text : "경로"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EXTENSION' , width: 68, visible: true, readonly: false, editable: false, sortable: true, header: {text : "확장자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'IMG_WIDTH' , width: 50, visible: true, readonly: false, editable: false, sortable: true, header: {text : "IMG_WIDTH"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'IMG_HEIGHT' , width: 52, visible: true, readonly: false, editable: false, sortable: true, header: {text : "IMG_HEIGHT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'URL'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "URL"}, styles: _Styles_textc, 		editor: _Editor_text } 
];


var sm_e_authuser = [
	{fieldName : 'COMPANY_CODE'                  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_ID'                       , width: 55, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사번"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_CODE'                     , width: 50, visible: true, readonly: false, editable: false, sortable: true, header: {text : "코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_NAME'                     , width: 61, visible: true, readonly: false, editable: false, sortable: true, header: {text : "성명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_DESC'                     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "USER_DESC"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LOGIN_ID'                      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "LOGIN_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_PASSWORD'                 , width: 92, visible: true, readonly: false, editable: false, sortable: true, header: {text : "비밀번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USE_YN'                        , width: 34, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사용"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ACCESS_GRADE'                  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ACCESS_GRADE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GROUP_CODE'                    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "GROUP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LOGIN_TIME'                    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "LOGIN_TIME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LOGOUT_TIME'                   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "LOGOUT_TIME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'IP_ADDR'                       , width: 97, visible: true, readonly: false, editable: false, sortable: true, header: {text : "최근접속주소"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INPUT_DUTY_ID'                 , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "INPUT_DUTY_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INPUT_DATE'                    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "INPUT_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHG_DUTY_ID'                   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHG_DUTY_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHG_DATE'                      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHG_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HR_COMPANY_CODE'               , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "HR_COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HR_DEPT_CODE'                  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "HR_DEPT_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AM_COMPANY_CODE'               , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "AM_COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AM_DEPT_CODE'                  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "AM_DEPT_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AM_DEPT_DIV'                   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "AM_DEPT_DIV"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DATA_GRADE'                    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DATA_GRADE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'MANAGER_YN'                    , width: 29, visible: true, readonly: false, editable: false, sortable: true, header: {text : "팀장"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_AUTH'                     , width: 78, visible: true, readonly: false, editable: false, sortable: true, header: {text : "권한등급"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_LEVEL'                    , width: 49, visible: true, readonly: false, editable: false, sortable: true, header: {text : "자료등급"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_AUTH_USER_MP_TAG'           , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_AUTH_USER_MP_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_AUTH_USER_TP_TAG'           , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_AUTH_USER_TP_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_AUTH_USER_PASSWORD_CHG_YN'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_AUTH_USER_PASSWORD_CHG_YN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_AUTH_USER_PASSWORD_CHG_DATE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_AUTH_USER_PASSWORD_CHG_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_AUTH_USER_PASSWORD_OLD1'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_AUTH_USER_PASSWORD_OLD1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_AUTH_USER_PASSWORD_OLD2'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_AUTH_USER_PASSWORD_OLD2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_AUTH_USER_PASSWORD_OLD3'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_AUTH_USER_PASSWORD_OLD3"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CUR_DEPT_CODE'                 , width: 47, visible: true, readonly: false, editable: false, sortable: true, header: {text : "코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RRN_PRE'                       , width: 74, visible: true, readonly: false, editable: false, sortable: true, header: {text : "주민번호(앞)"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RRN_POST'                      , width: 62, visible: true, readonly: false, editable: false, sortable: true, header: {text : "주민번호(뒤)"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GRADE_CODE'                    , width: 54, visible: true, readonly: false, editable: false, sortable: true, header: {text : "직위"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL_CODE'                    , width: 55, visible: true, readonly: false, editable: false, sortable: true, header: {text : "직급"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'OGR_DIV'                       , width: 50, visible: true, readonly: false, editable: false, sortable: true, header: {text : "GW관리"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'                     , width: 127, visible: true, readonly: false, editable: false, sortable: true, header: {text : "발령부서"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CUR_DEPT_NAME'                 , width: 172, visible: true, readonly: false, editable: false, sortable: true, header: {text : "근무부서명칭"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LOGIN_LOCK_CNT'                , width: 91, visible: true, readonly: false, editable: false, sortable: true, header: {text : "로그인 오류회수"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COM_DIV'                       , width: 64, visible: true, readonly: false, editable: false, sortable: true, header: {text : "COM_DIV"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ACCESS_IP'                     , width: 97, visible: true, readonly: false, editable: false, sortable: true, header: {text : "IP주소 접근제한"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USERINFO_ADMIN'                , width: 50, visible: true, readonly: false, editable: false, sortable: true, header: {text : "USERINFO_ADMIN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USERINFO_GET'                  , width: 68, visible: true, readonly: false, editable: false, sortable: true, header: {text : "USERINFO_GET"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var de_sm_itdetail = [
	{fieldName : 'COMPANY_CODE'   , width: 90, visible: true, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEQ'            , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_CODE'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_NAME'       , width: 106, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SYS_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_DATE'   , width: 95, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_TITLE'  , width: 471, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_TITLE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_EMPNO'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_EMPNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NAME'       , width: 46, visible: true, readonly: false, editable: false, sortable: true, header: {text : "EMP_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'      , width: 144, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_CONTENT'      , width: 471, visible: true, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_CONTENT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_DATE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PROCESS_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_EMPNO'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PROCESS_EMPNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NAME_1'     , width: 85, visible: true, readonly: false, editable: false, sortable: true, header: {text : "EMP_NAME_1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_CONTENT'     , width: 471, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PROCESS_CONTENT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_TYPE'       , width: 85, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SYS_TYPE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_CHOICE'     , width: 17, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SYS_CHOICE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_FDATE'  , width: 93, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PROCESS_FDATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_TDATE'  , width: 74, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PROCESS_TDATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_RATE'   , width: 25, visible: true, readonly: false, editable: false, sortable: true, header: {text : "PROCESS_RATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHK_DAY'        , width: 16, visible: true, readonly: false, editable: false, sortable: true, header: {text : "CHK_DAY"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dl_sm_authapp1 = [
	{fieldName : 'SORT_TAG'               , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SORT_TAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NAPPRONO'               , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "NAPPRONO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'APPROVAL_STRTITLE'      , width: 149, visible: true, readonly: false, editable: false, sortable: true, header: {text : "타이틀"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DATEDAY'                , width: 71, visible: true, readonly: false, editable: false, sortable: true, header: {text : "기안일자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'APPROVAL_STRPERSONNO'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "APPROVAL_STRPERSONNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HR_PERS_MASTER_EMP_NAME'   , width: 50, visible: true, readonly: false, editable: false, sortable: true, header: {text : "기안자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_CODE_DEPT_DEPT_NAME' , width: 125, visible: true, readonly: false, editable: false, sortable: true, header: {text : "기안부서"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_CNT'               , width: 41, visible: true, readonly: false, editable: false, sortable: true, header: {text : "처리건"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ISREPORTEND'            , width: 54, visible: true, readonly: false, editable: false, sortable: true, header: {text : "승인여부"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_CONTENT'           , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "AUTH_CONTENT"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dl_sm_ipprocess = [
	{fieldName : 'PROCESS_STAUS'  , width: 98, visible: true, readonly: false, editable: false, sortable: true, header: {text : "진행상태"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_CODE'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SYS_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_NAME'       , width: 56, visible: true, readonly: false, editable: false, sortable: true, header: {text : "업무단위"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_CHOICE'     , width: 54, visible: true, readonly: false, editable: false, sortable: true, header: {text : "LEVEL"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_TITLE'  , width: 280, visible: true, readonly: false, editable: false, sortable: true, header: {text : "요청제목"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP'            , width: 172, visible: true, readonly: false, editable: false, sortable: true, header: {text : "요청자정보"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_DATE'   , width: 68, visible: true, readonly: false, editable: false, sortable: true, header: {text : "요청일자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_NAME'   , width: 51, visible: true, readonly: false, editable: false, sortable: true, header: {text : "담당자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_FDATE'  , width: 76, visible: false, readonly: false, editable: false, sortable: true, header: {text : "개발시작일자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_TDATE'  , width: 81, visible: false, readonly: false, editable: false, sortable: true, header: {text : "개발종료일자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TRANS_DATE'     , width: 78, visible: true, readonly: false, editable: false, sortable: true, header: {text : "이관요청일자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TRANS_OK_DATE'  , width: 80, visible: true, readonly: false, editable: false, sortable: true, header: {text : "이관확정일자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPLETE_TAG'   , width: 56, visible: true, readonly: false, editable: false, sortable: true, header: {text : "완료여부"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NAPPRONO'       , width: 56, visible: true, readonly: false, editable: false, sortable: true, header: {text : "결재번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEQ'            , width: 34, visible: false, readonly: false, editable: false, sortable: true, header: {text : "Seq"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REQUEST_CONTENT'            , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "REQUEST_CONTENT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROCESS_CONTENT'            , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PROCESS_CONTENT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PROGRESS'       , width: 70, visible: false, readonly: false, editable: false, sortable: true, header: {text : "Progress"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TRANS_NAPPRONO' , width: 80, visible: true, readonly: false, editable: false, sortable: true, header: {text : "이관결재번호"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dl_sm_authapp_list = [
	{fieldName : 'SORT_TAG'               , width: 49, visible: false, readonly: false, editable: false, sortable: true, header: {text : "Sort Tag"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NAPPRONO'               , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "NAPPRONO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'APPROVAL_STRTITLE'      , width: 297, visible: true, readonly: false, editable: false, sortable: true, header: {text : "타이틀"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DATEDAY'                , width: 87, visible: true, readonly: false, editable: false, sortable: true, header: {text : "기안일자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'APPROVAL_STRPERSONNO'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "APPROVAL_STRPERSONNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HR_PERS_MASTER_EMP_NAME'   , width: 78, visible: true, readonly: false, editable: false, sortable: true, header: {text : "기안자"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_CODE_DEPT_DEPT_NAME' , width: 125, visible: true, readonly: false, editable: false, sortable: true, header: {text : "기안부서"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_CONTENT'           , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "AUTH_CONTENT"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_REASON'            , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "AUTH_REASON"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dl_vw_pers_master2 = [
	{fieldName : 'COMPANY_CODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NO'        , width: 43, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사번"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NAME'      , width: 68, visible: true, readonly: false, editable: false, sortable: true, header: {text : "이름"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RRN_PRE'       , width: 49, visible: true, readonly: false, editable: false, sortable: true, header: {text : "주민-앞"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RRN_POST'      , width: 52, visible: true, readonly: false, editable: false, sortable: true, header: {text : "주민-뒤"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_CODE'     , width: 66, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'     , width: 163, visible: true, readonly: false, editable: false, sortable: true, header: {text : "근무부서명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REAL_DEPT_CODE'     , width: 79, visible: true, readonly: false, editable: false, sortable: true, header: {text : "소속부서코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REAL_DEPT_NAME'     , width: 223, visible: true, readonly: false, editable: false, sortable: true, header: {text : "소속부서"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GRADE_CODE'    , width: 33, visible: true, readonly: false, editable: false, sortable: true, header: {text : "코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GRADE_NAME'    , width: 57, visible: true, readonly: false, editable: false, sortable: true, header: {text : "직위"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL_CODE'    , width: 33, visible: true, readonly: false, editable: false, sortable: true, header: {text : "코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL_NAME'    , width: 57, visible: true, readonly: false, editable: false, sortable: true, header: {text : "직급"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TITLE_CODE'    , width: 66, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Title Code"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TITLE_NAME'    , width: 246, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Title Name"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_CODE'      , width: 52, visible: true, readonly: false, editable: false, sortable: true, header: {text : "우편번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR1'         , width: 297, visible: true, readonly: false, editable: false, sortable: true, header: {text : "주소1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR2'         , width: 298, visible: true, readonly: false, editable: false, sortable: true, header: {text : "주소2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PHONE_PRE'     , width: 50, visible: true, readonly: false, editable: false, sortable: true, header: {text : "지역번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PHONE_POST'    , width: 76, visible: true, readonly: false, editable: false, sortable: true, header: {text : "전화번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CELL_PRE'      , width: 66, visible: true, readonly: false, editable: false, sortable: true, header: {text : "핸드폰국번"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CELL_POST'     , width: 126, visible: true, readonly: false, editable: false, sortable: true, header: {text : "핸드폰번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BIRTHDAY'      , width: 67, visible: true, readonly: false, editable: false, sortable: true, header: {text : "생일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LUNAR_SOLAR'   , width: 50, visible: true, readonly: false, editable: false, sortable: true, header: {text : "양/음력"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEX_DIV'       , width: 30, visible: true, readonly: false, editable: false, sortable: true, header: {text : "성별"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'JOIN_DATE'     , width: 65, visible: true, readonly: false, editable: false, sortable: true, header: {text : "입사일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RETIRE_DATE'   , width: 72, visible: true, readonly: false, editable: false, sortable: true, header: {text : "퇴사일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COM_DIV'       , width: 69, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DB출처"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dl_sm_auth_favorite_01 = [
	{fieldName : 'SYS_NAME'        , width: 57, visible: true, readonly: true, editable: false, sortable: true, header: {text : "시스템명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMCODE'         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PGMCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMNAME'         , width: 218, visible: true, readonly: true, editable: false, sortable: true, header: {text : "프로그램명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEVELOPEDTAG'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DEVELOPEDTAG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'MENULEVEL'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "MENULEVEL"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TAG_YN'          , width: 28, visible: false, readonly: false, editable: false, sortable: true, header: {text : "선택"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FAVORITE_PGMCODE'          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "FAVORITE_PGMCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPANY_CODE'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NO'          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EMP_NO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEQ'             , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SEQ"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dl_vw_pers_master = [
	{fieldName : 'COMPANY_CODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NO'        , width: 43, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사번"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NAME'      , width: 68, visible: true, readonly: false, editable: false, sortable: true, header: {text : "이름"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RRN_PRE'       , width: 49, visible: true, readonly: false, editable: false, sortable: true, header: {text : "주민-앞"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RRN_POST'      , width: 52, visible: true, readonly: false, editable: false, sortable: true, header: {text : "주민-뒤"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_CODE'     , width: 66, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'     , width: 163, visible: true, readonly: false, editable: false, sortable: true, header: {text : "근무부서명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REAL_DEPT_CODE'     , width: 79, visible: true, readonly: false, editable: false, sortable: true, header: {text : "소속부서코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'REAL_DEPT_NAME'     , width: 223, visible: true, readonly: false, editable: false, sortable: true, header: {text : "소속부서"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GRADE_CODE'    , width: 33, visible: true, readonly: false, editable: false, sortable: true, header: {text : "코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GRADE_NAME'    , width: 57, visible: true, readonly: false, editable: false, sortable: true, header: {text : "직위"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL_CODE'    , width: 33, visible: true, readonly: false, editable: false, sortable: true, header: {text : "코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL_NAME'    , width: 57, visible: true, readonly: false, editable: false, sortable: true, header: {text : "직급"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TITLE_CODE'    , width: 66, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Title Code"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TITLE_NAME'    , width: 246, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Title Name"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_CODE'      , width: 52, visible: true, readonly: false, editable: false, sortable: true, header: {text : "우편번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR1'         , width: 297, visible: true, readonly: false, editable: false, sortable: true, header: {text : "주소1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ADDR2'         , width: 298, visible: true, readonly: false, editable: false, sortable: true, header: {text : "주소2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PHONE_PRE'     , width: 50, visible: true, readonly: false, editable: false, sortable: true, header: {text : "지역번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PHONE_POST'    , width: 76, visible: true, readonly: false, editable: false, sortable: true, header: {text : "전화번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CELL_PRE'      , width: 66, visible: true, readonly: false, editable: false, sortable: true, header: {text : "핸드폰국번"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CELL_POST'     , width: 126, visible: true, readonly: false, editable: false, sortable: true, header: {text : "핸드폰번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'BIRTHDAY'      , width: 67, visible: true, readonly: false, editable: false, sortable: true, header: {text : "생일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LUNAR_SOLAR'   , width: 50, visible: true, readonly: false, editable: false, sortable: true, header: {text : "양/음력"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEX_DIV'       , width: 30, visible: true, readonly: false, editable: false, sortable: true, header: {text : "성별"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'E_MAIL'        , width: 306, visible: true, readonly: false, editable: false, sortable: true, header: {text : "E Mail"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'JOIN_DATE'     , width: 65, visible: true, readonly: false, editable: false, sortable: true, header: {text : "입사일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RETIRE_DATE'   , width: 72, visible: true, readonly: false, editable: false, sortable: true, header: {text : "퇴사일"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COM_DIV'       , width: 69, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DB출처"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var hr_d_code_level = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL_CODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "LEVEL_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LEVEL_NAME'  , width: 120, visible: true, readonly: false, editable: false, sortable: true, header: {text : "LEVEL_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GRADE_CODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "GRADE_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ORDER_SEQ'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ORDER_SEQ"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USE_YN'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "USE_YN"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var hr_d_code_grade = [
	{fieldName : 'GRADE_CODE'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "GRADE_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GRADE_NAME'    , width: 140, visible: true, readonly: false, editable: false, sortable: true, header: {text : "GRADE_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GRADE_ENG_MANG'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "GRADE_ENG_MANG"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ORDER_SEQ'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ORDER_SEQ"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_authuser_bk = [
	{fieldName : 'COMPANY_CODE'             , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_ID'                  , width: 55, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사번"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_CODE'                , width: 45, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'                , width: 127, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서명칭"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_NAME'                , width: 61, visible: true, readonly: false, editable: false, sortable: true, header: {text : "성명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_DESC'                , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "USER_DESC"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LOGIN_ID'                 , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "LOGIN_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_PASSWORD'            , width: 65, visible: true, readonly: false, editable: false, sortable: true, header: {text : "비밀번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USE_YN'                   , width: 27, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사용"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ACCESS_GRADE'             , width: 109, visible: false, readonly: false, editable: false, sortable: true, header: {text : "권한등급"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GROUP_CODE'               , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "GROUP_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LOGIN_TIME'               , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "LOGIN_TIME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LOGOUT_TIME'              , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "LOGOUT_TIME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'IP_ADDR'                  , width: 97, visible: true, readonly: false, editable: false, sortable: true, header: {text : "최근접속주소"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INPUT_DUTY_ID'            , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "INPUT_DUTY_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INPUT_DATE'               , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "INPUT_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHG_DUTY_ID'              , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHG_DUTY_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHG_DATE'                 , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHG_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HR_COMPANY_CODE'          , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "HR_COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'HR_DEPT_CODE'             , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "HR_DEPT_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AM_COMPANY_CODE'          , width: 115, visible: true, readonly: false, editable: false, sortable: true, header: {text : ""회계
회사코드""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AM_DEPT_CODE'             , width: 69, visible: true, readonly: false, editable: false, sortable: true, header: {text : ""회계
부서코드""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DATA_GRADE'               , width: 69, visible: false, readonly: false, editable: false, sortable: true, header: {text : "자료등급"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'MANAGER_YN'               , width: 29, visible: true, readonly: false, editable: false, sortable: true, header: {text : "팀장"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SM_AUTH_USER_AM_DEPT_CODE'               , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SM_AUTH_USER_AM_DEPT_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_AUTH'                , width: 109, visible: true, readonly: false, editable: false, sortable: true, header: {text : "권한등급"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_LEVEL'               , width: 69, visible: true, readonly: false, editable: false, sortable: true, header: {text : "자료등급"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AM_DEPT_NAME'             , width: 145, visible: true, readonly: false, editable: false, sortable: true, header: {text : "회계부서명"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dl_sm_auth_favorite = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMP_NO'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EMP_NO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMCODE'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PGMCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMNAME'     , width: 169, visible: true, readonly: true, editable: false, sortable: true, header: {text : "메뉴명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEQ'         , width: 42, visible: true, readonly: false, editable: false, sortable: true, header: {text : "순번"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COUNT'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COUNT"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var dl_sm_auth_pgmcode = [
	{fieldName : 'SEL'         , width: 27, visible: false, readonly: false, editable: false, sortable: true, header: {text : "선택"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMCODE'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PGMCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMNAME'     , width: 194, visible: true, readonly: false, editable: false, sortable: true, header: {text : "메뉴명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEVELOPEDTAG'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DEVELOPEDTAG"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_auth_user_rank = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_ID'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "USER_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'RANK_CODE'   , width: 38, visible: true, readonly: false, editable: false, sortable: true, header: {text : "RANK_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DNAME'       , width: 157, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DNAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'MANAGE_TAG'  , width: 49, visible: true, readonly: false, editable: false, sortable: true, header: {text : "MANAGE_TAG"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_d_comcode_authrlevel = [
	{fieldName : 'DCODE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DNAME' , width: 181, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Dname"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DNAME1' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DNAME1"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_d_comcode_authrank = [
	{fieldName : 'DCODE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DNAME' , width: 181, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Dname"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DNAME1' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DNAME1"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_d_auth_user_type = [
	{fieldName : 'COMPANY_CODE'  , width: 613, visible: true, readonly: false, editable: false, sortable: true, header: {text : ""Sm Auth User Type
Company Code""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_ID'       , width: 613, visible: true, readonly: false, editable: false, sortable: true, header: {text : ""Sm Auth User Type
User Id""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_TYPE_CODE'       , width: 1024, visible: true, readonly: false, editable: false, sortable: true, header: {text : ""Sm Auth User Type
Auth Type Code""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_CODE'     , width: 613, visible: true, readonly: false, editable: false, sortable: true, header: {text : ""Sm Auth User Type
Auth Code""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_ID'        , width: 521, visible: true, readonly: false, editable: false, sortable: true, header: {text : ""Sm Code Typed
Sys Id""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_YN'       , width: 613, visible: true, readonly: false, editable: false, sortable: true, header: {text : ""Sm Auth User Type
Auth Yn""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_TYPE_NAME'       , width: 1312, visible: true, readonly: false, editable: false, sortable: true, header: {text : ""Sm Code Typem
Auth Type Name""}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_NAME'     , width: 1312, visible: true, readonly: false, editable: false, sortable: true, header: {text : ""Sm Code Typed
Auth Name""}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_authuser_sel = [
	{fieldName : 'SEL'          , width: 35, visible: true, readonly: false, editable: false, sortable: true, header: {text : "선택"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_ID'      , width: 65, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사번"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_NAME'    , width: 86, visible: true, readonly: false, editable: false, sortable: true, header: {text : "성명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_DESC'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "USER_DESC"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LOGIN_ID'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "LOGIN_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_PASSWORD'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "USER_PASSWORD"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPANY_CODE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USE_YN'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "USE_YN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LOGIN_TIME'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "LOGIN_TIME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'IP_ADDR'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "IP_ADDR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LOGOUT_TIME'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "LOGOUT_TIME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_CODE'    , width: 58, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'    , width: 142, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서명칭"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_auth_user_dept = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_ID'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "USER_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_CODE'   , width: 66, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'   , width: 205, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_YN'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "AUTH_YN"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_auth_user_group = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_ID'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "USER_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GROUP_NAME'  , width: 205, visible: true, readonly: true, editable: false, sortable: true, header: {text : "그룹명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GROUP_CODE'  , width: 57, visible: true, readonly: false, editable: false, sortable: true, header: {text : "그룹코드"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_l_auth = [
	{fieldName : 'GROUP_CODE', width: 66, visible: true, readonly: false, editable: false, sortable: true, header: {text : "그룹코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GROUP_NAME', width: 148, visible: true, readonly: false, editable: false, sortable: true, header: {text : "그룹명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USE_TAG'   , width: 67, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사용여부"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_d_code_ref = [
	{fieldName : 'COM_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COM_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COM_NAME', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COM_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_authuser = [
	{fieldName : 'USER_ID'      , width: 65, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사번"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_NAME'    , width: 86, visible: true, readonly: false, editable: false, sortable: true, header: {text : "성명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_DESC'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "USER_DESC"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LOGIN_ID'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "LOGIN_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_PASSWORD'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "USER_PASSWORD"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPANY_CODE' , width: 59, visible: true, readonly: false, editable: false, sortable: true, header: {text : "회사코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USE_YN'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "USE_YN"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LOGIN_TIME'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "LOGIN_TIME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'IP_ADDR'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "IP_ADDR"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'LOGOUT_TIME'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "LOGOUT_TIME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_CODE'    , width: 58, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME'    , width: 142, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서명칭"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_d_company = [
	{fieldName : 'COMPANY_CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'COMPANY_NAME', width: 155, visible: true, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_authmenu = [
	{fieldName : 'FROOT_ID' , width: 211, visible: true, readonly: false, editable: false, sortable: true, header: {text : "FROOT_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ROOT_ID'  , width: 36, visible: true, readonly: false, editable: false, sortable: true, header: {text : "ROOT_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'MENU_NAME'  , width: 170, visible: true, readonly: false, editable: false, sortable: true, header: {text : "MENU_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_d_systemcode = [
	{fieldName : 'SYS_ID'  , width: 52, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SYS_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_NAME'  , width: 287, visible: true, readonly: false, editable: false, sortable: true, header: {text : "SYS_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_authuserbatchuser = [
	{fieldName : 'USER_ID'   , width: 60, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사번"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_NAME' , width: 61, visible: true, readonly: false, editable: false, sortable: true, header: {text : "성명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_CODE' , width: 58, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GRADE_CODE' , width: 37, visible: true, readonly: false, editable: false, sortable: true, header: {text : "직위"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_user_auth_limit_dw = [
	{fieldName : 'COMPCODE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'EMPNO'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "EMPNO"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMCODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PGMCODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_I'   , width: 61, visible: false, readonly: false, editable: false, sortable: true, header: {text : "AUTH_I"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_R'   , width: 61, visible: false, readonly: false, editable: false, sortable: true, header: {text : "AUTH_R"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_D'   , width: 61, visible: false, readonly: false, editable: false, sortable: true, header: {text : "AUTH_D"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_P'   , width: 61, visible: false, readonly: false, editable: false, sortable: true, header: {text : "AUTH_P"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMCODE_2'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "PGMCODE_2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'PGMNAME'  , width: 242, visible: true, readonly: true, editable: false, sortable: true, header: {text : "PGMNAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ISNEW'    , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ISNEW"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_auth_user_menu = [
	{fieldName : 'COMPANY_CODE' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_ID'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "USER_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_ID'       , width: 41, visible: true, readonly: false, editable: false, sortable: true, header: {text : "코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INPUT_DUTY_ID'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "INPUT_DUTY_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INPUT_DATE'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "INPUT_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHG_DUTY_ID'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHG_DUTY_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CHG_DATE'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CHG_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_YN'      , width: 56, visible: true, readonly: false, editable: false, sortable: true, header: {text : "접근권한"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'MASTER_YN'    , width: 65, visible: true, readonly: false, editable: false, sortable: true, header: {text : "마스타권한"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_NAME'     , width: 96, visible: true, readonly: false, editable: false, sortable: true, header: {text : "시스템명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SORT_ORDER'   , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SORT_ORDER"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ISNEW'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ISNEW"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_auth_user_type = [
	{fieldName : 'COMPANY_CODE'  , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "COMPANY_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_ID'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "USER_ID"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_TYPE_CODE'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "AUTH_TYPE_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_CODE'     , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "AUTH_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SYS_ID'        , width: 61, visible: true, readonly: false, editable: false, sortable: true, header: {text : "업무구분"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_YN'       , width: 39, visible: true, readonly: false, editable: false, sortable: true, header: {text : "권한"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_TYPE_NAME'       , width: 87, visible: true, readonly: false, editable: false, sortable: true, header: {text : "분류명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'AUTH_NAME'     , width: 97, visible: true, readonly: false, editable: false, sortable: true, header: {text : "AUTH_NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ORDER1'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ORDER1"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ORDER2'        , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ORDER2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ISNEW'         , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "ISNEW"}, styles: _Styles_textc, 		editor: _Editor_text } 
];


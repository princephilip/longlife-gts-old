var dl_sm_test_study = [
	{fieldName : 'DOC_SEQ'     , width: 84, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Doc Seq"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'VARCHAR_TYPE'     , width: 93, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Varchar 타입"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NUMBER_TYPE' , width: 93, visible: true, readonly: false, editable: false, sortable: true, header: {text : "숫자 소수점없음"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NUMBER_TYPE2' , width: 102, visible: true, readonly: false, editable: false, sortable: true, header: {text : "숫자 소수점2"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEFAULT_DATA' , width: 87, visible: true, readonly: false, editable: false, sortable: true, header: {text : "기본값"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DATE_TYPE'   , width: 75, visible: true, readonly: false, editable: false, sortable: true, header: {text : "날짜 타입"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'ZIP_CODE'    , width: 74, visible: true, readonly: false, editable: false, sortable: true, header: {text : "Zip Code"}, styles: _Styles_textc, 		editor: _Editor_text } 
];


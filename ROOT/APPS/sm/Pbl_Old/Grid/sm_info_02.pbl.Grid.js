var sm_q_descauthmodi = [
	{fieldName : 'NAME', width: 133, visible: true, readonly: false, editable: false, sortable: true, header: {text : "성명 / 부서"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEC' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SEC"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_q_descauth = [
	{fieldName : 'NAME', width: 133, visible: true, readonly: false, editable: false, sortable: true, header: {text : "성명 / 부서"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CODE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEC' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SEC"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_f_deptemp = [
	{fieldName : 'DEPT_CODE2', width: 58, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAME2', width: 112, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서명칭"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_ID'   , width: 70, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사원번호"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_NAME' , width: 72, visible: true, readonly: false, editable: false, sortable: true, header: {text : "사원명"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'NAME'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "NAME"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'CODE'      , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'SEC'       , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "SEC"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'GRADE_NAME'       , width: 98, visible: true, readonly: false, editable: false, sortable: true, header: {text : "직위"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TITLE_NAME'       , width: 97, visible: true, readonly: false, editable: false, sortable: true, header: {text : "직책"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_datetimes = [
	{fieldName : 'DATETIMES', width: 171, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DATETIMES"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_e_infodiv = [
	{fieldName : 'INFO_DIV_CODE', width: 98, visible: true, readonly: false, editable: false, sortable: true, header: {text : "알림구분코드"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INFO_DIV_NAME', width: 274, visible: true, readonly: false, editable: false, sortable: true, header: {text : "알림구분명칭"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_d_codedept = [
	{fieldName : 'DEPT_CODE', width: 58, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_CODE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_NAM' , width: 318, visible: true, readonly: false, editable: false, sortable: true, header: {text : "DEPT_NAM"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'DEPT_KIND' , width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "DEPT_KIND"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_d_infodesctitle = [
	{fieldName : 'WRITE_DATE', width: 80, visible: false, readonly: false, editable: false, sortable: true, header: {text : "WRITE_DATE"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'INFO_TITLE', width: 405, visible: true, readonly: false, editable: false, sortable: true, header: {text : "INFO_TITLE"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_c_descauthmodi = [
	{fieldName : 'WRITE_DATE', width: 258, visible: true, readonly: false, editable: false, sortable: true, header: {text : "작성내용"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'FROM_DATE' , width: 70, visible: true, readonly: false, editable: false, sortable: true, header: {text : "기간"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'TO_DATE'   , width: 70, visible: true, readonly: false, editable: false, sortable: true, header: {text : "~"}, styles: _Styles_textc, 		editor: _Editor_text } 
];

var sm_c_deptemp = [
	{fieldName : 'DEPT_CODE', width: 117, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_ID'  , width: 46, visible: true, readonly: false, editable: false, sortable: true, header: {text : "부서/사원"}, styles: _Styles_textc, 		editor: _Editor_text }, 
	{fieldName : 'USER_NAME'  , width: 129, visible: true, readonly: false, editable: false, sortable: true, header: {text : "USER_NAME"}, styles: _Styles_textc, 		editor: _Editor_text } 
];


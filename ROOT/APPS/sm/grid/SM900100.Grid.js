var fields_dg_1 = [
			{"fieldName" : "SYS_ID"         , "dataType" : "text"   },
			{"fieldName" : "FIELD_NAME"     , "dataType" : "text"   },
			{"fieldName" : "FIELD_NAME_KOR" , "dataType" : "text"   },
			{"fieldName" : "BASE_APPLY"    	, "dataType" : "text"   },
			{"fieldName" : "DATA_TYPE"      , "dataType" : "text"   },
			{"fieldName" : "WIDTH"          , "dataType" : "number" },
			{"fieldName" : "MUST_INPUT"     , "dataType" : "text" },
			{"fieldName" : "DEFAULT_VALUE"  , "dataType" : "text" },
			{"fieldName" : "TAG"            , "dataType" : "text"   },
			{"fieldName" : "VISIBLE"        , "dataType" : "text"   },
			{"fieldName" : "READONLY"       , "dataType" : "text"   },
			{"fieldName" : "EDITABLE"       , "dataType" : "text"   },
			{"fieldName" : "SORTABLE"       , "dataType" : "text"   },
			{"fieldName" : "BUTTON"         , "dataType" : "text"   },
			{"fieldName" : "IMAGE_LIST"     , "dataType" : "text"   },
			{"fieldName" : "HEADER"         , "dataType" : "text"   },
			{"fieldName" : "FOOTER"         , "dataType" : "text"   },
			{"fieldName" : "STYLES"         , "dataType" : "text"   },
			{"fieldName" : "DYNAMIC_STYLES" , "dataType" : "text"   },
			{"fieldName" : "EDITOR"         , "dataType" : "text"   },
			{"fieldName" : "RENDERER"       , "dataType" : "text"   },
			{"fieldName" : "FILTERS"        , "dataType" : "text"   },
			{"fieldName" : "VALIDATIONS"    , "dataType" : "text"   }
	];

var columns_dg_1 = [
			{fieldName: "SYS_ID"         , width: 60,  must_input:"Y", default_value:"", visible: false, styles: _Styles_textc,  header : {text: "시스템 ID"}        },
			{fieldName: "FIELD_NAME"     , width: 150, must_input:"Y", default_value:"", styles: _Styles_text,   header : {text: "필드명"}           },
			{fieldName: "FIELD_NAME_KOR" , width: 150, must_input:"Y", default_value:"", styles: _Styles_text,   header : {text: "한글필드명"}           },
			{fieldName: "BASE_APPLY"     , width: 30, must_input:"N", default_value:"", styles: _Styles_text,  button: "action", alwaysShowButton: true, header : {text: "적용"}         },
			{fieldName: "DATA_TYPE"      , width: 100, must_input:"N", default_value:"text", styles: _Styles_text,   editor: _Editor_dropdown, header : {text: "데이타타입"}       },
			{fieldName: "WIDTH"          , width: 60,  must_input:"N", default_value:"100", styles: _Styles_number, header : {text: "넓이"}             },
			{fieldName: "MUST_INPUT"     , width: 55,  must_input:"N", default_value:"N", styles: _Styles_checkbox, renderer: _Renderer_check_YN, header : {text: "필수입력"}     },
			{fieldName: "DEFAULT_VALUE"  , width: 150, must_input:"N", default_value:"", styles: _Styles_text,   header : {text: "초기값"}   },
			{fieldName: "TAG"            , width: 300, must_input:"N", default_value:"", styles: _Styles_text,   header : {text: "태그"}             },
			{fieldName: "VISIBLE"        , width: 45,  must_input:"N", default_value:"Y", styles: _Styles_checkbox,   renderer: _Renderer_check_YN, header : {text: "보이기"}           },
			{fieldName: "READONLY"       , width: 55,  must_input:"N", default_value:"N", styles: _Styles_checkbox,   renderer: _Renderer_check_YN, header : {text: "읽기전용"}         },
			{fieldName: "EDITABLE"       , width: 40,  must_input:"N", default_value:"Y", styles: _Styles_checkbox,   renderer: _Renderer_check_YN, header : {text: "수정"}             },
			{fieldName: "SORTABLE"       , width: 40,  must_input:"N", default_value:"Y", styles: _Styles_checkbox,   renderer: _Renderer_check_YN, header : {text: "정렬"}             },
			{fieldName: "BUTTON"         , width: 70,  must_input:"N", default_value:"", styles: _Styles_text,   editor: _Editor_dropdown, header : {text: "버튼"}             },
			{fieldName: "IMAGE_LIST"     , width: 100, must_input:"N", default_value:"", styles: _Styles_text,  header : {text: "이미지리스트"}     },
			{fieldName: "HEADER"         , width: 200, must_input:"N", default_value:"", styles: _Styles_text,  header : {text: "헤드"}             },
			{fieldName: "FOOTER"         , width: 200, must_input:"N", default_value:"", styles: _Styles_text,  header : {text: "푸터"}             },
			{fieldName: "STYLES"         , width: 150, must_input:"N", default_value:"_Styles_text", styles: _Styles_text,  editor: _Editor_dropdown, header : {text: "스타일"}           },
			{fieldName: "DYNAMIC_STYLES" , width: 200, must_input:"N", default_value:"", styles: _Styles_text,  header : {text: "동적스타일"}       },
			{fieldName: "EDITOR"         , width: 150, must_input:"N", default_value:"_Editor_text", styles: _Styles_text,  editor: _Editor_dropdown, header : {text: "에디터"}           },
			{fieldName: "RENDERER"       , width: 150, must_input:"N", default_value:"", styles: _Styles_text,  editor: _Editor_dropdown, header : {text: "렌더러"}           },
			{fieldName: "FILTERS"        , width: 150, must_input:"N", default_value:"", styles: _Styles_text,  editor: _Editor_dropdown, header : {text: "필터"}             },
			{fieldName: "VALIDATIONS"    , width: 300, must_input:"N", default_value:"", styles: _Styles_text,  header : {text: "유효검사"}         }
	];
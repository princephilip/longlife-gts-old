var fields_dg_1 = [
   	{fieldName : "L_CODE1" , dataType : "text"   },
 		{fieldName : "L_NAME1" , dataType : "text"   },
 		{fieldName : "L_CODE2" , dataType : "text"   },
 		{fieldName : "L_NAME2" , dataType : "text"   },
 		{fieldName : "L_CODE3" , dataType : "text"   },
 		{fieldName : "L_NAME3" , dataType : "text"   }
	];

var columns_dg_1 = [
   	{fieldName: "L_CODE1" , width: 80 , visible: false, must_input:"N", editable: false, styles: _Styles_textc, editor: _Editor_text, header : {text: "대분류코드"} },
		{fieldName: "L_NAME1" , width: 150, visible: true , must_input:"N", editable: false, styles: _Styles_text , editor: _Editor_text, header : {text: "대분류"} },
		{fieldName: "L_CODE2" , width: 80 , visible: false, must_input:"N", editable: false, styles: _Styles_textc, editor: _Editor_text, header : {text: "중분류코드"} },
		{fieldName: "L_NAME2" , width: 150, visible: true , must_input:"N", editable: false, styles: _Styles_text , editor: _Editor_text, header : {text: "중분류"} },
		{fieldName: "L_CODE3" , width: 80 , visible: false, must_input:"N", editable: false, styles: _Styles_textc, editor: _Editor_text, header : {text: "소분류코드"} },
		{fieldName: "L_NAME3" , width: 150, visible: true , must_input:"N", editable: false, styles: _Styles_text , editor: _Editor_text, header : {text: "소분류"} }
];

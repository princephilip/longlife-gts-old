var columns_dg_devprop = [
  {fieldName: 'SYS_ID'           , width: 60,  must_input: 'N', visible: true, readonly: true , editable: false, sortable: true, header: {text: "SYS_ID"}           , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'PGM_CODE'         , width: 80,  must_input: 'N', visible: true, readonly: true , editable: false, sortable: true, header: {text: "PGM_CODE"}         , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'GRID_ID'          , width: 70,  must_input: 'N', visible: true, readonly: true , editable: false, sortable: true, header: {text: "GRID_ID"}          , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'FIELDNAME'        , width: 120, must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "FIELDNAME"}        , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'SEQ'              , width: 50,  must_input: 'N', visible: false, readonly: false, editable: true , sortable: true, header: {text: "SEQ"}             , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'TABLE_NAME'       , width: 100, must_input: 'N', visible: false, readonly: false, editable: true , sortable: true, header: {text: "TABLE(Option)"}   , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'WIDTH'            , width: 60,  must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "width"}            , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'MUST_INPUT'       , width: 80,  must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "must_input"}       , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'VISIBLE'          , width: 60,  must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "visible"}          , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'READONLY'         , width: 60,  must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "readonly"}         , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'EDITABLE'         , width: 60,  must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "editable"}         , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'SORTABLE'         , width: 60,  must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "sortable"}         , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'HEADER'           , width: 80,  must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "header"}           , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'HEADER_TEXT'      , width: 100, must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "header_text"}      , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'STYLES'           , width: 50,  must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "styles"}           , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'EDITOR'           , width: 50,  must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "editor"}           , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'LOOKUPDISPLAY'    , width: 100, must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "lookupdisplay"}    , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'RENDERER'         , width: 150, must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "renderer"}         , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'BUTTON'           , width: 50,  must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "button"}           , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'ALWAYSSHOWBUTTON' , width: 50,  must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "alwaysshowbutton"} , styles: _Styles_text , editor: _Editor_text }
];


var columns_dg_devpropsrc = [
  {fieldName: 'SYS_ID'           , width: 60,  must_input: 'N', visible: true, readonly: true , editable: false, sortable: true, header: {text: "SYS_ID"}           , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'PGM_CODE'         , width: 80,  must_input: 'N', visible: true, readonly: true , editable: false, sortable: true, header: {text: "PGM_CODE"}         , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'GRID_ID'          , width: 70,  must_input: 'N', visible: true, readonly: true , editable: false, sortable: true, header: {text: "GRID_ID"}          , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'FIELDNAME'        , width: 120, must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "FIELDNAME"}        , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'SEQ'              , width: 50,  must_input: 'N', visible: false, readonly: false, editable: true , sortable: true, header: {text: "SEQ"}             , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'TABLE_NAME'       , width: 100, must_input: 'N', visible: false, readonly: false, editable: true , sortable: true, header: {text: "TABLE(Option)"}   , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'WIDTH'            , width: 60,  must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "width"}            , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'MUST_INPUT'       , width: 80,  must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "must_input"}       , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'VISIBLE'          , width: 60,  must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "visible"}          , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'READONLY'         , width: 60,  must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "readonly"}         , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'EDITABLE'         , width: 60,  must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "editable"}         , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'SORTABLE'         , width: 60,  must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "sortable"}         , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'HEADER'           , width: 80,  must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "header"}           , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'HEADER_TEXT'      , width: 100, must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "header_text"}      , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'STYLES'           , width: 50,  must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "styles"}           , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'EDITOR'           , width: 50,  must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "editor"}           , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'LOOKUPDISPLAY'    , width: 100, must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "lookupdisplay"}    , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'RENDERER'         , width: 150, must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "renderer"}         , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'BUTTON'           , width: 50,  must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "button"}           , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'ALWAYSSHOWBUTTON' , width: 50,  must_input: 'N', visible: true, readonly: false, editable: true , sortable: true, header: {text: "alwaysshowbutton"} , styles: _Styles_text , editor: _Editor_text }
];



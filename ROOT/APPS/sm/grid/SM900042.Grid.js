var columns_dg_1 = [
  {fieldName: 'UPDATE_TYPE'      , width: 100, must_input: 'N', visible: false, readonly: false , editable: false, sortable: true, header: {text: "UPDATE_TYPE"}     , styles: _Styles_textc , editor: _Editor_text },
  {fieldName: 'UPDATABLE'        , width: 100, must_input: 'N', visible: false, readonly: false , editable: false, sortable: true, header: {text: "UPDATABLE"}       , styles: _Styles_checkbox , editor: _Editor_checkbox },
  {fieldName: 'UPDATE_ISKEY'     , width: 100, must_input: 'N', visible: false, readonly: false , editable: false, sortable: true, header: {text: "UPDATE_ISKEY"}    , styles: _Styles_checkbox , editor: _Editor_checkbox },
  {fieldName: 'SYS_ID'           , width: 80,  must_input: 'N', visible: false, readonly: true , editable: false, sortable: true, header: {text: "SYS_ID(PK)"}       , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'PGM_CODE'         , width: 100, must_input: 'N', visible: false, readonly: true , editable: false, sortable: true, header: {text: "PGM_CODE(PK)"}    , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'GRID_ID'          , width: 90,  must_input: 'N', visible: false, readonly: true , editable: false, sortable: true, header: {text: "GRID_ID(PK)"}      , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'SEQ'              , width: 70,  must_input: 'N', visible: false, readonly: true, editable: false , sortable: true, header: {text: "SEQ(PK)"}         , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'FIELD_SEQ'        , width: 50,  must_input: 'N', visible: false, readonly: false, editable: false, sortable: true, header: {text: "FIELD_SEQ"}        , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'FIELDNAME'        , width: 120, must_input: 'N', visible: true, readonly: false, editable: false, sortable: true, header: {text: "FIELDNAME"}        , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'TABLE_NAME'       , width: 100, must_input: 'N', visible: false, readonly: false, editable: false, sortable: true, header: {text: "TABLE(Option)"}   , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'WIDTH'            , width: 60,  must_input: 'N', visible: false, readonly: false, editable: false, sortable: true, header: {text: "width"}            , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'MUST_INPUT'       , width: 80,  must_input: 'N', visible: false, readonly: false, editable: false, sortable: true, header: {text: "must_input"}       , styles: _Styles_checkbox , editor: _Editor_checkbox },
  {fieldName: 'VISIBLE'          , width: 60,  must_input: 'N', visible: false, readonly: false, editable: false, sortable: true, header: {text: "visible"}          , styles: _Styles_checkbox , editor: _Editor_checkbox },
  {fieldName: 'READONLY'         , width: 60,  must_input: 'N', visible: false, readonly: false, editable: false, sortable: true, header: {text: "readonly"}         , styles: _Styles_checkbox , editor: _Editor_checkbox },
  {fieldName: 'EDITABLE'         , width: 60,  must_input: 'N', visible: false, readonly: false, editable: false, sortable: true, header: {text: "editable"}         , styles: _Styles_checkbox , editor: _Editor_checkbox },
  {fieldName: 'SORTABLE'         , width: 60,  must_input: 'N', visible: false, readonly: false, editable: false, sortable: true, header: {text: "sortable"}         , styles: _Styles_checkbox , editor: _Editor_checkbox },
  {fieldName: 'HEADER'           , width: 160, must_input: 'N', visible: false, readonly: false, editable: false, sortable: true, header: {text: "header"}           , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'HEADER_TEXT'      , width: 130, must_input: 'N', visible: true, readonly: false, editable: false, sortable: true, header: {text: "header_text"}      , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'STYLES'           , width: 100, must_input: 'N', visible: false, readonly: false, editable: false, sortable: true, header: {text: "styles"}           , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'EDITOR'           , width: 100, must_input: 'N', visible: true, readonly: false, editable: false, sortable: true, header: {text: "editor"}           , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'LOOKUPDISPLAY'    , width: 100, must_input: 'N', visible: false, readonly: false, editable: false, sortable: true, header: {text: "lookupdisplay"}    , styles: _Styles_checkbox , editor: _Editor_checkbox },
  {fieldName: 'RENDERER'         , width: 150, must_input: 'N', visible: false, readonly: false, editable: false, sortable: true, header: {text: "renderer"}         , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'BUTTON'           , width: 100, must_input: 'N', visible: false, readonly: false, editable: false, sortable: true, header: {text: "button"}           , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'ALWAYSSHOWBUTTON' , width: 100, must_input: 'N', visible: false, readonly: false, editable: false, sortable: true, header: {text: "alwaysShowbutton"} , styles: _Styles_checkbox , editor: _Editor_checkbox },
  {fieldName: 'HEADERTOOLTIP'    , width: 130, must_input: 'N', visible: false, readonly: false, editable: false, sortable: true, header: {text: "headerToolTip"}    , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'MERGE'            , width: 60,  must_input: 'N', visible: false, readonly: false, editable: false, sortable: true, header: {text: "merge"}            , styles: _Styles_checkbox , editor: _Editor_checkbox },
  {fieldName: 'MAXLENGTH'        , width: 60,  must_input: 'N', visible: false, readonly: false, editable: false, sortable: true, header: {text: "maxLength"}        , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'FOOTEREXPR'       , width: 130, must_input: 'N', visible: false, readonly: false, editable: false, sortable: true, header: {text: "footerExpr"}       , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'FOOTERALIGN'      , width: 130, must_input: 'N', visible: false, readonly: false, editable: false, sortable: true, header: {text: "footerAlign"}      , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'FOOTERSTYLE'      , width: 130, must_input: 'N', visible: false, readonly: false, editable: false, sortable: true, header: {text: "footerStyle"}      , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'FOOTERPRE'        , width: 130, must_input: 'N', visible: false, readonly: false, editable: false, sortable: true, header: {text: "footerPre"}        , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'FOOTERPOST'       , width: 130, must_input: 'N', visible: false, readonly: false, editable: false, sortable: true, header: {text: "footerPost"}       , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'RESIZABLE'        , width: 60,  must_input: 'N', visible: false, readonly: false, editable: false, sortable: true, header: {text: "resizble"}         , styles: _Styles_checkbox , editor: _Editor_checkbox }
];

var columns_dg_2 = [
  {fieldName: 'SYS_ID'           , width: 60,  must_input: 'N', visible: false, readonly: false , editable: false, sortable: true, header: {text: "SYS_ID"}           , styles: _Styles_text , editor: _Editor_text, merge: true },
  {fieldName: 'PGM_CODE'         , width: 70,  must_input: 'N', visible: true,  readonly: false , editable: false, sortable: true, header: {text: "PGM_CODE"}         , styles: _Styles_text , editor: _Editor_text, merge: true },
  {fieldName: 'PGM_NAME'         , width: 150, must_input: 'N', visible: true,  readonly: false , editable: false, sortable: true, header: {text: "PGM_NAME"}         , styles: _Styles_text , editor: _Editor_text, merge: true },
  {fieldName: 'GRID_ID'          , width: 50, must_input: 'N', visible: true,  readonly: false, editable: false, sortable: true, header: {text: "GRID"}         , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'GRID_RNK'         , width: 30,  must_input: 'N', visible: true,  readonly: false , editable: false, sortable: true, header: {text: "순번"}           , styles: _Styles_text , editor: _Editor_text }
  
];

var columns_dg_3 = [
  {fieldName: 'ORDER_SEQ'  , width: 50, must_input: 'N', visible: true, readonly: true , editable: false, sortable: false, header: {text: "정렬순서"}         , styles: _Styles_numberc , editor: _Editor_number },
  {fieldName: 'FIELDNAME'  , width: 100, must_input: 'N', visible: true, readonly: false , editable: true, sortable: false, header: {text: "컬럼명"}           , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'FIELDTITLE' , width: 120, must_input: 'N', visible: true, readonly: false , editable: true, sortable: false, header: {text: "컬럼 TITLE"}       , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'ACTION'     , width: 15, must_input: 'N', visible: true, readonly: false, editable: false, sortable: false, header: {text:"삭제"}, styles: _Styles_textc, editor: _Editor_text, button: "action", alwaysShowButton: true},
  {fieldName: 'EDITOR'     , width: 85, must_input: 'N', visible: false, readonly: false , editable: true, sortable: false, header: {text: "EDITOR"}       , styles: _Styles_text , editor: _Editor_text }
];

var columns_dg_4 = [
  {fieldName: 'SAVED_DATE' , width: 160, must_input: 'N', visible: true, readonly: false , editable: true, sortable: false, header: {text: "수정일자"}     , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'PGM_CODE'   , width: 85, must_input: 'N', visible: true, readonly: false , editable: true, sortable: false, header: {text: "프로그램코드"}  , styles: _Styles_text , editor: _Editor_text },
  {fieldName: 'GRID_ID'    , width: 65, must_input: 'N', visible: true, readonly: false, editable: false, sortable: false, header: {text: "그리드 ID"}     , styles: _Styles_textc, editor: _Editor_text },
  {fieldName: 'FF_TITLE'   , width: 65, must_input: 'N', visible: false, readonly: false, editable: false, sortable: false, header: {text: "그리드 ID"}     , styles: _Styles_textc, editor: _Editor_text },
  {fieldName: 'FF_COLUMNS' , width: 65, must_input: 'N', visible: false, readonly: false, editable: false, sortable: false, header: {text: "그리드 ID"}     , styles: _Styles_textc, editor: _Editor_text },
  {fieldName: 'FF_OBJECT'  , width: 200, must_input: 'N', visible: true, readonly: false, editable: false, sortable: false, header: {text: "설정json"}     , styles: _Styles_textc, editor: _Editor_text },
  {fieldName: 'FF_ID'      , width: 65, must_input: 'N', visible: true, readonly: false, editable: false, sortable: false, header: {text: "History ID"}     , styles: _Styles_textc, editor: _Editor_text },
];
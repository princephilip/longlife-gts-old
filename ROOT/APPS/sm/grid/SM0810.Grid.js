var fields_dg_parent = [
     		{fieldName : "CODE_ID"             , dataType : "text"   },
 			{fieldName : "CODE_ID_NM"          , dataType : "text"   },
 			{fieldName : "CODE_ID_DC"          , dataType : "text"   },
 			{fieldName : "USE_AT"          	   , dataType : "text" },
 			{fieldName : "CL_CODE"             , dataType : "text"   }
	];

var columns_dg_parent = [
   			{fieldName: "CODE_ID"       , width: 60 , visible: true, must_input:"Y", isKey:'Y', editable: true, styles: _Styles_textc, editor: _Editor_text, header : {text: "코드ID"}},
			{fieldName: "CODE_ID_NM"    , width: 120, visible: true , must_input:"Y", editable: true, styles: _Styles_text , editor: _Editor_text, header : {text: "코드ID명"}        },
			{fieldName: "CODE_ID_DC"    , width: 220, visible: true , must_input:"Y", editable: true, styles: _Styles_text , editor: _Editor_text, header : {text: "코드설명"}        },
			{fieldName: 'USE_AT',		 width: 60,  must_input: 'N', visible: true, readonly: false, editable: true, sortable: true, header: {text: "사용여부"}, styles: _Styles_checkbox, editor: _Editor_text, renderer: _Renderer_check_YN },
			{fieldName: "CL_CODE"       , width: 60, visible: false , must_input:"Y", editable: false, styles: _Styles_text , editor: _Editor_text, header : {text: "븐류코드"}        }
	];

var fields_dg_child = [
   		     {fieldName : "CODE_ID"       , dataType : "text" },
   		     {fieldName : "CODE"          , dataType : "text" }, 
			 {fieldName : "CODE_NM"       , dataType : "text"}, 
			 {fieldName : "CODE_DC"       , dataType : "text"}, 
		     {fieldName : "USE_AT"        , dataType : "text"}
	];

var columns_dg_child = [
   			 {fieldName: "CODE_ID"      , width: 60 , must_input:"Y", isKey:'Y', visible: false, styles: _Styles_textc,  header : {text: "코드ID"}    },
   		     {fieldName: "CODE"        	, width: 80, must_input:"Y", isKey:'Y', visible: true, styles: _Styles_textc,  header : {text: "코드"}     },
			 {fieldName: "CODE_NM"      , width: 140, must_input:"Y", visible: true, styles: _Styles_text,  header : {text: "코드명"}     }, 
			 {fieldName: "CODE_DC"      , width: 300, must_input:"Y", visible: true, styles: _Styles_text,  header : {text: "코드설명"}     },
		     {fieldName: 'USE_AT',		 width: 60,  must_input: 'N', visible: true, readonly: false, editable: true, sortable: true, header: {text: "사용여부"}, styles: _Styles_checkbox, editor: _Editor_text, renderer: _Renderer_check_YN }
	];
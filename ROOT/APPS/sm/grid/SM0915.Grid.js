var fields_dg_parent = [
      {fieldName : "SYS_ID"             , dataType : "text"   },
      {fieldName : "HCODE"              , dataType : "text"   },
      {fieldName : "HNAME"              , dataType : "text"   },
      {fieldName : "FLOAT_POS"          , dataType : "number" },
      {fieldName : "REMARK"             , dataType : "text"   }
  ];

var columns_dg_parent = [
      {fieldName: "SYS_ID"       , width: 60 , visible: true, must_input:"Y", editable: true, styles: _Styles_textc, editor: _Editor_text, header : {text: "업무코드"}},
      {fieldName: "HCODE"        , width: 200, visible: true , must_input:"Y", isKey:'Y', editable: true, styles: _Styles_text , editor: _Editor_text, header : {text: "주코드"}        },
      {fieldName: "HNAME"        , width: 250, visible: true , must_input:"Y", editable: true, styles: _Styles_text , editor: _Editor_text, header : {text: "주코드명"}        },
      {fieldName: "FLOAT_POS"    , width: 70, visible: true , must_input:"N", editable: true, styles: _Styles_numberc, editor: _Editor_number, header : {text: "정렬순서"}        },
      {fieldName: "REMARK"       , width: 400, visible: true , must_input:"N", editable: true, styles: _Styles_text , editor: _Editor_text, header : {text: "비고"}        }
  ];

var fields_dg_child = [
         {fieldName : "SYS_ID"         , dataType : "text"   },
         {fieldName : "HCODE"          , dataType : "text"   },
         {fieldName : "USE_YESNO"      , dataType : "text"   },  
         {fieldName : "DCODE"          , dataType : "text"   }, 
         {fieldName : "DNAME"          , dataType : "text"   }, 
         {fieldName : "DVALUE"         , dataType : "number" }, 
         {fieldName : "RELATE_CODE1"   , dataType : "text"   }, 
         {fieldName : "RELATE_CODE2"   , dataType : "text"   }, 
         {fieldName : "RELATE_CODE3"   , dataType : "text"   }, 
         {fieldName : "RELATE_CODE4"   , dataType : "text"   }, 
         {fieldName : "RELATE_CODE5"   , dataType : "text"   }, 
         {fieldName : "DVALUE1"        , dataType : "text"   }, 
         {fieldName : "DVALUE2"        , dataType : "text"   }, 
         {fieldName : "DVALUE3"        , dataType : "text"   }, 
         {fieldName : "DVALUE4"        , dataType : "text"   }, 
         {fieldName : "DVALUE5"        , dataType : "text"   },
         {fieldName : "DNAME1"         , dataType : "text"   }, 
         {fieldName : "DNAME2"         , dataType : "text"   }, 
         {fieldName : "DNAME3"         , dataType : "text"   }, 
         {fieldName : "DNAME4"         , dataType : "text"   }, 
         {fieldName : "DNAME5"         , dataType : "text"   }, 
         {fieldName : "FIX_CODE1"      , dataType : "text"   }, 
         {fieldName : "FIX_CODE2"      , dataType : "text"   }, 
         {fieldName : "FIX_CODE3"      , dataType : "text"   }, 
         {fieldName : "FIX_CODE4"      , dataType : "text"   }, 
         {fieldName : "FIX_CODE5"      , dataType : "text"   }, 
         {fieldName : "RMK"            , dataType : "text"   }
  ];

var columns_dg_child = [
         {fieldName: "SYS_ID"       , width: 60 , must_input:"Y", visible: false, styles: _Styles_textc, header : {text: "업무코드"}   },
         {fieldName: "HCODE"        , width: 100, must_input:"Y", visible: false, styles: _Styles_textc, header : {text: "주코드"}     },
         {fieldName: "USE_YESNO"    , width: 60,  must_input:"N", visible: true,  editable: false, styles: _Styles_checkbox, renderer: _Renderer_check_YN, header : {text: "사용여부"}     },
         {fieldName: "DCODE"        , width: 100, must_input:"Y", visible: true,  styles: _Styles_text,  header : {text: "부코드"}     }, 
         {fieldName: "DNAME"        , width: 200, must_input:"Y", visible: true,  styles: _Styles_text,  header : {text: "부코드명"}   },
         {fieldName: "DVALUE"       , width: 100, must_input:"N", visible: true,  styles: _Styles_number,  editor: _Editor_number, header : {text: "정렬순서"}  },
         {fieldName: "RELATE_CODE1" , width: 100, must_input:"N", visible: true,  styles: _Styles_text,  header : {text: "관련코드1"}  },
         {fieldName: "RELATE_CODE2" , width: 100, must_input:"N", visible: true,  styles: _Styles_text,  header : {text: "관련코드2"}  },
         {fieldName: "RELATE_CODE3" , width: 100, must_input:"N", visible: true,  styles: _Styles_text,  header : {text: "관련코드3"}  },
         {fieldName: "RELATE_CODE4" , width: 100, must_input:"N", visible: true,  styles: _Styles_text,  header : {text: "관련코드4"}  },
         {fieldName: "RELATE_CODE5" , width: 100, must_input:"N", visible: true,  styles: _Styles_text,  header : {text: "관련코드5"}  },
         {fieldName: "DVALUE1"      , width: 100, must_input:"N", visible: true,  styles: _Styles_text,  header : {text: "자료값1"}    },
         {fieldName: "DVALUE2"      , width: 100, must_input:"N", visible: true,  styles: _Styles_text,  header : {text: "자료값2"}    },
         {fieldName: "DVALUE3"      , width: 100, must_input:"N", visible: true,  styles: _Styles_text,  header : {text: "자료값3"}    },
         {fieldName: "DVALUE4"      , width: 100, must_input:"N", visible: true,  styles: _Styles_text,  header : {text: "자료값4"}    },
         {fieldName: "DVALUE5"      , width: 100, must_input:"N", visible: true,  styles: _Styles_text,  header : {text: "자료값5"}    },
         {fieldName: "DNAME1"       , width: 100, must_input:"N", visible: true,  styles: _Styles_text,  header : {text: "부코드명1"}  },
         {fieldName: "DNAME2"       , width: 100, must_input:"N", visible: true,  styles: _Styles_text,  header : {text: "부코드명2"}  },
         {fieldName: "DNAME3"       , width: 100, must_input:"N", visible: true,  styles: _Styles_text,  header : {text: "부코드명3"}  },
         {fieldName: "DNAME4"       , width: 100, must_input:"N", visible: true,  styles: _Styles_text,  header : {text: "부코드명4"}  },
         {fieldName: "DNAME5"       , width: 100, must_input:"N", visible: true,  styles: _Styles_text,  header : {text: "부코드명5"}  },
         {fieldName: "FIX_CODE1"    , width: 100, must_input:"N", visible: true,  styles: _Styles_text,  header : {text: "고정코드1"}  },
         {fieldName: "FIX_CODE2"    , width: 100, must_input:"N", visible: true,  styles: _Styles_text,  header : {text: "고정코드2"}  },
         {fieldName: "FIX_CODE3"    , width: 100, must_input:"N", visible: true,  styles: _Styles_text,  header : {text: "고정코드3"}  },
         {fieldName: "FIX_CODE4"    , width: 100, must_input:"N", visible: true,  styles: _Styles_text,  header : {text: "고정코드4"}  },
         {fieldName: "FIX_CODE5"    , width: 100, must_input:"N", visible: true,  styles: _Styles_text,  header : {text: "고정코드5"}  },
         {fieldName: "RMK"          , width: 100, must_input:"N", visible: true,  styles: _Styles_text,  header : {text: "비고"}       }
  ];
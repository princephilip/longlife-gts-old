var columns_dg_1 = [
		{fieldName: "SYS_ID"       , width:  60, must_input:"Y", visible: true , readonly: true , editable: true , styles: _Styles_textc  , editor: _Editor_text  , header : {text:"업무코드"}  },
		{fieldName: "SYS_NAME"     , width:  80, must_input:"N", visible: true , readonly: true , editable: false, styles: _Styles_textc  , editor: _Editor_text  , header : {text:"업무명"}    },
		{fieldName: "HCODE"        , width: 200, must_input:"Y", visible: true , readonly: false, editable: true , styles: _Styles_text   , editor: _Editor_text  , header : {text:"주코드"}    , isKey:'Y'},
		{fieldName: "HNAME"        , width: 250, must_input:"Y", visible: true , readonly: false, editable: true , styles: _Styles_text   , editor: _Editor_text  , header : {text:"주코드명"}  },
		{fieldName: "FLOAT_POS"    , width:  70, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_numberc, editor: _Editor_number, header : {text:"정렬순서"}  },
		{fieldName: "RMK"          , width: 800, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_text   , editor: _Editor_text  , header : {text:"비고"}      }
	];

var columns_dg_2 = [
		{fieldName: "DCODE"        , width: 100, must_input:"Y", visible: true , readonly: false, editable: true , styles: _Styles_textc   , editor: _Editor_text    , header : {text: "부코드"}       , isKey:'Y'}, 
		{fieldName: "DNAME"        , width: 200, must_input:"Y", visible: true , readonly: false, editable: true , styles: _Styles_text    , editor: _Editor_text    , header : {text: "부코드명"}     },
		{fieldName: "USE_YN"       , width:  60, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_checkbox, editor: _Editor_checkbox, header : {text: "사용여부"}     },
		{fieldName: "DVALUE"       , width: 100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_number  , editor: _Editor_number  , header : {text: "정렬순서"}     },
		{fieldName: "DVALUE2"      , width: 100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_text    , editor: _Editor_text    , header : {text: "자료값(문자)"} },
		{fieldName: "RELATE_CODE1" , width: 100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_text    , editor: _Editor_text    , header : {text: "관련코드1"}    },
		{fieldName: "RELATE_CODE2" , width: 100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_text    , editor: _Editor_text    , header : {text: "관련코드2"}    },
		{fieldName: "RELATE_CODE3" , width: 100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_text    , editor: _Editor_text    , header : {text: "관련코드3"}    },
		{fieldName: "RELATE_CODE4" , width: 100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_text    , editor: _Editor_text    , header : {text: "관련코드4"}    },
		{fieldName: "RELATE_CODE5" , width: 100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_text    , editor: _Editor_text    , header : {text: "관련코드5"}    },
		{fieldName: "DNAME1"       , width: 100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_text    , editor: _Editor_text    , header : {text: "부코드명1"}    },
		{fieldName: "DNAME2"       , width: 100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_text    , editor: _Editor_text    , header : {text: "부코드명2"}    },
		{fieldName: "FIX_CODE1"    , width: 100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_text    , editor: _Editor_text    , header : {text: "고정코드1"}    },
		{fieldName: "FIX_CODE2"    , width: 100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_text    , editor: _Editor_text    , header : {text: "고정코드2"}    },
		{fieldName: "FIX_CODE3"    , width: 100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_text    , editor: _Editor_text    , header : {text: "고정코드3"}    },
		{fieldName: "FIX_CODE4"    , width: 100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_text    , editor: _Editor_text    , header : {text: "고정코드4"}    },
		{fieldName: "FIX_CODE5"    , width: 100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_text    , editor: _Editor_text    , header : {text: "고정코드5"}    },
		{fieldName: "RMK"          , width: 800, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_text    , editor: _Editor_text    , header : {text: "비고"}         },
		{fieldName: "HCODE"        , width: 100, must_input:"Y", visible: false, readonly: false, editable: true , styles: _Styles_textc   , editor: _Editor_text    , header : {text: "주코드"}       },
		{fieldName: "SYS_ID"       , width:  60, must_input:"Y", visible: false, readonly: false, editable: true , styles: _Styles_textc   , editor: _Editor_text    , header : {text: "업무코드"}     }
	];
	
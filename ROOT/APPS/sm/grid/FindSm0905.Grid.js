var fields_dg_1 = [
   	{fieldName : "CODE"   	           , dataType : "text"   },
   	{fieldName : "LABEL"    	         , dataType : "text"   }
	];

var columns_dg_1 = [
   	{fieldName: "CODE"        , width: 50 , visible: false, must_input:"Y", isKey :"Y", editable: false, styles: _Styles_textc, editor: _Editor_text, header : {text: "코드"}},
   	{fieldName: "LABEL"       , width: 192 , visible: true, must_input:"Y", isKey :"Y", editable: false, styles: _Styles_text, editor: _Editor_text, header : {text: "업체명"}}
];

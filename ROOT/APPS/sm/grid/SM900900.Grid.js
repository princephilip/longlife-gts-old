var fields_dg_1 = [
    {fieldName: 'INCUST_CODE'     , dataType: 'text' }, 
    {fieldName: 'ORDER_NO'        , dataType: 'text' }, 
    {fieldName: 'ORDER_LINK_SEQ'  , dataType: 'number' }, 
    {fieldName: 'ORDER_DATE'      , dataType: 'datetime' }, 
    {fieldName: 'LINK_BRANCH_ID'  , dataType: 'text' }, 
    {fieldName: 'LINK_CAR_NO'     , dataType: 'text' }, 
    {fieldName: 'DELI_BRANCH_ID'  , dataType: 'text' }, 
    {fieldName: 'DELI_CAR_NO'     , dataType: 'text' }, 
    {fieldName: 'CRUD_TYPE'       , dataType: 'text' }, 
    {fieldName: 'PROC_CODE'       , dataType: 'text' }, 
    {fieldName: 'PROC_DATE'       , dataType: 'datetime' }, 
    {fieldName: 'LINK_TMS_NO'     , dataType: 'text' }, 
    {fieldName: 'DELI_TMS_NO'     , dataType: 'text' }, 
    {fieldName: 'WDATE'           , dataType: 'datetime' }, 
    {fieldName: 'WUSER_ID'        , dataType: 'text' }, 
    {fieldName: 'WIP'             , dataType: 'text' }, 
    {fieldName: 'RDATE'           , dataType: 'datetime' }, 
    {fieldName: 'RUSER_ID'        , dataType: 'text' }, 
    {fieldName: 'RIP'             , dataType: 'text' } 
];

var columns_dg_1 = [
    {fieldName:  'INCUST_CODE'    , width: 100, isKey: "Y", must_input: 'N', visible: true , readonly: false , editable: true , sortable: true , header:  {text: 'COLS1'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'ORDER_NO'       , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS2'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'ORDER_LINK_SEQ' , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS3'}, styles: _Styles_number, editor: _Editor_number}, 
    {fieldName:  'ORDER_DATE'     , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: '한글'}, styles: _Styles_date, editor: _Editor_date}, 
    {fieldName:  'LINK_BRANCH_ID' , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS5'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'LINK_CAR_NO'    , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS6'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'DELI_BRANCH_ID' , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS7'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'DELI_CAR_NO'    , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS8'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'CRUD_TYPE'      , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS9'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'PROC_CODE'      , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS10'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'PROC_DATE'      , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS11'}, styles: _Styles_date, editor: _Editor_date}, 
    {fieldName:  'LINK_TMS_NO'    , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS12'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'DELI_TMS_NO'    , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS13'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'WDATE'          , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS14'}, styles: _Styles_date, editor: _Editor_date}, 
    {fieldName:  'WUSER_ID'       , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS15'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'WIP'            , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS16'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'RDATE'          , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS17'}, styles: _Styles_date, editor: _Editor_date}, 
    {fieldName:  'RUSER_ID'       , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS18'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'RIP'            , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS19'}, styles: _Styles_text, editor: _Editor_text} 
];

var fields_dg_2 = [
    {fieldName: 'INCUST_CODE'     , dataType: 'text' }, 
    {fieldName: 'ORDER_NO'        , dataType: 'text' }, 
    {fieldName: 'ORDER_LINK_SEQ'  , dataType: 'number' }, 
    {fieldName: 'ORDER_DATE'      , dataType: 'datetime' }, 
    {fieldName: 'LINK_BRANCH_ID'  , dataType: 'text' }, 
    {fieldName: 'LINK_CAR_NO'     , dataType: 'text' }, 
    {fieldName: 'DELI_BRANCH_ID'  , dataType: 'text' }, 
    {fieldName: 'DELI_CAR_NO'     , dataType: 'text' }, 
    {fieldName: 'CRUD_TYPE'       , dataType: 'text' }, 
    {fieldName: 'PROC_CODE'       , dataType: 'text' }, 
    {fieldName: 'PROC_DATE'       , dataType: 'datetime' }, 
    {fieldName: 'LINK_TMS_NO'     , dataType: 'text' }, 
    {fieldName: 'DELI_TMS_NO'     , dataType: 'text' }, 
    {fieldName: 'WDATE'           , dataType: 'datetime' }, 
    {fieldName: 'WUSER_ID'        , dataType: 'text' }, 
    {fieldName: 'WIP'             , dataType: 'text' }, 
    {fieldName: 'RDATE'           , dataType: 'datetime' }, 
    {fieldName: 'RUSER_ID'        , dataType: 'text' }, 
    {fieldName: 'RIP'             , dataType: 'text' } 
];

var columns_dg_2 = [
    {fieldName:  'INCUST_CODE'    , width: 100, isKey: "Y", must_input: 'N', visible: true , readonly: false , editable: true , sortable: true , header:  {text: 'COLS1'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'ORDER_NO'       , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS2'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'ORDER_LINK_SEQ' , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS3'}, styles: _Styles_number, editor: _Editor_number}, 
    {fieldName:  'ORDER_DATE'     , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: '한글'}, styles: _Styles_date, editor: _Editor_date}, 
    {fieldName:  'LINK_BRANCH_ID' , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS5'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'LINK_CAR_NO'    , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS6'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'DELI_BRANCH_ID' , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS7'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'DELI_CAR_NO'    , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS8'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'CRUD_TYPE'      , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS9'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'PROC_CODE'      , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS10'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'PROC_DATE'      , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS11'}, styles: _Styles_date, editor: _Editor_date}, 
    {fieldName:  'LINK_TMS_NO'    , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS12'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'DELI_TMS_NO'    , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS13'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'WDATE'          , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS14'}, styles: _Styles_date, editor: _Editor_date}, 
    {fieldName:  'WUSER_ID'       , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS15'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'WIP'            , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS16'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'RDATE'          , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS17'}, styles: _Styles_date, editor: _Editor_date}, 
    {fieldName:  'RUSER_ID'       , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS18'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName:  'RIP'            , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS19'}, styles: _Styles_text, editor: _Editor_text} 
];


var fields_dg_3 = [
    {fieldName: 'COMPANY_CODE'  , dataType: 'text' }, 
    {fieldName: 'PROJ_CODE' 		, dataType: 'text' }, 
    {fieldName: 'SEQ' 					, dataType: 'number' }, 
    {fieldName: 'CUST_CODE' 		, dataType: 'datetime' }, 
    {fieldName: 'CUST_NAME' 		, dataType: 'text' }, 
    {fieldName: 'CUST_DIV' 			, dataType: 'text' }, 
    {fieldName: 'UP_CONST_CODE' , dataType: 'text' }, 
    {fieldName: 'UP_CONST_NAME' , dataType: 'text' }, 
    {fieldName: 'CONST_CODE' 		, dataType: 'text' }, 
    {fieldName: 'CONST_NAME' 		, dataType: 'text' }, 
    {fieldName: 'DETAIL_CODE' 	, dataType: 'datetime' }, 
    {fieldName: 'DETAIL_NAME' 	, dataType: 'text' }, 
    {fieldName: 'REPRESENT_NAME' , dataType: 'text' }, 
    {fieldName: 'BIZ_STATUS' 		, dataType: 'datetime' }, 
    {fieldName: 'BIZ_TYPE' 			, dataType: 'text' }, 
    {fieldName: 'PHONE' 				, dataType: 'text' }, 
    {fieldName: 'FAX' 					, dataType: 'datetime' }, 
    {fieldName: 'ZIP_CODE' 			, dataType: 'text' }, 
    {fieldName: 'ADDR1' 				, dataType: 'text' },
    {fieldName: 'ADDR2' 				, dataType: 'text' },
    {fieldName: 'REMARKS' 			, dataType: 'text' },
    {fieldName: 'CREATE_YN' 		, dataType: 'text' },
    {fieldName: 'PHONE_DIV' 		, dataType: 'text' }
];

var columns_dg_3 = [
    {fieldName: 'COMPANY_CODE'   , width: 100, isKey: "Y", must_input: 'N', visible: true , readonly: false , editable: true , sortable: true , header:  {text: 'COLS1'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName: 'PROJ_CODE' 		 , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS2'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName: 'SEQ' 					 , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS3'}, styles: _Styles_number, editor: _Editor_number}, 
    {fieldName: 'CUST_CODE' 		 , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: '한글'}, styles: _Styles_date, editor: _Editor_date}, 
    {fieldName: 'CUST_NAME' 		 , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS5'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName: 'CUST_DIV' 			 , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS6'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName: 'UP_CONST_CODE'  , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS7'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName: 'UP_CONST_NAME'  , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS8'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName: 'CONST_CODE' 		 , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS9'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName: 'CONST_NAME' 		 , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS10'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName: 'DETAIL_CODE' 	 , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS11'}, styles: _Styles_date, editor: _Editor_date}, 
    {fieldName: 'DETAIL_NAME' 	 , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS12'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName: 'REPRESENT_NAME' , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS13'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName: 'BIZ_STATUS' 		 , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS14'}, styles: _Styles_date, editor: _Editor_date}, 
    {fieldName: 'BIZ_TYPE' 			 , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS15'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName: 'PHONE' 				 , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS16'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName: 'FAX' 					 , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS17'}, styles: _Styles_date, editor: _Editor_date}, 
    {fieldName: 'ZIP_CODE' 			 , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS18'}, styles: _Styles_text, editor: _Editor_text}, 
    {fieldName: 'ADDR1' 				 , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS19'}, styles: _Styles_text, editor: _Editor_text},
    {fieldName: 'ADDR2' 				 , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS19'}, styles: _Styles_text, editor: _Editor_text},
    {fieldName: 'REMARKS' 			 , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS19'}, styles: _Styles_text, editor: _Editor_text},
    {fieldName: 'CREATE_YN' 		 , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS19'}, styles: _Styles_text, editor: _Editor_text},
    {fieldName: 'PHONE_DIV' 		 , width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: true , header:  {text: 'COLS19'}, styles: _Styles_text, editor: _Editor_text}
];

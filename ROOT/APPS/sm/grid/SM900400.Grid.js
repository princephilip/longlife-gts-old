/*
var fields_dg_1 = [ 
      {fieldName: 'COMPANY_CODE'     , dataType: 'text' },
      {fieldName: 'USER_ID'          , dataType: 'text' },
      {fieldName: 'REC_SEQ'          , dataType: 'text' },
      {fieldName: 'EMP_COMPANY_CODE' , dataType: 'text' },
      {fieldName: 'GUBUN_CODE'       , dataType: 'text' },
      {fieldName: 'EMP_NO'           , dataType: 'text' },      
      {fieldName: 'REC_NAME'         , dataType: 'text' },
      {fieldName: 'HP_NO'            , dataType: 'text' },
      {fieldName: 'EMP_NAME'         , dataType: 'text' }      
  ];
*/
var columns_dg_1 = [ 
      {fieldName: 'COMPANY_CODE'     , width: 80 , must_input: 'Y', visible: false, readonly: true, editable: false, sortable: true, header: {text:"회사코드"}     , styles: _Styles_text, editor: _Editor_text },
      {fieldName: 'USER_ID'          , width: 60 , must_input: 'Y', visible: false, readonly: true, editable: false, sortable: true, header: {text:"아이디"}       , styles: _Styles_text, editor: _Editor_text },
      {fieldName: 'REC_SEQ'          , width: 80 , must_input: 'Y', visible: true,  readonly: true, editable: false, sortable: true, header: {text:"순번"}         , styles: _Styles_text, editor: _Editor_text },
      {fieldName: 'EMP_COMPANY_CODE' , width: 60 , must_input: 'N', visible: false, readonly: true, editable: false, sortable: true, header: {text:"사원회사코드"} , styles: _Styles_text, editor: _Editor_text },
      {fieldName: 'GUBUN_CODE'       , width: 100, must_input: 'N', visible: false, readonly: true, editable: false, sortable: true, header: {text:"구분"}    		 , styles: _Styles_textc, editor: _Editor_dropdown ,lookupDisplay: true},    
      {fieldName: 'EMP_NO'           , width: 120, must_input: 'N', visible: false, readonly: true, editable: false, sortable: true, header: {text:"사원번호"}   , styles: _Styles_textc, editor: _Editor_text },      
      {fieldName: 'REC_NAME'         , width: 120, must_input: 'N', visible: true , readonly: false, editable: true, sortable: true, header: {text:"성 명"}       , styles: _Styles_textc, editor: _Editor_text },
      {fieldName: 'HP_NO'            , width: 200, must_input: 'N', visible: true , readonly: false, editable: true, sortable: true, header: {text:"핸드폰번호"}  , styles: _Styles_textc, editor: _Editor_text }     
  ];

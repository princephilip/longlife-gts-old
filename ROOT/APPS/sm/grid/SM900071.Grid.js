var columns_dg_1 = [ 
	{fieldName: 'SYS_ID'            , width:  60, must_input: 'N', visible: true , readonly: true , editable: true , sortable: false, header: {text: "시스템"}                  , styles: _Styles_textc     , editor: _Editor_text },
  {fieldName: 'CODE'							, width: 100, must_input: 'N', visible: true , readonly: true , editable: true , sortable: false, header: {text: "키코드"}  	 						  , styles: _Styles_textc			, editor: _Editor_text },
	{fieldName: 'NAME'							, width: 150, must_input: 'Y', visible: true , readonly: true , editable: true , sortable: false, header: {text: "사용명칭"}      						, styles: _Styles_textc			, editor: _Editor_text },
	{fieldName: 'HTML_STRING'				, width: 150, must_input: 'Y', visible: false, readonly: true , editable: true , sortable: false, header: {text: "HTML소스_스트링"}      			, styles: _Styles_textc			, editor: _Editor_text },
	{fieldName: 'HTML_STRING_VIEW'	, width: 150, must_input: 'N', visible: false, readonly: false, editable: true , sortable: false, header: {text: "HTML소스_스트링_뷰(임의컬럼)"} , styles: _Styles_textc			, editor: _Editor_text },
	{fieldName: 'USE_YN'						, width: 70	, must_input: 'N', visible: true , readonly: false, editable: true , sortable: false, header: {text: "사용여부"}      						, styles: _Styles_checkbox	, editor: _Editor_checkbox },
	{fieldName: 'REMK'							, width: 250, must_input: 'N', visible: false, readonly: true , editable: true , sortable: false, header: {text: "비고"}      							, styles: _Styles_textc			, editor: _Editor_text }
];
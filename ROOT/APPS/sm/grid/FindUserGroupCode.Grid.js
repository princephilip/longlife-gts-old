var fields_dg_1 = [
   	{fieldName : "COMPANY_CODE", 	dataType : "text"},
	{fieldName : "USER_GROUP_CODE", dataType : "text"},
	{fieldName : "USER_GROUP_NAME", dataType : "text"},
	{fieldName : "REMARKS", 		dataType : "text"}
];

var columns_dg_1 = [
   	{fieldName: "COMPANY_CODE", 		width: 50 , visible: false, must_input:"N", isKey :"Y", editable: false, styles: _Styles_textc, editor: _Editor_text, header : {text: "회사코드"}},
	{fieldName: "USER_ID", 				width: 90, visible: true , must_input:"N", isKey:'Y', editable: false, styles: _Styles_text , editor: _Editor_text, header : {text: "사용자ID"}},
	{fieldName: "USER_NAME", 			width: 140, visible: true , must_input:"N", editable: false, styles: _Styles_text , editor: _Editor_text, header : {text: "사용자명"}        },
	{fieldName: "USER_DESC", 			width: 150, visible: false , must_input:"N", editable: false, styles: _Styles_text , editor: _Editor_text, header : {text: ""}        }
];

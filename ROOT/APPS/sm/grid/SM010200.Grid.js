
var columns_dg_1 = [
		{fieldName: "COMPANY_CODE"      , width:  60, must_input:"N", visible: false , readonly: true , editable: true , styles: _Styles_textc  , editor: _Editor_text  , header : {text:"회사코드"}  },
		{fieldName: "DEPT_CODE"       	, width:  100, must_input:"N", visible: true , readonly: true , editable: true , styles: _Styles_textc  , editor: _Editor_text  , header : {text:"부서코드"}  },
		{fieldName: "OBJ_TYPE"       		, width:  100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_textc	 , editor: _Editor_dropdown ,	lookupDisplay:true , header : {text:"부서타입"}  },
		{fieldName: "DEPT_NAME"       	, width:  200, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_textc  , editor: _Editor_text  , header : {text:"부서명"}  },
		{fieldName: "DEPT_SNAME"       	, width:  200, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_textc  , editor: _Editor_text  , header : {text:"부서명(상세)"}  },
		{fieldName: "UP_DEPT_CODE"      , width:  100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_textc  , editor: _Editor_text  , header : {text:"상위부서코드"}  },
		{fieldName: "KOSTL"       			, width:  100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_textc  , editor: _Editor_text  , header : {text:"견적서번호"}  },
		{fieldName: "SALE_DIV"       		, width:  100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_textc	 , editor: _Editor_dropdown ,	lookupDisplay:true , header : {text:"매출구분"}  },
		{fieldName: "USE_YESNO"       	, width:  60, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_checkbox,	lookupDisplay:true  , header : {text:"사용여부"}  },
		{fieldName: "SORT_ORDER"       	, width:  60, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_number  , editor: _Styles_number  , header : {text:"정렬순서"}  },
		{fieldName: "PROC_NAME"       	, width: 160, must_input:"N", visible: true , readonly: true, editable: false , styles: _Styles_text  , editor: _Editor_text  , header : {text:"부서별 공정"} , button: "action", alwaysShowButton: true }
	];


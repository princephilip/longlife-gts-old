var fields_dg_1 = [
		{"fieldName" : "DCODE"    		  , "dataType" : "text"     },
		{"fieldName" : "DNAME"		      , "dataType" : "text"     }
	];

var columns_dg_1 = [
		{fieldName: "DCODE"      , width: 60, styles: _Styles_text, header : {text: "업무구분"}            },
		{fieldName: "DNAME"      , width: 140, styles: _Styles_text, header : {text: "업무명"}             }
	];
	
//스타일의 앞의 선택 부분을 조정해야 한다.

var fields_dg_2 = [
		{"fieldName" : "D1"    		  , "dataType" : "text"     },
		{"fieldName" : "D2"		      , "dataType" : "text"     },
		{"fieldName" : "D3"		      , "dataType" : "text"     },
		{"fieldName" : "D4"		      , "dataType" : "text"     },
		{"fieldName" : "D5"		      , "dataType" : "text"     },
		{"fieldName" : "D6"		      , "dataType" : "text"     }
	];

var columns_dg_2 = [
		{fieldName: "D1"      , width: 100, styles: _Styles_text, header : {text: "명칭"}  			         },
		{fieldName: "D2"      , width: 100, styles: _Styles_text, header : {text: "정렬순서"}            },
		{fieldName: "D3"      , width: 100, styles: _Styles_text, header : {text: "관련코드"}            },
		{fieldName: "D4"      , width: 100, styles: _Styles_text, header : {text: "마감월"}      	       },
		{fieldName: "D5"      , width: 100, styles: _Styles_text, header : {text: "마감일"}        	     },
		{fieldName: "D6"      , width: 100, styles: _Styles_text, header : {text: "사원명"}          	   }		
	];
	
var fields_dg_3 = [
		{"fieldName" : "D1"    		  , "dataType" : "text"     },
		{"fieldName" : "D2"		      , "dataType" : "text"     },
		{"fieldName" : "D3"		      , "dataType" : "text"     },
		{"fieldName" : "D4"		      , "dataType" : "text"     }
	];

var columns_dg_3 = [
		{fieldName: "D1"      , width: 150, styles: _Styles_text, header : {text: "테이블명"}           	},
		{fieldName: "D2"      , width: 150, styles: _Styles_text, header : {text: "컬럼명"}             	},
		{fieldName: "D3"      , width: 150, styles: _Styles_text, header : {text: "조건"}             	},
		{fieldName: "D4"      , width: 150, styles: _Styles_text, header : {text: "트리거생성"}             	}
	];

var fields_dg_4 = [
		{"fieldName" : "D1"    		  , "dataType" : "text"     },
		{"fieldName" : "D2"		      , "dataType" : "text"     }
	];

var columns_dg_4 = [
		{fieldName: "D1"      , width: 150, styles: _Styles_text, header : {text: "사번"}           	},
		{fieldName: "D2"      , width: 150, styles: _Styles_text, header : {text: "성명"}             	}
	];	
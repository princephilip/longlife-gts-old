var fields_dg_list = [
       		{fieldName : "COMPANY_CODE"    	  , dataType : "text" },
       		{fieldName : "BBS_ID"          	  , dataType : "text" },
 			{fieldName : "BBS_NM"          	  , dataType : "text" },
 			{fieldName : "TRGET_ID"        	  , dataType : "text" },
 			{fieldName : "REGIST_SE_CODE"  	  , dataType : "text" },
 			{fieldName : "REGIST_SE_CODE_NM"  , dataType : "text" }
	];

var columns_dg_list = [
   			{fieldName: "COMPANY_CODE"		, width: 60 , visible: false, must_input:"Y", isKey:'Y', editable: false, styles: _Styles_textc, editor: _Editor_text, header : {text: ""}},
			{fieldName: "BBS_ID"    		, width: 160, visible: true , must_input:"Y", isKey:'Y', editable: false, styles: _Styles_text , editor: _Editor_text, header : {text: "게시판ID"}},
			{fieldName: "BBS_NM"    		, width: 230, visible: true , must_input:"Y", editable: false, styles: _Styles_text , editor: _Editor_text, header : {text: "게시판명"}        },
			{fieldName: 'TRGET_ID' 			, width: 60, visible: false , must_input:"Y", editable: false, styles: _Styles_text , editor: _Editor_text, header : {text: ""}        },
			{fieldName: "REGIST_SE_CODE"    , width: 80, visible: true , must_input:"Y", editable: false, styles: _Styles_text , editor: _Editor_text, header : {text: "동록구분"}},
			{fieldName: "REGIST_SE_CODE_NM" , width: 150, visible: true , must_input:"Y", editable: false, styles: _Styles_text , editor: _Editor_text, header : {text: "등록구분명"}}
	];

var fields_dg_1 = [
   		     {fieldName : "COMPANY_CODE"    , dataType : "text" },
   		     {fieldName : "BBS_ID"          , dataType : "text" }, 
			 {fieldName : "TRGET_ID"       	, dataType : "text"}, 
			 {fieldName : "USER_ID"       	, dataType : "text"}, 
		     {fieldName : "USER_NAME"       , dataType : "text"}
	];

var columns_dg_1 = [
   			 {fieldName: "COMPANY_CODE"     , width: 60 , must_input:"Y", isKey:'Y', visible: false, styles: _Styles_textc,  header : {text: ""}    },
   		     {fieldName: "BBS_ID"        	, width: 80, must_input:"Y", isKey:'Y', visible: false, styles: _Styles_textc,  header : {text: ""}     },
			 {fieldName: "TRGET_ID"      	, width: 140, must_input:"Y",  isKey:'Y', visible: false, styles: _Styles_text,  header : {text: ""}     }, 
			 {fieldName: "USER_ID"        	, width: 140, visible: true , isKey:"Y", must_input:"Y", editable: true, styles: _Styles_text , button: "action", alwaysShowButton: true, editor: _Editor_text, header : {text: "사용자ID"}},
			 {fieldName: "USER_NAME"      	, width: 240, must_input:"Y", visible: true, editable: false, styles: _Styles_text,  header : {text: "사용자명"}     }
	];
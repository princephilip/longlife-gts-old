var columns_dg_1 = [
		{fieldName: "COMP_CODE"    , width:  60, must_input:"N", visible: true, readonly: true , editable: true , styles: _Styles_textc  , editor: _Editor_text  , header : {text:"회사코드"}  },
		{fieldName: "HCODE"        , width:  60, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_text   , editor: _Editor_text  , header : {text:"코드"}    , isKey:'Y' },
		{fieldName: "HNAME"        , width: 200, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_text   , editor: _Editor_text  , header : {text:"명칭"}  },
		{fieldName: "REMARKS"      , width: 300, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_text   , editor: _Editor_text  , header : {text:"비고"}      }
	];

var columns_dg_2 = [
		{fieldName: "COMP_CODE" , width: 100, must_input:"N", visible: true, readonly: false, editable: true , styles: _Styles_textc   , editor: _Editor_text    , header : {text: "회사코드"}  },
		{fieldName: "HCODE"    	, width:  60, must_input:"N", visible: true, readonly: false, editable: true , styles: _Styles_text    , editor: _Editor_text    , header : {text: "코드"}     },
		{fieldName: "DCODE"    	, width:  60, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_text    , editor: _Editor_text    , header : {text: "코드"}, isKey:'Y' },
		{fieldName: "DNAME"    	, width: 200, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_text    , editor: _Editor_text    , header : {text: "명칭"}     },
		{fieldName: "VALUE1"   	, width: 150, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_text    , editor: _Editor_text		, header : {text: "값1"}     },
		{fieldName: "VALUE2"   	, width: 150, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_text    , editor: _Editor_text	  , header : {text: "값2"}     },
		{fieldName: "VALUE3"   	, width: 150, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_text    , editor: _Editor_text    , header : {text: "정렬순서"} },
		{fieldName: "USE_YN" 		, width:  80, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_checkbox, editor: _Editor_checkbox, header : {text: "사용유무"}    },
		{fieldName: "REMARKS" 	, width: 300, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_text    , editor: _Editor_text    , header : {text: "비고"}    }
	];


var columns_dg_1 = [
		{fieldName: "COMPANY_CODE"      , width:  60 , must_input:"N", visible: false, readonly: true , editable: true , styles: _Styles_textc  , editor: _Editor_text  	 , header : {text:"회사코드"}  },
		{fieldName: "DEPT_CODE"      		, width:  100, must_input:"N", visible: true , readonly: true , editable: true , styles: _Styles_textc  , editor: _Editor_text  	 , header : {text:"부서코드"}  },
		{fieldName: "DEPT_NAME"      		, width:  250, must_input:"Y", visible: true , readonly: false, editable: true , styles: _Styles_text		, editor: _Editor_text		 , header : {text:"부서명"}  },
		{fieldName: "DEPT_ENG"      		, width:  100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_textc  , editor: _Editor_text  	 , header : {text:"부서명(영문)"}  },
		{fieldName: "TAX_COMPANY_CODE"  , width:  100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_textc  , editor: _Editor_text  	 , header : {text:"세무사업장코드"}  },
		{fieldName: "COST_TAG"      		, width:  100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_textc  , editor: _Editor_text  	 , header : {text:"원가구분"}  	},
		{fieldName: "COST_CODE"      		, width:  100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_textc  , editor: _Editor_text  	 , header : {text:"원가코드"}  	},
		{fieldName: "LEVELS"      			, width:  100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_textc  , editor: _Editor_text  	 , header : {text:"레벨코드"}  	},
		{fieldName: "LEVEL1"      			, width:  100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_textc  , editor: _Editor_text  	 , header : {text:"레벨코드1"}  },
		{fieldName: "LEVEL2"      			, width:  100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_textc  , editor: _Editor_text  	 , header : {text:"레벨코드2"}  },
		{fieldName: "LEVEL3"      			, width:  100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_textc  , editor: _Editor_text  	 , header : {text:"레벨코드3"}  },
		{fieldName: "SORT_ORDER"      	, width:  100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_textc  , editor: _Editor_text  	 , header : {text:"정렬순서"}  	},
		{fieldName: "USING_TAG"      		, width:  100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_textc  , editor: _Editor_dropdown , header : {text:"사용유무"} , lookupDisplay:true 	},
		{fieldName: "OFFICE_CODE"     	, width:  100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_textc  , editor: _Editor_text  	 , header : {text:"사업장코드"} },
		{fieldName: "GROUP_CODE"      	, width:  100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_textc  , editor: _Editor_text  	 , header : {text:"그룹코드"}  	},
		{fieldName: "BUDGET_CONTROLTAG" , width:  100, must_input:"N", visible: true , readonly: false, editable: true , styles: _Styles_textc  , editor: _Editor_text  	 , header : {text:"예산유무"}  	}
	];
	
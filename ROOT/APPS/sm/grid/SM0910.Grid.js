 fields_dg_user = [
 		{fieldName : "COMPANY_CODE"         , dataType : "text"   },
 		{fieldName : "USER_ID"              , dataType : "text"   },
 		{fieldName : "USER_NAME"			      , dataType : "text"   },
 		{fieldName : "USER_DESC"			      , dataType : "text"   },
 		{fieldName : "REMARK"					      , dataType : "text"   }
	];

var columns_dg_user = [
 		{fieldName: "COMPANY_CODE"       , width: 50 , visible: false, must_input:"N", editable: true, styles: _Styles_textc, editor: _Editor_text, header : {text: "회사코드"}},
		{fieldName: "USER_ID"        , width: 80, visible: true , must_input:"N", byte_input:"Y", byte_len:6, editable: true, styles: _Styles_text , editor: _Editor_text, header : {text: "사용자ID"}},
		{fieldName: "USER_NAME"        , width: 140, visible: true , must_input:"N", editable: true, styles: _Styles_text , editor: _Editor_text, header : {text: "사용자명"}},
		{fieldName: "USER_DESC"       , width: 140, visible: true , must_input:"N", readonly:true,editable: false, styles: _Styles_text , editor: _Editor_text, header : {text: "사용자설명"}},
		{fieldName: "REMARK"       , width: 140, visible: true , must_input:"N", editable: false, styles: _Styles_text , editor: _Editor_text, header : {text: "비고"}        }
];

var fields_dg_1 = [
 		{fieldName : "COMPANY_CODE"             , dataType : "text"   },
 		{fieldName : "USER_ID"             			, dataType : "text"   },
 		{fieldName : "USER_GROUP_CODE"          , dataType : "text"   },
 		{fieldName : "USER_GROUP_NAME"          , dataType : "text"   }
	];

var columns_dg_1 = [
 		{fieldName: "COMPANY_CODE"       , width: 50 , visible: false, must_input:"Y", isKey:"Y", editable: true, styles: _Styles_textc, editor: _Editor_text, header : {text: "회사코드"}},
		{fieldName: "USER_ID"       		 , width: 50, visible: false , must_input:"Y", isKey:"Y", byte_input:"Y", byte_len:7, editable: true, styles: _Styles_text , editor: _Editor_text, header : {text: "사용자ID"}},
		{fieldName: "USER_GROUP_CODE"    , width: 150, visible: true , must_input:"Y", isKey:"Y", button: "action", alwaysShowButton: true,byte_input:"Y", byte_len:6, editable: true, styles: _Styles_text , editor: _Editor_text, header : {text: "그룹코드"}},
		{fieldName: "USER_GROUP_NAME"    , width: 200, visible: true , must_input:"N", readonly:true,editable: false, styles: _Styles_text , editor: _Editor_text, header : {text: "그룹명"}        }
];

var fields_dg_menu = [
   	{fieldName : "COMPANY_CODE"   , dataType : "text"   },
   	{fieldName : "USER_GROUP_CODE", dataType : "text"   },
   	{fieldName : "SYS_ID"         , dataType : "text"   },
   	{fieldName : "PGM_CODE"       , dataType : "text"   }, 
   	{fieldName : "PGM_NAME"       , dataType : "text"   }, 
   	{fieldName : "MENU_CODE"      , dataType : "text"   },
   	{fieldName : "MENU_NAME"      , dataType : "text"   }, 
   	{fieldName : "SORT_ORDER"     , dataType : "text"   }, 
		{fieldName : "AUTH_I"         , dataType : "text"   }, 
		{fieldName : "AUTH_R"         , dataType : "text"   }, 
		{fieldName : "AUTH_D"         , dataType : "text"   }, 
		{fieldName : "AUTH_P"         , dataType : "text"   }, 
		{fieldName : "AUTH_E"         , dataType : "text"   }
];

var columns_dg_menu = [
      {fieldName: "COMPANY_CODE"     , width: 60 , must_input:"N", isKey:'Y',visible: false, styles: _Styles_textc,  header : {text: "회사코드"}    },
      {fieldName: "USER_GROUP_CODE"  , width: 100, must_input:"N", isKey:'Y',visible: false, styles: _Styles_textc,  header : {text: "그룹코드"}     },
			{fieldName: 'SYS_ID', width: 80,  must_input: 'N', isKey: 'N', visible: true,  lookupDisplay: true,  readonly: false, editable: true, sortable: true, header: {text: "시스템코드"}, styles: _Styles_text, editor: _Editor_dropdown },
			{fieldName: "PGM_CODE"         , width: 200, must_input:"N", isKey:'Y',visible: false, editable: true,styles: _Styles_text,  header : {text: "프로그램코드"}     },
			{fieldName: "PGM_NAME"         , width: 200, must_input:"N", visible: false, styles: _Styles_text,  header : {text: "프로그램명"}     },
			{fieldName: "MENU_CODE"        , width: 80, must_input:"N", visible: true, editable: true,styles: _Styles_text,  header : {text: "메뉴코드"}     }, 
			{fieldName: "MENU_NAME"        , width: 200, must_input:"N", visible: true, editable: true,styles: _Styles_text,  header : {text: "메뉴명"}     },
			{fieldName: "SORT_ORDER"       , width: 60, must_input:"N", visible: true, editable: true,styles: _Styles_textc,  header : {text: "정열순서"}     },
			{fieldName: 'AUTH_I', width: 50, must_input: 'N', visible: true, readonly: false, editable: true, sortable: true, header: {text: "입력"}, styles: _Styles_checkbox, editor: _Editor_text, renderer: _Renderer_check_YN },
			{fieldName: 'AUTH_R', width: 50, must_input: 'N', visible: true, readonly: false, editable: true, sortable: true, header: {text: "조회"}, styles: _Styles_checkbox, editor: _Editor_text, renderer: _Renderer_check_YN },
			{fieldName: 'AUTH_D', width: 50, must_input: 'N', visible: true, readonly: false, editable: true, sortable: true, header: {text: "삭제"}, styles: _Styles_checkbox, editor: _Editor_text, renderer: _Renderer_check_YN },
			{fieldName: 'AUTH_P', width: 50, must_input: 'N', visible: true, readonly: false, editable: true, sortable: true, header: {text: "인쇄"}, styles: _Styles_checkbox, editor: _Editor_text, renderer: _Renderer_check_YN },
			{fieldName: 'AUTH_E', width: 50, must_input: 'N', visible: true, readonly: false, editable: true, sortable: true, header: {text: "엑셀"}, styles: _Styles_checkbox, editor: _Editor_text, renderer: _Renderer_check_YN }
	];
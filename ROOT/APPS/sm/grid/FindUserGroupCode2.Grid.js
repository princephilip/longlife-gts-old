var fields_dg_1 = [
   	{fieldName : "COMPANY_CODE", 	dataType : "text"},
	{fieldName : "USER_GROUP_CODE", dataType : "text"},
	{fieldName : "USER_GROUP_NAME", dataType : "text"},
	{fieldName : "REMARKS", 		dataType : "text"}
];

var columns_dg_1 = [
   	{fieldName: "COMPANY_CODE", 		width: 50 , visible: false, must_input:"N", isKey :"Y", editable: false, styles: _Styles_textc, editor: _Editor_text, header : {text: "회사코드"}},
	{fieldName: "USER_GROUP_CODE", 		width: 50, visible: true , must_input:"N", isKey:'Y', editable: false, styles: _Styles_textc , editor: _Editor_text, header : {text: "코드"}},
	{fieldName: "USER_GROUP_NAME", 		width: 200, visible: true , must_input:"N", editable: false, styles: _Styles_text , editor: _Editor_text, header : {text: "그룹명"}        },
	{fieldName: "REMARKS", 				width: 150, visible: false , must_input:"N", editable: false, styles: _Styles_text , editor: _Editor_text, header : {text: "비고"}        }
];

var fields_dg_1 = [ 
      {fieldName: 'COMPANY_CODE', dataType: 'text' },
      {fieldName: 'SYS_ID', dataType: 'text' },
      {fieldName: 'SYS_NAME', dataType: 'text' },
      {fieldName: 'USE_YESNO', dataType: 'text' },
      {fieldName: 'SORT_ORDER', dataType: 'number' }
  ];

var columns_dg_1 = [ 
      {fieldName: 'COMPANY_CODE', width: 80, must_input: 'Y', isKey: 'Y', visible: false, readonly: false, editable: true, sortable: true, header: {text: "시스템코드"}, styles: _Styles_textc, editor: _Editor_text, lookupdisplay: false },
      {fieldName: 'SYS_ID', width: 80, must_input: 'Y', isKey: 'Y',visible: true, readonly: false, editable: true, sortable: true, header: {text: "시스템코드"}, styles: _Styles_textc, editor: _Editor_text, lookupdisplay: false },
      {fieldName: 'SYS_NAME', width: 100, must_input: 'N', visible: true, readonly: false, editable: true, sortable: true, header: {text: "시스템명"}, styles: _Styles_text, editor: _Editor_text, lookupdisplay: false },
      {fieldName: 'USE_YESNO', width: 60, must_input: 'N', default_value: 'N', visible: true, readonly: false, editable: true, sortable: true, header: {text: "사용유무"}, styles: _Styles_checkbox, editor: _Editor_text, lookupdisplay: false },
      {fieldName: 'SORT_ORDER', width: 60, must_input: 'N', visible: true, readonly: false, editable: true, sortable: true, header: {text: "정렬순서"}, styles: _Styles_numberc, editor: _Editor_number, lookupdisplay: false }
  ];

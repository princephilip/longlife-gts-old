var _IsModal = (window.dialogArguments?true:false);
var _IsPopup = (window.opener?true:false);
var _Caller  = (_IsModal?window.dialogArguments:(_IsPopup?window.opener:window.parent));
var ls_inged = false;

var options_dg_1 = {
		panel: {visible: false},
		footer: {visible: false},
		checkBar: {visible: false},
		statusBar: {visible: false},
		select: {style: RealGrids.SelectionStyle.ROWS},
    edit: {
			insertable: true,
			appendable: true,
			updatable: true,
			deletable: true
    }
};

var coSALARY_DIV2 = _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100', new Array('HR', 'SALARY_DIV'), 'json2');
var coS_APPLY_YYMM2 = _X.XmlSelect('hr', 'HR_PAYX_BASE_MASTER', 'HR_PAYX_BASE_MASTER_R01', new Array(_Caller._FindCode.findcode[0], _Caller._FindCode.findcode[1]), 'json2');
var coCOLS = _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_CODE_ALLOW', new Array('%', '1'), 'json2');

//화면 디자인관련 요소들 초기화 작업
function x_InitForm(){
	
	_X.SetHTML(ubtns, _X.Button("xBtnSave_dg_1", "x_DAO_Save(dg_1)", "/Theme/images/btn/x_save") + _X.Button("xBtnClose", "x_Close()", "/Theme/images/btn/x_close"));
	
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", _FIND_SYS, _FIND_GRID, _FIND_QUERY+"|" + _FIND_SELECT);
	
	_X.DropDownList_SetData(S_SALARY_DIV2, coSALARY_DIV2, _Caller._FindCode.findcode[1], false, false, null);
	_X.DropDownList_SetData(S_APPLY_YYMM2, coS_APPLY_YYMM2, _Caller._FindCode.findcode[2], false, true, '전체');
   
}

function x_DAO_Retrieve(){
	
	dg_1.Retrieve([_Caller._FindCode.findcode[0], S_SALARY_DIV2.value, S_APPLY_YYMM2.value]);
}

//xe_GridRowFocusChange가 안되서...추가
function xe_M_RealGridRowFocusChange(a_obj, oldIndex, newIndex){
	
//	if(dg_1.GetItem(newIndex.itemIndex + 1, 'ROW_DIV') == 0){
//			dg_1.GridReadOnly( true);
//	}else{
//			dg_1.GridReadOnly( false);
//	}
}

function xe_GridRowFocusChange(a_dg, a_newrow, a_oldrow){//alert("myAlert((xe_GridRowFocusChange)");
	
}

function x_DAO_Save(){
	
	setTimeout('x_SaveGridData()', 0);                 
}

function x_DAO_Insert(row){}
function x_DAO_Delete(row){}
function x_DAO_Duplicate(row){}
function x_Duplicate_After(obj, rowIdx){}
function x_DAO_Excel(){dg_1.ExcelExport();}
function xe_GridRowFocusChange(a_dg, a_row, a_col){}
function xe_GridDoubleClick(a_dg, a_ctrlKey, a_altKey, a_shiftKey, a_stageX, a_stageY){alert('9');}
	
function xe_M_RealGridRowFocusChange(a_obj, oldIndex, newIndex){
	dg_1.GridReadOnly( true);
}

var xe_M_RealGridItemClick = function(a_obj, cellIndex) {
	
//	alert(a_obj.id);
//	alert(cellIndex.itemIndex +1); 로우
//	alert(a_obj.GetColumnIndexByName(cellIndex.fieldName)); 컬럼순서
//	alert(cellIndex.fieldName); 컬럼이름

	var ls_row = cellIndex.itemIndex +1;
	var ls_column_name = cellIndex.fieldName;
	
	var ls_apply_yymm = dg_1.GetItem(ls_row, 'APPLY_YYMM');
	var ls_allow_code = dg_1.GetItem(ls_row, ls_column_name);

	pf_gridReadNone(ls_apply_yymm, ls_allow_code);
}

//사용중인 수당일 경우 수정불가능 하게 함
function pf_gridReadNone(a__apply_yymm, a_allow_code){
	
	var ls_res = _X.XmlSelect('hr', 'HR_PAYX_BASE_MASTER', 'HR_PAYX_BASE_MASTER_R02', new Array('100', S_SALARY_DIV2.value, a__apply_yymm, a_allow_code), 'array');
	
	if(ls_res[0][0] > 0) {
  	dg_1.GridReadOnly( true);
  }else{
  	dg_1.GridReadOnly( false);
  }
}

function xe_GridDataChange(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	
	var itemIndex = a_dg.GridRoot().getCurrent().itemIndex;
	var datas = a_dg.GridRoot().GetRowData(itemIndex);
	
	var lo_arr = new Array();
	lo_arr.push([datas.COLS1, 'COLS1']);
	lo_arr.push([datas.COLS2, 'COLS2']);
	lo_arr.push([datas.COLS3, 'COLS3']);
	lo_arr.push([datas.COLS4, 'COLS4']);
	lo_arr.push([datas.COLS5, 'COLS5']);
	lo_arr.push([datas.COLS6, 'COLS6']);
	lo_arr.push([datas.COLS7, 'COLS7']);
	lo_arr.push([datas.COLS8, 'COLS8']);
	lo_arr.push([datas.COLS9, 'COLS9']);
	lo_arr.push([datas.COLS10, 'COLS10']);
	for(var i = 0; i < lo_arr.length; i++){
		
		if(lo_arr[i][1] != a_col && lo_arr[i][0] != 0 && lo_arr[i][0] == a_newvalue){
			_X.MsgBox('확인', '동일한 데이타가 있습니다.');
			a_dg.SetItem(a_row, a_col, a_oldvalue);
			return;
		}
	}
}

function xe_GridDataLoad(a_dg){

}

function xe_EditChanged(a_obj, a_val, a_label, a_cobj){
	
	lb_first_load = true;
	setTimeout('x_DAO_Retrieve()', 0);
	
	if(a_obj.id == 'S_SALARY_DIV2'){
		var coS_APPLY_YYMM2 = _X.XmlSelect('hr', 'HR_PAYX_BASE_MASTER', 'HR_PAYX_BASE_MASTER_R01', new Array(_Caller._FindCode.findcode[0], a_val), 'json2');

		$("#S_APPLY_YYMM2").selectBox('destroy');
		
		if(typeof(coS_APPLY_YYMM2[0]) == 'undefined' || coS_APPLY_YYMM2[0] == null){
			coS_APPLY_YYMM2 = [{'code':'','label':'데이타없음'}];
			_X.DropDownList_SetData(S_APPLY_YYMM2, coS_APPLY_YYMM2, null, false, false, null);
		}else{
			_X.DropDownList_SetData(S_APPLY_YYMM2, coS_APPLY_YYMM2, '%', false, true, '전체');
		}
	
		$("#S_APPLY_YYMM2").selectBox();
	}
}

function xe_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){
	
}

//입력 컨트롤에서 Enter키가 눌러진 경우 호출
function xe_InputKeyEnter(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){

}

//xe_GridItemDoubleClick가 안되서...추가
function xe_M_RealGridItemDoubleClick(a_obj, cellIndex){

}

function xe_GridItemDoubleClick(a_dg, a_row, a_col){

}

function xe_GridButtonClick(a_dg, a_row, a_col, a_colname){
	
	a_dg.SetItem(a_row, a_col, '');
	
	if(a_dg.id=="dg_2" && a_colname == "SELECT_RETURN") {
		x_ReturnRowData(a_dg, a_row);
	}
}

function x_ReturnRowData(a_dg, a_row){

	if(typeof(xc_ReturnRowData)!="undefined"){xc_ReturnRowData(a_dg, a_row);return;}
	var rtValue = a_dg.GetRowData(a_row);
	//var rtValue = a_dg.GetItem(a_row,);

	if(_IsModal) {
		window.returnValue = rtValue;
	} else {
		if(_Caller) {			
			_Caller._FindCode.returnValue = rtValue;
			_Caller.x_ReceivedCode(rtValue);
			//_Caller._X.UnBlock();
		}
	}
	setTimeout('x_Close()',0);
}

function x_Confirm(){
	if (dg_2.GetRow()==0) return;
	x_ReturnRowData(dg_2, dg_2.GetRow());

}

function x_Close() {  
	
	var rtValue = dg_1.GetRowData(dg_1.GetRow());
	//var rtValue = a_dg.GetItem(a_row,);

	if(_IsModal) {
		window.returnValue = rtValue;
	} else {
		if(_Caller) {			
			_Caller._FindCode.returnValue = rtValue;
			_Caller.x_ReceivedCode(rtValue);
			//_Caller._X.UnBlock();
		}
	}
	
	if(_IsModal || _IsPopup) {
		window.close();
	} else {
		if(_Caller.$("#findmodal").length>0?_Caller.$("#findmodal").dialog("close"):window.close());
	}
}

function xe_GridLayoutComplete(a_dg){
	
	if(a_dg.id == "dg_1"){
		
//		a_dg.GridRoot().setStyles({
//	    body: {
//	    	dynamicStyles: [{
//	          criteria: [
//	              "value['ROW_DIV'] = '0'"
//	          ],
//	          styles: [
//	              "background=#11ff0000"
//	          ]
//	      }]
//	    }
//	  });
		
		dg_1.SetCombo('COLS1', coCOLS);
	  dg_1.SetCombo('COLS2', coCOLS);
	  dg_1.SetCombo('COLS3', coCOLS);
	  dg_1.SetCombo('COLS4', coCOLS);
	  dg_1.SetCombo('COLS5', coCOLS);
	  dg_1.SetCombo('COLS6', coCOLS);
	  dg_1.SetCombo('COLS7', coCOLS);
	  dg_1.SetCombo('COLS8', coCOLS);
	  dg_1.SetCombo('COLS9', coCOLS);
		dg_1.SetCombo('COLS10', coCOLS);
		setTimeout("x_DAO_Retrieve()",0);
		
		dg_1.GridReadOnly( true);
	}
}

﻿//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 : @프로그램코드|프로그램명
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
// 	성명					  소속                작성일
//
//===========================================================================================
//
//===========================================================================================

function x_InitForm2() {
	_X.InitGrid( {grid:dg_1, sqlId: "SM0920_01", shared:false, updatable:true} );
	_X.InitGrid( {grid:dg_2, sqlId: "SM0920_02", shared:false, updatable:true} );
	_X.InitGrid( {grid:dg_3, sqlId: "SM0920_03", shared:false, updatable:false} );
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted) {
	if ( ab_allcompleted ) {
		dg_2.SetCheckBar(true);
		dg_3.SetCheckBar(true);

		dg_2.SetCombo("USER_TAG", _X.XmlSelect("com", "COMMON", "BM_COMM_DETAIL" , new Array("03", '%', '%'), 'json2'));

		x_DAO_Retrieve(dg_1);
	}
}

function x_DAO_Retrieve2(a_dg){
	dg_1.Retrieve([mytop._CompanyCode]);
	//dg_3.Retrieve([mytop._CompanyCode,'%','%','%','%']);
	dg_3.Retrieve([mytop._CompanyCode, S_FIND.value]);
}

function xe_EditChanged2(a_obj, newValue, oldValue){

}

function xe_InputKeyEnter2(a_obj, event, ctrlKey, altKey, shiftKey){
	if(a_obj.id=="S_FIND") {
		dg_3.Retrieve([mytop._CompanyCode, S_FIND.value]);
	}

}

function xe_InputKeyDown2(a_obj, event, keyCode, ctrlKey, altKey, shiftKey){

}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

function x_DAO_Saved2(){

}

function x_DAO_Insert2(a_dg, rowIdx){
	return 100;
}

function x_Insert_After2(a_dg, rowIdx){
	if(a_dg.id=="dg_1") {
		dg_1.SetItem(rowIdx, "COMPANY_CODE", mytop._CompanyCode);
	}
}

function x_DAO_Duplicate2(a_dg, rowIdx){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){

}

function x_DAO_Delete2(a_dg){
	return 100;
}

function x_DAO_Deleted2(a_dg){

}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){

}

function xe_TabChanging2(a_tab, newIdx, oldIdx, newTab, oldTab){
	return 100;
}

function xe_TabChanged2(a_tab, newIdx, oldIdx, newTab, oldTab){

}

function xe_GridDataChange2(a_dg, rowIdx, colName, newValue, oldValue){

}

function xe_GridDataChanged2(a_dg, rowIdx, colName, newValue, oldValue){

}

function xe_GridRowFocusChange2(a_dg, newRowIdx, oldRowIdx){
  	if (a_dg.id == "dg_1" && dg_2.GetChangedData().length > 0 ) {
		if (_X.MsgBoxYesNo("확인", "변경된 데이타가 있습니다. 무시하고 작업을 계속 진행하시겠습니까?") !=1 ) return false;
  	}
	return true;
}

function xe_GridRowFocusChanged2(a_dg, newRowIdx){
	if(a_dg.id == "dg_1") {
		dg_2.Retrieve( [dg_1.GetItem(newRowIdx, "COMPANY_CODE"), dg_1.GetItem(newRowIdx, "ACT_CODE")] );
	}
}

function xe_GridItemFocusChange2(a_dg, newRowIdx, newColName, oldRowIdx, oldColName){

}

function xe_GridItemFocusChanged2(a_dg, newRowIdx, newColName){

}

function xe_BeforeGridDataLoad2(a_dg){
	return 100;
}

function xe_GridDataLoad2(a_dg){
	return 100;
}

function xe_GridButtonClick2(a_dg, rowIdx, colName){

}

function xe_GridHeaderClick2(a_dg, colName){

}

function xe_GridItemClick2(a_dg, rowIdx, colIdx, colName){

}

function xe_GridItemDoubleClick2(a_dg, rowIdx, colIdx, colName){
	if(a_dg.id == "dg_2" && rowIdx>0) {
		dg_2.DeleteRow(rowIdx);
	} else if(a_dg.id == "dg_3" ) {
		uf_user_add_do(rowIdx);
	}
}

function xe_GridBeforeKeyDown2(a_obj, event, keyCode, ctrlKey, altKey, shiftKey) {
	return 100;
}

function xe_GridKeyDown2(a_obj, event, keyCode, ctrlKey, altKey, shiftKey) {

}

function xe_FileButtonClick2(a_obj, rowIdx, colName, div){
	return 100;
}

function xe_FileChanged2(a_dg, rowIdx, colName, fileName, fileSize, fileType, firstData){
	return 100;
}

function xe_FileUploaded2(a_dg, uploadInfo){
	return 100;
}

function xe_FileDeleted2(a_dg, rowIdx, colName){
	return 100;
}

function x_Close2() {
	return 100;
}

function uf_user_del() {
	dg_2.DeleteCheckedRows();
	// var srows = dg_2.GetCheckedRows().reverse();
	// $(srows).each(function(idx,val){
	// 	dg_2.DeleteRow(val);
	// });
}

function uf_user_add() {
	var srows = dg_3.GetCheckedRows();
	$(srows).each(function(idx,val){
		uf_user_add_do(val);
	});
}


function uf_user_add_do(rowIdx) {
	var fRow = dg_2.Find(dg_3.GetItem(rowIdx, "EMP_NO"), ["USER_ID"]);

	if(fRow.row <=0) {
		var newRow = dg_2.InsertRow();
		dg_2.SetItem(newRow, "COMPANY_CODE",dg_1.GetItem(dg_1.GetRow(), "COMPANY_CODE"));
		dg_2.SetItem(newRow, "ACT_CODE", 	dg_1.GetItem(dg_1.GetRow(), "ACT_CODE"));
		dg_2.SetItem(newRow, "USER_ID", 	dg_3.GetItem(rowIdx, "EMP_NO"));
		dg_2.SetItem(newRow, "USER_NAME", 	dg_3.GetItem(rowIdx, "EMP_NAME"));
		dg_2.SetItem(newRow, "DEPT_NAME", 	dg_3.GetItem(rowIdx, "DEPT_NAME"));
		dg_2.SetItem(newRow, "USER_TAG", 	dg_3.GetItem(rowIdx, "USER_TAG"));
	}
}

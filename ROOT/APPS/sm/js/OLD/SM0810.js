_DelTrigger=true;

function x_InitForm2(){
	_X.InitGrid(grid_parent, "dg_parent", "100%", "100%", "sm", "SM0810", "XG_COMTC_CMMN_CODE|C_XG_COMTC_CMMN_CODE_01");
	_X.InitGrid(grid_child, "dg_child", "100%", "100%", "sm", "SM0810", "XG_COMTC_CMMN_CODE|C_XG_COMTC_CMMN_CODE_02");
	return 0;
}

//grid onload completed
function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted){
		setTimeout('x_DAO_Retrieve(dg_parent)',0);
	}
	return 0;
}

//자료조회
function x_DAO_Retrieve2(a_dg){
	param = new Array(S_FIND.value == "" ? '%' : S_FIND.value);
	dg_parent.Retrieve(param);
	return 0;
}

//html input type 의 자려가 변경된 경우 발생
function xe_EditChanged2(a_obj, a_val){	return 100;}

//enter key event 발생시 호출
function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	switch(a_obj.id) {
	  case "S_FIND":
	  	x_DAO_Retrieve(dg_parent);
	  	break;
	}
	return 0;
}

//keydown event 발생시 호출
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){return 100;}

//데이타 저장
function x_DAO_Save2(a_dg){return 100;}

//데이타 저장후 callback
function x_DAO_Saved2(){return 100;}

//데이타 유효성 check
function x_DAO_ChkErr2(){
	var sNo = 0;
	var chkMsg = "";

	var cData = dg_child.GetChangedData();
	if(cData==null) return true;
	for(var i=0; i<cData.length; i++){
		if(cData[i].job == "D") continue;	//삭제된것은 제외한다.
		sNo = cData[i].idx;
		sValue = cData[i].data["CODE"];
		if(!pf_dup(dg_child, sValue, sNo, 'GRID')){
			chkMsg = eval(sNo) + " 번째 행에 중복된 코드가 존재합니다";
			break;
		}
	}
	if(chkMsg!="") {
		_X.MsgBox('확인', chkMsg);
		dg_child.SetRow(sNo);
		dg_child.focus();
		return false;
	}
	return true;
}

//grid insert/add
function x_DAO_Insert2(a_dg, row){return 100;}

//grid insert 수행후 발생
function x_Insert_After2(a_dg, rowIdx){
	if(a_dg==dg_parent) {
		dg_parent.SetItem(rowIdx, "USE_AT", "Y");
		dg_parent.SetItem(rowIdx, "CL_CODE", "EFC");
	}
	return 0;
}

//grid 복제
function x_DAO_Duplicate2(a_dg, row){return 100;}

//grid 복제후
function x_Duplicate_After2(a_dg, rowIdx){return 100;}

//grid 삭제
function x_DAO_Delete2(a_dg, row){return 100;}

//excel 저장
function x_DAO_Excel2(a_dg){return 100;}

//자료조회
function x_DAO_Child_Retrieve2(a_dg){
	param = new Array(dg_parent.GetItem(dg_parent.GetRow(),'CODE_ID'));
	a_dg.Retrieve(param);
	return 0;
}

//grid child insert/add
function x_DAO_Child_Insert2(a_dg, row){return 100;}

//grid child insert 수행후 발생
//default 값 처리
function x_Child_Insert_After2(a_dg, rowIdx){
	dg_child.SetItem(rowIdx, "CODE_ID", dg_parent.GetItem(dg_parent.GetRow(), "CODE_ID"));
	dg_child.SetItem(rowIdx, "USE_AT", "Y");
	return 0;
}

//grid child 복제
function x_DAO_Child_Duplicate2(a_dg, row){return 100;}

//grid child 복제후
function x_Duplicate_Child_After2(a_dg, rowIdx){return 100;}

//grid child 삭제
function x_DAO_Child_Delete2(a_dg, row){return 100;}

//excel 저장
function x_DAO_Child_Excel2(a_dg){return 100;}

//출력
function x_DAO_Print2(){return 100;}

//tab 변경시
function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 1;}

//tab 변경시
function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}

//grid data 변경시
function xe_GridDataChange2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	if(a_dg.id == "dg_parent"){
		if(a_col == "CODE_ID"){
			var ls_hcode1 = a_dg.GetItem(a_row, "CODE_ID");
			for(var i=1; i<=a_dg.RowCount(); i++){
				if( i == a_row ){
					continue ;
				}
				ls_hcode2 = a_dg.GetItem(i, "CODE_ID");
				if( ls_hcode1 == ls_hcode2){
					a_dg.SetItem(a_row, "CODE_ID", "");
					_X.MsgBox("확인"," 중복된 데이터가 존재합니다.");
				  return;
				}
			}
		}
	}
	return 0;
}

//grid row focus change
function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){return true;}

//grid row focus change
function xe_GridRowFocusChanged2(a_dg, a_newrow){return 100;}


//item focus change event
function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){return true;}

//grid data loaded
function xe_GridDataLoad2(a_dg){return 100;}

//찾기창 호출 후
function x_FindCode2(a_dg, a_row, a_findKey, a_findstr, a_win) {return 100;}

//찾기창 호출 후
function x_ReceivedCode2(a_returnValue) {return 100;}

//grid item button click event
function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){return 100;}

//header click event
function xe_GridHeaderClick2(a_dg, a_col, a_colname){return 100;}

//item double click event
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){return 100;}

//tree item checked event
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){return 100;}

//tree item checked event
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){return 100;}

//- anychart  point click event
function xe_ChartPointClick2(a_dg, a_event){return 100;}

//- anychart  point select event
function xe_ChartPointSelect2(a_dg, a_event){return 100;}

//- anychart  point mouse over event
function xe_ChartPointMouseOver2(a_dg){return 100;}

//- anychart  point mouse out event
function xe_ChartPointMouseOut2(a_dg, a_event){return 100;}

//window(mdi) close
function x_Close2() {return 100;}

//중복체크
function pf_dup(a_dg, a_value, a_row, a_type) {
	if (a_type==null) a_type="SQL";
	
	if (a_type=="GRID") 
	{
		for(var i=1; i<a_dg.RowCount(); i++){
			if (a_dg.GetItem(i,"CODE_ID")==a_value&&i!=a_row) {
				return false;
			}	
		}
	} 
	return true;
}

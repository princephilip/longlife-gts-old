function x_InitForm2(){
	_X.InitGrid(grid_list, "dg_list", "100%", "100%", "sm", "SM0815", "XG_COMTN_BBS_USE|C_SM0815_01");
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM0815", "XG_COMTN_BBS_USE|C_SM0815_02");
	return 0;
}

//grid onload completed
function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted){
		setTimeout('x_DAO_Retrieve(dg_list)',0);
	}
	return 0;
}

//자료조회
function x_DAO_Retrieve2(a_dg){
	param = new Array(mytop._CompanyCode);
	dg_list.Retrieve(param);
	return 0;
}

//html input type 의 자려가 변경된 경우 발생
function xe_EditChanged2(a_obj, a_val){	return 100;}

//enter key event 발생시 호출
function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){return 100;}

//keydown event 발생시 호출
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){return 100;}

//데이타 저장
function x_DAO_Save2(a_dg){return 100;}

//데이타 저장후 callback
function x_DAO_Saved2(){return 100;}

//데이타 유효성 check
function x_DAO_ChkErr2(){
	var sNo = 0;
	var chkMsg = "";

	var cData = dg_1.GetChangedData();
	if(cData==null) return true;
	for(var i=0; i<cData.length; i++){
		if(cData[i].job == "D") continue;	//삭제된것은 제외한다.
		sNo = cData[i].idx;
		sValue = cData[i].data["USER_ID"];
		if(!pf_dup(dg_1, sValue, sNo, 'GRID')){
			chkMsg = eval(sNo) + " 번째 행에 중복된 코드가 존재합니다";
			break;
		}
	}
	if(chkMsg!="") {
		_X.MsgBox('확인', chkMsg);
		dg_1.SetRow(sNo);
		dg_1.focus();
		return false;
	}
	return true;
}

//grid insert/add
function x_DAO_Insert2(a_dg, row){return 100;}

//grid insert 수행후 발생
function x_Insert_After2(a_dg, rowIdx){
	if(a_dg==dg_1) {
		dg_1.SetItem(rowIdx, "COMPANY_CODE", mytop._CompanyCode);
		dg_1.SetItem(rowIdx, "BBS_ID", dg_list.GetItem(dg_list.GetRow(),"BBS_ID"));
		dg_1.SetItem(rowIdx, "TRGET_ID", dg_list.GetItem(dg_list.GetRow(),"TRGET_ID"));
	}
	return 0;
}

//grid 복제
function x_DAO_Duplicate2(a_dg, row){return 100;}

//grid 복제후
function x_Duplicate_After2(a_dg, rowIdx){return 100;}

//grid 삭제
function x_DAO_Delete2(a_dg, row){return 100;}

//excel 저장
function x_DAO_Excel2(a_dg){return 100;}

//출력
function x_DAO_Print2(){return 100;}

//tab 변경시
function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 1;}

//tab 변경시
function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}

//grid data 변경시
function xe_GridDataChange2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){return 100;}

//grid row focus change
function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	var lb_return = true;
	if(a_dg==dg_list){
		if (dg_1.IsDataChanged()) {
			if(_X.MsgBoxYesNo("변경된 자료가 있습니다 \r\n 계속 진행 하시겠습니까?")==1) {
				return lb_return;
			} else {
				return false;
			}
		}
	}
	return lb_return;
}

//grid row focus change
function xe_GridRowFocusChanged2(a_dg, a_newrow){
	if(a_dg==dg_list){
		if(a_newrow>0) {
			param = new Array(mytop._CompanyCode,dg_list.GetItem(dg_list.GetRow(),"BBS_ID"),dg_list.GetItem(dg_list.GetRow(),"TRGET_ID"));
			dg_1.Retrieve(param);
		} else dg_1.Reset();
	}
	return 100;
}


//item focus change event
function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){return true;}

//grid data loaded
function xe_GridDataLoad2(a_dg){return 100;}

//찾기창 호출 후
//찾기창 호출 후
function x_FindCode2(a_dg, a_row, a_findKey, a_findstr, a_win) {
		if(a_dg.GetRowState(a_row)!="I") return 0;
		switch(a_findKey) {
			case "USER_ID":
					_FindCode.grid_row = a_row;
					_X.CommonFindCode(window,"사용자찾기","com",a_findKey, a_findstr, new Array(mytop._CompanyCode), "FindUserId|FindCode|FindUserId",500, 500);
				break;
		}
	return 0;
}

//찾기창 호출 후
function x_ReceivedCode2(a_returnValue) {
	switch(_FindCode.finddiv){
		case "USER_ID":
			//그룹권한복사
			dg_1.SetItem(dg_1.GetRow(),"USER_ID",_FindCode.returnValue.USER_ID);
			dg_1.SetItem(dg_1.GetRow(),"USER_NAME",_FindCode.returnValue.USER_NAME);
			break;
	}
	return 0;
}

//gr
//grid item button click event
function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){return 100;}

//header click event
function xe_GridHeaderClick2(a_dg, a_col, a_colname){return 100;}

//item double click event
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){return 100;}

//tree item checked event
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){return 100;}

//tree item checked event
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){return 100;}

//- anychart  point click event
function xe_ChartPointClick2(a_dg, a_event){return 100;}

//- anychart  point select event
function xe_ChartPointSelect2(a_dg, a_event){return 100;}

//- anychart  point mouse over event
function xe_ChartPointMouseOver2(a_dg){return 100;}

//- anychart  point mouse out event
function xe_ChartPointMouseOut2(a_dg, a_event){return 100;}

//window(mdi) close
function x_Close2() {return 100;}

//중복체크
function pf_dup(a_dg, a_value, a_row, a_type) {
	if (a_type==null) a_type="SQL";
	
	if (a_type=="GRID") 
	{
		for(var i=1; i<a_dg.RowCount(); i++){
			if (a_dg.GetItem(i,"USER_ID")==a_value&&i!=a_row) {
				return false;
			}	
		}
	} 
	return true;
}

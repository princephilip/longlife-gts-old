﻿//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 : @PGM_CODE|PGM_NAME
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
// 
// 
//===========================================================================================
//	
//===========================================================================================

function x_InitForm2(){
		var comboData1 = _CB_YN;
		_X.DropDownList_SetData(S_USEYN, comboData1, 'Y', false, true, null);
		//obj, data, default, [code]label, %표기, ?
		
		var comboData2 = _X.XmlSelect("sm", "CodeCommon", "CODE_COM_LIST", new Array('CS','COST_CODE'), "json2");			//원가코드
		_X.DropDownList_SetData(COST_CODE, comboData2, '', false, true, '선택');
		
		var comboData3 = _X.XmlSelect("sm", "CodeCommon", "CODE_COM_LIST", new Array('HR','DEPT_DIV'), "json2");			//부서구분
		_X.DropDownList_SetData(DEPT_DIV, comboData3, '', false, true, '선택');
		
		var comboData4 = _CB_YN;
		_X.DropDownList_SetData(USING_TAG, comboData4, '', false, true, '');																					//사용여부
		//_X.DDLB_SetData(BUDGET_CONTROLTAG, comboData1, null, false, false, '');
		_X.DropDownList_SetData(BUDGET_CONTROLTAG, comboData4, null, false, false, '');
		
		var comboData5 = _X.XmlSelect("SM", "SM_CODE_OFFICE", "SM_CODE_OFFICE_R02", new Array('100'), "json2");				//세무사업장
		_X.DropDownList_SetData(TAX_COMPANY_CODE, comboData5, null, false, false, '');
		
		var comboData6 = [{"code":"01","label":"공통"},{"code":"02","label":"원가"}];																	//원가구분
		_X.DropDownList_SetData(COST_TAG, comboData6, null, false, false, '');
		
		var comboData7 = _X.XmlSelect("SM", "SM_CODE_OFFICE", "SM_CODE_OFFICE_R02", new Array('100'), "json2");				//사업장
		_X.DropDownList_SetData(OFFICE_CODE, comboData7, null, false, false, '');		
		
		var comboData8 = _X.XmlSelect("SM", "SM_CODE_TAXOFFICE", "SM_CODE_TAXOFFICE_R01", new Array('100'), "json2");	//관할세무서
		_X.DropDownList_SetData(TAX_OFFICE_CODE, comboData8, '', false, true, '선택');		
		
		var comboData9 = _X.XmlSelect("sm", "CodeCommon", "CODE_COM_LIST", new Array('HR','AREA_DIV'), "json2");			//지역구분
		_X.DropDownList_SetData(AREA_DIV, comboData9, '', false, true, '선택');		
		
		var comboData10 = _X.XmlSelect("sm", "CodeCommon", "CODE_COM_LIST", new Array('HR','FC_DEPT'), "json2");			//해외현장구분
		_X.DropDownList_SetData(FC_DEPT, comboData10, '', false, true, '선택');				
		
		var comboData11 = _X.XmlSelect("sm", "CodeCommon", "CODE_COM_LIST", new Array('AC','MANUFACDIV'), "json2");		//제조구분
		_X.DropDownList_SetData(MANUFAC_DIV, comboData11, '', false, true, '선택');
		
		var comboData12 = _X.XmlSelect("sm", "CodeCommon", "CODE_COM_LIST", new Array('HR','NATION_CD'), "json2");		//국가코드
		_X.DropDownList_SetData(NATION_CD, comboData12, '', false, true, '선택');
		
		var comboData13 = _X.XmlSelect("sm", "CodeCommon", "CODE_COM_LIST", new Array('HR','DEPT_TYPE_DIV'), "json2");		//공사종류
		_X.DropDownList_SetData(DEPT_TYPE_DIV, comboData13, '', false, true, '선택');
		
		var comboData14 = [{"code":"01","label":"본실"},{"code":"02","label":"부실"}];																		//본부실구분
		_X.DropDownList_SetData(BONBOO_CODE, comboData14, '', false, true, '선택');
		
		var comboData15 = [{"code":"01","label":"본사부서"},{"code":"02","label":"본사팀"},{"code":"03","label":"현장"}];	//본부실구분
		_X.DropDownList_SetData(SIL_CODE, comboData15, '', false, true, '선택');
		
		//var comboData16 = _CB_YN;																																													//조직표기구
		//_X.DropDownList_SetData(ORG_DISP_YN, comboData16, '', false, true, '선택');		
		
		var comboData16 = _X.XmlSelect("sm", "CodeCommon", "CODE_COM_LIST", new Array('SM','TAX_DIV'), "json2");					//과세구분
		_X.DropDownList_SetData(TAX_DIV, comboData16, '', false, true, '선택');
		
		var comboData17 = _CB_YN;																																													//식대통장별도구분
		_X.DropDownList_SetData(FOOD_DEPOSIT_YN, comboData17, '', false, true, '선택');
		
		var comboData18 = _X.XmlSelect("sm", "CodeCommon", "CODE_COM_LIST", new Array('HR','DEPT_GROUP'), "json2");				//사업부코드
		_X.DropDownList_SetData(DEPT_GROUP, comboData15, '', false, true, '선택');
		
		var comboData19 = [{"code":"01","label":"A사업"},{"code":"02","label":"B사업"},{"code":"03","label":"C사업"}];	//총괄사업코드
		_X.DropDownList_SetData(BUSINESS_CODE, comboData19, '', false, true, '선택');
		
		_X.InitGrid(grid_1, "dg_1", "100%", "100%", "SM", "SM011300", "SM_CODE_DEPT|SM_CODE_DEPT_C01");
		
		$("#btn_upperdeptcodefind	").bind("click", function(){_X.FindDeptCode(window,"S_SLIP_DEPT_CODE","", new Array("%"))});
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) x_DAO_Retrieve(dg_1);
}

function x_DAO_Retrieve2(a_dg){
	var as_compcode = '100';
	var as_deptcode = S_DEPTCODE.value;
	var as_useyn = S_USEYN.value;

	var param = new Array(as_compcode,as_deptcode, as_useyn);
	dg_1.Retrieve(param);
}

function xe_EditChanged2(a_obj, a_val){
	if(a_obj == S_USEYN || a_obj == S_DEPTCODE){
		//alert(a_val+';'+a_label+';'+a_cobj);
		x_DAO_Retrieve();
	}
	
	alert(a_obj.id+';'+a_val);
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){}
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

function x_DAO_Saved2(){

}

function x_DAO_Insert2(a_dg, row){
	return 100;
}

function x_Insert_After2(a_dg, rowIdx){
   if(a_dg.id == 'dg_1'){
   	 alert(1);
   	 dg_1.SetItem(rowIdx, 'Company_code', mytop._CompanyCode);
   	 alert(2);
   }                                 
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){

}

function x_DAO_Delete2(a_dg){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){

}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 1;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){

}

function xe_GridDataChange2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	//alert("xe_GridDataChange(" + a_row + " , " + a_col + ") : " + a_newvalue);
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){

}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){

}

function xe_GridDataLoad2(a_dg){
	//if(_S_Date !=null) {
	//		_E_Date = new Date();
	//		S_PROCESSTIME.value = (_E_Date - _S_Date)/1000;
	//}
  //x_DAO_Retrieve();
}

function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){
}

function x_ReceivedCode2(a_retVal){
	
}
	
function xe_GridHeaderClick2(a_dg, a_col, a_colname){

}

function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
	//alert("xe_GridItemDoubleClick(" + a_row + ", " + a_col + ") " + a_colname);
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){

}

function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){

}

function xe_ChartPointClick2(a_dg, a_event){

}

function xe_ChartPointSelect2(a_dg, a_event){

}

function xe_ChartPointMouseOver2(a_dg){

}

function xe_ChartPointMouseOut2(a_dg, a_event){

}

function x_Close2() {
	return 100;
}

function pf_Find(a_obj, a_value){
	var options = "{fields = ['DEPT_CODE', 'DEPT_NAME'],values = ['"+a_value+"', '"+a_value+"']};"
	alert(options);
	var ls_result = dg_1.search(options);
	alert(ls_result)
	
}
﻿function x_InitForm(){
		xbtns.innerHTML = "<span style='width:100%;height:100%;vertical-align:middle;'>"
									+ _X.Buttons(_PGM_CODE)
									+ "</span>"	;
		dgtab1.innerHTML = "<a href='javascript:pf_TabChanged(1)'>관련테이블</a> <a href='javascript:pf_TabChanged(2)'>작업자</a>";

		_X.InitGrid(grid_1, "dg_1", "99%", "99%", "SM", "SM011900", "SM_COMCODE_D|SM011900_dg_1");									
		_X.InitGrid(grid_2, "dg_2", "99%", "99%", "SM", "SM011900", "SM_COMCODE_D|SM011900_dg_1");
		_X.InitGrid(grid_3, "dg_3", "99%", "99%", "SM", "SM011900", "SM_COMCODE_D|SM011900_dg_1");
		_X.InitGrid(grid_4, "dg_4", "99%", "99%", "SM", "SM011900", "SM_COMCODE_D|SM011900_dg_1");
}

function x_ReceivedCode(){
}

function x_DAO_Retrieve(){
	var param = new Array('MCPAUTH','SM', 'Y', '3');
	dg_1.Retrieve(param);
}

function x_DAO_Save(){
}

function x_DAO_Insert(row){
	dg_1.InsertRow(row);
}

function x_DAO_Delete(row){
	dg_1.DeleteRow(row);
}

function x_DAO_Duplicate(row){
	dg_1.DuplicateRow(row);
}

function x_Duplicate_After(obj, rowIdx){
	if(obj==dg_1) {
		dg_1.SetItem(rowIdx, "SLIP_DATE", null);
	}
}

function x_DAO_Excel(){
	//alert(dg_1.RowCount());
	dg_1.ExcelExport();
}

function x_DAO_Print(){
	dg_1.SetItem(2,"REMARK1","적용변경 001");
	dg_1.SetItem(3, 12,"적용변경 002");
	dg_1.SetItem(4,"SLIP_DATE", new Date());
	dg_1.SetItem(5,"REMARK1", new Date());
	
}

function xe_GridLayoutComplete(a_obj){
}

function xe_EditChanged(a_obj, a_val, a_label, a_cobj)
{
	//if(a_obj == S_TO_DATE){
	//	x_DAO_Retrieve();
	//}
}

function xe_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey)
{
}

function xe_GridRowFocusChange(a_obj, a_newrow, a_oldrow)
{
	//alert(a_obj.id + " : " + a_newrow + "," + a_oldrow);
	//옆에꺼 해야지.
	return true;
}

function xe_GridItemFocusChange(a_obj, a_newrow, a_newcol, a_oldrow, a_oldcol)
{
	//alert(a_obj.id + " : " + a_newrow + "," + a_newcol + " : " + a_oldrow + "," + a_oldcol);
	return true;
}

function x_Close() {
	_X.CloseSheet(window);
}



function xe_GridOnLoad(a_obj){
}

function xe_GridDoubleClick(a_obj, a_event){

}

function xe_GridItemClick(a_obj, a_row, a_col, a_colname){
	//alert("xe_GridItemClick(" + a_row + ", " + a_col + ") " + a_colname);
}

function xe_GridItemDoubleClick(a_obj, a_row, a_col, a_colname){
	//alert("xe_GridItemDoubleClick(" + a_row + ", " + a_col + ") " + a_colname);
}

function xe_GridButtonClick(a_obj, a_row, a_col, a_colname){
	//alert("xe_GridItemClick(" + a_row + ", " + a_col + ") " + a_colname);
}

function xe_GridHeaderClick(a_obj, a_col, a_colname){
}

function xe_GridContextMenuItemSelect(a_obj, a_label, a_row, a_col, a_colname){
	alert("xe_GridContextMenuItemSelect(" + a_label + "," + a_row + ", " + a_col + ") " + a_colname);
}

function xe_GridMenuItemSelect(a_obj, a_label, a_checked, a_tag){
	alert("xe_GridMenuItemSelect(" + a_label + "," + a_checked + ", " + a_tag + ") ");
}

function xe_GridScrollToBottom(a_obj){
	//alert("xe_GridScrollToBottom(" + a_obj.id + ")");
}



function xe_GridKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey)
{
	alert('xe_GridKeyDown ' + a_obj.id + " : " + a_keyCode + a_ctrlKey + " : " +  a_altKey + " : " +  a_shiftKey);
}

function xe_GridKeyUp(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey)
{
	alert('xe_GridKeyUp ' + a_obj.id + " : " + a_keyCode + a_ctrlKey + " : " +  a_altKey + " : " +  a_shiftKey);
}

function xe_GridDataChange(a_obj, a_row, a_col, a_newvalue, a_oldvalue){
	//alert("xe_GridDataChange(" + a_row + " , " + a_col + ") : " + a_newvalue);
}

function itemEditEndChecker(rowIndex, columnIndex, item, dataField, oldValue, newValue) {
}

function itemEditBeginningChecker(rowIndex, columnIndex, item, dataField) {
	//입력모드가 아닌경우 편집 불가
	return true;
}


function xe_GridSearchChange(a_obj){

}

function xe_GridDataLoad(a_obj){
	if(_S_Date !=null) {
		_E_Date = new Date();
		S_PROCESSTIME.value = (_E_Date - _S_Date)/1000;
	}
  //x_DAO_Retrieve();
}

function xe_GridItemIconClick(a_obj, a_RowIndex, a_ColumnIndex){
}

function pf_TabChanged(a_idx){

	if(a_idx == 1){
		//dg_3.setVisible(true);
		grid_3.style.display = 'block';
		
		//dg_4.setVisible(false);
		grid_4.style.display = 'none';
				
		//_X.InitGrid(grid_3, "dg_3", "99%", "99%", "SM", "SM011900", "SM_COMCODE_D|SM011900_dg_1");
	}else if(a_idx == 2){
		//dg_3.setVisible(false);
		grid_3.style.display = 'none';
				
		//dg_4.setVisible(true);
		grid_4.style.display = 'block';
		//_X.InitGrid(grid_3, "dg_4", "99%", "99%", "SM", "SM011900", "SM_COMCODE_D|SM011900_dg_1");
	}
}
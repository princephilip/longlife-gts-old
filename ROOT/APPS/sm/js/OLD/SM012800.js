﻿//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : @SM012800|카렌더관리
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
// SKLEE  				 X-INTERNETINFO			 20140724   
//
//========================================================================================
//
//========================================================================================

var ibGridBorder=true;

function x_InitForm(){
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM012800", "SM_CODE_CALENDAR|SM_CODE_HOLYBASE_C01",false,false,"HOLY_DATE|MOON_TAG");
	_X.InitGrid(grid_2, "dg_2", "100%", "100%", "sm", "SM012800", "SM_CODE_CALENDAR|SM_CODE_HOLYDATE_C01",false,false,"HOLY_DATE|HOLY_DAY");
}

function xe_GridLayoutComplete(a_dg, a_allcompleted){
	if(a_allcompleted) {
		if(a_dg=dg_2)	_CurGrid = a_dg;
		x_DAO_Retrieve();
		dg_2.setFocus();
	}
}

function x_DAO_Retrieve(){
	if(x_IsAllDataChanged()>0) {
			if(_X.MsgBoxYesNo("확인","추가/변경중인 데이타가 있습니다.\n조회 시 변경중인 데이타가 초기화 됩니다.\n\n계속 진행 하시겠습니까?") == 1)  {
			x_SaveGridData();
		}
	}
	dg_1.Retrieve();
	param = new Array(top._CompanyCode, _X.StrPurify(sle_basicyear.value));
	dg_2.Retrieve(param)
}

function xe_GridRowFocusChange(a_dg, a_newrow, a_oldrow){
}

function xe_GridDataLoad(a_dg){}

function x_DAO_Save(a_dg){
	if(pf_errCheck(a_dg) > 0) return;
	x_SaveGridData();
}

function x_DAO_Saved(){}

function pf_errCheck(a_dg){
	return 0;
}

function x_DAO_Insert(a_dg, row){
	_CurGrid.InsertRow(0);
}

function x_Insert_After(a_dg, rowIdx){
	if(a_dg != null && a_dg.id == "dg_1") {
		a_dg.SetItem(rowIdx, 'MOON_TAG','N');
		a_dg.SetItem(rowIdx, 'PRE_DAYS',0);
		a_dg.SetItem(rowIdx, 'NEXT_DAYS',0);
		
	} else {
		a_dg.SetItem(rowIdx, 'COMPANY_CODE',top._CompanyCode);
		a_dg.SetItem(rowIdx, 'HOLY_YEAR',sle_basicyear.value);
	}
}

function x_DAO_Delete(a_dg, row){
	if(_X.MsgBoxYesNo('선택된 데이타를 삭제 하시겠습니까?') == 2) return;
	_CurGrid.DeleteRow(row);
}

function x_DAO_Duplicate(a_dg, row){
	_CurGrid.DuplicateRow(row);
}

function x_Duplicate_After(a_dg, rowIdx){}

function x_DAO_Excel(a_dg){
	_CurGrid.ExcelExport();
}

function x_DAO_Print(a_dg){
	_X.MsgBox("인쇄 개발중");	
}

function x_Close() {
	if(x_IsAllDataChanged()>0) {
		if(_X.MsgBoxYesNo("추가/변경중인 데이타가 있습니다.\n\n자료를 저장 하시겠습니까?") == 1)  x_SaveGridData();
	}
	_X.CloseSheet(window);
}

function xe_EditChanged(a_obj, a_val, a_label, a_cobj){
	switch (a_obj.id) {
		case "sle_basicyear":
			x_DAO_Retrieve();
			break;
	}
}
function xe_InputKeyEnter(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){}
function xe_TabChanged(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){}
function xe_GridItemClick(a_dg, a_row, a_col, a_colname){}
function xe_GridDataChange(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	if(a_col=="HOLY_DAY") {
			dg_2.SetItem(a_row,"HOLY_DATE",sle_basicyear.value+a_newvalue);
	}
}
function xe_GridButtonClick(a_dg, a_row, a_col, a_colname){}

function pf_create_HolyDate()	{
	if(_X.MsgBoxYesNo(sle_basicyear .value + " 년의 공휴일 기초자료를 생성 하시겠습니까?")==2) return;
	rc = _X.ExecProc("sm", "SP_SM_CODE_HOLYDATE", new Array(top._CompanyCode,sle_basicyear.value));
	if(rc>0) x_DAO_Retrieve();
}

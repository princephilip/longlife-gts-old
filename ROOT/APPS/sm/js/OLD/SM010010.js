//var _Theme = getTheme();

//화면 디자인관련 요소들 초기화 작업
function x_InitForm()
{
	xsearch.innerHTML = _X.SearchLabel("센터명")
										+ _X.TextInput("ITEM_SEARCH");						

	xbtns.innerHTML = _X.Buttons(_PGM_CODE);
									
	_X.Grid(grid_col_1, "dg_1", "100%", "100%", "sm", "SM010010", "CenterInfo|CENTER_INFO_E01");	
	_X.Grid(grid_col_2, "dg_2", "100%", "100%", "sm", "SM010010_1", "FundInfo|FUND_INFO_E01");	
	
	rMateGridInit();
	
	_X.DDLB_SetData(REPORT_DIV, _CB_REPORT_DIV, null, false, false, '');
	
	var comboData1 = _X.XmlSelect("sm", "CodeCom", "CODE_COMC_D01", new Array('CALC_MM_DIV'), "json2");
	
	_X.DropDownList_SetData(CALC_SM_DIV, comboData1, null, false, false, '선택');
	_X.DropDownList_SetData(CALC_EM_DIV, comboData1, null, false, false, '선택');
	_X.DropDownList_SetData(ACCPAY_M_DIV, comboData1, null, false, false, '선택');
	
	// 프리폼 비활성화
	$("#freefrom").attr("disabled", true); 
}


function x_ReceivedCode(){
	switch(_FindCode.finddiv){
		case "POST_CODE":
			POST_CODE.value = _FindCode.returnValue.POST_CODE;
			ADDRESS1.value = _FindCode.returnValue.ADDR;
			
			dg_1.setItem(dg_1.getRow(), "POST_CODE", _FindCode.returnValue.POST_CODE); 
		  dg_1.setItem(dg_1.getRow(), "ADDRESS1", _FindCode.returnValue.ADDR); 
			
			//xm_InputSyncGrid(POST_CODE);
			//xm_InputSyncGrid(ADDRESS1);
			$("#ADDRESS2").focus();
			break;
			
		case "BANK_CODE":
		  dg_1.setItem(dg_1.getRow(), "BANK_CODE", _FindCode.returnValue.BANK_CODE); 
		  dg_1.setItem(dg_1.getRow(), "BANK_NAME", _FindCode.returnValue.BANK_NAME); 
		  
			BANK_CODE.value = _FindCode.returnValue.BANK_CODE;
			BANK_NAME.value = _FindCode.returnValue.BANK_NAME;
			break;	
	}
	
}

function x_DAO_Retrieve()
{
	dg_1.retrieve(new Array('%' + ITEM_SEARCH.value + '%'));	
	
	setTimeout("uf_freefrom()", 1000);
}

function uf_freefrom(){
	var ls_cnt = dg_1.rowCount();
	if(ls_cnt == 0){ 
		$("#freefrom").attr("disabled", true); 
	}else{
		$("#freefrom").attr("disabled", false); 
	}
}

function x_DAO_Save()
{
	
	var errRow = pf_errCheck('dg_1');
	if (errRow>0) {
		dg_1.setRow(errRow);
	  dg_1.focus();
		return;
	}
	
	var errRow = pf_errCheck('dg_2');
	if (errRow>0) {
		dg_2.setRow(errRow);
	  dg_2.focus();
		return;
	}
	
		if (dg_1.DataGrid().getItemEditorInstance() != null)
		dg_1.DataGrid().setEditedItemPosition(null);
		if (dg_2.DataGrid().getItemEditorInstance() != null)
		dg_2.DataGrid().setEditedItemPosition(null);

	setTimeout("x_SaveGridData()",100);
}

function pf_errCheck(a_tag)
{
	
	var sArr  = new Array();
	var sNo = 0;
	var chkRow = 0;
	var chkMsg = "";
	
	if(a_tag == 'dg_1'){
		var cData = dg_1.GridRoot().getChangedData();
	  if(cData==null || cData=="")	return;
		
	}else{
		var cData = dg_2.GridRoot().getChangedData();
	   if(cData==null || cData=="")	return;
		var ls_center_code =  dg_1.getItem(dg_1.getRow(), "CENTER_CODE");
		var ls_seq = _X.XmlSelect("sm", "FundInfo", "FUND_INFO_S01", new Array(ls_center_code), "array");
	}
	

	for(var i=0; i<cData.length; i++){
		sNo = cData[i].idx;
		
		if(a_tag == 'dg_1'){
			
			if (cData[i].data["CENTER_CODE"]=="") {
				chkMsg = eval(sNo+1) + " 번째 행의 센터코드는 필수 항목입니다";
				break;
			}
			
			if (cData[i].data["CENTER_NAME"]=="") {
				chkMsg = eval(sNo+1) + " 번째 행의 센터명은 필수 항목입니다";
				break;
			}
			
			
			if (cData[i].job=="I") {
				if (pf_dup(cData[i].data["CENTER_CODE"])==false) 
				{
					chkMsg = eval(sNo+1) + " 번째 행에 중복된 센터코드가 존재합니다";
					break;
				}
			}
		}else{
			if (cData[i].data["SEQNO"]==""||cData[i].data["SEQNO"]==null) {
				ls_seq++;
				dg_2.setItem(sNo+1, "SEQNO", Number(ls_seq));
			}
		}
	}
	
	if(chkMsg!="") _X.MsgBox('확인', chkMsg);
	return (chkMsg==""?0:sNo+1);
}
function pf_dup(a_value, a_row, a_type) {
	if (a_type==null) a_type=="SQL";
	
	if (a_type=="GRID") 
	{
		for(var i=1; i<dg_1.rowCount(); i++){
			if (dg_1.getItem(i,"CENTER_CODE")==a_value&&i!=a_row) {
				return false;
			}	
		}
	} else 
	{
		sData = _X.XmlSelect("sm", "CenterInfo", "CENTER_INFO_S01", new Array(a_value), "array");
		
		
		if(sData==a_value) return false;
	}
	return true;
}

function pf_dup_fund(a_value, a_row, a_type) {
	if (a_type==null) a_type=="SQL";
	
	if (a_type=="GRID") 
	{
		for(var i=1; i<dg_1.rowCount(); i++){
			if (dg_1.getItem(i,"FUND_DIV")==a_value&&i!=a_row) {
				return false;
			}	
		}
	} else 
	{
		sData = _X.XmlSelect("sm", "FundInfo", "FUND_INFO_S02", new Array(a_value), "array");
		
		
		if(sData==a_value) return false;
	}
	return true;
}

function x_DAO_Insert(row)
{
	
	$("#freefrom").attr("disabled", false); 
	
	if(dg_1.rowCount() < 1) return;
	
	dg_2.insertRow(row);
	
	var insRow = dg_2.rowCount();
	dg_2.SetItem(insRow, "GR_MODE", "I");
	//dg_2.SetItem(insRow, "FUND_DIV", "1000");
	dg_2.setItem(insRow, "CENTER_CODE_D", dg_1.getItem(dg_1.getRow(), "CENTER_CODE")); 
}

function x_DAO_Delete(row)
{
	dg_2.deleteRow(row);
}

function x_DAO_Duplicate(row){
	dg_1.duplicateRow(row);
}

function x_Duplicate_After(obj, rowIdx){
	if(obj==dg_1)	{
		obj.setItem(rowIdx, "CENTER_CODE", "");
	}
}

function x_DAO_Excel(){
	dg_1.excelExport();
}

function xe_GridLayoutComplete(a_obj){
	
	//Combo 데이타 설정
	if(a_obj == dg_1){
		dg_1.setCombo("REPORT_DIV", _CB_REPORT_DIV);	//신고유형
		
		var comboData1 = _X.XmlSelect("sm", "CodeCom", "CODE_COMC_D01", new Array('CALC_MM_DIV'), "json2");
		var comboData = _X.XmlSelect("com", "FindCode", "BankCodeList", new Array(''), "json2")
		dg_1.setCombo("BANK_CODE", comboData);
		
		dg_1.setCombo("CALC_SM_DIV", comboData1);	
		dg_1.setCombo("CALC_EM_DIV", comboData1);	
		dg_1.setCombo("ACCPAY_M_DIV", comboData1);	
	}
	
	if(a_obj == dg_2){
		var comboData1 = _X.XmlSelect("sm", "CodeCom", "CODE_COMC_D01", new Array('FUND_DIV'), "json2");
		var comboData2 = _X.XmlSelect("sm", "CodeCom", "CODE_COMC_D01", new Array('TERM_DIV'), "json2");
		dg_2.setCombo("FUND_DIV", comboData1);	
		dg_2.setCombo("TERM_DIV", comboData2);	
	}
}

function xe_EditChanged(a_obj, a_val, a_label, a_cobj)
{

}

function xe_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey)
{
	if(a_obj == ITEM_SEARCH && a_keyCode==KEY_ENTER){
		x_DAO_Retrieve();
	}
	
}



function x_Close() {
	_X.CloseSheet(window);
}

function itemEditEndChecker(rowIndex, columnIndex, item, dataField, oldValue, newValue) {
	
	if (dataField == "CENTER_CODE") {
		if (newValue.length > 4){
		  return "문자 길이는 5보다 작아야 합니다.";
		}
		if (pf_dup(newValue,rowIndex+1,"SQL")==false) {
		  return "중복된 센터코드가 존재합니다";
		}   
	}
	
	
	if (dataField == "ESTAB_YEAR") {
		if (newValue.length > 4){
		  return "설립년도 길이는 5보다 작아야 합니다.";
		}  
	}
	
	if (dataField == "CALC_S_DATE") {
		if (newValue.length > 2){
		  return "계산시작일 길이는 3보다 작아야 합니다.";
		}  
	}
	
	if (dataField == "CALC_E_DATE") {
		if (newValue.length > 2){
		  return "계산종료일 길이는 3보다 작아야 합니다.";
		}  
	}
	
	if (dataField == "ACC_DATE") {
		if (newValue.length > 2){
		  return "고지일자 길이는 3보다 작아야 합니다.";
		}  
	}
	
	if (dataField == "ACCPAY_DATE") {
		if (newValue.length > 2){
		  return "납부기한 길이는 3보다 작아야 합니다.";
		}  
	}
	
	if (dataField == "MAIN_EMAIL") {
		if (!validateEmail(newValue)){
		  
		  return "이메일 주소의 형식이 올바르지 않습니다.";
		}  
	}	
	
	if (dataField == "FUND_DIV") {
	
		if (pf_dup_fund(newValue,rowIndex+1,"SQL")==false) {
		  return "중복된 기금코드가 존재합니다";
		}   
	}
	
}

function itemEditBeginningChecker(rowIndex, columnIndex, item, dataField) {
	if (dg_1.GetItem(rowIndex+1,"GR_MODE")=="U"&&dataField=="CENTER_CODE") 	return false;
	return true;
}

function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function xe_GridRowFocusChange(a_obj, a_row, a_col)
{
	if(a_obj==dg_1) {
		if(dg_1.rowCount()>0) pf_ChildRetrieve(a_obj,a_row);
	}
}

function pf_ChildRetrieve(a_obj, a_row)
{
	if(a_obj == dg_1){
		dg_2.reset();
		var ls_center_code = a_obj.getItem(a_row, "CENTER_CODE");
		dg_2.retrieve(new Array(ls_center_code));	
	}
}

function xe_GridDataLoad(a_obj){
  if(a_obj==dg_1) {
		if(dg_1.rowCount()>0) pf_ChildRetrieve(a_obj,1);
	}
}
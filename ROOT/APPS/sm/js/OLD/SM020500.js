//화면 디자인관련 요소들 초기화 작업
function x_InitForm()
{
	_X.InitGrid(grid_list, "dg_list", "100%", "100%", "sm", "SM020500", "SM_AUTH_MENU_GROUP|SM_AUTH_MENU_GROUP_R02");
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM020500", "SM_AUTH_USER_MENU|SM_AUTH_USER_MENU_C02",false,false,"MENU_AUTH|SYS_ID|PGM_CODE");
}

//찾기창 호출 후
function x_ReceivedCode(){
	switch(_FindCode.finddiv){
		case "MENU_AUTH":
			//권한 복사
			pf_AuthCopy(_FindCode.returnValue.MENU_AUTH);
			break;
	}
}

function xe_GridOnLoad(a_dg)
{
}

function x_DAO_Retrieve(a_dg)
{
	var param = new Array(S_PGM_CODE.value);
	dg_list.Retrieve(param);
}

//data loaded
function xe_GridDataLoad(a_dg) {
}
	

function x_DAO_Save(a_dg)
{
	var errRow = pf_errCheck();
	if (errRow>0) {
		dg_list.SetRow(errRow);
	  dg_list.focus();
		return;
	}
	setTimeout("x_SaveGridData()",0);
}

function x_DAO_Insert(a_dg, row)
{
	
	pf_PgmFind();
	//a_dg.InsertRow(row);
}

function x_DAO_Delete(a_dg, row)
{
	var rows = a_dg.GetCheckedRows();
	var delcnt = 0;
	var dRow = 0;
	if(rows.length>0) 
	{
		for(var i=0; i<rows.length; i++){
			dRow = parseInt(rows[i])+1;
			a_dg.DeleteRow(dRow-delcnt);
			delcnt++;
		}
	} else {
		a_dg.DeleteRow(row);
	}
}

function x_DAO_Duplicate(a_dg, row){
	a_dg.DuplicateRow(row);
}

function x_Duplicate_After(a_dg, rowIdx){
}

function x_DAO_Excel(a_dg){
	a_dg.ExcelExport();
}

function xe_GridLayoutComplete(a_dg, a_allcompleted){
	if(a_dg.id=="dg_1") setTimeout("dg_1.SetCheckBar(true)",0);
}

function xe_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey)
{
}

//입력 컨트롤에서 Enter키가 눌러진 경우 호출
function xe_InputKeyEnter(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey)
{
	switch(a_obj.id) {
		case "S_PGM_CODE":
			pf_dg1_retrieve();
			break;
	}
}

function xe_EditChanged(a_obj, a_val, a_label, a_cobj)
{
	switch(a_obj.id)
	{
		case "S_SYS_ID":
			x_DAO_Retrieve();
			break;
		case "S_PGM_CODE":
		case "chk_AUTH_I":
		case "chk_AUTH_R":
		case "chk_AUTH_D":
		case "chk_AUTH_P":
			pf_SelectAll(_X.StrReplace(a_obj.id,"chk_",""),a_obj.checked);
			break;
	}
}

function xe_GridRowFocusChange(a_dg, a_row, a_col)
{
	if(a_dg.id=="dg_list") {
		pf_dg1_retrieve(a_row);
	}
}

function xe_GridItemDoubleClick(a_dg, a_row, a_col) 
{
	
}

function xe_GridItemClick(a_dg, a_row, a_col, a_colname)
{
}

function xe_GridItemIconClick(a_dg, a_row, a_col)
{
}

function x_Close() {
	_X.CloseSheet(window);
}

function pf_errCheck()
{
	var cData = dg_list.GetChangedData();

	var sArr  = new Array();
	var sNo = 0;
	var chkRow = 0;
	var chkMsg = "";

	if(cData==null || cData=="")	return;
	
	for(var i=0; i<cData.length; i++){
	}
	if(chkMsg!="") _X.MsgBox('확인', chkMsg);
	return (chkMsg==""?0:sNo+1);
}

function pf_dg1_retrieve(a_row) {
	if(a_row==null)
		a_row = dg_list.GetRow();
	if(a_row<=0)
		return;
				
	var menu_auth = dg_list.GetItem(a_row,'MENU_AUTH');
	dg_1.Retrieve(new Array(menu_auth, S_SYS_ID.value, S_PGM_CODE.value));
}

//전체선택/해제처리
function pf_SelectAll(a_column, a_flag)
{
	dg_1.GridCommit();
	
	for(var i = 1; i <= dg_1.RowCount(); i++)
	{
		dg_1.SetItem(i, a_column, (a_flag?'Y':'N'));
	}
}

function pf_AuthFind()
{
	var a_row = dg_list.GetRow();
	if(a_row<=0)
		return;
	var user_id = dg_list.GetItem(a_row,'MENU_AUTH');
	_X.FindMenuAuth(window,"MENU_AUTH","", new Array(user_id));
}

function pf_PgmFind()
{
	var a_row = dg_list.GetRow();
	if(a_row<=0)
		return;
	var user_id = dg_list.GetItem(a_row,'MENU_AUTH');
	_SM.FindPgmCode(window,"PGM_CODE","",new Array(user_id));
}

function pf_AuthCopy(auth_code)
{
	var a_row = dg_list.GetRow();
	if(a_row<=0)
		return;
	var user_id = dg_list.GetItem(a_row,'MENU_AUTH');
	if(_X.MsgBoxYesNo("원본 : [" + auth_code + "] 대상 : [" + user_id + "]<br>권한 복사작업을 진행 하시겠습니까?","",2)!=1) return;

	//권한설정 초기화 Procedure 실행
	_X.ExecProc("sm", "SP_SM_MenuAuthCopy", new Array(auth_code, user_id, top._ClientIP, top._UserID));	
	dg_1.Retrieve(new Array(user_id, S_SYS_ID.value, S_PGM_CODE.value));
}

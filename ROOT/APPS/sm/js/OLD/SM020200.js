//화면 디자인관련 요소들 초기화 작업
function x_InitForm()
{
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM020200", "SM_AUTH_PGMCODE|SM_AUTH_PGMCODE_C01",false,false,"SYS_ID|PGM_CODE");
}

//찾기창 호출 후
function x_ReceivedCode(){
	switch(_FindCode.finddiv){
		case "PGM_CODE":
			//소스 복사
			pf_SourceCopy(_FindCode.returnValue.PGM_CODE, _FindCode.returnValue.PGM_NAME);
			break;
	}
}

function xe_GridOnLoad(a_dg) 
{
}
	
function x_DAO_Retrieve()
{
	dg_1.Retrieve(new Array(S_SYS_ID.value,'%' + S_PGM_CODE.value + '%'));
}

//data loaded
function xe_GridDataLoad(a_obj) {
}
	

function pf_errCheck()
{
	var cData = dg_1.GetChangedData();

	var sArr  = new Array();
	var sNo = 0;
	var chkRow = 0;
	var chkMsg = "";

	if(cData==null || cData=="")	return;
	
	for(var i=0; i<cData.length; i++){
		if(cData[i].job == "D") continue;
		sNo = cData[i].idx;
		//중복체크...
		if (cData[i].job=="I") {
			if (pf_dupPGMCODE(cData[i].data["PGM_CODE"])==false) 
			{
				chkMsg = eval(sNo) + " 번째 행에 중복된 메뉴코드가 존재합니다";
				break;
			}
		}
	}
	if(chkMsg!="") _X.MsgBox('확인', chkMsg);
	return (chkMsg==""?0:sNo);
}

function x_DAO_Save(a_dg)
{
	var errRow = pf_errCheck();
	if (errRow>0) {
		dg_1.SetRow(errRow);
	  dg_1.focus();
		return;
	}
	setTimeout("x_SaveGridData()",0);
}

function x_DAO_Insert(a_dg, row)
{
	var newRow = a_dg.InsertRow(row);
	a_dg.SetItem(newRow, "SYS_ID", (S_SYS_ID.value=="%"?"":S_SYS_ID.value));
	a_dg.SetItem(newRow, "PGM_URL", "/XG010.do");	
}

function x_DAO_Delete(a_dg, row)
{
	a_dg.DeleteRow(row);
}

function x_DAO_Duplicate(a_dg, row){
	a_dg.DuplicateRow(row);
}

function x_Duplicate_After(a_dg, rowIdx){
	if(a_dg==dg_1)	{
		a_dg.SetItem(rowIdx, "PGM_CODE", "");
	}
}

function x_DAO_Excel(a_dg){
	a_dg.ExcelExport();
}

function xe_GridLayoutComplete(a_obj){
	var	comboData = _X.XmlSelect("sm", "CodeCom", "SM_AUTH_SYS_R01", new Array('SYS_DIV'), "json2");
	//Combo 데이타 설정
	dg_1.SetCombo("SYS_ID", comboData);			//시스템구분
}

function xe_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey)
{
}

//입력 컨트롤에서 Enter키가 눌러진 경우 호출
function xe_InputKeyEnter(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey)
{
	switch(a_obj.id) {
		case "S_PGM_CODE":
			//검색할 필드명을 배열에 넣는다
			dg_1.SetFocusByElement(a_obj, new Array("PGM_CODE", "PGM_NAME"));
			break;
	}
}


//필드값이 바뀐경우 
function xe_GridDataChange(a_obj, rowIndex, columnIndex, newValue, oldValue)
{
	switch(a_obj.GetColumnNameByIndex(columnIndex)) {
	case "PGM_CODE":
		a_obj.SetItem(rowIndex,"SORT_CODE",newValue);
		break;
	}
}


function xe_EditChanged(a_obj, a_val, a_label, a_cobj)
{
	
	switch(a_obj)
	{
		case S_SYS_ID:
    	x_DAO_Retrieve();
			break;
		case chk_SHOW_TAG:
		case chk_MOVE_BTN_TAG:
		case chk_UPDATE_TAG:
		case chk_RETRIEVE_TAG:
		case chk_INSERT_TAG:
		case chk_APPEND_TAG:
		case chk_DUPLICATE_TAG:
		case chk_DELETE_TAG:
		case chk_PRINT_TAG:
		case chk_EXCEL_TAG:
		case chk_CLOSE_TAG:
		case chk_WEB_TAG:
		case chk_DEVELOPED_TAG:
		case chk_HELP_TAG:
    	pf_SelectAll(_X.StrReplace(a_obj.id,"chk_",""),a_obj.checked);
			break;
	}
}

//전체선택/해제처리
function pf_SelectAll(a_column, a_flag)
{
	for(var i = 1; i <= dg_1.RowCount(); i++)
	{
		dg_1.SetItem(i, a_column, (a_flag?'Y':'N'));
	}
}

function xe_GridRowFocusChange(a_obj, a_row, a_col)
{
}

function xe_GridItemDoubleClick(a_obj, row, a_col) 
{
	
}


function xe_GridRowFocusChange(a_obj, a_oldrow, a_newrow)
{
}

function xe_GridItemIconClick(a_obj, a_row, a_col)
{
}

function x_Close() {
	_X.CloseSheet(window);
}

//메뉴코드 중복체크
function pf_dupPGMCODE(a_value, a_row, a_type) {

	if (a_type==null) a_type="SQL";
	
	if (a_type=="GRID") 
	{
		for(var i=1; i<dg_1.RowCount(); i++){
			if (dg_1.GetItem(i,"PGM_CODE")==a_value&&i!=a_row) {
				return false;
			}	
		}
	} else {
			sData = _X.XmlSelect("sm", "SM_AUTH_PGMCODE", "SM_AUTH_PGMCODE_R01", new Array(a_value), "array");
			if(sData==a_value) return false;
	}
	return true;
}

function pf_PgmFind()
{
	var a_row = dg_1.GetRow();
	if(a_row<=0)
		return;
	var pgm_code = dg_1.GetItem(a_row,'PGM_CODE');
	_X.FindPgmCode(window,"PGM_CODE","", new Array(pgm_code));
	//_X.FindCode(window,"프로그램코드찾기","com","", new Array(pgm_code), "PGM_CODE","FindPgmCode|FindCode|FindPgmCode",500, 650);
}

function pf_SourceCopy(pgm_code, pgm_name)
{
	var a_row = dg_1.GetRow();
	if(a_row<=0)
		return;
	var t_pgm_code = dg_1.GetItem(a_row,'PGM_CODE');
	var t_pgm_name = dg_1.GetItem(a_row,'PGM_NAME');

	if(_X.MsgBoxYesNo("원본 : [" + pgm_code + "] " + pgm_name + "<br>대상 : [" + t_pgm_code + "] " + t_pgm_name + "<br>소스 복사작업을 진행 하시겠습니까?","",2)!=1) return;
	if(_X.CallJsp("/Mighty/jsp/SourceCopy.jsp?fpgm=" + pgm_code + "&tpgm=" + t_pgm_code + "")) {
		_X.MsgBox("소스복사작업이 완료되었습니다.");
	}
}
//화면 디자인관련 요소들 초기화 작업
function x_InitForm()
{
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM020400", "SM_AUTH_MENU_GROUP|SM_AUTH_MENU_GROUP_C01",false,false,"MENU_AUTH");
}

//찾기창 호출 후
function x_ReceivedCode(){
}

function xe_GridOnLoad(a_dg) 
{
}
	
function x_DAO_Retrieve()
{
	dg_1.Retrieve(new Array('%' + S_MENU_GROUP.value + '%'));
}

//data loaded
function xe_GridDataLoad(a_obj) {
}
	

function pf_errCheck()
{
	var cData = dg_1.GetChangedData();

	var sArr  = new Array();
	var sNo = 0;
	var chkRow = 0;
	var chkMsg = "";

	if(cData==null || cData=="")	return;
	
	for(var i=0; i<cData.length; i++){
		if(cData[i].job == "D") continue;
		sNo = cData[i].idx;
		//중복체크...
		if (cData[i].job=="I") {
			if (pf_dupMENUAUTH(cData[i].data["MENU_AUTH"])==false) 
			{
				chkMsg = eval(sNo) + " 번째 행에 중복된 메뉴코드가 존재합니다";
				break;
			}
		}
	}
	if(chkMsg!="") _X.MsgBox('확인', chkMsg);
	return (chkMsg==""?0:sNo);
}

function x_DAO_Save(a_dg)
{
	var errRow = pf_errCheck();
	if (errRow>0) {
		dg_1.SetRow(errRow);
	  dg_1.focus();
		return;
	}
	setTimeout("x_SaveGridData()",0);
}

function x_DAO_Insert(a_dg, row)
{
	var newRow = a_dg.InsertRow(row);
	a_dg.SetItem(newRow, "USE_YN", "Y");
}

function x_DAO_Delete(a_dg, row)
{
	a_dg.DeleteRow(row);
}

function x_DAO_Duplicate(a_dg, row){
	a_dg.DuplicateRow(row);
}

function x_Duplicate_After(a_dg, rowIdx){
	if(a_dg==dg_1)	{
		a_dg.SetItem(rowIdx, "MENU_AUTH", "");
	}
}

function x_DAO_Excel(a_dg){
	a_dg.ExcelExport();
}

function xe_GridLayoutComplete(a_obj){
}

function xe_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey)
{
}

//입력 컨트롤에서 Enter키가 눌러진 경우 호출
function xe_InputKeyEnter(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey)
{
	switch(a_obj.id) {
		case "S_MENU_GROUP":
			x_DAO_Retrieve();
			break;
	}
}

//필드값이 바뀐경우 
function xe_GridDataChange(a_obj, rowIndex, columnIndex, newValue, oldValue)
{
}


function xe_EditChanged(a_obj, a_val, a_label, a_cobj)
{
	switch(a_obj)
	{
		case S_MENU_GROUP:
		    	x_DAO_Retrieve();
			break;
	}
}

function xe_GridRowFocusChange(a_obj, a_row, a_col)
{
}

function xe_GridItemDoubleClick(a_obj, row, a_col) 
{
	
}


function xe_GridRowFocusChange(a_obj, a_oldrow, a_newrow)
{
}

function xe_GridItemIconClick(a_obj, a_row, a_col)
{
}

function x_Close() {
	_X.CloseSheet(window);
}

//메뉴코드 중복체크
function pf_dupMENUAUTH(a_value, a_row, a_type) {
	if (a_type==null) a_type="SQL";
	
	if (a_type=="GRID") 
	{
		for(var i=1; i<dg_1.RowCount(); i++){
			if (dg_1.GetItem(i,"MENU_AUTH")==a_value&&i!=a_row) {
				return false;
			}	
		}
	} else {
			sData = _X.XmlSelect("sm", "SM_AUTH_MENU_GROUP", "SM_AUTH_MENU_GROUP_R01", new Array(a_value), "array");
			if(sData==a_value) return false;
	}
	return true;
}

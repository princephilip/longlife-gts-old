//var _Theme = getTheme();
var iconColumn1;
var itemIconClickHandler = function(event) {xe_M_GridItemIconClick(dg_1,event);}

//화면이 로딩된 이후 초기값 설정
function x_InitForm()
{
	xbtns.innerHTML = "<span style='width:100%;height:100%;vertical-align:middle;'>"
									+ _X.MainButtons(_PGM_CODE,"dg_1")
									+ "</span>"	;
		

	//_X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM900200", "XG_GRID|GRID_INFO");

	//시스템구분 Dropdown 처리	
	//var coData1 = _X.XmlSelect("sm", "CodeCom", "SYS_ID", new Array(), "json2");
	//_X.DropDownList_SetData(S_SYS_ID, 	coData1, null, true, true, '선택');
	

}

//Grid 설정이 완료된 이후 호출
function xe_GridLayoutComplete(a_obj){
}

//찾기 화면에서 선택값을 넘긴 경우 호출
function x_ReceivedCode(){

	switch(_FindCode.finddiv){
		case "G_PGM_CODE":
			dg_1.SetItem(_FindCode.grid_row, "PGM_CODE", _FindCode.returnValue.PGM_CODE);
			break;
	}

}

//조회 버튼 클릭
function x_DAO_Retrieve(a_dg)
{	

}

//저장 버튼 클릭
function x_DAO_Save(a_dg)
{
	if(a_dg!=null && a_dg.id=="dg_2") {
		dg_2.Save();
		return;
	}
	var errRow = pf_errCheck();
	if (errRow>0) {
		dg_1.SetRow(errRow);
	  dg_1.focus();
		return;
	}
	setTimeout("x_SaveGridData()",0);

}

//데이타 에러 체크
function pf_errCheck()
{
	return 0;
}

function pf_dup(a_value, a_row, a_type) {
	return true;
}

//추가,삽입 버튼 클릭
function x_DAO_Insert(a_dg, row)
{
	a_dg.InsertRow(row);
}

//데이타 추가 후 호출
function x_Insert_After(a_dg, rowIdx){
	if(a_dg.id=="dg_1") {
		dg_1.SetItem(rowIdx, "SYS_ID", null);
		dg_1.SetItem(rowIdx, "GRID_ID", null);
	} else if(a_dg.id=="dg_2") {
		dg_2.SetItem(rowIdx, "SYS_ID", 	  dg_1.GetItem(dg_1.GetRow(), "SYS_ID"));
		dg_2.SetItem(rowIdx, "PGM_CODE", 	dg_1.GetItem(dg_1.GetRow(), "PGM_CODE"));
		dg_2.SetItem(rowIdx, "GRID_ID", 	dg_1.GetItem(dg_1.GetRow(), "GRID_ID"));
	}
}

//삭제 버튼 클릭
function x_DAO_Delete(a_dg, row)
{
	a_dg.DeleteRow(row);
}

//복제 버튼 클릭
function x_DAO_Duplicate(a_dg, row){

	a_dg.DuplicateRow(row);

}

//데이타 복제 후 호출
function x_Duplicate_After(a_dg, rowIdx){
	if(a_dg.id=="dg_1") {
		dg_1.SetItem(rowIdx, "GRID_ID", null);
	} else if(a_dg.id=="dg_2") {
		dg_2.SetItem(rowIdx, "FIELD_SEQ", 	null);
		dg_2.SetItem(rowIdx, "FIELD_NAME", 	null);
	}
}

//엑셀 버튼 클릭
function x_DAO_Excel(a_dg){
	a_dg.ExcelExport();
}

//인쇄 버튼 클릭
function x_DAO_Print(a_dg){

}

//입력 컨트롤의 데이타 값이 변경된 경우 호출
function xe_EditChanged(a_obj, a_val, a_label, a_cobj)
{
	switch(a_obj.id) {
		case "S_SYS_ID":
			x_DAO_Retrieve();
			break;
	}
}

//입력 컨트롤에서 키가 눌러진 경우 호출
function xe_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey)
{
}

//입력 컨트롤에서 Enter키가 눌러진 경우 호출
function xe_InputKeyEnter(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey)
{
}

//Grid 컨트롤의 Row 선택이 변경된 경우 호출
function xe_GridRowFocusChange(a_dg, a_newrow, a_oldrow)
{
	if(a_dg.id=="dg_1" && a_newrow>0) {
		//dg_2 데이타 변경여부 체크	
		if(dg_2.IsDataChanged()) {
			//alert("변경된 데이타가 존재합니다.");
			//return false;
		}
					
		var param = new Array(a_dg.GetItem(a_newrow,'SYS_ID'), a_dg.GetItem(a_newrow,'PGM_CODE'), a_dg.GetItem(a_newrow,'GRID_ID'));
		dg_2.Retrieve(param);
	}

	return true;
}

//Grid 컨트롤의 Cell 선택이 변경된 경우 호출
function xe_GridItemFocusChange(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol)
{
	//alert(a_dg.id + " : " + a_newrow + "," + a_newcol + " : " + a_oldrow + "," + a_oldcol);
	return true;
}

function xe_GridOnLoad(a_dg){
}

function xe_GridDoubleClick(a_dg, a_event){

}

function xe_GridItemClick(a_dg, a_row, a_col, a_colname){
	//alert("xe_GridItemClick(" + a_row + ", " + a_col + ") " + a_colname);
}

function xe_GridItemDoubleClick(a_dg, a_row, a_col, a_colname){
	//alert("xe_GridItemDoubleClick(" + a_row + ", " + a_col + ") " + a_colname);
}

function xe_GridButtonClick(a_dg, a_row, a_col, a_colname){
	if(a_dg.id=="dg_1" && a_colname == "PGM_CODE") {
		_FindCode.grid_row= a_row;
		_X.FindPgmCode(window,"G_PGM_CODE","", new Array());
	} else if(a_dg.id=="dg_1" && a_colname == "SQL_ID") {
		if(_X.MsgBoxYesNo("Grid정보 초기화 작업을 진행하시겠습니까?","",2) == 1) {
			var l_subSystem = a_dg.GetItem(a_row,"SYS_ID");
			var l_sqlFile = a_dg.GetItem(a_row,"SQL_ID").split('|')[0];
			var l_sqlKey = a_dg.GetItem(a_row,"SQL_ID").split('|')[1];
			var l_args = new Array("20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101","20130101");
			//var l_args = new Array("20130101", "20130101", '%', '%', '%','%','%','%','%','%', '%', '%', '%', '%','%','%','%','0','0','0', '99999999999','%','%','%','%','%');
			var rowData = _X.XmlSelect(l_subSystem, l_sqlFile, l_sqlKey, l_args, "grid_schima", "");
			if(rowData=="OK") {
				_X.MsgBox("작업중 정상 종료 되었습니다.");
				var param = new Array(a_dg.GetItem(a_row,'SYS_ID'), a_dg.GetItem(a_row,'PGM_CODE'), a_dg.GetItem(a_row,'GRID_ID'));
				dg_2.Retrieve(param);				
			} else {
				_X.MsgBox("작업중 오류 발생", rowData);
			}				
		}
	} else if(a_dg.id=="dg_2" && a_colname == "BASE_APPLY") {
		pf_applytobase(a_dg.GetItem(a_row,"FIELD_NAME"));
	} 
	
}

function xe_GridHeaderClick(a_dg, a_col, a_colname){
}

function xe_GridContextMenuItemSelect(a_dg, a_label, a_row, a_col, a_colname){
	alert("xe_GridContextMenuItemSelect(" + a_label + "," + a_row + ", " + a_col + ") " + a_colname);
}

function xe_GridMenuItemSelect(a_dg, a_label, a_checked, a_tag){
	alert("xe_GridMenuItemSelect(" + a_label + "," + a_checked + ", " + a_tag + ") ");
}

function xe_GridScrollToBottom(a_dg){
	//alert("xe_GridScrollToBottom(" + a_obj.id + ")");
}



function xe_GridKeyDown(a_dg, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey)
{
	alert('xe_GridKeyDown ' + a_dg.id + " : " + a_keyCode + a_ctrlKey + " : " +  a_altKey + " : " +  a_shiftKey);
}

function xe_GridKeyUp(a_dg, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey)
{
	alert('xe_GridKeyUp ' + a_dg.id + " : " + a_keyCode + a_ctrlKey + " : " +  a_altKey + " : " +  a_shiftKey);
}

function xe_GridDataChange(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	//alert("xe_GridDataChange(" + a_row + " , " + a_col + ") : " + a_newvalue);
}

function itemEditEndChecker(rowIndex, columnIndex, item, dataField, oldValue, newValue) {
}

function itemEditBeginningChecker(rowIndex, columnIndex, item, dataField) {
	//입력모드가 아닌경우 편집 불가
	return true;
}


function xe_GridSearchChange(a_dg){

}

function xe_GridDataLoad(a_dg){
}

function xe_GridItemIconClick(a_dg, a_RowIndex, a_ColumnIndex){
}

//닫기 버튼 클릭
function x_Close() {
	_X.CloseSheet(window);
}

//=========================================================================================================
// 사용자 정의 함수
//=========================================================================================================

//Grid파일 생성
function pf_creategrid() {
	if(_X.MsgBoxYesNo("Grid정보 파일 생성을 진행하시겠습니까?","",2) != 1)
		return;
	var row = dg_1.GetRow();
	if (row > 0) {
		var subSystem = dg_1.GetItem(row, 'SYS_ID');
		var pgmCode   = dg_1.GetItem(row, 'PGM_CODE');
		
		_X.CreateRealGridFile(subSystem, pgmCode);	
	}
}

//Detail 구문생성
function pf_createdetail() {
	var rowCnt = dg_2.RowCount();
	var detailSyntax = "";
	var detailResize = "";
	var rowNo = 1;
	var colNo = 0;
	
	for (var i=1; i<=rowCnt; i++) {
		colNo++;
		if(colNo>3){
			rowNo++;
			colNo=1;
		}			
		var fName  = dg_2.GetItem(i, "FIELD_NAME");
		var fNameK = dg_2.GetItem(i, "FIELD_NAME_KOR");
		var dType  = dg_2.GetItem(i, "DATA_TYPE");
		
		detailSyntax += (colNo==1 ? String.fromCharCode(13) : "")
									+ "	<div class='detail_label" + (colNo==1 ? "_left" : "") + "'>" + fNameK + "</div> "
									+ "<div id='di_" + rowNo + "" + colNo + "' class='detail_input_bg'> "
									+ "<input id='" + fName + "' type='text' class='detail_input'> "
									+ "</div>"  + String.fromCharCode(13)
									;
		detailResize	+= (colNo==1 ? String.fromCharCode(13) : "")
									+ "di_" + rowNo + "" + colNo + ".ResizeInfo = {init_width:" + (colNo==3 ? "225" : "220") + ",  init_height:0, anchor:{x1:1,y1:0,x2:" + (colNo==3 ? "0.4" : "0.3") + ",y2:0}};"
									+ String.fromCharCode(13)
									;								
	}

	_ParamValue	= "<div id='freeform' class='detail_box'>"
							+ detailSyntax +  String.fromCharCode(13) 
							+ "</div>"
							+ String.fromCharCode(13) + String.fromCharCode(13) 
							+ "freeform.ResizeInfo = {init_width:1000, init_height:" + (rowNo*25)+ ", anchor:{x1:1,y1:0,x2:1,y2:0}};" + String.fromCharCode(13) 
							+ detailResize 
							+ String.fromCharCode(13);

	var modal = "<div id='syntaxmodal'><iframe src='" + "/Mighty/jsp/ShowSyntax.jsp" + "' class='iframe_popup'></iframe></div>";
	$(modal).dialog({title:'Detail 구문생성', show:false, width:900, height:600, modal:true, resizable:true, 
									close:function(){try {$(this).remove();} catch(e) {}}
								});
}

//Detail 구문생성(세로)
function pf_createdetail2() {
	var rowCnt = dg_2.RowCount();
	var detailSyntax = "";
	var detailResize = "";
	var rowNo = 1;
	var colNo = 0;
	
	for (var i=1; i<=rowCnt; i++) {
		colNo++;
		if(colNo>3){
			rowNo++;
			colNo=1;
		}			
		var fName  = dg_2.GetItem(i, "FIELD_NAME");
		var fNameK = dg_2.GetItem(i, "FIELD_NAME_KOR");
		var dType  = dg_2.GetItem(i, "DATA_TYPE");
		
		detailSyntax += "	<div class='detail_label_left'>" + fNameK + "</div> "
									+ "<div id='di_" + fName + "' class='detail_input_bg w349'>"
									+ "<input id='" + fNameK + "' type='text' class='detail_input'></div> "
									+ "<div class='detail_line'></div>"
									+ String.fromCharCode(13)
									;
	}

	_ParamValue	= "<div id='freeform' class='detail_box'>" + String.fromCharCode(13)
							+ detailSyntax +  String.fromCharCode(13) 
							+ "</div>"
							+ String.fromCharCode(13) + String.fromCharCode(13) 
							+ "freeform.ResizeInfo = {init_width:450, init_height:630, anchor:{x1:0,y1:1,x2:0,y2:1}};" + String.fromCharCode(13) 
							+ String.fromCharCode(13);

	var modal = "<div id='syntaxmodal'><iframe src='" + "/Mighty/jsp/ShowSyntax.jsp" + "' class='iframe_popup'></iframe></div>";
	$(modal).dialog({title:'Detail 구문생성(세로)', show:false, width:900, height:600, modal:true, resizable:true, 
									close:function(){try {$(this).remove();} catch(e) {}}
								});
}

//Grid 미리보기
function pf_gridpreview() {
	var row = dg_1.GetRow();
	if (row > 0) {
		var subSystem = dg_1.GetItem(row, 'SYS_ID');
		var pgmCode   = dg_1.GetItem(row, 'PGM_CODE');
		var gridId    = dg_1.GetItem(row, 'GRID_ID');
		var pgmUrl    = "/Mighty/template/XG010.jsp?pcd=SM900300" + "&gsid=" + subSystem.toLowerCase() + "&gpcd=" + pgmCode + "&ggid=" + gridId;		

		window.open(pgmUrl);
	}
}

//표준에 적용
function pf_applytobase(fieldName) {
	if(_X.MsgBoxYesNo("설정된 Grid정보를 표준에  적용하시겠습니까?","",2) != 1)
		return;
	
	if(fieldName==null)
		fieldName = '%';
	
	var row = dg_1.GetRow();
	if (row > 0) {
		var subSystem = dg_1.GetItem(row, 'SYS_ID');
		var pgmCode   = dg_1.GetItem(row, 'PGM_CODE');
		var gridId    = dg_1.GetItem(row, 'GRID_ID');

		_X.ExecProc("sm", "SP_XG_GridSchima_ToBase", new Array(subSystem, pgmCode, gridId, fieldName));
	}
	
}

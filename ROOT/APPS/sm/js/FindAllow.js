var _IsModal = (window.dialogArguments?true:false);
var _IsPopup = (window.opener?true:false);
var _Caller  = (_IsModal?window.dialogArguments:(_IsPopup?window.opener:window.parent));

var options_dg_1 = {
		panel: {visible: false},
		footer: {visible: false},
		checkBar: {visible: false},
		statusBar: {visible: false},
		select: {style: RealGrids.SelectionStyle.ROWS},
    edit: {
			insertable: false,
			appendable: false,
			updatable: false,
			deletable: false
    }
};

var options_dg_2 = {
		panel: {visible: false},
		footer: {visible: false},
		checkBar: {visible: false},
		statusBar: {visible: false},
		select: {style: RealGrids.SelectionStyle.ROWS},
    edit: {
			insertable: false,
			appendable: false,
			updatable: false,
			deletable: false
    }
};

//화면 디자인관련 요소들 초기화 작업
function x_InitForm(){

	_X.InitGrid(grid_1, "dg_1", "100%", "100%", _FIND_SYS, _FIND_GRID, _FIND_QUERY+"|" + _FIND_SELECT);
	_X.InitGrid(grid_2, "dg_2", "100%", "100%", _FIND_SYS, _FIND_GRID, _FIND_QUERY+"|" + _FIND_SELECT + '2');
	
}

function x_DAO_Retrieve(){
	
	dg_1.Retrieve(['100', 1, S_SEARCH.value]);
}

//xe_GridRowFocusChange가 안되서...추가
function xe_M_RealGridRowFocusChange(a_obj, oldIndex, newIndex){

	if(a_obj.id == 'dg_1'){
		
		dg_2.Retrieve(['100', dg_1.GetItem(newIndex.itemIndex +1, 'ALLOW_CODE')]);
	}
}

function xe_GridRowFocusChange(a_dg, a_newrow, a_oldrow){//alert("myAlert((xe_GridRowFocusChange)");
	alert('why?');
}

function x_DAO_Save(){}
function x_DAO_Insert(row){}
function x_DAO_Delete(row){}
function x_DAO_Duplicate(row){}
function x_Duplicate_After(obj, rowIdx){}
function x_DAO_Excel(){dg_1.ExcelExport();}
function xe_GridRowFocusChange(a_dg, a_row, a_col){}
function xe_GridDoubleClick(a_dg, a_ctrlKey, a_altKey, a_shiftKey, a_stageX, a_stageY){alert('9');}

function xe_GridDataLoad(a_dg){
	$('#S_SEARCH').focus();
}

function xe_EditChanged(a_obj, a_val, a_label, a_cobj){
	
}

function xe_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){
	
}

//xe_InputKeyEnter가 안되서...추가
function xm_InputKeyDown(){
	
	setTimeout('x_DAO_Retrieve()', 0);
}

//입력 컨트롤에서 Enter키가 눌러진 경우 호출
function xe_InputKeyEnter(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){

}

//xe_GridItemDoubleClick가 안되서...추가
function xe_M_RealGridItemDoubleClick(a_obj, cellIndex){

	if(a_obj.id == 'dg_2'){
		x_ReturnRowData(a_obj, cellIndex.itemIndex + 1);
	}
}

function xe_GridItemDoubleClick(a_dg, a_row, a_col){
	alert('why?');
}

function xe_GridButtonClick(a_dg, a_row, a_col, a_colname){
	
	if(a_dg.id=="dg_2" && a_colname == "SELECT_RETURN") {
		x_ReturnRowData(a_dg, a_row);
	}
}

function x_ReturnRowData(a_dg, a_row){

	if(typeof(xc_ReturnRowData)!="undefined"){xc_ReturnRowData(a_dg, a_row);return;}
	var rtValue = a_dg.GetRowData(a_row);
	//var rtValue = a_dg.GetItem(a_row,);

	if(_IsModal) {
		window.returnValue = rtValue;
	} else {
		if(_Caller) {			
			_Caller._FindCode.returnValue = rtValue;
			_Caller.x_ReceivedCode(rtValue);
			//_Caller._X.UnBlock();
		}
	}
	setTimeout('x_Close()',0);
}

function x_Confirm(){
	if (dg_2.GetRow()==0) return;
	x_ReturnRowData(dg_2, dg_2.GetRow());

}

function x_Close() {
	if(_IsModal || _IsPopup) {
		window.close();
	} else {
		if(_Caller.$("#findmodal").length>0?_Caller.$("#findmodal").dialog("close"):window.close());
	}
}

function xe_GridLayoutComplete(a_dg){
	
	if(a_dg.id != "dg_1"){
		setTimeout("x_DAO_Retrieve()",0);
	}
}
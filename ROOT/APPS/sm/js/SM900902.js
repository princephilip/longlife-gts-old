
/**
 *<pre>
 * Statements
 *</pre>
 
 * @Class Name : SM900902.js
 * @Descriptio   SlickGrid Group 예제
 * @Modification Information
 * @
 * @  수정일       수정자         수정내용
 * @ -------    	 --------    ---------------------------
 * @ 2015.10.21    이상규        최초 생성
 * @
 */
function x_InitForm2(){
  _X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM900902", "SM900902|SM900902");
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
  if(ab_allcompleted) {
  	dg_1.SetMaskValue( "COLS5", function(args) {
  		return _X.ToString( args.value, "YYYY-MM" );
  	});
  	dg_1.SetMaskValue( "COLS2", function(args) {
  		if (!args.rowIdx) {
  			dg_1.GetRowData2(-1);
  		}
  		return args.value + "원";
  	});
  	dg_1.SetMaskValue( "COLS10", function(args) {
  		return _X.KoreanNumber( parseInt( args.value, 10 ) );
  	});
			
    x_DAO_Retrieve(a_dg);
  }
}

function x_DAO_Retrieve2(a_dg){
  dg_1.Retrieve([]);
}

function xe_EditChanged2(a_obj, a_val){return 100;}
function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){return 100;}
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){return 100;}
function x_DAO_Save2(a_dg){return 100;}
function x_DAO_Saved2(){return 100;}
function x_DAO_ChkErr2(){return true;}
function x_DAO_Insert2(a_dg, row){return 100;}

function x_Insert_After2(a_dg, rowIdx){
	dg_1.SetItem(rowIdx, "COLS4", 2);
	return 100;
}

function x_DAO_Duplicate2(a_dg, row){return 100;}
function x_Duplicate_After2(a_dg, rowIdx){return 100;}
function x_DAO_Delete2(a_dg, row){return 100;}
function x_DAO_Excel2(a_dg){return 100;}
function x_DAO_Print2(){return 100;}
function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}
function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}
function xe_GridDataChange2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){return 100;}
function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){return true;}
function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){return true;}
function xe_GridDataLoad2(a_dg){return 100;}
function x_FindCode2(a_dg, a_row, a_findKey, a_findstr, a_win) {return 100;}
function x_ReceivedCode2(){return 100;}
function xe_GridButtonClick2(a_dg, a_row, a_col){return 100;}
function xe_GridHeaderClick2(a_dg, a_col, a_colname){return 100;}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){return 100;}
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){return 100;}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){return 100;}
function xe_ChartPointClick2(a_dg, a_event){return 100;}
function xe_ChartPointSelect2(a_dg, a_event){return 100;}
function xe_ChartPointMouseOver2(a_dg){return 100;}
function xe_ChartPointMouseOut2(a_dg, a_event){return 100;}

function x_Close2() {
	return 100;
}

function pf_setGroup1(){
	dg_1.SetGroup( ["COLS4", "COLS1"], false );
}

function pf_setGroup2(){
	dg_1.SetGroup( ["COLS4", "COLS1"], true );
}

function pf_resetGroup(){
	dg_1.SetGroup();
}

function pf_isGroup() {
	alert( dg_1.IsGroup() );
}

function pf_CollapseGroup1() {
	dg_1.CollapseGroup();
}


function pf_ExpandGroup1() {
	dg_1.ExpandGroup();
}

function pf_CollapseGroup2() {
	dg_1.CollapseGroup(1);
}

function pf_ExpandGroup2() {
	dg_1.ExpandGroup(1);
}

function pf_SetGroupFootVisible1() {
	dg_1.SetGroupFootVisible( null, false );
}

function pf_SetGroupFootVisible2() {
	dg_1.SetGroupFootVisible( null, true );
}

function pf_SetGroupFootVisible3() {
	dg_1.SetGroupFootVisible( "COLS1", false );
}

function pf_SetGroupFootVisible4() {
	dg_1.SetGroupFootVisible( "COLS1", true );
}

function pf_SetCountIf1() {
	dg_1.SetCountIf( "COLS9", function(args) {
		return args.value == "N";
	}, true);//xe_GridLayoutComplete2 에서 사용 시 세번째 인자 true 생략
}

function pf_SetCountIf2() {
	dg_1.SetCountIf( "COLS9", function(args) {
		return args.value == "Y";
	}, true);//xe_GridLayoutComplete2 에서 사용 시 세번째 인자 true 생략
}

function pf_SetGroupCustom1() {
	dg_1.SetGroupCustom( "COLS7", function(args) {
		args.avg( Number( args.item[ "COLS2" ] ) + Number( args.item[ "COLS7" ] ) );
	}, true);//xe_GridLayoutComplete2 에서 사용 시 세번째 인자 true 생략
}

function pf_SetGroupMaskValue1() {
	dg_1.SetGroupMaskValue( "COLS7", function(args) {
		return _X.FormatCommaPoint2( args.value, 0 ) + "원";
	}, true);//xe_GridLayoutComplete2 에서 사용 시 세번째 인자 true 생략
}

function pf_SetGroupMaskValue2() {
	dg_1.SetGroupMaskValue( "COLS2", function(args) {
		if ( args.oriVal > 9999 ) {
			return _X.FormatCommaPoint2( String( args.oriVal ).substring( 0, String( args.oriVal ).length - 4 ), 0, true ) + "만";
		}
		return args.value;
	}, true);//xe_GridLayoutComplete2 에서 사용 시 세번째 인자 true 생략
}

function pf_copyToClipboard() {
	var item = dg_1.GetRowData( dg_1.GetRow() );
	var str = "";
	
	for ( var prop in item ) {
		str += item[ prop ] + "	";
	}
	
  var $temp = $("<textarea>");
  $("body").append($temp);
  $temp.val(str).select();
  document.execCommand("copy");
  $temp.remove();
}

function pf_Fixed() {
	dg_1.SetFixed(2);
}

function pf_SetFooterRowVisibility(visible, footerRowCount) {
//	if ( footerRowCount && footerRowCount > 1 ) {
//		dg_1._SlickGrid.setOptions( { "footerRowCount": footerRowCount } );
//	}
	
	dg_1._SlickGrid.setFooterRowHeight(50);
	dg_1._SlickGrid.setFooterRowCount(1);
	dg_1._SlickGrid.setFooterRowVisibility(true);
	
	dg_1._SlickGrid.setColumns( dg_1._SlickGrid.getColumns() );
	
	var di = dg_1._SlickGrid.getFooterRowColumn("COLS3");
	di.innerHTML = "흐흐";
	var di = dg_1._SlickGrid.getFooterRowColumn("COLS4");
	di.classList.add( "style-text-right" );
	di.innerHTML = 288;
}

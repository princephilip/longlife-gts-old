﻿//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : @프로그램코드|프로그램명
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO		  
//
//========================================================================================
//
//========================================================================================

function x_InitForm(){

	xbtns.innerHTML = "<span style='width:100%;height:100%;vertical-align:middle;'>"
									+ _X.MainButtons(_PGM_CODE, 'dg_1')
									+ "</span>"	; 

	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "", "", "|");

}

function xe_GridLayoutComplete(a_dg, a_allcompleted){
}

function xe_GridOnLoad(a_obj){
}

function x_DAO_Retrieve(a_dg){
	var param = new Array();
	a_dg.Retrieve();
}

function xe_EditChanged(a_obj, a_val, a_label, a_cobj){
}

function xe_InputKeyEnter(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){
}

function x_DAO_Save(a_dg){
	setTimeout('x_SaveGridData()', 0);
}

function x_DAO_Insert(a_dg, row){
	a_dg.InsertRow(row);
}

function x_Insert_After(a_dg, rowIdx){
}

function x_DAO_Duplicate(a_dg, row){
	a_dg.DuplicateRow(row);
}

function x_Duplicate_After(a_dg, rowIdx){
}

function x_DAO_Delete(a_dg, row){
	a_dg.DeleteRow(a_dg.GetRow());
}

function x_DAO_Excel(a_dg){
	a_dg.ExcelExport();
}

function x_DAO_Print(a_dg){
}

function xe_TabChanged(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_TabChanging(a_div, a_navidx, a_preidx, a_curtab, a_pretab){
}

function xe_GridDataChange(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridScrollToBottom(a_dg){}
function xe_GridDataLoad(a_dg){}
function x_ReceivedCode(){}

// - change -
function xe_GridItemFocusChange(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){return true;}
function xe_GridHeaderListChange(a_obj, a_rowIndex, a_columnIndex, a_itemRenderer){}
function xe_GridSelectChange(a_obj, a_rowIndex, a_columnIndex){}
function xe_GridSearchChange(a_dg){}

// - click -
function xe_GridClick(a_obj, a_controlKey, a_altKey, a_shiftKey, a_stageX, a_stageY){}
function xe_GridButtonClick(a_dg, a_row, a_col, a_colname){}
function xe_GridDoubleClick(a_dg, a_event){}
function xe_GridItemClick(a_dg, a_row, a_col, a_colname){}
function xe_GridHeaderClick(a_dg, a_col, a_colname){}
function xe_GridItemIconClick(a_dg, a_RowIndex, a_ColumnIndex){}
function xe_GridItemDoubleClick(a_dg, a_row, a_col, a_colname){}

// - keydown -
function xe_GridKeyDown(a_dg, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}
function xe_GridKeyUp(a_dg, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

// - tree -
function xe_GridItemChecked(a_obj, a_itemIndex, a_checked){}
function xe_TreeItemExpanding(a_obj, a_itemIndex, a_rowId){}

// - chart -
function xe_ChartPointClick(a_obj, a_event){}
function xe_ChartPointSelect(a_obj, a_event){}
function xe_ChartPointMouseOver(a_obj, a_event){}
function xe_ChartPointMouseOut(a_obj, a_event){}

// - other -
function xe_GridMenuSelect(a_obj){}
function xe_GridMenuItemSelect(a_dg, a_label, a_checked, a_tag){}
function xe_GridContextMenuItemSelect(a_dg, a_label, a_row, a_col, a_colname){}
function xe_GridScroll(a_obj, a_delta, a_direction){}
function xe_GridSort(a_obj, a_columnIndex, a_dataField, a_multiColumnSort){}
function xe_GridHeaderRelease(a_obj, a_columnIndex, a_dataField){}
function xe_GridHeaderShift(a_obj, a_newIndex, a_oldIndex){}
function xe_GridItemRollOver(a_obj, a_rowIndex, a_columnIndex){}

function x_Close() {
	_X.CloseSheet(window);
}

// = user ================================================================================
function pf_errCheck(){return 0;}


//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 : @SM010200|부서코드
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
//     			1차 생성
//
//===========================================================================================
//	
//===========================================================================================

function x_InitForm2(){
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM01050", "SM01050|SM01050_R01", false, true);
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted){
		var ls_using = [{"code":'Y',"label":"사용"}, {"code":'N',"label":"사용안함"}];
		dg_1.SetCombo('USING_TAG', ls_using);
		x_DAO_Retrieve(dg_1);
	}
}

function x_DAO_Retrieve2(a_dg){
	var param = new Array(S_FIND.value, mytop._CompanyCode,  S_USE_YESNO.value);
	dg_1.Retrieve(param);
}

function xe_EditChanged2(a_obj, a_val){
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	cData = dg_1.GetChangedData();
	
	var ls_maxseq =  Number(_X.XS("sm","SM01050", "R_SM01050_DEPT_CODE_MAX", new Array(mytop._CompanyCode), "json2")[0].max_code); ;	// 품목코드 자동세팅을 위함. 모든탭에서 쓰일 쿼리, 추후에 탭별로 품목코드의 두번째자리의 숫자를 다르게 하고싶으면 tab별로 나눠야함.
	
	console.log(ls_maxseq);
	for(i=0; i<cData.length; i++){
		if(cData[i].job == "D") continue;
		
		sNo = cData[i].idx;
		
		for(j=1;j<=dg_1.RowCount();j++){
			if(dg_1.GetItem(cData[i].idx, 'DEPT_NAME') == dg_1.GetItem(j, 'DEPT_NAME') ){
				if(j == cData[i].idx) continue;
				chkMsg =  j + '행과 ' + sNo + " 번째 행의 부서명이 중복됩니다.\r\n 재입력해주세요." ;
				_X.MsgBox("확인", chkMsg);			
				return 0;
			}
		}
						
		if (cData[i].job=="I") {
			
			var ls_maxseq_string = String(ls_maxseq+1);
			
			if( 0 < ls_maxseq_string.length && ls_maxseq_string.length <= 3){
				dg_1.SetItem( cData[i].idx, "DEPT_CODE", 'D' + String(_X.LPad(++ls_maxseq,3,'0') ) );
			}
			if(ls_maxseq_string.length == 4){
				dg_1.SetItem( cData[i].idx, "DEPT_CODE", 'D' + String(_X.LPad(++ls_maxseq,4,'0') ) );
			}
			if(ls_maxseq_string.length == 5){
				dg_1.SetItem( cData[i].idx, "DEPT_CODE", 'D' + String(_X.LPad(++ls_maxseq,5,'0') ) );
			}
			if(ls_maxseq_string.length == 6){
				dg_1.SetItem( cData[i].idx, "DEPT_CODE", 'D' + String(_X.LPad(++ls_maxseq,6,'0') ) );
			}
			if(ls_maxseq_string.length == 7){
				dg_1.SetItem( cData[i].idx, "DEPT_CODE", 'D' + String(_X.LPad(++ls_maxseq,7,'0') ) );
			}
			if(ls_maxseq_string.length == 8){
				dg_1.SetItem( cData[i].idx, "DEPT_CODE", 'D' + String(_X.LPad(++ls_maxseq,8,'0') ) );
			}
	  }	
	}
	
	return true;
}

function x_DAO_Saved2(){

}

function x_DAO_Insert2(a_dg, row){
	return 100;
}

function x_Insert_After2(a_dg, rowIdx){
	if(a_dg.id == 'dg_1'){
		dg_1.SetItem(rowIdx, 'COMPANY_CODE', mytop._CompanyCode);
		dg_1.SetItem(rowIdx, 'USING_TAG'	 , 'Y');
		dg_1.SetItem(rowIdx, 'SORT_ORDER'	 , '99');
	}
	return 100;
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 1;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){

}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){

}

function xe_GridDataLoad2(a_dg){

}

function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){
}

function x_ReceivedCode2(a_retVal){
	
}
	
function xe_GridHeaderClick2(a_dg, a_col, a_colname){

}

function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){

}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){

}

function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){

}

function xe_ChartPointClick2(a_dg, a_event){

}

function xe_ChartPointSelect2(a_dg, a_event){

}

function xe_ChartPointMouseOver2(a_dg){

}

function xe_ChartPointMouseOut2(a_dg, a_event){

}

function x_Close2() {
	return 100;
}


//화면이 로딩된 이후 초기값 설정
function x_InitForm()
{
	xbtns.innerHTML = "<span style='width:100%;height:100%;vertical-align:middle;'>"
									+ _X.MainButtons(_PGM_CODE, _ggid)
									+ "</span>"	;
	_X.InitGrid(grid_1, _ggid, "100%", "100%", _gsid, _gpcd, "SM900010|GRID_COLS_BASE");
}

//Grid 설정이 완료된 이후 호출
function xe_GridLayoutComplete(a_obj){
}

//찾기 화면에서 선택값을 넘긴 경우 호출
function x_ReceivedCode(){
}

//조회 버튼 클릭
function x_DAO_Retrieve(a_dg)
{	
}

//저장 버튼 클릭
function x_DAO_Save(a_dg)
{
}

//추가,삽입 버튼 클릭
function x_DAO_Insert(a_dg, row)
{
	a_dg.InsertRow(row);
}

//데이타 추가 후 호출
function x_Insert_After(a_dg, rowIdx){
}

//삭제 버튼 클릭
function x_DAO_Delete(a_dg, row)
{
}

//복제 버튼 클릭
function x_DAO_Duplicate(a_dg, row){
}

//데이타 복제 후 호출
function x_Duplicate_After(a_dg, rowIdx){
}

//엑셀 버튼 클릭
function x_DAO_Excel(a_dg){
}

//인쇄 버튼 클릭
function x_DAO_Print(a_dg){
}

//입력 컨트롤의 데이타 값이 변경된 경우 호출
function xe_EditChanged(a_obj, a_val, a_label, a_cobj)
{
}

//입력 컨트롤에서 키가 눌러진 경우 호출
function xe_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey)
{
}

//Grid 컨트롤의 Row 선택이 변경된 경우 호출
function xe_GridRowFocusChange(a_dg, a_newrow, a_oldrow)
{
	return true;
}

//Grid 컨트롤의 Cell 선택이 변경된 경우 호출
function xe_GridItemFocusChange(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol)
{
	return true;
}

function xe_GridOnLoad(a_dg){
}

function xe_GridDoubleClick(a_dg, a_event){

}

function xe_GridItemClick(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemDoubleClick(a_dg, a_row, a_col, a_colname){
}

function xe_GridButtonClick(a_dg, a_row, a_col, a_colname){
}

function xe_GridHeaderClick(a_dg, a_col, a_colname){
}

function xe_GridContextMenuItemSelect(a_dg, a_label, a_row, a_col, a_colname){
}

function xe_GridMenuItemSelect(a_dg, a_label, a_checked, a_tag){
}

function xe_GridScrollToBottom(a_dg){
}

function xe_GridKeyDown(a_dg, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey)
{
}

function xe_GridKeyUp(a_dg, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey)
{
}

function xe_GridDataChange(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function itemEditEndChecker(rowIndex, columnIndex, item, dataField, oldValue, newValue) {
}

function itemEditBeginningChecker(rowIndex, columnIndex, item, dataField) {
	return true;
}


function xe_GridSearchChange(a_dg){

}

function xe_GridDataLoad(a_dg){
}

function xe_GridItemIconClick(a_dg, a_RowIndex, a_ColumnIndex){
}

//닫기 버튼 클릭
function x_Close() {
	_X.CloseSheet(window);
}

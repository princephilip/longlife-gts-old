/**
 *<pre>
 * Statements
 *</pre>
 
 * @Class Name : SM900900.js
 * @Descriptio   SlickGrid 300 count data
 * @Modification Information
 * @
 * @  수정일       수정자         수정내용
 * @ -------    	 --------    ---------------------------
 * @ 2015.10.21    이상규        최초 생성
 * @
 */
function x_InitForm2(){
  _X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM900900", "SM900900|SM900901");
  _X.InitGrid(grid_2, "dg_2", "100%", "100%", "sm", "SM900900", "SM900900|SM900901");
  _X.InitGrid(grid_3, "dg_3", "100%", "100%", "sm", "SM900900", "SM900900|SM900902");
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
  if(ab_allcompleted) {
  	a_dg.SetMaskValue( "ORDER_DATE", function(args) {
  		return _X.ToString( args.value, "YYYY-MM" );
  	});
  	a_dg.SetMaskValue( "PROC_CODE", function(args) {
  		return _X.KoreanNumber( parseInt( args.value, 10 ) );
  	});
			
    x_DAO_Retrieve(a_dg);
  }
}

function x_DAO_Retrieve2(a_dg){
  dg_1.Retrieve([]);
  dg_2.Retrieve([]);
  dg_3.Retrieve([]);
}

function xe_EditChanged2(a_obj, a_val){return 100;}
function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){return 100;}
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){return 100;}
function x_DAO_Save2(a_dg){return 100;}
function x_DAO_Saved2(){return 100;}
function x_DAO_ChkErr2(){return true;}
function x_DAO_Insert2(a_dg, row){return 100;}
function x_Insert_After2(a_dg, rowIdx){return 100;}
function x_DAO_Duplicate2(a_dg, row){return 100;}
function x_Duplicate_After2(a_dg, rowIdx){return 100;}
function x_DAO_Delete2(a_dg, row){return 100;}
function x_DAO_Excel2(a_dg){return 100;}
function x_DAO_Print2(){return 100;}
function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}
function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}
function xe_GridDataChange2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){return 100;}
function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){return true;}
function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){return true;}
function xe_GridDataLoad2(a_dg){return 100;}
function x_FindCode2(a_dg, a_row, a_findKey, a_findstr, a_win) {return 100;}
function x_ReceivedCode2(){return 100;}
function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){return 100;}
function xe_GridHeaderClick2(a_dg, a_col, a_colname){return 100;}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){return 100;}
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){return 100;}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){return 100;}
function xe_ChartPointClick2(a_dg, a_event){return 100;}
function xe_ChartPointSelect2(a_dg, a_event){return 100;}
function xe_ChartPointMouseOver2(a_dg){return 100;}
function xe_ChartPointMouseOut2(a_dg, a_event){return 100;}

var testSortAsc = false;
function x_Close2() {
//	if ( footerRowCount && footerRowCount > 1 ) {
//		dg_1._SlickGrid.setOptions( { "footerRowCount": footerRowCount } );
//	}
	
	dg_1._SlickGrid.setFooterRowVisibility(true);
	
//	if (true) {
		dg_1._SlickGrid.setColumns( dg_1._SlickGrid.getColumns() );
//	}
//console.log(dg_1._SlickGrid.getFooterRow());
	return;
	return 100;
}

function pf_ExcelExport() {
	dg_1.ExcelExport();
}

function pf_excelupload(){		
	if(_X.MsgBoxYesNo("엑셀 업로드 하시겠습니까?") == "2") return;
	pf_ExcelUpload();
}

function uf_ExcelImported(a_id, a_cnt){
	ls_id = a_id;
	
	var ls_rtn = _X.ExecProc('hr', 'SP_MA_PROJ_CUST_UPLOAD', new Array(top._CompanyCode, S_PROJ_CODE.value, ls_id))
	
	_X.MsgBox('확인', '엑셀업로드를 완료했습니다.');
	x_DAO_Retrieve2(dg_1);
}
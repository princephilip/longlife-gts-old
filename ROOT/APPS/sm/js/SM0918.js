function x_InitForm2(){
	_X.InitGrid(grid_user, "dg_user", "100%", "100%", "sm", "SM0910", "SM_AUTH_USER|R_SM0910_01");
	_X.InitGrid(grid_menu, "dg_menu", "100%", "100%", "sm", "SM0910", "SM_AUTH_GROUP_MENU|S_SM0918_01");
	return 0;
}

//grid onload completed
function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted){
		dg_user.GridReadOnly(true);
		dg_menu.GridReadOnly(true);
		var	comboData  = _X.XmlSelect("sm", "SM_AUTH_SYS", "R_SM0903_01", new Array(mytop._CompanyCode), "json2");
		//Combo 데이타 설정
		dg_menu.SetCombo("SYS_ID", comboData);				//시스템구분
		setTimeout('x_DAO_Retrieve(dg_user)',0);
	}
	return 0;
}

//자료조회
function x_DAO_Retrieve2(a_dg){
	var param = new Array(mytop._CompanyCode, S_FIND.value);
	dg_user.Retrieve(param);
	return 0;
}

//html input type 의 자려가 변경된 경우 발생
function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id) {
	  case "S_SYS_ID":
			var param1 = new Array(mytop._CompanyCode, dg_user.getItemString(dg_user.GetRow(), 'USER_ID'), a_val, S_FIND2.value);
			dg_menu.Retrieve(param1);
	  	break;
	}
	return 0;
}

//enter key event 발생시 호출
function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	switch(a_obj.id) {
	  case "S_FIND":
	  	x_DAO_Retrieve(dg_user);
	  	break;
	  case "S_FIND2":
	  	x_DAO_Child_Retrieve();
	  	break;
	}
	return 0;
}

//keydown event 발생시 호출
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){return 100;}

//데이타 저장
function x_DAO_Save2(a_dg){return 100;}

//데이타 저장후 callback
function x_DAO_Saved2(){
	return 0;
}

//데이타 유효성 check
function x_DAO_ChkErr2(){
	return true;
}

//grid insert/add
function x_DAO_Insert2(a_dg, row){
	return 100;
}

//grid insert 수행후 발생
function x_Insert_After2(a_dg, rowIdx){
	return 0;
}

//grid 복제
function x_DAO_Duplicate2(a_dg, row){return 100;}

//grid 복제후
function x_Duplicate_After2(a_dg, rowIdx){return 100;}

//grid 삭제
function x_DAO_Delete2(a_dg, row){return 100;}

//excel 저장
function x_DAO_Excel2(a_dg){return 100;}

//자료조회
function x_DAO_Child_Retrieve(){
	var param1 = new Array(mytop._CompanyCode, dg_user.GetItem(dg_user.GetRow(), 'USER_ID'), S_SYS_ID.value, S_FIND2.value);
	dg_menu.Retrieve(param1);
	return 0;
}

//출력
function x_DAO_Print2(){return 100;}

//tab 변경시
function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 1;}

//tab 변경시
function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}

//grid data 변경시
function xe_GridDataChange2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	return 0;
}

//grid row focus change
function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

//grid row focus change
function xe_GridRowFocusChanged2(a_dg, a_newrow){	
	if(a_dg == dg_user) {
		x_DAO_Child_Retrieve();
	}
	return 100;
}

//item focus change event
function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){return true;}

//grid data loaded
function xe_GridDataLoad2(a_dg){return 100;}

//찾기창 호출 후
function x_FindCode2(a_dg, a_row, a_findKey, a_findstr, a_win) {
	return 0;
}

//찾기창 호출 후
function x_ReceivedCode2(a_returnValue) {
	return 0;
}

//grid item button click event
function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){return 100;}

//header click event
function xe_GridHeaderClick2(a_dg, a_col, a_colname){return 100;}

//item double click event
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){return 100;}

//tree item checked event
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){return 100;}

//tree item checked event
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){return 100;}

//- anychart  point click event
function xe_ChartPointClick2(a_dg, a_event){return 100;}

//- anychart  point select event
function xe_ChartPointSelect2(a_dg, a_event){return 100;}

//- anychart  point mouse over event
function xe_ChartPointMouseOver2(a_dg){return 100;}

//- anychart  point mouse out event
function xe_ChartPointMouseOut2(a_dg, a_event){return 100;}

//window(mdi) close
function x_Close2() {return 100;}

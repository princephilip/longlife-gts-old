//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 : [CM501020.js] 관리대상
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
//   smlee							 20171026				개인권한 신청
//
//===========================================================================================

function x_InitForm2(){
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "SM", "SM0945", "SM0945|SM0945_01", false, true);
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
	
	var status_div = [{"code":"0","label":"대기"},{"code":"Y","label":"승인"},{"code":"N","label":"반려"}];
	_X.DropDownList_SetData(S_STATUS_DIV, status_div, null, false, true, '전체');	

	var	pos_div  = _X.XmlSelect('hr', 'HR_COMCODE', 'GRADE_CODE'            , new Array() , 'json2');
	dg_1.SetCombo("POS_CODE", pos_div);

	var sys_div = _X.XmlSelect("sm", "SM_AUTH_SYS", "W_SM0915_01", new Array(mytop._CompanyCode), "json2");
	dg_1.SetCombo("SYS_ID", sys_div);

	pf_set_auth();
	x_DAO_Retrieve2(dg_1);
	}
}

function x_DAO_Retrieve2(a_dg){

	switch(a_dg.id){
		case 'dg_1':
			var param = new Array(mytop._CompanyCode, S_STATUS_DIV.value, S_EMP_NAME.value);
			dg_1.Retrieve(param);
		break;
	}
//	groupByDuration(a_dg)
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id){
		case 'S_STATUS_DIV':		
			x_DAO_Retrieve(dg_1);
		break;
	}
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	switch(a_obj.id){
		case 'S_EMP_NAME':
			x_DAO_Retrieve(dg_1);
		break;
	}
}
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){	
	var cData1 		= dg_1.GetChangedData();
	for (i=0 ; i < cData1.length; i++){

		if(cData1[i].job == 'D') continue;

		if(cData1[i].job == 'I' || cData1[i].job == 'U' ){
			var div_y =	dg_1.GetItem(cData1[i].idx, "STATUS_Y");
			var div_n =	dg_1.GetItem(cData1[i].idx, "STATUS_N");
			if (div_y == "Y"){
				dg_1.SetItem(cData1[i].idx,"STATUS_TAG","Y");
			}else if (div_n == "Y"){
				dg_1.SetItem(cData1[i].idx,"STATUS_TAG","N");
			}else{
				dg_1.SetItem(cData1[i].idx,"STATUS_TAG","0");
			}

			if(cData1[i].job == 'I' ){
				var grid1_max	= _X.XmlSelect("sm", "SM0945", "SM0945_R01" , new Array('') , "array");
				dg_1.SetItem(cData1[i].idx, 'DOC_SEQ', String(++grid1_max[0][0]) ) ;
			}

		}
	}	
	return true;
}

function x_DAO_Saved2(){

}

function x_DAO_Insert2(a_dg, row){
	_X.FindEmp(window,"G_LABOR_CODE",'', new Array(mytop._CompanyCode, '12'));
	return;

	return 100;
}

function x_Insert_After2(a_dg, rowIdx){
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){

}

function x_DAO_Delete2(a_dg){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){

}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	if(a_dg.id == "dg_1"){
		switch (a_col){
			case "STATUS_Y":
				if(a_newvalue == "Y"){
					dg_1.SetItem(a_row, "STATUS_N", "N");									
				}
			break;
			case "STATUS_N":
				if(a_newvalue == "Y"){
					dg_1.SetItem(a_row, "STATUS_Y", "N");									
				}
			break;
			case "SYS_ID":				
					dg_1.SetItem(a_row, "PGM_PATH", "");
					dg_1.SetItem(a_row, "PGM_NAME", "");
					dg_1.SetItem(a_row, "PGM_CODE", "");			
			break;
		}
	}

}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){

}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridItemFocusChanged(a_dg, a_newrow, a_newcol){

}

function xe_GridDataLoad2(a_dg){

}

function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){ //  a_col <-> a_colname 순서바뀜
	switch(a_col) {
		case "DEPT_NAME":
			_X.CommonFindCode(window,"부서코드찾기","hr","F_DEPT_CODE2","",new Array(mytop._CompanyCode,'%'),"FindDeptCode|HrFindCode|FindDeptCode",500, 650);
		break;	
		case "PGM_NAME":
			if(dg_1.GetItem(a_row, 'SYS_ID') == ""){
 				_X.MsgBox("확인","메뉴 구분을 선택해 주십시오.");
			}else{
				_X.FindMenu(window,"F_PGM_NAME",'', new Array(mytop._CompanyCode, dg_1.GetItem(a_row, 'SYS_ID')));
			}
		
		break;	
	}
	return 100;

}

function x_ReceivedCode2(a_retVal){
	switch(_FindCode.finddiv){		
		case "G_LABOR_CODE":
			if ( a_retVal == null ) {
				return;
			}

			var NRow = dg_1.InsertRow(0);
			dg_1.SetItem(NRow, "COMPANY_CODE", mytop._CompanyCode);
			dg_1.SetItem(NRow, "USER_ID", a_retVal.EMP_NO);
			dg_1.SetItem(NRow, "USER_NAME", a_retVal.EMP_NAME);
			dg_1.SetItem(NRow, "DEPT_CODE", a_retVal.DEPT_CODE);
			dg_1.SetItem(NRow, "DEPT_NAME", a_retVal.DEPT_NAME);
			dg_1.SetItem(NRow, "POS_CODE", a_retVal.GRADE_CODE);						
			dg_1.SetItem(NRow, "REG_DATE", _X.ToString(_X.GetSysDate(),'YYYYMMDD'));
			dg_1.SetItem(NRow, "STATUS_TAG", "0");	
		break;
		case 'F_DEPT_CODE2':
			dg_1.SetItem(dg_1.GetRow(), 'DEPT_CODE', _FindCode.returnValue.DEPT_CODE);
			dg_1.SetItem(dg_1.GetRow(), 'DEPT_NAME', _FindCode.returnValue.DEPT_NAME);
		break;
		case 'F_PGM_NAME':
			dg_1.SetItem(dg_1.GetRow(), 'PGM_PATH', _FindCode.returnValue.NAME_PATH);
			dg_1.SetItem(dg_1.GetRow(), 'PGM_NAME', _FindCode.returnValue.PGM_NAME);
			dg_1.SetItem(dg_1.GetRow(), 'PGM_CODE', _FindCode.returnValue.PGM_CODE);
		break;
	}
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){

}

function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){

}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){

}

function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){

}

function xe_ChartPointClick2(a_dg, a_event){

}

function xe_ChartPointSelect2(a_dg, a_event){

}

function xe_ChartPointMouseOver2(a_dg){

}

function xe_ChartPointMouseOut2(a_dg, a_event){

}

function x_Close2() {
	return 100;
}

// 권한 세팅
function pf_set_auth() {  
  if( _X.HaveActAuth('시스템관리자') ) {    
  } else {
  	dg_1.SetAutoReadOnly("STATUS_Y"  , function (args) {return true;}, true);
    dg_1.SetAutoReadOnly("STATUS_N"  , function (args) {return true;}, true);   
 }
}
//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : [SM02010] 가맹점관리
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
function x_InitForm2(){
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM02010", "SM02010|SM02010_C01", true, true);
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {

		var	ls_month  = _X.XS('com', 'COMMON', 'SM_COMCODE_D' , ['SM','MONTH'], 'json2');
		_X.DDLB_SetData(AM_START_MONTH, ls_month, null, false, false);

		setTimeout("x_DAO_Retrieve()",100);
	}
}

function x_DAO_Retrieve2(a_dg){
 	dg_1.Retrieve(['%', '%']);
}

function xe_EditChanged2(a_obj, a_val){
}


function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

//저장후 호출
function x_DAO_Saved2(){
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
	switch(_FindCode.finddiv) {
		case	"ZIP_CODE" :
					var ll_row = dg_1.GetRow();
					dg_1.SetItem(ll_row, "ZIP_CODE"   , a_retVal.zipNo);	// 주소찾이만 이런형식으로 받음
					dg_1.SetItem(ll_row, "ADDR1"      , a_retVal.roadAddrPart1 + a_retVal.roadAddrPart2);
					dg_1.SetItem(ll_row, "ADDR2"      , a_retVal.addrDetail);
					ADDR2.focus();
					break;
	}
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

//우편번호찾기
function uf_ZipFind(a_param) {
	if(dg_1.RowCount() == 0) return;
	_X.FindStreetAddr(window,a_param,"", "");
}


/**
 *<pre>
 * Statements
 *</pre>

 * @Class Name : SM900002.js
 * @Descriptio   Grid 관리
 * @Modification Information
 * @
 * @  수정일     수정자      수정내용
 * @ -------    --------    ---------------------------
 * @ 2016.10.28     JH        최초 생성
  */
var curSys = "";
var curPgm = "";
var curPgmname = "";
var curGrid = "";
var curCol = "";   // 현재 작업중인 칼럼명 
var mime = 'text/x-mariadb'; // codemirror mime type
var isSqlChanged = true; // SQL변경 자동 이벤트 막기
var vFieldRows = {};
var vColnames = {};  // 칼럼 Objects
var vIsPreview = false; 

var vFreeform = {}; // Freeform Settings 정보 

function x_InitForm2(){
  _X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM900040", "SM900040|DEV_GRID_COLS", false, true);
  _X.InitGrid(grid_2, "dg_2", "100%", "100%", "sm", "SM900040", "SM900040|DEV_PGM_CODE", false, false);
  _X.InitGrid(grid_3, "dg_3", "100%", "100%", "sm", "SM900040", "SM900040|DEV_COLUMN_LIST", false, false);
  _X.InitGrid(grid_4, "dg_4", "100%", "100%", "sm", "SM900040", "SM900040|DEV_FREEFORM", false, false);

  //_X.InitGrid(grid_devpropsrc, "dg_devpropsrc", "100%", "100%", "sm", "SM900002", "SM900002|SM900002_GRIDSRC", false, false);
  //$(".new-grid").hide();

  // 수정SQL 적용 버튼
  //$("#btn-modified-sql").click(function() {
    //if( !$(this).hasClass("disabled") ) exeSql();
  //});

  // 초기화 버튼 
  $("#btn-reset").click(function() {
    pf_set_working_grid();

    // dg_1 에서 dg_3(column list) 생성
    pf_ff_reset_start("db");

    // dg_3(column list) 에서 vFreeform 생성
    pf_setting_initial();

    // vFreeform에서 Layout과 Html 그리기 
    pf_preview();

    if(dg_3.RowCount()>0) {
      dg_3.SetRow(1);
      xe_GridRowFocusChange2(dg_3, 1, 1);
    }

    // pf_ff_reset();
    // pf_reset_setting();
  });

  // History 버튼
  $('#historyModal').on('shown.bs.modal', function () {
    // _X.InitGrid(grid_4, "dg_4", "100%", "100%", "sm", "SM900040", "SM900040|DEV_FREEFORM", false, false);
    _X.ResetGrid(dg_4, columns_dg_4, null, null, "SM900040|DEV_FREEFORM");

    var tRow = dg_2.GetRow();
    dg_4.Retrieve(new Array(dg_2.GetItem(tRow, "PGM_CODE"), dg_2.GetItem(tRow, "GRID_ID")));
  });
  // $("#btn-history").click(function() {
    
  // });

  // 전체 width, title, label-width, colcnt 수정시 
  $("#ff-width, #ff-title, #ff-label-width, #ff-colcnt").on("change", function() {
    pf_setting_initial("cols");
    pf_preview();
  });

  // 클립보드 복사버튼
  $("#copybtn-code-source").click(function() {
    copyToClipboard( $("#code-html").val() );
  });

  // 탭버튼 클릭(열고, 닫고)
  $(".ffgrid a").click(function() {
    pf_show_panel($(this));
  });

  // 이벤트 바인딩
  pf_bind_event();

  return 100;
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
  if(ab_allcompleted) {
    var vBool = [{code:'true', label:'true'}, {code:'false', label:'false'}];
    var vUpdType = [{code:'char', label:'char'}, {code:'number', label:'number'}, {code:'date', label:'date'}];

    // dg_1.SetCombo("VISIBLE",       vBool);       // 보이기
    // dg_1.SetCombo("READONLY",      vBool);       // 보이기
    // dg_1.SetCombo("EDITABLE",      vBool);       // 보이기
    // dg_1.SetCombo("SORTABLE",      vBool);       // 보이기
    // dg_1.SetCombo("MERGE",         vBool);       // 보이기
    // dg_1.SetCombo("RESIZABLE",     vBool);       // 보이기
    // dg_1.SetCombo("LOOKUPDISPLAY", vBool);       // 보이기

  }
  return 100;
}

function x_DAO_Retrieve2(a_dg){
  dg_2.Retrieve(new Array(S_SYS_ID.value, '%'+S_PGM_CODE.value+'%'));

  return 0;
}

//html input type 의 자려가 변경된 경우 발생
function xe_EditChanged2(a_obj, a_val){
}

//enter key event 발생시 호출
function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
  if(a_obj==S_PGM_CODE) {
    x_DAO_Retrieve();
  }

  return 0;
}

//keydown event 발생시 호출
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){return 100;}

//데이타 저장
function x_DAO_Save2(a_dg){ // #as_pgm#, #as_title#, #as_cols#, #as_ff#, #as_user#, #as_ip#
  var params = [
    curPgm, curGrid, $("#ff-title").val(), vFreeform.layout, JSON.stringify(vFreeform), mytop._UserID, '127.0.0.1'
  ];
  //_X.ExecSql("sm", "SM900040", "PROC_SAVE", params, 'json2');
  var li_result = _X.ExecProc("sm", "SP_SM_DEV_FREEFORM", params);

  //alert("저장했습니다.")
  if(li_result==1) {
    _X.Noty("저장했습니다.", null, "success", 500);
  } else {
    _X.Noty("저장중 오류가 발생했습니다.", null, "danger", 500);
  }

  return 0;
  //return 100;
}

//데이타 저장후 callback
function x_DAO_Saved2(){
  $(".new-grid").hide();

  var params = new Array(curSys, curPgm, curGrid, $("#S_SQL_FILE").val(), $("#S_SQL_ID").val(), $("#S_UPDATE_TABLE").val());
  var li_rtn = _X.ExecSql("sm", "SM900002", "DEV_GRID_UPDATE", params);

  if(li_rtn!=1) {
    alert("GRID 정보 저장 중 오류가 발생했습니다.");
  }
  //Grid Script 생성(2016.11.24 KYY)
  _X.ExecProc("sm", "SP_SM_DEV_GRID_SCRIPT", new Array(curSys, curPgm));

  return 100;
}

//데이타 유효성 check
function x_DAO_ChkErr2(){
  return true;
}

//grid insert/add
function x_DAO_Insert2(a_dg, row){return 100;}

//grid insert 수행후 발생
function x_Insert_After2(a_dg, rowIdx){
  if(a_dg==dg_1) {
    dg_1.SetItem(rowIdx, "SYS_ID", curSys);
    dg_1.SetItem(rowIdx, "PGM_CODE", curPgm);
    dg_1.SetItem(rowIdx, "GRID_ID", $("#S_GRID_ID").val());
  }
  return 0;
}

//grid 복제
function x_DAO_Duplicate2(a_dg, row){return 100;}

//grid 복제후
function x_Duplicate_After2(a_dg, rowIdx){
  return 0;
}

//grid 삭제
function x_DAO_Delete2(a_dg, row){return 100;}

//excel 저장
function x_DAO_Excel2(a_dg){return 100;}

//출력
function x_DAO_Print2(){
  return 100;
}

//tab 변경시
function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}

//tab 변경시
function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}

//grid data 변경시
function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){

  // if(a_dg==dg_3) {
  //   if(a_col=="FIELDTITLE") {
  //     var tId = dg_3.GetItem(a_row, "FIELDNAME");
  //     $("label[for='"+tId+"']").html(a_newvalue);
  //   }
  // }

  return 0;
}

//grid row focus change
function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
  if(a_dg==dg_2 && a_newrow>0) {
    var tSys = dg_2.GetItem(a_newrow, "SYS_ID");
    var tPgm = dg_2.GetItem(a_newrow, "PGM_CODE");
    var tGrid = dg_2.GetItem(a_newrow, "GRID_ID");

    pf_selectGridId(tSys, tPgm, tGrid);
    /*
    $(".new-grid").hide();

    curSys = dg_2.GetItem(a_newrow, "SYS_ID");
    curPgm = dg_2.GetItem(a_newrow, "PGM_CODE");
    pf_setGridIds(curSys, curPgm );
    */
  }

  if(a_dg==dg_3 && a_newrow>0) {
    var tCname = dg_3.GetItem(a_newrow, "FIELDNAME");
    // layout 에서 Selected 처리 
    pf_sel_column(tCname);
    // preview 에서 Selected 처리
    pf_sel_col_pv(tCname);
    // setting 정보 세팅
    pf_set_setting(tCname);

    // pf_event_col( dg_3.GetItem(a_newrow, "FIELDNAME") );
  }

  return true;
}

//item focus change event
function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){return true;}

//grid data loaded
function xe_GridDataLoad2(a_dg){
  return 100;
}

//찾기창 호출 후
function x_FindCode2(a_dg, a_row, a_findKey, a_findstr, a_win) {return 100;}

//찾기창 호출 후
function x_ReceivedCode2(){
}

//grid item button click event
function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){
  pf_del_col(a_row);

  return 100;
}

//header click event
function xe_GridHeaderClick2(a_dg, a_col, a_colname){return 100;}

//item double click event
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
  if(a_dg==dg_4&&a_row>0) {
    var tFid = dg_4.GetItem(a_row, "FF_ID");
    var tSt  = dg_4.GetItem(a_row, "FF_OBJECT");

    alert( tSt );
  }
  return 100;
}

//tree item checked event
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){return 100;}

//tree item checked event
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){return 100;}

//- anychart  point click event
function xe_ChartPointClick2(a_dg, a_event){return 100;}

//- anychart  point select event
function xe_ChartPointSelect2(a_dg, a_event){return 100;}

//- anychart  point mouse over event
function xe_ChartPointMouseOver2(a_dg){return 100;}

//- anychart  point mouse out event
function xe_ChartPointMouseOut2(a_dg, a_event){return 100;}

//window(mdi) close
function x_Close2() {return 100;}


//현재 작업중인 그리드 정보 세팅
var pf_set_working_grid = function() {
  var tRow = dg_2.GetRow();
  curSys = dg_2.GetItem(tRow, "SYS_ID");
  curPgm = dg_2.GetItem(tRow, "PGM_CODE");
  curPgmname = dg_2.GetItem(tRow, "PGM_NAME");
  curGrid = dg_2.GetItem(tRow, "GRID_ID");

  $(".wrap-btn-grids").html("<strong>["+curPgm+"] "+curPgmname+" - "+curGrid+"</strong>");
}

// 칼럼추가
function pf_add_col() {
  var tRow = dg_3.GetRow();

  dg_3.InsertRow();
  //pf_ff_reset();
  pf_column_list_reset();
}

// 칼럼삽입
function pf_ins_col() {
  var tRow = dg_3.GetRow();
  if(tRow<1) tRow = 1;

  dg_3.InsertRow( tRow );
  //pf_ff_reset();
  pf_column_list_reset();
}

// 칼럼삭제
function pf_del_col(aRow) {
  if(aRow>0) {
    dg_3.DeleteRow(aRow);
    var tCnt = dg_3.RowCount();

    if(dg_3.GetRow()>tCnt && tCnt>0) {
      dg_3.SetRow( tCnt );
    }
    //pf_ff_reset();
    pf_column_list_reset();
  }
}

// Grid Id 선택
var pf_selectGridId = function(aSysID, aPgm, aGid) {
  // 기존 Grid 선택시
  aGid = aGid.replace("gridbtn_", "");
  curGrid = aGid;

  $("#S_GRID_ID").val(aGid);
  $(".wrap-btn-grids .btn").not(".btn-danger").removeClass("btn-warning").addClass("btn-info");

  dg_1.Retrieve(new Array(aSysID, aPgm, aGid));
  $("#gridbtn_"+aGid).not(".btn-danger").removeClass("btn-info").addClass("btn-warning");


}

// 새로운 FREEFORM 시작 ( aDiv : db, layout, SQL, TEXT )
var pf_ff_reset_start = function(aDiv) {
  if(!$("#ff-width").val()) {
    alert("Width를 입력해야 합니다.");
    return;
  }

  // DB Grid Cols 정보에서 시작 
  if(aDiv=="db") {
    if(dg_3.RowCount()>=0) {
      if(!confirm("아래 설정한 프리폼이 초기화 됩니다. 계속 하시겠습니까?")) {
        return;
      }
    }

    dg_3.Reset();
    var tCnt = dg_1.RowCount();
    for(var i=1; i<=tCnt; i++) {
      if(dg_1.GetItem(i, "VISIBLE")=="Y") {
        var tRow = dg_3.InsertRow(0);
        dg_3.SetItem(tRow, "ORDER_SEQ",  tRow);
        dg_3.SetItem(tRow, "FIELDNAME",  dg_1.GetItem(i, "FIELDNAME"));
        dg_3.SetItem(tRow, "FIELDTITLE", dg_1.GetItem(i, "HEADER_TEXT"));
        dg_3.SetItem(tRow, "EDITOR",     dg_1.GetItem(i, "EDITOR"));
      }
    }
  } else if(aDiv=="layout") {
    // vFreeform 수정 ( layout )
    var tLayouts = [],
        tCols    = [];
    var tIdx = 0;
    var tWidth = $("#ff-width").val(),
        tLblwidth = $("#ff-label-width").val(),
        tCtrlwidth = 0;

    $(".wrap-ff-array ul").each(function(i, e) {
      var li_elcnt = $(e).find("li span").length;
      tCtrlwidth = Math.floor( tWidth / li_elcnt ) - Number(tLblwidth);

      $( $(e).find("li span") ).each(function(j, el) {
        var tId = $(el).attr("id").replace("FFC_", "");
        tCols.push( tId );
        tIdx++;

        vFreeform.cols[ tId ].r = tIdx;
        vFreeform.cols[ tId ].w = tCtrlwidth;
        // var tRow = dg_3.InsertRow(0);
        // dg_3.SetItem(tRow, "FIELDNAME",  tId);
        // dg_3.SetItem(tRow, "FIELDTITLE", vFreeform.cols[tId].l.t);
        // dg_3.SetItem(tRow, "EDITOR",     pf_type_to_editor(vFreeform.cols[tId].c.t));
      });

      tLayouts.push( tCols );
      tCols = [];
    });

    vFreeform.layout = tLayouts;

    for(var i=1; i<=dg_3.RowCount(); i++) { 
      dg_3.SetItem(i, "ORDER_SEQ", vFreeform.cols[ dg_3.GetItem(i, "FIELDNAME") ].r);
    }
    dg_3.Sort([{"sortAsc": true,"sortCol":{"field":"ORDER_SEQ","dataType":"number"}}]);
  }
}


// FREEFORM 설정정보(칼럼정보로 부터 vFreeform 생성 후, preview 진행 )
// aDiv : layout, cols, all
var pf_setting_initial = function(aDiv) {
  if(!aDiv) aDiv = "all";

  var tColCnt = dg_3.RowCount(),
      tHtml = "",
      tmpHtml = "",
      tObjs = "",
      tRCnt = 0,
      tCCnt = Number($("#ff-colcnt").val()),
      tCidx = 0;

  var tTitle = $("#ff-title").val(),
      tWidth = $("#ff-width").val(),
      tLblwidth = $("#ff-label-width").val(),
      tCtrlwidth = Math.floor( tWidth / tCCnt ) - Number(tLblwidth) ,
      tFieldname = "",
      tEditor = "",
      tType = "",
      tVisible = "";

  var tArrShow = [];

  // tCtrlwidth 5px 단위로 올림 (px 단위일 때,)
  // 100 일때는 % 적용 
  switch(tWidth) {
    case "100":
      //tWrapWidth = " wr"+tWidth;
      tCtrlwidth = 150;
      break;
    default:
      tCtrlwidth = tCtrlwidth - tCtrlwidth%5 + 5;
  }

  // 기본정보 세팅
  vFreeform.setting = {
    t: tTitle,
    w: tWidth,
    c: tCCnt,
    wl: tLblwidth,
    wc: tCtrlwidth
  }
  if(aDiv=="all"||aDiv=="cols") vFreeform.cols = {};
  if(aDiv=="all"||aDiv=="layout") vFreeform.layout = [];

  var tFn = "",
      tFt = "",
      tEd = "";

  // 칼럼정보 세팅
  for(var i=1;i<=tColCnt;i++) {
    if(tCidx>0 && tCidx%tCCnt==0) {
      if(aDiv=="all"||aDiv=="cols") vFreeform.cols[tFn].c.w = ""; // 마지막 칼럼의 width 설정 삭제
      if(aDiv=="all"||aDiv=="layout") {
        vFreeform.layout.push( tArrShow );
        tArrShow = [];
        tCidx = 0;
      }
    }

    tFn = dg_3.GetItem(i, "FIELDNAME");
    tFt = dg_3.GetItem(i, "FIELDTITLE");
    tEd = dg_3.GetItem(i, "EDITOR");

    if(aDiv=="all"||aDiv=="layout") {
      tArrShow.push( tFn );
      tCidx++;
    }

    if(aDiv=="all"||aDiv=="cols") {
      switch(tEd) {
        case "_Editor_date" :
          tType = "date";
          break;
        case "_Editor_dropdown" :
          tType = "select";
          break;
        case "_Editor_checkbox" :
          tType = "checkbox";
          break;
        default :
          tType = "text";
      }

      vFreeform.cols[ tFn ] = {
                              r: i,
                              l: {
                                t: tFt,
                                w: tLblwidth,
                                v: "Y"
                              },
                              c: {
                                w: tCtrlwidth,
                                t: tType,
                                bl: "Y",
                                bt: "N",
                                s: ""
                              }
                            };
    }
  }

  vFreeform.cols[tFn].c.w = ""; // 마지막 칼럼의 width 설정 삭제
  vFreeform.layout.push( tArrShow );



  // console.log( JSON.stringify(vFreeform) )
  //pf_preview();
}
/* -- 샘플 

vFreeform = {
  setting: {
    t: "",    // title
    w: "700", // width
    c: 3,     // col_count
    wl: 100,  // label_width
    wc: 120   // control_width
  },
  layout: [["company_code", "proj_name"],["proj_name", "proc_div"],["remark", "share_yn"]],
  cols: {
    "company_code": {
      s: "0101",
      l: {t: "회사코드" // title, w: "80" // width, v: "Y" // visible },                              // label
      c: {w: "120" // width,t: "Checkbox" // type, bl: "Y" // borderleft, s: "detail_date" // style}  // control
    },
    "proj_code": {
      s: "0102",
      l: {t: "회사코드", w: "80", v: "Y"},
      c: {w: "120",t: "Checkbox",bl: "Y",s: "detail_date"}
    }
  }
}

*/




// FreeForm Preview ( vFreeform(json) 에서 Preview 만들기 )
var pf_preview = function(aIslayout) {
  if(aIslayout==null) aIslayout = true;

  // preview 생성
  var tHtml  = "",
      tHtml2 = "",
      tBtn   = "",
      tBtlcl = "",
      tCw    = "",
      tBleft = "",
      tTitle = vFreeform.setting.t,
      tWidth = vFreeform.setting.w,
      tCCnt  = Number(vFreeform.setting.c), // 칼럼에 ele 개수
      tRcnt  = vFreeform.layout.length,
      tUlid  = "",
      tWrapWidth = "";
  
  vIsPreview = true;
  if(aIslayout) $(".wrap-ff-array").html("");
  
  $(vFreeform.layout).each(function(i, e) {
    tCCnt = e.length; // 칼럼내 ele 개수
    if(aIslayout) {
      tUlid = "ff-row-"+(i+1);
      $(".wrap-ff-array").append( '<ul id="'+tUlid+'" class="ff-row connectedSortable"></ul>' );
    }

    $(e).each(function(j, el) {
      if(aIslayout) $("#"+tUlid).append( '<li><span id="FFC_'+el+'" col-name="'+el+'" class="label label-danger">'+el+'</span></li>' );

      tHtml2 += '    <label class="detail_label '+(vFreeform.cols[el].l.w?"w"+vFreeform.cols[el].l.w:"")+'" for="'+el+'">'+vFreeform.cols[el].l.t+'</label>\n';
      
      if(vFreeform.cols[el].c.w) {
        tCw = ' w'+vFreeform.cols[el].c.w;
      } else {
        tCw = '';
      }

      if(vFreeform.cols[el].c.bt=="Y") {
        tBtn   = '    <button id="BTN_'+el+'" class="search_find_img"></button>\n';
        tBtlcl = ' has_button';
      } else {
        tBtn = '';
        tBtlcl = '';
      }

      if(vFreeform.cols[el].c.bl=="N") {
        tBleft = " noborder";
      } else {
        tBleft = "";
      }

      switch(vFreeform.cols[el].c.t) {
        case "date" :
          tHtml2 += '    <div class="detail_input_bg'+tCw+tBtlcl+tBleft+'">\n      <input type="text" id="'+el+'" class="detail_date" />\n    </div>\n\n';
          break;
        case "select" :
          tHtml2 += '    <div class="detail_input_bg'+tCw+tBtlcl+tBleft+'">\n      <select id="'+el+'" ></select>\n    </div>\n\n';
          break;
        case "checkbox" :
          tHtml2 += '    <div class="detail_input_bg'+tCw+tBtlcl+tBleft+'">\n      <input type="checkbox" id="'+el+'" />\n'+tBtn+'    </div>\n\n';
          break;
        default :
          tHtml2 += '    <div class="detail_input_bg'+tCw+tBtlcl+tBleft+'">\n      <input type="text" id="'+el+'" />\n'+tBtn+'    </div>\n\n';
      }

    });

    // 전체 width (마지막 칼럼에 w가 있을때, row-closing 넣어주기 ) 
    if(tCw!='') {
      tHtml2 = tHtml2 + '    <div class="row-closing"></div>\n\n';
    }

    tHtml += '  <div class="detail_row'+(tRcnt==(i+1)?" detail_row_bb":"")+'">\n'+tHtml2+'  </div>\n\n';
    tHtml2 = "";
  });

  if(aIslayout) {
    // jquery sortable plugin 
    $( $(".wrap-ff-array ul") ).sortable({
      connectWith: ".connectedSortable",
      update : function(e, ui) { 
        if (this === ui.item.parent()[0]) {
          // layout 에서 dg_3(column list) 생성
          pf_ff_reset_start("layout");

          // dg_3(column list) 에서 vFreeform 생성 (layout or cols)
          // pf_setting_initial("layout");

          // vFreeform에서 Layout과 Html 그리기 
          pf_preview(false);
        }
      }
    }).disableSelection();

    // 컬럼선택 이벤트 추가
    $(".wrap-ff-array span.label").off().on("click", function() {
      var tCname = $(this).attr("col-name");

      // 칼럼 클릭 이벤트 모음 
      pf_event_col(tCname);
    });
  }

  // 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 일때는 % 적용 
  switch(tWidth) {
    case "100":
      tWrapWidth = " wr"+tWidth;
      break;
    default:
      tWrapWidth = " w"+tWidth;
  }

  tHtml = '<div id="freeform" class="detail_box ver2'+tWrapWidth+(tTitle?" has_title":"")+'">\n' 
        + (tTitle?'  <label class="sub_title">'+tTitle+'</label>\n':'')
        + tHtml 
        + '</div>';

  //$(".wrap-ff-preview").html( tHtml );
  $(".wrap-ff-preview").html('<div id="ff-src" style="width:'+tWidth+(tWidth=="100"?"%":"")+'">\n'+tHtml+'\n</div>');
  $("#code-html").val(tHtml);

  // 프리폼 이벤트 적용 
  xm_InputInitial();

  // 칼럼(label) 클릭 이벤트 추가 
  $(".wrap-ff-preview label").off().on("click", function() {
    var tCname = $(this).attr("for");
    
    // 칼럼 클릭 이벤트 모음 
    pf_event_col(tCname);
  });
  // 칼럼(div) 클릭 이벤트 추가 
  $(".wrap-ff-preview .detail_input_bg").off().on("click", function() {
    // console.log( $(this).find("input, select, textarea").attr("id") )
    var tCname = $(this).find("input, select, textarea").attr("id");

    // 칼럼 클릭 이벤트 모음 
    pf_event_col(tCname);
  });

  $("#freeform_setting").css("height", $("#ff-preview .panel-body").height() - 5);

  // 칼럼 width 찌그러진곳 찾기
  pf_chk_validate();
}

// 클립보드 복사
var copyToClipboard = function(str) {
  var $temp = $("<textarea>");
  $("body").append($temp);
  $temp.val(str).select();
  document.execCommand("copy");
  $temp.remove();
}

// 패널 열고 닫기
var pf_show_panel = function(aObj) {
  var tTitle = $(aObj).html();
  var tPanel = $(aObj).attr("aria-controls");

  if(tTitle.indexOf("▼")>0) {
    // 열기
    $(".wrap-"+tPanel).show();
    if(tPanel=="ff-preview") $(".wrap-ff-setting").show();
    $(aObj).html(tTitle.replace("▼", "▲"));
  } else if(tTitle.indexOf("▲")>0) { 
    // 닫기 
    $(".wrap-"+tPanel).hide();
    if(tPanel=="ff-preview") $(".wrap-ff-setting").hide();
    $(aObj).html(tTitle.replace("▲", "▼"));
  }
  //($(this).attr("aria-controls")); //
}

// 칼럼 클릭시 이벤트 처리 
var pf_event_col = function(aCname) {
  // layout 에서 Selected 처리 
  pf_sel_column(aCname);
  // preview 에서 Selected 처리
  pf_sel_col_pv(aCname);
  // column 목록에서 Selected 처리 
  pf_sel_column_list(aCname);
  // setting 정보 세팅
  pf_set_setting(aCname);
}

// designer 에서 컬럼 클릭
var pf_sel_column = function(aCname) {
  // 디자이너 선택표시 
  $(".wrap-ff-array span.label-warning").removeClass("label-warning").addClass("label-danger");
  $("#FFC_"+aCname).removeClass("label-danger").addClass("label-warning");
}

// preview 에서 컬럼 클릭
var pf_sel_col_pv = function(aCname) {
  $(".wrap-ff-preview .selected").removeClass("selected");

  $("label[for="+aCname+"]").addClass("selected");
  $("#"+aCname).parent().addClass("selected");
}

// column 목록에서 Selected 처리 
var pf_sel_column_list = function(aCname) {
  var tRow = dg_3.RowCount();
  for(var i=1;i<=tRow;i++) {
    if(dg_3.GetItem(i, "FIELDNAME")==aCname) {
      dg_3.SetRow(i);
    }
  }
}

// label, column 정보 setting
var pf_set_setting = function(aCname) {
  var tCol = aCname.toUpperCase();
  var tCtitle = vFreeform.cols[tCol].l.t;
  var tEditor = vFreeform.cols[tCol].c.t;
  var tObjLabel = $(".wrap-ff-preview label[for="+tCol+"]");
  var tObjWrap = $("#"+tCol).parent();
  curCol = tCol;
  // $(".wrap-ff-preview prev")

  // Label
  $("#setting-l-title").val(tCtitle);
  $("#setting-l-width").val( vFreeform.cols[tCol].l.w );
  $("#setting-l-visible").prop("checked", (vFreeform.cols[tCol].l.v=="Y" ? true : false) );

  // Input
  
  $("#setting-i-width").val( vFreeform.cols[tCol].c.w );
  $("#setting-i-type").val( tEditor );
  $("#setting-i-border").prop("checked", (vFreeform.cols[tCol].c.bl=="Y" ? true : false) );
  if(tEditor=="text") {
    $("#setting-i-button").prop("disabled", false );
    $("#setting-i-button").prop("checked", (vFreeform.cols[tCol].c.bt=="Y" ? true : false) );
  } else {
    $("#setting-i-button").prop("disabled", true );
    $("#setting-i-button").prop("checked", false );
  }
  
  
  // $("#setting-l-width").val( $(tObjLabel).outerWidth() );
  // $("#setting-i-width").val( $(tObjWrap).outerWidth() );

}

// type 문자열에서 Editor 문자열 가져오기
var pf_type_to_editor = function(aType) {
  var tRtn = "";

  switch(aType) {
    case "date" :
      tRtn = "_Editor_date";
      break;
    case "select" :
      tRtn = "_Editor_dropdown";
      break;
    case "checkbox" :
      tRtn = "_Editor_checkbox";
      break;
    default :
      tRtn = "_Editor_text";
  }

  return tRtn;
}

// 이벤트 바인딩
var pf_bind_event = function() {
  // setting-l-title , setting-l-width , setting-l-visible
  // setting-i-width , setting-i-type  , setting-i-border  , setting-i-button
  $("#setting-l-title").on("change", function() {
    vFreeform.cols[curCol].l.t = $(this).val();
    dg_3.SetItem( dg_3.GetRow(), "FIELDTITLE", $(this).val() );
    pf_preview();

    // layout 에서 Selected 처리 
    pf_sel_column(curCol);
    // preview 에서 Selected 처리
    pf_sel_col_pv(curCol);
  });

  $("#setting-l-width").on("change", function() {
    vFreeform.cols[curCol].l.w = $(this).val();
    pf_preview();

    // layout 에서 Selected 처리 
    pf_sel_column(curCol);
    // preview 에서 Selected 처리
    pf_sel_col_pv(curCol);
  });

  $("#setting-i-width").on("change", function() {
    vFreeform.cols[curCol].c.w = $(this).val();
    pf_preview();

    // layout 에서 Selected 처리 
    pf_sel_column(curCol);
    // preview 에서 Selected 처리
    pf_sel_col_pv(curCol);
  });

  $("#setting-i-type").on("change", function() {
    vFreeform.cols[curCol].c.t = $(this).val();
    pf_preview();

    // layout 에서 Selected 처리 
    pf_sel_column(curCol);
    // preview 에서 Selected 처리
    pf_sel_col_pv(curCol);
  });

  $("#setting-i-button").on("click", function() {
    vFreeform.cols[curCol].c.bt = ( $(this).prop("checked") ? "Y" : "N" );
    pf_preview();

    // layout 에서 Selected 처리 
    pf_sel_column(curCol);
    // preview 에서 Selected 처리
    pf_sel_col_pv(curCol);
  });

  $("#setting-i-border").on("click", function() {
    vFreeform.cols[curCol].c.bl = ( $(this).prop("checked") ? "Y" : "N" );
    pf_preview();

    // layout 에서 Selected 처리 
    pf_sel_column(curCol);
    // preview 에서 Selected 처리
    pf_sel_col_pv(curCol);
  });

}

// preview 에서 width이상(anomaly) 발견하기
var pf_chk_validate = function() {
  var tPnum = /[0-9]/

  $("label.detail_label").each(function(i, e) {
    var tClass = $(e).attr("class");
    var tId = $(this).attr("for");

    $(tClass.split(" ")).each(function(j, el) {
      if(tPnum.test(el)) {
        var tW = el.replace("w", "");
        if( tPnum.test( tW ) && (Number(tW)-10)!=$(e).outerWidth() ) {
          $("label[for='"+tId+"']").css("background-color", "#f2dede");
        }
      }
    });
    // tClass = tClass.replace(/[a-w]|xyz|\s|\_/g, '');

    
  });

  $("div.detail_input_bg").each(function(i, e) {
    var tObj = $(e);
    var tClass = $(e).attr("class");
    var tId = $(this).find("input, select, checkbox").attr("id");

    $(tClass.split(" ")).each(function(j, el) {
      if(tPnum.test(el)) {
        var tW = el.replace("w", "");
        if( tPnum.test( tW ) && (Number(tW))!=$(e).outerWidth() ) {
          $(tObj).css("background-color", "#f2dede");
        }
      }
    });
  });
}

/* -- 샘플 

vFreeform = {
  layout: [["company_code", "proj_name"],["proj_name", "proc_div"],["remark", "share_yn"]],
  setting: {
    "company_code": {
      s: "0101",
      label: {title: "회사코드", width: "80", visible: "Y"},
      control: {width: "120",type: "Checkbox",borderleft: "Y",style: "detail_date"}
    },
    "proj_code": {
      s: "0101",
      label: {title: "회사코드", width: "80", visible: "Y"},
      control: {width: "120",type: "Checkbox",borderleft: "Y",style: "detail_date"}
    }
  }
}

*/
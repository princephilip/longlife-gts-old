//var _Theme = getTheme();
var iconColumn1;
var itemIconClickHandler = function(event) {xe_M_GridItemIconClick(dg_1,event);}

//화면이 로딩된 이후 초기값 설정
function x_InitForm()
{
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM900100", "XG_GRID_COLS_BASE|GRID_COLS_BASE");
}

//Grid 설정이 완료된 이후 호출
function xe_GridLayoutComplete(a_obj){
	if(a_obj.id=="dg_1"){
		var coStyles = [{"code":"","label":" "},{"code":"_Styles_text","label":"text"},{"code":"_Styles_textc","label":"textc"},{"code":"_Styles_number","label":"number"},{"code":"_Styles_numberc","label":"numberc"},{"code":"_Styles_numberf","label":"numberf"},{"code":"_Styles_checkbox","label":"checkbox"},{"code":"_Styles_date","label":"date"},{"code":"_Styles_datetime","label":"datetime"}];
		dg_1.SetCombo("STYLES", coStyles);	
		var coDataTypes = [{"code":"","label":" "},{"code":"text","label":"text"},{"code":"bool","label":"bool"},{"code":"number","label":"number"},{"code":"datetime","label":"datetime"}];
		dg_1.SetCombo("DATA_TYPE", coDataTypes);
		var coRenderer = [{"code":"","label":" "},{"code":"check_YN","label":"check_YN"}];
		dg_1.SetCombo("RENDERER", coRenderer);

		var coButton = [{"code":"","label":" "},{"code":"action","label":"action"},{"code":"popup","label":"popup"}];
		dg_1.SetCombo("BUTTON", coButton);
		
		dg_1.SetFixed(3);
	}
	
}

//찾기 화면에서 선택값을 넘긴 경우 호출
function x_ReceivedCode(){
}

//조회 버튼 클릭
function x_DAO_Retrieve(a_dg)
{	
	if(a_dg==null || a_dg.id=="dg_1") {	
	  var param = new Array(S_SYS_ID.value, S_FIELD_NAME.value);
		dg_1.Retrieve(param);
	}
}

//저장 버튼 클릭
function x_DAO_Save(a_dg)
{
	var errRow = pf_errCheck();
	if (errRow>0) {
		dg_1.SetRow(errRow);
	  dg_1.focus();
		return;
	}
	setTimeout("x_SaveGridData()",0);

}

//데이타 에러 체크
function pf_errCheck()
{
	return 0;
}

//추가,삽입 버튼 클릭
function x_DAO_Insert(a_dg, row)
{
	a_dg.InsertRow(row);
}

//데이타 추가 후 호출
function x_Insert_After(a_dg, rowIdx){
	if(a_dg.id=="dg_1") {
		dg_1.SetItem(rowIdx, "SYS_ID", S_SYS_ID.value);
	}
}

//삭제 버튼 클릭
function x_DAO_Delete(a_dg, row)
{
	a_dg.DeleteRow(row);
}

//복제 버튼 클릭
function x_DAO_Duplicate(a_dg, row){

	a_dg.DuplicateRow(row);

}

//데이타 복제 후 호출
function x_Duplicate_After(a_dg, rowIdx){
	if(a_dg.id=="dg_1") {
		dg_1.SetItem(rowIdx, "FIELD_NAME", null);
	}
}

//엑셀 버튼 클릭
function x_DAO_Excel(a_dg){
	//alert(dg_1.RowCount());
	a_dg.ExcelExport();
}

//인쇄 버튼 클릭
function x_DAO_Print(a_dg){
}

//입력 컨트롤의 데이타 값이 변경된 경우 호출
function xe_EditChanged(a_obj, a_val, a_label, a_cobj)
{
	switch(a_obj.id) {
		case "S_SYS_ID":
			x_DAO_Retrieve();
			break;
	}
}

//입력 컨트롤에서 키가 눌러진 경우 호출
function xe_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey)
{
}

//입력 컨트롤에서 Enter키가 눌러진 경우 호출
function xe_InputKeyEnter(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey)
{
	switch(a_obj.id) {
		case "S_FIELD_NAME":
			x_DAO_Retrieve();
			break;
	}
}

//Grid 컨트롤의 Row 선택이 변경된 경우 호출
function xe_GridRowFocusChange(a_dg, a_newrow, a_oldrow)
{
	return true;
}

//Grid 컨트롤의 Cell 선택이 변경된 경우 호출
function xe_GridItemFocusChange(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol)
{
	//alert(a_dg.id + " : " + a_newrow + "," + a_newcol + " : " + a_oldrow + "," + a_oldcol);
	return true;
}

function xe_GridOnLoad(a_dg){
}

function xe_GridDoubleClick(a_dg, a_event){

}

function xe_GridItemClick(a_dg, a_row, a_col, a_colname){
	//alert("xe_GridItemClick(" + a_row + ", " + a_col + ") " + a_colname);
}

function xe_GridItemDoubleClick(a_dg, a_row, a_col, a_colname){
	//alert("xe_GridItemDoubleClick(" + a_row + ", " + a_col + ") " + a_colname);
}

function xe_GridButtonClick(a_dg, a_row, a_col, a_colname){
	if(a_dg.id=="dg_1" && a_colname == "BASE_APPLY") {
		pf_applyfrombase(a_dg.GetItem(a_row,"FIELD_NAME"));
	}
}

function xe_GridHeaderClick(a_dg, a_col, a_colname){
}

function xe_GridContextMenuItemSelect(a_dg, a_label, a_row, a_col, a_colname){
	alert("xe_GridContextMenuItemSelect(" + a_label + "," + a_row + ", " + a_col + ") " + a_colname);
}

function xe_GridMenuItemSelect(a_dg, a_label, a_checked, a_tag){
	alert("xe_GridMenuItemSelect(" + a_label + "," + a_checked + ", " + a_tag + ") ");
}

function xe_GridScrollToBottom(a_dg){
	//alert("xe_GridScrollToBottom(" + a_obj.id + ")");
}



function xe_GridKeyDown(a_dg, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey)
{
	alert('xe_GridKeyDown ' + a_dg.id + " : " + a_keyCode + a_ctrlKey + " : " +  a_altKey + " : " +  a_shiftKey);
}

function xe_GridKeyUp(a_dg, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey)
{
	alert('xe_GridKeyUp ' + a_dg.id + " : " + a_keyCode + a_ctrlKey + " : " +  a_altKey + " : " +  a_shiftKey);
}

function xe_GridDataChange(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	//alert("xe_GridDataChange(" + a_row + " , " + a_col + ") : " + a_newvalue);
}

function itemEditEndChecker(rowIndex, columnIndex, item, dataField, oldValue, newValue) {
}

function itemEditBeginningChecker(rowIndex, columnIndex, item, dataField) {
	//입력모드가 아닌경우 편집 불가
	return true;
}


function xe_GridSearchChange(a_dg){

}

function xe_GridDataLoad(a_dg){
}

function xe_GridItemIconClick(a_dg, a_RowIndex, a_ColumnIndex){
}

//닫기 버튼 클릭
function x_Close() {
	_X.CloseSheet(window);
}




//=========================================================================================================
// 사용자 정의 함수
//=========================================================================================================

//표준에 적용
function pf_applyfrombase(fieldName) {

	if(_X.MsgBoxYesNo("설정된 Grid 칼럼정보를 단위 시스템에<br/>적용하시겠습니까?","",2)!=1) return;

	var row = dg_1.GetRow();
	if (row > 0) {
		var subSystem = dg_1.GetItem(row, 'SYS_ID');

		_X.ExecProc("sm", "SP_XG_GridSchima_FromBase", new Array(subSystem, fieldName));
	}
	
}
/**
 *<pre>
 * Statements
 *</pre>

 * @Class Name : SM0901.js
 * @Descriptio	  시스템코드 관리
 * @Modification Information
 * @
 * @  수정일    	 수정자           수정내용
 * @ -------    --------    ---------------------------
 * @ 2013.03.03    박영찬        최초 생성
 * @
 */
function x_InitForm2(){
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM0901", "SM_AUTH_SYS|C_SM0901_01");
	return 100;
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		x_DAO_Retrieve(a_dg);
	}
	return 100;
}

function x_DAO_Retrieve2(a_dg){
	dg_1.Retrieve();
	return 0;
}

function xe_EditChanged2(a_obj, a_val){return 100;}
function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){return 100;}
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){return 100;}
function x_DAO_Save2(a_dg){return 100;}
function x_DAO_Saved2(){return 100;}
//데이타 유효성 check
function x_DAO_ChkErr2(){
	var cData = dg_1.GetChangedData();

	var sNo = 0;
	var chkMsg = "";

	if(cData==null || cData=="") return false;

	for(var i=0; i<cData.length; i++){
		if(cData[i].job == "D") continue;
		sNo = cData[i].idx;
		//중복체크...
		if (cData[i].job=="I") {
			if ((cData[i].data["SYS_ID"])==false) {
				chkMsg = eval(sNo) + " 번째 행에 중복된 코드가 존재합니다";
				break;
			}
		}
	}

	if(chkMsg!="") {
		_X.MsgBox('확인', chkMsg);
		dg_1.SetRow(sNo);
		dg_1.focus();
		return false;
	}
	return true;
}

function x_DAO_Insert2(a_dg, row){return 100;}
function x_Insert_After2(a_dg, rowIdx){
	//a_dg.SetItem(rowIdx,"COMPANY_CODE",mytop._CompanyCode);
	return 100;
}
function x_DAO_Duplicate2(a_dg, row){return 100;}
function x_Duplicate_After2(a_dg, rowIdx){
	a_dg.SetItem(rowIdx,"SYS_ID","");
	return 100;
}
function x_DAO_Delete2(a_dg, row){return 100;}
function x_DAO_Excel2(a_dg){return 100;}
function x_DAO_Print2(){return 100;}
function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}
function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}
function xe_GridDataChange2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){return 100;}
function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){return true;}
function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){return true;}
function xe_GridDataLoad2(a_dg){return 100;}
function x_FindCode2(a_dg, a_row, a_findKey, a_findstr, a_win) {return 100;}
function x_ReceivedCode2(){return 100;}
function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){return 100;}
function xe_GridHeaderClick2(a_dg, a_col, a_colname){return 100;}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){return 100;}
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){return 100;}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){return 100;}
function xe_ChartPointClick2(a_dg, a_event){return 100;}
function xe_ChartPointSelect2(a_dg, a_event){return 100;}
function xe_ChartPointMouseOver2(a_dg){return 100;}
function xe_ChartPointMouseOut2(a_dg, a_event){return 100;}
function x_Close2() {return 100;}
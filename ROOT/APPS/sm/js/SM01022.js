/**
 *<pre>
 * Statements
 *</pre>

 * @Class Name : SM01022.js
 * @Descriptio	 거래처관리
 * @Modification Information
 * @
 * @   수정일       수정자               수정내용
 * @ ----------    --------    ---------------------------
 * @  20161206 			srJin					최초수정작성
 */

Tab_Idx = 0;
var _IsMaster = _X.HaveActAuth('본사노임계좌관리') || _X.HaveActAuth('시스템관리자');

function x_InitForm2(){
	$(".search_find_img").click(function() {
		pf_ZipFind($(this).attr("id"));
	});

	_X.Tabs(tabs_1, 0);
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM01022", "SM01022|SM01022_01", true, true);

	if(!_IsMaster){ //재경만 사용여부 설정 권한(20180517 문석기C요청)
		$("#USE_YN").attr("readonly",true);
		$("#USE_YN").attr("disabled",true);
		$("#USE_REMARK").attr("readonly",true);
	}
	return 100;
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {

//		if(dg_1.RowCount() <= 0){
//			_X.FormSetAllDisable("freeform", true);
//			_X.FormSetAllDisable("freeform2", true);
//		}

	if(!_IsMaster){ //재경만 사용여부 설정 권한(20180517 문석기C요청)
		$("#USE_YN").attr("readonly",true);
		$("#USE_YN").attr("disabled",true);
		$("#USE_REMARK").attr("readonly",true);
	}

		//드랍다운 구성데이터 쿼리 매개변수에 담음.
		var	comboData   = _X.XmlSelect("sm", "SM01022", "R_SM01022_03", new Array('SM','CUSTTAG')		 			 	 , "json2");			 //거래처구분
		var	comboData2  = _X.XmlSelect("sm", "SM01022", "R_SM01022_03", new Array('SM','TRADETYPE')	 			 	 , "json2");		 	 //거래유형
		var	comboData3  = _X.XmlSelect("sm", "SM01022", "R_SM01022_03", new Array('SM','BUSINESSTYPE')			 , "json2");       //과세정보
		var	comboData4  = _X.XmlSelect("sm", "SM01022", "R_SM01022_03", new Array('SM','PAYGROUP')	 				 , "json2");			 //지급그룹
		var	comboData5  = _X.XmlSelect("sm", "SM01022", "R_SM01022_03", new Array('PM','PAYTAG')	 				 	 , "json2");			 //지불구분
		// 지불구분에 '자동' 추가
		tagInfo = new Object();
		tagInfo.code  = "";
		tagInfo.label = "자동";
		comboData5.unshift(tagInfo);

		_X.DDLB_SetData(CUST_TAG			, comboData 		, null, false, false); 	 //거래처구분 	드랍다운리스트 셋팅 . 쿼리에서 code, label 알리아스 지정해줘야함.
		_X.DDLB_SetData(TRADE_TYPE		, comboData2		, null, false, false); 	 //거래유형 		드랍다운리스트 셋팅
		_X.DDLB_SetData(BUSINESS_TYPE	, comboData3		, null, false, false); 	 //과세정보 		드랍다운리스트 셋팅
		_X.DDLB_SetData(PAY_GROUP			, comboData4		, null, false, false); 	 //지급그룹 		드랍다운리스트 셋팅
		_X.DDLB_SetData(PAY_TAG				, comboData5		, null, false, false); 	 //지불구분 		드랍다운리스트 셋팅

		dg_1.SetCombo("CUST_TAG"		 , comboData );			// 그리드 코드->한글 , grid 파일에서 lookupDispay:true 걸어줘야함. 쿼리에서 code, label 알리아스 지정해줘야함.
		dg_1.SetCombo("TRADE_TYPE"	 , comboData2);			// 그리드 코드->한글
		dg_1.SetCombo("BUSINESS_TYPE", comboData3);			// 그리드 코드->한글
		dg_1.SetCombo("PAY_GROUP"		 , comboData4);			// 그리드 코드->한글
		dg_1.SetCombo("PAY_TAG"		 	 , comboData5);			// 그리드 코드->한글

		x_DAO_Retrieve2(dg_1);
	}
	return 100;
}

function x_DAO_Retrieve2(a_dg){

	//조회조건 : 기둘
	// 첫 탭의 배열번호가 0이라서 하위 CASE '0' 과 같이 맞춰서 쓸 변수들 초기화.
	var ls_cust_tag = '0';
	    ls_closing_yn = "Y";

	var ls_vendor = S_VENDOR_NAME.value; // 거래처명 or 코드검색
  		if(ls_vendor == "" || S_VENDOR_NAME.length <= 0) ls_vendor = '%';

	switch(Tab_Idx)	{
		// 전체
		case 0 :
			ls_cust_tag = "%"; ls_closing_yn = "Y";
			break;
		// 일반사업자
		case 1 :
			ls_cust_tag = "1"; ls_closing_yn = "Y";
			break;
		// 폐업자
		case 2 :
			ls_cust_tag = "%"; ls_closing_yn = "N";
			break;
		// 개인
		case 3 :
			ls_cust_tag = "2"; ls_closing_yn = "%";
			break;
		// 공공기관
		case 4 :
			ls_cust_tag = "7"; ls_closing_yn = "%";
			break;
		// 금융기관
		case 5 :
			ls_cust_tag = "4"; ls_closing_yn = "%";
			break;
		// 기타
		case 6 :
			ls_cust_tag = "6"; ls_closing_yn = "%";
			break;
		// 사원
		case 7 :
			ls_cust_tag = "5"; ls_closing_yn = "%";
			break;
		// 부서
		case 8 :
			ls_cust_tag = "3"; ls_closing_yn = "%";
			break;
		// 건설사
		case 9 :
			ls_cust_tag = "9"; ls_closing_yn = "%";
			break;
	}

	var param = new Array(ls_cust_tag,  ls_closing_yn=="N"?"%":S_USE_YESNO.value, ls_closing_yn, ls_vendor,  S_DIV.value, '%');
	//alert(param);
	dg_1.Retrieve(param);

	if(dg_1.RowCount() <= 0){
		_X.FormSetAllDisable("freeform", true);
		_X.FormSetAllDisable("freeform2", true);
	} else {
		_X.FormSetAllDisable("freeform", false);
		_X.FormSetAllDisable("freeform2", false);
	}

	return 0;
}

function xe_EditChanged2(a_obj, a_val){
	if(a_obj.id == 'S_USE_YESNO' || a_obj.id == 'S_DIV'){
		x_DAO_Retrieve2(dg_1);
		if(dg_1.RowCount() <= 0){
			_X.FormSetAllDisable("freeform", true);
			_X.FormSetAllDisable("freeform2", true);
		} else {
			_X.FormSetAllDisable("freeform", false);
			_X.FormSetAllDisable("freeform2", false);
		}
	}
	if(a_obj.id == 'BUSINESS_CHK_DATE'){
		dg_1.SetItem(dg_1.GetRow(), 'BUSINESS_CHK_DATE' , BUSINESS_CHK_DATE.value.replace(/\-/g,'') )
	}
	if(a_obj.id == 'CLOSING_DATE'){
		dg_1.SetItem(dg_1.GetRow(), 'CLOSING_DATE' 			, CLOSING_DATE.value.replace(/\-/g,'') )
	}
	if(a_obj.id == 'DEPOSIT_FROMDATE'){
		dg_1.SetItem(dg_1.GetRow(), 'DEPOSIT_FROMDATE' 	, DEPOSIT_FROMDATE.value.replace(/\-/g,'') )
	}
	if(a_obj.id == 'DEPOSIT_TODATE'){
		dg_1.SetItem(dg_1.GetRow(), 'DEPOSIT_TODATE' 		, DEPOSIT_TODATE.value.replace(/\-/g,'') )
	}
	if(a_obj.id == 'INPUT_DATE'){
		dg_1.SetItem(dg_1.GetRow(), 'INPUT_DATE' 				, INPUT_DATE.value.replace(/\-/g,'') )
	}
	return 100;
}

//enter key event 발생시 호출
function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	switch(a_obj){
		case S_VENDOR_NAME:
			x_DAO_Retrieve2(dg_1);
			break;
	}
	return 0;
}

//keydown event 발생시 호출
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){
}

//데이타 저장
function x_DAO_Save2(a_dg){return 100;}

//데이타 저장후 callback
function x_DAO_Saved2(){

	return 100;
}

//데이타 유효성 check
function x_DAO_ChkErr2(){

	var cData = dg_1.GetChangedData();
	if(cData==null || cData=="") return true;

	var sNo = 0;
	var chkMsg = "";

	for(var i=0; i<cData.length; i++){
		if(cData[i].job == "D") continue;

		sNo = cData[i].idx;

		if (cData[i].job=="I") {

			if(Tab_Idx == 2){
/*				if(cData[i].data["CLOSING_DATE"] == '' || cData[i].data["CLOSING_DATE"] == null || cData[i].data["CLOSING_DATE"] == ' ' ){
						chkMsg = sNo + "행에 폐업일자를 입력해주세요.";
						_X.MsgBox("확인", chkMsg);
						return 0;
					}*/
			}

			if (cData[i].data["CUST_NAME"] == null || cData[i].data["CUST_NAME"] == '' || cData[i].data["CUST_NAME"] == ' ') {
				chkMsg = sNo + " 번째 행에 거래처명을 기입하시기 바랍니다.";
				_X.MsgBox("확인", chkMsg);
				return 0;
			}

			if (cData[i].data["TRADE_TYPE"] == null || cData[i].data["TRADE_TYPE"] == '' || cData[i].data["TRADE_TYPE"] == ' ') {
				chkMsg = sNo + " 번째 행에 거래유형을 선택하시기 바랍니다.";
				_X.MsgBox("확인", chkMsg);
				return 0;
			}

			if (cData[i].data["CUST_CODE"].indexOf('-')>0) {
				chkMsg = "사업자번호에 -를 제외하고 입력하시기 바랍니다.";
				_X.MsgBox("확인", chkMsg);
				return 0;
			}

			for(var j=0;j < dg_1.RowCount(); j++){
				if(j==sNo) continue;
				if(dg_1.GetItem(j,"CUST_CODE") == cData[i].data["CUST_CODE"]){
					chkMsg = sNo + "행에 중복된 [거래처코드]가 있습니다.";
					_X.MsgBox("확인", chkMsg);
					return 0;
				}

/*				if(dg_1.GetItem(j,"CUST_NAME") == cData[i].data["CUST_NAME"]){
					chkMsg = sNo + "행에 중복된 [상호(거래처명)]이 있습니다.";
					_X.MsgBox("확인", chkMsg);
					return 0;
				}

				if(dg_1.GetItem(j,"CUST_NAME") == cData[i].data["CUST_NAME"]){
					chkMsg = sNo + "행에 중복된 [거래처명]이 있습니다.";
					_X.MsgBox("확인", chkMsg);
					return 0;
				}*/
			}
		}
	}

	return true;
}

//grid insert/add
function x_DAO_Insert2(a_dg, row){return 100;}

//grid insert 수행후 발생
function x_Insert_After2(a_dg, rowIdx){
	if(S_DIV.value == 'O' || S_DIV.value == 'A' || S_DIV.value == 'R'){
		dg_1.SetItem(rowIdx, 'CUSTOMER_USE', S_DIV.value);
	}
	switch(Tab_Idx)	{
		// 일반
		case 0 :
			dg_1.SetItem(rowIdx, 'CUST_TAG', '1');
			break;
		// 폐업자
		case 1 :
			dg_1.SetItem(rowIdx, 'CUST_TAG', '1');
			break;
		// 개인
		case 2 :
			dg_1.SetItem(rowIdx, 'CUST_TAG', '2');
			break;
		// 공공기관
		case 3 :
			dg_1.SetItem(rowIdx, 'CUST_TAG', '7');
			break;
		// 금융기관
		case 4 :
			dg_1.SetItem(rowIdx, 'CUST_TAG', '4');
			break;
		// 기타
		case 5 :
			dg_1.SetItem(rowIdx, 'CUST_TAG', '6');
			break;
		// 사원
		case 6 :
			dg_1.SetItem(rowIdx, 'CUST_TAG', '5');
			break;
		// 부서
		case 7 :
			dg_1.SetItem(rowIdx, 'CUST_TAG', '3');
			break;
		// 건설사
		case 8 :
			dg_1.SetItem(rowIdx, 'CUST_TAG', '9');
			break;
	}
	dg_1.SetItem(rowIdx, 'USE_YN', 'Y');

//	dg_1.SetItem(rowIdx, "cust_tag", ls_cust_tag);
//	fdw_detail2.SetItem(rowIdx, "cust_tag", ls_cust_tag);
//
//	dg_1.SetItem(rowIdx, "use_yn", "Y");
//	fdw_detail2.SetItem(rowIdx, "use_yn", "Y");
//
//	dg_1.Sefdw_detail2tItem(rowIdx, "customer_use", "O");
//	.SetItem(rowIdx, "customer_use", "O");
//
//	dg_1.SetItem(rowIdx, "serial_number", "0000");
//	fdw_detail2.SetItem(rowIdx, "serial_number", "0000");
//
//	dg_1.SetItem(rowIdx, "emp_no", mytop.gs_user_id);
//	fdw_detail2.SetItem(rowIdx, "emp_no", mytop.gs_user_id);
	return 0;
}

//grid 복제
function x_DAO_Duplicate2(a_dg, row){return 100;}

//grid 복제후
function x_Duplicate_After2(a_dg, rowIdx){return 100;}

//grid 삭제
function x_DAO_Delete2(a_dg, row){
	var ls_cust = dg_1.GetItem(dg_1.GetRow(), "CUST_CODE");

	var li_cnt = _X.XS("sm", "SM01022", "R_SM01022_07", new Array(ls_cust), "json2");

  if(li_cnt[0].cnt > 0) {
  	_X.MsgBox("확인", "사용중인 거래처 입니다.");
  	return 0;
 	}

	if(_X.MsgBoxYesNo("선택된 거래처 정보를 삭제 하시겠습니까?") == "2") return;

	_X.ExecSql("SM", "SM01022", "CUST_DELECT_LOG", [ls_cust, dg_1.GetItem(dg_1.GetRow(), "CUST_TAG"), dg_1.GetItem(dg_1.GetRow(), "CUST_NAME"),mytop._UserID,mytop._ClientIP]);

	return 100;
}

//excel 저장
function x_DAO_Excel2(a_dg){return 100;}

//출력
function x_DAO_Print2(){return 100;}

//tab 변경시
function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}

//tab 변경시
function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	Tab_Idx = a_new_idx; // 탭 넘버넘김.(전역변수)

	if(Tab_Idx==2) {
		$(".detail_cust").inputmask("999999-9999999",{placeholder:" ", clearMaskOnLostFocus: true });
	} else {
		$(".detail_cust").inputmask("999-99-99999",{placeholder:" ", clearMaskOnLostFocus: true });
	}

	x_DAO_Retrieve2(dg_1); // 조회.

	return 100;
}

//grid data 변경시
function xe_GridDataChange2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){

	return 0;
}

//grid row focus change
function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){

	return true;
}

//grid row focus change
function xe_GridRowFocusChanged2(a_dg, a_newrow, a_oldrow){
	if(a_dg.id == 'dg_1'){
		if(_X.Trim(dg_1.GetItem(a_newrow, 'CUST_CODE')) != '' && dg_1.GetRowState(a_newrow) == "N"){
			  $("input[id=CUST_CODE]").attr("readonly",true);
		} else{
				$("input[id=CUST_CODE]").attr("readonly",false);
		}
		if(dg_1.RowCount() <= 0){
			_X.FormSetAllDisable("freeform", true);
			_X.FormSetAllDisable("freeform2", true);
		} else {
			_X.FormSetAllDisable("freeform", false);
			_X.FormSetAllDisable("freeform2", false);
		}
	}

		//pf_check_proc_div(a_newrow);

//	switch(dg_1.GetItem(a_newrow, "USER_TAG")) {
//		case "":
//			$("#btn_deptfind").hide();
//			$("#btn_centerfind").hide();
//			$("#btn_vendorfind").hide();
//			break;
//
//		case "1":
//			_X.FormSetDisable(new Array(DEPT_CODE, DEPT_NAME), false);
//			$("#btn_deptfind").show();
//			$("#btn_centerfind").hide();
//			$("#btn_vendorfind").hide();
//			break;
//
//		case "2":
//			_X.FormSetDisable(new Array(DEPT_CODE, DEPT_NAME), true);
//			$("#btn_deptfind").hide();
//			$("#btn_centerfind").hide();
//			$("#btn_vendorfind").hide();
//			break;
//
//		case "3":
//			_X.FormSetDisable(new Array(DEPT_CODE, DEPT_NAME), true);
//			$("#btn_deptfind").hide();
//			$("#btn_centerfind").hide();
//			$("#btn_vendorfind").show();
//			break;
//
//		case "4":
//			_X.FormSetDisable(new Array(DEPT_CODE, DEPT_NAME), true);
//			$("#btn_deptfind").hide();
//			$("#btn_centerfind").hide();
//			$("#btn_vendorfind").show();
//			break;
//	}
}

//item focus change event
function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){return true;}

//grid data loaded
function xe_GridDataLoad2(a_dg){
	//pf_ZipFind();

	return 100;
}

//찾기창 호출 후
function x_FindCode2(a_dg, a_row, a_findKey, a_findstr, a_win) {
	//alert(_FindCode.returnValue);
	return 100;
}


//찾기 화면에서 선택값을 넘긴 경우 호출
function x_ReceivedCode2(a_retVal){
	switch(_FindCode.finddiv){
 		case 'POST_CODE':

			var ls_zipNo 			= a_retVal.zipNo;
			var ls_Addr 			= a_retVal.roadAddrPart1 + a_retVal.roadAddrPart2;
			var ls_AddrDetail = a_retVal.addrDetail;

			dg_1.SetItem(dg_1.GetRow(), 'ZIP' 	, ls_zipNo);
			dg_1.SetItem(dg_1.GetRow(), 'ADDR' 	, ls_Addr);
			dg_1.SetItem(dg_1.GetRow(), 'ADDR2' , ls_AddrDetail);

		break;

		case 'POST_CODE2':

			var ls_zipNo 			= a_retVal.zipNo;
			var ls_Addr 			= a_retVal.roadAddrPart1 + a_retVal.roadAddrPart2;
			var ls_AddrDetail = a_retVal.addrDetail;

			dg_1.SetItem(dg_1.GetRow(), 'RECEIVE_ZIP' 	, ls_zipNo);
			dg_1.SetItem(dg_1.GetRow(), 'RECEIVE_ADDR' 	, ls_Addr);
			dg_1.SetItem(dg_1.GetRow(), 'RECEIVE_ADDR2' , ls_AddrDetail);

		break;

		case 'BANK_CODE':

      var ls_bank_code = _FindCode.returnValue.BANK_CODE;
      var ls_bank_name = _FindCode.returnValue.BANK_NAME;

			dg_1.SetItem(dg_1.GetRow(), 'BANK_CODE' 	, ls_bank_code);
			dg_1.SetItem(dg_1.GetRow(), 'BANK_NAME' 	, ls_bank_name);

		break;

	}
}

//grid item button click event
function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){return 100;}

//header click event
function xe_GridHeaderClick2(a_dg, a_col, a_colname){return 100;}

//item double click event
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){return 100;}

//tree item checked event
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){return 100;}

//tree item checked event
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){return 100;}

//- anychart  point click event
function xe_ChartPointClick2(a_dg, a_event){return 100;}

//- anychart  point select event
function xe_ChartPointSelect2(a_dg, a_event){return 100;}

//- anychart  point mouse over event
function xe_ChartPointMouseOver2(a_dg){return 100;}

//- anychart  point mouse out event
function xe_ChartPointMouseOut2(a_dg, a_event){return 100;}

//window(mdi) close
function x_Close2() {return 100;}
//
////코드 중복체크
//function pf_dupUSERID(a_value, a_row,  a_type, a_colname) {
//	if(a_type == "GRID") {
//		for(var i = 1; i < dg_1.RowCount(); i++){
//			if (dg_1.GetItem(i, a_colname) == a_value && i != a_row) {
//				return false;
//			}
//		}
//	} else {
//			switch(a_colname){
//				case 'CUST_CODE':
//					var data = _X.XmlSelect("im", "IM030001", "R_IM010200_02", new Array(''), "array");
//					if(data[0][0] == a_value) return false;
//				break;
//				case 'CUST_NAME':
//					var data = _X.XmlSelect("im", "IM030001", "R_IM030001_04", new Array(''), "array");
//					if(data[0][0] == a_value) return false;
//				break;
//				case 'BUYR_CORP_NM':
//					var data = _X.XmlSelect("im", "IM030001", "R_IM030001_05", new Array(''), "array");
//					if(data[0][0] == a_value) return false;
//				break;
//			}
//	}
//
//	return true;
//}


//우편번호찾기
function pf_ZipFind(a_param) {
	var ls_Crow = dg_1.GetRow();
	if(ls_Crow <= 0) {return 0;}
		 switch (a_param){
		 	case 'btn_addrfind':
				_X.FindStreetAddr(window,"POST_CODE","", "");
			break;
			case 'btn_addrfind2':
				_X.FindStreetAddr(window,"POST_CODE2","", "");
			break;
			case 'btn_bankcode':
			 	_X.FindBankCode(window,"BANK_CODE", "", "");
			break;
		}
}

function pf_ZipFind2() {
	var a_row = dg_1.GetRow();
	if(a_row<=0)
		return;
	_FindCode.grid_row = a_row;
	_X.findStreetAddr(window,"POST_CODE","", "");
}


function pf_create_id(){
	var ls_cData1 = dg_1.GetChangedData();
	if(ls_cData1.length > 0){
		_X.MsgBox("확인", "저장 후 진행해주시기 바랍니다.");
		return;
	}

	var ls_cust = dg_1.GetItem(dg_1.GetRow(), 'CUST_CODE');
	var ls_cu_nm = dg_1.GetItem(dg_1.GetRow(), 'CUST_NAME');

	var ls_id_chk = _X.XS("sm", "SM01022", "R_SM01022_06", new Array(ls_cust), "json2");

    if(ls_id_chk[0].cnt > 0) {
    	_X.MsgBox("확인", "이미 계정이 생성되었습니다.");
    	return;
   	}
	if(_X.MsgBoxYesNo("확인", "선택된 거래처의 계정을 생성하시겠습니까?\n( 계정ID : 사업자코드, 계정초기비밀번호 : x )") == "2") return;

	//거래처 계정생성 프로시저
	var ls_create_id = _X.ExecProc("pm", "SM_CREATE_CUST_ID", new Array(mytop._CompanyCode, ls_cust, ls_cu_nm, '9', 'x', 'Y', 'Y', mytop._UserID, mytop._ClientIP, 'Y','Y','Y','Y','Y'));

	if(ls_create_id == 1){
		_X.MsgBox("성공", "계정 생성이 완료되었습니다.");
		x_DAO_Retrieve2('All');
	}else{
		_X.MsgBox("오류", "계정 생성중 오류가 발생했습니다.");
	}

/* 프로시져 파라미터 순서
  as_company_code     VARCHAR2,    -- 회사코드
  as_user_id    VARCHAR2,    	   -- 유저계정
  as_user_name  VARCHAR2,		   -- 유저명
  as_user_tag   VARCHAR2,          -- 유저구분(직원 : 1/임원 : 2/협력업체 : 9)
  as_user_pass	VARCHAR2,		   -- 패스워드
  as_use_yn     VARCHAR2,    	   -- 사용여부 Y
  as_use_yesno VARCHAR2,           -- 사용여부(기존에 있던것 로직오류 미발생 위해 셋팅)Y
  as_user_input_id VARCHAR2,       -- 입력자
  as_user_input_ip VARCHAR2,       -- 입력자IP
  as_i VARCHAR2,     -- 권한들
  as_r VARCHAR2,     -- ????
  as_d VARCHAR2,     -- ????
  as_p VARCHAR2,     -- ????
  as_e VARCHAR2     -- ????
 */
}

function pf_atch_file() {
	if(dg_1.RowCount() <= 0) return;

	var cData = dg_1.GetChangedData();
	if (cData!=null && cData!="") {
		_X.MsgBox('확인', '자료를 저장한 후 파일을 추가 하십시요!.');
		return;
	}

	var atchFileId = dg_1.GetItem(dg_1.GetRow(), "ATTACH_FILE_ID");
	_X.FileUpload(1, 'SM', "ATTACH_FILE_ID", atchFileId);
}

function x_FileUploaded(obj, ReturnValue) {
	if ( ReturnValue == null ) {
		return;
	}

	switch(obj) {
		case "ATTACH_FILE_ID": // 가압류파일
			dg_1.SetItem(dg_1.GetRow(), "ATTACH_FILE_ID", ReturnValue[0].atchFileId);
			dg_1.SetItem(dg_1.GetRow(),"ATTACH_FILE_NAME", ReturnValue[0].orignlFileNm);
			dg_1.Save(null, null, 'N');
			break;
	}
}

function x_FileDeleted(ReturnValue) {
	if ( ReturnValue == "ATTACH_FILE_ID" ) {
		dg_1.SetItem(dg_1.GetRow(),"ATTACH_FILE_ID", "");
		dg_1.SetItem(dg_1.GetRow(),"ATTACH_FILE_NAME", "");
		dg_1.Save(null, null, 'N');
	}
}

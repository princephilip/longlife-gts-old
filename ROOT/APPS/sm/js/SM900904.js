//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 : @SM900904|그리드 함수 정보
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
// 	이상규					X-INTERNET INFO     20170203
// 
//===========================================================================================
//	
//===========================================================================================

function x_InitForm2() {
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM900904", "SM900904|SM900904_1", false, false);
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted) {
	if ( ab_allcompleted ) {
		x_DAO_Retrieve2(a_dg);
	}
}

function x_DAO_Retrieve2(a_dg){
	a_dg.SetData(G_data);
	a_dg.SetRow(1);
}

function xe_EditChanged2(a_obj, newValue, oldValue){
	
}

function xe_InputKeyEnter2(a_obj, event, ctrlKey, altKey, shiftKey){
	dg_1.SetFocusByElement(a_obj, ["F_NM", "F_REMK"]);
	setTimeout(function() { a_obj.focus(); }, 50);
}

function xe_InputKeyDown2(a_obj, event, keyCode, ctrlKey, altKey, shiftKey){

}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

function x_DAO_Saved2(){

}

function x_DAO_Insert2(a_dg, rowIdx){
	return 100;
}

function x_Insert_After2(a_dg, rowIdx){

}

function x_DAO_Duplicate2(a_dg, rowIdx){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
	
}

function x_DAO_Delete2(a_dg){
	return 100;
}

function x_DAO_Deleted2(a_dg){

}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){

}

function xe_TabChanging2(a_tab, newIdx, oldIdx, newTab, oldTab){
	return 100;
}

function xe_TabChanged2(a_tab, newIdx, oldIdx, newTab, oldTab){

}

function xe_GridDataChange2(a_dg, rowIdx, colName, newValue, oldValue){

}

function xe_GridDataChanged2(a_dg, rowIdx, colName, newValue, oldValue){

}

function xe_GridRowFocusChange2(a_dg, newRowIdx, oldRowIdx){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, newRowIdx){
	func_demo_box.src = "APPS/x-demo/jsp/" + a_dg.GetItem(newRowIdx, "F_NM") + ".jsp";
}

function xe_GridItemFocusChange2(a_dg, newRowIdx, newColName, oldRowIdx, oldColName){

}

function xe_GridItemFocusChanged2(a_dg, newRowIdx, newColName){

}

function xe_BeforeGridDataLoad2(a_dg){
	return 100;
}

function xe_GridDataLoad2(a_dg){
	return 100;
}

function xe_GridButtonClick2(a_dg, rowIdx, colName){

}

function xe_GridHeaderClick2(a_dg, colName){

}

function xe_GridItemClick2(a_dg, rowIdx, colIdx, colName){

}

function xe_GridItemDoubleClick2(a_dg, rowIdx, colIdx, colName){

}

function xe_GridBeforeKeyDown2(a_obj, event, keyCode, ctrlKey, altKey, shiftKey) {
	return 100;
}

function xe_GridKeyDown2(a_obj, event, keyCode, ctrlKey, altKey, shiftKey) {

}

function xe_FileButtonClick2(a_obj, rowIdx, colName, div){
	return 100;
}

function xe_FileChanged2(a_dg, rowIdx, colName, fileName, fileSize, fileType, firstData){
	return 100;
}

function xe_FileUploaded2(a_dg, uploadInfo){
	return 100;
}

function xe_FileDeleted2(a_dg, rowIdx, colName){
	return 100;
}

function x_Close2() {
	return 100;
}

var G_data = [
			{"id": "id_00000001", "job": "", "num": 1, "F_NM": "_X.InitGrid" , "F_REMK": "그리드 초기화한다" },
			{"id": "id_00000002", "job": "", "num": 2, "F_NM": "_X.ResetGrid", "F_REMK": "그리드 새로운 정보로 리셋한다" },
			{"id": "id_00000003", "job": "", "num": 3, "F_NM": "a_dg.UpdateColumnHeader", "F_REMK": "헤더명과 헤더 툴팁을 변경한다" },
			{"id": "id_00000004", "job": "", "num": 4, "F_NM": "a_dg.Reset", "F_REMK": "그리드를 초기상태로 돌린다(리셋)" },
			{"id": "id_00000005", "job": "", "num": 5, "F_NM": "a_dg.Save", "F_REMK": "변경된 데이터를 저장한다" },
			{"id": "id_00000006", "job": "", "num": 6, "F_NM": "a_dg.Retrieve", "F_REMK": "데이터를 조회한다" },
			{"id": "id_00000007", "job": "", "num": 7, "F_NM": "a_dg.InsertRow", "F_REMK": "행을 추가한다" },
			{"id": "id_00000008", "job": "", "num": 8, "F_NM": "a_dg.DuplicateRow", "F_REMK": "지정한 행을 복제한다" },
			{"id": "id_00000009", "job": "", "num": 9, "F_NM": "a_dg.DeleteRow", "F_REMK": "지정한 행을 삭제한다" },
			{"id": "id_00000010", "job": "", "num": 10, "F_NM": "a_dg.ExcelExport", "F_REMK": "그리드의 데이터를 엑셀로 내린다" },
			{"id": "id_00000011", "job": "", "num": 11, "F_NM": "a_dg.GetColumns", "F_REMK": "전체 컬럼의 정보를 가져온다" },
			{"id": "id_00000012", "job": "", "num": 12, "F_NM": "a_dg.SetColumns", "F_REMK": "전체 컬럼의 정보를 변경한다" },
			{"id": "id_00000013", "job": "", "num": 13, "F_NM": "a_dg.GetColumn", "F_REMK": "지정한 컬럼의 정보를 가져온다" },
			{"id": "id_00000014", "job": "", "num": 14, "F_NM": "a_dg.SetColumn", "F_REMK": "지정한 컬럼의 정보를 변경한다" },
			{"id": "id_00000015", "job": "", "num": 15, "F_NM": "a_dg.RowCount", "F_REMK": "전체 행의 수를 가져온다" },
			{"id": "id_00000016", "job": "", "num": 16, "F_NM": "a_dg.GetData", "F_REMK": "전체 데이타를 가져온다" },
			{"id": "id_00000017", "job": "", "num": 17, "F_NM": "a_dg.SetData", "F_REMK": "전체 데이타를 변경한다" },
			{"id": "id_00000018", "job": "", "num": 18, "F_NM": "a_dg.GetTotalValue", "F_REMK": "지정한 컬럼의 데이타 합계를 가져온다" },
			{"id": "id_00000019", "job": "", "num": 19, "F_NM": "a_dg.GetFilteredData", "F_REMK": "필터된 데이타를 가져온다" },
			{"id": "id_00000020", "job": "", "num": 20, "F_NM": "a_dg.ClearRows", "F_REMK": "전체 데이타를 초기화한다" },
			{"id": "id_00000021", "job": "", "num": 21, "F_NM": "a_dg.GetRowData", "F_REMK": "지정한 행의 데이터를 가져온다" },
			{"id": "id_00000022", "job": "", "num": 22, "F_NM": "a_dg.SetRowData", "F_REMK": "지정한 행의 데이터를 변경한다" },
			{"id": "id_00000023", "job": "", "num": 23, "F_NM": "a_dg.AppendRowsData", "F_REMK": "그리드에 데이터 행들을 추가한다" },
			{"id": "id_00000024", "job": "", "num": 24, "F_NM": "a_dg.GetRow", "F_REMK": "선택한 행의 번호를 가져온다" },
			{"id": "id_00000025", "job": "", "num": 25, "F_NM": "a_dg.GetCell", "F_REMK": "선택한 셀의 정보를 가져온다" },
			{"id": "id_00000026", "job": "", "num": 26, "F_NM": "a_dg.SetRow", "F_REMK": "지정한 셀로 포커스를 이동시킨다" },
			{"id": "id_00000027", "job": "", "num": 27, "F_NM": "a_dg.GetPureItem", "F_REMK": "셀의 순수한 값을 가져온다" },
			{"id": "id_00000028", "job": "", "num": 28, "F_NM": "a_dg.GetItem", "F_REMK": "셀의 값을 가져온다(타입별 데이타 변경됨)" },
			{"id": "id_00000029", "job": "", "num": 29, "F_NM": "a_dg.SetItem", "F_REMK": "셀의 값을 변경한다" },
			{"id": "id_00000030", "job": "", "num": 30, "F_NM": "a_dg.GetFieldValues", "F_REMK": "지정한 컬럼의 행 데이터를 배열로 가져온다" },
			{"id": "id_00000031", "job": "", "num": 31, "F_NM": "a_dg.SetFieldValues", "F_REMK": "지정한 컬럼의 행 데이터를 변경한다" },
			{"id": "id_00000032", "job": "", "num": 32, "F_NM": "a_dg.GetSummary", "F_REMK": "지정한 컬럼의 총합, 최대값, 최소값, 평균, 개수를 가져온다" },
			{"id": "id_00000033", "job": "", "num": 33, "F_NM": "a_dg.GetColumnNames", "F_REMK": "전체 컬럼의 이름을 가져온다(대상: grid)" },
			{"id": "id_00000034", "job": "", "num": 34, "F_NM": "a_dg.GetColumnNamesForConfig", "F_REMK": "전체 컬럼의 이름을 가져온다(대상: config)" },
			{"id": "id_00000035", "job": "", "num": 35, "F_NM": "a_dg.GetColumnTypeByName", "F_REMK": "지정한 컬럼의 타입을 가져온다" },
			{"id": "id_00000036", "job": "", "num": 36, "F_NM": "a_dg.GetSQLData", "F_REMK": "변경된 데이타의 쿼리를 가져온다" },
			{"id": "id_00000037", "job": "", "num": 37, "F_NM": "a_dg.GetAllStateRows", "F_REMK": "추가, 변경된 행들의 정보를 가져온다" },
			{"id": "id_00000038", "job": "", "num": 38, "F_NM": "a_dg.GetChangedData", "F_REMK": "모든 변경된 행들의 정보를 가져온다" },
			{"id": "id_00000039", "job": "", "num": 39, "F_NM": "a_dg.GetRowState", "F_REMK": "지정한 행의 상태값을 가져온다" },
			{"id": "id_00000040", "job": "", "num": 40, "F_NM": "a_dg.SetRowsState", "F_REMK": "지정한 행의 상태값을 변경한다" },
			{"id": "id_00000041", "job": "", "num": 41, "F_NM": "a_dg.SetRowsStateOldNew", "F_REMK": "지정한 상태의 행들을 다른 상태로 변경한다" },
			{"id": "id_00000042", "job": "", "num": 42, "F_NM": "a_dg.GetLengthByState", "F_REMK": "지정한 상태의 행의 수를 가져온다" },
			{"id": "id_00000043", "job": "", "num": 43, "F_NM": "a_dg.ResetRowsStates", "F_REMK": "그리드의 상태값을 초기화 시킨다" },
			{"id": "id_00000044", "job": "", "num": 44, "F_NM": "a_dg.IsDataChanged", "F_REMK": "데이터의 변경 여부를 가져온다" },
			{"id": "id_00000045", "job": "", "num": 45, "F_NM": "a_dg.SetCombo", "F_REMK": "컬럼의 콤보박스를 설정한다" },
			{"id": "id_00000046", "job": "", "num": 46, "F_NM": "a_dg.SetAutoComboOptions", "F_REMK": "컬럼의 콤보박스의 옵션이 자동으로 변경되도록 한다" },
			{"id": "id_00000047", "job": "", "num": 47, "F_NM": "a_dg.MustInputCheck", "F_REMK": "필수 입력항목이 모두 입력 되었는지 확인한다" },
			{"id": "id_00000048", "job": "", "num": 48, "F_NM": "a_dg.ByteInputCheck", "F_REMK": "입력항목의 입력된 데이터의 길이를 확인한다" },
			{"id": "id_00000049", "job": "", "num": 49, "F_NM": "a_dg.Sort", "F_REMK": "지정한 컬럼들의 순서대로 데이터를 정렬 시킨다" },
			{"id": "id_00000050", "job": "", "num": 50, "F_NM": "a_dg.ResetSort", "F_REMK": "정렬된 데이터를 정렬해제 시킨다" },
			{"id": "id_00000051", "job": "", "num": 51, "F_NM": "a_dg.TreeExpand", "F_REMK": "트리의 지정한 행을 확장 시킨다" },
			{"id": "id_00000052", "job": "", "num": 52, "F_NM": "a_dg.TreeCollapse", "F_REMK": "트리의 지정한 행을 축소 시킨다" },
			{"id": "id_00000053", "job": "", "num": 53, "F_NM": "a_dg.TreeExpandLevel", "F_REMK": "트리의 지정한 레벨을 확장 시킨다" },
			{"id": "id_00000054", "job": "", "num": 54, "F_NM": "a_dg.TreeExpandAll", "F_REMK": "트리의 전체 행을 확장 시킨다" },
			{"id": "id_00000055", "job": "", "num": 55, "F_NM": "a_dg.TreeCollapseAll", "F_REMK": "트리의 전체 행을 축소 시킨다" },
			{"id": "id_00000056", "job": "", "num": 56, "F_NM": "a_dg.GetParent", "F_REMK": "트리의 지정한 행의 부모 행번호를 가져온다" },
			{"id": "id_00000057", "job": "", "num": 57, "F_NM": "a_dg.GetParents", "F_REMK": "트리의 지정한 행의 모든 부모 행번호를 배열로 가져온다" },
			{"id": "id_00000058", "job": "", "num": 58, "F_NM": "a_dg.ChangeParent", "F_REMK": "트리의 지정한 행의 부모를 변경한다" },
			{"id": "id_00000059", "job": "", "num": 59, "F_NM": "a_dg.TreeLevelUp", "F_REMK": "트리의 지정한 행의 레벨을 1 올린다" },
			{"id": "id_00000060", "job": "", "num": 60, "F_NM": "a_dg.GetChildren", "F_REMK": "트리의 지정한 행의 직계 자식들의 행번호 또는 행데이터를 가져온다" },
			{"id": "id_00000061", "job": "", "num": 61, "F_NM": "a_dg.GetDescendants", "F_REMK": "트리의 지정한 행의 모든 자식들의 행번호 또는 행데이터를 가져온다" },
			{"id": "id_00000062", "job": "", "num": 62, "F_NM": "a_dg.GetChildCount", "F_REMK": "트리의 지정한 행의 자식들의 행 수를 가져온다" },
			{"id": "id_00000063", "job": "", "num": 63, "F_NM": "a_dg.AddCurrent", "F_REMK": "트리의 지정한 행의 동일레벨의 행을 추가한다" },
			{"id": "id_00000064", "job": "", "num": 64, "F_NM": "a_dg.AddChild", "F_REMK": "트리의 지정한 행의 자식레벨의 행을 추가한다" },
			{"id": "id_00000065", "job": "", "num": 65, "F_NM": "a_dg.SetGroup", "F_REMK": "그룹을 생성한다" },
			{"id": "id_00000066", "job": "", "num": 66, "F_NM": "a_dg.IsGroup", "F_REMK": "그룹 상태 여부를 가져온다" },
			{"id": "id_00000067", "job": "", "num": 67, "F_NM": "a_dg.ExpandGroup", "F_REMK": "그룹의 지정한 레벨을 확장 시킨다" },
			{"id": "id_00000068", "job": "", "num": 68, "F_NM": "a_dg.CollapseGroup", "F_REMK": "그룹의 지정한 레벨을 축소 시킨다" },
			{"id": "id_00000069", "job": "", "num": 69, "F_NM": "a_dg.SetGroupFootVisible", "F_REMK": "그룹의 지정한 컬럼의 그룹 합계 표시 여부를 설정한다" },
			{"id": "id_00000070", "job": "", "num": 70, "F_NM": "a_dg.SetCountIf", "F_REMK": "그룹의 그룹합계중 count 사용 시 조건을 부여한다" },
			{"id": "id_00000071", "job": "", "num": 71, "F_NM": "a_dg.SetGroupCustom", "F_REMK": "그룹의 지정한 컬럼의 그룹합계를 임의로 계산하여 표시한다" },
			{"id": "id_00000072", "job": "", "num": 72, "F_NM": "a_dg.SetRowStyle", "F_REMK": "지정한 행의 스타일을 변경한다" },
			{"id": "id_00000073", "job": "", "num": 73, "F_NM": "a_dg.SetColumnStyle", "F_REMK": "지정한 컬럼의 스타일을 변경한다" },
			{"id": "id_00000074", "job": "", "num": 74, "F_NM": "a_dg.SetCellStyle", "F_REMK": "지정한 셀의 스타일을 변경한다" },
			{"id": "id_00000075", "job": "", "num": 75, "F_NM": "a_dg.SetColumnVisible", "F_REMK": "지정한 컬럼의 헤더 표시 여부를 설정한다" },
			{"id": "id_00000076", "job": "", "num": 76, "F_NM": "a_dg.SetMaskValue", "F_REMK": "지정한 컬럼의 값을 임의로 표시한다" },
			{"id": "id_00000077", "job": "", "num": 77, "F_NM": "a_dg.SetAutoRowStyle", "F_REMK": "행의 스타일을 조건을 주어 자동으로 변경한다" },
			{"id": "id_00000078", "job": "", "num": 78, "F_NM": "a_dg.SetAutoCellStyle", "F_REMK": "셀의 스타일을 조건을 주어 자동으로 변경한다" },
			{"id": "id_00000079", "job": "", "num": 79, "F_NM": "a_dg.SetAutoCellStyle2", "F_REMK": "셀의 스타일을 조건을 주어 자동으로 변경한다" },
			{"id": "id_00000080", "job": "", "num": 80, "F_NM": "a_dg.SetAutoValue", "F_REMK": "지정한 컬럼의 값을 조건을 주어 자동으로 변경한다" },
			{"id": "id_00000081", "job": "", "num": 81, "F_NM": "a_dg.SetAutoFooterValue", "F_REMK": "지정한 컬럼의 합계값을 조건을 주어 자동으로 변경한다" },
			{"id": "id_00000082", "job": "", "num": 82, "F_NM": "a_dg.SetAutoReadOnly", "F_REMK": "지정한 컬럼의 readonly 상태를 조건을 주어 자동으로 변경한다" },
			{"id": "id_00000083", "job": "", "num": 83, "F_NM": "a_dg.SetCellMerge", "F_REMK": "지정한 컬럼의 데이터를 merge 시킨다" },
			{"id": "id_00000084", "job": "", "num": 84, "F_NM": "a_dg.GridReadOnly", "F_REMK": "그리드를 전체 readonly 시킨다" },
			{"id": "id_00000085", "job": "", "num": 85, "F_NM": "a_dg.SetCheckBar", "F_REMK": "체크바의 표시여부를 설정한다" },
			{"id": "id_00000086", "job": "", "num": 86, "F_NM": "a_dg.GetCheckedRows", "F_REMK": "체크바의 체크된 행의 번호를 배열로 가져온다" },
			{"id": "id_00000087", "job": "", "num": 87, "F_NM": "a_dg.SetFocusByElement", "F_REMK": "지정한 요소의 값과 동일한 값을 가진 셀로 포커스를 이동시킨 후 행번호를 가져온다" },
			{"id": "id_00000088", "job": "", "num": 88, "F_NM": "a_dg.SetFocusByValue", "F_REMK": "지정한 값과 같은 값을 가진 셀로 포커스를 이동시킨 후 행번호를 가져온다" },
			{"id": "id_00000089", "job": "", "num": 89, "F_NM": "a_dg.GetDuplicateRowByValue", "F_REMK": "지정한 값들과 같은 값이 있는 행번호를 가져온다" },
			{"id": "id_00000090", "job": "", "num": 90, "F_NM": "a_dg.GetDuplicateRowByKey", "F_REMK": "지정한 값들과 같은 값이 있는 행번호를 가져온다" },
			{"id": "id_00000091", "job": "", "num": 91, "F_NM": "a_dg.GetSqlKey", "F_REMK": "현재 사용하는 쿼리아이디를 가져온다" },
			{"id": "id_00000092", "job": "", "num": 92, "F_NM": "a_dg.SetSqlKey", "F_REMK": "지정한 쿼리아이디로 사용하는 SQL 을 변경한다" },
			{"id": "id_00000093", "job": "", "num": 93, "F_NM": "a_dg.SetChildGrids", "F_REMK": "하위그리드를 설정한다" },
			{"id": "id_00000094", "job": "", "num": 94, "F_NM": "a_dg.ExcelUpload", "F_REMK": "엑셀 업로드를 한다" },
			{"id": "id_00000095", "job": "", "num": 95, "F_NM": "a_dg.FileUpload", "F_REMK": "파일 업로드를 한다" },
			{"id": "id_00000096", "job": "", "num": 96, "F_NM": "a_dg.FileDownload", "F_REMK": "파일 다운로드를 한다" },
			{"id": "id_00000097", "job": "", "num": 97, "F_NM": "a_dg.FileDelete", "F_REMK": "파일을 삭제한다" },
			{"id": "id_00000098", "job": "", "num": 98, "F_NM": "_X.IsDataChangedAll", "F_REMK": "전체 그리드의 변경 여부를 가져온다" }
	];
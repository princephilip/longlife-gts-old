/**
 *<pre>
 * Statements
 *</pre>
 
 * @Class Name : SM900050.js
 * @Descriptio   Grid.js -> DB Import
 * @Modification Information
 * @
 * @  수정일     수정자      수정내용
 * @ -------    --------    ---------------------------
 * @ 2016.10.28     JH        최초 생성
  */

var li_i = 0;
var li_cnt = 0;
var li_imp_cnt = 0;
var li_ok_cnt = 0;
var li_err_cnt = 0;
var vUpdCols = {};    // 업데이트 컬럼 정보 
//var vSqlObjs = {};  // xmlx 파일 정보용 임시 OBJ
//var vUpdTbls = {};  // UPDATE TABLE 정보용 임시 OBJ

function x_InitForm2(){
  _X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM900050", "SM900050|PGM_CODE_LIST", false, false);
  _X.InitGrid(grid_2, "dg_2", "100%", "100%", "sm", "SM900050", "SM900050|DEV_GRID_COLS", false, true);
  _X.InitGrid(grid_3, "dg_3", "100%", "100%", "sm", "SM900050", "SM900050|DEV_GRID", false, true);

  return 100;
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
  if(ab_allcompleted) {
    var comboData  = _X.XmlSelect("sm", "SM_AUTH_SYS", "R_SM0903_01", new Array(mytop._CompanyCode), "json2");
    var comboData2 = _X.XmlSelect("com", "COMMON", "SM_COMCODE_D", new Array("SM", "PGM_URL"), "json2");
    //[{"code":"/XG010.do"   , "label":"/XG010.do"}, {"code":"/XG_GRID1.do", "label":"/XG_GRID1.do"}, {"code":"/XG_SHARE.do", "label":"/XG_SHARE.do"}, {"code":"/XG_GRID2.do", "label":"/XG_GRID2.do"}, {"code":"/XG_FORM.do" , "label":"/XG_FORM.do"}];
    //Combo 데이타 설정
    dg_1.SetCombo("SYS_ID", comboData);       //시스템구분
    dg_1.SetCombo("PGM_URL", comboData2);     //template 유형
    setTimeout('x_DAO_Retrieve(dg_1)',0);
  }
  return 100;
}

function x_DAO_Retrieve2(a_dg){
  li_i = 0;
  li_cnt = 0;
  li_imp_cnt = 0;
  li_ok_cnt = 0;
  li_err_cnt = 0;

  dg_2.Reset();
  dg_3.Reset();
  dg_1.Retrieve(new Array(S_SYS_ID.value,'%' + S_PGM_CODE.value + '%'));

  return 0;
}

//html input type 의 자려가 변경된 경우 발생
function xe_EditChanged2(a_obj, a_val){
  if(a_obj==S_SYS_ID) {
    x_DAO_Retrieve();
  }
}

//enter key event 발생시 호출
function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
  switch(a_obj.id) {
    case "S_PGM_CODE":
      //검색할 필드명을 배열에 넣는다
      dg_1.SetFocusByElement(a_obj, new Array("PGM_CODE", "PGM_NAME"));
      break;
  }
  return 0;
}

//keydown event 발생시 호출
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){return 100;}

//데이타 저장
function x_DAO_Save2(a_dg){return 100;}

//데이타 저장후 callback
function x_DAO_Saved2(){
  
  // for(var i=0; i<ls_up_Array.length; i++){
  //  _X.ExecProc("sm", "PROC_SM_AUTH_GROUP_MENU_PGMNAME", new Array(ls_up_Array[i][0], ls_up_Array[i][1], ls_up_Array[i][2], ls_up_Array[i][3], ls_up_Array[i][4])); 
  // }  
  
  ls_up_Array = new Array();
  return 100;
}

//데이타 유효성 check
function x_DAO_ChkErr2(){
  return true;
}

//grid insert/add
function x_DAO_Insert2(a_dg, row){return 100;}

//grid insert 수행후 발생
function x_Insert_After2(a_dg, rowIdx){
  return 0;
}

//grid 복제
function x_DAO_Duplicate2(a_dg, row){return 100;}

//grid 복제후
function x_Duplicate_After2(a_dg, rowIdx){
  return 0;
}

//grid 삭제
function x_DAO_Delete2(a_dg, row){return 100;}

//excel 저장
function x_DAO_Excel2(a_dg){return 100;}

//출력
function x_DAO_Print2(){
  return 100;
}

//tab 변경시
function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}

//tab 변경시
function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}

//grid data 변경시
function xe_GridDataChange2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
  return 0;
}

//grid row focus change
function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){return true;}

//item focus change event
function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){return true;}

//grid data loaded
function xe_GridDataLoad2(a_dg){
  if(a_dg==dg_1) {
    li_cnt = dg_1.RowCount();
  }
  return 100;
}

//찾기창 호출 후
function x_FindCode2(a_dg, a_row, a_findKey, a_findstr, a_win) {return 100;}

//찾기창 호출 후
function x_ReceivedCode2(){
}

//grid item button click event
function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){return 100;}

//header click event
function xe_GridHeaderClick2(a_dg, a_col, a_colname){return 100;}

//item double click event
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){return 100;}

//tree item checked event
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){return 100;}

//tree item checked event
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){return 100;}

//- anychart  point click event
function xe_ChartPointClick2(a_dg, a_event){return 100;}

//- anychart  point select event
function xe_ChartPointSelect2(a_dg, a_event){return 100;}

//- anychart  point mouse over event
function xe_ChartPointMouseOver2(a_dg){return 100;}

//- anychart  point mouse out event
function xe_ChartPointMouseOut2(a_dg, a_event){return 100;}

//window(mdi) close
function x_Close2() {return 100;}

//전체선택/해제처리
function pf_SelectAll(a_column, a_flag) {
  for(var i = 1; i <= dg_1.RowCount(); i++){
    dg_1.SetItem(i, a_column, (a_flag?'Y':'N'));
  }
}

//DB없는것만 선택
function pf_SelectNew(a_column, a_flag) {
  for(var i = 1; i <= dg_1.RowCount(); i++){
    if(dg_1.GetItem(i, "EXISTS_DB")=="-") {
      dg_1.SetItem(i, "SELECTED", "Y");
    } else {
      dg_1.SetItem(i, "SELECTED", "N");
    }
  }
}


//프로그램 복사
function pf_SourceCopy(pgm_code, pgm_name) {
  var a_row = dg_1.GetRow();
  if(a_row<=0) return;
  var t_pgm_code = dg_1.GetItem(a_row,'PGM_CODE');
  var t_pgm_name = dg_1.GetItem(a_row,'PGM_NAME');

  if(_X.MsgBoxYesNo("원본 : [" + pgm_code + "] " + pgm_name + "\r\n대상 : [" + t_pgm_code + "] " + t_pgm_name + "\r\n소스 복사작업을 진행 하시겠습니까?","",2)!=1) {return;}
  if(_X.CallJsp("/Mighty/jsp/SourceCopy.jsp?fpgm=" + pgm_code + "&tpgm=" + t_pgm_code + "")) {
    _X.MsgBox("소스복사작업이 완료되었습니다.");
  }
}

// // Grid.js 파일 루프 돌면서 처리 
// var importGridJs = function() {
//   getStringFromJs();

//   for(var i=1;i<=dg_1.RowCount();i++) {
//     if(dg_1.GetItem(i, 'SELECTED') == 'Y') {
//       getStringFromJs(i, dg_1.GetItem(i, 'SYS_ID'), dg_1.GetItem(i, 'PGM_CODE'));
//     }
//   }
// }

// 자바스크립트 소스 String으로 가져오기 
var getStringFromJs = function() { 
  var txmlObj
  li_i++;
  
  if(li_i>li_cnt) {
    // 일괄작업 종료시 vSqlObjs 정보 dg_3에 넣기 
    alert("IMPORT 시도 : "+li_imp_cnt+"건\r\nIMPORT 성공 : "+li_ok_cnt+"건\r\nERROR : "+li_err_cnt+"건\r\n전체 : " + li_cnt+"건\r\n의 작업이 완료되었습니다.");
    li_i = 0;

    //pf_setSqlId();

    //return;
  } else {
    try {
      // 아직 전체 갯수보다 작을때, 
      if(dg_1.GetItem(li_i, 'SELECTED')=='Y') {
        li_imp_cnt++;
        var tSysid = dg_1.GetItem(li_i, 'SYS_ID');
        var tPgmcode = dg_1.GetItem(li_i, 'PGM_CODE');

        // js 소스코드 분석해서 SQL FILE & SQL ID 읽어오기 
        // .xmlx 파일 읽어오기 
        getXmlxinfo(tPgmcode, li_i);

        // console.log("vUpdCols", vUpdCols["COMPANY_CODE"]);

        // Grid.js 파일 읽어오기 
        $.ajax({
          type: "POST",
          dataType: 'text',
          url: "/APPS/"+tSysid.toLowerCase()+"/grid/"+tPgmcode+".Grid.js",
          async: false,
          success: function( data, textStatus, jQxhr ){ 
            li_ok_cnt++;

            // 읽어온 내용으로 저장용 Insert 처리
            setTimeout(function(){setDataGrid(data, li_i, tSysid, tPgmcode);},10);
            //setDataGrid(data, li_i, tSysid, tPgmcode);

            // 다음 프로그램코드 처리 
            //Timeout 처리(2016.11.24 KYY)
            setTimeout(function(){getStringFromJs();},1000);
          },
          error:function(e){  
            dg_1.SetItem(li_i, 'IMPORT_MESSAGE', e.statusText);
            dg_1.SetItem(li_i, 'IMP_GRID', 'ERR');

            // 다음 프로그램코드 처리 
            setTimeout(function(){getStringFromJs();},1000);
          }
        });
      } else {
        // 다음 프로그램코드 처리 
        //Timeout 처리(2016.11.24 KYY)
        setTimeout(function(){getStringFromJs();},1000);
      }   

    } catch (e) {
      //alert(e);
      setTimeout(function(){getStringFromJs();},1000);
    }
  }
}

// 저장하기 전, 칼럼 속성을 저장 Grid에 넣기
var setDataGrid = function(aJs, aRow, aSys, aPgm) {
  var patt = /(?:columns_)(\w+)/gi;
  var cObjs = aJs.match(patt);

  var preSys = ""
    , prePgm = ""
    , preGrid = "";
	
  try {
    eval(aJs);

    // Grid.js 파일내 Grid LOOP
    $.each(cObjs, function(idx, val) {
      eval("var objTar = "+val+"; ");

      // Grid 속성 내 칼럼 LOOP
      $.each(objTar, function(i, e) {
        tRow = dg_2.InsertRow();
        var cIdx = i + 1;

        dg_2.SetItem(tRow, "SYS_ID",           aSys.toUpperCase());
        dg_2.SetItem(tRow, "PGM_CODE",         aPgm);
        dg_2.SetItem(tRow, "GRID_ID",          val.replace(/columns_/, ''));
        dg_2.SetItem(tRow, "FIELDNAME",        e.fieldName);
        dg_2.SetItem(tRow, "SEQ",              cIdx);
        dg_2.SetItem(tRow, "FIELD_SEQ",        cIdx * 10);
        dg_2.SetItem(tRow, "WIDTH",            e.width);
        dg_2.SetItem(tRow, "MUST_INPUT",       e.must_input);
        dg_2.SetItem(tRow, "VISIBLE",          pf_convBoolYn(e.visible));
        dg_2.SetItem(tRow, "READONLY",         pf_convBoolYn(e.readonly));
        dg_2.SetItem(tRow, "EDITABLE",         pf_convBoolYn(e.editable));
        dg_2.SetItem(tRow, "SORTABLE",         pf_convBoolYn(e.sortable));
        dg_2.SetItem(tRow, "HEADER",           "{text: '"+e.header.text+"'}");
        dg_2.SetItem(tRow, "HEADER_TEXT",      e.header.text);
        dg_2.SetItem(tRow, "STYLES",           getStylesName(e.styles));
        dg_2.SetItem(tRow, "EDITOR",           getEditorName(e.editor));
        dg_2.SetItem(tRow, "LOOKUPDISPLAY",    pf_convBoolYn(e.lookupDisplay));
        dg_2.SetItem(tRow, "RENDERER",         e.renderer);
        dg_2.SetItem(tRow, "BUTTON",           e.button);
        dg_2.SetItem(tRow, "ALWAYSSHOWBUTTON", pf_convBoolYn(e.alwaysShowButton));

        dg_2.SetItem(tRow, "HEADERTOOLTIP",    e.headerToolTip);
        dg_2.SetItem(tRow, "MERGE",            pf_convBoolYn(e.merge));
        dg_2.SetItem(tRow, "MAXLENGTH",        e.maxLength);
        dg_2.SetItem(tRow, "FOOTEREXPR",       e.footerExpr);
        dg_2.SetItem(tRow, "FOOTERALIGN",      e.footerAlign);
        dg_2.SetItem(tRow, "FOOTERSTYLE",      e.footerStyle);
        dg_2.SetItem(tRow, "FOOTERPRE",        e.footerPre);
        dg_2.SetItem(tRow, "FOOTERPOST",       e.footerPost);
        dg_2.SetItem(tRow, "RESIZABLE",        pf_convBoolNy(e.resizable));

        if(vUpdCols!=null && typeof(vUpdCols)!="undefined" && typeof(vUpdCols[e.fieldName])!="undefined") {	//수정(2016.11.23 KYY)
          dg_2.SetItem(tRow, "UPDATE_TYPE",      vUpdCols[e.fieldName].type);
          dg_2.SetItem(tRow, "UPDATABLE",        vUpdCols[e.fieldName].updatable.substr(0,1).toUpperCase());	//수정(2016.11.23 KYY)
          dg_2.SetItem(tRow, "UPDATE_ISKEY",     vUpdCols[e.fieldName].iskeycol.substr(0,1).toUpperCase());		//수정(2016.11.23 KYY)
        }
        // console.log(eval(e.styles)); //, tE.constructor.name); 
      });
    });

    //if(typeof(cObjs.length)!="undefined")
    dg_1.SetItem(aRow, 'IMPORT_MESSAGE', cObjs.length+'개 IMPORT 완료');
    dg_1.SetItem(aRow, 'IMP_GRID', 'OK');

  } catch (e) {
  	//alert(e);
    if (e instanceof SyntaxError) {
      li_err_cnt++;
      dg_1.SetItem(aRow, 'IMPORT_MESSAGE', 'SYNTAX ERROR');
      dg_1.SetItem(aRow, 'IMP_GRID', 'ERR');
    }
  } 

  return true;
}

/*
// vSqlObjs 에서 Sqlid 찾아서 넣어주기 
function pf_setSqlId() { 
  var ttObj, ttObj2;
  for(var i=1;i<=dg_3.RowCount();i++) {
    ttObj = vSqlObjs[dg_3.GetItem(i, "SYS_ID")+"_"+dg_3.GetItem(i, "PGM_CODE")+"_"+dg_3.GetItem(i, "GRID_ID")];
    if(ttObj) {
      var tsqlObj = ttObj.split(",");
      dg_3.SetItem(i, "SQL_FILE", tsqlObj[0]);
      dg_3.SetItem(i, "SQL_ID", tsqlObj[1]);
    }
    if(typeof(vUpdTbls[dg_3.GetItem(i, "SYS_ID")+"_"+dg_3.GetItem(i, "PGM_CODE")+"_"+dg_3.GetItem(i, "SQL_FILE")+"_"+dg_3.GetItem(i, "SQL_ID")])!="undefined") {
      //console.log(ttObj2);
      dg_3.SetItem(i, "UPDATE_TABLE", vUpdTbls[dg_3.GetItem(i, "SYS_ID")+"_"+dg_3.GetItem(i, "PGM_CODE")+"_"+dg_3.GetItem(i, "SQL_FILE")+"_"+dg_3.GetItem(i, "SQL_ID")]);
    } 
  }
}
*/

// xmlx파일 읽기 
var getXmlQuery = function(aSys, aPgm, aSql, aSqlid, aRow1, aRow3) { // aRow1: dg_1 row, aRow3 : dg_3 row
  var tSys = aSys;
  var tPgm = aPgm;
  var tSqlfile = aSql;
  var tSqlid = aSqlid;

  var url_xml = '/APPS/sm/jsp/SM900050_xml.jsp';

  vUpdCols = {};

  if(tSys!=""&&tSqlfile!=""&&tSqlid!="") {
    // pgmcode.xmlx 파일 분석
    $.ajax({
      type: "POST",
      url: url_xml,
      dataType: "text",
      data: { sys_id: tSys, fname: tSqlfile, qid: tSqlid },
      async: false,
      success: function(xml) {
        xml = xml.replace('<?xml version="1.0" encoding="UTF-8"?>', '');
        xml = xml.replace('<!DOCTYPE querylist SYSTEM "../../../Mighty/Xml/query.dtd">', '');
				
				//예외처리 xml파일이 잘못된 경우 오류남(2016.11.23 KYY)
				try {
	        xmlDoc = $.parseXML( xml );
	        tXml = $( xmlDoc );
	        //tSql = $(tXml).find( "select#"+tSqlid+" sql" ).eq(0).text();
	        //isSqlChanged = false;
	        //Sql 추출
	        //ed_gridsql.setValue(tSql);
	        //setButtonActive("disabled");

	        //UPDATE TABLE & COLUMNS 정보
	        updTable = $(tXml).find( "select#"+tSqlid+" update_table" ).text();
	        tCols = $(tXml).find( "select#"+tSqlid+" column" );
	        tParams = $(tXml).find( "select#"+tSqlid+" param" );
	        updCols = new Array();
	        updPars = new Array();
	        var tIdx = 0;

	        dg_3.SetItem(aRow3, "SQL_FILE"     , tSqlfile);
	        dg_3.SetItem(aRow3, "SQL_ID"       , tSqlid);
	        dg_3.SetItem(aRow3, "UPDATE_TABLE" , updTable);
	        //vUpdTbls[aSys+"_"+aPgm+"_"+aSql+"_"+aSqlid] = updTable;
	        //console.log(updTable)
				
	        $.each(tCols, function(idx, val) {
	          tIdx = idx+1;
	          // if($(val).attr("name")==$("#fieldName_"+tIdx).val()) {
	          //   $("#update_type_"+tIdx).val( $(val).attr("type") );
	          //   $("#updatable_"+tIdx).val( $(val).attr("updatable") );
	          //   $("#update_iskey_"+tIdx).val( $(val).attr("iskeycol") );
	          // }
	          // console.log( $(val).attr("name") )
            //alert($(val).attr("name"));
	          vUpdCols[$(val).attr("name")] = { name: $(val).attr("name"), type: $(val).attr("type"), updatable: $(val).attr("updatable"), iskeycol: $(val).attr("iskeycol") };
            //alert(vUpdCols[$(val).attr("name")]);
	        });
				} catch (e) {
					//alert(e);
				}

        dg_1.SetItem(aRow1, 'IMP_XMLX', 'OK');
        
        /*
        // update column 정보 세팅 
        setGridUpdateInfo();

        $.each(tParams, function(idx, val) {
          updPars[idx] = { name: $(val).attr("name"), type: $(val).attr("type") };
        });

        setColorColsprop();
        getUpdateTables(tSql, updTable);
        setParams(updPars);
        //console.log(updCols);
        */
      },
      error:function(e){  
        dg_1.SetItem(aRow1, 'IMP_XMLX', 'ERR');
        //dg_1.SetItem(aRow1, 'IMPORT_MESSAGE2', dg_1.GetItem(aRow1, 'IMPORT_MESSAGE2') + ","+tSqlfile+"/"+tSqlid+" error");
      }
    });
  }

}


// xmlx 파일 정보 읽어오기 
var getXmlxinfo = function(aPgm, aRow) {
  var url_js = "/APPS/"+aPgm.substring(0, 2).toLowerCase()+"/js/"+aPgm+".js";

  // pgmcode.js 분석
  $.ajax({
    type: "POST",
    url: url_js,
    dataType: "text",
    async: false,
    success: function(data) {
      $("#js-txt").val(data);

      var patt = /InitGrid\((.*)\)/gi;
      var cObjs = data.match(patt);
      urlXml = new Object;
      var tGridId = "";
      var tQryStr = "";
      var tQryFile = "";
      var tQryId = "";
      //_X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM900050", "SM900050|PGM_CODE_LIST", false, false);
      //_X.InitGrid( {grid: dg_1, sqlId: "CM0525_01", share: false, updatable: false} );
      $.each(cObjs, function(idx, val) {
        val = val.replace(/InitGrid/, "");
        val = val.replace("(", "");
        val = val.replace(")", "");

        if(val.indexOf("{")>0) {
          // InitGrid Object 파라미터 
          eval("var tGridParam = "+val+";");
          tGridId = tGridParam.grid.id;
          tQryFile = (!tGridParam.sqlFile ? aPgm : tGridParam.sqlFile);
          tQryId = tGridParam.sqlId;
        } else {
          // InitGrid 옛날방식
          val = val.split(",");
          tGridId = val[1].replace(/\s/, ""); tGridId = tGridId.replace(/("|')/g, '');
          tQryStr = val[6].replace(/\s/, ""); tQryStr = tQryStr.replace(/("|')/g, '');
          tQryFile = tQryStr.split("|")[0];
          tQryId = tQryStr.split("|")[1];  
        }

        // dg_3 에 정보 넣기 
        // SQL FILE & ID 넣기 
        var tRow2 = dg_3.InsertRow();

        dg_3.SetItem(tRow2, "SYS_ID",        aPgm.substring(0, 2).toUpperCase());
        dg_3.SetItem(tRow2, "PGM_CODE",      aPgm);
        dg_3.SetItem(tRow2, "GRID_ID",       tGridId);

        // eval( 'vSqlObjs.'+aPgm.substring(0, 2).toUpperCase()+"_"+aPgm+"_"+tGridId+' = "' + tQryFile+','+tQryId+'";' );
        getXmlQuery(aPgm.substring(0, 2).toUpperCase(), aPgm, tQryFile, tQryId, aRow, tRow2);
      });

      // Grid.js 파일 분석
      // getGridJs(url_grid);

      // /(?:from|into|update|join)(?:\s+|\n+)(\w+)(?:\s+|\n+|$)/gi;
      dg_1.SetItem(aRow, 'IMP_JS', 'OK');
    },
    error: function() {
      // 파일 없을 때, 쿼리 창 열기
      // alert(url_js+' 파일을 찾을 수 없습니다.')
      dg_1.SetItem(aRow, 'IMP_JS', 'ERR');
    }
  });
}

var pf_convBoolYn = function(aVal) {
  return aVal==true ? "Y" : "N"
}
var pf_convBoolNy = function(aVal) {
  return aVal==null || aVal==true ? "Y" : "N"
}
var getStylesName = function(aObj) {
  switch(aObj) {
    case _Styles_text :
      return "_Styles_text";
      break;
    case _Styles_textc :
      return "_Styles_textc";
      break;
    case _Styles_textr :
      return "_Styles_textr";
      break;
    case _Styles_number :
      return "_Styles_number";
      break;
    case _Styles_numberc :
      return "_Styles_numberc";
      break;
    case _Styles_numberl :
      return "_Styles_numberl";
      break;
    case _Styles_numberf1 :
      return "_Styles_numberf1"; 
      break;
    case _Styles_numberf2 :
      return "_Styles_numberf2";
      break;
    case _Styles_numberf3 :
      return "_Styles_numberf3";
      break;
    case _Styles_numberf4 :
      return "_Styles_numberf4";
      break;
    case _Styles_date :
      return "_Styles_date";
      break;
    case _Styles_datetime :
      return "_Styles_datetime";
      break;
    case _Styles_checkbox : 
      return "_Styles_checkbox";
      break;
    case _Styles_tree :
      return "_Styles_tree";
      break;
    default :
      return "";
  }
  
  //   proptype: "object",
  //   width: "",
  //   className: ""
  // },
  // {
  //   title: "editor",
  //   value: "",
  //   proptype: "object",
  //   width: "",
  //   className: ""
  // },

}

var getEditorName = function(aObj) {
  switch(aObj) {
    case _Editor_text :
      return "_Editor_text";
      break;
    case _Editor_multiline :
      return "_Editor_multiline";
      break;
    case _Editor_number :
      return "_Editor_number";
      break;
    case _Editor_numberf1 :
      return "_Editor_numberf1";
      break;
    case _Editor_numberf2 :
      return "_Editor_numberf2";
      break;
    case _Editor_numberf3 :
      return "_Editor_numberf3";
      break;
    case _Editor_numberf4 :
      return "_Editor_numberf4";
      break;
    case _Editor_date :
      return "_Editor_date";
      break;
    case _Editor_datetime :
      return "_Editor_datetime";
      break;
    case _Editor_checkbox :
      return "_Editor_checkbox";
      break;
    case _Editor_dropdown :
      return "_Editor_dropdown";
      break;
    default :
      return "";
  }
}
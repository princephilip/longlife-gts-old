
function x_InitForm2(){
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM900071", "SM900071|R_SM900071_C1",true,true); 

  $("#S_SYS_ID").on("change", function(e, v) {
    x_DAO_Retrieve();
  });
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted){ 
		if(dg_1.RowCount() == 0){
			// _X.FormSetAllDisable("freeform1", true);
		}
		x_DAO_Retrieve();
	}
}

function x_DAO_Retrieve2(a_dg){
	var param = new Array($("#S_SYS_ID").val(), $("#S_FIND").val());
	
  dg_1.Retrieve(param);
}

function xe_EditChanged2(a_obj, a_val){
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
		x_DAO_Retrieve();
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	
	var cData		 	= dg_1.GetChangedData();
	//var	data_cnt  = _X.XmlSelect("SM", "SM900071", "R_SM900071_R1", new Array('') , "json2");	// µ¥ÀÌÅÍÀ¯¹«Ã¼Å©
	var max_code_q = _X.XmlSelect("SM", "SM900071", "R_SM900071_R2", new Array('') , "json2");	// ¸Æ½ºÄÚµå°ª
	
	for ( var i = 0; i < cData.length; i++ ) {
		if(cData[i].job == "D") continue;
		if (cData[i].job=="I") {
			dg_1.SetItem(cData[i].idx,'CODE'		, _X.LPad(++max_code_q[0].max_code , 5, "0" ) );
		}
	}
	
	return true;
}

function x_DAO_Saved2(){
  makePreviewFile();

	return 100;
}

function x_DAO_Insert2(a_dg, row){
	return 100;
}

function x_Insert_After2(a_dg, rowIdx){ 
	dg_1.SetItem(rowIdx, 'USE_YN'	, 'Y');
  dg_1.SetItem(rowIdx, 'SYS_ID' , (S_SYS_ID.value&&S_SYS_ID.value!="%" ? S_SYS_ID.value : "SM"));
	//_X.FormSetAllDisable("freeform1", false);
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){ 
 
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){  
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){  
	if(dg_1.RowCount() == 0){
		//_X.FormSetAllDisable("freeform1", true);
	}

  setPreview( a_dg.GetItem( a_newrow, "CODE" ) );
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridDataLoad2(a_dg){  
}

function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){ 
}

function x_ReceivedCode2(a_retVal){ 
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){
}

function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){
}

function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){
}

function xe_ChartPointClick2(a_dg, a_event){
}

function xe_ChartPointSelect2(a_dg, a_event){
}

function xe_ChartPointMouseOver2(a_dg){
}

function xe_ChartPointMouseOut2(a_dg, a_event){
}

function x_Close2() {
	return 100;
}
 
// Preview JSP 생성
var makePreviewFile = function() {
  //alert( dg_1.GetItem( dg_1.GetRow(), "CODE" ) );

  //return;

  $.ajax({
    method: "POST",
    url: "/APPS/sm/jsp/SM900071_make.jsp",
    dataType: "json",
    data: { code: dg_1.GetItem( dg_1.GetRow(), "CODE" ) },
    success: function( data, textStatus, jQxhr ){ 
    },
    error:function(e){  
      alert(e.responseText.replace(/\s/, ""));  
    }
  });
}

var setPreview = function(aCode) {
  $.ajax({
    method: "GET",
    url: "/APPS/sm/jsp_dev/"+aCode+".jsp",
    //success: function( data, textStatus, jQxhr ){ 
    //  alert(data)
    //},
    error:function(e){  
      $("#pre_view").html("none"); 
    }
  }).done(function( html ) {
    $("#pre_view").html(html);   
  });
}
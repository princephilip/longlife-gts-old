<!-- github - xinternet/mx5/dev_apps 에 작성 -->

var conditions;

function x_InitForm2(){
  _X.UnBlock();
  
  var arrTheme = [{code:'x5', label:'x5 기본형'}];
  _X.DDLB_SetData(S_THEME_ID , arrTheme, 'x5', false, true);

  // 전체 태그 가져오기
  getLayoutTags();
  // // tab sql - event 
  // $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  //   if(typeof(ed_before)=="undefined") setEditor( "before" );
  // });
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
  if(ab_allcompleted){ 
  }
}

function x_DAO_Retrieve2(a_dg){
  return 0;
}

function xe_EditChanged2(a_obj, a_val){
  if(a_obj==S_SYS_ID) {
    drawPreview();
    //console.log(max_code_q)
  }
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
  return 100;
}

function x_DAO_ChkErr2(){
  return true;
}

function x_DAO_Saved2(){
  makePreviewFile();

  return 100;
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}

function x_Insert_After2(a_dg, rowIdx){ 
}

function x_DAO_Duplicate2(a_dg, row){
  return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg){
  return 100;
}

function x_DAO_Excel2(a_dg){
  return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
  
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){ 
 
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){  
  return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){  
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridDataLoad2(a_dg){  
}

function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){ 
}

function x_ReceivedCode2(a_retVal){ 
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){
}

function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){
}

function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){
}

function xe_ChartPointClick2(a_dg, a_event){
}

function xe_ChartPointSelect2(a_dg, a_event){
}

function xe_ChartPointMouseOver2(a_dg){
}

function xe_ChartPointMouseOut2(a_dg, a_event){
}

function x_Close2() {
  return 100;
}

 
var drawPreview = function() {
  var param = new Array( $("#S_SYS_ID").val() );
  conditions = _X.XmlSelect("SM", "SM900070", "GET_RETR_LIST", new Array(param) , "array");
  var tSys = "";

  $(conditions).each(function(i, e) {
    if(tSys!=e[0]) {
      $("#pre_view").append("<h4>[ "+e[0]+" ]</h4><hr />");
      tSys = e[0];  
    }
    
    var tName = e[2];
    var tCode = e[1];

    $.ajax({
      method: "GET",
      url: "/APPS/sm/jsp_dev/"+e[1]+".jsp",
      //success: function( data, textStatus, jQxhr ){ 
      //  alert(data)
      //},
      error:function(e){  
        $("#pre_view").html("none"); 
      }
    }).done(function( html ) {
      var tHtml = '<div class="col-md-4"><div class="panel panel-default"><div class="panel-heading">'+tName+'<button class="pull-right btn btn-info btn-xs" data-toggle="modal" data-target="#srcModal" onclick="setCode('+i+')">소스</button></div><div class="panel-body">'+html+'</div></div></div>';
      $("#pre_view").append(tHtml);   
    });
  });

}


var setCode = function(aIdx) {
  $("#jsp-src").val(conditions[aIdx][3]);
}

// 전체 태그 가져오기
var getLayoutTags = function() {
  $.ajax({
    dataType: 'jsonp',
    url: "http://x119.kr/layouts/all_tags.json",
    jsonpCallback: "callback",
    success: function( data, textStatus, jQxhr ){ 
      setTagsButton(data);
    },
    error:function(e){  
      $("#pre_view").html("none"); 
    }
  });
}

// 태그 버튼 만들기
var setTagsButton = function(aData) {
  if(aData.themes.length>0) {
    $(aData.themes).each(function(i, e) {
      $("#tag_buttons").append("<button class='btn btn-"+(i==0?"info":"default")+" btn-sm tag-themes' tag-name='"+e.name+"'><i class='fa fa-"+(i==0?"check-":"")+"square-o'></i> "+e.name+" ("+e.count+") </button>");
    });
  }
  if(aData.divs.length>0) {
    $(aData.divs).each(function(i, e) {
      $("#tag_buttons").append("<button class='btn btn-default btn-sm tag-divs' tag-name='"+e.name+"'><i class='fa fa-square-o'></i> "+e.name+" ("+e.count+") </button>");
      //$("#tag_buttons").append("<button class='btn btn-"+(i==0?"info":"default")+" btn-sm tag-divs' tag-name='"+e.name+"'><i class='fa fa-"+(i==0?"check-":"")+"square-o'></i> "+e.name+" ("+e.count+") </button>");
    });
  }
  if(aData.controls.length>0) {
    $(aData.controls).each(function(i, e) {
      $("#tag_buttons").append("<button class='btn btn-default btn-sm tag-controls' tag-name='"+e.name+"'><i class='fa fa-square-o'></i> "+e.name+" ("+e.count+") </button>");
      //$("#tag_buttons").append("<button class='btn btn-"+(i==0?"info":"default")+" btn-sm tag-controls' tag-name='"+e.name+"'><i class='fa fa-"+(i==0?"check-":"")+"square-o'></i> "+e.name+" ("+e.count+") </button>");
    });
  }
  if(aData.tags.length>0) {
    $(aData.tags).each(function(i, e) {
      $("#tag_buttons").append("<button class='btn btn-default btn-sm tag-tags' tag-name='"+e.name+"'><i class='fa fa-square-o'></i> "+e.name+" ("+e.count+") </button>");
      //$("#tag_buttons").append("<button class='btn btn-"+(i==0?"info":"default")+" btn-sm tag-tags' tag-name='"+e.name+"'><i class='fa fa-"+(i==0?"check-":"")+"square-o'></i> "+e.name+" ("+e.count+") </button>");
    });
  }

  $("#tag_buttons").append("<button class='btn btn-danger btn-sm'> <i class='fa fa-search'></i> 조회하기 </button>");

  $("#tag_buttons .btn").off().on("click", function() {
    if( $(this).hasClass("btn-default") ) {
      $(this).removeClass("btn-default").addClass("btn-info");
      $(this).find("i").removeClass("fa-square-o").addClass("fa-check-square-o");
    } else {
      $(this).removeClass("btn-info").addClass("btn-default");
      $(this).find("i").addClass("fa-square-o").removeClass("fa-check-square-o");
    }
  });

  $("#tag_buttons .btn-danger").off().on("click", function() {
    getLayoutList();
  });
}

// 선택한 태그의 레이아웃 가져오기 
var getLayoutList = function () {
  var vTagsThemes = "";
  var vTagsDivs = "";
  var vTagsControls = "";
  var vTagsTags = "";

  $("#tag_buttons .btn-info").each(function(i, e) {
    if($(this).hasClass("tag-themes")) vTagsThemes += (vTagsThemes==""?"":",") + $(this).attr("tag-name");
    if($(this).hasClass("tag-divs")) vTagsDivs += (vTagsDivs==""?"":",") + $(this).attr("tag-name");
    if($(this).hasClass("tag-controls")) vTagsControls += (vTagsControls==""?"":",") + $(this).attr("tag-name");
    if($(this).hasClass("tag-tags")) vTagsTags += (vTagsTags==""?"":",") + $(this).attr("tag-name");
  });
  
  $.ajax({
    dataType: 'jsonp',
    data: {themes: vTagsThemes, divs: vTagsDivs, controls: vTagsControls, tags: vTagsTags},
    url: "http://x119.kr/layouts/tags_list.json",
    jsonpCallback: "callback",
    success: function( data, textStatus, jQxhr ){ 
      setLayoutsList(data);
    },
    error:function(e){  
      $("#layouts_list").html("");
    }
  });
}


var setLayoutsList = function(aDt) {
  //alert(aDt.length)
  $("#layouts_list").html("");
  $(aDt).each(function(i, e) {
    $("#layouts_list").append("<div class='panel panel-default col-xs-3'><div class='panel-body'><img src='http://x119.kr"+e.featured.sm.url+"' class='img-responsive' /></div></div>");
  });
}
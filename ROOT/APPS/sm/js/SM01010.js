//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 : @SM010100|공통코드
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
// @조홍식       	@엑스인터넷정보     @20140923    			1차 생성
//
//===========================================================================================
//	
//===========================================================================================

var select_all = "%"; 

function x_InitForm2(){
	if(typeof(x_InitForm3)!="undefined"){
		x_InitForm3();
	} 
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM01010", "SM_COMCODE|SM_COMCODE_C01", false, true);
	_X.InitGrid(grid_2, "dg_2", "100%", "100%", "sm", "SM01010", "SM_COMCODE|SM_COMCODE_C02", false, true);
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted){
		x_DAO_Retrieve();
	}
}

function x_DAO_Retrieve2(a_dg){
	dg_2.Reset();
	var param = new Array(S_SYS_ID.value, S_FIND.value, select_all);
	dg_1.Retrieve(param);
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id) {
	  case	"S_SYS_ID":
			  	x_DAO_Retrieve();
	  			break;
	}	
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	switch(a_obj.id) {
	  case	"S_FIND":
			  	x_DAO_Retrieve();
	  			break;
	}	
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

function x_DAO_Saved2(){

}

function x_DAO_Insert2(a_dg, row){
	if(a_dg.id == 'dg_1') {
		if(S_SYS_ID.value=="%") {
			_X.MsgBox("확인","업무구분을 확인 하세요!");
			return;
		}
	}else if(a_dg.id == "dg_2"){
		if(dg_1.RowCount == 0) return;
		var ls_hcode = dg_1.GetItem(dg_1.GetRow(), "HCODE");
		if(ls_hcode == null || ls_hcode == "") {
			_X.MsgBox("확인", "주코드를 입력하세요.");
			return;
		}
	}

	return 100;
}

function x_Insert_After2(a_dg, rowIdx){
	if(a_dg.id == "dg_1"){		
		dg_1.SetItem(rowIdx, "SYS_ID"  , S_SYS_ID.value);
		dg_1.SetItem(rowIdx, "SYS_NAME", _X.SelectGetFullText(S_SYS_ID));
	}else if(a_dg.id == "dg_2") {
		dg_2.SetItem(rowIdx, "HCODE" , dg_1.GetItem(dg_1.GetRow(), "HCODE"));
		dg_2.SetItem(rowIdx, "SYS_ID", dg_1.GetItem(dg_1.GetRow(), "SYS_ID"));
		dg_2.SetItem(rowIdx, "USE_YN", 'Y');
	}
	return 100;
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg){
	if(a_dg.id == 'dg_1') {
		if(dg_2.RowCount() > 0) {
			_X.MsgBox("확인", "부코드를 먼저 삭제 하세요.");
			return 0;
		}
	}
	
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 1;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){

}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	// dg_1 중복체크
	if(a_dg.id == "dg_1" && a_col == "HCODE"){
		var ls_code  = dg_1.GetItem(a_row, "HCODE");
		var ls_sysid = dg_1.GetItem(a_row, "SYS_ID");
		for(var i=1; i<=dg_1.RowCount(); i++){
			if( i == a_row ){
				continue ;
			}

			if( ls_code == dg_1.GetItem(i, "HCODE") && ls_sysid == dg_1.GetItem(i, "SYS_ID") ){
		   	dg_1.SetItem(a_row, "HCODE", "");
				_X.MsgBox("확인"," 중복된 데이터가 존재합니다.");
			  return;
			}
		}
	}

	// dg_2 중복체크
	if(a_dg.id == "dg_2" && a_col == "DCODE"){
		var ls_code  = dg_2.GetItem(a_row, "DCODE");
		var ls_sysid = dg_2.GetItem(a_row, "SYS_ID");
		for(var i=1; i<=dg_2.RowCount(); i++){
			if( i == a_row ){
				continue ;
			}

			if( ls_code == dg_2.GetItem(i, "DCODE") && ls_sysid == dg_2.GetItem(i, "SYS_ID") ){
		   	dg_2.SetItem(a_row, "DCODE", "");
				_X.MsgBox("확인"," 중복된 데이터가 존재합니다.");
			  return;
			}
		}
	}
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	switch (a_dg.id) {
		case	"dg_1":
					if(dg_2.IsDataChanged()){
						if(_X.MsgBoxYesNo('확인','변경된 데이타가 존재합니다.<br>계속 진행하시겠습니까?')=='2'){
							return false;
						}
					}
					break;
	}

	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
	switch (a_dg.id) {
		case	"dg_1":
					if(dg_1.RowCount() > 0) {
						dg_2.Retrieve(new Array(dg_1.GetItem(a_newrow, "HCODE"), dg_1.GetItem(a_newrow, "SYS_ID")));
					}
					break;
	}
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){

}

function xe_GridDataLoad2(a_dg){

}

function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){

}

function x_ReceivedCode2(a_retVal){
	
}
	
function xe_GridHeaderClick2(a_dg, a_col, a_colname){

}

function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){

}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){

}

function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){

}

function xe_ChartPointClick2(a_dg, a_event){

}

function xe_ChartPointSelect2(a_dg, a_event){

}

function xe_ChartPointMouseOver2(a_dg){

}

function xe_ChartPointMouseOut2(a_dg, a_event){

}

function x_Close2() {
	return 100;
}

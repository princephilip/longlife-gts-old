//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : @SM0401|SMS보내기
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
// 
//========================================================================================
//
//========================================================================================
var is_smstype = "4";
var il_msglen = 0;
var is_sended = "N";

function x_InitForm2(){
  _X.InitGrid(grid_1, "dg_1", "100%", "100%", "ss", "SS", "SM900400|SM_SMS_RECTEMP", true, true);
  $("#sender-phone-no").val( "07040109061" );
  
	//ar_list = _X.XmlSelect("hr","HR_COMCODE", "HR_COMCODE_R100",new Array('SM',  'GUBUN_CODE'), "json2");
	//dg_1.SetCombo("GUBUN_CODE",ar_list);	  
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
  if(a_dg != null && a_dg.id == "dg_1") { 
    // dg_1.SetCombo('GRADE_CODE', gf_getComboData("GRADE_CODE"), true);
    // dg_1.SetCombo('JOBKIND_CODE', gf_getComboData("JOBKIND_CODE"), true);
    // dg_1.SetCombo('DUTY_CODE', gf_getComboData("DUTY_CODE"), true);
    // dg_1.SetCombo('CONFIRM_TAG', gf_getComboData("APPROVAL_FLAG"), true);
  }
  if(ab_allcompleted){
    // _X.FormSetAllDisable('freeform', true);
    // _X.FormSetAllDisable('freeform2', true);
    x_DAO_Retrieve(dg_1);
  }
}

function x_DAO_Retrieve2(a_dg){
  return;
  
  if(a_dg != null && a_dg.id == "dg_1") {
    var param = new Array(top._CompanyCode, top._UserID);
    a_dg.Retrieve(param); 
  }
}

function xe_EditChanged2(a_obj, a_val){

}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){}
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
  return 100;
}
function x_DAO_ChkErr2(){
  /*var cData = dg_1.GetChangedData();
  var lo_map = new Object();
  var li_seq = 1;
  
  if(cData == null || cData == "" || typeof(cData) == "undefined") return 0;
  
  for(var i = 0; i < cData.length; i++){
    if(cData[i].job != 'I') continue;
    var ls_row = cData[i].idx;
    
    if(dg_1.GetItem(ls_row, 'APPLY_ORDER_DATE') == null || _X.Trim(dg_1.GetItem(ls_row, 'APPLY_ORDER_DATE')) == ""){
      _X.MsgBox('오류', '발령일자는 필수 입력 사항 입니다.');
      return false;
    }

    if(!_X.ChkDate(_X.ToString(dg_1.GetItem(ls_row, 'APPLY_ORDER_DATE'), "YYYYMMDD"), "발령일자")) return false;

    var ls_emp_no = dg_1.GetItem(ls_row, "EMP_NO");
    var ls_seq = _X.XmlSelect('hr', 'HR_ORDE_MASTER', 'HR_ORDE_MASTER_R02', new Array(top._CompanyCode, S_ORDER_NO.value, dg_1.GetItem(ls_row, "EMP_NO"), _X.ToString(dg_1.GetItem(ls_row, 'APPLY_ORDER_DATE'), "YYYYMMDD")), 'json2');
    
    if(lo_map[ls_emp_no] == null || lo_map[ls_emp_no] == "")
      lo_map[ls_emp_no] = 1;
    else
      lo_map[ls_emp_no]++;
    dg_1.SetItem(ls_row, 'ORDER_SEQ', _X.ToInt(ls_seq[0].code) + lo_map[ls_emp_no]);
    dg_1.SetItem(ls_row, 'SEQ', _X.ToInt(ls_seq[0].label) + li_seq);
    li_seq++;
  }
  return true;*/
}

function x_DAO_Saved2(){

  //alert(ls_rtn);
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}
function x_Insert_After2(a_dg, rowIdx){
	if(dg_1.RowCount() > 20) {
		_X.MsgBox('확인', '최대 20명까지 발송이 가능합니다.');
		dg_1.DeleteRow(0);
		return;
	}
  if(a_dg !=null && a_dg.id=="dg_1") {
    var tmaxSeq = getDataNextSeq(a_dg, "REC_SEQ");
    a_dg.SetItem(rowIdx, "COMPANY_CODE", top._CompanyCode);
    a_dg.SetItem(rowIdx, "USER_ID", top._UserID);
    a_dg.SetItem(rowIdx, "REC_SEQ", tmaxSeq);
    
    is_sended = "N";
    //_X.FormSetAllDisable('freeform2', false);
  }
}
/*직원, 하자신청자 이벤트
function pf_get_sawon() {
	 dg_1.InsertRow(dg_1);
  _SM.Findsawon(window,"Findsawon","", new Array());                
}

function pf_get_haja() {
	 dg_1.InsertRow(dg_1);
  _SM.Findhaja(window,"Findhaja","", new Array());                
}
*/
function pf_get_delete() {
 	x_DAO_Delete(dg_1);
}

function pf_get_insert() {
 	dg_1.InsertRow(0);
}

function x_DAO_Duplicate2(a_dg, row){
  return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){

}

function x_DAO_Delete2(a_dg){
  return 100;
}

function x_DAO_Excel2(a_dg){
  return 100;
}

function x_DAO_Print2(){

}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
  return 1;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){

}

function xe_GridDataChange2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){

}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
  return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){

}

function xe_GridDataLoad2(a_dg){
}

function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){

}

function x_ReceivedCode2(a_retVal){
  switch(_FindCode.finddiv){
//    case "Findsawon" :
//      dg_1.SetItem(dg_1.GetRow(), 'EMP_COMPANY_CODE', _FindCode.returnValue.COMPANY_CODE);
//      dg_1.SetItem(dg_1.GetRow(), 'EMP_NO', _FindCode.returnValue.EMP_NO);
//      dg_1.SetItem(dg_1.GetRow(), 'REC_NAME', _FindCode.returnValue.EMP_NAME);  
//      dg_1.SetItem(dg_1.GetRow(), 'HP_NO', _FindCode.returnValue.CELL_NO);  
//      break;
//    case "Findhaja" :
//      dg_1.SetItem(dg_1.GetRow(), 'REC_NAME', _FindCode.returnValue.RECE_MAN);  
//      dg_1.SetItem(dg_1.GetRow(), 'HP_NO', 		_FindCode.returnValue.CLAIM_TEL);  
//      break;      
    case "FindSmsMsg" :
      var tEmpty = true;

      if( $("#txtSmsMsg").val()!="" ) tEmpty = false;
      
      if(!tEmpty) {
        if( _X.MsgBoxYesNo("확인", "이미 입력한 문자열을 삭제하시겠습니까?") == "1" ) tEmpty = true;
      }

      if(tEmpty) $("#txtSmsMsg").val( _FindCode.returnValue.DVALUE2 );
      
      break;
  }
}
  
function xe_GridHeaderClick2(a_dg, a_col, a_colname){

}

function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){

}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){

}

function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){

}

function xe_ChartPointClick2(a_dg, a_event){

}

function xe_ChartPointSelect2(a_dg, a_event){

}

function xe_ChartPointMouseOver2(a_dg){

}

function xe_ChartPointMouseOut2(a_dg, a_event){

}

function x_Close2() {
  return 100;
}

/*
function pf_jsFindCode(a_findkey){
  
  if(dg_1.RowCount() > 0) {
  
    if(a_findkey == "FindEmpNoName") {
      if(dg_1.GetRowState(dg_1.GetRow()) == "I") {
        //_HR.FindEmpNoName(window,"FindEmpNoName","", new Array('%','100'));       
        _X.CommonFindCode(window,"사원검색","hr","FindEmpNoName","",new Array('%',top._CompanyCode),"FindEmpNoName|HrFindCode|FindEmpNoName",940, 410);
      } else {
        _X.MsgBox("추가 작업 시 검색이 가능합니다.");
      }
    } else {
      //_HR.FindEmpNoName(window,"FindEmpNoName","", new Array('%','100'));     
      _X.CommonFindCode(window, '부서코드 찾기', 'hr', 'F_DEPT_CODE', "", new Array(top._CompanyCode), 'FindDeptCode|HrFindCode|FindDeptCode', 600, 650);
      //_X.CommonFindCode(window,"부서검색","hr","F_DEPT_CODE","",new Array('%',top._CompanyCode),"FindEmpNoName|HrFindCode|FindEmpNoName",940, 410);
    }
  
  } else {
    _X.MsgBox("추가버튼을 누르신 후, 사원을 검색하실 수 있습니다.");
  }

}
*/
function pf_chk_len(a_text) {
  var strLen=0;
  var strLenTxt =  ' / 80 byte';
  for(i = 0; i < a_text.length;i++){
  if(escape(a_text.charAt(i)).length >= 4){
    strLen +=2;
    }
  else{
    if(escape(a_text.charAt(i)) !="%0D")
      strLen++;
    }   
    if(strLen>80){
      strLenTxt = ' MMS전환';
      is_smstype = "6";
      $("#sms-title").prop('readonly', false);
    } else {
      strLenTxt = ' / 80 byte';
      is_smstype = "4";
      //$("#sms-title").prop('readonly', true);
    }
  }                
  var ls_txt = '총 ' + String(strLen) + strLenTxt;
  $(".msg-length").html(ls_txt);
  il_msglen = strLen;
}
/*
function pf_get_message() {
  _X.CommonFindCode(window,"자주 사용하는 문자열","sm","FindSmsMsg","",new Array('%',top._CompanyCode),"FindSmsMsg|SmFindCode|FindSmsMsg",940, 410);
}
*/
//전송이벤트
function pf_send_sms() {
	
  var ls_smsmsg = $("#txtSmsMsg").val();
  var ls_sender_no = $("#sender-phone-no").val();
  var ls_msgtitle = $("#sms-title").val();
	
  _X.SendSMS('07040109061', ls_msgtitle, '테스트', ls_smsmsg, 'BM_ORDER_MASTER', 15, '', mytop._UserID);

  //var li_rtn = _X.ExecSql("ss", "SMS", "SM_SMS_TEMP_SEND", new Array(ls_msgtitle, '07040109061', ls_smsmsg));
  //if(li_rtn==1){
  //  alert('전송했습니다');
  //}
  return;
  /*
  if(dg_1.RowCount() < 1){
    _X.MsgBox("확인", "전송대상자가 없습니다!");
    return;
  }
  
  if(ls_smsmsg.length < 1){
    _X.MsgBox("확인", "문자내용이 입력되지 않았습니다. <br> 문자내용을 입력하여 주십시오.");
    return;
  }

  if(ls_sender_no.length < 5){
    _X.MsgBox("확인", "사용자 정보에 인증된 전화번호가 등록되어야 합니다.");
    return;
  } 
  
  if(dg_1.RowCount() > 20){
    _X.MsgBox("확인", "20명까지 전송이 가능합니다!");
    return;
  }  
  */
  /*
  var ls_sql = "SELECT COUNT(1) FROM SM_COMCODE_D WHERE HCODE='SMS_CERTFNO' AND DCODE=REPLACE(TRIM('" + txtSender.value + "'),'-','') "; 
  var li_cnt = ptdwo.SqlSelect("cm",ls_sql); 
  if(li_cnt == 0){
    ptcomm.MsgBox("확인", "입력하신 발신자 번호는 인증된 번호가 아닙니다.<BR>인증 후 사용 가능합니다.");
    return;
  }
  */
  /*
  if(is_smstype=="6" && ls_msgtitle.length == 0){
    _X.MsgBox("확인", "MMS의 경우 제목을 입력하셔야 합니다.");
    return;
  }
  
  if(il_msglen > 2000){
    _X.MsgBox("확인", "MMS는 2000 byte 미만으로 작성하셔야 합니다.");
    return;
  }

  if(is_sended == "Y"){
    if( _X.MsgBoxYesNo("확인", "이미 보낸 내역입니다. <br>다시 전송 하시겠습니까?") == "2" ) return;
  }
  */
	var cData = dg_1.GetChangedData();
	//if(cData==null || cData=="") return true;
	
	var sNo = 0;
	var chkMsg = "";

	for(var i=0; i<cData.length; i++){
		sNo = cData[i].idx;
			if(cData[i].data["HP_NO"] == "") {
				 _X.MsgBox("확인", "전화번호를 입력해주세요.");
				return;
			}
		}

  // 1. Grid 저장
  if(dg_1.IsDataChanged()){
  	 dg_1.Save('', '', 'N');	
//console.log(top._CompanyCode+ top._UserID +ls_msgtitle+ ls_smsmsg+ ls_sender_no+ top._ClientIP);
  	}

  // 2. 문자 전송
//console.log(top._CompanyCode + ' ' + top._UserID + ' ' + ls_msgtitle + ' '  + ls_smsmsg+ ' '  + ls_sender_no+ ' '  + top._ClientIP);
  //var li_rtn = _X.ExecProc('sm', 'SP_SM_SENDSMS', new Array(top._CompanyCode, top._UserID, ls_msgtitle, ls_smsmsg, ls_sender_no, top._ClientIP));
  //var li_rtn = _X.ExecProc('ss', 'SP_SM_SENDSMS', new Array('100', 'ADMIN', ls_msgtitle, ls_smsmsg, ls_sender_no, '192.168.100.185'));
  var li_rtn = _X.ExecSql("ss", "SMS", "SM_SMS_TEMP_SEND", new Array('070-4010-7581', '07040109061', ls_smsmsg));
alert(li_rtn)
  return;

   if(li_rtn>=0){ 
    is_sended = "Y"; 
    _X.MsgBox("확인", "문자전송 완료.");
      for(var i=dg_1.RowCount(); i>0; i--) {
        dg_1.DeleteRow( i );
      }
       dg_1.ClearRows();
      dg_1.Save('', '', 'N');
      dg_1.Retrieve( [top._CompanyCode, top._UserID] ); 
      is_sended = "N";
  } 

}
/*
function pf_AddNewAuth(a_dg) {
	var checkedRows = a_dg.GetCheckedRows();

  var groupCode   = dg_1.GetItem(dg_1.GetRow(),'HP_NO');
  for(var i=0;i<checkedRows.length;i++){ 	
  	if(checkedRows.length > 20 ){
  		_X.MsgBox("확인", "20명 이하 등록만 가능합니다.");
  		return;
  	} 		  	
  	
  	newrow = dg_1.InsertRow(0);
  	dg_1.SetItem(newrow,'EMP_COMPANY_CODE', a_dg.GetItem(checkedRows[i],"COMPANY_CODE"));    
  	dg_1.SetItem(newrow,'EMP_NO', a_dg.GetItem(checkedRows[i],"EMP_NO"));    
  	dg_1.SetItem(newrow,'REC_NAME', a_dg.GetItem(checkedRows[i],"EMP_NAME"));    
  	dg_1.SetItem(newrow,'HP_NO', a_dg.GetItem(checkedRows[i],"CELL_NO"));  
  	dg_1.SetItem(newrow,'GUBUN_CODE', a_dg.GetItem(checkedRows[i],"GUBUN_CODE"));      
  }
}

function pf_AddNewAuth2(a_dg) {
	var checkedRows = a_dg.GetCheckedRows();

  var groupCode   = dg_1.GetItem(dg_1.GetRow(),'HP_NO');
  for(var i=0;i<checkedRows.length;i++){
  	if(checkedRows.length > 20 ){
  		_X.MsgBox("확인", "20명 이하 등록만 가능합니다.");
  		return;
  	} 	*/	
/*
  	var arr1 = new Array(); //저장전 동일한 중복추가시 이미 추가시킨것은 다시 추가해도 그대로인 상태로 해줌.. 2015-03-25 상규&성룡
  	var arr2 = new Array();
  	arr1.push("EMP_COMPANY_CODE");
  	arr1.push("EMP_NO");
  	arr1.push("REC_NAME");
  	arr1.push("MENU_CODE");
  	arr2.push(String(mytop._CompanyCode));
  	arr2.push(String(groupCode));
  	arr2.push(String(a_dg.GetItem(checkedRows[i],"SYS_ID")));
  	arr2.push(String(a_dg.GetItem(checkedRows[i],"MENU_CODE")));

  	var res = dg_child.GetDuplicateRowByKey(arr2, arr1);*/

//  	if(res > 0) continue; //여기까지
/*  	
  	newrow = dg_1.InsertRow(0);
  	
  	dg_1.SetItem(newrow,'EMP_COMPANY_CODE', a_dg.GetItem(checkedRows[i],"COMPANY_CODE"));    
  	dg_1.SetItem(newrow,'REC_NAME', a_dg.GetItem(checkedRows[i],"RECE_MAN"));    
  	dg_1.SetItem(newrow,'HP_NO', a_dg.GetItem(checkedRows[i],"CLAIM_TEL"));    
  	dg_1.SetItem(newrow,'GUBUN_CODE', a_dg.GetItem(checkedRows[i],"GUBUN_CODE"));    
  }
}
*/
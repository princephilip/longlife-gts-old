/**
 *<pre>
 * Statements
 *</pre>
 
 * @Class Name :  SM0912.js
 * @Descriptio	  접속정보
 * @Modification Information
 * @
 * @  수정일        		 수정자                   수정내용
 * @ -------    --------    ---------------------------
 * @ 2013.03.03    박영찬        최초 생성
 * @
 */
function x_InitForm2(){
	_X.InitGrid(grid_parent, "dg_parent", "100%", "100%", "sm", "SM0912", "XG_SYS_SVCLOG|R_SM0913_01");
	_X.InitGrid(grid_child, "dg_child", "100%", "100%", "sm", "SM0912", "XG_SYS_SVCLOG|R_SM0913_02");
	return 0;
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted){
		dg_parent.SetFooter( true);
		dg_child.SetFooter( true);
		setTimeout('x_DAO_Retrieve(dg_parent)',0);
	}
	return 0;
}

//접속통계자료조회
function x_DAO_Retrieve2(a_dg){
	param = new Array(mytop._CompanyCode,_X.StrReplace(S_CON_FROM_DATE.value,'-'),_X.StrReplace(S_CON_TO_DATE.value,'-'),S_USER.value == "" ? '%' : S_USER.value);
	dg_child.Reset();
	dg_parent.Retrieve(param);
	return 0;
}

function xe_EditChanged2(a_obj, a_val){return 100;}
function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	switch(a_obj.id) {
	  case "S_USER":
	  	x_DAO_Retrieve(dg_parent);
	  	break;
	}
	return 0;
}
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){return 100;}
function x_DAO_Save2(a_dg){return 100;}
function x_DAO_Saved2(){return 100;}
function x_DAO_ChkErr2(){return true;}
function x_DAO_Insert2(a_dg, row){return 100;}
function x_Insert_After2(a_dg, rowIdx){return 100;}
function x_DAO_Duplicate2(a_dg, row){return 100;}
function x_Duplicate_After2(a_dg, rowIdx){return 100;}
function x_DAO_Delete2(a_dg, row){return 100;}
function x_DAO_Excel2(a_dg){return 100;}
function x_DAO_Print2(){return 100;}
function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 1;}
function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}
function xe_GridDataChange2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){return 100;}
function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){return true;}
function xe_GridRowFocusChanged2(a_dg, a_newrow){
	if(a_dg==dg_parent) {
		if(a_newrow>0) {
			param = new Array(mytop._CompanyCode,dg_parent.getItemString(a_newrow,'RQESTER_ID'),_X.StrReplace(S_CON_FROM_DATE.value,'-'),_X.StrReplace(S_CON_TO_DATE.value,'-'));
			dg_child.Retrieve(param);
		} 
	}
	return 100;
}
function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){return true;}
function xe_GridDataLoad2(a_dg){return 100;}
function x_FindCode2(a_dg, a_row, a_findKey, a_findstr, a_win) {return 100;}
function x_ReceivedCode2(a_returnValue) {return 100;}
function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){return 100;}
function xe_GridHeaderClick2(a_dg, a_col, a_colname){return 100;}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){return 100;}
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){return 100;}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){return 100;}
function xe_ChartPointClick2(a_dg, a_event){return 100;}
function xe_ChartPointSelect2(a_dg, a_event){return 100;}
function xe_ChartPointMouseOver2(a_dg){return 100;}
function xe_ChartPointMouseOut2(a_dg, a_event){return 100;}
function x_Close2() {return 100;}

//시스템 사용로그 집계
function pf_syslog_summary() {

	if(_X.MsgBoxYesNo("사용자 사용로그 정보를 집계 하시겠습니까?","",2)!=1) return;

	//권한설정 초기화 Procedure 실행
	li_rc = _X.ExecProc("sm", "dbo.PROC_XG_SYSLOG_SUMMARY", new Array(mytop._CompanyCode));	
	setTimeout('x_DAO_Retrieve(dg_parent)',0);
}
/**
 *<pre>
 * Statements
 *</pre>

 * @Class Name : SM0905.js
 * @Descriptio	  사용자정보관리
 * @Modification Information
 * @
 * @   수정일       수정자               수정내용
 * @ ----------    --------    ---------------------------
 * @ 2013.03.03    	박영찬     최초 생성
 * @ 2015.04.22 	 	SKLEE
 */
var li_idx = 0;
var ls_up_Array = new Array();
var ls_up_Pass = new Array();

var _S_STAT = ["1","2","9"]; //사용자구분:정직원, 계약직, 협력업체

function x_InitForm2(){
	_X.Tabs(tabs_1, 0);
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM0905", "SM_AUTH_USER|C_SM0905_01_2", false, true);	
	_X.InitGrid(grid_3, "dg_3", "100%", "100%", "sm", "SM0905", "SM_AUTH_USER|C_SM0905_01_3", false, true);
	_X.InitGrid(grid_4, "dg_4", "100%", "100%", "sm", "SM0905", "SM_AUTH_USER|C_SM0905_01", false, true);

	//	var comboData1 =  _X.XmlSelect("sm", "SM_AUTH_USER" ,"R_SM0905_02", new Array(mytop._CompanyCode), 'Array');
	//  _X.DropDownList_SetData(S_CORP, comboData1, '%', false, true, '전체');				//진행상태

	return 100;
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		//사용자구분
		var	comboData   = _X.CommXmlSelect('SM','USER_TAG');	//_X.XmlSelect("com", "COMMON", "W_COMMON_01", new Array('SM','USER_TAG'), "Array");
		var	comboData2  = _X.XmlSelect('hr', 'HR_COMCODE', 'GRADE_CODE'            , new Array() , 'json2');
		var	comboData3  = _X.XmlSelect('hr', 'HR_COMCODE', 'DUTY_CODE'                , new Array() , 'json2');

	//	_X.DDLB_SetData(S_USER_TAG, comboData, null, false, true);

		dg_1.SetCombo("USER_TAG", comboData);
		dg_1.SetCombo("POS_CODE", comboData2);
		dg_1.SetCombo("JOB_CODE", comboData3);

		dg_3.SetCombo("POS_CODE", comboData2);
		dg_3.SetCombo("JOB_CODE", comboData3);

		pf_set_button();
		x_DAO_Retrieve2(dg_1);
	}
	return 100;
}

function x_DAO_Retrieve2(a_dg){
	var stat = _S_STAT[tabs_1.GetTabIndex()];
	var params = new Array(mytop._CompanyCode, S_FIND.value, S_USE_YESNO.value, stat);

	//조회조건 : 유저아이디, 사용구분, 사용자구분
//	dg_1.Retrieve( [mytop._CompanyCode, S_FIND.value, S_USE_YESNO.value, S_USER_TAG.value] );

	switch(li_idx){
	 case 0:
      	dg_1.Retrieve( params );
      break;
      case 1:
		dg_3.Retrieve( params );
      break;
      case 2:
		dg_4.Retrieve( params );
      break;
	
	}
	return 0;
}

//html input type 의 자려가 변경된 경우 발생
function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id){
		case "S_USE_YESNO":
	//	case "S_USER_TAG":
			x_DAO_Retrieve();
			break;
	}
	return 100;
}

//enter key event 발생시 호출
function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	switch(a_obj.id){
		case "S_FIND":
			x_DAO_Retrieve();
			break;
	}
	return 0;
}

//keydown event 발생시 호출
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){
}

//데이타 저장
function x_DAO_Save2(a_dg){return 100;}

//데이타 저장후 callback
function x_DAO_Saved2(){

	//비번
	if(ls_up_Pass.length > 0){
		for(var i=0; i<ls_up_Pass.length; i++){
			_X.ExecProc("sm", "PROC_SM_AUTH_PASSWORD", new Array(ls_up_Pass[i][0], ls_up_Pass[i][1]));
		}
		ls_up_Pass = new Array();
	}
	return 100;
}

//데이타 유효성 check
function x_DAO_ChkErr2(){
	var ll_new_idx =  _X.GetTabIndex(tabs_1);
	var cData ;
	
	var sNo = 0;
	var chkMsg = "";
	var li_cnt = 0;
	var ls_value ="";

	if(ll_new_idx == 0){
		cData = dg_1.GetChangedData();

		if(cData==null || cData=="") return true;

		for(var i=0; i<cData.length; i++){
			if(cData[i].job == "D") continue;
			
			//중복체크...
			if (cData[i].job=="U") {

				ls_value = _X.XmlSelect("sm", "SM_AUTH_USER", "R_SM0905_02", new Array(mytop._CompanyCode, cData[i].data["LOGIN_ID"], cData[i].data["USER_ID"]), "array");
				li_cnt = Number(ls_value[0][0]);
				if(li_cnt > 0){
				chkMsg = "동일한 로그인ID가 존재합니다.\r\n" + "사번: "+ls_value[0][0];			
				break;
				}	
			}
		}

		if(chkMsg!="") {
			_X.MsgBox('확인', chkMsg);
			return false;
		}
	}else if(ll_new_idx == 1){

		cData = dg_3.GetChangedData();

		if(cData==null || cData=="") return true;

		for(var i=0; i<cData.length; i++){
			if(cData[i].job == "D") continue;

			sNo = cData[i].idx;
			//중복체크...
			
			if (cData[i].job=="I" || cData[i].job=="U") {
				if (cData[i].job=="I") {
		/*			if (pf_dupUSERID(cData[i].data["USER_ID"], sNo, "GRID",ll_dg) == false) {
						chkMsg = sNo + " 번째 행에 중복된 사용자아이디가 존재합니다";
						break;
					}*/
					if (pf_dupUSERID(cData[i].data["USER_ID"], sNo, "SQL",dg_3) == false) {
						chkMsg = "중복된 사용자 사번이 존재합니다";
						break;
					}

					//비번PROC로 변경
					ls_up_Pass[i] = new Array();
					ls_up_Pass[i][0] = dg_3.GetItem(cData[i].idx, "COMPANY_CODE");
					ls_up_Pass[i][1] = dg_3.GetItem(cData[i].idx, "USER_ID");

				}

				ls_value = _X.XmlSelect("sm", "SM_AUTH_USER", "R_SM0905_02", new Array(mytop._CompanyCode, cData[i].data["LOGIN_ID"], cData[i].data["USER_ID"]), "array");
				li_cnt = Number(ls_value[0][0]);
				if(li_cnt > 0){
				chkMsg = "동일한 로그인ID가 존재합니다.\r\n" + "사번: "+ls_value[0][1];			
				break;
				}	
			}
		}

		if(chkMsg!="") {
			_X.MsgBox('확인', chkMsg);
			return false;
		}


	}
	return true;
}

//grid insert/add
function x_DAO_Insert2(a_dg, row){
	_X.FindEmp(window, "G_Emp", "", new Array( mytop._CompanyCode, '2') );
	return;
	return 100;
}

//grid insert 수행후 발생
function x_Insert_After2(a_dg, rowIdx){
	a_dg.SetItem(rowIdx, "COMPANY_CODE", mytop._CompanyCode);
	a_dg.SetItem(rowIdx, "USE_YESNO","Y");
	a_dg.SetItem(rowIdx, "LOGIN_LOCK_YESNO", "N");
	a_dg.SetItem(rowIdx, "LOGIN_COUNT", 0);
	//USER_ID.focus();
	return 0;
}

//grid 복제
function x_DAO_Duplicate2(a_dg, row){return 100;}

//grid 복제후
function x_Duplicate_After2(a_dg, rowIdx){return 100;}

//grid 삭제
function x_DAO_Delete2(a_dg, row){
//	if(_X.MsgBoxYesNo("선택된 사용자 정보를 삭제 하시겠습니까?") == "2") return;
	var ll_new_idx =  _X.GetTabIndex(tabs_1);

	if(ll_new_idx == 1){
		if(_X.MsgBoxYesNo("사용자 ID: " + dg_3.GetItem(dg_3.GetRow(), "USER_ID") + "\r\n사용자명: " + dg_3.GetItem(dg_3.GetRow(), "USER_NAME") + "\r\n선택된 사용자 정보를 삭제 하시겠습니까?") == "2") return;
		dg_3.DeleteRow(dg_3.GetRow());
		return 0;
	}else if(ll_new_idx == 2){
		if(_X.MsgBoxYesNo("사용자 ID: " + dg_4.GetItem(dg_4.GetRow(), "USER_ID") + "\r\n사용자명: " + dg_4.GetItem(dg_4.GetRow(), "CUST_NAME") + "\r\n선택된 사용자 정보를 삭제 하시겠습니까?") == "2") return;
		dg_4.DeleteRow(dg_4.GetRow());
		return 0;
	}
}

//excel 저장
function x_DAO_Excel2(a_dg){return 100;}

//출력
function x_DAO_Print2(){return 100;}

//tab 변경시
function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}

//tab 변경시
function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	$("[id^='tabsub_']").hide();
  	$('#tabsub_'+_X.ToString(a_new_idx+1)).show();
  	
    li_idx = a_new_idx;
  	xe_BodyResize();
	pf_set_button();
	x_DAO_Retrieve2(dg_1);

}

//grid data 변경시
function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	return 0;
}

//grid row focus change
function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

//grid row focus change
function xe_GridRowFocusChanged2(a_dg, a_newrow, a_oldrow){
}

//item focus change event
function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){return true;}

//grid data loaded
function xe_GridDataLoad2(a_dg){return 100;}

//찾기창 호출 후
function x_FindCode2(a_dg, a_row, a_findKey, a_findstr, a_win) {return 100;}

//찾기 화면에서 선택값을 넘긴 경우 호출
function x_ReceivedCode(a_retVal){
	switch(_FindCode.finddiv){
		case 'F_DEPT_CODE2':
			dg_3.SetItem(dg_3.GetRow(), 'DEPT_CODE', _FindCode.returnValue.DEPT_CODE);
			dg_3.SetItem(dg_3.GetRow(), 'DEPT_NAME', _FindCode.returnValue.DEPT_NAME);
			break;
		case "Cust_Code":
		  	dg_1.SetItem(dg_1.GetRow(), 'VENDOR_CODE', _FindCode.returnValue.CUST_ID);
			dg_1.SetItem(dg_1.GetRow(), 'CUST_NAME', _FindCode.returnValue.CUST_NAME);
			break;
		case "G_Emp":
			if ( a_retVal == null ) {
				return;
			}

			var NRow = dg_3.InsertRow(0);
			dg_3.SetItem(NRow, "COMPANY_CODE", mytop._CompanyCode);
			dg_3.SetItem(NRow, "USER_ID", a_retVal.EMP_NO);
			dg_3.SetItem(NRow, "USER_NAME", a_retVal.EMP_NAME);	
			dg_3.SetItem(NRow, "RRN_NO", a_retVal.RRN_NO);	
			dg_3.SetItem(NRow, "USER_TAG", "2");		
			dg_3.SetItem(NRow, "USE_YN", "Y");
			dg_3.SetItem(NRow, "LOGIN_LOCK_YESNO", "N");
			dg_3.SetItem(NRow, "LOGIN_COUNT", 0);

			break;
		case "PGM_CODE":
			if (_X.GetTabIndex(tabs_1) == 0) {
				dg_1.SetItem(dg_1.GetRow(), "STD_CODE", _FindCode.returnValue.PGM_CODE);	
				dg_1.SetItem(dg_1.GetRow(), "PGM_NAME", _FindCode.returnValue.PGM_NAME);					
			} else if (_X.GetTabIndex(tabs_1) == 1) {
				dg_3.SetItem(dg_3.GetRow(), "STD_CODE", _FindCode.returnValue.PGM_CODE);	
				dg_3.SetItem(dg_3.GetRow(), "PGM_NAME", _FindCode.returnValue.PGM_NAME);					
			}
			break;
	}
}

//grid item button click event
function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){
	switch(a_col) {
		case "DEPT_NAME":
			_X.CommonFindCode(window,"부서코드찾기","hr","F_DEPT_CODE2","",new Array(mytop._CompanyCode,'%'),"FindDeptCode|HrFindCode|FindDeptCode",500, 650);
			break;
		case "CUST_NAME":
			_X.FindCustCode(window ,"Cust_Code" ,"" , new Array('20'));
			break;
		case "STD_CODE":
			_X.CommonFindCode(window,"프로그램코드찾기","com","PGM_CODE", "", new Array(mytop._CompanyCode),"FindPgmCode|FindCode|FindPgmCode",500, 550);
			break;
	}
	return 100;
}

//header click event
function xe_GridHeaderClick2(a_dg, a_col, a_colname){return 100;}

//item double click event
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){return 100;}

//tree item checked event
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){return 100;}

//tree item checked event
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){return 100;}

//- anychart  point click event
function xe_ChartPointClick2(a_dg, a_event){return 100;}

//- anychart  point select event
function xe_ChartPointSelect2(a_dg, a_event){return 100;}

//- anychart  point mouse over event
function xe_ChartPointMouseOver2(a_dg){return 100;}

//- anychart  point mouse out event
function xe_ChartPointMouseOut2(a_dg, a_event){return 100;}

//window(mdi) close
function x_Close2() {return 100;}

//메뉴코드 중복체크
function pf_dupUSERID(a_value, a_row,  a_type, a_dg) {
	if(a_type == "GRID") {
		for(var i = 1; i < dg_1.RowCount(); i++){
			if (a_dg.GetItem(i,"USER_ID") == a_value && i != a_row) {
				return false;
			}
		}
	} else {
		var data = _X.XmlSelect("sm", "SM_AUTH_USER", "R_SM0905_01", new Array(mytop._CompanyCode,a_value), "array");
		if(data[0][0] == a_value) return false;
	}
	return true;
}


function pf_resetPassword() {

	var ll_new_idx =  _X.GetTabIndex(tabs_1);

	var ls_user="";

	if(ll_new_idx == 0){
		if(dg_1.RowCount() < 1) return;

		if(dg_1.IsDataChanged()) {
			_X.MsgBox("확인", "변경된 데이타가 존재합니다.\n저장 후 다시 시도하여 주십시오.");
			return;
		}

		ls_user = dg_1.GetItem(dg_1.GetRow(), "USER_ID");
	//	if(_X.MsgBoxYesNo("비밀번호 초기화를 진행 하시겠습니까?") == "2") return;
		if(_X.MsgBoxYesNo("사용자 ID: " + ls_user + "\r\n사용자명: " + dg_1.GetItem(dg_1.GetRow(), "USER_NAME") + "\r\n비밀번호를 초기화 하시겠습니까?") == "2") return;

		_X.ExecProc("sm", "PROC_SM_AUTH_PASSWORD", new Array(mytop._CompanyCode, ls_user));
		_X.MsgBox("확인", "비밀번호 변경이 완료되었습니다.");
	}else if(ll_new_idx == 1){
		if(dg_3.RowCount() < 1) return;

		if(dg_3.IsDataChanged()) {
			_X.MsgBox("확인", "변경된 데이타가 존재합니다.\n저장 후 다시 시도하여 주십시오.");
			return;
		}

		ls_user = dg_3.GetItem(dg_3.GetRow(), "USER_ID");
	//	if(_X.MsgBoxYesNo("비밀번호 초기화를 진행 하시겠습니까?") == "2") return;
		if(_X.MsgBoxYesNo("사용자 ID: " + ls_user + "\r\n사용자명: " + dg_3.GetItem(dg_3.GetRow(), "USER_NAME") + "\r\n비밀번호를 초기화 하시겠습니까?") == "2") return;

		_X.ExecProc("sm", "PROC_SM_AUTH_PASSWORD", new Array(mytop._CompanyCode, ls_user));
		_X.MsgBox("확인", "비밀번호 변경이 완료되었습니다.");
	}else {
		if(dg_4.RowCount() < 1) return;

		if(dg_4.IsDataChanged()) {
			_X.MsgBox("확인", "변경된 데이타가 존재합니다.\n저장 후 다시 시도하여 주십시오.");
			return;
		}

		ls_user = dg_4.GetItem(dg_4.GetRow(), "USER_ID");
	//	if(_X.MsgBoxYesNo("비밀번호 초기화를 진행 하시겠습니까?") == "2") return;
		if(_X.MsgBoxYesNo("사용자 ID: " + ls_user + "\r\n사용자명: " + dg_4.GetItem(dg_4.GetRow(), "CUST_NAME") + "\r\n비밀번호를 초기화 하시겠습니까?") == "2") return;

		_X.ExecProc("sm", "PROC_SM_AUTH_PASSWORD", new Array(mytop._CompanyCode, ls_user));
		_X.MsgBox("확인", "비밀번호 변경이 완료되었습니다.");
	}

	/*
	if(_X.MsgBoxYesNo("사용자 ID: " + ls_user + "\r\n사용자명: " + dg_1.GetItem(dg_1.GetRow(), "USER_NAME") + "\r\n비밀번호를 초기화 하시겠습니까?") == "1") {
		dg_1.SetItem(dg_1.GetRow(), "OLD_PASSWORD", dg_1.GetItem(dg_1.GetRow(), "USER_PASSWORD"));
		dg_1.SetItem(dg_1.GetRow(), "USER_PASSWORD", ls_user);
		dg_1.SetItem(dg_1.GetRow(), "PASSWORD_UPDATE", _X.ToString(_X.GetSysDate(), "YYYY-MM-DD"));
		_X.MsgBox("비밀번호가 초기화 되었습니다.");
		dg_1.Save(null, null, "N");
	}
	*/
}

//권한 버튼 설정
function pf_set_button(a_new_idx) {
	$('#xBtnAppend,#xBtnDelete').hide();
	var a_new_idx = (a_new_idx != null ? a_new_idx : _X.GetTabIndex(tabs_1));
	switch(a_new_idx) {
		case 0: //정직원
			$('#xBtnAppend,#xBtnDelete').hide();
			break;
		case 1: //계약직
			$('#xBtnAppend,#xBtnDelete').show();
			break;
		case 2: //협력
			$('#xBtnAppend').hide();
			$('#xBtnDelete').show();
			break;		
	}
}

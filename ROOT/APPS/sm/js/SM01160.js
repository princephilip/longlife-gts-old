 //===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 : 메뉴얼
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
//   smlee							 20171113				메뉴얼
//
//===========================================================================================

function x_InitForm2(){
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM01160", "SM01160|SM01160_01", true, true);
	_X.InitGrid(grid_2, "dg_2", "100%", "100%", "sm", "SM01160", "SM01160|SM01160_02", false, true);
	_X.InitGrid(grid_3, "dg_3", "100%", "100%", "sm", "SM01160", "SM01160|SM01160_03", false, true);

	if(vSys!="") S_SYS_ID.value = vSys;
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		pf_set_pgm_lev();	

		x_DAO_Retrieve2(dg_1);	
	}
}

function x_DAO_Retrieve2(a_dg){ 
	var param;
	switch(a_dg.id){
		case 'dg_1':
		 var ls_div = '1';
		    if(S_PGM_LEV.value == '%'){
				ls_div = '0';
		    }			
			param = new Array(mytop._CompanyCode,  S_SYS_ID.value,  ls_div, S_PGM_LEV.value);				
		break;		
		case 'dg_2':
			param = new Array(mytop._CompanyCode, dg_1.GetItem(dg_1.GetRow(), 'SYS_ID_M'),  dg_1.GetItem(dg_1.GetRow(), 'PGM_CODE_M'), '1' );			
		break;	
		case 'dg_3':
			param = new Array(mytop._CompanyCode, dg_1.GetItem(dg_1.GetRow(), 'SYS_ID_M'),  dg_1.GetItem(dg_1.GetRow(), 'PGM_CODE_M'), '2' );			
		break;	
	}
	a_dg.Retrieve(param);
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id){
		case "S_SYS_ID":
			pf_set_pgm_lev();
			x_DAO_Retrieve(dg_1);
		break;
		case "S_PGM_LEV":
			x_DAO_Retrieve(dg_1);			
		break;
	}
}



function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	if(a_obj.id == 'S_FIND'){		
		x_DAO_Retrieve(dg_1);
	}
}
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	var cData1 		= dg_1.GetChangedData();
	for (i=0 ; i < cData1.length; i++){		
		if(cData1[i].job == 'I' || cData1[i].job == 'U' ){	
			if(dg_1.GetItem(dg_1.GetRow(), 'PGM_CODE') == '' || dg_1.GetItem(dg_1.GetRow(), 'PGM_CODE') == null){			
				_X.ES("sm", "SM01160", "SM01160_I01", [mytop._CompanyCode, 
														dg_1.GetItem(dg_1.GetRow(), 'SYS_ID_M'), 
														dg_1.GetItem(dg_1.GetRow(), 'PGM_CODE_M'),
														dg_1.GetItem(dg_1.GetRow(), 'MENU_CONTENT'),
														dg_1.GetItem(dg_1.GetRow(), 'MENU_PATH'),
														dg_1.GetItem(dg_1.GetRow(), 'MENU_ORDER'), 
														mytop._UserID, mytop._ClientIP
														]);
				_X.Noty('저장되었습니다.');	
				dg_1.SetItem(cData1[i].idx, 'COMPANY_CODE', mytop._CompanyCode ) ;	
				dg_1.SetItem(cData1[i].idx, 'SYS_ID', dg_1.GetItem(dg_1.GetRow(), 'SYS_ID_M') ) ;
				dg_1.SetItem(cData1[i].idx, 'PGM_CODE', dg_1.GetItem(dg_1.GetRow(), 'PGM_CODE_M') ) ;		
			}
		}
	}

	var cData2 		= dg_2.GetChangedData();
	var grid2_max	= _X.XmlSelect("sm", "SM01160", "SM01160_R02" , new Array(mytop._CompanyCode, dg_1.GetItem(dg_1.GetRow(), 'SYS_ID_M'), dg_1.GetItem(dg_1.GetRow(), 'PGM_CODE_M'), '1' ) , 'json2');
	for (i=0 ; i < cData2.length; i++){
		if(cData2[i].job == 'D') continue;
		if(cData2[i].job == 'I'){			
			dg_2.SetItem(cData2[i].idx, 'MENU_SEQ', String(++grid2_max[0].max_seq) ) ;
		}		
	}

	var cData3 		= dg_3.GetChangedData();
	var grid3_max	= _X.XmlSelect("sm", "SM01160", "SM01160_R02" , new Array(mytop._CompanyCode, dg_1.GetItem(dg_1.GetRow(), 'SYS_ID_M'), dg_1.GetItem(dg_1.GetRow(), 'PGM_CODE_M'), '2' ) , 'json2');
	for (i=0 ; i < cData3.length; i++){
		if(cData3[i].job == 'D') continue;
		if(cData3[i].job == 'I'){				
			dg_3.SetItem(cData3[i].idx, 'MENU_SEQ', String(++grid3_max[0].max_seq) ) ;
		}		
	}

	return true;
	
}

function x_DAO_Saved2(){

}

function x_DAO_Insert2(a_dg, row){
	return 100;
}

function x_Insert_After2(a_dg, rowIdx){
	switch(a_dg.id){
		case 'dg_2':
			dg_2.SetItem(rowIdx, 'COMPANY_CODE'		, mytop._CompanyCode);
			dg_2.SetItem(rowIdx, 'SYS_ID'			, dg_1.GetItem(dg_1.GetRow(), 'SYS_ID_M'));
			dg_2.SetItem(rowIdx, 'PGM_CODE'			, dg_1.GetItem(dg_1.GetRow(), 'PGM_CODE_M'));
			dg_2.SetItem(rowIdx, 'MENU_TAG'			, '1');			
		break;
		case 'dg_3':
			dg_3.SetItem(rowIdx, 'COMPANY_CODE'		, mytop._CompanyCode);
			dg_3.SetItem(rowIdx, 'SYS_ID'			, dg_1.GetItem(dg_1.GetRow(), 'SYS_ID_M'));
			dg_3.SetItem(rowIdx, 'PGM_CODE'			, dg_1.GetItem(dg_1.GetRow(), 'PGM_CODE_M'));
			dg_3.SetItem(rowIdx, 'MENU_TAG'			, '2');			
		break;
	}
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){

}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){

}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){

	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
	if ( a_dg.id == "dg_1" ) {
		x_DAO_Retrieve2(dg_2);
		x_DAO_Retrieve2(dg_3);
		uf_picture_set(a_dg,a_newrow);
	}

}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridItemFocusChanged(a_dg, a_newrow, a_newcol){

}

function xe_GridDataLoad2(a_dg){
	if(a_dg==dg_1&&vPgm!="") {
		for(var i=1;i<=dg_1.RowCount();i++) {
			if(dg_1.GetItem(i, "PGM_CODE")==vPgm) dg_1.SetRow(i, "PGM_CODE")
		}
	}

	if(a_dg != null && a_dg.id == "dg_1") {

		if(dg_1.GetItem(dg_1.GetRow(),'FILE_NAME') == ''){
			$("#photo_1").attr("src", mytop._CPATH +"/images/noimage.jpg" + "?" + new Date().getTime());	
		}else{
			uf_picture_set(a_dg,dg_1.GetRow());
		}
		
	}

}

function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){
}

function x_ReceivedCode2(a_retVal){
	
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){

}

function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){

}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){

}

function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){

}

function xe_ChartPointClick2(a_dg, a_event){

}

function xe_ChartPointSelect2(a_dg, a_event){

}

function xe_ChartPointMouseOver2(a_dg){

}

function xe_ChartPointMouseOut2(a_dg, a_event){

}

function x_Close2() {
	return 100;
}

function pf_ImageUpload(a_obj) {
	if(dg_1.GetItem(dg_1.GetRow(), 'PGM_CODE') == '' || dg_1.GetItem(dg_1.GetRow(), 'PGM_CODE') == null){
		if(MENU_CONTENT.value == '' &&  MENU_PATH.value == '' && MENU_ORDER.value == ''){
			_X.Noty("메뉴상세 등록 후 작업진행하시기 바랍니다.");
		}else{
			_X.Noty("변경된 데이터 저장 후 작업진행하시기 바랍니다.");
	    }
		return;
	}
		var fileid =  dg_1.GetItem(dg_1.GetRow(), "FILE_NAME");
		_X.FileUpload(1, 'SM', "FILE_NAME", fileid, false);
}

//image uploaded callback
function x_FileUploaded(obj, ReturnValue) {
	if(ReturnValue==null){
		return;
	}
	switch(obj) {
		case "FILE_NAME":
			dg_1.SetItem(dg_1.GetRow(), "FILE_NAME", ReturnValue[0].atchFileId);
			dg_1.SetItem(dg_1.GetRow(), "FILE_PATH", ReturnValue[0].fileStreCours + "\\" + ReturnValue[0].streFileNm);
			_X.Noty("첨부파일 정상적으로 업로드 되었습니다.", null, null, 1500);
			break;
	}

	dg_1.Save(null, null, "Y");
	d = new Date();
	$("#photo_1").attr("src", mytop._rootFileUpload + ReturnValue[0].fileStreCours + "\\" + ReturnValue[0].streFileNm + "?"+d.getTime());
}

//파일삭제
function x_FileDeleted(obj) {
	switch(obj) {
		case "FILE_NAME":
			dg_1.SetItem(dg_1.GetRow(),"FILE_NAME","");
			dg_1.SetItem(dg_1.GetRow(),"FILE_PATH","\\");
			break;

		case "fileupload":
			dg_1.SetItem(dg_1.GetRow(),"ISSUE_MAIN_FILE","");
			S_FILE_COUNT.value = _X.XmlSelect("com", "COMMON", "SM_COMM_FILE_COUNT", new Array(dg_1.GetItem(dg_1.GetRow(), 'ISSUE_MAIN_FILE')), "array")[0][0];// 첨부파일갯수
			dg_1.Save(null, null, "N");
			return;
		  break;
	}
	dg_1.Save(null, null, "N");
	_X.Noty("첨부파일 정상적으로 삭제 되었습니다.", null, null, 1500);

	$("#photo_1").attr("src", mytop._CPATH+"/Theme/images/noimage.jpg" + "?" + new Date().getTime());
}

function uf_picture_set(a_dg, a_newrow){
	//이미지 리셋시켜주는로직
	var photo1 = dg_1.GetItem(a_newrow, 'FILE_NAME');
	if(photo1 != null && photo1 != "") {
		console.log(mytop._rootFileUpload + dg_1.GetItem(a_newrow, 'FILE_PATH'))
		$("#photo_1").attr("src", mytop._rootFileUpload + dg_1.GetItem(a_newrow, 'FILE_PATH'));
	} else {
		$("#photo_1").attr("src", mytop._CPATH +"/Theme/images/noimage.jpg" + "?" + new Date().getTime());
	}
}

function pf_set_pgm_lev() {
  	var ls_lev = _X.XmlSelect("sm", "SM01160", "GET_PGM_LEV2" , new Array(mytop._CompanyCode,  S_SYS_ID.value) , 'json2');
	_X.DDLB_SetData(S_PGM_LEV, ls_lev, '%', false, true, '전체');	
}


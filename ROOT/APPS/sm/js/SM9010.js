﻿//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 : [SM9010.js] 프로그램 수정요청  (AS-IS: SM05210)
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
//   smlee							 20171020				프로그램 수정요청
//   이상규         엑스인터넷정보      20171024      메세지 기능 추가
//===========================================================================================

function x_InitForm2(){	
 _X.InitGrid( {grid: dg_1, sqlId: "SM9010_01"           , share: true ,  updatable: true} );
 _X.InitGrid( {grid: dg_2, sqlId: "SM_DEV_ISSUE_MSG_C01", share: false,  updatable: true} );
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		setInterval(function () {
				if ( dg_1.GetRow() > 0 ) {
					var cnt = _X.XS("sm", "SM9010", "SM_DEV_ISSUE_MSG_R02", [dg_1.GetItem(dg_1.GetRow(), "DOC_SEQ", false)], "array")[0][0];
					
					if ( cnt != dg_2.RowCount() ) {
						x_DAO_Retrieve2(dg_2);
					}
				}
		}, 2000);
		
//		var sys_div = _X.XmlSelect("sm", "SM_AUTH_SYS", "W_SM0915_01", new Array(mytop._CompanyCode), "json2");
		var sys_div = _X.XmlSelect("sm", "SM9010", "SM9010_SYSID", new Array(mytop._CompanyCode), "json2");
		dg_1.SetCombo("SYS_ID", sys_div);

		var order_div = [{"code":"1","label":"A"}, {"code":"2","label":"B"}, {"code":"3","label":"C"}];
		dg_1.SetCombo("WORK_ORDER", order_div);
	
		pf_set_auth();
		x_DAO_Retrieve2(dg_1);
	}
}

function x_DAO_Retrieve2(a_dg){
	switch(a_dg.id){
		case 'dg_1':
			dg_2.Reset();
			
			a_dg.Retrieve([S_SYS_ID.value, S_END_YN.value, S_DEV_TAG.value, S_DEV_END.value, S_KEYWORD.value]);
			break;
			
		case 'dg_2':
			a_dg.Retrieve([dg_1.GetItem(dg_1.GetRow(), "DOC_SEQ")]);
			break;
	}
//	groupByDuration(a_dg)
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id){
		case 'S_SYS_ID':
		case 'S_END_YN':	
		case 'S_DEV_TAG':	
		case 'S_DEV_END':		
			x_DAO_Retrieve(dg_1);
		break;
	}
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	switch(a_obj.id){
		case 'S_KEYWORD':
			x_DAO_Retrieve(dg_1);
			break;
		
		case 'MSG_INPUT':
			if ( a_ctrlKey ) {
				var docSeq = dg_1.GetItem(dg_1.GetRow(), "DOC_SEQ");
				dg_2.InsertRow(0, {
						DOC_SEQ: docSeq,
						MSG_SEQ: _X.XS("sm", "SM9010", "SM_DEV_ISSUE_MSG_R01", [docSeq], "array")[0][0],
						MSG_ID : mytop._UserID,
						MSG_NM : mytop._UserName,
						MSG_CONTENT: a_obj.value
				});
				
				a_obj.value = "";
				
				dg_2.Save(null, null, "N");
				x_DAO_Retrieve2(dg_2);
			}
			break;
	}
}
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	var cData1 		= dg_1.GetChangedData();
	var grid1_max	= _X.XmlSelect("sm", "SM9010", "SM9010_R01" , new Array('') , "array");
	for (i=0 ; i < cData1.length; i++){
		if(cData1[i].job == 'I' ){			
			dg_1.SetItem(cData1[i].idx, 'DOC_SEQ', String(++grid1_max[0][0]) ) ;
		}
	}

	return true;
}

function x_DAO_Saved2(){
	x_DAO_Retrieve2(dg_2);
}

function x_DAO_Insert2(a_dg, row){
	return 100;
}

function x_Insert_After2(a_dg, rowIdx){
  dg_1.SetItem(rowIdx, "REQ_NAME",   mytop._UserName);
  dg_1.SetItem(rowIdx, "REG_DATE",   _X.ToString(_X.GetSysDate(),'YYYYMMDD'));
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){

}

function x_DAO_Delete2(a_dg){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){

}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	if(a_dg.id == "dg_1"){
		switch (a_col){
			case "END_TAG":
				if(a_newvalue == "Y"){
					dg_1.SetItem(a_row, "END_DATE", _X.ToString(_X.GetSysDate(), "YYYYMMDD"));					
				}else{
					dg_1.SetItem(a_row, "END_DATE", "");				
				}
				break;
		}
	}

}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
	if ( a_dg.id == "dg_1" ) {
		x_DAO_Retrieve2(dg_2);
	}
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridItemFocusChanged(a_dg, a_newrow, a_newcol){

}

function xe_GridDataLoad2(a_dg){
	if ( a_dg.id == "dg_2" ) {
		pf_setMsg();
	}
}

function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){ //  a_col <-> a_colname 순서바뀜
	var fileId =  dg_1.GetItem(a_row, "FILE_ID");
	_X.FileUpload(1, 'SM', "FILE_ID", fileId, false);
}

function x_ReceivedCode2(a_retVal){	
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){

}

function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){

}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){

}

function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){

}

function xe_ChartPointClick2(a_dg, a_event){

}

function xe_ChartPointSelect2(a_dg, a_event){

}

function xe_ChartPointMouseOver2(a_dg){

}

function xe_ChartPointMouseOut2(a_dg, a_event){

}

function x_Close2() {
	return 100;
}

// 권한 세팅
function pf_set_auth() {  
  if(!_X.HaveActAuth('시스템관리자') ) { 
//  	dg_1.SetAutoReadOnly("WORK_ORDER"  , function (args) {return true;}, true);
    dg_1.SetAutoReadOnly("DEV_END_TAG"  , function (args) {return true;}, true);
    dg_1.SetAutoReadOnly("DEV_NAME"  , function (args) {return true;}, true);
    dg_1.SetAutoReadOnly("END_TAG"  , function (args) {return true;}, true);
    dg_1.SetAutoReadOnly("DEV_TAG"  , function (args) {return true;}, true);
  }
}

// 파일업로드 후
function x_FileUploaded(obj, ReturnValue) {
	if ( ReturnValue != null ) {
		switch(obj) {
			case "FILE_ID":
				dg_1.SetItem(dg_1.GetRow(), "FILE_ID"  , ReturnValue[0].atchFileId);
				dg_1.SetItem(dg_1.GetRow(), "FILE_NAME", ReturnValue[0].orignlFileNm);
				dg_1.SetItem(dg_1.GetRow(), "FILE_PATH", ReturnValue[0].fileStreCours);
				dg_1.SetItem(dg_1.GetRow(), "FILE_TYPE", ReturnValue[0].fileExtsn);
				dg_1.Save(null, null, "Y");
				break;
		}
	}
}

// 파일 삭제 후
function x_FileDeleted(obj) {
	switch(obj) {
		case "FILE_ID":
			dg_1.SetItem(dg_1.GetRow(), "FILE_ID"  , "");
			dg_1.SetItem(dg_1.GetRow(), "FILE_NAME", "");
			dg_1.SetItem(dg_1.GetRow(), "FILE_PATH", "");
			dg_1.SetItem(dg_1.GetRow(), "FILE_TYPE", "");
			dg_1.Save(null, null, "Y");
			break;
	}
}

// 메세지 셋팅
function pf_setMsg() {
	$("#MSG_CONTENT").empty();
	
	var msgHtml = "<pre id='pre_content' style='height: 299px; margin-top: -5px; background: lightgoldenrodyellow;'>";
	var flag;
	//var msgSeq;
	
	for ( var i = 1, ii = dg_2.RowCount(); i <= ii; i++ ) {
		flag   = dg_2.GetItem(i, "MSG_ID") == mytop._UserID;
		
		if ( flag ) {
			msgHtml += "<div style='float: right'>";
			//msgHtml += "<div style='float: right' id='msg_seq_" + dg_2.GetItem(i, "MSG_SEQ") + "'>";
			
			//msgSeq = dg_2.GetItem(i, "MSG_SEQ");
		} else {
			msgHtml += "<div style='float: left'>";
			//msgHtml += "<div style='float: left' id='msg_seq_" + dg_2.GetItem(i, "MSG_SEQ") + "'>";
		}
		
		msgHtml += "&nbsp;&nbsp;" + dg_2.GetItem(i, "MSG_NM") + " (" + _X.ToString(dg_2.GetItem(i, "RDT"), "MM-DD HH:MI:SS") + ")<pre>" + dg_2.GetItem(i, "MSG_CONTENT") + "</pre>" +
		           //(flag ? "<input type='button' class='btn btn-danger btn-xs ml3' value='삭제' onclick='pf_msgDelete(" + msgSeq + ");' style='float: right;'><input type='button' class='btn btn-success btn-xs' value='수정' onclick='pf_msgModify(" + msgSeq + ");' style='float: right;'>" : "") +
		           "</div><div class='detail_line' style='opacity: 0;'></div>";
	}
	
	msgHtml += "</pre>";
	
	$("#MSG_CONTENT").html(msgHtml);
	$("#pre_content").scrollTop(999999999999999999);
}

// 메세지 변경
function pf_msgModify() {
	alert("^^");
}

// 메세지 삭제
function pf_msgModify() {
	alert("!!");
}
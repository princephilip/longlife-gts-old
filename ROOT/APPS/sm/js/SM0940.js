﻿//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 : CM03025|실행공종 등록 -> CM1505|실행공종 등록
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
//   ParkHs					엑스인터넷정보		 20161026
//   ParkHs					엑스인터넷정보		 20161115 메뉴이동
//===========================================================================================
//
//===========================================================================================
var ls_comp = mytop._CompanyCode;
var ls_user = mytop._UserID;
function x_InitForm2(){	
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM0940", "SM0940|SM0940_01_TREE",false,false);	
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
/*		var ls_div = [{"code":"Y","label":"사용"}, {"code":"N","label":"미사용"}, {"code":"P","label":"조건부사용"}];
		dg_1.SetCombo("PM_USE_TAG", ls_div);
		dg_1.SetCombo("CM_USE_TAG", ls_div);
		$("#btn_projfind").bind("click", function(){_X.FindUserProjCode(window,"G_PROJECT","", new Array(ls_comp, ls_user));});*/
		x_DAO_Retrieve2(dg_1);
	}
}

function x_DAO_Retrieve2(a_dg){
	switch(a_dg.id){
		case 'dg_1':
			var param = new Array(mytop._CompanyCode, S_SYS_ID.value);
			dg_1.Retrieve(param);
		break;
	}
}

function xe_EditChanged2(a_obj, a_val){
	if(a_obj.id == 'S_SYS_ID'){
  	x_DAO_Retrieve(dg_1);
	}
}



function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

function x_DAO_Saved2(){

}

function x_DAO_Insert2(a_dg, row){
	return 100;
}

function x_Insert_After2(a_dg, rowIdx){
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){

}

function x_DAO_Delete2(a_dg){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){

}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){

}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){

	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridItemFocusChanged(a_dg, a_newrow, a_newcol){

}

function xe_GridDataLoad2(a_dg){
	dg_1.TreeExpandAll();
}

function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){
}

function x_ReceivedCode2(a_retVal){
	switch(_FindCode.finddiv){
		case "S_SYS_ID":
		  	x_DAO_Retrieve2(dg_1);
			break;
	}
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){

}

function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){

}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){

}

function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){

}

function xe_ChartPointClick2(a_dg, a_event){

}

function xe_ChartPointSelect2(a_dg, a_event){

}

function xe_ChartPointMouseOver2(a_dg){

}

function xe_ChartPointMouseOut2(a_dg, a_event){

}

function x_Close2() {
	return 100;
}


var _IsModal = (window.dialogArguments?true:false);
var _IsPopup = (window.opener?true:false);
var _Caller  = (_IsModal?window.dialogArguments:(_IsPopup?window.opener:window.parent));

var options_dg_1 = {
		panel: {visible: false},
		footer: {visible: false},
		checkBar: {visible: false},
		statusBar: {visible: false},
		select: {style: RealGrids.SelectionStyle.ROWS},
    edit: {
			insertable: false,
			appendable: false,
			updatable: false,
			deletable: false
    }
};

var coEMPLOYEE_DIV       = _X.XmlSelect('hr', 'HR_PERS_MASTER', 'HR030101_R53', new Array(), 'json2'); 

//화면 디자인관련 요소들 초기화 작업
function x_InitForm(){
	
	_X.SetHTML(ubtns, _X.Button("xBtnSave_dg_1", "x_DAO_Save()", "/Theme/images/btn/x_save") + _X.Button("xBtnClose", "x_Close()", "/Theme/images/btn/x_close"));
	
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "hr", "MakeEmployee", "HR_PERS_MASTER|HR030101_C101");
	
	_X.DropDownList_SetData(EMPLOYEE_DIV, coEMPLOYEE_DIV, '11', false, false, null);
	
}

function x_DAO_Retrieve(){
	var param = new Array(EMPLOYEE_DIV.value, '100', EMP_NAME.value);
	dg_1.Retrieve(param);			
}

function x_DAO_Save(){

	var ls_name_res = (EMP_NAME.value == null || EMP_NAME.value == '');
	 
	if(ls_name_res){
		_X.MsgBox('성명을 입력해 주십시오.');
		return;
	}
	var as_employee_div = EMPLOYEE_DIV.value;
	var as_emp_no       = EMP_NO.value;
	var as_company_code = dg_1.GetItem(1, 'COMPANY_CODE');
	var as_join_date    = JOIN_DATE.value;
	var as_emp_name     = EMP_NAME.value;
	
	if(_X.ExecProc("hr", "SP_HR_PERS_CREATE_EMP_NO", new Array(as_employee_div, as_emp_no, as_company_code, as_join_date, as_emp_name)) != -1){
		_X.MsgBox('사원등록이 정상적으로 이루어졌습니다.');
	}else{
		_X.MsgBox('사원등록 처리도중 오류가 발생했습니다.', '다시 등록하여 주십시오.');
	}	
}

function x_DAO_Insert(row){}
function x_DAO_Delete(row){}
function x_DAO_Duplicate(row){}
function x_Duplicate_After(obj, rowIdx){}
function x_DAO_Excel(){dg_1.ExcelExport();}

function xe_GridDataLoad(a_dg){

}

function xe_EditChanged(a_obj, a_val, a_label, a_cobj){
	if(a_obj.id == 'EMPLOYEE_DIV'){
		x_DAO_Retrieve();
	}
}

function xe_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){
	
//	if(typeof(xc_InputKeyDown)!="undefined"){xc_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey);return;}
//
//	if(a_obj==FIND_CODE && a_keyCode==KEY_DOWN)
//		$('#dg_1').focus();
}

//입력 컨트롤에서 Enter키가 눌러진 경우 호출
function xe_InputKeyEnter(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	
//	switch(a_obj.id) {
//		case "FIND_CODE":
//			x_DAO_Retrieve();
//			break;
//	}
}

function xe_GridRowFocusChange(a_dg, a_row, a_col){}
function xe_GridDoubleClick(a_dg, a_ctrlKey, a_altKey, a_shiftKey, a_stageX, a_stageY){alert('9');}
function xe_GridItemDoubleClick(a_dg, a_row, a_col){
	//if(typeof(xc_GridItemDoubleClick)!="undefined"){xc_GridItemDoubleClick(a_dg, a_row, a_col);return;}
	//x_ReturnRowData(a_dg, a_row);
}

function xe_GridButtonClick(a_dg, a_row, a_col, a_colname){
	//if(a_dg.id=="dg_1" && a_colname == "SELECT_RETURN") {
	//	x_ReturnRowData(a_dg, a_row);
	//}
}

function x_ReturnRowData(a_dg, a_row){

//	if(typeof(xc_ReturnRowData)!="undefined"){xc_ReturnRowData(a_dg, a_row);return;}
//	var rtValue = a_dg.GetRowData(a_row);
//	//var rtValue = a_dg.GetItem(a_row,);
//	
//	if(_IsModal) {
//		window.returnValue = rtValue;
//	} else {
//		if(_Caller) {			
//			_Caller._FindCode.returnValue = rtValue;
//			_Caller.x_ReceivedCode(rtValue);
//			//_Caller._X.UnBlock();
//		}
//	}
//	setTimeout('x_Close()',0);
}

function x_Confirm(){
//	var rows = dg_1.GetCheckedRows();
//	if (dg_1.GetRow()==0) return;
//
//	for(var i=0; i<rows.length; i++)
//	{
//		DRow = parseInt(rows[i] + 1);
//		IRow = _Caller.dg_1.InsertRow(0);
//		_Caller.dg_1.SetItem(IRow,"SYS_ID",dg_1.GetItem(DRow,'SYS_ID'));
//		_Caller.dg_1.SetItem(IRow,"PGM_CODE",dg_1.GetItem(DRow,'PGM_CODE'));
//		_Caller.dg_1.SetItem(IRow,"PGM_NAME",dg_1.GetItem(DRow,'PGM_NAME'));
//		_Caller.dg_1.SetItem(IRow,"AUTH_CODE",_Caller._FindCode.findcode[0]);
//		alert(_Caller._FindCode.findcode[0]);
//		_Caller.dg_1.SetItem(IRow,"AUTH_TAG","U");
//		_Caller.dg_1.SetItem(IRow,"AUTH_I","Y");
//		_Caller.dg_1.SetItem(IRow,"AUTH_R","Y");
//		_Caller.dg_1.SetItem(IRow,"AUTH_D","Y");
//		_Caller.dg_1.SetItem(IRow,"AUTH_P","Y");
//  	}
//	_Caller.x_DAO_Save();
//	x_ReturnRowData(dg_1, dg_1.GetRow());
}

function x_Close() {

	if(_IsModal || _IsPopup) {
		window.close();
	} else {
		if(_Caller.$("#findmodal").length>0?_Caller.$("#findmodal").dialog("close"):window.close());
	}
}

function xe_GridLayoutComplete(a_dg){

	if(a_dg != null && a_dg.id == 'dg_1') x_DAO_Retrieve();
}

//==================================================================================
//사용자함수
//==================================================================================

//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 : [SM0915.js] 사용자별 현장등록
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
//   Parkhs					엑스인터넷정보		 20161107
//   ParkHs					엑스인터넷정보		 20161115 메뉴이동
//===========================================================================================

function x_InitForm2(){
	_X.InitGrid( {grid:dg_1, sqlId: "SM0915_01", shared:false, updatable:false} );
	_X.InitGrid( {grid:dg_2, sqlId: "SM0915_02", shared:false, updatable:true} );
	//_X.InitGrid( {grid:dg_3, sqlId: "SM0915_03", shared:false, updatable:false} );
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		var	comboData   = _X.CommXmlSelect('SM','USER_TAG');	//_X.XmlSelect("com", "COMMON", "W_COMMON_01", new Array('SM','USER_TAG'), "Array");
		_X.DDLB_SetData(S_USER_TAG, comboData, null, false, true);
		dg_1.SetCombo("USER_TAG", comboData);
		x_DAO_Retrieve2(dg_1);
	}
}

function x_DAO_Retrieve2(a_dg){
	switch(a_dg.id){
		case 'dg_1':
	 		dg_2.Reset();
			dg_1.Retrieve( [mytop._CompanyCode, S_FIND.value, S_USE_YESNO.value, S_USER_TAG.value, S_TYPE.value] );

		break;
		case 'dg_2':
			dg_2.Retrieve( [mytop._CompanyCode, dg_1.GetItem(dg_1.GetRow(), 'USER_ID'), S_SYS_ID.value, '%'] );
			//dg_2.TreeCollapseLevel(3);
		break;
	}
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id){
		case "S_USE_YESNO":
		case "S_USER_TAG":
		case "S_TYPE":
			x_DAO_Retrieve(dg_1);
			break;
		case "S_SYS_ID":
			x_DAO_Retrieve(dg_2);
			break;
	}
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	var ls_result;
	switch(a_obj.id) {
		case "S_FIND":
		  	x_DAO_Retrieve2(dg_1);
			break;
	}
}
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	//var cData1	= dg_2.GetChangedData();
	//INSERT 데이타 테그 변경
	// var nrows = [];
	// for (i=0 ; i < cData1.length; i++){
	// 	if(cData1[i].job == "U" && cData1[i].data.NEW_MODE == "Y") {
	// 		//cData1[i].job = "I";
	// 		nrows.push(cData1[i].idx);
	// 		dg_2.SetItem(cData1[i].idx, "NEW_MODE", "N");
	// 	}
	// }
	// dg_2.SetRowsState(nrows, 'I');
	return true;
}

function x_DAO_Saved2(){

}

function x_DAO_Insert2(a_dg, row){

	return 100;
}

function x_Insert_After2(a_dg, rowIdx){
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){

}

function x_DAO_Delete2(a_dg){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){

}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	if(a_dg.id == "dg_2" && a_col.substr(0,4)=="AUTH") {

		if(a_dg.GetItem(a_row, 'NEW_MODE') == "Y") {
			a_dg.GetRowData(a_row).job = 'I';
			a_dg.SetItem(a_row, 'NEW_MODE', "N");
		}
		a_dg.TreeExpand(a_row,true);
		if(a_row<a_dg.RowCount() && a_dg.GetItem(a_row,'LEV') < a_dg.GetItem(a_row+1,'LEV')) {
			var crows = a_dg.GetDescendants(a_row);
			$(crows).each(function(inx,val){
				if(val) {
					if(a_dg.GetItem(val, 'NEW_MODE') == "Y") {
						a_dg.GetRowData(val).job = 'I';
						a_dg.SetItem(val, 'NEW_MODE', "N");
					}
					a_dg.SetItem(val,a_col,a_newvalue);
				}
			});
		}
	}
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	if(a_dg.id == 'dg_1'){
		if(dg_2.IsDataChanged()){
			if(_X.MsgBoxYesNo('확인','변경된 데이타가 존재합니다.\n계속 진행하시겠습니까?')=='2'){
				return false;
			}
		}
	}
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
	if(a_dg.id == 'dg_1'){
		x_DAO_Retrieve2(dg_2);
	}
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridItemFocusChanged(a_dg, a_newrow, a_newcol){

}

function xe_GridDataLoad2(a_dg){

}

function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){
}

function x_ReceivedCode2(a_retVal){
	switch(_FindCode.finddiv){
		case "USER_CODE":
			//사용자권한복사
			pf_UserCopy(_FindCode.returnValue.USER_ID, _FindCode.returnValue.USER_NAME);
			break;
	}
	return 0;
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){

}

function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){

}

function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){

}

function xe_ChartPointClick2(a_dg, a_event){

}

function xe_ChartPointSelect2(a_dg, a_event){

}

function xe_ChartPointMouseOver2(a_dg){

}

function xe_ChartPointMouseOut2(a_dg, a_event){

}

function x_Close2() {
	return 100;
}


//사용자 코드찾기
function pf_UserFind() {
	var a_row = dg_1.GetRow();
	if(a_row<=0)
		return;
	var userId = dg_1.GetItem(a_row,"USER_ID");

	_X.CommonFindCode(window,"사용자찾기","sm","USER_CODE", "", new Array(mytop._CompanyCode, userId),"FindUserGroupCode|SmFindCode|FindSmAuthGroup",333, 550);
};

//프로그램 권한 복사
function pf_UserCopy(userId, userName) {
	var a_row = dg_1.GetRow();
	if(a_row<=0) return;
	var t_userId = dg_1.GetItem(a_row,"USER_ID"); //부모의 아이디 (복사대상자)
	var t_userName = dg_1.GetItem(a_row,"USER_NAME"); //팝업창 사용자이름
	var s_userId = userId;//팝업창의 사용자아이디 (복사자료를 갖고있는 사람)
	var s_userName = userName;

	if(_X.MsgBoxYesNo("["+s_userName + "] 의 권한을\r\n[" + t_userName + "] 으로 복사 하시겠습니까?","",2)!=1) return;

	//권한설정 초기화 Procedure 실행
	li_rc = _X.ExecProc("sm", "SP_SM_USER_AUTH_COPY", new Array(mytop._CompanyCode, s_userId, t_userId));
	if(li_rc != -1) {
		x_DAO_Retrieve2(dg_2);
		_X.Noty("프로그램 사용권한 복사 완료");
	}
}

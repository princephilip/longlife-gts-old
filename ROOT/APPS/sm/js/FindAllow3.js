var _IsModal = (window.dialogArguments?true:false);
var _IsPopup = (window.opener?true:false);
var _Caller  = (_IsModal?window.dialogArguments:(_IsPopup?window.opener:window.parent));

var options_dg_1 = {
		panel: {visible: false},
		footer: {visible: false},
		checkBar: {visible: false},
		statusBar: {visible: false},
		select: {style: RealGrids.SelectionStyle.ROWS},
    edit: {
			insertable: false,
			appendable: false,
			updatable: false,
			deletable: false
    }
};

var coALLOW_CODE2 = _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_CODE_ALLOW', new Array('%', '1'), 'json2');
var coDEDUCT_CODE2 = _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_CODE_ALLOW', new Array('%', '2'), 'json2');
var coS_PAY_KIND2 = _X.XmlSelect('hr', 'HR_COMCODE', 'HR_COMCODE_R100', new Array('HR', 'PAY_KIND'), 'json2');

//화면 디자인관련 요소들 초기화 작업
function x_InitForm(){
	
	_X.SetHTML(ubtns, _X.Button("xBtnSave_dg_1", "x_DAO_Save()", "/Theme/images/btn/x_save") + _X.Button("xBtnClose", "x_Close()", "/Theme/images/btn/x_close"));
		
	_X.DropDownList_SetData(S_ALLOW_CODE2, coALLOW_CODE2, '01', false, false, null);
	_X.DropDownList_SetData(S_PAY_KIND2, coS_PAY_KIND2, null, false, false, null);
	
	S_BASIC_AMT2.value = 0;
}

function x_DAO_Retrieve(){

}

//xe_GridRowFocusChange가 안되서...추가
function xe_M_RealGridRowFocusChange(a_obj, oldIndex, newIndex){

}

function xe_GridRowFocusChange(a_dg, a_newrow, a_oldrow){//alert("myAlert((xe_GridRowFocusChange)");

}

function x_DAO_Save(){
	
	var ls_work_yymm = S_WORK_YYMM2.value;
	var li_basic_amt = S_BASIC_AMT2.value;
	
	if(ls_work_yymm==null||_X.Trim(ls_work_yymm)==""){ 
		_X.MsgBox('[지급년월] 항목은 필수입력 항목입니다.');
		return;
	}

	if(gf_chkDate2(ls_work_yymm, '지급년월')) return;

	var ls_msg_yn = 'Y';
	
	if(ls_work_yymm < _X.ToString(new Date(), 'YYYYMM')){
		if(_X.MsgBoxYesNo('확인', '현재 월보다 작은 월이 입력 되었습니다.<br>일괄등록 하시겠습니까?') == 1) ls_msg_yn = 'N';
		else return;
	}
	
	if(li_basic_amt == null || li_basic_amt == '' || li_basic_amt == 0){
		if(_X.MsgBoxYesNo('확인', '금액이 지정되지 않았거나 금액이 0원입니다.<br>일괄등록 하시겠습니까?') == 1) ls_msg_yn = 'N';
		else return;
	}
	
	var rtValue = {'ALLOW_CODE':S_ALLOW_CODE2.value, 'WORK_YYMM':S_WORK_YYMM2.value, 'PAY_KIND':S_PAY_KIND2.value, 'BASIC_AMT':S_BASIC_AMT2.value, 'LB_MSG_YN': ls_msg_yn};
	_Caller._FindCode.returnValue = rtValue;
	_Caller.x_ReceivedCode(rtValue);
	setTimeout('x_Close()',0);
}

function gf_chkDate2(as_date, as_msg){
	
	var lb_result=false;

	if (as_date.length!=6) lb_result=true;
	else if (_X.ToInt(as_date.substr(0,4))<1 || _X.ToInt(as_date.substr(0,4)) > 9999) 	lb_result=true;
	else if (_X.ToInt(as_date.substr(4,2))<1 || _X.ToInt(as_date.substr(4,2)) > 12) 	lb_result=true;

	if (lb_result && as_msg!=null) _X.MsgBox("날짜 유효성 체크", "[" + as_msg + "](" + as_date + ")가 유효하지 않습니다.");
	
	return lb_result;
}
	
function x_DAO_Insert(row){}
function x_DAO_Delete(row){}
function x_DAO_Duplicate(row){}
function x_Duplicate_After(obj, rowIdx){}
function x_DAO_Excel(){dg_1.ExcelExport();}
function xe_GridRowFocusChange(a_dg, a_row, a_col){}
function xe_GridDoubleClick(a_dg, a_ctrlKey, a_altKey, a_shiftKey, a_stageX, a_stageY){alert('9');}

function xe_GridDataLoad(a_dg){

}

function xe_EditChanged(a_obj, a_val, a_label, a_cobj){
	
	if(a_obj.id == 'S_PAY_DIV' || a_obj.id == 'S_PAY_DIV2'){
		
		$('#S_ALLOW_CODE2').selectBox('destroy');
		
		if(a_val == '1') {
			S_ALLOW_NAME.innerHTML = '수당명';
			_X.DropDownList_SetData(S_ALLOW_CODE2, coALLOW_CODE2, '01', false, false, null);
		}else{
			S_ALLOW_NAME.innerHTML = '공제명';
			 _X.DropDownList_SetData(S_ALLOW_CODE2, coDEDUCT_CODE2, '50', false, false, null);
		}
		
		$('#S_ALLOW_CODE2').selectBox();
	}
}

function xe_InputKeyDown(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){
	
}

//xe_InputKeyEnter가 안되서...추가
function xm_InputKeyDown(){
	
	var keyCode = event.keyCode;
	
	if((keyCode != 27 && keyCode != 8) && (keyCode < 48 || keyCode > 57) && (keyCode < 96 || keyCode > 105)) event.returnValue = false;
	
	if(this.id == 'S_BASIC_AMT2' && S_BASIC_AMT2.value == 0) S_BASIC_AMT2.value = '';
	//if(this.id == 'S_BASIC_AMT2' && keyCode == KEY_ENTER) S_BASIC_AMT2.value = _X.FormatComma(S_BASIC_AMT2.value);
}

//입력 컨트롤에서 Enter키가 눌러진 경우 호출
function xe_InputKeyEnter(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){

}

//xe_GridItemDoubleClick가 안되서...추가
function xe_M_RealGridItemDoubleClick(a_obj, cellIndex){

}

function xe_GridItemDoubleClick(a_dg, a_row, a_col){

}

function xe_GridButtonClick(a_dg, a_row, a_col, a_colname){

}

function x_ReturnRowData(a_dg, a_row){

	if(typeof(xc_ReturnRowData)!="undefined"){xc_ReturnRowData(a_dg, a_row);return;}
	var rtValue = a_dg.GetRowData(a_row);
	//var rtValue = a_dg.GetItem(a_row,);

	if(_IsModal) {
		window.returnValue = rtValue;
	} else {
		if(_Caller) {			
			_Caller._FindCode.returnValue = rtValue;
			_Caller.x_ReceivedCode(rtValue);
			//_Caller._X.UnBlock();
		}
	}
	setTimeout('x_Close()',0);
}

function x_Confirm(){
	if (dg_2.GetRow()==0) return;
	x_ReturnRowData(dg_2, dg_2.GetRow());

}

function x_Close() {
	if(_IsModal || _IsPopup) {
		window.close();
	} else {
		if(_Caller.$("#findmodal").length>0?_Caller.$("#findmodal").dialog("close"):window.close());
	}
}

function xe_GridLayoutComplete(a_dg){

}
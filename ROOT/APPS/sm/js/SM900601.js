var ed_gridsql;
var mime = 'text/x-mariadb'; 
var strExecSql = "";
var qryTables; // 쿼리 테이블 목록
var qryColumns = []; // 쿼리 칼럼 목록
var qryIds = []; // 쿼리 Id 목록
var strPgmCode = "";  // 작업중인 프로그램코드
var urlXml = new Object; // 쿼리 파일 URL OBJECT {"dg_1" : {"filename":"SM900601", "qryid":"SM900601_GRID"}}
var updTable = ""; // Update Table
var updCols = []; // Update Columns
var updPars = []; // Update Parameters
var isSqlChanged = true; // SQL변경 자동 이벤트 막기

function x_InitForm2(){
  _X.InitGrid(grid_devprop, "dg_devprop", "100%", "100%", "sm", "SM900601", "SM900601|SM900601_GRID", true, true);
  _X.InitGrid(grid_devpropsrc, "dg_devpropsrc", "100%", "100%", "sm", "SM900601", "SM900601|SM900601_GRIDSRC", true, true);
  
  return 100;
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
  if(ab_allcompleted) {
    // var comTF = [ {code: "true", label: "true"}, {code: "false", label: "false"} ];
    // var comYN = [ {code: "Y", label: "Y"}, {code: "N", label: "N"} ];

    // dg_devprop.SetCombo("MUST_INPUT" , comYN );
    // dg_devprop.SetCombo("VISIBLE"    , comTF );
    // dg_devprop.SetCombo("READONLY"   , comTF );
    // dg_devprop.SetCombo("EDITABLE"   , comTF );
    // dg_devprop.SetCombo("SORTABLE"   , comTF );
  }

  return 100;
}

function x_DAO_Retrieve2(a_dg){
  var tGridId = getWorkingGridId(); 

}

$(function() {
  _X.UnBlock();
  $("#wrap-grid-id").hide(); // 신규 Grid Id 입력 숨기기

  // (sql - highlight)
  // get mime type 
  if (window.location.href.indexOf('mime=') > -1) {
    mime = window.location.href.substr(window.location.href.indexOf('mime=') + 5);
  }

  // Grid_Id 수정될 때
  $("#grid-objs").change(function(e) {
    changedGridObj();
  });

  setEditor( "grid-sql" );
  $("#pageLayout").css({"overflow-y": "auto"});

  // CODE GENERATE & SAVE 버튼 
  $("#beta-go").click(function() {
    generateCode();
  });

  // 클립보드 복사버튼
  $(".row-4 .btn-warning").click(function() {
    var tId = $(this).attr("id");
    tId = tId.replace("copybtn-", "");
    copyToClipboard( $("#"+tId).val() );
  });

  // 수정SQL 적용 버튼
  $("#btn-modified-sql").click(function() {
    if( !$(this).hasClass("disabled") ) exeSql();
  });

  // 체크박스 이벤트
  $("#check-new-grid").on("change", function() {
    if($(this).is(':checked')) {
      // 신규
      $("#wrap-grid-ids").hide();
      $("#wrap-grid-id").show();

      ed_gridsql.setValue("");
      generateCode("refresh");
      $("#grid-cols-setting").empty();
      setGridColsHeader();
      dg_devprop.Reset();
      dg_devpropsrc.Reset();
    } else {
      $("#wrap-grid-ids").show();
      $("#wrap-grid-id").hide();

      $("a[data-gridid='"+$("#grid-objs").val()+"']").trigger("click");
    }
  });

  // Modal 오픈시 refresh
  $('#sqlModal').on('shown.bs.modal', function() { 
    ed_gridsql.refresh();
  });

  // Grid Cols Properties 헤더 넣기..
  setGridColsHeader();

  // 프로그램코드 검색 활성화
  init_pgmsearch();
});

// Grid Obj 선택 바뀌었을 때, 
var changedGridObj = function() {
  setGridColsInfo();
  generateCode('refresh');
  dg_devprop.Reset();
  x_DAO_Retrieve();
}

// editor Highliter 설정
var setEditor = function(aid) { 
  var editor = CodeMirror.fromTextArea(document.getElementById(aid), {
    mode: mime,
    indentWithTabs: true,
    smartIndent: true,
    lineNumbers: true,
    matchBrackets : true,
    autofocus: false,
    extraKeys: {"Ctrl-Space": "autocomplete"},
    hintOptions: {tables: {
      users: {name: null, score: null, birthDate: null},
      countries: {name: null, population: null, size: null}
    }}
  });

  eval("ed_"+aid.replace(/\-/g,"")+" = editor");

  // editor.on("blur", function(){
  //   resizePanelHeight("CLOSE");
  // });
  // editor.on("focus", function(){
  //   resizePanelHeight("OPEN");
  // });
  editor.on("change", function(){
    changedQuery();
  });
}

// 쿼리 수정됐을 때, 
var changedQuery = function() {
  if(isSqlChanged===false) {
    isSqlChanged = true;
    return;
  }

  // SQL 실행버튼 활성화
  setButtonActive();

  // sql에서 Parameter 추출
  var tSql = ed_gridsql.getValue();
  parsingParams(tSql);
}

// : 로 시작하는 Params 를 ##으로 변환
var convertParams = function(aSql) { 
  var strqry = aSql;
  var patt = /:(\w+)($|\s|\n|=|\)|,)/g;

  if(strqry!="") {
    strqry = strqry.replace(patt, "#$1#$2");  
  }

  // isSqlChanged = false;
  // ed_gridsql.setValue(strqry);

  return strqry;
}

// sql에서 parameter 추출
var parsingParams = function(aVal) {
  var cSql = convertParams(aVal);

  var patt = /#(\w+)#/g;
  //var patt = /:(\w+)/g; // [$|\s|\n|=|\)|,]
  var tempParams = new Array();
  var arrparams = new Array();
  var strParams = "";

  if(cSql!="") {
    // parsing parameter 
    if(patt.test(cSql)) {
      tempParams = cSql.match(patt);
    }

    if(tempParams!=null && tempParams.length>0) {
      tempParams = _.uniq(tempParams);
      $.each(tempParams, function(i, val) {
        var tPname = val.replace(/#/g, "");
        
        var tObj = new Object;
        tObj.name = tPname;
        tObj.type = "String";
        var tName = tObj.name.substring(0, 2);
        if(tName=="ai"||tName=="an") tObj.type = "Integer";
        arrparams.push(tObj);
      
      });
    }

    setParams(arrparams);
  }
}


// Grid 코드 생성
var setGridCode = function(aCopy) {
  var tcode = "";
  var tcols = $(".cols-field");
  var tCp = colsproperties;
  var tMaxLen1 = 0;
  var tMaxLen2 = 0;
  var tMaxLen3 = 0;
  var tMaxLen4 = 0;

  // columns_dg_1 - generate
  tcode += "var "+"columns_"+getWorkingGridId()+" = [\n";
  var tT = "";
  var tV = "";
  var tLen = 0;

  for(var i=1; i<=tcols.length; i++) {
    tcode += "  {";
    for(var j=0; j<tCp.length; j++) {
      if(tCp[j].isprop!==false) {
        tT = tCp[j].title;
        tV = getObjectVal("#"+tCp[j].title+"_"+i).trim();
        tLen = 0;

        // tCp[j].isoption=="true" 이면서 값이 없으면 패스
        if(!tCp[j].isoption||tV!="") {
          // field 이름에 따른 설정
          if(tT=="fieldName") {
            if(tMaxLen1<tV.length) tMaxLen1 = tV.length;
            tV = "'"+tV+"' " + (tV.length>0 ? "#INDENT_BLANK1_"+tV.length+"#" : "");
          } else if(tT=="width") {
            tV = (tV.length<5?"#BLANK_"+(3-tV.length)+"#":"") + (tV==""?"0":tV);
          } else if(tT=="header") {
            tLen = getTextLength(tV);
            if(tMaxLen2<tLen) tMaxLen2 = tLen;
            tV = "{text: '"+tV+"'}" + (tLen>0 ? " #INDENT_BLANK2_"+tLen+"#" : "");
          } else if(tT=="styles") {
            // field 값에 따른 설정
            if(tMaxLen3<tV.length) tMaxLen3 = tV.length;
            tV = tV+(tV.length>0 ? " #INDENT_BLANK3_"+tV.length+"#" : "");
          } else if(tT=="editor") {
            // field 값에 따른 설정
            if(tMaxLen4<tV.length) tMaxLen4 = tV.length;
            tV = tV+(tV.length>0 ? " #INDENT_BLANK4_"+tV.length+"#" : "");
          } else {
            // field 값에 따른 설정
            switch(tV) {
              case "true" :
              case "false" :
                tV = tV+(tV=="true"?" ":"");
                break;
              default :
                tV = "'"+tV+"'";
            }
          }
          tcode += tT+": "+tV+(j==tCp.length?" ":", ");
        }
      }
    }
    tcode += "#END_PROPERTIES# }"+(i==tcols.length?"":",")+" \n";
  }

  tcode = tcode.replace(/, #END_PROPERTIES#/g, "");

  // indent 처리
  var tBlank = "";
  for(var i=0; i<6; i++) {
    var patt = new RegExp("#BLANK_"+i+"#", "g");
    tcode = tcode.replace(patt, tBlank);
    tBlank += " ";
  }

  var tBlank = "";
  for(var i=tMaxLen1; i>=1; i--) {
    var patt = new RegExp("#INDENT_BLANK1_"+i+"#", "g");
    tcode = tcode.replace(patt, tBlank);
    tBlank += " ";
  }

  var tBlank = "";
  for(var i=tMaxLen2; i>=1; i--) {
    var patt = new RegExp("#INDENT_BLANK2_"+i+"#", "g");
    tcode = tcode.replace(patt, tBlank);
    tBlank += " ";
  }

  var tBlank = "";
  for(var i=tMaxLen3; i>=1; i--) {
    var patt = new RegExp("#INDENT_BLANK3_"+i+"#", "g");
    tcode = tcode.replace(patt, tBlank);
    tBlank += " ";
  }

  var tBlank = "";
  for(var i=tMaxLen4; i>=1; i--) {
    var patt = new RegExp("#INDENT_BLANK4_"+i+"#", "g");
    tcode = tcode.replace(patt, tBlank);
    tBlank += " ";
  }
  tcode += "];\n\n";

  if(aCopy===true) copyToClipboard(tcode);

  $("#code-grid").val(tcode);
}

// 한글포함문자열 Length
var getTextLength = function(str) {
  var len = 0;
  for (var i = 0; i < str.length; i++) {
    if (escape(str.charAt(i)).length == 6) {
      len++;
    }
    len++;
  }
  return len;
}

// 클립보드 복사
var copyToClipboard = function(str) {
  var $temp = $("<textarea>");
  $("body").append($temp);
  $temp.val(str).select();
  document.execCommand("copy");
  $temp.remove();
}

// 프로그램코드 찾기 - 검색로직
var substringMatcher = function(strs) {
  return function findMatches(q, cb) {
    var matches, substringRegex;

    // an array that will be populated with substring matches
    matches = [];

    // regex used to determine if a string contains the substring `q`
    substrRegex = new RegExp(q, 'i');

    // iterate through the pool of strings and for any string that
    // contains the substring `q`, add it to the `matches` array
    $.each(strs, function(i, str) {
      if ((substrRegex.test(str.PGM_NAME)||substrRegex.test(str.PGM_CODE)) && str.CHILD_MENU_CNT=="0") {
        matches.push(str);
      }
    });

    cb(matches);
  };
};

// 프로그램코드 찾기 - 초기화
var init_pgmsearch = function() {
  var tPgmlist;
  if(top==window) {
    tPgmlist = _X.XmlSelect("sm", "SM_AUTH_PGMCODE", "R_EGOVMAIN_01", new Array(_tCompanyCode,_tUserID), "json", "PGM_CODE");
  } else {
    tPgmlist = top._MyPgmList;
  }

  $('#search-pgmcode .typeahead').typeahead({
    hint: true,
    highlight: true,
    minLength: 1.,
    limit: 10,
  },
  {
    name: 'pgms',
    displayKey: 'PGM_CODE',
    source: substringMatcher(tPgmlist),
    templates: {
      empty: '<div class="empty-message">일치하는 목록이 없습니다.</div>',
      suggestion: Handlebars.compile('<div><strong>{{PGM_NAME}}</strong> <small>[{{PGM_CODE}}]</small></div>')
    }
  }).on('typeahead:selected', function($e, datum){
    // $(this).val(datum.PGM_CODE);
    // _X.OpenSheet(datum.PGM_CODE);
    var url_grid = "/APPS/"+datum.PGM_CODE.substring(0, 2).toLowerCase()+"/grid/"+datum.PGM_CODE+".Grid.js";
    var url_js = "/APPS/"+datum.PGM_CODE.substring(0, 2).toLowerCase()+"/js/"+datum.PGM_CODE+".js";

    strPgmCode = datum.PGM_CODE;
    generateCode('refresh');
    dg_devprop.Reset();
    $("#beta-go").removeClass("disabled");
    $("#tab-pgm-name").html("프로그램 코드 - "+"["+datum.PGM_CODE+"] "+datum.PGM_NAME);

    // pgmcode.js 분석
    $.ajax({
      url: url_js,
      dataType: "text",
      success: function(data) {
        $("#js-txt").val(data);

        var patt = /InitGrid\((.*)\)/gi;
        var cObjs = data.match(patt);
        urlXml = new Object;
        var tGridId = "";
        var tQryStr = "";
        var tQryFile = "";
        var tQryId = "";

        $.each(cObjs, function(idx, val) {
          val = val.replace(/InitGrid/, "");
          val = val.replace("(", "");
          val = val.replace(")", "");
          val = val.split(",");
          tGridId = val[1].replace(/\s/, ""); tGridId = tGridId.replace(/("|')/g, '');
          tQryStr = val[6].replace(/\s/, ""); tQryStr = tQryStr.replace(/("|')/g, '');
          tQryFile = tQryStr.split("|")[0];
          tQryId = tQryStr.split("|")[1];
          
          eval("urlXml."+tGridId+" = {qryFile: '"+tQryFile+"', qryId: '"+tQryId+"'}; ");
          // console.log(urlXml);
        });

        // Grid.js 파일 분석
        getGridJs(url_grid);

        // /(?:from|into|update|join)(?:\s+|\n+)(\w+)(?:\s+|\n+|$)/gi;
      },
      error: function() {
        // 파일 없을 때, 쿼리 창 열기
        alert(url_js+' 파일을 찾을 수 없습니다.')
      }
    });

  });

  $('#search-pgmcode .typeahead').focus(function () {
    $(this).select();
  });

}

// pgmcode.GRID.js 분석
var getGridJs = function(aUrl) {
  // pgmcode.GRID.js 분석
  $.ajax({
    url: aUrl,
    dataType: "script",
    success: function(data) {
      $("#grid-txt").val( data );

      // 그리드 파일 정보 가져오기
      getGridObject(data);

      eval(data);

      //alert(columns_dg_1[0].fieldName);
    },
    error: function() {
      // 파일 없을 때, 쿼리 창 열기
      alert(url_grid+' 파일을 찾을 수 없습니다.')
    }
  });
}

// xmlx파일 읽기 
var getXmlQuery = function(aDg) {
  var tObj = $("#grid-objs option:selected").val();
  if(aDg) tObj = aDg;

  if(!tObj) {
    alert("그리드파일을 읽지 못했습니다. 프로그램 코드를 다시 선택하세요.");
    return;
  }

  tObj = tObj.replace("columns_", "");

  eval("var url_xml = '/APPS/sm/jsp/SM900601_xml.jsp';");
  eval("var xml_sys = urlXml."+tObj+".qryFile.substring(0, 2).toLowerCase();");
  eval("var xml_file = urlXml."+tObj+".qryFile;");
  eval("var xml_id = urlXml."+tObj+".qryId;");

  $("#select-id").val( xml_id );

  // pgmcode.xmlx 파일 분석
  $.ajax({
    type: "POST",
    url: url_xml,
    dataType: "text",
    data: { sys_id: xml_sys, fname: xml_file, qid: xml_id },
    success: function(xml) {
      xml = xml.replace('<?xml version="1.0" encoding="UTF-8"?>', '');
      xml = xml.replace('<!DOCTYPE querylist SYSTEM "../../../Mighty/Xml/query.dtd">', '');

      xmlDoc = $.parseXML( xml );
      tXml = $( xmlDoc );
      tSql = $(tXml).find( "select#"+xml_id+" sql" ).eq(0).text();
      isSqlChanged = false;
      ed_gridsql.setValue(tSql);
      setButtonActive("disabled");

      //UPDATE TABLE & COLUMNS 정보
      updTable = $(tXml).find( "select#"+xml_id+" update_table" ).text();
      tCols = $(tXml).find( "select#"+xml_id+" column" );
      tParams = $(tXml).find( "select#"+xml_id+" param" );
      updCols = new Array();
      updPars = new Array();
      var tIdx = 0;

      $.each(tCols, function(idx, val) {
        tIdx = idx+1;
        // if($(val).attr("name")==$("#fieldName_"+tIdx).val()) {
        //   $("#update_type_"+tIdx).val( $(val).attr("type") );
        //   $("#updatable_"+tIdx).val( $(val).attr("updatable") );
        //   $("#update_iskey_"+tIdx).val( $(val).attr("iskeycol") );
        // }
        // console.log( $(val).attr("name") )
        updCols[idx] = { name: $(val).attr("name"), type: $(val).attr("type"), updatable: $(val).attr("updatable"), iskeycol: $(val).attr("iskeycol") };
      });

      // update column 정보 세팅 
      setGridUpdateInfo();

      $.each(tParams, function(idx, val) {
        updPars[idx] = { name: $(val).attr("name"), type: $(val).attr("type") };
      });

      setColorColsprop();
      getUpdateTables(tSql, updTable);
      setParams(updPars);
      //console.log(updCols);
    }
  });
}

// 프로그램 코드 선택 후, 그리드 파일 정보 가져오기
var getGridObject = function(aTxt) {
  var patt = /(?:columns_)(\w+)/gi;
  var cObjs = aTxt.match(patt);
  $("#grid-objs").empty();
  var oCnt = 0;
  var grid_names = "";

  // 그리드 선텍 list 생성
  $.each(cObjs, function(idx, val) {
    oCnt++;
    $("#grid-objs").append( "<option value=\""+cObjs[idx]+"\" >"+cObjs[idx].replace("columns_", "")+"</option>" );
    grid_names += (grid_names=="" ? "" : " , ") + "<a href=\"#\" data-gridid=\""+cObjs[idx]+"\">"+cObjs[idx].replace("columns_", "")+"</a>";
  });
  
  changedGridObj();

  // 그리드 선택 링크 생성
  $("#grid-ids").html("( "+grid_names+" )");
  $("#grid-ids a").click(function () {
    var gid = $(this).attr("data-gridid");
    $("#grid-objs").val(gid);

    changedGridObj();
  });

  // // 그리드 프로퍼티 세팅
  // if(oCnt>0) {
  //   setGridColsInfo(cObjs[0]);
  //   x_DAO_Retrieve();
  // }
  // console.log(cObjs);
}

//  grid Properties Setting 헤더 넣기
var setGridColsHeader = function() {
  var thtml = "";
  var ttitle = "";
  // header 넣기
  thtml += "<tr>"
  for(var j in colsproperties) {
    var tr = colsproperties[j];
    if(tr.title=="fieldName") thtml += "<th class=\"text-center\">No.</th>";
    ttitle = tr.title;
    if(ttitle.indexOf("_")>=0) ttitle = ttitle.replace("_","<br/>");
    else if(ttitle=="lookupDisplay") ttitle = "lookup<br/>Display";
    else if(ttitle=="alwaysShowButton") ttitle = "always<br/>ShowButton";
    thtml += "<th class=\"text-center "+(tr.className!=""?tr.className+" ":"")+(tr.width!=""?"w"+tr.width:"")+"\">"+ttitle+"</td>";
  }
  thtml += "</tr>";
    
  $("#grid-cols-setting").append(thtml);
}

// grid Properties Setting (aVal 은 그리드 id, aObj 는 칼럼 Object)
// aVal만 있으면 Grid.js 파일의 내용을 읽고, aObj가 있으면 DB에서 쿼리 실행 후, 오는 것으로 간주..
var setGridColsInfo = function(aVal, aObj) {
  var cols = $("#grid-cols").val();
  var thtml = "";
  var rId = 0;
  var tSrc = "script"; /* script:프로그램 스크립트에서 읽어오기, sql:쿼리에서 칼럼정보 가져오기 */

  $("#grid-cols-setting").empty();
  
  setGridColsHeader();

  var cObj = $("#grid-objs").val();
  if(aVal) cObj = aVal;

  var tObj; // 그리크 칼럼 Object
  if(!aObj) { // Grid.js 파일과 xmlx 파일에서 내용 읽어오기
    tSrc = "script";
    tObj = eval(cObj); // Grid.js 의 객체 직접 읽음.
  } else { // DB실행한 결과 object에서 내용 읽어오기
    tSrc = "sql";
    tObj = aObj;
  }
  
  var tRow = 0;
  var tIsCol = false; // Update 정보와 칼럼 시작을 나눔. true 일때 칼럼영역 false 일때 업데이트영역
  $.each(tObj, function(idx, val) {
    rId = (parseInt(idx)+1);
    thtml += "<tr class=\"tr-row-"+rId+" "+(tSrc=="sql" ? "notmatched" : "matched")+"\">"

    for(var j in colsproperties) {
      var tr = colsproperties[j]; // 칼럼 프로퍼티 정보 소스 제일 하단에 변수 설정돼 있습니다. 
      var vCurVal = "";

      if(tSrc=="script") { // DB에서 갖고 오면 값이 없음. 칼럼 정보(columnName, columnType)만 옴 
        if(tr.title=="header") {
          eval("vCurVal = val."+tr.title+".text ;");
        } else {
          eval("vCurVal = val."+tr.title+";");
        }
      }

      if(tr.title=="fieldName") {
        tIsCol = true;
        thtml += "<td class=\"text-center w30\">"+rId+"</td>"
        thtml += "<td class=\""+(tr.className!=""?tr.className+" ":"")+"\"><input id=\""+tr.title+"_"+rId+"\" type=\"text\" value=\""+(tSrc=="script" ? vCurVal.toUpperCase() : val.columnName)+"\" class=\"form-control input-sm cols-field \" /></td>";
      } else {
        var vprops = tr.value.split(",");
        if(vprops.length==2&&(tr.value=="Y,N"||tr.value=="N,Y"||tr.value=="true,false"||tr.value=="false,true")) {
          // checkbox
          var tChkval = ""; var tUnChkval = "";
          if(vprops[0]=="Y"||vprops[0]=="N") {
            tChkval = "Y"; tUnChkval = "N";
          } else if (vprops[0]=="true"||vprops[0]=="false") {
            tChkval = "true"; tUnChkval = "false";
          } else {
            tChkval = vprops[0]; tUnChkval = vprops[1];
          }
          thtml += "<td class=\""+(tr.className!=""?tr.className+" ":"")+(tr.width!=""?"w"+tr.width:"")+"\"><input type=\"checkbox\" id=\""+tr.title+"_"+rId+"\" class=\"form-control input-sm\" "+(vCurVal&&(vCurVal.toString()==tChkval) ? "checked":"")+" value=\""+tChkval+"\" unchecked-value=\""+tUnChkval+"\">";
        } else if(vprops.length>=2) {
          // select
          thtml += "<td class=\""+(tr.className!=""?tr.className+" ":"")+(tr.width!=""?"w"+tr.width:"")+"\"><select id=\""+tr.title+"_"+rId+"\" class=\"form-control f_"+tr.title+"\">";
          var tmpObj;
          for(var k in vprops) {
            if(tr.proptype=="object") {
              eval("tmpObj = " + vprops[k]);
            } else {
              tmpObj = vprops[k];
              if(typeof(vCurVal)!="undefined") vCurVal = vCurVal.toString();
              //console.log(vCurVal, tmpObj)
            }
            thtml += "<option value=\""+vprops[k]+"\" "+((tSrc=="script"&&vCurVal==tmpObj)||(tSrc=="sql"&&tmpObj=="char") ? "selected":"")+">"+vprops[k]+"</option>";
          }
          thtml += "</select></td>";
        } else {
          // input
          thtml += "<td class=\""+(tr.className!=""?tr.className+" ":"")+(tr.width!=""?"w"+tr.width:"")+"\"><input id=\""+tr.title+"_"+rId+"\" type=\"text\" value=\""+(typeof(vCurVal)=="undefined" ? "":vCurVal)+"\" class=\"form-control input-sm\" /></td>";
        }        
      }
    }
    thtml += "</tr>";
    
    $("#grid-cols-setting").append(thtml);
    thtml = "";

  });
  
  // update table 정보 넣어주기
  if(tSrc=="script") {
    getXmlQuery(cObj); // Xml 파일에서 Update 정보 읽어오고 넣어주기 
  } else { // DB실행한 결과 object에서 내용 읽어오기
    // DB에서 실행했을 때 이전에 다른 프로그램 속성 설정값 읽어오기--
    var tColobjs = $(".row-2 input.cols-field");
    var tColtxt = "@";
    $.each(tColobjs, function(idx, val) {
      tColtxt += $(val).val() + "@";
    });
    var param = new Array (strPgmCode.substring(0, 2), strPgmCode, tColtxt);
    dg_devpropsrc.Retrieve(param);

    // 그리드 프로퍼티 목록에 값 세팅
    setGridPropFromSrc();
  }

  // setColorColsprop();
  $("select.form-control,input[type=checkbox].form-control").on("change", function() {
    setColorColsprop();
  });

  $(".row-2 .form-control:not(#update-table)").on("focus", function() {
    $(".property-wrap .tab-content").css({height: "auto"});
  }).on("blur", function() {
    $(".property-wrap .tab-content").css({height: "200px"});
  }).on("change", function() {
    var tId = $(this).attr("id");
    tId = tId.split("_");

    $("tr.tr-row-"+tId[tId.length-1]).removeClass("matched").addClass("notmatched");
  });
}

// 그리드 속성값을 Prop Src 데이터 윈도우에서 세팅하기 
var setGridPropFromSrc = function() {
  var rcnt = dg_devpropsrc.RowCount();
  var tFname = "";

  // propsrc 데이터 윈도우 속성값을 위 Html 영역에 세팅
  for(var i=1; i<=rcnt; i++) {
    tFname = dg_devpropsrc.GetItem(i, "FIELDNAME");
    tTblrow = $("input.cols-field[value='"+tFname+"']").attr("id").split("_");
    tTblrow = tTblrow[tTblrow.length-1];
    $("tr.tr-row-"+tTblrow).removeClass("notmatched").addClass("matched");
    $.each(colsproperties, function(idx, val) {
      // 칼럼값 세팅.. 
      if(idx>3) {
        if($("#"+val.title+"_"+tTblrow).is(":checkbox")) {
          if($("#"+val.title+"_"+tTblrow).attr("value") == dg_devpropsrc.GetItem(i, val.title.toUpperCase() )) {
            $("#"+val.title+"_"+tTblrow).prop("checked", true);
          }
        } else {
          $("#"+val.title+"_"+tTblrow).val( dg_devpropsrc.GetItem(i, val.title.toUpperCase() ) ) ;
        }
      }
    });
  }
}

// update 테이블 정보 입력하기
var setGridUpdateInfo = function() {
  $.each(updCols, function(idx, val) {
    tIdx = idx+1;
    if(val.name==$("#fieldName_"+tIdx).val()) {
      $("#update_type_"+tIdx).val( val.type );
      $("#updatable_"+tIdx).prop("checked", (val.updatable=="Y" ? true : false));
      $("#update_iskey_"+tIdx).prop("checked", (val.iskeycol=="Y" ? true : false));
    }
    //updCols[idx] = { name: $(val).attr("name"), type: $(val).attr("type"), updatable: $(val).attr("updatable"), iskeycol: $(val).attr("iskeycol") };
  });
}

// 배열에서 params 생성하기 (aPrs:object {name:"as_date", type:"String"})
var setParams = function(aPrs) {
  // 기존 입력된 파라미터값 저장
  var tmpParams = new Array;
  var tmpObj;
  var tId = "";
  $.each($("#sql-params .col-paramname"), function(idx, val) {
    tId = $(this).attr("id").replace("pname-", "");
    tmpObj = new Object;
    tmpObj.name = tId;
    tmpObj.type = $("#ptype-"+tId).val();
    tmpParams.push(tmpObj);
  });

  // 배열에서 새로운 파라미터 입력
  var tHtml = "";
  $("#sql-params").empty();
  var tidx = 0;

  if(aPrs.length>0) {
    $.each(aPrs, function(idx, val) {
      tidx = idx + 1;
      tHtml = "<tr>";
      tHtml += "<td> "+tidx+". </td>";
      tHtml += "<td>"+"<input id=\"pname-"+val.name+"\" readonly class=\"form-control input-sm col-paramname\" value=\""+val.name+"\"/>"+"</td>";
      tHtml += "<td>"+"<select id=\"ptype-"+val.name+"\" class=\"form-control\">"+getParsOption(val.type)+"</select>"+"</td>";
      // tHtml += "<td>"+"<input id=\"pval-"+val.name+"\" class=\"form-control input-sm col-paramval\" value=\"\"/>"+"</td>";
      tHtml += "</tr>";

      $("#sql-params").append(tHtml);
    });
  } else {
    tHtml = "<tr><td>-</td><td>-</td></tr>";
    $("#sql-params").append(tHtml);
  }

  // 기존 저장된 파라미터값 다시 넣어주기
  $.each(tmpParams, function(idx, val) {
    $("#ptype-"+val.name).val(val.type);
  });
  
  $(".wrap-updtables input, .wrap-updtables select").on("focus", function() {
    resizePanelHeight("OPEN");
  }).on("blur", function() {
    resizePanelHeight("CLOSE");
  })
}

var getParsOption = function(aVal) {
  var tOption = "";

  $.each(parstypes, function(idx, val) {
    tOption += "<option value=\""+val+"\" "+(aVal==val ? "selected" : "")+">"+val+"</option>";
  });

  return tOption;
}

// select 의 선택값이 false 또는 N 일때, 컬러 바꾸기
var setColorColsprop = function() {
  $("select.form-control").each(function() {
    if($(this).val()=="false"||$(this).val()=="N") {
      $(this).addClass("fc_false");
    } else {
      $(this).removeClass("fc_false");
    }
  })
}

// xmlx용 code로 변환
var convertXmlQuery = function(aCopy) {
  var strxml = "";
  var strparam = "";
  var struc = "";
  var strqry = ed_gridsql.getValue();
  var tMaxLen = 0;
  var patt = /:(\w+)/g;

  if(strqry!=""&&patt.test(strqry)) {
    strqry = convertParams(strqry);
  }

  $.each($(".wrap-updtables input.form-control.col-paramname"), function() {
    //var re = new RegExp(":"+$(this).attr("id"), "g");
    //strqry = strqry.replace(re, "#"+$(this).attr("id")+"#");
    var tTypeId = $(this).attr("id");
    tTypeId = tTypeId.replace("pname-", "ptype-");
    strparam += "    <param name=\""+$(this).val()+"\" type=\""+$("#"+tTypeId).val()+"\"/>\n";
  });

  $.each($(".row-2 input.cols-field"), function() {
    var tId = $(this).attr("id");
    tId = tId.replace("fieldName_", "");
    var tName = $("#fieldName_"+tId).val();
    var tUpdatable = $("#updatable_"+tId).is(':checked') ? $("#updatable_"+tId).attr("value") : $("#updatable_"+tId).attr("unchecked-value");
    var tIskey = $("#update_iskey_"+tId).is(':checked') ? $("#update_iskey_"+tId).attr("value") : $("#update_iskey_"+tId).attr("unchecked-value");
    struc += "    <column name='"+tName+"' #INDENT_BLANK_"+tName.length+"# type='"+$("#update_type_"+tId).val()+"' updatable='"+tUpdatable+"' iskeycol='"+tIskey+"' />\n";
    if(tMaxLen<tName.length) tMaxLen = tName.length;
  });

  // indent 처리
  var tBlank = "";
  for(var i=tMaxLen; i>=1; i--) {
    var patt = new RegExp("#INDENT_BLANK_"+i+"#", "g");
    struc = struc.replace(patt, tBlank);
    tBlank += " ";
  }

  strxml += "  <select id=\""+$("#select-id").val()+"\">\n";
  strxml += "    <sql><![CDATA[\n";
  strxml += strqry + "\n";
  strxml += "    ]]></sql>\n\n";
  strxml += strparam + "\n\n";
  strxml += "    <update_table>"+$("#update-table").val()+"</update_table>\n\n";
  strxml += struc + "\n\n";
  strxml += "  </select>\n";

  $("#code-xmlx").val(strxml);
}

// FreeForm Html 생성
var setFreeformHtml = function(aVer) {
  if(aVer=="4") {
    var thtml = "";
    var tcols = $(".cols-field");
    var tCp = colsproperties;
    
    thtml += "<div id='freeform' class='detail_box'>\n";
    for(var i=1; i<=tcols.length; i++) {
      var tfieldname = $("#fieldName_"+i).val().trim();
      var ttitle = $("#header_"+i).val().trim();
      if($("#updatable_"+i).val()=="Y") {
        thtml += "  <div class='detail_label w120'>"+ttitle+"</div>\n";
        thtml += "  <div id='di_"+tfieldname+"' class='detail_input_bg'>\n";
        thtml += "    <input id='"+tfieldname+"' type='text' class='detail_input_fix w110' >\n";
        thtml += "  </div>\n";
      }
    }
    thtml += "</div><!-- .detail_box -->\n";

    $("#code-freeform").val(thtml);
  } else {
    var thtml = "";
    var tcols = $(".cols-field");
    var tCp = colsproperties;
    
    thtml += "  <div class=\"x5-ff\">\n";
    thtml += "    <div class=\"x5-row\">\n";
    for(var i=1; i<=tcols.length; i++) {
      var tfieldname = $("#fieldName_"+i).val().trim();
      var ttitle = $("#header_"+i).val().trim();
      if($("#updatable_"+i).val()=="Y") {
        thtml += "      <label for=\""+tfieldname+"\" class=\"x5-col-3 text-right\">"+ttitle+"</label>\n";
        thtml += "      <div class=\"x5-col-9\">\n";
        thtml += "        <input id=\""+tfieldname+"\" type='text' class='form-control input-sm' >\n";
        thtml += "      </div>\n";
      }
    }

    thtml += "    </div><!-- .x-row -->\n";
    thtml += "  </div><!-- .x5-ff -->\n";

    $("#code-freeform2").val(thtml);
  }
}

// 코드 생성하기
var generateCode = function(aDiv) {
  if(aDiv=="refresh") {
    $("#code-xmlx").val("");
    $("#code-grid").val("");
    $("#code-freeform").val("");
    $("#code-freeform2").val("");
  } else {
    var cObj = $("#grid-objs").val();
    var nGrid = $("#new-grid-id").val();
    nGrid = nGrid.replace(/\s/, "");

    if(strPgmCode=="") {
      alert("프로그램 코드를 선택하신 후, 작업하세요. ");
      return;
    } 

    convertXmlQuery();
    setGridCode();
    setFreeformHtml("4");
    // setFreeformHtml();

    // 그리드 정보 저장하기
    saveGridProperties();

    $("#update-table tr.notmatched").removeClass("notmatched").addClass("matched");
  }
}

// 그리드 정보 저장하기
var saveGridProperties = function() {
  _X.ExecSql("sm", "SM900601", "SM900601_GRID_DELETE", new Array(strPgmCode.substring(0, 2), strPgmCode, getWorkingGridId()), "array", "");

  dg_devprop.Reset();
  if(setDataGrid()) {
    dg_devprop.Save("", "", "N");
  }
}

// 저장하기 전, 칼럼 속성을 저장 Grid에 넣기
var setDataGrid = function() {
  var cObj = $(".row-2 input.cols-field");

  var tRow = 0;
  $.each(cObj, function(idx, val) {
    tRow = dg_devprop.InsertRow();
    var cIdx = idx + 1;

    // dg_devprop.SetItem(tRow, "GRID_CODE",     strPgmCode +"."+ cObj.replace("columns_", ""));
    dg_devprop.SetItem(tRow, "SYS_ID",           strPgmCode.substring(0, 2));
    dg_devprop.SetItem(tRow, "PGM_CODE",         strPgmCode);
    dg_devprop.SetItem(tRow, "GRID_ID",          getWorkingGridId());
    dg_devprop.SetItem(tRow, "FIELDNAME",        $(val).val());
    dg_devprop.SetItem(tRow, "SEQ",              idx+1);
    dg_devprop.SetItem(tRow, "WIDTH",            getObjectVal("#width_"+cIdx));
    dg_devprop.SetItem(tRow, "MUST_INPUT",       getObjectVal("#must_input_"+cIdx));
    dg_devprop.SetItem(tRow, "VISIBLE",          getObjectVal("#visible_"+cIdx));
    dg_devprop.SetItem(tRow, "READONLY",         getObjectVal("#readonly_"+cIdx));
    dg_devprop.SetItem(tRow, "EDITABLE",         getObjectVal("#editable_"+cIdx));
    dg_devprop.SetItem(tRow, "SORTABLE",         getObjectVal("#sortable_"+cIdx));
    dg_devprop.SetItem(tRow, "HEADER",           getObjectVal("#header_"+cIdx));
    dg_devprop.SetItem(tRow, "HEADER_TEXT",      "{text: '"+getObjectVal("#header_"+cIdx)+"'}");
    dg_devprop.SetItem(tRow, "STYLES",           getObjectVal("#styles_"+cIdx));
    dg_devprop.SetItem(tRow, "EDITOR",           getObjectVal("#editor_"+cIdx));
    dg_devprop.SetItem(tRow, "LOOKUPDISPLAY",    getObjectVal("#lookupDisplay_"+cIdx));
    dg_devprop.SetItem(tRow, "RENDERER",         getObjectVal("#renderer_"+cIdx));
    dg_devprop.SetItem(tRow, "BUTTON",           getObjectVal("#button_"+cIdx));
    dg_devprop.SetItem(tRow, "ALWAYSSHOWBUTTON", getObjectVal("#alwaysShowButton_"+cIdx));
  });

  return true;
}

// SQL 실행해서 칼럼정보 가져오기
var exeSql = function() {
  var tSql = setParamsToSql();
  getUpdateTables(tSql, $("#update-table").val());

  $.ajax({
    method: "POST",
    url: "/APPS/sm/jsp/SM900601_meta.jsp",
    dataType: "json",
    data: { query: tSql, tables: qryTables.join(',') },
    success: function( data, textStatus, jQxhr ){ 
      qryColumns = data.columns_info;
      setGridColsInfo( '', qryColumns );
      $("#sqlModal").modal("hide");
    },
    error:function(request, status, error){
      alert( "sql파일을 실행하는 중 오류가 발생했습니다."+$(request.responseText).find("#content_pop").html() );
    }
  });
}

// query에서 Update Table 목록 가져오기
var getUpdateTables = function(aSql, aTbl) {
  // parsing table name from query
  var patt = /(?:from|into|update|join)(?:\s+|\n+)(\w+)(?:\s+|\n+|$)/gi;
  var patt2 = /\(/g;
  var strqry = aSql;
  var tables = strqry.match(patt);
  qryTables = [];

  $("#update-table").empty();
  $("#update-table").append( "<option value=\"\">없음</option>" );
  if(strqry&&tables) {
    $.each(tables, function(idx, val) {
      if(!patt2.test(val)) {
        var arrtbl = val.replace(/\n/, " ").trim().toUpperCase().split(" ");
        qryTables[idx] = arrtbl[arrtbl.length-1];

        $("#update-table").append( "<option value=\""+qryTables[idx]+"\" "+(aTbl&&aTbl==qryTables[idx]?"selected":"")+">"+qryTables[idx]+"</option>" );
      }
    });
  }
}

// 실행하기 위해 SQL Query 에 parameter Value로 치환
var setParamsToSql = function() {
  var strqry = ed_gridsql.getValue();
  var tRow = 0;
  strqry = convertParams(strqry); //convertXmlQuery(strqry);

  $.each($(".col-paramname"), function() {
    var tn = $(this).attr("id").replace("pname-", "");
    var re = new RegExp("#"+tn+"#", "g");
    //strqry = strqry.replace(re, "'"+$(this).val()+"'"); 파라미터 값으로 수정
    strqry = strqry.replace(re, "''");
  });

  return strqry;
}

// 작업중인 그리드 명칭 가져오기
var getWorkingGridId = function() {
  var tGridId = $("#grid-objs").val().replace("columns_", "");

  if($("#check-new-grid").is(':checked')) {
    tGridId = $("#new-grid-id").val();
  }
  return tGridId;
}

// panel 높이 조정하기
var resizePanelHeight = function(aDiv) {
  return; 

  // if(aDiv=="OPEN") {
  //   $(".wrap-textarea").css({height: "500px"});
  //   $(".wrap-params").css({height: "450px"});
  // } else {
  //   $(".wrap-textarea").css({height: "100px"});
  //   $(".wrap-params").css({height: "70px"});
  // }
}

// SQL적용버튼 활성화
var setButtonActive = function(aStatus) {
  if(aStatus=="disabled") {
    $("#btn-modified-sql").removeClass("btn-info").addClass("disabled btn-default");
  } else {
    $("#btn-modified-sql").removeClass("disabled btn-default").addClass("btn-info");
  }
}

// input object(체크박스 포함)에서 상태에 따라 값 가져오기 aOid : #붙은 object id (예) #test_val
var getObjectVal = function(aOid) {
  var tV;
  if(aOid) {
    if($(aOid).is(':checkbox')) {
      tV = $(aOid).is(':checked') ? $(aOid).attr("value") : $(aOid).attr("unchecked-value") ;
    } else {
      tV = $(aOid).val();
    }
  }

  return tV;
}

// 컬럼 속성정보 배열 (나중에 버젼별로 ajax 처리) - proptype 는 변수의 타입(text, object)
var colsproperties = [
  {
    title: "update_type",
    value: "char,number",
    proptype: "text",
    width: "",
    isprop: false ,
    className: "bg-update"
  },
  {
    title: "updatable",
    value: "Y,N",
    proptype: "text",
    width: "",
    isprop: false ,
    className: "bg-update"
  },
  {
    title: "update_iskey",
    value: "N,Y",
    proptype: "text",
    width: "",
    isprop: false ,
    className: "bg-update"
  },
  {
    title: "fieldName",
    value: "",
    proptype: "text",
    width: "",
    className: ""
  },
  {
    title: "width",
    value: "0",
    proptype: "text",
    width: "50",
    className: ""
  },
  {
    title: "must_input",
    value: "N,Y",
    proptype: "text",
    width: "",
    className: ""
  },
  {
    title: "visible",
    value: "true,false",
    proptype: "text",
    width: "",
    className: ""
  },
  {
    title: "readonly",
    value: "true,false",
    proptype: "text",
    width: "",
    className: ""
  },
  {
    title: "editable",
    value: "true,false",
    proptype: "text",
    width: "",
    className: ""
  },
  {
    title: "sortable",
    value: "true,false",
    proptype: "text",
    width: "",
    className: ""
  },
  {
    title: "header",
    value: "",
    proptype: "text",
    width: "",
    className: ""
  },
  {
    title: "styles",
    value: "_Styles_text,_Styles_textc,_Styles_textr,_Styles_number,_Styles_numberc,_Styles_numberl,_Styles_numberf1,_Styles_numberf2,_Styles_numberf3,_Styles_numberf4,_Styles_date,_Styles_datetime,_Styles_checkbox,_Styles_tree",
    proptype: "object",
    width: "",
    className: ""
  },
  {
    title: "editor",
    value: "_Editor_text,_Editor_multiline,_Editor_number,_Editor_numberf1,_Editor_numberf2,_Editor_numberf3,_Editor_numberf4,_Editor_date,_Editor_datetime,_Editor_checkbox,_Editor_dropdown",
    proptype: "object",
    width: "",
    className: ""
  },
  {
    title: "lookupDisplay",
    value: ",true,false,aaa",
    isoption: true ,
    width: "",
    className: ""
  },
  {
    title: "renderer",
    value: ",true,false,aaa",
    isoption: true ,
    width: "",
    className: ""
  },
  {
    title: "button",
    value: ",action",
    isoption: true ,
    proptype: "text",
    width: "80",
    className: ""
  },
  {
    title: "alwaysShowButton",
    value: ",true,false",
    isoption: true ,
    proptype: "text",
    width: "80",
    className: ""
  }
];
// isprop: false 이면 나중에 Grid.js에 빠집니다.
// isoption: true 이면 나중에 프로퍼티 필수로 넣지 않습니다.

// 파라미터 타입 배열 
var parstypes = ["String", "Integer"];



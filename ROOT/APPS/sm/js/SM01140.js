﻿//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 : @프로그램코드|프로그램명
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
// 	성명					  소속                작성일
// 
//===========================================================================================
//	
//===========================================================================================

function x_InitForm2() {
	_X.Tabs(tabs_1, 0);
	_X.InitGrid( {grid: dg_1, sqlId: "SM01140_01", share: true, updatable: true} );
	_X.InitGrid( {grid: dg_2, sqlId: "SM01140_02", share: false, updatable: true} );

	$("#btn_sprojfind").bind("click", function(){_X.FindUserProjCode(window,"G_SPROJECT","", new Array(mytop._CompanyCode, mytop._UserID));});
	$("#btn_projfind").bind("click", function(){_X.FindUserProjCode(window,"G_PROJCD","", new Array(mytop._CompanyCode, mytop._UserID));});
	$('#btn_empfind').bind("click", function(){_X.FindEmp(window,"G_FINDEMP", INSTALL_EMP_NAME.value, new Array(mytop._CompanyCode, '12'));});
	$('#btn_empfind2').bind("click", function(){_X.FindEmp(window,"G_FINDEMP2", RETURN_EMP_NAME.value, new Array(mytop._CompanyCode, '12' ));});
	$('#btn_deptfind').bind("click", function(){_X.CommonFindCode(window, "부서코드찾기", "hr", "F_DEPT_CODE", "", [mytop._CompanyCode, "%"], "FindDeptCode|HrFindCode|FindDeptCode", 500, 650);});
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted) {
	if ( ab_allcompleted ) {
		x_DAO_Retrieve(dg_1);
	}
}

function x_DAO_Retrieve2(a_dg){
	switch(a_dg.id){
		case 'dg_1':
			var param = new Array(mytop._CompanyCode, S_PROJ_CD.value,  S_FIND.value, _X.GetTabIndex(tabs_1)==1 ? "Y" : "N", _X.GetTabIndex(tabs_1)==2 ? "Y" : "N");
			var sensorId = dg_1.GetItem(dg_1.GetRow(), 'SENSORID');
			var curRow = dg_1.GetRow();
			dg_1.Retrieve(param);

			var fRow = dg_1.Find(sensorId, "SENSORID");
			if(fRow.row>0) {
				dg_1.SetRow(fRow.row);
			} else if (dg_1.RowCount()>=curRow) {
				dg_1.SetRow(curRow);
			}
			sensorId = dg_1.GetItem(dg_1.GetRow(), 'SENSORID');
			param = new Array(mytop._CompanyCode, sensorId );
			dg_2.Retrieve(param);
		break;
		case 'dg_2':	
			param = new Array(mytop._CompanyCode, dg_1.GetItem(dg_1.GetRow(), 'SENSORID') );
			dg_2.Retrieve(param);
		break;
	}
}

function xe_EditChanged2(a_obj, newValue, oldValue){
	switch(a_obj.id){
		case 'S_PROJ_CD':
		case 'S_FIND':
			x_DAO_Retrieve(dg_1);
			break;
		case 'KEEP_YN':
			if (KEEP_YN.checked) {
				DISCARD_YN.checked = false;
				dg_1.SetItem(dg_1.GetRow(), "DISCARD_YN", "N");
			}
			break;
		case 'DISCARD_YN':
			if (DISCARD_YN.checked) {
				KEEP_YN.checked = false;
				dg_1.SetItem(dg_1.GetRow(), "KEEP_YN", "N");
			}
			break;
	}
}

function xe_InputKeyEnter2(a_obj, event, ctrlKey, altKey, shiftKey){
	if(a_obj==S_FIND) {
		x_DAO_Retrieve(dg_1);		
	}
}

function xe_InputKeyDown2(a_obj, event, keyCode, ctrlKey, altKey, shiftKey){

}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	var cData1 		= dg_1.GetChangedData();
	var li_cnt = 0;
	var ls_value ="";
	var chkMsg = "";

	for (i=0 ; i < cData1.length; i++){
		if(cData1[i].job == 'D') continue;
		if(cData1[i].job == 'I'){
			ls_value = _X.XmlSelect("sm", "SM01140", "R_SM01141_01", new Array(mytop._CompanyCode, cData1[i].data["SENSORID"]), "array");
			li_cnt = Number(ls_value[0][0]);
			if(li_cnt > 0){
			_X.MsgBox('확인',"동일한 장비번호가 존재합니다.") ;			
			return false;
			}	
			dg_1.SetItem(cData1[i].idx,'SENSOR_SN',cData1[i].data["SENSORID"]);
		}		
	}

	return true;
}

function x_DAO_Saved2(){
	x_DAO_Retrieve(dg_1);
}

function x_DAO_Insert2(a_dg, rowIdx){
	return 100;
}

function x_Insert_After2(a_dg, rowIdx){
	a_dg.SetItem(rowIdx, "COMPANY_CODE", mytop._CompanyCode);
}

function x_DAO_Duplicate2(a_dg, rowIdx){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
	
}

function x_DAO_Delete2(a_dg){
	return 100;
}

function x_DAO_Deleted2(a_dg){

}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){

}

function xe_TabChanging2(a_tab, newIdx, oldIdx, newTab, oldTab){
	return 100;
}

function xe_TabChanged2(a_tab, newIdx, oldIdx, newTab, oldTab){
	x_DAO_Retrieve(dg_1);
}

function xe_GridDataChange2(a_dg, rowIdx, colName, newValue, oldValue){

}

function xe_GridDataChanged2(a_dg, rowIdx, colName, newValue, oldValue){

}

function xe_GridRowFocusChange2(a_dg, newRowIdx, oldRowIdx){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, newRowIdx){
	if(a_dg.id == 'dg_1'){
		var ls_in_status = dg_1.GetRowState() == 'I' ? false : true;
		 $("#SENSORID").attr("readonly",ls_in_status);

		x_DAO_Retrieve2(dg_2);		
	}

}

function xe_GridItemFocusChange2(a_dg, newRowIdx, newColName, oldRowIdx, oldColName){

}

function xe_GridItemFocusChanged2(a_dg, newRowIdx, newColName){

}

function xe_BeforeGridDataLoad2(a_dg){
	return 100;
}

function xe_GridDataLoad2(a_dg){
	return 100;
}

function xe_GridButtonClick2(a_dg, rowIdx, colName){
/*	if(a_dg.id == "dg_1" && (colName == 'PROJ_CODE' || colName == 'PROJ_NAME')){
		_X.FindProjCode(window,"G_PROJCD", "", new Array(mytop._CompanyCode, '%'));
	}*/
}

function x_ReceivedCode2(a_retVal){
	switch(_FindCode.finddiv){
		case "G_SPROJECT":
			$('#S_PROJ_CD').selectBox('destroy');
		  	$("#S_PROJ_CD").val(_FindCode.returnValue.PROJ_CODE);
		  	$('#S_PROJ_CD').selectBox();
		  	xe_EditChanged2(S_PROJ_CD, S_PROJ_CD.value);
		break;
		case 'G_PROJCD':
			dg_1.SetItem(dg_1.GetRow(), 'PROJ_CODE', _FindCode.returnValue.PROJ_CODE);
			dg_1.SetItem(dg_1.GetRow(), 'PROJ_NAME', _FindCode.returnValue.PROJ_NAME);
		break;
		 case 'G_FINDEMP':
			var emp_no 	 = _FindCode.returnValue.EMP_NO;
      		var emp_name = _FindCode.returnValue.EMP_NAME;
			dg_1.SetItem(dg_1.GetRow(), 'INSTALL_EMP_NO', emp_no);
			dg_1.SetItem(dg_1.GetRow(), 'INSTALL_EMP_NAME', emp_name);
		break;
		case 'G_FINDEMP2':
			var emp_no 	 = _FindCode.returnValue.EMP_NO;
      		var emp_name = _FindCode.returnValue.EMP_NAME;
			dg_1.SetItem(dg_1.GetRow(), 'RETURN_EMP_NO', emp_no);
			dg_1.SetItem(dg_1.GetRow(), 'RETURN_EMP_NAME', emp_name);
		break;
		case "F_DEPT_CODE" : // 부서
			var emp_no 	 = _FindCode.returnValue.DEPT_CODE;
			var emp_no 	 = _FindCode.returnValue.DEPT_NAME;
			dg_1.SetItem(dg_1.GetRow(), "DEPT_CODE"    , a_retVal.DEPT_CODE); // 부서코드
			dg_1.SetItem(dg_1.GetRow(), "DEPT_NAME"    , a_retVal.DEPT_NAME); // 부서명			
		break;
	}
}

function xe_GridHeaderClick2(a_dg, colName){

}

function xe_GridItemClick2(a_dg, rowIdx, colIdx, colName){

}

function xe_GridItemDoubleClick2(a_dg, rowIdx, colIdx, colName){

}

function xe_GridBeforeKeyDown2(a_obj, event, keyCode, ctrlKey, altKey, shiftKey) {
	return 100;
}

function xe_GridKeyDown2(a_obj, event, keyCode, ctrlKey, altKey, shiftKey) {

}

function xe_FileButtonClick2(a_obj, rowIdx, colName, div){
	return 100;
}

function xe_FileChanged2(a_dg, rowIdx, colName, fileName, fileSize, fileType, firstData){
	return 100;
}

function xe_FileUploaded2(a_dg, uploadInfo){
	return 100;
}

function xe_FileDeleted2(a_dg, rowIdx, colName){
	return 100;
}

function x_Close2() {
	return 100;
}

function pf_sensor_init() {
	_X.EP("SM", "SP_XM_IOLOG_SENSOR_INIT", [mytop._UserID, mytop._ClientIP]);
	x_DAO_Retrieve(dg_1);
}

// 장비반납
function pf_return(){
	var ls_in_date  = dg_1.GetItem(dg_1.GetRow(), 'INSTALL_DATE');
	var ls_in_empno = dg_1.GetItem(dg_1.GetRow(), 'INSTALL_EMP_NO');
	var ls_re_date  = dg_1.GetItem(dg_1.GetRow(), 'RETURN_DATE');
	var ls_re_empno = dg_1.GetItem(dg_1.GetRow(), 'RETURN_EMP_NO');
	var ls_proj 	= dg_1.GetItem(dg_1.GetRow(), 'PROJ_CODE');

	if(ls_in_date == null || ls_in_date == ""){
		_X.MsgBox("확인", "등록일을 넣어주시기 바랍니다.");
		return;
	}

/*	if(ls_in_empno == null || ls_in_empno == ""){
		_X.MsgBox("확인", "인수자 정보를 넣어주시기 바랍니다.");
		return;
	}*/

	if(ls_re_date == null || ls_re_date == ""){
		_X.MsgBox("확인", "반납일을 넣어주시기 바랍니다.");
		return;
	}

	if(ls_re_empno == null || ls_re_empno == ""){
		_X.MsgBox("확인", "반납자 정보를 넣어주시기 바랍니다.");
		return;
	}

	if(ls_proj == null || ls_proj == ""){
		_X.MsgBox("확인", "현장을 넣어주시기 바랍니다.");
		return;
	}

	if ( _X.IsDataChangedAll(false) > 0 ) {
		_X.Noty("변경된 데이터 저장 후 작업진행하시기 바랍니다.");
 		return;
  	}

	if(_X.MsgBoxYesNo("【장비반납】", "장비반납을 하시겠습니까? \n기존정보가 장비 반납이력에 등록됩니다.") == 1)	{
		var ls_id  = dg_1.GetItem(dg_1.GetRow(), 'SENSORID');		
	
		var ll_return = _X.ExecProc("cm", "SP_SM_CHECKINOUT_SENSOR_HIS", [mytop._CompanyCode, ls_id, mytop._UserID, mytop._ClientIP]);

		if(ll_return < 1){
 			_X.MsgBox("에러", "장비 반납 중 에러가 발생하였습니다.");
		}else{
			_X.MsgBox("반납", "장비 반납이 완료되었습니다.");
		}
	}
	x_DAO_Retrieve2(dg_1);

}
/**
 *<pre>
 * Statements
 *</pre>

 * @Class Name : SM01020.js
 * @Descriptio	 거래처코드 등록
 * @Modification Information
 * @
 * @   수정일       수정자               수정내용
 * @ ----------    --------    ---------------------------
 * @  20170308 			jhbae					최초수정작성
 */
var ls_cust_code = "";

function x_InitForm2(){
	$("#grid_1").hide();

	$(".search_find_img").click(function() {
		pf_ZipFind($(this).attr("id"));
	});

	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM01020", "SM01020|SM01020_01", true, true);

	$('#btn_find_cust').on("click", function(){
    pf_find_cust();
  }); // 거래처코드

	return 100;
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {

//		if(dg_1.RowCount() <= 0){
//			_X.FormSetAllDisable("freeform", true);
//			_X.FormSetAllDisable("freeform2", true);
//		}

		//드랍다운 구성데이터 쿼리 매개변수에 담음.
		var	comboData   = _X.XmlSelect("sm", "SM01022", "R_SM01022_03", new Array('SM','CUSTTAG')		 			 	 , "json2");			 //거래처구분
		var	comboData2  = _X.XmlSelect("sm", "SM01022", "R_SM01022_03", new Array('SM','TRADETYPE')	 			 	 , "json2");		 	 //거래유형
		var	comboData3  = _X.XmlSelect("sm", "SM01022", "R_SM01022_03", new Array('SM','BUSINESSTYPE')			 , "json2");       //과세정보
		var	comboData4  = _X.XmlSelect("sm", "SM01022", "R_SM01022_03", new Array('SM','PAYGROUP')	 				 , "json2");			 //지급그룹

		// 현업에서 거래처코드 등록시 무조건 일반으로 등록 2017-11-24 관리팀 요청
		comboData = [{"code":"1", "label":"일반"}];
		_X.DDLB_SetData(CUST_TAG			, comboData 		, null, false, false); 	 //거래처구분 	드랍다운리스트 셋팅 . 쿼리에서 code, label 알리아스 지정해줘야함.
		_X.DDLB_SetData(TRADE_TYPE		, comboData2		, null, false, false); 	 //거래유형 		드랍다운리스트 셋팅
		_X.DDLB_SetData(BUSINESS_TYPE	, comboData3		, null, false, false); 	 //과세정보 		드랍다운리스트 셋팅
		_X.DDLB_SetData(PAY_GROUP			, comboData4		, null, false, false); 	 //지급그룹 		드랍다운리스트 셋팅

		dg_1.SetCombo("CUST_TAG"		 , comboData );			// 그리드 코드->한글 , grid 파일에서 lookupDispay:true 걸어줘야함. 쿼리에서 code, label 알리아스 지정해줘야함.
		dg_1.SetCombo("TRADE_TYPE"	 , comboData2);			// 그리드 코드->한글
		dg_1.SetCombo("BUSINESS_TYPE", comboData3);			// 그리드 코드->한글
		dg_1.SetCombo("PAY_GROUP"		 , comboData4);			// 그리드 코드->한글

		//x_DAO_Retrieve2(dg_1);
	}
	return 100;
}

function x_DAO_Retrieve2(a_dg){
	dg_1.Retrieve([ls_cust_code]);

	return 0;
}

function xe_EditChanged2(a_obj, a_val){
	if(a_obj.id == 'BUSINESS_CHK_DATE'){
		dg_1.SetItem(dg_1.GetRow(), 'BUSINESS_CHK_DATE' , BUSINESS_CHK_DATE.value.replace(/\-/g,'') )
	}
	if(a_obj.id == 'CLOSING_DATE'){
		dg_1.SetItem(dg_1.GetRow(), 'CLOSING_DATE' 			, CLOSING_DATE.value.replace(/\-/g,'') )
	}
	if(a_obj.id == 'DEPOSIT_FROMDATE'){
		dg_1.SetItem(dg_1.GetRow(), 'DEPOSIT_FROMDATE' 	, DEPOSIT_FROMDATE.value.replace(/\-/g,'') )
	}
	if(a_obj.id == 'DEPOSIT_TODATE'){
		dg_1.SetItem(dg_1.GetRow(), 'DEPOSIT_TODATE' 		, DEPOSIT_TODATE.value.replace(/\-/g,'') )
	}
	if(a_obj.id == 'INPUT_DATE'){
		dg_1.SetItem(dg_1.GetRow(), 'INPUT_DATE' 				, INPUT_DATE.value.replace(/\-/g,'') )
	}
	if(a_obj==CUST_TAG) {
		if(a_val=="2") {
			$(".detail_cust").inputmask("999999-9999999",{placeholder:" ", clearMaskOnLostFocus: true }); 
		} else {
			$(".detail_cust").inputmask("999-99-99999",{placeholder:" ", clearMaskOnLostFocus: true }); 
		}
	}
	return 100;
}

//enter key event 발생시 호출
function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	return 0;
}

//keydown event 발생시 호출
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){
}

//데이타 저장
function x_DAO_Save2(a_dg){return 100;}

//데이타 저장후 callback
function x_DAO_Saved2(){

	return 100;
}

//데이타 유효성 check
function x_DAO_ChkErr2(){

	var cData = dg_1.GetChangedData();
	if(cData==null || cData=="") return true;

	var sNo = 0;
	var chkMsg = "";

	for(var i=0; i<cData.length; i++){
		if(cData[i].job == "D") continue;

		sNo = cData[i].idx;

		if (cData[i].job=="I") {

			if (cData[i].data["CUST_NAME"] == null || cData[i].data["CUST_NAME"] == '' || cData[i].data["CUST_NAME"] == ' ') {
				chkMsg = "상호명을 기입하시기 바랍니다.";
				_X.MsgBox("확인", chkMsg);
				return 0;
			}

			if (cData[i].data["CUST_CODE"].trim().length != 10 && cData[i].data["CUST_CODE"].trim().length != 13 ) {
				chkMsg = "거래처 코드는 10자리로 입력해야 합니다.";
				_X.MsgBox("확인", chkMsg);
				return 0;
			}

			// if (cData[i].data["BUYR_CORP_NM"] == null || cData[i].data["BUYR_CORP_NM"] == '' || cData[i].data["BUYR_CORP_NM"] == ' ') {
			// 	chkMsg = sNo + " 번째 행에 전자(세금)계산서용 거래처명을 기입하시기 바랍니다.";
			// 	_X.MsgBox("확인", chkMsg);
			// 	return 0;
			// }

			// if (cData[i].data["TRADE_TYPE"] == null || cData[i].data["TRADE_TYPE"] == '' || cData[i].data["TRADE_TYPE"] == ' ') {
			// 	chkMsg = sNo + " 번째 행에 거래유형을 선택하시기 바랍니다.";
			// 	_X.MsgBox("확인", chkMsg);
			// 	return 0;
			// }


			// for(var j=0;j < dg_1.RowCount(); j++){
			// 	if(j==sNo) continue;
			// 	if(dg_1.GetItem(j,"CUST_CODE") == cData[i].data["CUST_CODE"]){
			// 		chkMsg = sNo + "행에 중복된 [거래처코드]가 있습니다.";
			// 		_X.MsgBox("확인", chkMsg);
			// 		return 0;
			// 	}

			// 	if(dg_1.GetItem(j,"CUST_NAME") == cData[i].data["CUST_NAME"]){
			// 		chkMsg = sNo + "행에 중복된 [상호(거래처명)]이 있습니다.";
			// 		_X.MsgBox("확인", chkMsg);
			// 		return 0;
			// 	}

			// 	if(dg_1.GetItem(j,"CUST_NAME") == cData[i].data["CUST_NAME"]){
			// 		chkMsg = sNo + "행에 중복된 [거래처명]이 있습니다.";
			// 		_X.MsgBox("확인", chkMsg);
			// 		return 0;
			// 	}
			// }
		}
	}

	return true;
}

//grid insert/add
function x_DAO_Insert2(a_dg, row){
	if(dg_1.IsDataChanged()) {
		if(_X.MsgBoxYesNo("확인", "변경된 데이타가 존재합니다. 저장하지 않고 새로운 거래처를 등록 하시겠습니까?","",2)!=1) return 0;
  }

	ls_cust_code = "";
  x_DAO_Retrieve();

	return 100;
}

//grid insert 수행후 발생
function x_Insert_After2(a_dg, rowIdx){
	dg_1.SetItem(rowIdx, 'TRADE_TYPE', '20');
	dg_1.SetItem(rowIdx, 'BUSINESS_TYPE', 'A');
	dg_1.SetItem(rowIdx, 'PAY_GROUP', '10');

	// A: 등록, O: 승인, R: 반려
	dg_1.SetItem(rowIdx, 'CUSTOMER_USE', 'A');
	dg_1.SetItem(rowIdx, 'CUST_TAG', '1');
	dg_1.SetItem(rowIdx, 'USE_YN', 'Y');

	dg_1.SetItem(rowIdx, 'EMP_NO', mytop._UserID);
//	dg_1.SetItem(rowIdx, 'INPUT_DATE', _X.ToString(_X.GetSysDate(),'yyyy-mm-dd') );

//	dg_1.SetItem(rowIdx, "cust_tag", ls_cust_tag);
//	fdw_detail2.SetItem(rowIdx, "cust_tag", ls_cust_tag);
//
//	dg_1.SetItem(rowIdx, "use_yn", "Y");
//	fdw_detail2.SetItem(rowIdx, "use_yn", "Y");
//
//	dg_1.Sefdw_detail2tItem(rowIdx, "customer_use", "O");
//	.SetItem(rowIdx, "customer_use", "O");
//
//	dg_1.SetItem(rowIdx, "serial_number", "0000");
//	fdw_detail2.SetItem(rowIdx, "serial_number", "0000");
//
//	dg_1.SetItem(rowIdx, "emp_no", mytop.gs_user_id);
//	fdw_detail2.SetItem(rowIdx, "emp_no", mytop.gs_user_id);
	return 0;
}

//grid 복제
function x_DAO_Duplicate2(a_dg, row){return 100;}

//grid 복제후
function x_Duplicate_After2(a_dg, rowIdx){return 100;}

//grid 삭제
function x_DAO_Delete2(a_dg, row){
	return 100;
}

//excel 저장
function x_DAO_Excel2(a_dg){return 100;}

//출력
function x_DAO_Print2(){return 100;}

//tab 변경시
function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}

//tab 변경시
function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

//grid data 변경시
function xe_GridDataChange2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){

	return 0;
}

//grid row focus change
function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){

	return true;
}

//grid row focus change
function xe_GridRowFocusChanged2(a_dg, a_newrow, a_oldrow){
}

//item focus change event
function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){return true;}

//grid data loaded
function xe_GridDataLoad2(a_dg){
	//pf_ZipFind();
	$("#findmodal").dialog("close");

	return 100;
}


//찾기 화면에서 선택값을 넘긴 경우 호출
function x_ReceivedCode2(a_retVal){
	switch(_FindCode.finddiv){
 		case 'POST_CODE':

			var ls_zipNo 			= a_retVal.zipNo;
			var ls_Addr 			= a_retVal.roadAddrPart1 + a_retVal.roadAddrPart2;
			var ls_AddrDetail = a_retVal.addrDetail;

			dg_1.SetItem(dg_1.GetRow(), 'ZIP' 	, ls_zipNo);
			dg_1.SetItem(dg_1.GetRow(), 'ADDR' 	, ls_Addr);
			dg_1.SetItem(dg_1.GetRow(), 'ADDR2' , ls_AddrDetail);

		break;

		case 'POST_CODE2':

			var ls_zipNo 			= a_retVal.zipNo;
			var ls_Addr 			= a_retVal.roadAddrPart1 + a_retVal.roadAddrPart2;
			var ls_AddrDetail = a_retVal.addrDetail;

			dg_1.SetItem(dg_1.GetRow(), 'RECEIVE_ZIP' 	, ls_zipNo);
			dg_1.SetItem(dg_1.GetRow(), 'RECEIVE_ADDR' 	, ls_Addr);
			dg_1.SetItem(dg_1.GetRow(), 'RECEIVE_ADDR2' , ls_AddrDetail);

		break;

		case 'BANK_CODE':

      var ls_bank_code = _FindCode.returnValue.BANK_CODE;
      var ls_bank_name = _FindCode.returnValue.BANK_NAME;

			dg_1.SetItem(dg_1.GetRow(), 'BANK_CODE' 	, ls_bank_code);
			dg_1.SetItem(dg_1.GetRow(), 'BANK_NAME' 	, ls_bank_name);

		break;

		case 'CUST_CODE':
			ls_cust_code = a_retVal.CUST_CODE ;

			x_DAO_Retrieve2(dg_1);

		break;

	}
}

//grid item button click event
function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){return 100;}

//header click event
function xe_GridHeaderClick2(a_dg, a_col, a_colname){return 100;}

//item double click event
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){return 100;}

//tree item checked event
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){return 100;}

//tree item checked event
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){return 100;}

//- anychart  point click event
function xe_ChartPointClick2(a_dg, a_event){return 100;}

//- anychart  point select event
function xe_ChartPointSelect2(a_dg, a_event){return 100;}

//- anychart  point mouse over event
function xe_ChartPointMouseOver2(a_dg){return 100;}

//- anychart  point mouse out event
function xe_ChartPointMouseOut2(a_dg, a_event){return 100;}

//window(mdi) close
function x_Close2() {return 100;}



//우편번호찾기
function pf_ZipFind(a_param) {
	var ls_Crow = dg_1.GetRow();
	if(ls_Crow <= 0) {return 0;}
		 switch (a_param){
		 	case 'btn_addrfind':
				_X.FindStreetAddr(window,"POST_CODE","", "");
			break;
			case 'btn_addrfind2':
				_X.FindStreetAddr(window,"POST_CODE2","", "");
			break;
			case 'btn_bankcode':
			 	_X.FindBankCode(window,"BANK_CODE", "", "");
			break;
		}
}

function pf_ZipFind2() {
	var a_row = dg_1.GetRow();
	if(a_row<=0)
		return;
	_FindCode.grid_row = a_row;
	_X.findStreetAddr(window,"POST_CODE","", "");
}

// 거래처 찾기 팝업
function pf_find_cust() {
	if(dg_1.IsDataChanged()) {
		if(_X.MsgBoxYesNo("확인", "변경된 데이타가 존재합니다. 저장하지 않고 계속 하시겠습니까?","",2)!=1) return;
  }

	_X.FindCustCode(window, "CUST_CODE", '', new Array('%'));
}
/**
 *<pre>
 * Statements
 *</pre>

 * @Class Name : SM900700.js
 * @Descriptio	 TREE TEST
 * @Modification Information
 * @
 * @  수정일       수정자      수정내용
 * @ -------    	--------    ---------------------------
 * @ 2015.10.08    SKLEE       최초 생성
 */

function x_InitForm2(){
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM900700", "SM900700|SM900700");
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		x_DAO_Retrieve(dg_1);
	}
}

function x_DAO_Retrieve2(a_dg){
	dg_1.Retrieve( [] );
}

function xe_EditChanged2(a_obj, a_val){}
function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){}
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){	}
function x_DAO_Save2(a_dg){}
function x_DAO_Saved2(){}
function x_DAO_ChkErr2(){}
function x_DAO_Insert2(a_dg, row){return 100;}
function x_Insert_After2(a_dg, rowIdx){}
function x_DAO_Duplicate2(a_dg, row){return 100;}
function x_Duplicate_After2(a_dg, rowIdx){}
function x_DAO_Delete2(a_dg, row){return 100;}
function x_DAO_Excel2(a_dg){return 100;}
function x_DAO_Print2(){return 100;}
function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}
function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){}
function xe_GridDataChange2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){}
function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){return true;}
function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){}
function xe_GridDataLoad2(a_dg){return 100;}
function x_FindCode2(a_dg, a_row, a_findKey, a_findstr, a_win) {return 100;}
function x_ReceivedCode2(){}
function x_Close2() {return 100;}
function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){return 100;}
function xe_GridHeaderClick2(a_dg, a_col, a_colname){return 100;}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){return 100;}

function pf_CollapseAll() {
	dg_1.TreeCollapseAll();
}

function pf_ExpandAll() {
	dg_1.TreeExpandAll();
}

function pf_ExpandLevel_2() {
	dg_1.TreeExpandLevel(2);
}

function pf_Collapse() {
	dg_1.TreeCollapse( dg_1.GetRow(), true );
}

function pf_Expand1(childRecursive) {
	dg_1.TreeExpand( dg_1.GetRow(), false );
}

function pf_Expand2(childRecursive) {
	dg_1.TreeExpand( dg_1.GetRow(), true );
}

function pf_GetParent() {
	alert( dg_1.GetParent( dg_1.GetRow() ) );
}

function pf_GetChildCount() {
	alert( dg_1.GetChildCount( dg_1.GetRow() ) );
}

function pf_GetChildren1() {
	alert( dg_1.GetChildren( dg_1.GetRow() ) );
}

function pf_GetChildren2() {
	var obj = dg_1.GetChildren( dg_1.GetRow(), true );
	var str = "";
	for ( var i = 0, ii = obj.length; i < ii; i++ ) {
		for ( var prop in obj[i] ) {
			str += prop + ": " + obj[i][ prop ] + ",\n";
		}
		str += "\n\n";
	}
	alert( str );
}

function pf_GetDescendants1() {
	alert( dg_1.GetDescendants( dg_1.GetRow() ) );
}

function pf_GetDescendants2() {
	var obj = dg_1.GetDescendants( dg_1.GetRow(), "obj" );
	var str = "";
	for ( var i = 0, ii = obj.length; i < ii; i++ ) {
		for ( var prop in obj[i] ) {
			str += prop + ": " + obj[i][ prop ] + ",\n";
		}
		str += "\n\n";
	}
	alert( str );
}

function pf_ChangeParent() {
	dg_1.ChangeParent( 10, 3 );
}

function pf_TreeLevelUp() {
	dg_1.TreeLevelUp();
}

function pf_AddCurrent() {
	dg_1.AddCurrent( dg_1.GetRow() );
}

function pf_AddChild() {
	dg_1.AddChild(dg_1.GetRow());
}

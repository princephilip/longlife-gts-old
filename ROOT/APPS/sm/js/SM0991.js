/**
 *<pre>
 * Statements
 *</pre>
 
 * @Class Name : SM9901.js
 * @Descriptio	 미등록 프로그램 찾기
 * @Modification Information
 * @
 * @  수정일        		 수정자                   수정내용
 * @ -------    --------    ---------------------------
 * @ 2015.01.07    박영찬        최초 생성
 * @
 */
	
//call 파라미터(사용자그룹, 회사코드)	
var pCompanyCode = null;
var pUserGroup = null;

	
function x_InitForm2(){
	if(_Caller._FindCode.findcode=="undefined" || _Caller._FindCode.findcode == null){
	}else{
		var callParam =_Caller._FindCode.findcode;
		pCompanyCode = callParam[0];
		pUserGroup = callParam[1];
	}
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM0991", "SM_AUTH_PGMCODE|R_SM0991_01");

	//버튼 function bind
	$("#btn_search").bind("click", function(){x_DAO_Retrieve();});
	$("#btn_confirm").bind("click", function(){x_Confirm();});
	
	return 100;
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		dg_1.SetCheckBar(true);
		var	comboData  = _X.XmlSelect("sm", "SM_AUTH_SYS", "W_SM0915_01", new Array(mytop._CompanyCode), "json2");
		dg_1.SetCombo("SYS_ID", comboData);	//시스템구분
		dg_1.GridReadOnly(true);
		setTimeout('x_DAO_Retrieve(dg_1)',0);
	}
	return 100;
}

function x_DAO_Retrieve2(a_dg){
	//alert(new Array(pCompanyCode,  S_SYS_ID.value + '%', pUserGroup, S_PGM_CODE.value + '%'));
	dg_1.Retrieve(new Array(pCompanyCode,  S_SYS_ID.value + '%', pUserGroup, S_PGM_CODE.value + '%'));
	return 0;
}

//html input type 의 자려가 변경된 경우 발생
function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id) {
		case "S_PGM_CODE":
		case "S_SYS_ID":
			x_DAO_Retrieve2(dg_1);
			break;
	}
	return 0;
}

//enter key event 발생시 호출
function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	switch(a_obj.id) {
		case "S_PGM_CODE":
			x_DAO_Retrieve2(dg_1);
			break;
	}
	return 0;
}

//keydown event 발생시 호출
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){return 100;}

//데이타 저장
function x_DAO_Save2(a_dg){return 100;}

//데이타 저장후 callback
function x_DAO_Saved2(){return 100;}

//데이타 유효성 check
function x_DAO_ChkErr2(){return true;}

//grid insert/add
function x_DAO_Insert2(a_dg, row){return 100;}

//grid insert 수행후 발생
function x_Insert_After2(a_dg, rowIdx){return 100;}

//grid 복제
function x_DAO_Duplicate2(a_dg, row){return 100;}

//grid 복제후
function x_Duplicate_After2(a_dg, rowIdx){return 100;}

//grid 삭제
function x_DAO_Delete2(a_dg, row){return 100;}

//excel 저장
function x_DAO_Excel2(a_dg){return 100;}

//출력
function x_DAO_Print2(){return 100;}

//tab 변경시
function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}

//tab 변경시
function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}

//grid data 변경시
function xe_GridDataChange2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){return 100;}

//grid row focus change
function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){return true;}

//item focus change event
function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){return true;}

//grid data loaded
function xe_GridDataLoad2(a_dg){return 100;}

//찾기창 호출 후
function x_FindCode2(a_dg, a_row, a_findKey, a_findstr, a_win) {return 100;}

//찾기창 호출 후
function x_ReceivedCode2(){return 100;}

//grid item button click event
function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){return 100;}

//header click event
function xe_GridHeaderClick2(a_dg, a_col, a_colname){return 100;}

//item double click event
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){return 100;}

//tree item checked event
//하위 데이타 checked : false/true
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){
	var data = dg_1.GetItem(a_itemIndex,"PGM_CODE");
	if(data.length==2||data.length==4||data.length==6) {
		pf_SelectAll(a_itemIndex, data, data.length,a_checked);
	}
	return 100;
}

//tree item checked event
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){return 100;}

//- anychart  point click event
function xe_ChartPointClick2(a_dg, a_event){return 100;}

//- anychart  point select event
function xe_ChartPointSelect2(a_dg, a_event){return 100;}

//- anychart  point mouse over event
function xe_ChartPointMouseOver2(a_dg){return 100;}

//- anychart  point mouse out event
function xe_ChartPointMouseOut2(a_dg, a_event){return 100;}

//window(mdi) close
function x_Close2() {return 100;}

//전체선택/해제처리
function pf_SelectAll(startRow, data, length, flag) {
	for(var i = startRow+1; i <= dg_1.RowCount(); i++){
		var pgmcode = dg_1.GetItem(i,"PGM_CODE");
		if(length>pgmcode.length||data.substring(0,length)!=pgmcode.substring(0,length)) break;
		dg_1.SetCheckRows(i, flag);
	}
}

//선택된 프로그램에 대하여 parent 창에 insert 처리
function x_Confirm() {
	var checkedRows = dg_1.GetCheckedRows();
	if(checkedRows.length==0) {
		_X.MsgBox("체크된 프로그램 코드가 없습니다!");
		return;
	}
	
	//IE9 _Caller._X.DgFindItemValue오류로 수정(2015.07.17 KYY)
	_Caller.pf_AddNewAuth(window.dg_1);
/*
  for(var i=0;i<checkedRows.length;i++){
  	
  	var arr1 = new Array(); //저장전 동일한 중복추가시 이미 추가시킨것은 다시 추가해도 그대로인 상태로 해줌.. 2015-03-25 상규&성룡
  	var arr2 = new Array();
  	arr1.push("COMPANY_CODE");
  	arr1.push("USER_GROUP_CODE");
  	arr1.push("SYS_ID");
  	arr1.push("MENU_CODE");
  	arr2.push(String(pCompanyCode));
  	arr2.push(String(pUserGroup));
  	arr2.push(String(dg_1.GetItem(checkedRows[i]+1,"SYS_ID")));
  	arr2.push(String(dg_1.GetItem(checkedRows[i]+1,"MENU_CODE")));
  	//alert(_Caller.dg_child.SetFocusByValue(arr1, arr2));
  	//var res = _Caller.dg_child.SetFocusByValue(arr1, arr2);
  	_Caller._X.UnBlock();
  	var res = _Caller.x_FindItem(arr1, arr2);
  	//alert(res);
  	
  	if(res.flag) continue; //여기까지
  	
  	newrow = _Caller.dg_child.InsertRow(0);
  	_Caller.dg_child.SetItem(newrow,'COMPANY_CODE', pCompanyCode);
  	_Caller.dg_child.SetItem(newrow,'USER_GROUP_CODE', pUserGroup);   
  	_Caller.dg_child.SetItem(newrow,'SYS_ID', dg_1.GetItem(checkedRows[i]+1,"SYS_ID"));    
  	_Caller.dg_child.SetItem(newrow,'PGM_CODE', dg_1.GetItem(checkedRows[i]+1,"PGM_CODE"));    
  	_Caller.dg_child.SetItem(newrow,'PGM_NAME', dg_1.GetItem(checkedRows[i]+1,"PGM_NAME"));    
  	_Caller.dg_child.SetItem(newrow,'MENU_CODE', dg_1.GetItem(checkedRows[i]+1,"MENU_CODE"));    
  	_Caller.dg_child.SetItem(newrow,'MENU_NAME', dg_1.GetItem(checkedRows[i]+1,"PGM_NAME"));    
  	_Caller.dg_child.SetItem(newrow,'AUTH_I', "Y");
  	_Caller.dg_child.SetItem(newrow,'AUTH_R', "Y");
  	_Caller.dg_child.SetItem(newrow,'AUTH_D', "Y");
  	_Caller.dg_child.SetItem(newrow,'AUTH_P', "Y");
  	_Caller.dg_child.SetItem(newrow,'AUTH_E', "Y");
  	_Caller.dg_child.SetItem(newrow,'SORT_ORDER', i+1);
  }
*/  
  if(_IsModal || _IsPopup) {
		window.close();
	} else {
		if(_Caller.$("#findmodal").length>0?_Caller.$("#findmodal").dialog("close"):window.close());
	}
}

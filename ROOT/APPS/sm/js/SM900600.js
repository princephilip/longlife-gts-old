var ed_before;
var ed_after;
var mime = 'text/x-mariadb';
var strExecSql = "";
var qryTables; // 쿼리 테이블 목록
var qryColumns = []; // 쿼리 칼럼 목록

$(function() {
  //$(".tab-pane").css("overflow-y", "scroll");
  $("#pageLayout").css("overflow-y", "scroll");
  _X.UnBlock();

  // (sql - highlight)
  // get mime type 
  if (window.location.href.indexOf('mime=') > -1) {
    mime = window.location.href.substr(window.location.href.indexOf('mime=') + 5);
  }

  // tab sql - event 
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    if(typeof(ed_before)=="undefined") setEditor( "before" );
  });

  $("#btn-convert").click(function() {
    $('#btn-after').tab('show');
  });


  // tab grid - event 
  $("#grid-cols").change(function(e) {
    setColsProperties();
  });

  setEditor( "before" );

  $("#select-id").change(function(e) {
    $("#process-2").addClass("panel-info");
  });
});



// 0. editor Highliter 설정
var setEditor = function(aid) { 
  var editor = CodeMirror.fromTextArea(document.getElementById("qry-"+aid), {
    mode: mime,
    indentWithTabs: true,
    smartIndent: true,
    lineNumbers: true,
    matchBrackets : true,
    autofocus: true,
    extraKeys: {"Ctrl-Space": "autocomplete"},
    hintOptions: {tables: {
      users: {name: null, score: null, birthDate: null},
      countries: {name: null, population: null, size: null}
    }}
  });
  if (aid=="before") {
    editor.on("change", function(cm, change) {
      parsingParams(cm.getValue());
    })  
  };

  eval("ed_"+aid+" = editor");
}


// 1. sql에서 parameter 추출
var parsingParams = function(aval) {
  var patt = /:(\w+)/g; // [$|\s|\n|=|\)|,]
  var tempParams = [];
  var arrparam = new Array();

  if(aval!="") {
    // 기존 입력된 parameter 임시저장
    $(".col-parameter").each(function(idx, val) {
      var tObj = new Object();
      
      tObj.value = $(this).val();
      tObj.name = $(this).attr("id");

      tempParams.push(tObj);
    });

    // parsing parameter 
    if(patt.test(aval)) {
      arrparam = aval.match(patt);
    }

    var phtml = "";
    if(arrparam!=null && arrparam.length>0) {
      arrparam = _.uniq(arrparam);
      $.each(arrparam, function( index, value ) {
        var val_selected = ["selected", ""];
        var val_name = value.replace(":", "");
        var patt2 = /(ai|an)(\w+)/g;
        if(patt2.test(val_name)) val_selected = ["", "selected"];

        phtml += "<div class=\"form-horizontal\"><div class=\"form-group\">"
        phtml += "<label for=\""+val_name+"\" class=\"col-sm-3 control-label\">"+(index+1)+". "+value+"</label>"
        phtml += "<div class=\"col-sm-5\"><input type=\"text\" class=\"form-control col-parameter input-sm\" id=\""+val_name+"\" placeholder=\"\"></div>"
        phtml += "<div class=\"col-sm-4\"><select id=\"type_"+val_name+"\" class=\"input-sm\"><option value=\"String\" "+val_selected[0]+">String</option><option value=\"Integer\" "+val_selected[1]+">Integer</option></select></div>"
        phtml += "</div></div>"
      });
    }
    $("#sql-params").empty().append(phtml);
    $("#process-1").addClass("panel-info");

    $.each(tempParams, function(idx, val) {
      $("#"+tempParams[idx].name).val(tempParams[idx].value);
    });

    // parameter 입력되면 색바꾸기~
    $(".col-parameter").change(function(e) {
      var scnt = 0;
      $.each($("input.form-control.col-parameter"), function() {
        var tval = $(this).val();
        tval = tval.replace(/\s/g, "");
        if(tval=="") scnt++;
      });

      if(scnt==0) $("#process-3").addClass("panel-info");
    });
  }
}


// 3. update column 목록 세팅
var setUpdateColunms = function() {
  // parsing table name from query
  var patt = /(?:from|into|update|join)(?:\s+|\n+)(\w+)(?:\s+|\n+|$)/gi;
  var patt2 = /\(/g;
  var strqry = ed_before.getValue();
  var tables = strqry.match(patt);
  qryTables = [];
console.log(tables)
  $.each(tables, function(idx, val) {
    if(!patt2.test(val)) {
      var arrtbl = val.replace(/\n/, " ").trim().toUpperCase().split(" ");
      qryTables[idx] = arrtbl[arrtbl.length-1];

      $("#update-table").append( "<option value=\""+qryTables[idx]+"\" >"+qryTables[idx]+"</option>" );
    }
  });
  
  // parsing column name from query
  getColumnInfo(); // ajax

}


// 3. 칼럼정보 가져오기
var getColumnInfo = function() {
  convertParams();
  
  $.ajax({
    method: "POST",
    url: "/APPS/sm/jsp/SM900600_meta.jsp",
    dataType: "json",
    data: { query: strExecSql, tables: qryTables.join(',') },
    success: function( data, textStatus, jQxhr ){ 
      qryColumns = data.columns_info;

      setHtmlColumns();
    }
  });
}

// 3. 칼럼정보로 속성입력 html 생성
var setHtmlColumns = function() {
  var thtml = "";
  
  if(qryColumns.length<=0) {
    alert("쿼리 실행 결과가 없어 컬럼 정보를 읽어올 수 없습니다. 올바른 파라미터를 입력하세요.");
    return;
  }

  $("#update-columns").empty();

  // header 넣기
  thtml += "<tr>";
  thtml += "<th><button class=\"btn btn-warning btn-xs\" onclick=\"chkAllUdpcols()\">선택</button></th>";
  thtml += "<th>Name</th>";
  thtml += "<th>Type</th>";
  thtml += "<th>Updatable</th>";
  thtml += "<th>Iskeycol</th>";
  thtml += "</tr>";

  $.each(qryColumns, function(idx, val) {
    thtml += "<tr>";
    thtml += "<td><input id=\"chk-updcol-"+idx+"\" type=\"checkbox\" checked class=\"chk-update-col\"></td>";
    thtml += "<td><input id=\"upd-colname-"+idx+"\" type=\"text\" readonly value=\""+qryColumns[idx].columnName+"\" class=\"form-control input-sm \" /></td>";
    thtml += "<td><select id=\"upd-coltype-"+idx+"\" class=\"form-control input-sm\"><option value=\"char\">char</option><option value=\"int\">int</option><option value=\"date\">date</option></select></td>";
    thtml += "<td><select id=\"upd-updatable-"+idx+"\" class=\"form-control input-sm\"><option value=\"Y\">Y</option><option value=\"N\">N</option></select></td>";
    thtml += "<td><select id=\"upd-iskeycol-"+idx+"\" class=\"form-control input-sm\"><option value=\"Y\">Y</option><option value=\"N\">N</option></select></td>";
    thtml += "</tr>";
  });
  $("#update-columns").append(thtml);
  // location.hash = "#process-updatecols";
}


// 전체 선택 (반전) 로직 수정
var chkAllUdpcols = function() {
  $("input:checkbox.chk-update-col").each(function() {
    $(this).prop("checked", !$(this).prop("checked") );
  });
}


// 3. 실행가능 sql로 변환
var convertParams = function() {
  var strqry = ed_before.getValue();

  $.each($("input.form-control.col-parameter"), function() {
    var re = new RegExp(":"+$(this).attr("id"), "g");
    strqry = strqry.replace(re, "'"+$(this).val()+"'");
  });

  strExecSql = strqry;
}

// 4. xml용 sql로 변환
var convertXmlQuery = function(aCopy) {
  var strxml = "";
  var strparam = "";
  var struc = "";
  var strqry = ed_before.getValue();
  var tMaxLen = 0;

  $.each($("input.form-control.col-parameter"), function() {
    var re = new RegExp(":"+$(this).attr("id"), "g");
    strqry = strqry.replace(re, "#"+$(this).attr("id")+"#");
    strparam += "    <param name=\""+$(this).attr("id")+"\" type=\""+$("#type_"+$(this).attr("id")).val()+"\"/>\n";
  });

  $.each($("input:checkbox.chk-update-col"), function() {
    if($(this).is(":checked")) {
      var tId = $(this).attr("id");
      tId = tId.replace("chk-updcol-", "");
      var tName = $("#upd-colname-"+tId).val();
      struc += "    <column name='"+tName+"' #INDENT_BLANK_"+tName.length+"# type='"+$("#upd-coltype-"+tId).val()+"' updatable='"+$("#upd-updatable-"+tId).val()+"' iskeycol='"+$("#upd-iskeycol-"+tId).val()+"' />\n";
      if(tMaxLen<tName.length) tMaxLen = tName.length;
    }
  });

  // indent 처리
  var tBlank = "";
  for(var i=tMaxLen; i>=1; i--) {
    var patt = new RegExp("#INDENT_BLANK_"+i+"#", "g");
    struc = struc.replace(patt, tBlank);
    tBlank += " ";
  }

  strxml += "  <select id=\""+$("#select-id").val()+"\">\n";
  strxml += "    <sql><![CDATA[\n";
  strxml += strqry + "\n";
  strxml += "    ]]></sql>\n\n";
  strxml += strparam + "\n\n";
  strxml += "    <update_table>"+$("#update-table").val()+"</update_table>\n\n";
  strxml += struc + "\n\n";
  strxml += "  </select>\n";

  if(aCopy===true) copyToClipboard(strxml);
  $("#qry-xml").val(strxml).select();
  // location.hash = "#process-xml";
}


// grid 생성
var createGrid = function() {
  var tCols = "";
  $.each(qryColumns, function(idx, ele) {
    tCols += (tCols!=""?",":"") + qryColumns[idx].columnName;
  });

  $("#grid-cols").val(tCols);
  $('#tab-1 a:last').tab('show');
  setColsProperties();
}


// grid column properties 설정 메뉴 만들기
var setColsProperties = function() {
  var cols = $("#grid-cols").val();
  var thtml = "";
  var rId = 0;

  $("#grid-cols-setting").empty();
  if(qryColumns.length>0) {
    cols = new Array();
    $.each(qryColumns, function(idx, ele) {
      cols.push(qryColumns[idx].columnName);
    });
  } else {
    cols = cols.split(",");
  }

  // header 넣기
  thtml += "<tr>"
  thtml += "<th>No.</th>"
  for(var j in colsproperties) {
    var tr = colsproperties[j];
    thtml += "<th>"+tr.title+"</td>";
  }
  thtml += "</tr>";
    
  $("#grid-cols-setting").append(thtml);
  thtml = "";

  // body 넣기
  for(var i in cols) {
    if(cols[i].trim()!="") {
      rId = (parseInt(i)+1);
      thtml += "<tr>"
      thtml += "<td>"+rId+"</td>"

      for(var j in colsproperties) {
        var tr = colsproperties[j];
        if(tr.title=="fieldName") {
          thtml += "<td><input id=\""+tr.title+"_"+rId+"\" type=\"text\" value=\""+cols[i].trim().toUpperCase()+"\" class=\"form-control input-sm cols-field \" /></td>";
        } else {
          var vals = tr.value.split(",");
          if(vals.length>=2) {
            // select
            thtml += "<td><select id=\""+tr.title+"_"+rId+"\" class=\"f_"+tr.title+"\">";
            for(var k in vals) {
              thtml += "<option value=\""+vals[k]+"\" >"+vals[k]+"</option>";
            }
            thtml += "</select></td>";
          /*} else if(vals.length==2) {
            // check
            thtml += "<td><input type=\"checkbox\" id=\""+tr.title+"_"+rId+"\" class=\"form-control input-sm\">";*/
          } else {
            // input
            thtml += "<td><input id=\""+tr.title+"_"+rId+"\" type=\"text\" value=\""+tr.value+"\" class=\"form-control input-sm\" /></td>";
          }        
        }
      }
      thtml += "</tr>";
      
      $("#grid-cols-setting").append(thtml);
      thtml = "";
    }
  }

  // selector 선택 이벤트 추가
  $(".f_styles, .f_editor").change(function(e) {
    var tVal = $(this).val();
    var tId = $(this).attr("id").split("_");
    var tgtId = (tId[0]=="styles" ? "editor" : "styles") + "_" + tId[1];
    var patt_t = /text/;
    var patt_n = /number/;
    var patt_d = /date/;

    if(patt_t.test(tVal)) {
      $("#"+tgtId).val("_Editor_text");
    } else if(patt_n.test(tVal)) {
      $("#"+tgtId).val("_Editor_number");
    } else if(patt_d.test(tVal)) {
      $("#"+tgtId).val("_Editor_date");
    }
  });


  // dataType 기본 속성 넣어주기.
  if(qryColumns.length>0) {
    $.each(qryColumns, function(idx, ele) {
      var tType = "";
      var tStyle = "";
      var tEditor = "";
      var pattn = /(int|double|float|number)/i;
      var pattd = /(date|time)/i;

      if( pattn.test(qryColumns[idx].columnType) ) {
        tType = "number";
        tStyle = "_Styles_number";
        tEditor = "_Editor_number";
      } else if ( pattd.test(qryColumns[idx].columnType) ) {
        tType = "datetime";
        tStyle = "_Styles_date";
        tEditor = "_Editor_date";
      } else {
        tType = "text";
        tStyle = "_Styles_text";
        tEditor = "_Editor_text";
      }
      // text, number, datatime
      $("#dataType_"+(idx+1)).val( tType );
      $("#styles_"+(idx+1)).val( tStyle );
      $("#editor_"+(idx+1)).val( tEditor );
    });
  }
}


var setGridCode = function(aCopy) {
  var tcode = "";
  var tcols = $(".cols-field");
  var tCp = colsproperties;
  var tMaxLen1 = 0;
  var tMaxLen2 = 0;

  // fields_dg_1 - generate
  tcode += "var fields_dg_1 = [\n";
  for(var i=1; i<=tcols.length; i++) { 
    var tval1 = $("#"+tCp[0].title+"_"+i).val().trim();
    var tval2 = $("#"+tCp[1].title+"_"+i).val().trim();
    if(tMaxLen1<tval1.length) tMaxLen1 = tval1.length;

    tcode += "    {";
    tcode += tCp[0].title+": '"+tval1+"' #INDENT_BLANK1_"+tval1.length+"# , ";
    tcode += tCp[1].title+": '"+tval2+"' ";
    tcode += "}"+(i==tcols.length?"":",")+" \n";
  }
  tcode += "];\n\n";

  // columns_dg_1 - generate
  tcode += "var columns_dg_1 = [\n";
  var tT = "";
  var tV = "";

  for(var i=1; i<=tcols.length; i++) {
    tcode += "    {";
    for(var j=0; j<tCp.length; j++) {
      if(!tCp[j].onlyField) {
        tT = tCp[j].title;
        tV = $("#"+tCp[j].title+"_"+i).val().trim();

        // tCp[j].isoption=="true" 이면서 값이 없으면 패스
        if(!tCp[j].isoption||tV!="") {
          // field 이름에 따른 설정
          if(tT=="fieldName") {
            tV = " '"+tV+"'" + (tV.length>0 ? " #INDENT_BLANK1_"+tV.length+"#" : "");
          } else if(tT=="width") {
            tV = (tV.length<5?"#BLANK_"+(4-tV.length)+"#":"") + (tV==""?"0":tV);
          } else if(tT=="header") {
            if(tMaxLen2<tV.length) tMaxLen2 = tV.length;
            tV = " {text: '"+tV+"'}" + (tV.length>0 ? " #INDENT_BLANK2_"+tV.length+"#" : "");
          } else if(tT=="styles"||tT=="editor") {
            // 그냥 출력..
          } else {
            // field 값에 따른 설정
            switch(tV) {
              case "true" :
              case "false" :
                tV = tV+(tV=="true"?" ":"");
                break;
              default :
                tV = "'"+tV+"'";
            }
          }
          tcode += tT+": "+tV+(j==tCp.length?" ":", ");
        }
      }
    }
    tcode += "#END_PROPERTIES#}"+(i==tcols.length?"":",")+" \n";
  }

  tcode = tcode.replace(/, #END_PROPERTIES#/g, "");

  // indent 처리
  var tBlank = "";
  for(var i=0; i<6; i++) {
    var patt = new RegExp("#BLANK_"+i+"#", "g");
    tcode = tcode.replace(patt, tBlank);
    tBlank += " ";
  }

  var tBlank = "";
  for(var i=tMaxLen1; i>=1; i--) {
    var patt = new RegExp("#INDENT_BLANK1_"+i+"#", "g");
    tcode = tcode.replace(patt, tBlank);
    tBlank += " ";
  }

  var tBlank = "";
  for(var i=tMaxLen2; i>=1; i--) {
    var patt = new RegExp("#INDENT_BLANK2_"+i+"#", "g");
    tcode = tcode.replace(patt, tBlank);
    tBlank += " ";
  }
  tcode += "];\n\n";

  if(aCopy===true) copyToClipboard(tcode);

  $("#grid-code").val(tcode).select();

  setFreeformHtml();
}

var setFreeformHtml = function() {
  var thtml = "";
  var tcols = $(".cols-field");
  var tCp = colsproperties;
  
  thtml += "  <div class=\"x5-ff\">\n";
  thtml += "    <div class=\"x5-row\">\n";
  for(var i=1; i<=tcols.length; i++) {
    var tfieldname = $("#fieldName_"+i).val().trim();
    var ttitle = $("#header_"+i).val().trim();
    thtml += "      <label for=\""+tfieldname+"\" class=\"x5-col-3 text-right\">"+ttitle+"</label>\n";
    thtml += "      <div class=\"x5-col-9\">\n";
    thtml += "        <input id=\""+tfieldname+"\" type='text' class='form-control input-sm' >\n";
    thtml += "      </div>\n";
  }

  thtml += "    </div><!-- .x-row -->\n";
  thtml += "  </div><!-- .x5-ff -->\n";

  $("#freeform-code").val(thtml).select();
}

var copyToClipboard = function(str) {
  var $temp = $("<textarea>");
  $("body").append($temp);
  $temp.val(str).select();
  document.execCommand("copy");
  $temp.remove();
}

// 컬럼 속성정보 배열 (나중에 버젼별로 ajax 처리)
var colsproperties = [
  {
    title: "fieldName",
    value: ""
  },
  {
    title: "dataType",
    onlyField: true,
    value: "text,number,datetime"
  },
  {
    title: "width",
    value: "0"
  },
  {
    title: "must_input",
    value: "N,Y"
  },
  {
    title: "visible",
    value: "true,false"
  },
  {
    title: "readonly",
    value: "true,false"
  },
  {
    title: "editable",
    value: "true,false"
  },
  {
    title: "sortable",
    value: "true,false"
  },
  {
    title: "header",
    value: ""
  },
  {
    title: "styles",
    value: "_Styles_text,_Styles_textc,_Styles_textr,_Styles_number,_Styles_numberc,_Styles_numberl,_Styles_numberf1,_Styles_numberf2,_Styles_numberf3,_Styles_numberf4,_Styles_date,_Styles_datetime,_Styles_checkbox,_Styles_tree"
  },
  {
    title: "editor",
    value: "_Editor_text,_Editor_multiline,_Editor_number,_Editor_numberf1,_Editor_numberf2,_Editor_numberf3,_Editor_numberf4,_Editor_date,_Editor_datetime,_Editor_checkbox,_Editor_dropdown"
  },
  {
    title: "mergeRule",
    value: "",
    isoption: true 
  },
  {
    title: "lookupDisplay",
    value: ",true,false,aaa",
    isoption: true 
  }
];
// isoption: true 이면 나중에 프로퍼티 필수로 넣지 않습니다.

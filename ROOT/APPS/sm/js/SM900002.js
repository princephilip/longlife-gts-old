/**
 *<pre>
 * Statements
 *</pre>

 * @Class Name : SM900002.js
 * @Descriptio   Grid 관리
 * @Modification Information
 * @
 * @  수정일     수정자      수정내용
 * @ -------    --------    ---------------------------
 * @ 2016.10.28     JH        최초 생성
  */
var curSys = "";
var curPgm = "";
var curGrid = "";
var mime = 'text/x-mariadb'; // codemirror mime type
var isSqlChanged = true; // SQL변경 자동 이벤트 막기
var vFieldRows = {};


function x_InitForm2(){
  _X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM900002", "SM900002|DEV_GRID_COLS", false, true);
  _X.InitGrid(grid_2, "dg_2", "100%", "100%", "sm", "SM900002", "SM900002|DEV_PGM_CODE", false, false);
  _X.InitGrid(grid_devpropsrc, "dg_devpropsrc", "100%", "100%", "sm", "SM900002", "SM900002|SM900002_GRIDSRC", false, false);
  $(".new-grid").hide();

  setEditor( "grid-sql" );

  // 수정SQL 적용 버튼
  $("#btn-modified-sql").click(function() {
    if( !$(this).hasClass("disabled") ) exeSql();
  });

  $("#btn-freeformModal").click(function() {
    $("#freeform-html").val("");
    var sHtml = "";
    var rSize = $("#S_FREEFORM_SIZE").val();
    var rRateSum = 0;


    if(rSize==null || rSize=="" || isNaN(rSize)) {
      alert("프리폼 사이즈를 입력해 주시기 바랍니다.");
      $("#S_FREEFORM_SIZE").focus();
      return;
    }
    rSize = parseInt(rSize);

    for(var i=0; i<dg_1.RowCount(); i++) {
      var rate = dg_1.GetItem(i, "FREEFORM_RATE");
      if(rate!=null && parseInt(rate) > 0) {
        rate = parseInt(rate);
          if(rRateSum==0) {
            sHtml += (sHtml!="" ? "\r\n" : "") + "<div class='detail_row'>";
          }
          sHtml += "\r\n\t<label class='detail_label w100'>" + dg_1.GetItem(i, "HEADER_TEXT") + "</label>"
                +  "\r\n\t<div class='detail_input_bg" + (rRateSum+rate>=12 ? "" : " w" + (rSize-200)*rate/12) + "'>"
                +  "\r\n\t\t<input id='" + dg_1.GetItem(i, "FIELDNAME") + "' type='text'>"
                +  "\r\n\t</div>"
                ;
          rRateSum += rate;
          if(rRateSum>=12) {
            sHtml += "\r\n</div>";
            rRateSum = 0;
          }
      }

    }
    $("#freeform-html").val(sHtml);
    $("#freeform-html").select();

  });

  $("#btn-freeformClose").click(function() {
    $("#freeform-html").val("");
  });


  $("#btn-copyModal").click(function() {
    if($("#S_TO_PGMCODE").val()=="") {
      _X.Noty("프로그램코드를 입력해 주세요");
      $("#S_TO_PGMCODE").focus();
      return;
    }
    if(curGrid==null || curGrid=="") {
      _X.Noty("선택된 그리드가 없습니다");
      return;
    }

    var param = ($("#S_TO_GRIDID").val()=="" ? new Array(curPgm, $("#S_TO_PGMCODE").val()) :  new Array(curPgm, $("#S_TO_PGMCODE").val(), curGrid, $("#S_TO_GRIDID").val()) );

    _X.ExecProc("sm", "SP_SM_DEV_GRID_COPY", param);
    $('#copyModal').modal('hide');
      _X.Noty("그리드 복사가 완료되었습니다.");

    $("#S_PGM_CODE").val($("#S_TO_PGMCODE").val());

  });

  $("#btn-copyClose").click(function() {
    $("#S_TO_PGMCODE").val("");
    $("#S_TO_GRIDID").val("");
  });

  $("#btn-upd-modal").click(function() {
    generateUpdXml();
  });

  $("#btn-updModal").click(function() { alert($("#upd-xml-txt").val())
    copyToClipboard( $("#upd-xml-txt").val() );
  });

  $("#btn-del-modal").click(function() {
    if(curGrid==null || curGrid=="") {
      _X.Noty("선택된 그리드가 없습니다");
      return;
    }

    if(_X.MsgBoxYesNo('확인', curGrid + ' 그리드 정보를 삭제 하시겠습니까?')=='2'){
      return;
    }

    _X.ExecProc("sm", "SP_SM_DEV_GRID_DEL", [curPgm, curGrid]);
    pf_setGridIds(curSys, curPgm );
    _X.Noty("그리드 삭제가 완료되었습니다.");
  });

  return 100;
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
  if(ab_allcompleted) {
    var vBool = [{code:'true', label:'true'}, {code:'false', label:'false'}];
    var vUpdType = [{code:'char', label:'char'}, {code:'number', label:'number'}, {code:'date', label:'date'}];

    // dg_1.SetCombo("VISIBLE",       vBool);       // 보이기
    // dg_1.SetCombo("READONLY",      vBool);       // 보이기
    // dg_1.SetCombo("EDITABLE",      vBool);       // 보이기
    // dg_1.SetCombo("SORTABLE",      vBool);       // 보이기
    // dg_1.SetCombo("MERGE",         vBool);       // 보이기
    // dg_1.SetCombo("RESIZABLE",     vBool);       // 보이기
    // dg_1.SetCombo("LOOKUPDISPLAY", vBool);       // 보이기

    dg_1.SetCombo("UPDATE_TYPE",  vUpdType);       // 보이기

    dg_1.SetCombo("STYLES",       gvStyles);       // 보이기
    dg_1.SetCombo("EDITOR",       gvEditor);       // 보이기
  }
  return 100;
}

function x_DAO_Retrieve2(a_dg){
  dg_2.Retrieve(new Array(S_SYS_ID.value, '%'+S_PGM_CODE.value+'%'));

  return 0;
}

//html input type 의 자려가 변경된 경우 발생
function xe_EditChanged2(a_obj, a_val){
}

//enter key event 발생시 호출
function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
  if(a_obj==S_PGM_CODE) {
    x_DAO_Retrieve();
  }

  return 0;
}

//keydown event 발생시 호출
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){return 100;}

//데이타 저장
function x_DAO_Save2(a_dg){return 100;}

//데이타 저장후 callback
function x_DAO_Saved2(){
  //Grid Script 생성(2016.11.24 KYY)
	_X.ExecProc("sm", "SP_SM_DEV_GRID_SCRIPT", new Array(curSys, curPgm));

  $(".new-grid").hide();
  $("#S_GRID_ID").val("");

  //Grids 버튼 리프레시
  pf_setGridIds(curSys, curPgm);

  return 100;
}

//데이타 유효성 check
function x_DAO_ChkErr2(){
  if($("#S_GRID_ID").val()!="") {
    curGrid = $("#S_GRID_ID").val();
  }
  //max값 부여
  var cData1    = dg_1.GetChangedData();
  var grid1_max = _X.XmlSelect("sm", "SM900002", "DEV_GRID_COLS_MAX_SEQ" , new Array(curSys, curPgm, curGrid) , "array");
  for (i=0 ; i < cData1.length; i++){
    if(cData1[i].job == 'I'){
      dg_1.SetItem(cData1[i].idx, 'SEQ', String(++grid1_max[0][0]) ) ;
    }

  }

  var params = new Array(curSys, curPgm, curGrid, $("#S_SQL_FILE").val(), $("#S_SQL_ID").val(), $("#S_UPDATE_TABLE").val());
  //SP로 변경 (2017.01.07 KYY)
  _X.ExecProc("sm", "SP_SM_DEV_GRID_SQL", params);

  return true;
}

//grid insert/add
function x_DAO_Insert2(a_dg, row){return 100;}

//grid insert 수행후 발생
function x_Insert_After2(a_dg, rowIdx){
  if(a_dg==dg_1) {
    dg_1.SetItem(rowIdx, "SYS_ID", curSys);
    dg_1.SetItem(rowIdx, "PGM_CODE", curPgm);
    dg_1.SetItem(rowIdx, "GRID_ID", $("#S_GRID_ID").val());
  }
  return 0;
}

//grid 복제
function x_DAO_Duplicate2(a_dg, row){return 100;}

//grid 복제후
function x_Duplicate_After2(a_dg, rowIdx){
  a_dg.SetItem(rowIdx, "SEQ", "0");
  return 0;
}

//grid 삭제
function x_DAO_Delete2(a_dg, row){return 100;}

//excel 저장
function x_DAO_Excel2(a_dg){return 100;}

//출력
function x_DAO_Print2(){
  return 100;
}

//tab 변경시
function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}

//tab 변경시
function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}

//grid data 변경시
function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){

  if(a_col=="FIELD_SEQ") {
    a_dg.Sort([{"sortAsc": true,"sortCol":{"field":a_col,"dataType":"number"}}]);
    for(var i=1; i<=dg_1.RowCount(); i++) {
      if(dg_1.GetItem(i, "FIELD_SEQ") != i * 10) {
        dg_1.SetItem(i, "FIELD_SEQ", i * 10);
      }
    }
  }

  return 0;
}

//grid row focus change
function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
  if(a_dg==dg_2 && a_newrow>0) {
    $(".new-grid").hide();

    curSys = dg_2.GetItem(a_newrow, "SYS_ID");
    curPgm = dg_2.GetItem(a_newrow, "PGM_CODE");
    pf_setGridIds(curSys, curPgm );
  }

  return true;
}

//item focus change event
function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){return true;}

//grid data loaded
function xe_GridDataLoad2(a_dg){
  return 100;
}

//찾기창 호출 후
function x_FindCode2(a_dg, a_row, a_findKey, a_findstr, a_win) {return 100;}

//찾기창 호출 후
function x_ReceivedCode2(){
}

//grid item button click event
function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
  alert(a_colname);
  return 100;
}

//header click event
function xe_GridHeaderClick2(a_dg, a_col, a_colname){return 100;}

//item double click event
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){return 100;}

//tree item checked event
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){return 100;}

//tree item checked event
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){return 100;}

//- anychart  point click event
function xe_ChartPointClick2(a_dg, a_event){return 100;}

//- anychart  point select event
function xe_ChartPointSelect2(a_dg, a_event){return 100;}

//- anychart  point mouse over event
function xe_ChartPointMouseOver2(a_dg){return 100;}

//- anychart  point mouse out event
function xe_ChartPointMouseOut2(a_dg, a_event){return 100;}

//window(mdi) close
function x_Close2() {return 100;}

// Pgmcode에 포함된 Grid ID 가져와서 선택버튼 추가
var pf_setGridIds = function(aSysID, aPgm) {
  var ls_res = _X.XmlSelect('sm', 'SM900002', 'DEV_PGM_CODE', new Array('%', aPgm), 'json2');
  // console.log(ls_res);
  $(".wrap-btn-grids").html("");
  var la_grids = ls_res[0].grid_ids.split(",");

  if(la_grids.length>=1) {
    $(la_grids).each(function(i, e) {
      if(e!="") $(".wrap-btn-grids").append( '<button id="gridbtn_'+e+'" class="btn btn-xs '+(curGrid==e?'btn-warning':'btn-info')+' btn-grid-id">'+e+'</button>' );
    });

    // New Grid Btn
    $(".wrap-btn-grids").append( '<button id="btn-grid-new" class="btn btn-xs btn-danger">New Grid</button>' );

    $(".btn-grid-id").off().on("click", function() {
      pf_selectGridId( aSysID, aPgm, $(this).attr("id") );
    });

    $("#btn-grid-new").off().on("click", function() {
      pf_addGrid();
    });

    // 첫번째 버튼 클릭
    if($("#gridbtn_"+curGrid).length>0) {
      $("#gridbtn_"+curGrid).trigger('click');
    } else {
      $(".wrap-btn-grids .btn:first").trigger('click');
    }

    if($(".wrap-btn-grids .btn").length==1) dg_1.Reset();
  }
}

// Grid Id 선택
var pf_selectGridId = function(aSysID, aPgm, aGid) {
  // 기존 Grid 선택시
  aGid = aGid.replace("gridbtn_", "");
  curGrid = aGid;

  $("#S_GRID_ID").val(aGid);
  $(".wrap-btn-grids .btn").not(".btn-danger").removeClass("btn-warning").addClass("btn-info");

  dg_1.Retrieve(new Array(aSysID, aPgm, aGid));
  $("#gridbtn_"+aGid).not(".btn-danger").removeClass("btn-info").addClass("btn-warning");

  // Master 에서 SQL file 정보 가져오기
  var ls_res = _X.XmlSelect('sm', 'SM900002', 'DEV_GRID', new Array(curSys, aPgm, aGid), 'json2');

  var la_sql_file = "";
  var la_sql_id = "";
  var la_upd_tbl = "";

  if(typeof(ls_res[0])!="undefined") {
    la_sql_file = ls_res[0].sql_file;
    la_sql_id = ls_res[0].sql_id;
    la_upd_tbl = ls_res[0].update_table;
  }

  $("#grid-sql").val("");
  $("#S_SQL_FILE").val(la_sql_file);
  $("#S_SQL_ID").val(la_sql_id);
  $("#S_UPDATE_TABLE").val(la_upd_tbl);
}

// New Grid 선택
var pf_addGrid = function() {
  dg_1.Reset();
  $(".new-grid").show();
  $("#S_GRID_ID").val("");
}

// editor Highliter 설정
var setEditor = function(aid) {
  var editor = CodeMirror.fromTextArea(document.getElementById(aid), {
    mode: mime,
    indentWithTabs: true,
    smartIndent: true,
    lineNumbers: true,
    matchBrackets : true,
    autofocus: false,
    extraKeys: {"Ctrl-Space": "autocomplete"},
    hintOptions: {tables: {
      users: {name: null, score: null, birthDate: null},
      countries: {name: null, population: null, size: null}
    }}
  });

  eval("ed_"+aid.replace(/\-/g,"")+" = editor");

  // editor.on("blur", function(){
  //   resizePanelHeight("CLOSE");
  // });
  // editor.on("focus", function(){
  //   resizePanelHeight("OPEN");
  // });
  editor.on("change", function(){
    changedQuery();
  });
}

// 쿼리 수정됐을 때,
var changedQuery = function() {
  if(isSqlChanged===false) {
    isSqlChanged = true;
    return;
  }

  // SQL 실행버튼 활성화
  setButtonActive();

  // sql에서 Parameter 추출
  var tSql = ed_gridsql.getValue();
  parsingParams(tSql);
}

// SQL적용버튼 활성화
var setButtonActive = function(aStatus) {
  if(aStatus=="disabled") {
    $("#btn-modified-sql").removeClass("btn-info").addClass("disabled btn-default");
  } else {
    $("#btn-modified-sql").removeClass("disabled btn-default").addClass("btn-info");
  }
}

// : 로 시작하는 Params 를 ##으로 변환
var convertParams = function(aSql) {
  var strqry = aSql;
  var patt = /:(\w+)($|\s|\n|=|\)|,)/g;

  if(strqry!="") {
    strqry = strqry.replace(patt, "#$1#$2");
  }

  // isSqlChanged = false;
  // ed_gridsql.setValue(strqry);

  return strqry;
}

// sql에서 parameter 추출
var parsingParams = function(aVal) {
  var cSql = convertParams(aVal);

  var patt = /#(\w+)#/g;
  //var patt = /:(\w+)/g; // [$|\s|\n|=|\)|,]
  var tempParams = new Array();
  var arrparams = new Array();
  var strParams = "";

  if(cSql!="") {
    // parsing parameter
    if(patt.test(cSql)) {
      tempParams = cSql.match(patt);
    }

    if(tempParams!=null && tempParams.length>0) {
      tempParams = _.uniq(tempParams);
      $.each(tempParams, function(i, val) {
        var tPname = val.replace(/#/g, "");

        var tObj = new Object;
        tObj.name = tPname;
        tObj.type = "String";
        var tName = tObj.name.substring(0, 2);
        if(tName=="ai"||tName=="an") tObj.type = "Integer";
        arrparams.push(tObj);

      });
    }

    setParams(arrparams);
  }
}

// 배열에서 params 생성하기 (aPrs:object {name:"as_date", type:"String"})
var setParams = function(aPrs) {
  // 기존 입력된 파라미터값 저장
  var tmpParams = new Array;
  var tmpObj;
  var tId = "";
  $.each($("#sql-params .col-paramname"), function(idx, val) {
    tId = $(this).attr("id").replace("pname-", "");
    tmpObj = new Object;
    tmpObj.name = tId;
    tmpObj.type = $("#ptype-"+tId).val();
    tmpParams.push(tmpObj);
  });

  // 배열에서 새로운 파라미터 입력
  var tHtml = "";
  $("#sql-params").empty();
  var tidx = 0;

  if(aPrs.length>0) {
    $.each(aPrs, function(idx, val) {
      tidx = idx + 1;
      tHtml = "<tr>";
      tHtml += "<td> "+tidx+". </td>";
      tHtml += "<td>"+"<input id=\"pname-"+val.name+"\" readonly class=\"form-control input-sm col-paramname\" value=\""+val.name+"\"/>"+"</td>";
      tHtml += "<td>"+"<select id=\"ptype-"+val.name+"\" class=\"form-control\">"+getParsOption(val.type)+"</select>"+"</td>";
      // tHtml += "<td>"+"<input id=\"pval-"+val.name+"\" class=\"form-control input-sm col-paramval\" value=\"\"/>"+"</td>";
      tHtml += "</tr>";

      $("#sql-params").append(tHtml);
    });
  } else {
    tHtml = "<tr><td>-</td><td>-</td></tr>";
    $("#sql-params").append(tHtml);
  }

  // 기존 저장된 파라미터값 다시 넣어주기
  $.each(tmpParams, function(idx, val) {
    $("#ptype-"+val.name).val(val.type);
  });

}

var getParsOption = function(aVal) {
  var tOption = "";

  $.each(parstypes, function(idx, val) {
    tOption += "<option value=\""+val+"\" "+(aVal==val ? "selected" : "")+">"+val+"</option>";
  });

  return tOption;
}

// SQL 실행해서 칼럼정보 가져오기
var exeSql = function() {
  var tSql = setParamsToSql(); // 실행하기 위한 쿼리로 수정
  getUpdateTables(tSql, $("#update-table").val()); // 업데이트 테이블명 읽어오기

  // 쿼리 실행해서 Column Type 가져오기
  $.ajax({
    method: "POST",
    url: "/APPS/sm/jsp/SM900002_meta.jsp",
    dataType: "json",
    data: { query: tSql, tables: qryTables.join(',') },
    success: function( data, textStatus, jQxhr ){
      qryColumns = data.columns_info;
      setGridColsInfo( '', qryColumns );
      $("#4").modal("hide");
    },
    error:function(request, status, error){
      alert( "sql파일을 실행하는 중 오류가 발생했습니다."+$(request.responseText).find("#content_pop").html() );
    }
  });
}

// 실행하기 위해 SQL Query 에 parameter Value로 치환
var setParamsToSql = function() {
  var strqry = ed_gridsql.getValue();
  var tRow = 0;
  strqry = convertParams(strqry); //convertXmlQuery(strqry);

  $.each($(".col-paramname"), function() {
    var tn = $(this).attr("id").replace("pname-", "");
    var re = new RegExp("#"+tn+"#", "g");
    //strqry = strqry.replace(re, "'"+$(this).val()+"'"); 파라미터 값으로 수정
    strqry = strqry.replace(re, "''");
  });

  return strqry;
}


// query에서 Update Table 목록 가져오기
var getUpdateTables = function(aSql, aTbl) {
  // parsing table name from query
  var patt = /(?:from|into|update|join)(?:\s+|\n+)(\w+)(?:\s+|\n+|$)/gi;
  var patt2 = /\(/g;
  var strqry = aSql;
  var tables = strqry.match(patt);
  qryTables = [];

  $("#update-table").empty();
  $("#update-table").append( "<option value=\"\">없음</option>" );
  if(strqry&&tables) {
    $.each(tables, function(idx, val) {
      if(!patt2.test(val)) {
        var arrtbl = val.replace(/\n/, " ").trim().toUpperCase().split(" ");
        qryTables[idx] = arrtbl[arrtbl.length-1];

        $("#update-table").append( "<option value=\""+qryTables[idx]+"\" "+(aTbl&&aTbl==qryTables[idx]?"selected":"")+">"+qryTables[idx]+"</option>" );
      }
    });
  }
}


// grid Properties Setting (aVal 은 그리드 id, aObj 는 칼럼 Object)
// aVal만 있으면 Grid.js 파일의 내용을 읽고, aObj가 있으면 DB에서 쿼리 실행 후, 오는 것으로 간주..
var setGridColsInfo = function(aVal, aObj) {
  var cols = $("#grid-cols").val();
  var thtml = "";
  var rId = 0;
  var tSrc = "script"; /* script:프로그램 스크립트에서 읽어오기, sql:쿼리에서 칼럼정보 가져오기 */

  dg_1.Reset();
  // $("#grid-cols-setting").empty();
  // setGridColsHeader();

  var cObj = $("#S_GRID_ID").val();
  if(aVal) cObj = aVal;

  var tObj; // 그리크 칼럼 Object
  //if(!aObj) { // Grid.js 파일과 xmlx 파일에서 내용 읽어오기
    //tSrc = "script";
    //tObj = eval(cObj); // Grid.js 의 객체 직접 읽음.
  if(false){ // Grid.js 에서 읽어오는건 우선 안씀.(모두 DB에서 읽어와서 세팅)
    //
  } else { // DB실행한 결과 object에서 내용 읽어오기
    tSrc = "sql";
    tObj = aObj;
  }

  vFieldRows = {};
  var tRow = 0;
  var tColtxt = "@";
  //var tIsCol = false; // Update 정보와 칼럼 시작을 나눔. true 일때 칼럼영역 false 일때 업데이트영역
  $.each(tObj, function(idx, val) {
    rId = (parseInt(idx)+1);
    // thtml += "<tr class=\"tr-row-"+rId+" "+(tSrc=="sql" ? "notmatched" : "matched")+"\">"

    tRow = dg_1.InsertRow();
    var cIdx = rId;
    //dg_1.SetItem(tRow, "SYS_ID",           curSys.toUpperCase());
    //dg_1.SetItem(tRow, "PGM_CODE",         curPgm);
    //dg_1.SetItem(tRow, "GRID_ID",          cObj);
    dg_1.SetItem(tRow, "SEQ",              rId);
    dg_1.SetItem(tRow, "FIELD_SEQ",        rId * 10);
    dg_1.SetItem(tRow, "FIELDNAME",        val.columnName);
    // dg_2.SetItem(tRow, "WIDTH",            e.width);
    // dg_2.SetItem(tRow, "MUST_INPUT",       e.must_input);
    // dg_2.SetItem(tRow, "VISIBLE",          e.visible);
    // dg_2.SetItem(tRow, "READONLY",         e.readonly);
    // dg_2.SetItem(tRow, "EDITABLE",         e.editable);
    // dg_2.SetItem(tRow, "SORTABLE",         e.sortable);
    // dg_2.SetItem(tRow, "HEADER",           "{text: '"+e.header.text+"'}");
    // dg_2.SetItem(tRow, "HEADER_TEXT",      e.header.text);
    // dg_2.SetItem(tRow, "STYLES",           getStylesName(e.styles));
    // dg_2.SetItem(tRow, "EDITOR",           getEditorName(e.editor));
    // dg_2.SetItem(tRow, "LOOKUPDISPLAY",    e.lookupdisplay);
    // dg_2.SetItem(tRow, "RENDERER",         e.renderer);
    // dg_2.SetItem(tRow, "BUTTON",           e.button);
    // dg_2.SetItem(tRow, "ALWAYSSHOWBUTTON", e.alwaysShowButton);

    vFieldRows[val.columnName] = rId;
    tColtxt += val.columnName + "@";

    /* dg_1 - insert 로 대체
    for(var j in colsproperties) {
      var tr = colsproperties[j]; // 칼럼 프로퍼티 정보 소스 제일 하단에 변수 설정돼 있습니다.
      var vCurVal = "";

      if(tSrc=="script") { // DB에서 갖고 오면 값이 없음. 칼럼 정보(columnName, columnType)만 옴
        if(tr.title=="header") {
          eval("vCurVal = val."+tr.title+".text ;");
        } else {
          eval("vCurVal = val."+tr.title+";");
        }
      }

      if(tr.title=="fieldName") {
        tIsCol = true;
        thtml += "<td class=\"text-center w30\">"+rId+"</td>"
        thtml += "<td class=\""+(tr.className!=""?tr.className+" ":"")+"\"><input id=\""+tr.title+"_"+rId+"\" type=\"text\" value=\""+(tSrc=="script" ? vCurVal.toUpperCase() : val.columnName)+"\" class=\"form-control input-sm cols-field \" /></td>";
      } else {
        var vprops = tr.value.split(",");
        if(vprops.length==2&&(tr.value=="Y,N"||tr.value=="N,Y"||tr.value=="true,false"||tr.value=="false,true")) {
          // checkbox
          var tChkval = ""; var tUnChkval = "";
          if(vprops[0]=="Y"||vprops[0]=="N") {
            tChkval = "Y"; tUnChkval = "N";
          } else if (vprops[0]=="true"||vprops[0]=="false") {
            tChkval = "true"; tUnChkval = "false";
          } else {
            tChkval = vprops[0]; tUnChkval = vprops[1];
          }
          thtml += "<td class=\""+(tr.className!=""?tr.className+" ":"")+(tr.width!=""?"w"+tr.width:"")+"\"><input type=\"checkbox\" id=\""+tr.title+"_"+rId+"\" class=\"form-control input-sm\" "+(vCurVal&&(vCurVal.toString()==tChkval) ? "checked":"")+" value=\""+tChkval+"\" unchecked-value=\""+tUnChkval+"\">";
        } else if(vprops.length>=2) {
          // select
          thtml += "<td class=\""+(tr.className!=""?tr.className+" ":"")+(tr.width!=""?"w"+tr.width:"")+"\"><select id=\""+tr.title+"_"+rId+"\" class=\"form-control f_"+tr.title+"\">";
          var tmpObj;
          for(var k in vprops) {
            if(tr.proptype=="object") {
              eval("tmpObj = " + vprops[k]);
            } else {
              tmpObj = vprops[k];
              if(typeof(vCurVal)!="undefined") vCurVal = vCurVal.toString();
              //console.log(vCurVal, tmpObj)
            }
            thtml += "<option value=\""+vprops[k]+"\" "+((tSrc=="script"&&vCurVal==tmpObj)||(tSrc=="sql"&&tmpObj=="char") ? "selected":"")+">"+vprops[k]+"</option>";
          }
          thtml += "</select></td>";
        } else {
          // input
          thtml += "<td class=\""+(tr.className!=""?tr.className+" ":"")+(tr.width!=""?"w"+tr.width:"")+"\"><input id=\""+tr.title+"_"+rId+"\" type=\"text\" value=\""+(typeof(vCurVal)=="undefined" ? "":vCurVal)+"\" class=\"form-control input-sm\" /></td>";
        }
      }
    }
    thtml += "</tr>";

    $("#grid-cols-setting").append(thtml);
    thtml = "";
    */

  });

  //// update table 정보 넣어주기
  //if(tSrc=="script") {
  //  getXmlQuery(cObj); // Xml 파일에서 Update 정보 읽어오고 넣어주기
  //} else { // DB실행한 결과 object에서 내용 읽어오기
  // DB에서 실행했을 때 이전에 다른 프로그램 속성 설정값 읽어오기--
  //var tColobjs = $(".row-2 input.cols-field");
  //$.each(tColobjs, function(idx, val) {
  var param = new Array (curSys, curPgm, tColtxt);
  dg_devpropsrc.Retrieve(param);

  // 이전에 설정값 참고해서 그리드 프로퍼티 목록에 값 세팅
  setGridPropFromSrc();
  //}

  // setColorColsprop();
  //$("select.form-control,input[type=checkbox].form-control").on("change", function() {
  //  setColorColsprop();
  //});

  /*
  $(".row-2 .form-control:not(#update-table)").on("focus", function() {
    $(".property-wrap .tab-content").css({height: "auto"});
  }).on("blur", function() {
    $(".property-wrap .tab-content").css({height: "200px"});
  }).on("change", function() {
    var tId = $(this).attr("id");
    tId = tId.split("_");

    $("tr.tr-row-"+tId[tId.length-1]).removeClass("matched").addClass("notmatched");
  });
  */

  $('#sqlModal').modal('hide');
}

/*
//  grid Properties Setting 헤더 넣기
var setGridColsHeader = function() {
  var thtml = "";
  var ttitle = "";
  // header 넣기
  thtml += "<tr>"
  for(var j in colsproperties) {
    var tr = colsproperties[j];
    if(tr.title=="fieldName") thtml += "<th class=\"text-center\">No.</th>";
    ttitle = tr.title;
    if(ttitle.indexOf("_")>=0) ttitle = ttitle.replace("_","<br/>");
    else if(ttitle=="lookupDisplay") ttitle = "lookup<br/>Display";
    else if(ttitle=="alwaysShowButton") ttitle = "always<br/>ShowButton";
    thtml += "<th class=\"text-center "+(tr.className!=""?tr.className+" ":"")+(tr.width!=""?"w"+tr.width:"")+"\">"+ttitle+"</td>";
  }
  thtml += "</tr>";

  $("#grid-cols-setting").append(thtml);
}
*/

// 그리드 속성값을 Prop Src 데이터 윈도우에서 세팅하기
var setGridPropFromSrc = function() {
  var rcnt = dg_devpropsrc.RowCount();
  var tFname = "";
  var tRow = 0;
  var fieldcnt = dg_1.RowCount();

  // propsrc 데이터 윈도우 속성값을 위 Html 영역에 세팅
  for(var i=1; i<=rcnt; i++) {
    tFname = dg_devpropsrc.GetItem(i, "FIELDNAME");
    tRow = vFieldRows[tFname];

    dg_1.SetItem(tRow, "UPDATE_TYPE",     (dg_devpropsrc.GetItem(i, "STYLES").indexOf("number")!=-1 ? "number":"char"));
    dg_1.SetItem(tRow, "UPDATABLE",       "Y");
    dg_1.SetItem(tRow, "UPDATE_ISKEY",    "N");

    dg_1.SetItem(tRow, "WIDTH",            dg_devpropsrc.GetItem(i, "WIDTH"));
    dg_1.SetItem(tRow, "MUST_INPUT",       dg_devpropsrc.GetItem(i, "MUST_INPUT"));
    dg_1.SetItem(tRow, "VISIBLE",          dg_devpropsrc.GetItem(i, "VISIBLE"));
    dg_1.SetItem(tRow, "READONLY",         dg_devpropsrc.GetItem(i, "READONLY"));
    dg_1.SetItem(tRow, "EDITABLE",         dg_devpropsrc.GetItem(i, "EDITABLE"));
    dg_1.SetItem(tRow, "SORTABLE",         dg_devpropsrc.GetItem(i, "SORTABLE"));
    dg_1.SetItem(tRow, "HEADER",           dg_devpropsrc.GetItem(i, "HEADER"));
    dg_1.SetItem(tRow, "HEADER_TEXT",      dg_devpropsrc.GetItem(i, "HEADER_TEXT"));
    dg_1.SetItem(tRow, "STYLES",           dg_devpropsrc.GetItem(i, "STYLES"));
    dg_1.SetItem(tRow, "EDITOR",           dg_devpropsrc.GetItem(i, "EDITOR"));
    dg_1.SetItem(tRow, "LOOKUPDISPLAY",    dg_devpropsrc.GetItem(i, "LOOKUPDISPLAY"));
    dg_1.SetItem(tRow, "RENDERER",         dg_devpropsrc.GetItem(i, "RENDERER"));
    dg_1.SetItem(tRow, "BUTTON",           dg_devpropsrc.GetItem(i, "BUTTON"));
    dg_1.SetItem(tRow, "ALWAYSSHOWBUTTON", dg_devpropsrc.GetItem(i, "ALWAYSSHOWBUTTON"));
    dg_1.SetItem(tRow, "HEADERTOOLTIP",    dg_devpropsrc.GetItem(i, "HEADERTOOLTIP"));
    dg_1.SetItem(tRow, "MERGE",            dg_devpropsrc.GetItem(i, "MERGE"));
    dg_1.SetItem(tRow, "MAXLENGTH",        dg_devpropsrc.GetItem(i, "MAXLENGTH"));
    dg_1.SetItem(tRow, "FOOTEREXPR",       dg_devpropsrc.GetItem(i, "FOOTEREXPR"));
    dg_1.SetItem(tRow, "FOOTERALIGN",      dg_devpropsrc.GetItem(i, "FOOTERALIGN"));
    dg_1.SetItem(tRow, "FOOTERSTYLE",      dg_devpropsrc.GetItem(i, "FOOTERSTYLE"));
    dg_1.SetItem(tRow, "FOOTERPRE",        dg_devpropsrc.GetItem(i, "FOOTERPRE"));
    dg_1.SetItem(tRow, "FOOTERPOST",       dg_devpropsrc.GetItem(i, "FOOTERPOST"));
    dg_1.SetItem(tRow, "RESIZABLE",        dg_devpropsrc.GetItem(i, "RESIZABLE"));
    //tTblrow = tTblrow[tTblrow.length-1];
    //$("tr.tr-row-"+tTblrow).removeClass("notmatched").addClass("matched");
    /*
    $.each(colsproperties, function(idx, val) {
      // 칼럼값 세팅..
      if(idx>3) {
        if($("#"+val.title+"_"+tTblrow).is(":checkbox")) {
          if($("#"+val.title+"_"+tTblrow).attr("value") == dg_devpropsrc.GetItem(i, val.title.toUpperCase() )) {
            $("#"+val.title+"_"+tTblrow).prop("checked", true);
          }
        } else {
          $("#"+val.title+"_"+tTblrow).val( dg_devpropsrc.GetItem(i, val.title.toUpperCase() ) ) ;
        }
      }
    });
    */
  }
//FIELD_SEQ
  // propsrc 에 없는 항목(Width 없으면) 기본값으로 세팅
  for(var i=1; i<=fieldcnt; i++) {
    //if(dg_1.GetItem(i, "WIDTH")=="") {
    if(dg_1.GetItem(i, "UPDATE_TYPE")=="") {  
      var tFname = dg_1.GetItem(i, "FIELDNAME");

      dg_1.SetItem(i, "UPDATE_TYPE",     (getBestProp("STYLES", tFname).indexOf("number")!=-1 ? "number":"char"));
      dg_1.SetItem(i, "UPDATABLE",       "Y");
      dg_1.SetItem(i, "UPDATE_ISKEY",    "N");

      dg_1.SetItem(i, "WIDTH",            100);
      dg_1.SetItem(i, "VISIBLE",          "Y");
      dg_1.SetItem(i, "EDITABLE",         "Y");
      dg_1.SetItem(i, "SORTABLE",         "Y");
      dg_1.SetItem(i, "HEADER_TEXT",      dg_1.GetItem(i, "FIELDNAME"));
      dg_1.SetItem(i, "STYLES",           getBestProp("STYLES", tFname));
      dg_1.SetItem(i, "EDITOR",           getBestProp("EDITOR", tFname));
      dg_1.SetItem(i, "RESIZABLE",        "Y");
    }
  }
}

function getBestProp(aProp, aColName) {
  var pNumber = /number|NUMBER|amt|AMT|amount|AMOUNT|sum|SUM|avg|AVG|price|PRICE|cost|COST/;
  var pDate = /date|DATE/;
  var rType = "text";

  if(pNumber.test(aColName)) {
    rType = "number";
  } else if (pDate.test(aColName)) {
    rType = "date";
  }

  if(aProp=="EDITOR") {
    return "_Editor_"+rType;
  } else {
    return "_Styles_"+rType;
  }
}

function generateUpdXml() {
  $("#upd-xml-txt").val();

  var li_row = dg_1.RowCount();
  var ls_xml = [];
  var tMaxLen = 0;

  for(var i=1; i<=li_row; i++) {
    var tName = dg_1.GetItem(i, "FIELDNAME");
    var tCol  = dg_1.GetItem(i, "UPDATE_TYPE") || "char";
    var tUdp  = dg_1.GetItem(i, "UPDATABLE") || "N";
    var tKey  = dg_1.GetItem(i, "UPDATE_ISKEY") || "N";

    var tSrc = "    <column name='"+tName+"' #INDENT_BLANK_"+tName.length+"# type='"+tCol+"' updatable='"+tUdp+"' iskeycol='"+tKey+"' />\n";
    ls_xml.push( tSrc );
    if(tMaxLen<tName.length) tMaxLen = tName.length;
    /*
    if($(this).is(":checked")) {
      var tId = $(this).attr("id");
      tId = tId.replace("chk-updcol-", "");
      var tName = $("#upd-colname-"+tId).val();
      struc += "    <column name='"+tName+"' #INDENT_BLANK_"+tName.length+"# type='"+$("#upd-coltype-"+tId).val()+"' updatable='"+$("#upd-updatable-"+tId).val()+"' iskeycol='"+$("#upd-iskeycol-"+tId).val()+"' />\n";
      if(tMaxLen<tName.length) tMaxLen = tName.length;
    }
    */
  }

  var ls_rtn = ls_xml.join('');

  // indent 처리
  var tBlank = "";
  for(var i=tMaxLen; i>=1; i--) {
    var patt = new RegExp("#INDENT_BLANK_"+i+"#", "g");
    ls_rtn = ls_rtn.replace(patt, tBlank);
    tBlank += " ";
  }

  $("#upd-xml-txt").val( ls_rtn );
}

// 클립보드 복사
var copyToClipboard = function(str) {
  var $temp = $("<textarea>");
  $("body").append($temp);
  $temp.val(str).select();
  document.execCommand("copy");
  $temp.remove();
}


/* ****************************************************************************************** */
// 파라미터 타입 배열
var parstypes = ["String", "Integer"];

var gvStyles = [
  {code: '_Styles_text', label: '_Styles_text'},
  {code: '_Styles_textc', label: '_Styles_textc'},
  {code: '_Styles_textr', label: '_Styles_textr'},
  {code: '_Styles_seq', label: '_Styles_seq'},
  {code: '_Styles_number', label: '_Styles_number'},
  {code: '_Styles_numberc', label: '_Styles_numberc'},
  {code: '_Styles_numberl', label: '_Styles_numberl'},
  {code: '_Styles_numberf1', label: '_Styles_numberf1'},
  {code: '_Styles_numberf2', label: '_Styles_numberf2'},
  {code: '_Styles_numberf3', label: '_Styles_numberf3'},
  {code: '_Styles_numberf4', label: '_Styles_numberf4'},
  {code: '_Styles_date', label: '_Styles_date'},
  {code: '_Styles_datemonth', label: '_Styles_datemonth'},
  {code: '_Styles_datetime', label: '_Styles_datetime'},
  {code: '_Styles_hhmm', label: '_Styles_hhmm'},
  {code: '_Styles_checkbox', label: '_Styles_checkbox'},
  {code: '_Styles_tree', label: '_Styles_tree'}
];

var gvEditor = [
  {code: '_Editor_text', label: '_Editor_text'},
  {code: '_Editor_number', label: '_Editor_number'},
  {code: '_Editor_numberf1', label: '_Editor_numberf1'},
  {code: '_Editor_numberf2', label: '_Editor_numberf2'},
  {code: '_Editor_numberf3', label: '_Editor_numberf3'},
  {code: '_Editor_numberf4', label: '_Editor_numberf4'},
  {code: '_Editor_date', label: '_Editor_date'},
  {code: '_Editor_datemonth', label: '_Editor_datemonth'},
  {code: '_Editor_datetime', label: '_Editor_datetime'},
  {code: '_Editor_hhmm', label: '_Editor_hhmm'},
  {code: '_Editor_checkbox', label: '_Editor_checkbox'},
  {code: '_Editor_dropdown', label: '_Editor_dropdown'}
];

/*
// 컬럼 속성정보 배열 (나중에 버젼별로 ajax 처리) - proptype 는 변수의 타입(text, object)
var colsproperties = [
  {
    title: "update_type",
    value: "char,number",
    proptype: "text",
    width: "",
    isprop: false ,
    className: "bg-update"
  },
  {
    title: "updatable",
    value: "Y,N",
    proptype: "text",
    width: "",
    isprop: false ,
    className: "bg-update"
  },
  {
    title: "update_iskey",
    value: "N,Y",
    proptype: "text",
    width: "",
    isprop: false ,
    className: "bg-update"
  },
  {
    title: "fieldName",
    value: "",
    proptype: "text",
    width: "",
    className: ""
  },
  {
    title: "width",
    value: "0",
    proptype: "text",
    width: "50",
    className: ""
  },
  {
    title: "must_input",
    value: "N,Y",
    proptype: "text",
    width: "",
    className: ""
  },
  {
    title: "visible",
    value: "true,false",
    proptype: "text",
    width: "",
    className: ""
  },
  {
    title: "readonly",
    value: "true,false",
    proptype: "text",
    width: "",
    className: ""
  },
  {
    title: "editable",
    value: "true,false",
    proptype: "text",
    width: "",
    className: ""
  },
  {
    title: "sortable",
    value: "true,false",
    proptype: "text",
    width: "",
    className: ""
  },
  {
    title: "header",
    value: "",
    proptype: "text",
    width: "",
    className: ""
  },
  {
    title: "styles",
    value: "_Styles_text,_Styles_textc,_Styles_textr,_Styles_number,_Styles_numberc,_Styles_numberl,_Styles_numberf1,_Styles_numberf2,_Styles_numberf3,_Styles_numberf4,_Styles_date,_Styles_datetime,_Styles_checkbox,_Styles_tree",
    proptype: "object",
    width: "",
    className: ""
  },
  {
    title: "editor",
    value: "_Editor_text,_Editor_multiline,_Editor_number,_Editor_numberf1,_Editor_numberf2,_Editor_numberf3,_Editor_numberf4,_Editor_date,_Editor_datetime,_Editor_checkbox,_Editor_dropdown",
    proptype: "object",
    width: "",
    className: ""
  },
  {
    title: "lookupDisplay",
    value: ",true,false,aaa",
    isoption: true ,
    width: "",
    className: ""
  },
  {
    title: "renderer",
    value: ",true,false,aaa",
    isoption: true ,
    width: "",
    className: ""
  },
  {
    title: "button",
    value: ",action",
    isoption: true ,
    proptype: "text",
    width: "80",
    className: ""
  },
  {
    title: "alwaysShowButton",
    value: ",true,false",
    isoption: true ,
    proptype: "text",
    width: "80",
    className: ""
  }
];
*/
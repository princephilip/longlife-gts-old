/**
 *<pre>
 * Statements
 *</pre>

 * @Class Name : SM0903.js
 * @Descriptio	  메뉴목록관리
 * @Modification Information
 * @
 * @  수정일        		 수정자                   수정내용
 * @ -------    --------    ---------------------------
 * @ 2013.03.03    박영찬        최초 생성
 * @ 2013.05.26    박종현				 프로그램명, 정렬코드 변경시 메뉴그룹에 업데이트
 */

var ls_up_Array = new Array();

function x_InitForm2(){
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM0903", "SM_AUTH_PGMCODE|C_SM0903_01");
	return 100;
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		var	comboData  = _X.XmlSelect("sm", "SM_AUTH_SYS", "R_SM0903_01", new Array(mytop._CompanyCode), "json2");
		var comboData2 = _X.XmlSelect("com", "COMMON", "SM_COMCODE_D", new Array("SM", "PGM_URL"), "json2");
		//[{"code":"/XG010.do"   , "label":"/XG010.do"}, {"code":"/XG_GRID1.do", "label":"/XG_GRID1.do"}, {"code":"/XG_SHARE.do", "label":"/XG_SHARE.do"}, {"code":"/XG_GRID2.do", "label":"/XG_GRID2.do"}, {"code":"/XG_FORM.do" , "label":"/XG_FORM.do"}];
		//Combo 데이타 설정
		dg_1.SetCombo("SYS_ID", comboData);				//시스템구분
		dg_1.SetCombo("PGM_URL", comboData2);			//template 유형
		setTimeout('x_DAO_Retrieve(dg_1)',0);
	}
	return 100;
}

function x_DAO_Retrieve2(a_dg){
	dg_1.Retrieve(new Array(S_SYS_ID.value,'%' + S_PGM_CODE.value + '%'));
	return 0;
}

//html input type 의 자려가 변경된 경우 발생
function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id) {
		case "S_SYS_ID":
			x_DAO_Retrieve();
			break;
		case "chk_SHOW_YESNO":
		case "chk_UPDATE_YESNO":
		case "chk_RETRIEVE_YESNO":
		case "chk_INSERT_YESNO":
		case "chk_APPEND_YESNO":
		case "chk_DUPLICATE_YESNO":
		case "chk_DELETE_YESNO":
		case "chk_PRINT_YESNO":
		case "chk_EXCEL_YESNO":
		case "chk_CLOSE_YESNO":
		case "chk_DEVELOPED_YESNO":
		case "chk_HELP_YESNO":
			pf_SelectAll(_X.StrReplace(a_obj.id,"chk_",""),a_obj.checked);
			break;
	}
}

//enter key event 발생시 호출
function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	switch(a_obj.id) {
		case "S_PGM_CODE":
			//검색할 필드명을 배열에 넣는다
			var ll = dg_1.SetFocusByElement(a_obj, new Array("PGM_CODE", "PGM_NAME"))
			break;
	}
	return 0;
}

//keydown event 발생시 호출
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){return 100;}

//데이타 저장
function x_DAO_Save2(a_dg){return 100;}

//데이타 저장후 callback
function x_DAO_Saved2(){
	// for(var i=0; i<ls_up_Array.length; i++){
	// 	_X.ExecProc("sm", "PROC_SM_AUTH_GROUP_MENU_PGMNAME", new Array(ls_up_Array[i][0], ls_up_Array[i][1], ls_up_Array[i][2], ls_up_Array[i][3], ls_up_Array[i][4]));
	// }

	ls_up_Array = new Array();
	return 100;
}

//데이타 유효성 check
function x_DAO_ChkErr2(){
	var cData = dg_1.GetChangedData();

	var sNo = 0;
	var chkMsg = "";

	if(cData==null || cData=="") return false;

	for(var i=0; i<cData.length; i++){
		if(cData[i].job == "D") continue;
		sNo = cData[i].idx;
		//중복체크...
		if (cData[i].job=="I") {
			if (pf_dupPGMCODE(cData[i].data["PGM_CODE"],cData[i].idx)==false) {
				chkMsg = eval(sNo) + " 번째 행에 중복된 메뉴코드가 존재합니다";
				break;
			}
		}else if(cData[i].job=="U"){
			ls_up_Array[i] = new Array();

			ls_up_Array[i][0] = dg_1.GetItem(cData[i].idx, "COMPANY_CODE");
			ls_up_Array[i][1] = dg_1.GetItem(cData[i].idx, "SYS_ID");
			ls_up_Array[i][2] = dg_1.GetItem(cData[i].idx, "PGM_CODE");
			ls_up_Array[i][3] = dg_1.GetItem(cData[i].idx, "SORT_CODE");
			ls_up_Array[i][4] = dg_1.GetItem(cData[i].idx, "PGM_NAME");
		}
	}

	if(chkMsg!="") {
		_X.MsgBox('확인', chkMsg);
		dg_1.SetRow(sNo);
		dg_1.focus();
		return false;
	}
	return true;
}

//grid insert/add
function x_DAO_Insert2(a_dg, row){return 100;}

//grid insert 수행후 발생
function x_Insert_After2(a_dg, rowIdx){
	a_dg.SetItem(rowIdx, "COMPANY_CODE", mytop._CompanyCode);
	a_dg.SetItem(rowIdx, "SYS_ID", (S_SYS_ID.value=="%"?"":S_SYS_ID.value));
	a_dg.SetItem(rowIdx, "PGM_URL", "/XG010.do");
	return 0;
}

//grid 복제
function x_DAO_Duplicate2(a_dg, row){return 100;}

//grid 복제후
function x_Duplicate_After2(a_dg, rowIdx){
	if(a_dg==dg_1)	{
		a_dg.SetItem(rowIdx, "PGM_CODE", "");
	}
	return 0;
}

//grid 삭제
function x_DAO_Delete2(a_dg, row){return 100;}

//excel 저장
function x_DAO_Excel2(a_dg){return 100;}

//출력
function x_DAO_Print2(){
	_X.SetiReport("sm",  "인사명령신청서", "asValue1=100&asValue2=1&asValue3=2040299&asValue4=가나다");
	/*
	//report명|서브시스템|queryfile|queryid|data type|share
	_X.SetRexpert("일계표", "sm",  "SM_AUTH_PGMCODE|C_SM0903_01", "memo", false);
	_X.RptRetrieve(new Array(S_SYS_ID.value,'%' + S_PGM_CODE.value + '%'));
	return 100;
	*/
}

//tab 변경시
function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}

//tab 변경시
function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}

//grid data 변경시
function xe_GridDataChange2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	switch(a_col) {
		case "PGM_CODE":
			a_dg.SetItem(a_row,"SORT_CODE",a_newvalue);
			break;
	}
	return 0;
}

//grid row focus change
function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){return true;}

//item focus change event
function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){return true;}

//grid data loaded
function xe_GridDataLoad2(a_dg){return 100;}

//찾기창 호출 후
function x_FindCode2(a_dg, a_row, a_findKey, a_findstr, a_win) {return 100;}

//찾기창 호출 후
function x_ReceivedCode2(){
	switch(_FindCode.finddiv){
		case "PGM_CODE":
			//소스 복사
			pf_SourceCopy(_FindCode.returnValue.PGM_CODE, _FindCode.returnValue.PGM_NAME);
			break;
		}
}

//grid item button click event
function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){return 100;}

//header click event
function xe_GridHeaderClick2(a_dg, a_col, a_colname){return 100;}

//item double click event
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){return 100;}

//tree item checked event
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){return 100;}

//tree item checked event
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){return 100;}

//- anychart  point click event
function xe_ChartPointClick2(a_dg, a_event){return 100;}

//- anychart  point select event
function xe_ChartPointSelect2(a_dg, a_event){return 100;}

//- anychart  point mouse over event
function xe_ChartPointMouseOver2(a_dg){return 100;}

//- anychart  point mouse out event
function xe_ChartPointMouseOut2(a_dg, a_event){return 100;}

//window(mdi) close
function x_Close2() {return 100;}

//전체선택/해제처리
function pf_SelectAll(a_column, a_flag) {
	for(var i = 1; i <= dg_1.RowCount(); i++){
		dg_1.SetItem(i, a_column, (a_flag?'Y':'N'));
	}
}

//메뉴코드 중복체크
function pf_dupPGMCODE(a_value, a_row) {
	for(var i=1; i<dg_1.RowCount(); i++){
		if (dg_1.GetItem(i,"PGM_CODE")==a_value&&i!=a_row) {
			return false;
		}
	}
	return true;
}

//프로그램 코드찾기
function pf_PgmFind() {
	var a_row = dg_1.GetRow();
	if(a_row<=0)
		return;
	var pgm_code = dg_1.GetItem(a_row,'PGM_CODE');
	_X.CommonFindCode(window,"프로그램코드찾기","com","PGM_CODE", "", new Array(mytop._CompanyCode),"FindPgmCode|FindCode|FindPgmCode",500, 550);
}

//프로그램 복사
function pf_SourceCopy(pgm_code, pgm_name) {
	var a_row = dg_1.GetRow();
	if(a_row<=0) return;
	var t_pgm_code = dg_1.GetItem(a_row,'PGM_CODE');
	var t_pgm_name = dg_1.GetItem(a_row,'PGM_NAME');

	if(_X.MsgBoxYesNo("원본 : [" + pgm_code + "] " + pgm_name + "\r\n대상 : [" + t_pgm_code + "] " + t_pgm_name + "\r\n소스 복사작업을 진행 하시겠습니까?","",2)!=1) {return;}
	if(_X.CallJsp("/Mighty/jsp/SourceCopy.jsp?fpgm=" + pgm_code + "&tpgm=" + t_pgm_code + "")) {
		_X.MsgBox("소스복사작업이 완료되었습니다.");
	}
}

function pf_GridConv() {
	var pgmCode = dg_1.GetItem(dg_1.GetRow(), 'PGM_CODE');
	if(pgmCode.length>=7) {
		window.open("/Theme/jsp/xg_Real2Slick.jsp?sys=" + dg_1.GetItem(dg_1.GetRow(), 'SYS_ID') + "&pcd=" + pgmCode, pgmCode, "top=300, left=300, width=300, height=300, toolbar=no, menubar=no, scrollbars=no, resizable=yes");
	}

/*
	var rowCnt = dg_1.RowCount();

	var sRow = dg_1.GetRow();
	rowCnt = dg_1.GetRow();

	for(var i=sRow; i<=rowCnt; i++) {
		var pgmCode = dg_1.GetItem(i, 'PGM_CODE');
		if(pgmCode.length>=7) {
			window.open("/Theme/jsp/xg_Real2Slick.jsp?sys=" + dg_1.GetItem(i, 'SYS_ID') + "&pcd=" + pgmCode, pgmCode, "width=300, height=300, toolbar=no, menubar=no, scrollbars=no, resizable=yes");
		}
	}
*/
}

function pf_Slick2DB() {
	var pgmCode = dg_1.GetItem(dg_1.GetRow(), 'PGM_CODE');
	if(pgmCode.length>=6) {
		window.open("/Theme/jsp/xg_Slick2DB.jsp?sys=" + dg_1.GetItem(dg_1.GetRow(), 'SYS_ID') + "&pcd=" + pgmCode, pgmCode, "top=300, left=300, width=300, height=300, toolbar=no, menubar=no, scrollbars=no, resizable=yes");
	}
}

function pf_Real2DB() {
	var pgmCode = dg_1.GetItem(dg_1.GetRow(), 'PGM_CODE');
	if(pgmCode.length>=6) {
		window.open("/Theme/jsp/xg_Real2DB.jsp?sys=" + dg_1.GetItem(dg_1.GetRow(), 'SYS_ID') + "&pcd=" + pgmCode, pgmCode, "top=300, left=300, width=300, height=300, toolbar=no, menubar=no, scrollbars=no, resizable=yes");
	}
}
//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 : SM0906|사용자정보관리
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
//
//===========================================================================================
var ls_up_Array = new Array();
var ls_up_Pass = new Array();

function x_InitForm2(){
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "sm", "SM0906", "SM0906|Z_USER_C01", true, true);
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {

		//사용자구분
		var	ls_coustcd  = _X.XS('sm', 'SM0906', 'BM_COMM_DETAIL_R01' , [], 'json2');
		_X.DDLB_SetData(S_CUST_CODE, ls_coustcd, null, false, false);
		dg_1.SetCombo("USER_DIV", ls_coustcd);

		//메신저
		var	ls_messenger  = _X.XS('com', 'COMMON', 'BM_COMM_DETAIL' , ['81','%','%'], 'json2');
		dg_1.SetCombo("MESSENGER_DIV", ls_messenger);

		//직급메신저
		var	ls_messenger_gub  = _X.XS('com', 'COMMON', 'BM_COMM_DETAIL' , ['80','%','%'], 'json2');
		dg_1.SetCombo("JIK_GUB", ls_messenger_gub);

		//page구분
		var	ls_pagetag  = _X.XS('com', 'COMMON', 'SM_COMCODE_D' , ['SM','PAGE_TAG'], 'json2');
		dg_1.SetCombo("PAGE_TAG", ls_pagetag);

		x_DAO_Retrieve();
	}
	return 100;
}

function x_DAO_Retrieve2(a_dg){
	//조회조건 : 유저아이디, 사용구분, 사용자구분
	dg_1.Retrieve([mytop._CompanyCode, S_USER_ID.value, S_CUST_CODE.value]);
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id){
		case	"S_CUST_CODE":
					x_DAO_Retrieve();
					break;
	}

	return 100;
}

//enter key event 발생시 호출
function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	switch(a_obj.id) {
		case	"S_USER_ID":
					x_DAO_Retrieve();
					break;
	}

	return 0;
}

//keydown event 발생시 호출
function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){
}

//데이타 저장
function x_DAO_Save2(a_dg){return 100;}

//데이타 저장후 callback
function x_DAO_Saved2(){
	var nrow = dg_1.GetRow();
	x_DAO_Retrieve();
	dg_1.SetRow(nrow);

	//비번
	/*if(ls_up_Pass.length > 0){
		for(var i=0; i<ls_up_Pass.length; i++){
			_X.ExecProc("sm", "PROC_SM_AUTH_PASSWORD", new Array(ls_up_Pass[i][0], ls_up_Pass[i][1]));
		}
		ls_up_Pass = new Array();
	}*/
	return 100;
}

//데이타 유효성 check
function x_DAO_ChkErr2(){
	/*var cData = dg_1.GetChangedData();
	if(cData==null || cData=="") return true;

	var sNo = 0;
	var chkMsg = "";

	for(var i=0; i<cData.length; i++){
		if(cData[i].job == "D") continue;

		sNo = cData[i].idx;
		//중복체크...
		if (cData[i].job=="I") {
			if (pf_dupUSERID(cData[i].data["USER_ID"], sNo, "GRID") == false) {
				chkMsg = sNo + " 번째 행에 중복된 사용자아이디가 존재합니다";
				break;
			}
			if (pf_dupUSERID(cData[i].data["USER_ID"], sNo, "SQL") == false) {
				chkMsg = sNo + " 번째 행에 중복된 사용자아이디가 존재합니다";
				break;
			}

			// 신규사용자 초기 비밀번호 설정
			ls_up_Pass[i] = new Array();
			ls_up_Pass[i][0] = dg_1.GetItem(cData[i].idx, "COMPANY_CODE");
			ls_up_Pass[i][1] = dg_1.GetItem(cData[i].idx, "USER_ID");
		}
	}

	if(chkMsg!="") {
		_X.MsgBox('확인', chkMsg);
		dg_1.SetRow(sNo);
		USER_ID.focus();
		return false;
	}*/

	return true;
}

//grid insert/add
function x_DAO_Insert2(a_dg, row){return 100;}

//grid insert 수행후 발생
function x_Insert_After2(a_dg, rowIdx){
	a_dg.SetItem(rowIdx, "COMPCODE", mytop._CompanyCode);
	a_dg.SetItem(rowIdx, "USINGTAG","Y");
	a_dg.SetItem(rowIdx, "MESSENGER_DIV","99");
	a_dg.SetItem(rowIdx, "JIK_GUB","99");
	return 100;
}

//grid 복제
function x_DAO_Duplicate2(a_dg, row){return 100;}

//grid 복제후
function x_Duplicate_After2(a_dg, rowIdx){return 100;}

//grid 삭제
function x_DAO_Delete2(a_dg, row){
	if(_X.MsgBoxYesNo("선택된 사용자 정보를 삭제 하시겠습니까?") == "2") return;
	return 100;
}

//excel 저장
function x_DAO_Excel2(a_dg){return 100;}

//출력
function x_DAO_Print2(){return 100;}

//tab 변경시
function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}

//tab 변경시
function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){return 100;}

//grid data 변경시
function xe_GridDataChange2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){return 0;}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	if(a_dg.id == 'dg_1'){
		if(a_col == 'PASSWORD2'){
			dg_1.SetItem(a_row, 'PASSWORD', dg_1.GetItem(a_row,'PASSWORD2'));
		}
	}
	return 100;
}

//grid row focus change
function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

//grid row focus change
function xe_GridRowFocusChanged2(a_dg, a_newrow, a_oldrow){
}

//item focus change event
function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){return true;}

//grid data loaded
function xe_GridDataLoad2(a_dg){return 100;}

//찾기 화면에서 선택값을 넘긴 경우 호출
function x_ReceivedCode2(a_retVal){
	if(a_retVal == null) return;

	switch(_FindCode.finddiv){
		case	"find_custdiv":
					dg_1.SetItem(dg_1.GetRow(), "CUST_CODE", _FindCode.returnValue.CUST_CODE);
					dg_1.SetItem(dg_1.GetRow(), "CUST_NAME", _FindCode.returnValue.CUST_NAME);
					break;
	}
}

//grid item button click event
function xe_GridButtonClick2(a_dg, a_row, a_col, a_colname){
	if(a_dg.id == 'dg_1'){
		if(dg_1.GetItem(a_row, 'USER_DIV') == '' || dg_1.GetItem(a_row, 'USER_DIV') == null){
			_X.MsgBox("사용자구분을 입력해 주세요.");
			return;
		}

		if(a_col == 'CUST_NAME'){
			_X.CommonFindCode(window,"거래처찾기","com",'find_custdiv','',[],"FindCust|FindCode|FindCustCode",1200, 600);
		}
	}
	return 100;
}

//header click event
function xe_GridHeaderClick2(a_dg, a_col, a_colname){return 100;}

//item double click event
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){return 100;}

//tree item checked event
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){return 100;}

//tree item checked event
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){return 100;}

//- anychart  point click event
function xe_ChartPointClick2(a_dg, a_event){return 100;}

//- anychart  point select event
function xe_ChartPointSelect2(a_dg, a_event){return 100;}

//- anychart  point mouse over event
function xe_ChartPointMouseOver2(a_dg){return 100;}

//- anychart  point mouse out event
function xe_ChartPointMouseOut2(a_dg, a_event){return 100;}

//window(mdi) close
function x_Close2() {return 100;}

//사용자ID 중복체크
function pf_dupUSERID(a_value, a_row,  a_type) {
	if(a_type == "GRID") {
		for(var i = 1; i < dg_1.RowCount(); i++){
			if (dg_1.GetItem(i,"USER_ID") == a_value && i != a_row) {
				return false;
			}
		}
	} else {
		var data = _X.XmlSelect("sm", "SM_AUTH_USER", "R_SM0905_01", new Array(mytop._CompanyCode,a_value), "array");
		if(data[0][0] == a_value) return false;
	}
	return true;
}

function pf_resetPassword() {

	if(dg_1.RowCount() < 1) return;

	if(dg_1.IsDataChanged()) {
		_X.MsgBox("확인", "변경된 데이타가 존재합니다.\n저장 후 다시 시도하여 주십시오.");
		return;
	}

	if(_X.MsgBoxYesNo("비밀번호 초기화를 진행 하시겠습니까?") == "2") return;

	var ls_user = dg_1.GetItem(dg_1.GetRow(), "USER_ID");
	_X.ExecProc("sm", "PROC_SM_AUTH_PASSWORD", new Array(mytop._CompanyCode, ls_user));
	_X.MsgBox("확인", "비밀번호 변경이 완료되었습니다.");

	/*
	if(_X.MsgBoxYesNo("사용자 ID: " + ls_user + "\r\n사용자명: " + dg_1.GetItem(dg_1.GetRow(), "USER_NAME") + "\r\n비밀번호를 초기화 하시겠습니까?") == "1") {
		dg_1.SetItem(dg_1.GetRow(), "OLD_PASSWORD", dg_1.GetItem(dg_1.GetRow(), "USER_PASSWORD"));
		dg_1.SetItem(dg_1.GetRow(), "USER_PASSWORD", ls_user);
		dg_1.SetItem(dg_1.GetRow(), "PASSWORD_UPDATE", _X.ToString(_X.GetSysDate(), "YYYY-MM-DD"));
		_X.MsgBox("비밀번호가 초기화 되었습니다.");
		dg_1.Save(null, null, "N");
	}
	*/
}

//부서찾기 POPUP
function pf_DeptFind(a_findkey, a_find){
	_X.FindIFDept(this, a_findkey, a_find, "");
}


// 업체 POPUP
function pf_FindVend(a_findkey, a_find){
	_X.CommonFindCode(window,"거래처찾기","com",a_findkey,a_find,new Array(mytop._CompanyCode, "5"),"FindVend|FindCode|FindVend",1200, 550);
}

function uf_changeuser(){
	if (dg_1.RowCount() < 1){
		_X.MsgBox("확인","선택된 사용자가 없습니다.");
		return;
	}
	var ls_user_id = dg_1.GetItem(dg_1.GetRow(), "LOGINID");
	var ls_cust    = dg_1.GetItem(dg_1.GetRow(), "CUST_CODE");
	var ls_empname = dg_1.GetItem(dg_1.GetRow(), "EMPNAME");

	if(_X.MsgBoxYesNo("확인", "사용자[" + ls_empname + "] 로 로그인 아이디를 바꾸시겠습니까?") == 1) {
		mytop._UserID = ls_user_id;
		mytop._UserName = ls_empname;
		mytop._CustCode = ls_cust;

		//실행권한 설정
		mytop._MyActAuth = _X.XmlSelect("sm", "SM_AUTH_PGMCODE", "MY_AUTH_ACT", new Array(mytop._CompanyCode,ls_user_id), "array", "AUTH_ACT").join();

		//mytop.f_head.td_username.innerHTML = '<font style="color:#08537a; font-size:8pt; font-weight:bold; padding-left:3px;">&quot;' +  ls_empname + '&quot;님</font>'
	}

}

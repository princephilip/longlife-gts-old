<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE querylist SYSTEM "../../../Mighty/Xml/query.dtd">

<querylist>

	<select id="PT02030_R01">
	<sql><![CDATA[

  SELECT A.CUST_CODE
       , A.ORDER_NO
       , A.ITEM_NAME
       , A.REMARKS
       , A.UNIT
       , SUM(CASE WHEN A.ORDER_DATE = B.ORDER_STARTDATE THEN A.ITEM_QTY ELSE NULL END) AS QTY1
       , SUM(CASE WHEN A.ORDER_DATE = TO_DATE(B.ORDER_STARTDATE)+1 THEN A.ITEM_QTY ELSE NULL END) AS QTY2
       , SUM(CASE WHEN A.ORDER_DATE = TO_DATE(B.ORDER_STARTDATE)+2  THEN A.ITEM_QTY ELSE NULL END) AS QTY3
       , SUM(CASE WHEN A.ORDER_DATE = TO_DATE(B.ORDER_STARTDATE)+3  THEN A.ITEM_QTY ELSE NULL END) AS QTY4
       , SUM(CASE WHEN A.ORDER_DATE = TO_DATE(B.ORDER_STARTDATE)+4  THEN A.ITEM_QTY ELSE NULL END) AS QTY5
       , SUM(CASE WHEN A.ORDER_DATE = TO_DATE(B.ORDER_STARTDATE)+5  THEN A.ITEM_QTY ELSE NULL END) AS QTY6
       , SUM(CASE WHEN A.ORDER_DATE = TO_DATE(B.ORDER_STARTDATE)+6  THEN A.ITEM_QTY ELSE NULL END) AS QTY7
	     , SUM(CASE WHEN A.ORDER_DATE BETWEEN B.ORDER_STARTDATE AND TO_DATE(B.ORDER_STARTDATE)+6 THEN A.ITEM_QTY ELSE NULL END) AS QTY_TOT
       , D.ITEM_CODE AS ITEM_CODE
       , FN_GETITEMNAME(D.ITEM_NAME, D.ITEM_DNAME) AS AGREE_NAME
       , ' ' AS AGREE_DNAME
       , D.UNIT AS ITEM_UNIT
       , D.ITEM_STD
       , FN_GET_COST(A.UNIT_COST, ED.ITEM_COST, E.COST) AS COST
       , FN_GET_SUPPORTCOST_2(A.COMP_CODE,TO_CHAR( E.CHANGE_DATE, 'YYYYMMDD'), D.ITEM_CODE, A.CUST_CODE) AS SUPPORT_COST
       , FN_GET_COST(A.UNIT_COST, ED.ITEM_COST, E.COST) - FN_GET_SUPPORTCOST_2(A.COMP_CODE,TO_CHAR( E.CHANGE_DATE, 'YYYYMMDD'), D.ITEM_CODE, A.CUST_CODE) AS SUP_COST
       , FLOOR(SUM(NVL(A.ITEM_QTY, 0)) * NVL(ED.ITEM_COST, E.COST)) AS SUPPLY_AMT
       , FLOOR(SUM(NVL(A.ITEM_QTY, 0)) * FN_GET_SUPPORTCOST_2(A.COMP_CODE,TO_CHAR( E.CHANGE_DATE, 'YYYYMMDD'), D.ITEM_CODE, A.CUST_CODE)) AS SUPPORT_AMT
       , FLOOR(SUM(NVL(A.ITEM_QTY, 0)) * NVL(ED.ITEM_COST, E.COST)) - FLOOR(SUM(NVL(A.ITEM_QTY, 0)) * FN_GET_SUPPORTCOST_2(A.COMP_CODE,TO_CHAR( E.CHANGE_DATE, 'YYYYMMDD'), D.ITEM_CODE, A.CUST_CODE)) AS SUP_AMT
       , nvl(A.BRAND_NAME, D.BRAND_NAME) AS BRAND_NAME
       , A.BUYER_NAME
    FROM BM_ORDE_MASTER B INNER JOIN BM_ORDE_DETAIL A
                                  ON A.ORDER_NO = B.ORDER_NO AND A.COMP_CODE = B.COMP_CODE
                     LEFT OUTER JOIN BM_CODE_ITEM D
                                  ON A.ITEM_CODE = D.ITEM_CODE AND A.COMP_CODE = D.COMP_CODE
                                 AND NOT EXISTS ( SELECT 1 FROM BM_CODE_STOPITEM Z
                                                   WHERE D.ITEM_CODE = Z.ITEM_CODE
                                                     AND A.ORDER_DATE BETWEEN Z.STOP_SDATE AND Z.STOP_EDATE
                                                     AND A.COMP_CODE = #as_compcode#)
                     LEFT JOIN BM_CODE_SALES_ALL E
                                  ON A.ITEM_CODE = E.ITEM_CODE AND A.COMP_CODE = E.COMP_CODE
                                 AND A.ORDER_DATE BETWEEN E.CHANGE_DATE AND E.END_DATE
                     LEFT JOIN TABLE (PKG_VW_ESTI_DETAIL.FN_VW_ESTI_DETAIL(#as_compcode#,#as_yyyymm#,#as_cust#)) ED
                                  ON B.COMP_CODE = ED.COMP_CODE
                                 AND B.CUST_CODE = ED.CUST_CODE
                                 AND ED.YYYYMM = B.ESTI_YYMM
                                 AND ED.ITEM_CODE = A.ITEM_CODE
                                 AND ED.NEIS_NAME = A.ITEM_NAME
   WHERE A.CUST_CODE = #as_cust#
     AND A.ORDER_NO = #as_orderno#
     AND A.COMP_CODE = #as_compcode#
GROUP BY A.CUST_CODE,
         A.ORDER_NO,
         A.ITEM_NAME,
         A.REMARKS,
         A.UNIT,
         D.ITEM_CODE
       , D.ITEM_NAME
       , D.ITEM_DNAME
       , D.UNIT
       , D.ITEM_STD
       , A.UNIT_COST, ED.ITEM_COST, E.COST
       , E.CHANGE_DATE
       , A.COMP_CODE
       , nvl(A.BRAND_NAME, D.BRAND_NAME)
       , A.BUYER_NAME
ORDER BY A.ORDER_NO  ASC, MAX(A.ORDER_SEQ) ASC

	]]></sql>
	<param name="as_cust" type="String"/>
	<param name="as_orderno" type="String"/>
	<param name="as_compcode" type="String"/>
	<param name="as_yyyymm" type="String"/>
	</select>


	<select id="PT02030_R02">
	<sql><![CDATA[

  SELECT B.SUPPORT_YEAR
       , '보조금총액' AS TITLE
       , SUM(NVL(A.SUPPORT_AMT, 0)) + SUM(NVL(A.SUPPORT_AMT_ADD, 0)) AS AMT
       , A.COMP_CODE
    FROM BM_SUPP_YEAR A
              INNER JOIN BM_CODE_SUPPORTTERM B
                      ON #as_ym# BETWEEN B.FROM_YYMM AND B.TO_YYMM
                     AND A.CONT_YEAR = B.SUPPORT_YEAR
                     AND A.COMP_CODE = B.COMP_CODE
   WHERE A.CUST_CODE = #as_cust#
    AND A.COMP_CODE = #as_compcode#
GROUP BY B.SUPPORT_YEAR, A.COMP_CODE
   UNION ALL
  SELECT (SELECT SUPPORT_YEAR FROM BM_CODE_SUPPORTTERM WHERE #as_ym# BETWEEN FROM_YYMM AND TO_YYMM AND COMP_CODE = #as_compcode#) AS SUPPORT_YEAR
       , '사용액계' AS TITLE
       , SUM(NVL(SUPPORT_AMT, 0)) AS AMT
       , COMP_CODE
    FROM TABLE (PKG_VW_ORDE_CLAIM.FN_VW_ORDE_CLAIM2(#as_compcode#,(SELECT FROM_YYMM || '01' FROM BM_CODE_SUPPORTTERM WHERE #as_ym# BETWEEN FROM_YYMM AND TO_YYMM AND COMP_CODE = #as_compcode#),
     (SELECT TO_CHAR(ADD_MONTHS(TO_DATE( TO_YYMM || '01'),1)-1,'YYYYMMDD') FROM BM_CODE_SUPPORTTERM WHERE #as_ym# BETWEEN FROM_YYMM AND TO_YYMM AND COMP_CODE = #as_compcode#),
     #as_cust#,
      '%'))
 GROUP BY COMP_CODE

	]]></sql>
	<param name="as_cust" type="String"/>
	<param name="as_ym" type="String"/>
	<param name="as_compcode" type="String"/>
	</select>


	<select id="PT02030_U03">
	<sql><![CDATA[

  SELECT A.ORDER_NO,
         A.CUST_CODE,
         A.ORDER_STARTDATE,
         A.ORDER_ENDDATE,
         TO_CHAR(A.ORDER_STARTDATE,'YYYY-MM-DD') || ' ~ ' || TO_CHAR(A.ORDER_ENDDATE,'YYYY-MM-DD')	AS ORDER_TERM,
         A.ORDER_STATUS,
         B.CUST_NAME,
         A.MEAL_DIV,
         C.DNAME AS MEAL_NAME,
         A.COMP_CODE,
         A.CNSLT_NO,
         A.ESTI_YYMM,
         NULL		AS ATCH_FILE_ID,
         LPAD(ROW_NUMBER() OVER(ORDER BY A.ROW_INPUT_DATE DESC),3,'0')	AS R_NUM
    FROM BM_ORDE_MASTER A INNER JOIN BM_CODE_CUST B
                                  ON A.CUST_CODE = B.CUST_CODE
                                  AND A.COMP_CODE = B.COMP_CODE
         LEFT OUTER JOIN BM_COMM_DETAIL C
                      ON A.MEAL_DIV = C.DCODE
                     AND C.HCODE = '52'
                     AND A.COMP_CODE = C.COMP_CODE
   WHERE A.CUST_CODE = #as_cust#
     AND #as_month# BETWEEN TO_CHAR(A.ORDER_STARTDATE,'YYYYMM') AND  TO_CHAR(A.ORDER_ENDDATE,'YYYYMM')
     AND A.COMP_CODE = #as_compcode#
     AND NVL(A.RICE_YN,'N')='N'
ORDER BY ORDER_STARTDATE DESC, A.ORDER_NO DESC

	]]></sql>
	<param name="as_cust" type="String"/>
	<param name="as_month" type="String"/>
	<param name="as_compcode" type="String"/>
	</select>


	<!-- 파일경로 -->
	<select id="GET_FILE_LOC">
	<sql><![CDATA[

		SELECT	SUBSTR(FILE_LOC,6)
		FROM		BM_ORDER_FILE
		WHERE		CUST_CODE	=	#as_cust#
		AND			FILE_ID		=	#as_fileid#

	]]></sql>
	<param name="as_cust" type="String"/>
	<param name="as_fileid" type="String"/>
	</select>


	<!-- 엑셀업로드:시작일자 -->
	<select id="UPLOAD_GET_FRDT">
	<sql><![CDATA[

		SELECT	REPLACE(SUBSTR(MAX(C_002),6,10),'.','')
		FROM	XM_EXCEL_DATA
		WHERE	PGM_ID	=	#as_pgmid#
		AND		USER_ID	=	#as_userid#
		AND		C_002	LIKE	'기간%'

	]]></sql>
	<param name="as_pgmid" type="String"/>
	<param name="as_userid" type="String"/>
	</select>


	<!-- 엑셀업로드:시작요일 -->
	<select id="UPLOAD_GET_DAYNM">
	<sql><![CDATA[

		SELECT TO_CHAR(TO_DATE(#as_frdt#),'day') from dual

	]]></sql>
	<param name="as_frdt" type="String"/>
	</select>


	<!-- 엑셀업로드:월요일_일자 -->
	<select id="UPLOAD_GET_MONDAY">
	<sql><![CDATA[

		SELECT	REPLACE(MAX(C_008),'.','')
		FROM	XM_EXCEL_DATA
		WHERE	PGM_ID	=	#as_pgmid#
		AND		USER_ID	=	#as_userid#
		AND		C_002	=	'NO'

	]]></sql>
	<param name="as_pgmid" type="String"/>
	<param name="as_userid" type="String"/>
	</select>


	<!-- 엑셀업로드:시작요일 -->
	<select id="UPLOAD_CHK_REGTIME">
	<sql><![CDATA[

		select nvl(value1, ''), nvl(value2, '') from bm_comm_detail where hcode = '98' and dcode = '02' and use_yn = 'Y' and comp_code = #as_comp#

	]]></sql>
	<param name="as_comp" type="String"/>
	</select>


	<!-- 엑셀업로드:숫자체크 -->
	<select id="UPLOAD_CHK_NUM">
	<sql><![CDATA[

		SELECT	COUNT(*)
		FROM	(	SELECT	IS_NUMBER(C_008)	AS CHK1
						,	IS_NUMBER(C_010)	AS CHK2
						,	IS_NUMBER(C_013)	AS CHK3
						,	IS_NUMBER(C_014)	AS CHK4
						,	IS_NUMBER(C_015)	AS CHK5
						,	IS_NUMBER(C_016)	AS CHK6
						,	IS_NUMBER(C_018)	AS CHK7
					FROM	XM_EXCEL_DATA
					WHERE	PGM_ID	=	#as_pgmid#
					AND		USER_ID	=	#as_userid#
					AND		IS_NUMBER(C_002)	=	1
				)	T
		WHERE	CHK1 = 0 OR CHK2 = 0 OR CHK3 = 0 OR CHK4 = 0 OR CHK5 = 0 OR CHK6 = 0 OR CHK7 = 0

	]]></sql>
	<param name="as_pgmid" type="String"/>
	<param name="as_userid" type="String"/>
	</select>





</querylist>

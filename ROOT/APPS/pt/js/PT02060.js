//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : 일단위주문관리
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
var is_custcode = mytop._CustCode;
var is_orderno_new = "";
var is_closetime = "24";

function x_InitForm2(){
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "pt", "PT02060", "PT02060|PT02060_C01", false, true);

	if(_X.HaveActAuth("시스템관리자")) is_custcode = '1000001';
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		var la_MEAL_DIV    = _X.XmlSelect("com", "COMMON", "BM_COMM_DETAIL" , new Array("52", '%', 'Y'), 'json2');
		dg_1.SetCombo("MEAL_DIV", la_MEAL_DIV);

		var la_ORDERYN = [{"code":"N", "label":"대기중"}, {"code":"Y", "label":"발주중"}];
		dg_1.SetCombo("ORDER_YN", la_ORDERYN);

		var la_CALL = [{"code":"N", "label":" "}, {"code":"Y", "label":"  "}];
		dg_1.SetCombo("CALL_REQUEST", la_CALL);

		var ls_rtn = _X.XmlSelect("pt", "PT02060", "GET_CLOSE_TIME", [mytop._CompanyCode], "array")[0][0];
		if(ls_rtn!=null && ls_rtn!=""){
			is_closetime = ls_rtn;
			span_closetime.innerHTML = "주문서 수정가능시간 : 전일 " + is_closetime.substr(0,2) + "시 " + is_closetime.substr(2,2) + "분";
		}

		dg_1.SetAutoReadOnly("MEAL_DIV",  function (args) {if(args.dataContext.ORDER_YN == "Y") return true;}, false);
		dg_1.SetAutoReadOnly("ITEM_NAME", function (args) {if(args.dataContext.ORDER_YN == "Y") return true;}, false);
		dg_1.SetAutoReadOnly("UNIT",      function (args) {if(args.dataContext.ORDER_YN == "Y") return true;}, false);
		dg_1.SetAutoReadOnly("REMARKS",   function (args) {if(args.dataContext.ORDER_YN == "Y") return true;}, false);
		dg_1.SetAutoReadOnly("ITEM_QTY",  function (args) {if(args.dataContext.ORDER_YN == "Y") return true;}, false);
		dg_1.SetAutoReadOnly("CHIN_YN",   function (args) {if(args.dataContext.ORDER_YN == "Y") return true;}, false);
		dg_1.SetAutoReadOnly("REMARKS2",  function (args) {if(args.dataContext.ORDER_YN == "Y") return true;}, false);

		xe_EditChanged(sle_basicmonth, sle_basicmonth.value);
	}
}

function x_DAO_Retrieve2(a_dg){
	var ls_yymm = _X.StrPurify(sle_basicmonth.value);
	dg_1.Retrieve([S_ORDER_NO.value, mytop._CompanyCode, ls_yymm, is_custcode]);
}

function xe_EditChanged2(a_obj, a_val){
	if(a_obj.id == "sle_basicmonth") {
		var ls_yymm = _X.StrPurify(sle_basicmonth.value);

		var la_ORDERNO = _X.XmlSelect("pt", "PT02060", "ORDER_LIST", new Array(mytop._CompanyCode, is_custcode, ls_yymm), 'json2');
		_X.DDLB_SetData(S_ORDER_NO, la_ORDERNO, null, true, false);
		is_orderno_new = "";
	}
	x_DAO_Retrieve();
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	if(a_obj.id == 'S_FIND'){
		if(S_FIND.value != ""){
			var li_fRow =  dg_1.SetFocusByElement('S_FIND',['ITEM_NAME','REMARKS','AGREE_NAME'], false);
			if(li_fRow > 0) {
				setTimeout("dg_1.SetRow(" + li_fRow.toString() + ")",100);
			} else {
				_X.MsgBox("확인","검색한 품목이 존재하지 않습니다.");
			}
		}
	}
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	var l_modrows = ptdwo.GetModifiedRows(dg_1.dw, DW_BUFFER_primary);
	var ls_enabledt = ptdwo.SqlSelect("om", " SELECT TO_CHAR(SYSDATE+1,'YYYYMMDD HH24MI') FROM DUAL ").split(" ");
	for(var i=0; i<l_modrows.length;i++){

		var ls_date = _X.ToString(dg_1.GetItem(l_modrows[i], "order_date"), 'yyyymmdd');
		if(ls_date < ls_enabledt[0]){
			_X.MsgBox("확인", "주문서 수정 가능시간이 지난 데이터는 저장할 수 없습니다!");
			ptdwo.ResetUpdate();
			jf_dw_retrieve();
			return false;
		}

		if(ls_date == ls_enabledt[0] && ls_enabledt[1] > is_closetime){
			_X.MsgBox("확인", "주문서 수정 가능시간이 지난 데이터는 저장할 수 없습니다!");
			ptdwo.ResetUpdate();
			jf_dw_retrieve();
			return false;
		}

	}


	var li_newrows = ptdwo.GetNewRows(dg_1.dw, DW_BUFFER_primary);
	var li_rtn = ptdwo.SaveDW(dg_1.dw);
	if(li_rtn==-1){
		ptdwo.ResetUpdate();
		jf_dw_retrieve();
		return;
	}

	_X.MsgBox("확인", "저장되었습니다.");
	return 0;

	//return true;
}

//저장후 호출
function x_DAO_Saved2(){
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
	if(a_dg.id == 'dg_1') {
		var ls_orderno = "";
		var ls_orderseq = "";
		var ls_date;
		if(S_ORDER_NO.value == "" || S_ORDER_NO.value == null) {		//발주서가 없을시
			var ls_today = _X.ToString(_X.GetSysDate(), "yyyymmdd");

			if(is_orderno_new == "") {
				//주문번호
				var ls_order = _X.XmlSelect("pt", "PT02060", "MAX_ORDER_NO", [mytop._CompanyCode, is_custcode, ls_today], "array")[0][0];
		  	is_orderno = ls_order;

				var ls_sdate = _X.StrPurify(sle_basicmonth.value)+'01';
				var ls_edate = _X.LastDate(ls_sdate);
				_X.ES("pt", "PT02060", "BM_ORDE_MASTER_C01", [mytop._CompanyCode, is_custcode, ls_sdate, ls_edate, is_orderno, mytop._UserID, mytop._ClientIP]);
		  	is_orderno_new = ls_order;
		  }
			ls_date = ls_today;
			ls_orderno = is_orderno_new;
		} else {				//발주서가 있을시
			ls_date = _X.ToString(dg_1.GetItem(dg_1.GetRow(), "ORDER_DATE"),'yyyymmdd');
			ls_orderno = S_ORDER_NO.value;
		}
		dg_1.SetItem(rowIdx, "ORDER_NO", ls_orderno);
		var li_seq = Number(dg_1.GetTotalValue("ORDER_SEQ", "MAX"))+1;
		dg_1.SetItem(rowIdx, "ORDER_SEQ", li_seq);
		dg_1.SetItem(rowIdx, "ORDER_DATE", _X.ToDate(ls_date));
		dg_1.SetItem(rowIdx, "USE_DATE", _X.ToDate(ls_date));
		dg_1.SetItem(rowIdx, "CUST_CODE", is_custcode);
		dg_1.SetItem(rowIdx, "ORDER_STATUS", '01');
		dg_1.SetItem(rowIdx, "UNIT", 'kg');
		dg_1.SetItem(rowIdx, "MEAL_DIV", '2');
		dg_1.SetItem(rowIdx, "CHIN_YN", 'N');
		dg_1.SetItem(rowIdx, "CHANGE_DIV", 'Y');
		dg_1.SetItem(rowIdx, "COMP_CODE",mytop._CompanyCode);
	}
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	if(a_dg.id == "dg_1") {
		if(dg_1.GetItem(dg_1.GetRow(), "ORDER_YN") == 'Y') {
			_X.MsgBox("확인", "이미 발주된 내용은 삭제가 불가능 합니다.");
			return 0;
	  }
  }

	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	if(a_dg.id == 'dg_1') {
		switch(a_col) {
			case	"ORDER_DATE":
						dg_1.SetItem(a_row, "USE_DATE", _X.ToDate(a_newvalue));
						break;
		}
	}
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
	if(a_dg.id == 'dg_1') {
		uf_Set_Color();
	}
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
	if(a_dg.id == 'dg_1') {
		switch(a_colname) {
			case	"ITEM_NAME": 	// 센터품목명
						if(dg_1.GetItem(a_row, "ORDER_YN") == "Y") return;
						var ls_find = dg_1.GetItem(a_row, "ITEM_NAME");
						_X.CommonFindCode(window,"품목검색","com",'ITEM_CODE',ls_find,[is_custcode, mytop._CompanyCode],"FindCodeItem2|FindCode|BM_CODE_ITEM_R02",800, 600);
						break;
			case	"CANCEL_REQUEST":
						dg_1.SetItem(a_row, 'ITEM_QTY', 0);
						break;
			case	"CALL_REQUEST":
						//전화요청(2014.08.13 김양열)
						var ls_date = _X.ToString(dg_1.GetItem(dg_1.GetRow(), "ORDER_DATE"),'yyyy.mm.dd');
						var ls_itemname  = dg_1.GetItem(a_row,'AGREE_NAME');
						var ls_order_no  = dg_1.GetItem(a_row,'ORDER_NO');
						var ls_order_seq = dg_1.GetItem(a_row,'ORDER_SEQ');

						if(_X.MsgBoxYesNo("전화요청", "<b>" + ls_date + "</b>일자 <b>" + ls_itemname + "</b> 품목 관련하여 <br>급식지원센터에 <b>전화요청</b>을 하시겠습니까?") == 1) {
							dg_1.SetItem(a_row,'CALL_REQUEST','Y');
							var ls_msg = "<b>" + mytop._UserName + "</b>님이 <b>" + ls_date + "</b>일자 <b>" + ls_itemname + "</b> 품목 관련하여 <br>급식지원센터에 전화요청을 하셨습니다."
							_X.EP('pt','SP_SM_CALL_REQUEST', [mytop._CompanyCode, mytop._UserID, '01', ls_msg, mytop._ClientIP, '9000001', ls_order_no + "|" + ls_order_seq]);
							_X.EP('pt','SP_BM_ORDE_DETAIL_CALL_REQ', [ls_order_no, ls_order_seq, 'Y']);
							_X.MsgBox("전화요청이 처리되었습니다.");
						}
						break;
		}
	}
}

function x_ReceivedCode2(a_retVal){
	if(_FindCode.returnValue == null) return;

	switch(_FindCode.finddiv) {
		case	"ITEM_CODE" :
					dg_1.SetItem(dg_1.GetRow(), "ITEM_NAME", _FindCode.returnValue.ITEM_NAME);
					dg_1.SetItem(dg_1.GetRow(), "REMARKS", _FindCode.returnValue.REMARKS);
					dg_1.SetItem(dg_1.GetRow(), "UNIT", _FindCode.returnValue.UNIT);
					break;
	}
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){}
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}

function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){
}

function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

function uf_Set_Color() {
	var s_red = { "color": "#FF0000" };
	var s_blk = { "color": "#000000" };

	for(var i = 1; i <= dg_1.RowCount(); i++){
		dg_1.SetCellStyle(i, "ITEM_NAME", (dg_1.GetItem(i, "CHANGE_DIV") == "Y") ? s_red : s_blk);
		dg_1.SetCellStyle(i, "REMARKS"  , (dg_1.GetItem(i, "CHANGE_DIV") == "Y") ? s_red : s_blk);
	}
}

//거래명세서
function uf_invoice() {
	if(dg_1.RowCount() < 1) {
		return;
	}

	mytop.rpt_caller = window;

	var ls_gbn = '1';
	var ls_fromdate = dg_1.GetItem(dg_1.GetRow(), 'order_date');
	var ls_todate		= dg_1.GetItem(dg_1.GetRow(), 'order_date');

	ptdwo.SetReport('om','dr_om04252_4',false);

	ptdwo.RptRetrieve(mytop._CompanyCode, is_custcode, ls_fromdate, ls_todate, ls_gbn);

	return 0;
}

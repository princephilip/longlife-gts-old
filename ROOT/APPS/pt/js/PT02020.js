//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : 시장조사자료조회
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
var _CustCode = mytop._CustCode;
var _APPLY_RATE  = 0;

function x_InitForm2(){
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "pt", "PT02020", "PT02020|PT02020_R01", false, false);
	_X.InitGrid(grid_2, "dg_2", "100%", "100%", "pt", "PT02020", "PT02020|PT02020_R02", false, false);

	if(_X.HaveActAuth("시스템관리자")) _CustCode = '1000001';
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		setTimeout("x_DAO_Retrieve()",100);
	}
}

function x_DAO_Retrieve2(a_dg){
	var ls_yymm = _X.StrPurify(sle_basicmonth.value);

	dg_2.Reset();
	dg_1.Retrieve([mytop._CompanyCode, _CustCode, ls_yymm]);
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id) {
		case	"sle_basicmonth":
					x_DAO_Retrieve();
					break;
		case "S_RANGE": //비율변경
					var ll_left  = Number(a_val);
					var ll_right = 1000 - Number(a_val);

					grid_1.ResizeInfo = {init_width:ll_left,  init_height:665, width_increase:1, height_increase:1};
					grid_2.ResizeInfo = {init_width:ll_right, init_height:665, width_increase:0, height_increase:1};
					//grid_1.ResizeInfo = {init_width:500,  init_height:665, width_increase:1, height_increase:1};
					//grid_2.ResizeInfo = {init_width:500, init_height:665, width_increase:0, height_increase:1};
					xm_BodyResize(true);
					break;
	}
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	if(a_obj.id == 'S_FIND'){
		if(S_FIND.value != ""){
			var li_fRow =  dg_1.SetFocusByElement('S_FIND',['NEIS_NAME','ITEM_DNAME'], false);
			if(li_fRow > 0) {
				setTimeout("dg_1.SetRow(" + li_fRow.toString() + ")",100);
			} else {
				_X.MsgBox("확인","검색한 품목이 존재하지 않습니다.");
			}
		}
	}
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

//저장후 호출
function x_DAO_Saved2(){
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
	if(a_dg.id == 'dg_1') {
		var ls_date = _X.StrPurify(sle_basicmonth.value)+"01";
		var ls_neis = dg_1.GetItem(a_newrow, "ITEM_NAME");
		if(ls_neis == ""){
			dg_2.Reset();
			return;
		}
		dg_2.Retrieve(["%", ls_date, "%", mytop._CompanyCode, ls_neis]);
	}
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){}
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

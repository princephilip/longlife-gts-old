//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : 주문서등록_쌀
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
var is_cust = mytop._CustCode;
var is_custname = mytop._CustName;
var is_precust = "";

function x_InitForm2(){
	$("#btn_edit").hide();
	$("#btn_chk").hide();
	$("#btn_cancel").hide();

	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "pt", "PT02050", "PT02050|PT02050_C01", true, false);
	_X.InitGrid(grid_2, "dg_2", "100%", "100%", "pt", "PT02050", "PT02050|PT02050_C02", true, true);
	_X.InitGrid(grid_3, "dg_3", "100%", "100%", "pt", "PT02050", "PT02050|PT02050_R03", false, false);

	if(_X.HaveActAuth("시스템관리자")) {
		is_cust = '%';
		is_custname = '';
	}

	S_TO_DATE.value = _X.RelativeDate(_X.ToString(_X.GetSysDate(),'yyyy-mm-dd'), 7);
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		switch(mytop._UserTag){
			case	"01":
						$("#btn_edit").show();
						break;
			case	"05":
						dg_1.SetCheckBar(true);
						$("#btn_chk").show();
						$("#btn_cancel").show();
						break;
			default:
						dg_1.SetCheckBar(true);
						$("#btn_edit").show();
						$("#btn_chk").show();
						$("#btn_cancel").show();
						break;
		}

		setTimeout("x_DAO_Retrieve()",100);
	}
}

function x_DAO_Retrieve2(a_dg){
	if(mytop._UserTag=='05'){
		dg_1.Retrieve([mytop._CompanyCode, '%', is_cust, _X.StrPurify(S_FROM_DATE.value), _X.StrPurify(S_TO_DATE.value), S_CUST_NAME.value, S_SUPPLIER.value]);
	}else{
		dg_1.Retrieve([mytop._CompanyCode, is_cust, '%', _X.StrPurify(S_FROM_DATE.value), _X.StrPurify(S_TO_DATE.value), S_CUST_NAME.value, S_SUPPLIER.value]);
	}
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id) {
		case	"S_FROM_DATE":
		case	"S_TO_DATE":
		case	"S_CUST_NAME":
		case	"S_SUPPLIER":
					x_DAO_Retrieve();
					break;
		case	"SUPPORT_AMT":
		case	"ADD_SUPPORT_AMT":
					var li_row = dg_2.GetRow();
					var ll_tot = _X.ToInt(dg_2.GetItem(li_row, "SUPPORT_AMT")) + _X.ToInt(dg_2.GetItem(li_row, "ADD_SUPPORT_AMT"));
					dg_2.SetItem(li_row, "SUPPORT_TOT", ll_tot);
					uf_set_remain();
					break;
		case	"ALREADY_USED_AMT":
					uf_set_remain();
					break;
	}
}

function uf_set_remain() {
	var li_row = dg_2.GetRow();
	var ll_rem = _X.ToInt(dg_2.GetItem(li_row, "SUPPORT_AMT")) + _X.ToInt(dg_2.GetItem(li_row, "ADD_SUPPORT_AMT"))
							-	_X.ToInt(dg_2.GetItem(li_row, "ALREADY_USED_AMT")) - _X.ToInt(dg_2.GetItem(li_row, "USED_AMT"));
	dg_2.SetItem(li_row, "REMAIN_AMT", ll_rem);
	dg_2.SetItem(li_row, "BUY_NH", Math.round(ll_rem / 21610 * 10) / 10);
	dg_2.SetItem(li_row, "BUY_JH", Math.round(ll_rem / 19610 * 10) / 10);
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

//저장후 호출
function x_DAO_Saved2(){
}

function x_DAO_Insert2(a_dg, row){
	if(a_dg.id == 'dg_1') {
	}

  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
	if(a_dg.id == 'dg_1') {
		dg_1.SetItem(rowIdx, "ORDER_DATE", _X.ToString(_X.GetSysDate(), "yyyymmdd"));
		dg_1.SetItem(rowIdx, "CUST_CODE" , is_cust);
		dg_1.SetItem(rowIdx, "CUST_NAME" , is_custname);
	}

}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
	if(a_dg.id == 'dg_1') {
		var ls_cust = (a_newrow > 0 ? dg_1.GetItem(a_newrow, 'CUST_CODE') : is_cust);
		if(ls_cust!=is_precust) {
			var ls_year = _X.StrPurify(S_FROM_DATE.value).substr(0,4);
			dg_2.Retrieve([mytop._CompanyCode, ls_cust, ls_year]);
			dg_3.Retrieve([mytop._CompanyCode, ls_cust, ls_year + '0301', _X.LastDate((_X.ToInt(ls_year) +1) + '0201')]);
			if(dg_2.RowCount()>0) {
				dg_2.SetItem(1,'USED_AMT', (dg_3.RowCount()<=0 ? 0 :dg_3.GetItem(dg_3.RowCount(),'USED_AMT')));
				dg_2.ResetRowsStates();
			}
			is_precust = ls_cust;
		}
	}
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
	if(a_dg.id == 'dg_1'){
		var ls_orderno = dg_1.GetItem(a_row, "ORDER_NO");
		var ls_cust    = dg_1.GetItem(a_row, "CUST_CODE");
		uf_openedit(ls_orderno, ls_cust);
	}
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

//등록화면 오픈
function uf_openedit(a_orderno, a_cust) {
	if(mytop._UserTag=="05") return;

	_PT.FindPT02051(window, "PT02051","", new Array(a_orderno, a_cust));
}

//공급자확정
function uf_buyerchk(){
	var checkedRows = dg_1.GetCheckedRows();
	if(checkedRows.length == 0) {
		_X.MsgBox("선택된 데이터가 없습니다.");
		return false;
	}

	if(_X.MsgBoxYesNo("확인", "선택된 데이터를 확정처리하시겠습니까?") != 1) return;

	var result = _X.GetGridCheckedKeys(dg_1, ["ROWID"]);

	if(result!=null) {
		_X.EP("pt", "SP_BM_ORDE_ORDERLIST_CHK", [result.NUM, "Y", mytop._UserID, mytop._ClientIP]);
		x_DAO_Retrieve();
		_X.MsgBox("확인", "확정처리가 완료되었습니다!");
	}
}

//공급자확정취소
function uf_buyerchkcancel(){
	var checkedRows = dg_1.GetCheckedRows();
	if(checkedRows.length == 0) {
		_X.MsgBox("선택된 데이터가 없습니다.");
		return false;
	}

	if(_X.MsgBoxYesNo("확인", "선택된 데이터를 확정취소하시겠습니까?\n배송확인 된 데이터는 취소되지 않습니다.") != 1) return;

	var result = _X.GetGridCheckedKeys(dg_1, ["ROWID"]);

	if(result!=null) {
		_X.EP("pt", "SP_BM_ORDE_ORDERLIST_CHK", [result.NUM, "N", mytop._UserID, mytop._ClientIP]);
		x_DAO_Retrieve();
		_X.MsgBox("확인", "확정취소가 완료되었습니다!");
	}
}


//거래명세서
function uf_invoice() {

	mytop.rpt_caller = window;

	if(dg_1.RowCount() < 1) {
		return;
	}
	var ls_school = dg_1.GetItem(dg_1.GetRow(), "CUST_CODE");
	ptdwo.SetReport('om','dr_om04252_rice',false);
	//var ls_stddate = sle_basic.value;
	var ls_stddate = _X.ToString(_X.GetSysDate(), "yyyymmdd");
	var ls_gbn = '6';
	ptdwo.RptRetrieve(mytop._CompanyCode,ls_school, _X.StrPurify(S_FROM_DATE.value), _X.StrPurify(S_TO_DATE.value), ls_gbn, ls_stddate, 'S');
	return 0;

}

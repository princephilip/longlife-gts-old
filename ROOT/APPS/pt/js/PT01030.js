//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : 학교기본정보관리
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
var is_custcode  = mytop._CustCode;

function x_InitForm2(){
	_X.Tabs(tabs_1, 0);

	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "pt", "PT01030", "PT01030|BM_CODE_CUST_R01", true, true);
	_X.InitGrid(grid_2, "dg_2", "100%", "100%", "pt", "PT01030", "PT01030|BM_CHARGE_MAN_C01", false, true);

	if(_X.HaveActAuth("시스템관리자")) is_custcode = '%';
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		var la_SUPPORT_GRADE = _X.XmlSelect("com", "COMMON", "BM_COMM_DETAIL" , new Array("07", '%', '%'), 'json2');
		var la_SGRP_CODE     = _X.XmlSelect("com", "COMMON", "BM_COMM_DETAIL" , new Array("BM01", '%', 'Y'), 'json2');
		var la_SCHOOL_GBN    = _X.XmlSelect("com", "COMMON", "BM_COMM_DETAIL" , new Array("BM02", '%', 'Y'), 'json2');

		_X.DDLB_SetData(SUPPORT_GRADE, la_SUPPORT_GRADE, null, false, false);
		_X.DDLB_SetData(SGRP_CODE    , la_SGRP_CODE    , null, false, false);
		_X.DDLB_SetData(SCHOOL_GBN   , la_SCHOOL_GBN   , null, false, false);

		dg_1.SetCombo("SUPPORT_GRADE", la_SUPPORT_GRADE);
		dg_1.SetCombo("SGRP_CODE"    , la_SGRP_CODE);
		dg_1.SetCombo("SCHOOL_GBN"   , la_SCHOOL_GBN);

		setTimeout("x_DAO_Retrieve()",100);
	}
}

function x_DAO_Retrieve2(a_dg){
	var ls_div = '01';
	var ls_find = is_custcode;
	var ls_use = 'Y';
	dg_1.Retrieve([ls_div, ls_find, ls_use, '%', mytop._CompanyCode]);
}

function xe_EditChanged2(a_obj, a_val){
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){

	return true;
}

//저장후 호출
function x_DAO_Saved2(){
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}

function x_Insert_After2(a_dg, rowIdx){
	if(dg_1.RowCount() <= 0) return;

	if(a_dg.id == 'dg_2') {
		var li_max = dg_2.GetTotalValue("CHARGE_CODE", "MAX");

		dg_2.SetItem(rowIdx, "CUST_CODE"  , dg_1.GetItem(dg_1.GetRow(), "CUST_CODE"));
		dg_2.SetItem(rowIdx, "USE_YN"     , "Y");
		dg_2.SetItem(rowIdx, "CHARGE_CODE", li_max + 1);
		dg_2.SetItem(rowIdx, "COMP_CODE"  , mytop._CompanyCode);
	}

}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	if(a_dg.id == "dg_1") {
  }

	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
	if(a_dg.id == 'dg_1') {
		var ls_custcode = dg_1.GetItem(a_newrow, "CUST_CODE");
		dg_2.Retrieve([ls_custcode, mytop._CompanyCode]);
	}
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){ //a_col <-> a_colname 위치변경
}

function x_ReceivedCode2(a_retVal){
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){}
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

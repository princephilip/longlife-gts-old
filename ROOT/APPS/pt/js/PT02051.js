//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : 주문서등록_쌀(팝업)
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
//var is_cust = mytop._CustCode;
var is_orderno = "";
var _ITEMS_I;

function x_InitForm2(){
  if(typeof(_Caller._FindCode.findcode)!="undefined") {
    is_orderno  = _Caller._FindCode.findcode[0];
  } else {
  	x_Close();
  }

	_ITEMS_I = new Array(REMARKS2);

	$("#btn_cnfm").hide();
	$("#btn_cncl").hide();
	$("#btn_dlvy").hide();

	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "pt", "PT02051", "PT02051|PT02051_C01", true, true);
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		var la_time = _X.XmlSelect("pt", "PT02051", "GET_TIME" , [], 'json2');

		_X.DDLB_SetData(IN_STIME, la_time, null, false, false);
		_X.DDLB_SetData(IN_ETIME, la_time, null, false, false);

		setTimeout("x_DAO_Retrieve()",100);
	}
}

function x_DAO_Retrieve2(a_dg){
	dg_1.Retrieve([is_orderno]);
	if(dg_1.RowCount() == 0) {
		dg_1.InsertRow();
		$("#btn_cnfm").show();
	}

	uf_setItem();
	uf_setBuyer();

	if(dg_1.GetItem(dg_1.GetRow(), "ORDER_YN")=="Y"){
		_X.FormSetDisable(_ITEMS_I, true);
		$("#btn_cnfm").hide();
		if(dg_1.GetItem(dg_1.GetRow(), "ORDER_CHK_YN")=="Y"){
		 	$("#btn_cncl").hide();
			if(dg_1.GetItem(dg_1.GetRow(), "IN_CHK_DATE")==""||dg_1.GetItem(dg_1.GetRow(), "IN_CHK_DATE")==null){
			 	$("#btn_dlvy").show();
			}else{
			 	$("#btn_dlvy").hide();
			}

		}else{
		 	$("#btn_cncl").show();
		}
	}else{
		_X.FormSetDisable(_ITEMS_I, false);
		$("#btn_cnfm").show();
		$("#btn_cncl").hide();
		$("#btn_dlvy").hide();
	}
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id) {
		case	"CUST_CODE_BUYER":
					uf_getbuyertelno(a_val);
					break;
		case	"ITEM_CODE":
					var li_row = dg_1.GetRow();
					var ls_cust = dg_1.GetItem(li_row, "CUST_CODE");
					var ls_date = _X.ToString(dg_1.GetItem(li_row, "USE_DATE"), 'yyyy-mm-dd');

					var ls_data = _X.XmlSelect("pt", "PT02051", "GET_ITEM_INFO", [mytop._CompanyCode, ls_cust, ls_date, a_val], "array");
					dg_1.SetItem(li_row, "UNIT", ls_data[0][0]);
					dg_1.SetItem(li_row, "TAX_YN", ls_data[0][1]);

					if(dg_1.GetItem(li_row, "ITEM_QTY")||"" != ""){
						dg_1.SetItem(li_row, "SUPPLY_AMT", Number(ls_data[0][2])*dg_1.GetItem(li_row, "ITEM_QTY"));
					}
					dg_1.SetItem(li_row, "ITEM_NAME", ls_data[0][4]);
					dg_1.SetItem(li_row, "CUST_CODE_BUYER", ls_data[0][3]);
					uf_setBuyer();
					uf_getbuyertelno(ls_data[3]);
					break;
		case	"ITEM_QTY":
					var li_row = dg_1.GetRow();
					var ls_itemcode = dg_1.GetItem(li_row, "ITEM_CODE");
					var ls_cust = dg_1.GetItem(li_row, "CUST_CODE");
			 		var ls_date = _X.ToString(dg_1.GetItem(li_row, "USE_DATE"), 'yyyy-mm-dd');

					if(ls_itemcode=="" || ls_itemcode==null) return 100;

					var ls_data = _X.XmlSelect("pt", "PT02051", "GET_ITEM_INFO", [mytop._CompanyCode, ls_cust, ls_date, ls_itemcode], "array");
					dg_1.SetItem(li_row, "SUPPLY_AMT", ls_data[0][2]*Number(a_val));
					break;
		case	"USE_DATE":
					var li_row = dg_1.GetRow();
					var ls_cust = dg_1.GetItem(li_row, "CUST_CODE");
			 		var ls_date = _X.ToString(a_val, 'yyyy-mm-dd');
			 		uf_setItem();

					var ls_itemcode = dg_1.GetItem(li_row, "ITEM_CODE");

					if(ls_itemcode=="" || ls_itemcode==null) return 100;

					var ls_data = _X.XmlSelect("pt", "PT02051", "GET_ITEM_INFO", [mytop._CompanyCode, ls_cust, ls_date, ls_itemcode], "array");
					dg_1.SetItem(li_row, "SUPPLY_AMT", ls_data[0][2]*dg_1.GetItem(li_row, "ITEM_QTY"));
					uf_setBuyer();
					break;
	}
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	var cData1 	= dg_1.GetChangedData();

	if(cData1.length > 0){
		var ls_sql = "";
		for (i=0 ; i < cData1.length; i++){
			if(cData1[i].job == 'D') continue;

			if(dg_1.GetItem(cData1[i].idx, "IN_STIME") > dg_1.GetItem(cData1[i].idx, "IN_ETIME")) {
				_X.MsgBox("확인", "배송요청 시작시간이 종료시간보다 큽니다!");
				return false;
			}

			if(cData1[i].job == 'I'){
				//주문번호
				var ls_cust = dg_1.GetItem(cData1[i].idx, "CUST_CODE");
				var ls_date = _X.ToString(dg_1.GetItem(cData1[i].idx, "ORDER_DATE"), 'yyyymmdd');
				var ls_order = _X.XmlSelect("pt", "PT02051", "MAX_ORDER_NO", [mytop._CompanyCode, ls_cust, ls_date], "array")[0][0];
		  	is_orderno = ls_order;

				dg_1.SetItem(cData1[i].idx, "ORDER_NO", is_orderno);
				dg_1.SetItem(cData1[i].idx, "ORDER_SEQ", 900);
			}
		}
	}

	return true;
}

//저장후 호출
function x_DAO_Saved2(){
	var ls_cust = dg_1.GetItem(dg_1.GetRow(), "CUST_CODE");
	var ls_date = _X.ToString(dg_1.GetItem(dg_1.GetRow(), "ORDER_DATE"), 'yyyy-mm-dd');
	_X.ES("pt", "PT02051", "BM_ORDE_MASTER_C01", [mytop._CompanyCode, ls_cust, ls_date, is_orderno, mytop._UserID, mytop._ClientIP]);
	x_DAO_Retrieve();
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
	if(a_dg.id == 'dg_1') {
		var ls_date = _X.ToString(_X.GetSysDate(), "yyyy-mm-dd");
		dg_1.SetItem(rowIdx, "ORDER_DATE", ls_date);
		dg_1.SetItem(rowIdx, "USE_DATE", ls_date);
		dg_1.SetItem(rowIdx, "COMP_CODE", mytop._CompanyCode);
		dg_1.SetItem(rowIdx, "CUST_CODE", mytop._CustCode);
		dg_1.SetItem(rowIdx, "CUST_NAME", mytop._CustName);
		dg_1.SetItem(rowIdx, "ORDER_STATUS", "등록");
		dg_1.SetItem(rowIdx, "ORDER_YN", "N");
		dg_1.SetItem(rowIdx, "IN_STIME", "07");
		dg_1.SetItem(rowIdx, "IN_ETIME", "07");

		xe_EditChanged(USE_DATE, ls_date);
	}

}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	if(dg_1.GetItem(dg_1.GetRow(), "ORDER_CHK_YN")=="Y"){
		_X.MsgBox("확인", "공급업체 확정 이후 데이터는 삭제하실 수 없습니다!");
		return;
	}

	if(_X.MsgBoxYesNo("삭제", "데이터를 삭제하시겠습까?\n(삭제한 데이터는 복구할 수 없습니다.)") != 1) return 1;

	var ls_orderno = dg_1.GetItem(dg_1.GetRow(), "ORDER_NO");

	var ll_cnt = Number(_X.XmlSelect("pt", "PT02051", "CHK_ORDER_DEL", [ls_orderno], "array")[0][0]);
	if(ll_cnt > 1){
		_X.MsgBox("확인", "발주처리가 완료되어 삭제하실수 없습니다.\n담당자에게 연락해 주십시오.");
		return ;
	}

	var li_ret = _X.EP('pt','SP_BM_ORDE_DELETE2', [ls_orderno]);
	if(li_ret < 0) {
		_X.MsgBox("확인", "데이터 삭제에 실패하였습니다!");
	} else {
		_X.MsgBox("확인", "데이터를 삭제 하였습니다.");
		is_orderno = "";
	}

	x_DAO_Retrieve();

	return 0;
	//return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
	if(a_retVal == null) return;

	switch(_FindCode.finddiv){
		case	"CUST_CODE":
					dg_1.SetItem(dg_1.GetRow(), "CUST_CODE", _FindCode.returnValue.CUST_CODE);
					dg_1.SetItem(dg_1.GetRow(), "CUST_NAME", _FindCode.returnValue.CUST_NAME);
					uf_setItem();
					uf_setBuyer();
					break;
	}
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	_Caller.x_DAO_Retrieve();
  if(_IsModal || _IsPopup) {
    window.close();
  }else{
    if(_Caller.$("#findmodal").length>0?_Caller.$("#findmodal").dialog("close"):window.close());
  }
}

// 학교검색
function uf_findCust() {
	if(mytop._UserTag != '51' && mytop._UserTag != '09') return;
	_X.CommonFindCode(window,"거래처찾기","com",'CUST_CODE','',[],"FindCust|FindCode|FindCustCode",$(document).width()-10, $(document).height()-10);
}

function uf_setItem() {
	var ls_cust = dg_1.GetItem(dg_1.GetRow(), "CUST_CODE");
	var ls_date = _X.ToString(dg_1.GetItem(dg_1.GetRow(), "USE_DATE"), "YYYY-MM-DD");

	var la_item = _X.XmlSelect("pt", "PT02051", "GET_RICE_ITEM" , [mytop._CompanyCode, ls_cust, ls_date], 'json2');
	_X.DDLB_SetData(ITEM_CODE, la_item, null, false, true, "선택");
}

function uf_setBuyer() {
	var ls_cust = dg_1.GetItem(dg_1.GetRow(), "CUST_CODE");
	var ls_item = dg_1.GetItem(dg_1.GetRow(), "ITEM_CODE");
	var ls_date = _X.ToString(dg_1.GetItem(dg_1.GetRow(), "USE_DATE"), "YYYY-MM-DD");

	var la_item = _X.XmlSelect("pt", "PT02051", "GET_RICE_BUYER" , [mytop._CompanyCode, ls_cust, ls_item, ls_date], 'json2');
	_X.DDLB_SetData(CUST_CODE_BUYER, la_item, null, false, true, "선택");
}

//발주하기
function uf_order_confirm() {
	if(dg_1.RowCount() < 1) return ;
	var ls_orderyn = dg_1.GetItem(dg_1.GetRow(), "ORDER_YN");
	if(ls_orderyn=="Y"){
		_X.MsgBox("확인", "이미 발주처리 되었습니다!");
		return;
	}

	if(_X.MsgBoxYesNo("확인", "발주처리하시겠습니까?") != 1) return;

	dg_1.Save(null, null, 'N');

	var ls_order_no = dg_1.GetItem(i, "ORDER_NO");
	var li_ret = _X.EP('pt','SP_OM_ORDERLISTMAKERICE', [mytop._CompanyCode, ls_order_no]);
	if(li_ret < 0) {
		_X.MsgBox("확인", "발주 처리중 오류가 발생 했습니다.");
	} else {
		_X.MsgBox("확인", "발주 처리 되었습니다.");
	}
	x_DAO_Retrieve();
}

function uf_order_cancel() {
	if(dg_1.RowCount() < 1) return ;
	if(_X.MsgBoxYesNo("확인", "발주취소하시겠습니까?") != 1) return;

	dg_1.Save(null, null, 'N');

	var ls_order_no = dg_1.GetItem(i, "ORDER_NO");
	var li_ret = _X.EP('pt','SP_OM_ORDERLISTDELRICE', [mytop._CompanyCode, ls_order_no]);
	if(li_ret < 0) {
		_X.MsgBox("확인", "발주 취소처리중 오류가 발생 했습니다.");
	} else {
		_X.MsgBox("확인", "발주 취소처리 되었습니다.");
	}

	x_DAO_Retrieve();
}

//배송확인
function uf_confirm_delivery(){
	if(dg_1.RowCount() < 1) return ;
	if(_X.MsgBoxYesNo("확인", "배송확인 처리 하시겠습니까?") != 1) return;

	var ls_orderno = dg_1.GetItem(dg_1.GetRow(), "ORDER_NO");

	_X.ES("pt", "PT02051", "UPDATE_ORDER_CONFIRM", [ls_orderno, mytop._UserID, mytop._ClientIP]);

	_X.MsgBox("확인", "배송확인 처리가 완료되었습니다!");
	x_DAO_Retrieve();
}

function uf_getbuyertelno(a_buyer){
	var ls_telno = _X.XmlSelect("pt", "PT02051", "GET_BUYER_TELNO", [mytop._CompanyCode, a_buyer], "array")[0][0];
	dg_1.SetItem(dg_1.GetRow(), "TEL_NO", ls_telno);
}

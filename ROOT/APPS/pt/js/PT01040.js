//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : 요청사항등록
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
var is_user  = mytop._CustCode;
var is_user_receiver = '9000001';
var _ITEMS_I;

function x_InitForm2(){
	_ITEMS_I = new Array(REQ_DIV, CONTENTS);
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "pt", "PT01040", "PT01040|BM_MESSAGE_C01", true, true);
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		var la_END_YN = [{"code":"Y", "label":"확인"}, {"code":"N", "label":"미확인"}];
		_X.DDLB_SetData(S_END_YN, la_END_YN, null, false, true);

		var la_REQ_DIV = _X.XmlSelect("com", "COMMON", "BM_COMM_DETAIL" , new Array("54", '%', 'Y'), 'json2');
		_X.DDLB_SetData(REQ_DIV, la_REQ_DIV, null, false, false);
		dg_1.SetCombo("REQ_DIV", la_REQ_DIV);

		// dg_1.SetAutoReadOnly("REQ_DIV", function (args) {
		// 	if(args.dataContext.END_YN == "Y") return true;
		// }, false);

		// dg_1.SetAutoReadOnly("CONTENTS", function (args) {
		// 	if(args.dataContext.END_YN == "Y") return true;
		// }, false);

		_X.FormSetDisable(_ITEMS_I, true);

		setTimeout("x_DAO_Retrieve()",100);
	}
}

function x_DAO_Retrieve2(a_dg){
	//is_user = "1000053";
	dg_1.Retrieve([is_user, S_END_YN.value, mytop._CompanyCode]);
}

function xe_EditChanged2(a_obj, a_val){
	x_DAO_Retrieve();
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){

	return true;
}

//저장후 호출
function x_DAO_Saved2(){
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}

function x_Insert_After2(a_dg, rowIdx){
	if(a_dg.id == 'dg_1') {
		var li_next = Number(_X.XmlSelect("pt", "PT01040", "GET_NEXT_VAL", [], "array")[0][0]);

		dg_1.SetItem(rowIdx, "COMP_CODE"  , mytop._CompanyCode);
		dg_1.SetItem(rowIdx, "MESSAGE_ID" , li_next);
		dg_1.SetItem(rowIdx, "SENDER"     , is_user);
		dg_1.SetItem(rowIdx, "RECEIVER"   , is_user_receiver);
		dg_1.SetItem(rowIdx, "OPEN_YN"    , "N");
		dg_1.SetItem(rowIdx, "END_YN"     , "N");
		dg_1.SetItem(rowIdx, "REQ_DIV"    , "03");
		dg_1.SetItem(rowIdx, "SUB_SEQ"    , 0);
	}

}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	if(a_dg.id == "dg_1") {
		if(dg_1.GetItem(row, "END_YN") == "Y") {
			_X.MsgBox("확인", "확인완료 된 내용은 삭제 불가능 합니다.");
			return 0;
		}
  }

	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
	if(a_dg.id == 'dg_1') {
		if(dg_1.GetItem(a_newrow, "END_YN") == "Y") {
			_X.FormSetDisable(_ITEMS_I, true);
		} else {
			_X.FormSetDisable(_ITEMS_I, false);
		}

	}
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){ //a_col <-> a_colname 위치변경
}

function x_ReceivedCode2(a_retVal){
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){}
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

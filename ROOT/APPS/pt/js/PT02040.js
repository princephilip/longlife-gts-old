//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : 발주견적(학교)
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
var _CustCode = mytop._CustCode;

function x_InitForm2(){
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "pt", "PT02040", "PT02040|PT02040_C01", false, true);
	_X.InitGrid(grid_2, "dg_2", "100%", "100%", "pt", "PT02040", "PT02040|PT02040_C02", false, true);
	_X.InitGrid(grid_3, "dg_3", "100%", "100%", "pt", "PT02040", "PT02040|PT02040_R03", false, false);
	_X.InitGrid(grid_down, "dg_down", "100%", "100%", "pt", "PT02040", "PT02040|EXCEL_R04", false, false);

	if(_X.HaveActAuth("시스템관리자")) _CustCode = '1000001';

	//_CustCode = '1000089';
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		var la_APPLYCOST = _X.XmlSelect("com", "COMMON", "BM_COMM_DETAIL" , new Array("BM01", '%', 'Y'), 'json2');
		_X.DDLB_SetData(S_APPLYCOST, la_APPLYCOST, null, false, false);

		dg_1.SetCombo("SGRP_CODE", la_APPLYCOST);

		dg_2.SetAutoFooterValue("BUY_CUST_CODE", function(idx, ele) {
			var datas = dg_2.GetData();
			var sum   = 0;
			var i, ii;
			for ( i = 0, ii = datas.length; i < ii; i++ ) {
				sum += Number(datas[i].SUPPLY_AMT) + Number(datas[i].TAX_AMT);
			}
			return _X.FormatComma(sum);
		});

		dg_3.SetAutoFooterValue("BUY_CUST_CODE", function(idx, ele) {
			var datas = dg_3.GetData();
			var sum   = 0;
			var i, ii;
			for ( i = 0, ii = datas.length; i < ii; i++ ) {
				sum += Number(datas[i].SUPPLY_AMT) + Number(datas[i].TAX_AMT);
			}
			return _X.FormatComma(sum);
		});


		setTimeout("x_DAO_Retrieve()",100);
	}
}

function x_DAO_Retrieve2(a_dg){
	var ls_yymm = _X.StrPurify(sle_basicmonth.value);

	dg_2.Reset();
	dg_3.Reset();
	dg_1.Retrieve([mytop._CompanyCode, _CustCode, ls_yymm]);
}

function xe_EditChanged2(a_obj, a_val){
	x_DAO_Retrieve();
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	// var cData1 	= dg_1.GetChangedData();

	// if(cData1.length > 0){
	// 	var li_seq = dg_1.GetTotalValue("SEQ", "MAX");

	// 	for (i=0 ; i < cData1.length; i++){
	// 		if(cData1[i].job == 'D') continue;

	// 		if(dg_1.GetItem(cData1[i].idx, "NEIS_CODE") == "" || dg_1.GetItem(cData1[i].idx, "NEIS_CODE") == null) {
	// 			dg_1.SetRow(cData1[i].idx);
	// 			_X.MsgBox("확인", cData1[i].idx + " 번째 행의 식품데이터가 입력되지 않았습니다.");
	// 			return false;
	// 		}

	// 		if(cData1[i].job == 'I'){
	// 			li_seq++;
	// 			dg_1.SetItem(cData1[i].idx, "SEQ", li_seq);
	// 		}
	// 	}
	// }

	return true;
}

//저장후 호출
function x_DAO_Saved2(){
}

function x_DAO_Insert2(a_dg, row){
	if(a_dg.id == 'dg_2') {
		if(dg_1.RowCount() == 0) return 0;

		if(dg_1.IsDataChanged()) {
			_X.MsgBox("확인", "상단 견적데이터를 저장 후 추가하세요!");
			return 0;
		}

		if(dg_1.GetItem(dg_1.GetRow(), "FIX_YN")=="Y"){
			_X.MsgBox("확인", "확정된 데이터는 수정하실 수 없습니다!");
			return;
		}
	}

  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
	if(a_dg.id == 'dg_1') {
		var li_max = dg_1.GetTotalValue("SEQ", "MAX");

		dg_1.SetItem(rowIdx, "COMP_CODE", mytop._CompanyCode);
		dg_1.SetItem(rowIdx, "CUST_CODE", _CustCode);
		dg_1.SetItem(rowIdx, "YYYYMM"   , _X.StrPurify(sle_basicmonth.value));
		dg_1.SetItem(rowIdx, "SEQ"      , li_max + 1);
		dg_1.SetItem(rowIdx, "ESTI_DATE", _X.ToString(_X.GetSysDate(),'YYYYMMDD'));
	}

	if(a_dg.id == 'dg_2') {
		var li_max = dg_1.GetTotalValue("ITEM_SEQ", "MAX");

		dg_2.SetItem(rowIdx, "COMP_CODE", mytop._CompanyCode);
		dg_2.SetItem(rowIdx, "CUST_CODE", _CustCode);
		dg_2.SetItem(rowIdx, "YYYYMM"   , _X.StrPurify(sle_basicmonth.value));
		dg_2.SetItem(rowIdx, "SEQ"      , dg_1.GetItem(dg_1.GetRow(), "SEQ"));
		dg_2.SetItem(rowIdx, "ITEM_SEQ" , li_max + 1);
	}

}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	if(a_dg.id == "dg_1") {
		if(_X.MsgBoxYesNo("삭제", "데이터를 삭제하시겠습까?</br>(삭제한 데이터는 복구할 수 없습니다.)") == 2) return 1;

		var ls_yymm = dg_1.GetItem(row, "YYYYMM");
		var li_seq  = dg_1.GetItem(row, "SEQ");
		var li_ret = _X.EP('pt','SP_BM_ESTIMATE_DEL', [mytop._CompanyCode, _CustCode, ls_yymm, li_seq]);
		if(li_ret < 0) {
			_X.MsgBox("확인", "발주견적 삭제중 오류가 발생 했습니다.");
			return 1;
		}

		x_DAO_Retrieve();
		return 0;
  }

	if(a_dg.id == "dg_2") {
		if(dg_1.GetItem(dg_1.GetRow(), "FIX_YN")=="Y"){
			_X.MsgBox("확인", "확정된 데이터는 수정하실 수 없습니다!");
			return;
		}
  }

	return 100;
}

function x_DAO_Excel2(a_dg){
	dg_2.ExcelExport(true, "발주견적(학교)_" + _X.ToString(_X.GetSysDate(), "YYYYMMDD_HHMISS"));
	return 0;
	//return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	if(a_dg.id == 'dg_1') {
		switch(a_col) {
			case	"FIX_YN":
						if(a_newvalue == "Y") {
							dg_1.SetItem(a_row,"FIX_DATE", _X.ToString(_X.GetSysDate(),'YYYYMMDD'));
						} else {
							dg_1.SetItem(a_row,"FIX_DATE", "");
						}
						break;
		}
	}

	if(a_dg.id == 'dg_2') {
		switch(a_col) {
			case	"CONVERT_STD":  		// 환산기준
						dg_2.SetItem(a_row,"CONVERT_COST",Number(a_newvalue)*dg_2.GetItem(a_row,"COST"));
						dg_2.SetItem(a_row,"ITEM_COST"   ,Number(a_newvalue)*dg_2.GetItem(a_row,"COST"));
						uf_calcAmt(a_row);
						break;
			case	"COST":  		// 매출단가
						dg_2.SetItem(a_row,"CONVERT_COST",Number(a_newvalue)*dg_2.GetItem(a_row,"CONVERT_STD"));
						dg_2.SetItem(a_row,"ITEM_COST"   ,Number(a_newvalue)*dg_2.GetItem(a_row,"CONVERT_STD"));
						uf_calcAmt(a_row);
						break;
			case	"CONVERT_COST":  		// 환산단가
						dg_2.SetItem(a_row,"ITEM_COST",Number(a_newvalue));
						uf_calcAmt(a_row);
						break;
			case	"ITEM_COST":  		// 입찰단가
			case	"ITEM_QTY":  		// 총량
						uf_calcAmt(a_row);
						break;
		}
	}
}

function uf_calcAmt(a_row) {
	dg_2.SetItem(a_row, "SUPPLY_AMT", dg_2.GetItem(a_row, "ITEM_COST")*dg_2.GetItem(a_row, "ITEM_QTY"));
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
	if(a_dg.id == 'dg_1') {
		dg_3.Reset();
		var ls_yymm = _X.StrPurify(sle_basicmonth.value);
		var li_seq  = dg_1.GetItem(a_newrow, 'SEQ');
		dg_2.Retrieve([mytop._CompanyCode, _CustCode, ls_yymm, li_seq]);
	}

	if(a_dg.id == 'dg_2') {
		var ls_yymm = _X.StrPurify(sle_basicmonth.value);
		var li_seq     = dg_2.GetItem(a_newrow, "SEQ");
		var li_itemseq = dg_2.GetItem(a_newrow, "ITEM_SEQ");
		var ls_nice    = dg_2.GetItem(a_newrow, "NEIS_NAME");
		dg_3.Retrieve([mytop._CompanyCode, _CustCode, ls_yymm, li_seq, li_itemseq, ls_nice]);
	}
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
	if(a_dg.id == 'dg_2') {
		uf_Set_Color();
	}
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
	if(a_dg.id == 'dg_2') {
		switch(a_colname) {
			case	"NEIS_NAME": 	// 식품명
						var ls_neis = dg_2.GetItem(a_row, "NEIS_NAME");
						_X.CommonFindCode(window,"품목분류검색","com",'NEIS_CODE',ls_neis,[],"FindCodeNeis|FindCode|IF_CODE_NEIS_R01",500, 700);
						break;
			case	"ITEM_NAME": 	// 센터품목명
						var ls_date = _X.StrPurify(sle_basicmonth.value)+"01";
						var ls_neis = dg_2.GetItem(a_row, "NEIS_NAME");
						_X.CommonFindCode(window,"품목검색","com",'ITEM_CODE',ls_neis,['%', ls_date, '%', mytop._CompanyCode],"FindCodeItem|FindCode|BM_CODE_ITEM_R01",1200, 600);
						break;
			case	"BUY_CUST_NAME": 	// 매입처
						_X.CommonFindCode(window,"매입처검색","com",'BUY_CUST_CODE','',['Y', mytop._CompanyCode],"FindCust|FindCode|BM_CODE_CUST_R01",1200, 600);
						break;
		}
	}
}

function x_ReceivedCode2(a_retVal){
	if(_FindCode.returnValue == null) return;

	switch(_FindCode.finddiv) {
		case	"NEIS_CODE" :
					var ls_code = _FindCode.returnValue.NEIS_CODE;
					var ls_name = _FindCode.returnValue.NEIS_NAME;
					dg_2.SetItem(dg_2.GetRow(), "NEIS_CODE", ls_code);
					dg_2.SetItem(dg_2.GetRow(), "NEIS_NAME", ls_name);
					dg_2.SetItem(dg_2.GetRow(), "NEIS_MATCH_YN", "Y");
					break;
		case	"ITEM_CODE" :
					dg_2.SetItem(dg_2.GetRow(), "ITEM_NAME"  , _FindCode.returnValue.ITEM_NAME);
					dg_2.SetItem(dg_2.GetRow(), "ITEM_DNAME" , _FindCode.returnValue.ITEM_DNAME);
					dg_2.SetItem(dg_2.GetRow(), "UNIT"       , _FindCode.returnValue.UNIT);
					dg_2.SetItem(dg_2.GetRow(), "ITEM_STD"   , _FindCode.returnValue.ITEM_STD);
					dg_2.SetItem(dg_2.GetRow(), "ITEM_CODE"  , _FindCode.returnValue.ITEM_CODE);
					dg_2.SetItem(dg_2.GetRow(), "BRAND_NAME" , _FindCode.returnValue.BRAND_NAME);
					dg_2.SetItem(dg_2.GetRow(), "TAX_YN"     , _FindCode.returnValue.TAX_YN);
					dg_2.SetItem(dg_2.GetRow(), "COST"       , Number(_FindCode.returnValue.COST));
					dg_2.SetItem(dg_2.GetRow(), "QTY_CHANGE" , Number(_FindCode.returnValue.QTY_CHANGE));
					dg_2.SetItem(dg_2.GetRow(), "ITEM_MATCH_YN", "Y");
					dg_2.SetItem(dg_2.GetRow(), "CONVERT_STD", 1);
					dg_2.SetItem(dg_2.GetRow(), "CONVERT_COST", Number(_FindCode.returnValue.COST));
					dg_2.SetItem(dg_2.GetRow(), "ITEM_COST", Number(_FindCode.returnValue.COST));
					dg_2.SetItem(dg_2.GetRow(), "SUPPLY_AMT", Number(_FindCode.returnValue.COST) * Number(dg_2.GetItem(dg_2.GetRow(), "ITEM_QTY")));
					break;
		case	"BUY_CUST_CODE" :
					var ls_code = _FindCode.returnValue.CUST_CODE;
					var ls_name = _FindCode.returnValue.CUST_NAME;
					dg_2.SetItem(dg_2.GetRow(), "BUY_CUST_CODE", ls_code);
					dg_2.SetItem(dg_2.GetRow(), "BUY_CUST_NAME", ls_name.split('/')[0]);
					break;
	}
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){}
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

function uf_Set_Color() {
	var s_red = { "color": "#FF0000" };
	var s_blk = { "color": "#000000" };

	for(var i = 1; i <= dg_2.RowCount(); i++){
		var ll_cost = dg_2.GetItem(i, "ITEM_COST");
		var ll_std  = dg_2.GetItem(i, "CONVERT_STD");
		var ll_min  = dg_2.GetItem(i, "MIN_PRICE");
		dg_2.SetCellStyle(i, "ITEM_COST", ( ll_cost == 0 || ll_cost < (ll_std*ll_min) )  ? s_red : s_blk);

		var ls_match = dg_2.GetItem(i, "NEIS_MATCH_YN");
		dg_2.SetCellStyle(i, "NEIS_CODE", ( ls_match == 'N' )  ? s_red : s_blk);
		dg_2.SetCellStyle(i, "NEIS_NAME", ( ls_match == 'N' )  ? s_red : s_blk);
	}
}

function uf_cost_update(){
	if(dg_1.GetRow() < 1){
		_X.MsgBox("확인","수정할 견적서를 선택하세요!");
		return;
	}

	var ls_yymm = _X.StrPurify(sle_basicmonth.value);
	var li_seq  = dg_1.GetItem(dg_1.GetRow(), "SEQ");

	var li_ret = _X.EP('pt','SP_BM_ESTIITEM_UPD2', [mytop._CompanyCode, _CustCode, ls_yymm, ls_seq]);
	if(li_ret < 0) {
		_X.MsgBox("확인", "발주견적 단가반영중 오류가 발생 했습니다.");
		return;
	}

	x_DAO_Retrieve();
}

function uf_dw_excel(){
	if(dg_1.RowCount() < 1) return;
	var li_row = dg_1.GetRow();

	var ls_yymm     = _X.StrPurify(sle_basicmonth.value);
	var li_seq      = dg_1.GetItem(li_row, "SEQ");
	var ls_estiname = dg_1.GetItem(li_row, "ESTI_NAME");
	dg_down.Retrieve([mytop._CompanyCode, _CustCode, ls_yymm, li_seq]);
	//dg_down.SetItem(1, "NEIS_NAME_T", ls_estiname);
	dg_down.ExcelExport(true, "발주견적(학교)_" + _X.ToString(_X.GetSysDate(), "YYYYMMDD_HHMISS"));
}


function uf_dataupload(){
	var ls_yymm = _X.StrPurify(sle_basicmonth.value);
	var ls_sgrpcode = S_APPLYCOST.value;

	dg_1.ExcelUpload("pt", function (args) {
		_X.EP("pt", "SP_BM_ESTI_UPLOAD", [mytop._CompanyCode, _CustCode, ls_yymm, ls_sgrpcode, "PT02040", mytop._UserID, mytop._ClientIP], function (res) {
			if ( res != -1 ) {
				_X.MsgBox("엑셀데이타 업로드 완료.");
				x_DAO_Retrieve();
			}
			else {
				_X.MsgBox("엑셀데이타 업로드 중 오류가 발생 하였습니다.");
			}
		});
	},
	null, 1, "PT02040", false);
}


//===========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//===========================================================================================
// 프로그램명 : PT01010|홈
// --------------------------------------- 변경이력 ----------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    ---------------------------------------
//   jhbae         엑스인터넷정보      20171025
//===========================================================================================
//
//===========================================================================================

_XU.Initial = function() {
  // // 상태정보
  // _XV.status_info = new Vue({
  //   el: '#status-info',
  //   data: {
  //     item: null
  //   },
  //   updated: function() {
  //     $("#status-info .stats").on("click", function() {
  //       _XU.set_status();
  //     });
  //   }
  // });

  // // 상태정보 세팅
  // _XU.set_status();

  // // 납품할발주목록
  // _XV.ordr_list = new Vue({
  //   el: '#ordr-list',
  //   data: {
  //     data: []
  //   }
  // });

  // // 정산할발주목록
  // _XV.acct_list = new Vue({
  //   el: '#acct-list',
  //   data: {
  //     data: []
  //   }
  // });

  // // 입찰목록
  // _XV.bidd_list1 = new Vue({
  //   el: '#bidd-list1',
  //   data: {
  //     data: []
  //   }
  // });

  // // 견적목록
  // _XV.bidd_list2 = new Vue({
  //   el: '#bidd-list2',
  //   data: {
  //     data: []
  //   }
  // });

  // //----_XU.get_ordr_list();
  // //----_XU.get_acct_list();
  // //----_XU.get_bidd_list();
}

// 상태정보 세팅
_XU.set_status = function() {
  var ls_status = _X.XmlSelect('pt', 'PT01010', 'GET_STATUS', [_CompanyCode, _UserID], 'json2');
  _XV.status_info.item = ls_status[0];
}

// 납품할발주목록
_XU.get_ordr_list = function() {
  var v_cnt = 7; // 보여줄건수
  var data_cnt = 0;

  var ls_ordr = _X.XmlSelect('pt', 'PT01010', 'GET_ORDER_LIST', [_CompanyCode, _UserID], 'json2');
  data_cnt = ls_ordr.length;
  if(data_cnt < v_cnt) {
    for(var i=data_cnt; i < v_cnt; i++) {
      ls_ordr.push('');
    }
  }
  _XV.ordr_list.data = ls_ordr;
}


// 정산할발주목록
_XU.get_acct_list = function() {
  var v_cnt = 7; // 보여줄건수
  var data_cnt = 0;

  var ls_acct = _X.XmlSelect('pt', 'PT01010', 'GET_ACCT_LIST', [_CompanyCode, _UserID], 'json2');
  data_cnt = ls_acct.length;
  if(data_cnt < v_cnt) {
    for(var i=data_cnt; i < v_cnt; i++) {
      ls_acct.push('');
    }
  }
  _XV.acct_list.data = ls_acct;
}


// 입찰/견적요청 목록 세팅
_XU.get_bidd_list = function() {
  var v_cnt = 5; // 보여줄건수
  var data_cnt = 0;

  // 입찰
  var ls_bidds1 = _X.XmlSelect('pt', 'PT01010', 'GET_BIDD_LIST', [_UserID, "B"], 'json2');
  data_cnt = ls_bidds1.length;
  if(data_cnt < v_cnt) {
    for(var i=data_cnt; i < v_cnt; i++) {
      ls_bidds1.push('');
    }
  }
  _XV.bidd_list1.data = ls_bidds1;

  // 견적
  var ls_bidds2 = _X.XmlSelect('pt', 'PT01010', 'GET_BIDD_LIST', [_UserID, "E"], 'json2');
  data_cnt = ls_bidds2.length;
  if(data_cnt < v_cnt) {
    for(var i=data_cnt; i < v_cnt; i++) {
      ls_bidds2.push('');
    }
  }
  _XV.bidd_list2.data = ls_bidds2;
}

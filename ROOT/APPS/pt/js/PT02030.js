//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : 주간발주서관리
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
var is_kkk = '';
var is_order = ""; //발주번호
var is_openfile =''; //엑셀화일 OPEN경로
var is_base_dir = "/FTP/upload/";

var is_cust = mytop._CustCode;

function x_InitForm2(){
	$("#btn_exceldown").hide();

	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "pt", "PT02030", "PT02030|PT02030_R01", false, false);
	_X.InitGrid(grid_2, "dg_2", "100%", "100%", "pt", "PT02030", "PT02030|PT02030_R02", false, false);
	_X.InitGrid(grid_3, "dg_3", "100%", "100%", "pt", "PT02030", "PT02030|PT02030_U03", false, true);

	if(_X.HaveActAuth("시스템관리자")) is_cust = '1000001';

	//is_cust = '1000053';	// 테스트용
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {

		var la_STATUS = [{"code":"02", "label":"미확인"}, {"code":"03", "label":"확인완료"}];
		dg_3.SetCombo("ORDER_STATUS", la_STATUS);

		dg_2.SetAutoFooterValue("AMT", function(idx, ele) {
			var datas = dg_2.GetData();
			var sum   = 0;
			var i, ii;
			for ( i = 0, ii = datas.length; i < ii; i++ ) {
				if ( datas[i].TITLE == "보조금총액" ) {
					sum += Number(datas[i].AMT);
				} else {
					sum -= Number(datas[i].AMT);
				}
			}
			return _X.FormatComma(sum);
		});

		setTimeout("x_DAO_Retrieve()",100);
	}
}

function x_DAO_Retrieve2(a_dg){
	var ls_yymm = _X.StrPurify(sle_basicmonth.value);

	dg_1.Reset();
	dg_3.Retrieve([is_cust, ls_yymm, mytop._CompanyCode]);
	dg_2.Retrieve([is_cust, ls_yymm, mytop._CompanyCode]);

	if(dg_3.RowCount() == 0) {
		$("#btn_exceldown").hide();
	} else {
		$("#btn_exceldown").show();

		dg_3.SetFocusByValue("001", ["R_NUM"]);	// 최근입력한 ROW로이동
	}

}

function uf_etc_retrieve() {
	var ls_orderno = dg_3.GetItem(dg_3.GetRow(), "ORDER_NO");
	var ls_yyyymm = dg_3.GetItem(dg_3.GetRow(), "ESTI_YYMM");

	dg_1.Retrieve([is_cust, ls_orderno, mytop._CompanyCode, ls_yyyymm]);
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id) {
		case	"sle_basicmonth":
					x_DAO_Retrieve();
					break;
	}
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

function x_DAO_Saved2(){
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
	if(a_dg.id == 'dg_3') {
		uf_set_date_txt();
		uf_etc_retrieve();
	}
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
	if(a_dg.id == 'dg_3') {
		uf_Set_Color();
	}
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){}
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

function uf_Set_Color() {
	var s_red = { "color": "#FF0000" };
	var s_blk = { "color": "#000000" };

	for(var i = 1; i <= dg_3.RowCount(); i++){
		dg_3.SetCellStyle(i, "ORDER_TERM"  , (dg_3.GetItem(i, "ORDER_STATUS") == "02") ? s_red : s_blk);
		dg_3.SetCellStyle(i, "MEAL_NAME"   , (dg_3.GetItem(i, "ORDER_STATUS") == "02") ? s_red : s_blk);
		dg_3.SetCellStyle(i, "ORDER_STATUS", (dg_3.GetItem(i, "ORDER_STATUS") == "02") ? s_red : s_blk);
		dg_3.SetCellStyle(i, "ORDER_NO"    , (dg_3.GetItem(i, "ORDER_STATUS") == "02") ? s_red : s_blk);
	}
}

function uf_ExcelImport() {
	var ls_pgmid = "PT02030";

	// 엑셀임시파일삭제
	_X.ES("com", "COMMON", "XM_EXCEL_DATA_D01", [ls_pgmid, mytop._UserID]);

	dg_1.ExcelUpload("pt", function (args) {
		_X.EP("pt", "SP_BM_ORDE_UPLOAD1", [ls_pgmid, mytop._UserID], function (res) {
			if ( res == -1 ) {
				_X.MsgBox("엑셀데이타 업로드 중 오류가 발생 하였습니다.");
				return;
			}

			var ls_from = _X.XmlSelect("pt", "PT02030", "UPLOAD_GET_FRDT", [ls_pgmid, mytop._UserID], "array")[0][0];
			var ls_end = _X.ToString(_X.RelativeDate(_X.ToDate(ls_from,'YYYY-MM-DD'), 6), 'YYYYMMDD');

			var ls_daynm = _X.XmlSelect("pt", "PT02030", "UPLOAD_GET_DAYNM", [ls_from], "array")[0][0];
			if(ls_daynm != "월요일") {
				_X.MsgBox('확인', '발주서는 월요일 부터 일요일까지 등록 할 수 있습니다.');
				return;
			}

			var ls_first_date_check = _X.XmlSelect("pt", "PT02030", "UPLOAD_GET_MONDAY", [ls_pgmid, mytop._UserID], "array")[0][0];
			if(ls_from.substr(4, 4).toString() != ls_first_date_check){
				_X.MsgBox("확인", "발주서 파일의 날짜가 일치하지 않습니다.");
				return;
			}

			/*==============요일, 시간체크, 이전꺼는 올릴수 없음========================*/
			var ls_return_chk = _X.XmlSelect("pt", "PT02030", "UPLOAD_CHK_REGTIME", [mytop._CompanyCode], "array");
			if(ls_return_chk.length > 0) {
				var ls_chktime = ls_return_chk[0][0];
				var ls_chkdate = ls_return_chk[0][1];
				if(ls_chkdate != "" && ls_chkdate != null && ls_chktime != "" && ls_chktime != null) {
					var li_datediv = 0;
					switch(ls_chkdate) {
						case '월':
								li_datediv = 7;
								break;
						case '화':
								li_datediv = 6;
								break;
						case '수':
								li_datediv = 5;
								break;
						case '목':
								li_datediv = 4;
								break;
						case '금':
								li_datediv = 3;
								break;
						case '토':
								li_datediv = 2;
								break;
						case '일':
								li_datediv = 1;
								break;

					}
					var ld_now = _X.GetSysDate();
					var ls_check_date = _X.ToString(_X.RelativeDate(ld_now, li_datediv), 'YYYYMMDD');
					var ls_time = _X.ToString(ld_now, "hh");

					if(ls_from < ls_check_date) {
						_X.MsgBox("확인", "등록 기간을 초과하였습니다. 학교급식지원센터에 문의 해 주십시요.");
						return;
					}

					if(ls_from == ls_check_date && ls_time >= ls_chktime) {
						_X.MsgBox("확인", "등록 제한시간이 지났습니다. 학교급식지원센터에 문의 해 주십시요.");
						return;
					}
				}
			}
			/*============요일, 시간체크, 이전꺼는 올릴수 없음 ==============끝=========*/

			var li_numchk = Number(_X.XmlSelect("pt", "PT02030", "UPLOAD_CHK_NUM", [ls_pgmid, mytop._UserID], "array")[0][0]);
			if(li_numchk > 0){
				_X.MsgBox("확인", "수량은 숫자만 등록 가능합니다. <br>자세한 사항은 관리자에게 문의 바랍니다.");
				return;
			}

			var ls_yymm = _X.StrPurify(sle_basicmonth.value);

			// 업로드처리
			var li_ret = _X.EP('pt','SP_BM_ORDE_UPLOAD2', [mytop._CompanyCode, is_cust, ls_yymm, ls_from, ls_end, ls_pgmid, mytop._UserID, mytop._ClientIP]);
			if(li_ret < 0) {
				_X.MsgBox("확인", "엑셀 업로드중 오류가 발생 했습니다.");
				return;
			}

			// 엑셀파일업로드
			_X.MsgBox("확인", "주문서 등록을 완료 했습니다.");
			setTimeout("x_DAO_Retrieve()",100);

		});
	},
	null, 1, ls_pgmid, false);


	return;

// 	/*---------------------------------------------------------------------------------------------------------------------*/

//   is_openfile = fdw_4.ImportExcelFile();
//   //alert(is_openfile);
// 	if(is_openfile != '' && is_openfile != null) {       //히든dw에 파일 올리기
// 		var li_row = 1;
// 		//alert(fdw_4.GetItem(1, "compute_0003"));

// 		if(fdw_4.GetItem(1, "compute_0003") == "【 발  주  서 】") {


// 			//var ls_from = _X.StrPurify(fdw_4.GetItem(2, "compute_0001").substr(5, 10), "."); //동일 날짜 체크
// 			var ls_from = _X.StrPurify(_X.StrPurify(fdw_4.GetItem(2, "compute_0001")," "), ".").substr(3, 8); //동일 날짜 체크
// 			//alert(ls_from);
// 			var ls_end = _X.ToString(_X.RelativeDate(ls_from, 6), 'YYYYMMDD');
// 			//alert(ls_end)
// 			var ls_sql = " SELECT COUNT(*) FROM BM_ORDE_MASTER WHERE CUST_CODE = '" + is_cust + "' AND ('" + ls_from + "' BETWEEN ORDER_STARTDATE AND ORDER_ENDDATE OR '" + ls_end + "' BETWEEN ORDER_STARTDATE AND ORDER_ENDDATE ) and comp_code = '" + mytop._CompanyCode + "'";
// //			if( ls_from <= '20110228'){
// //				_X.MsgBox("확인", "발주서 등록이 마감처리되었습니다. ");
// //				return
// //			}
// 			/*
// 			if(ptdwo.SqlSelect("om", ls_sql) > 0) {
// 				_X.MsgBox('확인', '같은날짜로 등록된 발주서가 있습니다.');
// 				return;
// 			}
// 			*/
// 			if(ptdwo.SqlSelect("om", " SELECT TO_CHAR(TO_DATE(" + ls_from + "),'day') from dual ") != "월요일") {
// 				_X.MsgBox('확인', '발주서는 월요일 부터 일요일까지 등록 할 수 있습니다.');
// 				return;
// 			}

// 			//var ls_first_date = _X.StrPurify(fdw_4.GetItem(2, "compute_0001").substr(10, 5), ".");
// 			var ls_first_date = ls_from.substr(4, 4).toString;
// 			var ls_first_date_check = _X.StrPurify(fdw_4.GetItem(5,"compute_0007"),".").toString;
// 			//alert(ls_first_date);
// 			//alert(ls_first_date_check);
// 			if(ls_first_date != ls_first_date_check){
// 				_X.MsgBox("확인", "발주서 파일의 날짜가 일치하지 않습니다.");
// 				return;
// 			}

// 			/*==============요일, 시간체크, 이전꺼는 올릴수 없음========================*/
// 				var ls_return_chk = new Array();
// 				ls_return_chk = ptdwo.SqlSelectsArray("om", " select nvl(value1, ''), nvl(value2, '') from bm_comm_detail where hcode = '98' and dcode = '02' and use_yn = 'Y' and comp_code = '"+ mytop._CompanyCode + "'");
// 				if(ls_return_chk.length > 0) {
// 					var ls_chktime = ls_return_chk[0][0];
// 					var ls_chkdate = ls_return_chk[0][1];
// 					if(ls_chkdate != "" && ls_chkdate != null && ls_chktime != "" && ls_chktime != null) {
// 						var li_datediv = 0;
// 						switch(ls_chkdate) {
// 							case '월':
// 									li_datediv = 7;
// 									break;
// 							case '화':
// 									li_datediv = 6;
// 									break;
// 							case '수':
// 									li_datediv = 5;
// 									break;
// 							case '목':
// 									li_datediv = 4;
// 									break;
// 							case '금':
// 									li_datediv = 3;
// 									break;
// 							case '토':
// 									li_datediv = 2;
// 									break;
// 							case '일':
// 									li_datediv = 1;
// 									break;

// 						}
// 						var ld_now = _X.GetSysDate();
// 						var ls_check_date = _X.ToString(_X.RelativeDate(ld_now, li_datediv), 'YYYYMMDD');
// 						var ls_time = _X.ToString(ld_now, "hh");

// 						if(ls_from < ls_check_date) {
// 							_X.MsgBox("확인", "등록 기간을 초과하였습니다. 학교급식지원센터에 문의 해 주십시요.");
// 							return;
// 						}

// 						if(ls_from == ls_check_date && ls_time >= ls_chktime) {
// 							_X.MsgBox("확인", "등록 제한시간이 지났습니다. 학교급식지원센터에 문의 해 주십시요.");
// 							return;
// 						}
// 					}
// 				}
// 			/*============요일, 시간체크, 이전꺼는 올릴수 없음 ==============끝=========*/


// 			//조식,중식,석식 구분
// 			var ls_meal_div = '2';
// 			var ls_meal_name = fdw_4.GetItem(2, "compute_0001").substr(fdw_4.GetItem(2, "compute_0001").indexOf('(') + 1, (fdw_4.GetItem(2, "compute_0001").indexOf(')') - fdw_4.GetItem(2, "compute_0001").indexOf('(')) - 1);
// 			switch(ls_meal_name) {
// 				case '조식':
// 						ls_meal_div = '1';
// 						break;
// 				case '중식':
// 						ls_meal_div = '2';
// 						break;
// 				case '석식':
// 						ls_meal_div = '3';
// 						break;
// 				case '간식':
// 						ls_meal_div = '4';
// 						break;
// 				case '친환경':
// 						ls_meal_div = '5';
// 						break;
// 			}

// 			dg_1.Reset();

// 			var ls_today = ptdwo.SqlSelect("om", " SELECT SUBSTR(TO_CHAR( SYSDATE, 'YYYYMMDD') , 1, 8) from dual ");
// 			var ls_locdiv = ptdwo.SqlSelect("om", " SELECT nvl(loc_div, '00') from bm_code_cust where cust_code = '" + is_cust + "' and comp_code = '" + mytop._CompanyCode + "'");
// 			var ls_max = ptdwo.SqlSelect("bm", " select max(Substr(ORDER_NO, 11, 4)) from bm_orde_master where Substr(ORDER_NO, 1, 8) = '" + ls_today + "' and comp_code = '" + mytop._CompanyCode + "'");
// 					ls_max = '000000' + String(Number(ls_max) + 1);
// 		     ls_max = ls_max.substr(ls_max.length - 4, 4);
// 			is_order = ls_today +ls_locdiv+ ls_max;

// 			var li_order_seq = 0;
// 			if(uf_file_upload() == -1) {
// 				fdw_5.Reset();
// 				dg_3.Reset();
// 				x_DAO_Retrieve();
// 				return;
// 			}


// 			//dg_3 세팅
// 			var li_row3 = ptdwo.Inserts(dg_3.dw, 0);
// 			dg_3.SetItem(li_row3, "cust_code", is_cust);
// 			dg_3.SetItem(li_row3, "cust_name", mytop.gs_custname);
// 			dg_3.SetItem(li_row3, "order_startdate", _X.Todate(ls_from));
// 			dg_3.SetItem(li_row3, "order_enddate", _X.Todate(ls_end));
// 			dg_3.SetItem(li_row3, "ORDER_STATUS", '02');
// 			dg_3.SetItem(li_row3, "meal_div", ls_meal_div);
// 			dg_3.SetItem(li_row3, "meal_name", ls_meal_name);
// 			dg_3.SetItem(li_row3, "comp_code", mytop._CompanyCode);
// 			//dg_3 세팅 끝


// 			dg_1.dw.Modify("qty1_t.text    = '" + fdw_4.GetItem(5,"compute_0007") + "'");
// 			dg_1.dw.Modify("qty2_t.text    = '" + fdw_4.GetItem(5,"compute_0009") + "'");
// 			dg_1.dw.Modify("qty3_t.text    = '" + fdw_4.GetItem(5,"compute_0012") + "'");
// 			dg_1.dw.Modify("qty4_t.text    = '" + fdw_4.GetItem(5,"compute_0013") + "'");
// 			dg_1.dw.Modify("qty5_t.text    = '" + fdw_4.GetItem(5,"compute_0014") + "'");
// 			dg_1.dw.Modify("qty6_t.text    = '" + fdw_4.GetItem(5,"compute_0015") + "'");
// 			dg_1.dw.Modify("qty7_t.text    = '" + fdw_4.GetItem(5,"compute_0017") + "'");

// 			var ls_chin = 'N';
// 			for(var i=1;i<= fdw_4.RowCount();i++) {
// 			  if (fdw_4.GetItem(i, "compute_0002") != "" && fdw_4.GetItem(i, "compute_0002") != "식품명 / 상세식품명" && fdw_4.GetItem(i, "compute_0002") != "식품명/상세식품명") {
// 			  	li_order_seq ++;
// 					ptdwo.Inserts(dg_1.dw, li_row);
// 					dg_1.SetItem(li_row, "order_seq", li_order_seq);
// 					dg_1.SetItemString(li_row, "item_name", _X.StrReplace(_X.StrReplace(_X.Rtrim(_X.Ltrim(fdw_4.GetItem(i, "compute_0002").substr(0,50))), "\'", "＇"), '\"', '〃'));
// 					dg_1.SetItemString(li_row, "remarks", _X.StrReplace(_X.StrReplace(_X.Rtrim(_X.Ltrim(fdw_4.GetItem(i, "compute_0004").substr(0,200))), "\'", "＇"), '\"', '〃'));
// 					dg_1.SetItemString(li_row, "unit", _X.StrReplace(_X.StrReplace(_X.Rtrim(_X.Ltrim(fdw_4.GetItem(i, "compute_0005"))), "\'", "＇"), '\"', '〃'));
// 					//fdw_5 생성
// 					if(Number(fdw_4.GetItem(i, "compute_0007")) != 0 && Number(fdw_4.GetItem(i, "compute_0007")) != null) {
// 						if(isNaN(_X.Rtrim(_X.Ltrim(_X.StrPurify(fdw_4.GetItem(i, "compute_0007"), ',')))) == true) {
// 							_X.MsgBox("확인", "수량은 숫자만 등록 가능합니다. <br>자세한 사항은 관리자에게 문의 바랍니다.");
// 							uf_file_del();
// 							fdw_5.reset();
// 							x_DAO_Retrieve();
// 							return;
// 						}
// 						dg_1.SetItem(li_row, "qty1", Number(fdw_4.GetItem(i, "compute_0007")));
// 						var li_row5 = ptdwo.Inserts(fdw_5.dw, 0);
// 						li_order_seq ++;
// 						fdw_5.SetItem(li_row5, "cust_code", is_cust);
// 						fdw_5.SetItem(li_row5, "order_date", _X.Todate(ls_from));
// 						fdw_5.SetItem(li_row5, "use_date", _X.Todate(ls_from));
// 						fdw_5.SetItem(li_row5, "order_seq", li_order_seq);
// 						fdw_5.SetItem(li_row5, "item_name", _X.StrReplace(_X.StrReplace(_X.Rtrim(_X.Ltrim(fdw_4.GetItem(i, "compute_0002").substr(0,50))), "\'", "＇"), '\"', '〃'));
// 						fdw_5.SetItem(li_row5, "item_qty", _X.Round(_X.Rtrim(_X.Ltrim(_X.StrPurify(fdw_4.GetItem(i, "compute_0007"), ','))),2));
// 						fdw_5.SetItem(li_row5, "unit", _X.StrReplace(_X.StrReplace(_X.Rtrim(_X.Ltrim(fdw_4.GetItem(i, "compute_0005"))), "\'", "＇"), '\"', '〃'));
// 						fdw_5.SetItem(li_row5, "remarks", _X.StrReplace(_X.StrReplace(_X.Rtrim(_X.Ltrim(fdw_4.GetItem(i, "compute_0004").substr(0,200))), "\'", "＇"), '\"', '〃'));
// 						fdw_5.SetItem(li_row5, "meal_div", ls_meal_div);

// 						ls_chin = chk_mon.checked ? 'Y':'N';
// 						fdw_5.SetItem(li_row5, "chin_yn", ls_chin);
// 						fdw_5.SetItem(li_row5, "comp_code", mytop._CompanyCode);

// 					}
// 					//fdw_5 생성 끝

// 					if(Number(fdw_4.GetItem(i, "compute_0009")) != 0 && Number(fdw_4.GetItem(i, "compute_0009")) != null) {
// 						if(isNaN(_X.Rtrim(_X.Ltrim(_X.StrPurify(fdw_4.GetItem(i, "compute_0009"), ',')))) == true) {
// 							_X.MsgBox("확인", "수량은 숫자만 등록 가능합니다. <br>자세한 사항은 관리자에게 문의 바랍니다.");
// 							uf_file_del();
// 							fdw_5.reset();
// 							x_DAO_Retrieve();
// 							return;
// 						}
// 						dg_1.SetItem(li_row, "qty2", Number(fdw_4.GetItem(i, "compute_0009")));
// 						var li_row5 = ptdwo.Inserts(fdw_5.dw, 0);
// 						li_order_seq ++;
// 						fdw_5.SetItem(li_row5, "cust_code", is_cust);
// 						fdw_5.SetItem(li_row5, "order_date", _X.Todate(_X.ToString(_X.RelativeDate(ls_from, 1), 'YYYYMMDD')));
// 						fdw_5.SetItem(li_row5, "use_date", _X.Todate(_X.ToString(_X.RelativeDate(ls_from, 1), 'YYYYMMDD')));
// 						fdw_5.SetItem(li_row5, "order_seq", li_order_seq);
// 						fdw_5.SetItem(li_row5, "item_name", _X.StrReplace(_X.StrReplace(_X.Rtrim(_X.Ltrim(fdw_4.GetItem(i, "compute_0002").substr(0,50))), "\'", "＇"), '\"', '〃'));
// 						fdw_5.SetItem(li_row5, "item_qty", _X.Round(_X.Rtrim(_X.Ltrim(_X.StrPurify(fdw_4.GetItem(i, "compute_0009"), ','))),2));
// 						fdw_5.SetItem(li_row5, "unit", _X.StrReplace(_X.StrReplace(_X.Rtrim(_X.Ltrim(fdw_4.GetItem(i, "compute_0005"))), "\'", "＇"), '\"', '〃'));
// 						fdw_5.SetItem(li_row5, "remarks", _X.StrReplace(_X.StrReplace(_X.Rtrim(_X.Ltrim(fdw_4.GetItem(i, "compute_0004").substr(0,200))), "\'", "＇"), '\"', '〃'));
// 						fdw_5.SetItem(li_row5, "meal_div", ls_meal_div);

// 						ls_chin = chk_tue.checked ? 'Y':'N';
// 						fdw_5.SetItem(li_row5, "chin_yn", ls_chin);
// 						fdw_5.SetItem(li_row5, "comp_code", mytop._CompanyCode);
// 					}

// 					if(Number(fdw_4.GetItem(i, "compute_0012")) != 0 && Number(fdw_4.GetItem(i, "compute_0012")) != null) {
// 						if(isNaN(_X.Rtrim(_X.Ltrim(_X.StrPurify(fdw_4.GetItem(i, "compute_00012"), ',')))) == true) {
// 							_X.MsgBox("확인", "수량은 숫자만 등록 가능합니다. <br>자세한 사항은 관리자에게 문의 바랍니다.");
// 							uf_file_del();
// 							fdw_5.reset();
// 							x_DAO_Retrieve();
// 							return;
// 						}
// 						dg_1.SetItem(li_row, "qty3", Number(fdw_4.GetItem(i, "compute_0012")));
// 						var li_row5 = ptdwo.Inserts(fdw_5.dw, 0);
// 						li_order_seq ++;
// 						fdw_5.SetItem(li_row5, "cust_code", is_cust);
// 						fdw_5.SetItem(li_row5, "order_date", _X.Todate(_X.ToString(_X.RelativeDate(ls_from, 2), 'YYYYMMDD')));
// 						fdw_5.SetItem(li_row5, "use_date", _X.Todate(_X.ToString(_X.RelativeDate(ls_from, 2), 'YYYYMMDD')));
// 						fdw_5.SetItem(li_row5, "order_seq", li_order_seq);
// 						fdw_5.SetItem(li_row5, "item_name", _X.StrReplace(_X.StrReplace(_X.Rtrim(_X.Ltrim(fdw_4.GetItem(i, "compute_0002").substr(0,50))), "\'", "＇"), '\"', '〃'));
// 						fdw_5.SetItem(li_row5, "item_qty",  _X.Round(_X.Rtrim(_X.Ltrim(_X.StrPurify(fdw_4.GetItem(i, "compute_0012"), ','))),2));
// 						fdw_5.SetItem(li_row5, "unit", _X.StrReplace(_X.StrReplace(_X.Rtrim(_X.Ltrim(fdw_4.GetItem(i, "compute_0005"))), "\'", "＇"), '\"', '〃'));
// 						fdw_5.SetItem(li_row5, "remarks", _X.StrReplace(_X.StrReplace(_X.Rtrim(_X.Ltrim(fdw_4.GetItem(i, "compute_0004").substr(0,200))), "\'", "＇"), '\"', '〃'));
// 						fdw_5.SetItem(li_row5, "meal_div", ls_meal_div);
// 						fdw_5.SetItem(li_row5, "comp_code", mytop._CompanyCode);

// 						ls_chin = chk_wed.checked ? 'Y':'N';
// 						fdw_5.SetItem(li_row5, "chin_yn", ls_chin);
// 						fdw_5.SetItem(li_row5, "comp_code", mytop._CompanyCode);
// 					}

// 					if(Number(fdw_4.GetItem(i, "compute_0013")) != 0 && Number(fdw_4.GetItem(i, "compute_0013")) != null) {
// 						if(isNaN(_X.Rtrim(_X.Ltrim(_X.StrPurify(fdw_4.GetItem(i, "compute_0013"), ',')))) == true) {
// 							_X.MsgBox("확인", "수량은 숫자만 등록 가능합니다. <br>자세한 사항은 관리자에게 문의 바랍니다.");
// 							uf_file_del();
// 							fdw_5.reset();
// 							x_DAO_Retrieve();
// 							return;
// 						}
// 						dg_1.SetItem(li_row, "qty4", Number(fdw_4.GetItem(i, "compute_0013")));
// 						var li_row5 = ptdwo.Inserts(fdw_5.dw, 0);
// 						li_order_seq ++;
// 						fdw_5.SetItem(li_row5, "cust_code", is_cust);
// 						fdw_5.SetItem(li_row5, "order_date", _X.Todate(_X.ToString(_X.RelativeDate(ls_from, 3), 'YYYYMMDD')));
// 						fdw_5.SetItem(li_row5, "use_date", _X.Todate(_X.ToString(_X.RelativeDate(ls_from, 3), 'YYYYMMDD')));
// 						fdw_5.SetItem(li_row5, "order_seq", li_order_seq);
// 						fdw_5.SetItem(li_row5, "item_name", _X.StrReplace(_X.StrReplace(_X.Rtrim(_X.Ltrim(fdw_4.GetItem(i, "compute_0002").substr(0,50))), "\'", "＇"), '\"', '〃'));
// 						fdw_5.SetItem(li_row5, "item_qty", _X.Round(_X.Rtrim(_X.Ltrim(_X.StrPurify(fdw_4.GetItem(i, "compute_0013"), ','))),2));
// 						fdw_5.SetItem(li_row5, "unit", _X.StrReplace(_X.StrReplace(_X.Rtrim(_X.Ltrim(fdw_4.GetItem(i, "compute_0005"))), "\'", "＇"), '\"', '〃'));
// 						fdw_5.SetItem(li_row5, "remarks", _X.StrReplace(_X.StrReplace(_X.Rtrim(_X.Ltrim(fdw_4.GetItem(i, "compute_0004").substr(0,200))), "\'", "＇"), '\"', '〃'));
// 						fdw_5.SetItem(li_row5, "meal_div", ls_meal_div);
// 						fdw_5.SetItem(li_row5, "comp_code", mytop._CompanyCode);

// 						ls_chin = chk_thu.checked ? 'Y':'N';
// 						fdw_5.SetItem(li_row5, "chin_yn", ls_chin);
// 						fdw_5.SetItem(li_row5, "comp_code", mytop._CompanyCode);
// 					}


// 					if(Number(fdw_4.GetItem(i, "compute_0014")) != 0 && Number(fdw_4.GetItem(i, "compute_0014")) != null) {
// 						if(isNaN(_X.Rtrim(_X.Ltrim(_X.StrPurify(fdw_4.GetItem(i, "compute_0014"), ',')))) == true) {
// 							_X.MsgBox("확인", "수량은 숫자만 등록 가능합니다. <br>자세한 사항은 관리자에게 문의 바랍니다.");
// 							uf_file_del();
// 							fdw_5.reset();
// 							x_DAO_Retrieve();
// 							return;
// 						}
// 						dg_1.SetItem(li_row, "qty5", Number(fdw_4.GetItem(i, "compute_0014")));
// 						var li_row5 = ptdwo.Inserts(fdw_5.dw, 0);
// 						li_order_seq ++;
// 						fdw_5.SetItem(li_row5, "cust_code", is_cust);
// 						fdw_5.SetItem(li_row5, "order_date", _X.Todate(_X.ToString(_X.RelativeDate(ls_from, 4), 'YYYYMMDD')));
// 						fdw_5.SetItem(li_row5, "use_date", _X.Todate(_X.ToString(_X.RelativeDate(ls_from, 4), 'YYYYMMDD')));
// 						fdw_5.SetItem(li_row5, "order_seq", li_order_seq);
// 						fdw_5.SetItem(li_row5, "item_name", _X.StrReplace(_X.StrReplace(_X.Rtrim(_X.Ltrim(fdw_4.GetItem(i, "compute_0002").substr(0,50))), "\'", "＇"), '\"', '〃'));
// 						fdw_5.SetItem(li_row5, "item_qty", _X.Round(_X.Rtrim(_X.Ltrim(_X.StrPurify(fdw_4.GetItem(i, "compute_0014"), ','))),2));
// 						//alert(fdw_4.GetItem(i, "compute_0014"));
// 						fdw_5.SetItem(li_row5, "unit", _X.StrReplace(_X.StrReplace(_X.Rtrim(_X.Ltrim(fdw_4.GetItem(i, "compute_0005"))), "\'", "＇"), '\"', '〃'));
// 						fdw_5.SetItem(li_row5, "remarks", _X.StrReplace(_X.StrReplace(_X.Rtrim(_X.Ltrim(fdw_4.GetItem(i, "compute_0004").substr(0,200))), "\'", "＇"), '\"', '〃'));
// 						fdw_5.SetItem(li_row5, "meal_div", ls_meal_div);
// 						fdw_5.SetItem(li_row5, "comp_code", mytop._CompanyCode);

// 						ls_chin = chk_fri.checked ? 'Y':'N';
// 						fdw_5.SetItem(li_row5, "chin_yn", ls_chin);
// 						fdw_5.SetItem(li_row5, "comp_code", mytop._CompanyCode);
// 					}

// 					if(Number(fdw_4.GetItem(i, "compute_0015")) != 0 && Number(fdw_4.GetItem(i, "compute_0015")) != null) {
// 						if(isNaN(_X.Rtrim(_X.Ltrim(_X.StrPurify(fdw_4.GetItem(i, "compute_0015"), ',')))) == true) {
// 							_X.MsgBox("확인", "수량은 숫자만 등록 가능합니다. <br>자세한 사항은 관리자에게 문의 바랍니다.");
// 							uf_file_del();
// 							fdw_5.reset();
// 							x_DAO_Retrieve();
// 							return;
// 						}
// 						dg_1.SetItem(li_row, "qty6", Number(fdw_4.GetItem(i, "compute_0015")));
// 						var li_row5 = ptdwo.Inserts(fdw_5.dw, 0);
// 						li_order_seq ++;
// 						fdw_5.SetItem(li_row5, "cust_code", is_cust);
// 						fdw_5.SetItem(li_row5, "order_date", _X.Todate(_X.ToString(_X.RelativeDate(ls_from, 5), 'YYYYMMDD')));
// 						fdw_5.SetItem(li_row5, "use_date", _X.Todate(_X.ToString(_X.RelativeDate(ls_from, 5), 'YYYYMMDD')));
// 						fdw_5.SetItem(li_row5, "order_seq", li_order_seq);
// 						fdw_5.SetItem(li_row5, "item_name", _X.StrReplace(_X.StrReplace(_X.Rtrim(_X.Ltrim(fdw_4.GetItem(i, "compute_0002").substr(0,50))), "\'", "＇"), '\"', '〃'));
// 						fdw_5.SetItem(li_row5, "item_qty", _X.Round(_X.Rtrim(_X.Ltrim(_X.StrPurify(fdw_4.GetItem(i, "compute_0015"), ','))),2));
// 						fdw_5.SetItem(li_row5, "unit", _X.StrReplace(_X.StrReplace(_X.Rtrim(_X.Ltrim(fdw_4.GetItem(i, "compute_0005"))), "\'", "＇"), '\"', '〃'));
// 						fdw_5.SetItem(li_row5, "remarks", _X.StrReplace(_X.StrReplace(_X.Rtrim(_X.Ltrim(fdw_4.GetItem(i, "compute_0004").substr(0,200))), "\'", "＇"), '\"', '〃'));
// 						fdw_5.SetItem(li_row5, "meal_div", ls_meal_div);
// 						fdw_5.SetItem(li_row5, "comp_code", mytop._CompanyCode);

// 						ls_chin = chk_sat.checked ? 'Y':'N';
// 						fdw_5.SetItem(li_row5, "chin_yn", ls_chin);
// 						fdw_5.SetItem(li_row5, "comp_code", mytop._CompanyCode);
// 					}
// 					if(Number(fdw_4.GetItem(i, "compute_0017")) != 0 && Number(fdw_4.GetItem(i, "compute_0017")) != null) {
// 						if(isNaN(_X.Rtrim(_X.Ltrim(_X.StrPurify(fdw_4.GetItem(i, "compute_0017"), ',')))) == true) {
// 							_X.MsgBox("확인", "수량은 숫자만 등록 가능합니다. <br>자세한 사항은 관리자에게 문의 바랍니다.");
// 							uf_file_del();
// 							fdw_5.reset();
// 							x_DAO_Retrieve();
// 							return;
// 						}
// 						dg_1.SetItem(li_row, "qty7", Number(fdw_4.GetItem(i, "compute_0017")));
// 						var li_row5 = ptdwo.Inserts(fdw_5.dw, 0);
// 						li_order_seq ++;
// 						fdw_5.SetItem(li_row5, "cust_code", is_cust);
// 						fdw_5.SetItem(li_row5, "order_date", _X.Todate(_X.ToString(_X.RelativeDate(ls_from, 6), 'YYYYMMDD')));
// 						fdw_5.SetItem(li_row5, "use_date", _X.Todate(_X.ToString(_X.RelativeDate(ls_from, 6), 'YYYYMMDD')));
// 						fdw_5.SetItem(li_row5, "order_seq", li_order_seq);
// 						fdw_5.SetItem(li_row5, "item_name", _X.StrReplace(_X.StrReplace(_X.Rtrim(_X.Ltrim(fdw_4.GetItem(i, "compute_0002").substr(0,50))), "\'", "＇"), '\"', '〃'));
// 						fdw_5.SetItem(li_row5, "item_qty", _X.Round(_X.Rtrim(_X.Ltrim(_X.StrPurify(fdw_4.GetItem(i, "compute_0017"), ','))),2));
// 						fdw_5.SetItem(li_row5, "unit", _X.StrReplace(_X.StrReplace(_X.Rtrim(_X.Ltrim(fdw_4.GetItem(i, "compute_0005"))), "\'", "＇"), '\"', '〃'));
// 						fdw_5.SetItem(li_row5, "remarks", _X.StrReplace(_X.StrReplace(_X.Rtrim(_X.Ltrim(fdw_4.GetItem(i, "compute_0004").substr(0,200))), "\'", "＇"), '\"', '〃'));
// 						fdw_5.SetItem(li_row5, "meal_div", ls_meal_div);
// 						fdw_5.SetItem(li_row5, "comp_code", mytop._CompanyCode);

// 						ls_chin = chk_sun.checked ? 'Y':'N';
// 						fdw_5.SetItem(li_row5, "chin_yn", ls_chin);
// 						fdw_5.SetItem(li_row5, "comp_code", mytop._CompanyCode);
// 					}

// 					//dg_1.SetItem(li_row, "qty_total", Number(fdw_4.GetItem(i, "compute_0018")));
// 					li_row ++ ;
// 				}
// 			}
// 		} else {
// 			_X.MsgBox("확인", "잘못된 양식을 올리셧습니다. <br> 다시 확인하시고 올려주십시요.");
// 			return;
// 		}
// 		var li_newrow3 = ptdwo.GetNewRows(dg_3.dw, DW_BUFFER_primary);
// 		if(li_newrow3.length > 0) {
// 			dg_3.SetItem(li_newrow3[0], "ORDER_NO", is_order);
// 			for(i=1; i<=fdw_5.RowCount(); i++) {
// 				fdw_5.SetItem(i, "ORDER_NO", is_order);
// 			}
// 		}

// 		dg_3.Save(null, null, 'N');
// 		uf_Set_Color();
// 		ptdwo.RunSql("om", " UPDATE BM_ORDE_MASTER SET ROW_INPUT_DATE = SYSDATE, ROW_INPUT_EMP_NO = '" + mytop.gs_user_id + "', ROW_INPUT_IP = '"+mytop.gs_clientip+"' WHERE ORDER_NO = '"+is_order+"' and comp_code = '" + mytop._CompanyCode + "'");
// 		ptdwo.SaveDW(fdw_5.dw);
// 		ptdwo.RunSql("om", " UPDATE BM_ORDE_DETAIL SET ROW_INPUT_DATE = SYSDATE, ROW_INPUT_EMP_NO = '" + mytop.gs_user_id + "', ROW_INPUT_IP = '"+mytop.gs_clientip+"' WHERE ORDER_NO = '"+is_order+"' and comp_code = '" + mytop._CompanyCode + "'");


// /*친환경 급식 요일 선택 저장*/
// 		var ls_mon = chk_mon.checked ? 'Y':'N';
// 		var ls_tue = chk_tue.checked ? 'Y':'N';
// 		var ls_wed = chk_wed.checked ? 'Y':'N';
// 		var ls_thu = chk_thu.checked ? 'Y':'N';
// 		var ls_fri = chk_fri.checked ? 'Y':'N';
// 		var ls_sat = chk_sat.checked ? 'Y':'N';
// 		var ls_sun = chk_sun.checked ? 'Y':'N';

// 		var ls_rtn = ptdwo.SqlSelect("om", "select 1 from bm_code_chinday where cust_code = '" + is_cust +"' and comp_code = '" + mytop._CompanyCode + "'");
// 		if(ls_rtn > 0) {
// 			ptdwo.RunSql("om", " update bm_code_chinday set DAY1 = '" + ls_mon + "', DAY2 = '" + ls_tue + "', DAY3 = '" + ls_wed + "', DAY4 = '" + ls_thu + "', DAY5 = '" + ls_fri + "', DAY6 = '" + ls_sat + "', DAY7 = '" + ls_sun + "' WHERE CUST_CODE = '" + is_cust +"' and comp_code = '" + mytop._CompanyCode + "'");
// 		} else {
// 			ptdwo.RunSql("om", " insert into bm_code_chinday (CUST_CODE, DAY1, DAY2, DAY3, DAY4, DAY5, DAY6, DAY7, comp_code) VALUES ('" + is_cust + "', '" + ls_mon + "', '" + ls_tue + "', '" + ls_wed + "', '" + ls_thu
// 		  	                   + "', '" + ls_fri + "', '" + ls_sat + "', '" + ls_sun + "','" + mytop._CompanyCode  + "')" );
// 		}
// /*친환경 급식 요일 선택 저장 끝*/



// 		//_X.MessageBox("확인", "주문서 등록을 완료 했습니다.");
// //		td_confirm.style.visibility = "visible";

// 		var ls_school = mytop.gs_custname;//dg_3.GetItem(dg_3.GetRow(), "cust_name");
// 		var ls_ord = dg_3.GetItem(dg_3.GetRow(), 'ORDER_NO');
// 		//excel_form.href = "/FTP/upload/school/" + ls_school + "_" + ls_ord + ".xls";

// 		//수정(2016.05.29 KYY)
// 		//ptdwo.ExecProc("om", "주문서 등록을 완료 했습니다.", "SP_OM_ITEMAGREEMENT_SET", true, false, mytop._CompanyCode,ls_ord);
// 		ptdwo.ExecProc("om", "주문서 등록을 완료 했습니다.", "SP_BM_SETFROMESTIMATE", true, false, mytop._CompanyCode, ls_ord, sle_basicmonth.value);

// 		uf_etc_retrieve();
// 	}
//	return 100;
}

//엑셀 업로드
function uf_file_upload(){
	var local_file = is_openfile;

	if(local_file != null && local_file != ""){
		var ls_group_seq = 0;

		var file_name = local_file.substring(local_file.lastIndexOf("\\")+1, _X.ByteLen(local_file));
		var file_ext = file_name.substring(file_name.lastIndexOf(".")+1, _X.ByteLen(file_name));
		var ls_file_name = is_cust+'_'+is_order+'.'+file_ext;	//파일명
		var ls_file_path = is_base_dir+'school/'+is_order.substring(0, 4)+'/';
		//alert(ls_fil_path);
		var ls_sql = "INSERT INTO BM_ORDER_FILE( LOGINID, FILE_ID, FILE_NM, FILE_LOC, FILE_TYPE, REF_KEY, RMK, CUST_CODE, ROW_INPUT_DATE)"
					     + " VALUES ('" + mytop.gs_user_id + "', '" + is_order + "', '" + ls_file_name + "', '" + ls_file_path+ls_file_name + "', '" + file_ext + "', '', '', '" + is_cust+ "', SYSDATE ) ";
		var upload_result = comm.HttpUpload2(ls_file_path, local_file, ls_file_name);
		//var upload_result = mytop.PetaAPI.FtpUpload(local_file, is_base_dir+'school/'+mytop.gs_custname+'_'+is_order+'.'+file_ext);
		if(upload_result < 0){
			_X.MsgBox("확인", "파일이 등록되지 않았습니다.");
			return -1;
		}
           ptdwo.RunSql('sm', ls_sql);
	}
}

//숫자 오류로 인한 업로드 실패시 파일 삭제
function uf_file_del() {
	var ls_custcode = is_cust;
	var ls_order = is_order;
	var ls_sql = "select file_loc from bm_order_file where cust_code = '"+ls_custcode+"' and file_id = '"+ls_order+"'";
	var ls_result = ptdwo.SqlSelect('om', ls_sql);
	var result = ptdwo.DelServerFile(ls_result);

	var ls_return0 = ptdwo.RunSql("bm", " DELETE FROM bm_order_file WHERE cust_code = '"+ls_custcode+"' and file_id = '"+ls_order+"'");
}

//발주취소
function uf_order_del() {
	var ls_order = dg_3.GetItem(dg_3.GetRow(), "ORDER_NO");
	if(ls_order == "" || ls_order == null) {
		return;
	}

	var ls_cnsltno = dg_3.GetItem(dg_3.GetRow(), "CNSLT_NO");
	if(ls_order != "" ) {	// << 이상함
		_X.MsgBox("확인", "AT연계 발주데이터는 삭제하실수 없습니다. <br> 학교급식지원센터에 연락해 주십시오.");
		return;
	}

	var ls_sql = "SELECT COUNT(*) FROM BM_ORDE_ORDERLIST WHERE ORDER_NO ='" + ls_order +"' and comp_code = '" + mytop._CompanyCode + "'";
	var ll_cnt = ptdwo.SqlSelect('om', ls_sql);
	if(ll_cnt > 1){
		_X.MsgBox("확인", "발주처리가 완료되어 삭제하실수 없습니다. <br> 학교급식지원센터에 연락해 주십시오.");
		return ;
	}

	//파일삭제
	if(_X.MsgBox("파일삭제","발주 삭제 하시겠습까?</br>(삭제한 파일은 복구할 수 없습니다.)",icon_Question,btn_YesNo,2)==1){
		var ls_fileid = dg_3.GetItem(dg_3.GetRow(), "ATCH_FILE_ID");
		if(ls_fileid != "")	_X.FileDelete(atchFileId, "1");
	} else {
		return;
	}

	// 발주삭제처리
	var li_ret = _X.EP('pt','SP_BM_ORDE_DELETE', [mytop._CompanyCode, is_cust, ls_order]);
	if(li_ret < 0) {
		_X.MsgBox("확인", "발주취소에 실패 하였습니다.\n파일이 삭제 되었지만 기록이 남아있습니다.");
	} else {
		_X.MsgBox("확인", "발주를 취소 하였습니다.");
	}

	x_DAO_Retrieve();
}

//상단에 날짜 넣어주
function uf_set_date_txt() {
	var ls_date = _X.ToString(dg_3.GetItem(dg_3.GetRow(), "ORDER_STARTDATE"), 'YYYYMMDD');
	if(ls_date == "" || ls_date == null) {
		dg_1.UpdateColumnHeader("QTY1", '월');
		dg_1.UpdateColumnHeader("QTY2", '화');
		dg_1.UpdateColumnHeader("QTY3", '수');
		dg_1.UpdateColumnHeader("QTY4", '목');
		dg_1.UpdateColumnHeader("QTY5", '금');
		dg_1.UpdateColumnHeader("QTY6", '토');
		dg_1.UpdateColumnHeader("QTY7", '일');
	} else {
		dg_1.UpdateColumnHeader("QTY1", _X.ToString(ls_date, 'MM.DD'));
		dg_1.UpdateColumnHeader("QTY2", _X.ToString(_X.RelativeDate(ls_date, 1), 'MM.DD'));
		dg_1.UpdateColumnHeader("QTY3", _X.ToString(_X.RelativeDate(ls_date, 2), 'MM.DD'));
		dg_1.UpdateColumnHeader("QTY4", _X.ToString(_X.RelativeDate(ls_date, 3), 'MM.DD'));
		dg_1.UpdateColumnHeader("QTY5", _X.ToString(_X.RelativeDate(ls_date, 4), 'MM.DD'));
		dg_1.UpdateColumnHeader("QTY6", _X.ToString(_X.RelativeDate(ls_date, 5), 'MM.DD'));
		dg_1.UpdateColumnHeader("QTY7", _X.ToString(_X.RelativeDate(ls_date, 6), 'MM.DD'));
	}
}

function uf_fileopen() {
	var ls_ord = dg_3.GetItem(dg_3.GetRow(), 'ORDER_NO');

	var ls_result = _X.XmlSelect("pt", "PT02030", "GET_FILE_LOC" , [is_cust, ls_ord], "array");
	if(ls_result.length == 0){
		_X.MsgBox('확인', '파일이 존재하지 않습니다.');
		return;
	}

	var ls_file_loc = ls_result[0][0];
	_X.FileDownload2(ls_file_loc);

	//var file_name = ls_file_loc.substring(ls_file_loc.lastIndexOf('/')+1, ls_file_loc.length);
	//var result = comm.HttpDownload(ls_file_loc, file_name, false);
}

// 발주금액비교완료
function uf_confirm_amt() {
	var ls_ORDER_NO = dg_3.GetItem(dg_3.GetRow(), "ORDER_NO");
	if(ls_ORDER_NO != "" && ls_ORDER_NO != null) {
		dg_3.SetItem(dg_3.GetRow(), "ORDER_STATUS", "03");
		dg_3.Save(null, null, 'N');
		uf_Set_Color();
		_X.MsgBox("확인", "금액확인이 완료 되었습니다.");
	}
}

// 발주예정보기(레포트)
function uf_order_estimate() {
	mytop.rpt_caller = window;
	ptdwo.SetReport('om','dr_om01030_1',false);
	var ls_orderno = dg_3.GetItem(dg_3.GetRow(), "ORDER_NO");
	ptdwo.RptRetrieve(is_cust, ls_orderno, mytop._CompanyCode);
}

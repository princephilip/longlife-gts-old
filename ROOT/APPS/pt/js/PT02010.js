//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : 시장조사자료관리(학교)
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
var _CustCode = mytop._CustCode;
var _APPLY_RATE  = 0;

function x_InitForm2(){
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "pt", "PT02010", "PT02010|PT02010_C01", false, true);
	_X.InitGrid(grid_2, "dg_2", "100%", "100%", "pt", "PT02010", "PT02010|PT02010_R02", false, false);
	_X.InitGrid(grid_down, "dg_down", "100%", "100%", "pt", "PT02010", "PT02010|EXCEL_R03", false, false);

	if(_X.HaveActAuth("시스템관리자")) _CustCode = '1000001';
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		setTimeout("x_DAO_Retrieve()",100);
	}
}

function x_DAO_Retrieve2(a_dg){
	var ls_yymm = _X.StrPurify(sle_basicmonth.value);

	dg_2.Reset();
	dg_1.Retrieve([mytop._CompanyCode, _CustCode, ls_yymm]);

	var ls_fixyn = _X.XmlSelect("pt", "PT02010", "GET_FIX_YN" , [mytop._CompanyCode, _CustCode, ls_yymm], "array")[0][0];
	var dc_rate  = _X.XmlSelect("pt", "PT02010", "GET_DC_RATE", [mytop._CompanyCode, _CustCode, ls_yymm], "array")[0][0];

	span_dc.innerHTML = (dc_rate=="0" ? "" : " " + dc_rate + "% 할인");
	_APPLY_RATE = (1.0 - Number(dc_rate) /100.0);

	if(ls_fixyn=="Y"){
		span_text.innerHTML= "확정";
		dg_1.GridReadOnly(true);
	}else{
		span_text.innerHTML= "미확정";
		dg_1.GridReadOnly(false);
	}
}

function xe_EditChanged2(a_obj, a_val){
	x_DAO_Retrieve();
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	if(a_obj.id == 'S_FIND'){
		if(S_FIND.value != ""){
			var li_fRow =  dg_1.SetFocusByElement('S_FIND',['NEIS_NAME','ITEM_DNAME'], false);
			if(li_fRow > 0) {
				setTimeout("dg_1.SetRow(" + li_fRow.toString() + ")",100);
			} else {
				_X.MsgBox("확인","검색한 품목이 존재하지 않습니다.");
			}
		}
	}
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	var cData1 	= dg_1.GetChangedData();

	if(cData1.length > 0){
		var li_seq = dg_1.GetTotalValue("SEQ", "MAX");

		for (i=0 ; i < cData1.length; i++){
			if(cData1[i].job == 'D') continue;

			if(dg_1.GetItem(cData1[i].idx, "NEIS_CODE") == "" || dg_1.GetItem(cData1[i].idx, "NEIS_CODE") == null) {
				dg_1.SetRow(cData1[i].idx);
				_X.MsgBox("확인", cData1[i].idx + " 번째 행의 식품데이터가 입력되지 않았습니다.");
				return false;
			}

			if(cData1[i].job == 'I'){
				li_seq++;
				dg_1.SetItem(cData1[i].idx, "SEQ", li_seq);
			}
		}
	}

	return true;
}

//저장후 호출
function x_DAO_Saved2(){
}

function x_DAO_Insert2(a_dg, row){
	if(a_dg.id == 'dg_1') {
	}

  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
	if(a_dg.id == 'dg_1') {
		dg_1.SetItem(rowIdx, "COMP_CODE"     , mytop._CompanyCode);
		dg_1.SetItem(rowIdx, "CUST_CODE"     , _CustCode);
		dg_1.SetItem(rowIdx, "YYYYMM"        , _X.StrPurify(sle_basicmonth.value));
		dg_1.SetItem(rowIdx, "CONVERT_STATUS", "N");
	}

}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	// if(a_dg.id == "dg_1") {
	// 	if(dg_1.GetItem(row, "END_YN") == "Y") {
	// 		_X.MsgBox("확인", "확인완료 된 내용은 삭제 불가능 합니다.");
	// 		return 0;
	// 	}
 //  }

	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	if(a_dg.id == 'dg_1') {
		switch(a_col) {
			case	"CONVERT_STD":
						dg_1.SetItem(a_row,"CONVERT_COST", Math.floor(Number(a_newvalue)*dg_1.GetItem(a_row,"MARKET_COST") * _APPLY_RATE));
						break;
			case	"MARKET_COST":
						dg_1.SetItem(a_row,"CONVERT_COST", Math.floor(Number(a_newvalue)*dg_1.GetItem(a_row,"CONVERT_STD") * _APPLY_RATE));
						break;
		}
	}
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
	if(a_dg.id == 'dg_1') {
		var ls_cust  = dg_1.GetItem(a_newrow, 'CUST_CODE');
		var ls_neis  = dg_1.GetItem(a_newrow, 'NEIS_CODE');
		dg_2.Retrieve([mytop._CompanyCode, ls_cust, ls_neis]);
	}
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
	if(a_dg.id == 'dg_1') {
		switch(a_colname) {
			case	"NEIS_NAME": 	// 식품명
						_X.CommonFindCode(window,"품목분류검색","com",'NEIS_CODE','',[],"FindCodeNeis|FindCode|IF_CODE_NEIS_R01",500, 700);
						break;
			case	"ITEM_NAME": 	// 센터품목명
						var ls_date = _X.StrPurify(sle_basicmonth.value)+"01";
						var ls_neis = dg_1.GetItem(a_row, "NEIS_NAME")+"/"+dg_1.GetItem(a_row, "NEIS_DNAME");
						_X.CommonFindCode(window,"품목검색","com",'ITEM_CODE',ls_neis,['%', ls_date, '%', mytop._CompanyCode],"FindCodeItem|FindCode|BM_CODE_ITEM_R01",1200, 600);
						break;
		}
	}
}

function x_ReceivedCode2(a_retVal){
	if(_FindCode.returnValue == null) return;

	switch(_FindCode.finddiv) {
		case	"NEIS_CODE" :
					var ls_code = _FindCode.returnValue.NEIS_CODE;
					var ls_name = _FindCode.returnValue.NEIS_NAME;
					dg_1.SetItem(dg_1.GetRow(), "NEIS_CODE", ls_code);
					dg_1.SetItem(dg_1.GetRow(), "NEIS_NAME", ls_name.split('/')[0]);
					dg_1.SetItem(dg_1.GetRow(), "NEIS_DNAME", ls_name.split('/')[1]);
					dg_1.SetItem(dg_1.GetRow(), "NEIS_MATCH_YN", "Y");
					break;
		case	"ITEM_CODE" :
					dg_1.SetItem(dg_1.GetRow(), "ITEM_NAME"  , _FindCode.returnValue.ITEM_NAME);
					dg_1.SetItem(dg_1.GetRow(), "ITEM_DNAME" , _FindCode.returnValue.ITEM_DNAME);
					dg_1.SetItem(dg_1.GetRow(), "UNIT"       , _FindCode.returnValue.UNIT);
					dg_1.SetItem(dg_1.GetRow(), "ITEM_STD"   , _FindCode.returnValue.ITEM_STD);
					dg_1.SetItem(dg_1.GetRow(), "GREEN_YN"   , (_FindCode.returnValue.ITEM_DIV=="01" ? "Y" : "N"));
					dg_1.SetItem(dg_1.GetRow(), "ITEM_CODE"  , _FindCode.returnValue.ITEM_CODE);
					dg_1.SetItem(dg_1.GetRow(), "BRAND_NAME" , _FindCode.returnValue.BRAND_NAME);
					dg_1.SetItem(dg_1.GetRow(), "TAX_YN"     , _FindCode.returnValue.TAX_YN);
					dg_1.SetItem(dg_1.GetRow(), "MARKET_COST", Number(_FindCode.returnValue.MARKET_COST));
					dg_1.SetItem(dg_1.GetRow(), "COST"       , Number(_FindCode.returnValue.COST));
					dg_1.SetItem(dg_1.GetRow(), "QTY_CHANGE" , Number(_FindCode.returnValue.QTY_CHANGE));
					dg_1.SetItem(dg_1.GetRow(), "ITEM_MATCH_YN", "Y");
					dg_1.SetItem(dg_1.GetRow(), "CONVERT_STD", 1);
					var applyRate = (_FindCode.returnValue.DC_APPLY_YN=="Y" ? _APPLY_RATE : 1);
					dg_1.SetItem(dg_1.GetRow(), "CONVERT_COST", Math.floor(dg_1.GetItem(dg_1.GetRow(), "MARKET_COST") * applyRate));
					break;
	}
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){}
function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}


function uf_datadown(){
	var ls_yymm  = _X.StrPurify(sle_basicmonth.value);
	var ls_fixyn = _X.XmlSelect("pt", "PT02010", "GET_FIX_YN2" , [mytop._CompanyCode, _CustCode, ls_yymm], "array")[0][0];
	if(ls_fixyn != "Y"){
		_X.MsgBox("확인","확정된 자료만 다운로드 받으실 수 있습니다!");
		return;
	}

	if(dg_1.IsDataChanged()) {
		if(_X.MsgBoxYesNo("확인", "변경된 데이터를 저장하시겠습니까?") == 1) {
			dg_1.Save(null, null, 'N');
		}
	}

	dg_down.Retrieve([mytop._CompanyCode, _CustCode, ls_yymm]);
	dg_down.ExcelExport(true, "시장소자사료관리(학교)_" + _X.ToString(_X.GetSysDate(), "YYYYMMDD_HHMISS"));
}


function uf_dataupload(){
	var ls_yymm = _X.StrPurify(sle_basicmonth.value);
	//var ls_sgrpcode = '10';

	var li_cnt = Number(_X.XmlSelect("pt", "PT02010", "CHECK_CNT" , [mytop._CompanyCode, _CustCode, ls_yymm], "array")[0][0]);
	if(li_cnt > 0){
		if(_X.MsgBoxYesNo("확인", "기존 자료가 있습니다!<br>삭제하고 다시 올리시겠습니까?") == 2) {
			x_DAO_Retrieve();
			return;
		}
	}

	dg_1.ExcelUpload("pt", function (args) {
		_X.EP("pt", "SP_BM_SCHL_COSTREPORT", [mytop._CompanyCode, _CustCode, ls_yymm, "PT02010", mytop._UserID, mytop._ClientIP], function (res) {
			if ( res != -1 ) {
				_X.MsgBox("엑셀데이타 업로드 완료.");
				x_DAO_Retrieve();
			}
			else {
				_X.MsgBox("엑셀데이타 업로드 중 오류가 발생 하였습니다.");
			}
		});
	},
	null, 2, "PT02010", false);
}

function uf_delall(){
	if(_X.MsgBoxYesNo("확인", "선택된 데이터를 전체삭제 하시겠습니까?") == 2) return;

	var ls_yymm = _X.StrPurify(sle_basicmonth.value);
	var li_rtn = _X.EP('pt','SP_BM_SCHLCOSTREPORT_DEL', [mytop._CompanyCode,_CustCode,ls_yymm]);
	if(li_rtn<0){
		_X.MsgBox("확인","시장조사자료 삭제 시 오류가 발생했습니다!<BR>관리자에 문의해주세요!");
		return;
	}
	_X.MsgBox("확인","전체삭제 되었습니다!");
	x_DAO_Retrieve();
}

function uf_calc(){
	var ls_yymm = _X.StrPurify(sle_basicmonth.value);
	_X.EP('pt','SP_BM_MARKETCOST_CONVERT', [mytop._CompanyCode,_CustCode,ls_yymm]);
	x_DAO_Retrieve();
}

function uf_name_change(){
	var ls_yymm = _X.StrPurify(sle_basicmonth.value);
	if(_X.MsgBoxYesNo("확인", ls_yymm + "월의 품목명을 수정반영 처리하시겠습니까?") == 2) return;
	_X.EP('pt','SP_BM_ITEMNAME_CHANGE_ALL', [mytop._CompanyCode, ls_yymm]);
	x_DAO_Retrieve();
}

function uf_cost_change(){
	ls_yymm = _X.StrPurify(sle_basicmonth.value);
	if(_X.MsgBoxYesNo("확인", ls_yymm + "월의 단가를 일괄반영  처리하시겠습니까?") == 2) return;
	_X.EP('pt','SP_BM_COST_CHANGE_ALL', [mytop._CompanyCode, ls_yymm]);
	x_DAO_Retrieve();
}

function uf_item_matching() {
	var ls_yymm = _X.StrPurify(sle_basicmonth.value);
	_X.EP('pt','SP_BM_SCHLCOSTREPORT', [mytop._CompanyCode,_CustCode,ls_yymm]);
}

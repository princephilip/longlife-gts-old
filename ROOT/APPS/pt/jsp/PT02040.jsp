<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<jsp:include page="/Mighty/jsp/UploadExcelForm.jsp"/>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true"></html:authbutton>
		</span>
	</div>

	<div class='option_label' >기준년월</div>
	<div class="option_input_bg" >
		<html:basicdate id="dateimages" classType="search" dateType="month" moveBtn="true" readOnly="true"></html:basicdate>
	</div>

	<div id='userxbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<div class='option_label' style="border-width: 0px 0px 0px 0px;">적용단가</div>
			<div class='option_input_bg w100'>
				<select id="S_APPLYCOST" style="width:100%"></select>&nbsp;
			</div>
			<button type="button" id='btn_costupdate'    class="btn btn-success btn-xs" onClick="uf_cost_update();">일괄변경</button>&nbsp;&nbsp;
			<button type="button" id='btn_excelupload' class="btn btn-success btn-xs" onClick="uf_ExcelImport();">EXCEL 불러오기</button>
			<button type="button" id='btn_exceldown' class="btn btn-success btn-xs" onClick="uf_dw_excel();">EXCEL 다운로드</button>
		</span>
	</div>
</div>

<div id='grid_1' class='grid_div'>
	<div id="dg_1" class="slick-grid"></div>
</div>

<div id='grid_2' class='grid_div has_button mt5'>
	<div class='child_button float_right'>
		<html:authchildbutton id='buttons' grid='dg_2' append='true' insert='false' duplicate='false' delete='true' excel='false' retrieve='false' ></html:authchildbutton>
	</div>
	<div id="dg_2" class="slick-grid"></div>
</div>

<div id='grid_3' class='grid_div mt5'>
	<div id="dg_3" class='slick-grid'></div>
</div>

<div id='grid_down' class='grid_div' style= "visibility:hidden">
	<div id="dg_down" class='slick-grid'></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo	= {init_width:1000, init_height: 35, width_increase:1, height_increase:0};
	grid_1.ResizeInfo			= {init_width:1000, init_height:100, width_increase:1, height_increase:0.15};
	grid_2.ResizeInfo			= {init_width:1000, init_height:399, width_increase:1, height_increase:0.60};
	grid_3.ResizeInfo			= {init_width:1000, init_height:166, width_increase:1, height_increase:0.25};
	grid_down.ResizeInfo	= {init_width: 100, init_height: 10, width_increase:1, height_increase:0};

</script>


<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<jsp:include page="/Mighty/jsp/UploadExcelForm.jsp"/>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true"></html:authbutton>
		</span>
	</div>

	<div class='option_label' >기준년월</div>
	<div class="option_input_bg" >
		<html:basicdate id="dateimages" classType="search" dateType="month" moveBtn="true" readOnly="true"></html:basicdate>
	</div>

	<div class='option_label w100'>찾기</div>
	<div class='option_input_bg'>
		<input id='S_FIND' type='text' class='option_input_fix w150'>
		(식품명)&nbsp;&nbsp;
		<span id="span_text" style="font-weight:bold;color:blue;display:inline"></span>
		<span id="span_dc" style="font-weight:bold;color:red;display:inline;margin-left:10px"></span>
	</div>

	<div id='userxbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<button type="button" id='btn_change'     class="btn btn-primary btn-xs" onClick="uf_cost_change();">변경</button>
			<button type="button" id='btn_dataupload' class="btn btn-success btn-xs" onClick="uf_dataupload();">조사자료 Upload</button>
			<button type="button" id='btn_datadown'   class="btn btn-success btn-xs" onClick="uf_datadown();">조사결과 Down</button>
			<button type="button" id='btn_del_all'    class="btn btn-success btn-xs" onClick="uf_delall();">전체삭제</button>&nbsp;&nbsp;
			<button type="button" id='btn_auto_match' class="btn btn-warning btn-xs" onClick="uf_item_matching();">자동매칭</button>
		</span>
	</div>
</div>

<div id='grid_1' class='grid_div'>
	<div id="dg_1" class="slick-grid"></div>
</div>

<div id='grid_2' class='grid_div has_title'>
	<label class='sub_title'>이전 납품내역</label>
	<div id="dg_2" class="slick-grid"></div>
</div>

<div id='grid_down' class='grid_div' style= "visibility:hidden">
	<div id="dg_down" class='slick-grid'></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo	= {init_width:1000, init_height: 35, width_increase:1, height_increase:0};
	grid_1.ResizeInfo			= {init_width:1000, init_height:365, width_increase:1, height_increase:1};
	grid_2.ResizeInfo			= {init_width:1000, init_height:300, width_increase:1, height_increase:0};
	grid_down.ResizeInfo	= {init_width: 100, init_height: 10, width_increase:1, height_increase:0};

</script>


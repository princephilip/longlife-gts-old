<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<jsp:include page="/Mighty/jsp/UploadExcelForm.jsp"/>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true" pgmcode='PT02051'></html:authbutton>
		</span>
	</div>

	<div id='userxbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<button type="button" id='btn_cnfm' class="btn btn-success btn-xs" onClick="uf_order_confirm();">발주하기</button>
			<button type="button" id='btn_cncl' class="btn btn-success btn-xs" onClick="uf_order_cancel();">발주취소</button>
			<button type="button" id='btn_dlvy' class="btn btn-success btn-xs" onClick="uf_confirm_delivery();">배송확인</button>
		</span>
	</div>
</div>

<div id='form_right' class='grid_div'>

	<div id='form_free1' class='detail_box ver2'>
		<div class='detail_row'>
			<label class='detail_label' style="width:100px">학교명</label>
			<div class='detail_input_bg has_button' style="width:320px">
				<input type='text' id='CUST_NAME' class='detail_input_fix' readonly>
				<button id='btn_findcust' class="search_find_img" onClick="uf_findCust()" />
			</div>
			<label class='detail_label' style="width:100px">학교코드</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='CUST_CODE' class='detail_input_fix' readonly>
			</div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">배송요청일시</label>
			<div class='detail_input_bg' style="width:570px">
				<input type='text' id='USE_DATE' class='detail_date' style='width:100px'>&nbsp;일&nbsp;&nbsp;&nbsp;
				<select id="IN_STIME" class='detail_input_fix' style='width:60px'></select>&nbsp;시&nbsp;&nbsp;~&nbsp;
				<select id="IN_ETIME" class='detail_input_fix' style='width:60px'></select>&nbsp;시
			</div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">신청품목</label>
			<div class='detail_input_bg' style="width:320px">
				<select id="ITEM_CODE" class='detail_input_fix'></select>
			</div>
			<label class='detail_label' style="width:100px">상태</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='ORDER_STATUS' class='detail_input_fix' readonly>
			</div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">수량</label>
			<div class='detail_input_bg' style="width:320px">
				<input type='text' id='ITEM_QTY' class='detail_float2'>
			</div>
			<label class='detail_label' style="width:100px">금액</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='SUPPLY_AMT' class='detail_amt' readonly>
			</div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">공급업체</label>
			<div class='detail_input_bg' style="width:320px">
				<select id="CUST_CODE_BUYER" class='detail_input_fix'></select>
			</div>
			<label class='detail_label' style="width:100px">연락처</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='TEL_NO' class='detail_input_fix' readonly>
			</div>
		</div>

		<div class='detail_row detail_row_bb'>
			<label class='detail_label' style="width:100px">메모</label>
			<div class='detail_input_bg' style="width:570px">
				<textarea id="REMARKS2" style="height:90px;"></textarea>
			</div>
		</div>

	</div>

	<!-- <div id='grid_1' class='grid_div' style= "visibility:hidden"> -->
	<div id='grid_1' class='grid_div'>
		<div id="dg_1" class='slick-grid'></div>
	</div>

</div>


<script type="text/javascript">
	topsearch.ResizeInfo		= {init_width:1000, init_height: 35, width_increase:1, height_increase:0};

	form_right.ResizeInfo		= {init_width:1000, init_height:300, width_increase:1, height_increase:0};
		form_free1.ResizeInfo	= {init_width: 670, init_height:290, width_increase:0, height_increase:0};
		grid_1.ResizeInfo			= {init_width:1000, init_height:100, width_increase:1, height_increase:1};

</script>


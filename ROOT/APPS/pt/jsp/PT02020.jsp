<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<jsp:include page="/Mighty/jsp/UploadExcelForm.jsp"/>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true"></html:authbutton>
		</span>
	</div>

	<div class='option_label' >기준년월</div>
	<div class="option_input_bg" >
		<html:basicdate id="dateimages" classType="search" dateType="month" moveBtn="true" readOnly="true"></html:basicdate>
	</div>

	<div class='option_label w100'>찾기</div>
	<div class='option_input_bg'>
		<input id='S_FIND' type='text' class='option_input_fix w150'>
		(식품명)&nbsp;&nbsp;
	</div>

	<div class='option_input_bg D_RANGE' style='float: right;'>
		<input id='S_RANGE' type="range" class="S_RANGE_BAR" min="300" max="1000" step="10" name="range" value="600" style='width: 100px;'>
	</div>
	<div class='option_label D_RANGE' style='float: right;'>화면비율</div>

<!-- 	<div id='userxbtns' class="option_input_bg" style="padding:0px 3px 0px 0px;background-color: #f8f8f8;border-width: 0px 0px 0px 0px;font-weight:bold;color:#666633;">
		<span style="width:100%;height:100%;vertical-align:middle;">
				사이즈변경
				<input id='S_SIZE' type='text' class='option_amt' style="width:40px">
				<button type="button" id='btn_change' class="btn btn-primary btn-xs" onClick="uf_size();">변경</button>
		    &nbsp;&nbsp;<input id='S_SHOW' type='checkbox' checked=true>다른브랜드품목보기&nbsp;&nbsp;&nbsp;
		</span>
	</div> -->
</div>

<div id='grid_1' class='grid_div mr5'>
	<div id="dg_1" class="slick-grid"></div>
</div>

<div id='grid_2' class='grid_div'>
	<div id="dg_2" class="slick-grid"></div>
</div>


<script type="text/javascript">
	topsearch.ResizeInfo	= {init_width:1000, init_height: 35, width_increase:1, height_increase:0};
	grid_1.ResizeInfo			= {init_width: 600, init_height:665, width_increase:1, height_increase:1};
	grid_2.ResizeInfo			= {init_width: 400, init_height:665, width_increase:0, height_increase:1};

</script>


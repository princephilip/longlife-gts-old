<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<style>
  #status-info .stats { cursor: hand; }
</style>

<div id="status-info" class="row">
<!--   <div class="col-md-3">
    <div class="card">
      <div class="content">
        <div class="row">
          <div class="col-xs-5">
            <div class="icon-big icon-warning text-center">
              <i class="ti-truck"></i>
            </div>
          </div>
          <div class="col-xs-7">
            <div class="numbers">
              <a href="/XG_page.do?pcd=PT0301010">
                <p>{{item.cnt_order_label}}</p>
                {{item.cnt_order}}
              </a>
            </div>
          </div>
        </div>
        <div class="footer">
          <hr>
          <div class="stats">
            <i class="ti-reload"></i> Updated now
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="card">
      <div class="content">
        <div class="row">
          <div class="col-xs-5">
            <div class="icon-big icon-info text-center">
              <i class="ti-envelope"></i>
            </div>
          </div>
          <div class="col-xs-7">
            <div class="numbers">
              <a href="/XG_page.do?pcd=PT0301030">
                <p>{{item.cnt_calc_label}}</p>
                {{item.cnt_calc}}
              </a>
            </div>
          </div>
        </div>
        <div class="footer">
          <hr>
          <div class="stats">
            <i class="ti-reload"></i> Updated now
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="card">
      <div class="content">
        <div class="row">
          <div class="col-xs-5">
            <div class="icon-big icon-success text-center">
              <i class="ti-write"></i>
            </div>
          </div>
          <div class="col-xs-7">
            <div class="numbers">
              <a href="/XG_page.do?pcd=PT0201010">
                <p>{{item.cnt_bidd_label}}</p>
                {{item.cnt_bidd}}
              </a>
            </div>
          </div>
        </div>
        <div class="footer">
          <hr>
          <div class="stats">
            <i class="ti-reload"></i> Updated now
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="card">
      <div class="content">
        <div class="row">
          <div class="col-xs-5">
            <div class="icon-big icon-danger text-center">
              <i class="ti-money"></i>
            </div>
          </div>
          <div class="col-xs-7">
            <div class="numbers">
              <a href="/XG_page.do?pcd=PT0201020">
                <p>{{item.cnt_estim_label}}</p>
                {{item.cnt_estim}}
              </a>
            </div>
          </div>
        </div>
        <div class="footer">
          <hr>
          <div class="stats">
            <i class="ti-reload"></i> Updated now
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="header">
        <h5 class="title">납품할 발주내역</h5>
        <p class="small">납품을 요청한 최근 발주내역 목록</p>
      </div>

      <div class="table-responsive">
        <table id="ordr-list" class="table table-hover">
          <thead>
            <tr>
              <th class="per3">No.</th>
              <th class="per7">발주번호</th>
              <th class="per33">현장</th>
              <th class="per50">품목</th>
              <th class="per7">발주일자</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="(dt, idx) in data" v-bind:p-no="dt.order_no" v-bind:p-idx="idx">
              <td>{{idx+1}}</td>
              <td class="small text">{{dt.proj_order_no}}</td>
              <td class="small text">{{dt.proj_name}}</td>
              <td class="long-text">{{dt.item_name}} 외 {{dt.item_cnt - 1}}건</td>
              <td class="small text">{{dt.order_date}}</td>
            </tr>
          </tbody>
        </table>
      </div>

    </div>
  </div>

  <div class="col-md-6">
    <div class="card">
      <div class="header">
        <h5 class="title">정산대상 발주내역</h5>
        <p class="small">정산 확인이 필요한 최근 발주내역 목록</p>
      </div>

      <div class="table-responsive">
        <table id="acct-list" class="table table-hover">
          <thead>
            <tr>
              <th class="per3">No.</th>
              <th class="per7">발주번호</th>
              <th class="per33">현장</th>
              <th class="per50">품목</th>
              <th class="per7">공급가액</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="(dt, idx) in data" v-bind:p-no="dt.order_no" v-bind:p-idx="idx">
              <td>{{idx+1}}</td>
              <td class="small text">{{dt.proj_order_no}}</td>
              <td class="small text">{{dt.proj_name}}</td>
              <td class="long-text">{{dt.item_name}}</td>
              <td class="small text-right">{{dt.input_amt | number}}</td>
            </tr>
          </tbody>
        </table>
      </div>

    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="header">
        <h5 class="title">입찰요청</h5>
        <p class="small">입찰 가능한 최신 목록</p>
      </div>

      <div class="table-responsive">
        <table id="bidd-list1" class="table table-hover">
          <thead>
            <tr>
              <th class="per3">No.</th>
              <th class="per7">입찰번호</th>
              <th class="per35">현장</th>
              <th class="per36">입찰명</th>
              <th class="per7">의뢰일자</th>
              <th class="per7">마감일자</th>
              <th class="per5">상태</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="(dt, idx) in data" v-bind:p-no="dt.bid_no" v-bind:p-idx="idx">
              <td>{{idx+1}}</td>
              <td class="small text">{{dt.proj_bid_no}}</td>
              <td class="small text">{{dt.proj_name}}</td>
              <td class="long-text">{{dt.title}}</td>
              <td class="small text">{{dt.req_date}}</td>
              <td class="small text">{{dt.bid_edate}}</td>
              <td class="small text">{{dt.status_name}}</td>
            </tr>
          </tbody>
        </table>
      </div>

    </div>
  </div>

  <div class="col-md-6">
    <div class="card">
      <div class="header">
        <h5 class="title">견적요청</h5>
        <p class="small">견적을 요청한 최근 목록</p>
      </div>

      <div class="table-responsive">
        <table id="bidd-list2" class="table table-hover">
          <thead>
            <tr>
              <th class="per3">No.</th>
              <th class="per7">견적번호</th>
              <th class="per35">현장</th>
              <th class="per36">견적명</th>
              <th class="per7">의뢰일자</th>
              <th class="per7">마감일자</th>
              <th class="per5">상태</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="(dt, idx) in data" v-bind:p-no="dt.bid_no" v-bind:p-idx="idx">
              <td>{{idx+1}}</td>
              <td class="small text">{{dt.proj_bid_no}}</td>
              <td class="small text">{{dt.proj_name}}</td>
              <td class="long-text">{{dt.title}}</td>
              <td class="small text">{{dt.req_date}}</td>
              <td class="small text">{{dt.bid_edate}}</td>
              <td class="small text">{{dt.status_name}}</td>
            </tr>
          </tbody>
        </table>
      </div>

    </div>
  </div> -->
</div>

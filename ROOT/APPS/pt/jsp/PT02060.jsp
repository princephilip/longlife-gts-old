<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<jsp:include page="/Mighty/jsp/UploadExcelForm.jsp"/>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true"></html:authbutton>
		</span>
	</div>

	<div class='option_label' >기준년월</div>
	<div class="option_input_bg" >
		<html:basicdate id="dateimages" classType="search" dateType="month" moveBtn="true" readOnly="true"></html:basicdate>
	</div>

	<div class='option_label w100'>발주서</div>
	<div class='option_input_bg'>
		<select id="S_ORDER_NO" style="width:260px"></select>
	</div>

	<div class='option_label w100'>품목명(찾기)</div>
	<div class='option_input_bg'>
		<input id='S_FIND' type='text' class='option_input_fix w150'>
	</div>

	<div id='userxbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<div class="option_input_bg" style="padding:1px 5px 0px 5px;background-color: #f8f8f8;border-width: 0px 0px 0px 0px;color:#666633;">
				<span style="color:blue;" id="span_closetime"></span>
			</div>
			<button type="button" id='btn_invoice' class="btn btn-success btn-xs" onClick="uf_invoice();">거래명세서</button>
		</span>
	</div>
</div>

<div id='grid_1' class='grid_div'>
	<div id="dg_1" class="slick-grid"></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo	= {init_width:1000, init_height: 35, width_increase:1, height_increase:0};
	grid_1.ResizeInfo			= {init_width:1000, init_height:665, width_increase:1, height_increase:1};

</script>


<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<style>
.detail_box.ver2 th {
  text-align: center !important;
  color: #494949;
  background-color: #f3f5f3;
  border-top: 1px solid #d0d0d0;
  border-left: 1px solid #d0d0d0;
}

.detail_box.ver2 td {
  vertical-align: top;
  border-top: 1px solid #d0d0d0;
  border-left: 1px solid #d0d0d0;
}

.detail_box.ver2 td.bg_readonly {
  background-color: #f0f0f0;
}

.detail_box.ver2 td.bg_sky {
  background-color: #DBEEF4;
}

.detail_box.ver2 td.bg_pink {
  background-color: #FDEADA;
}

.detail_box.ver2 td.bg_dsky {
  background-color: #B9CDE5;
}

.detail_box.ver2 td .detail_text {
  padding: 1px 0px 1px 2px;
}

.detail_box.ver2 td input.c_transparent {
  color: transparent;
}

.detail_box.ver2 table {
  width: 100%;
  border-bottom: 1px solid #d0d0d0;
  border-right: 1px solid #d0d0d0;
  margin-bottom: 6px;
}

.detail_box.ver2 table input[type=text] {
  border: 0;
  height: 25px;
  background-color: transparent;
}


.grid_div.has_title .sub_unit, .detail_box.ver2.has_title .sub_unit {
  float: right;
  margin-top: -22px;
  padding-right: 15px;
}

.even .slick-cell.selected, .odd .slick-cell.selected {
  border-bottom: 1px solid rgba(208, 208, 208, 1);
}

.detail_amt, .detail_float1, .detail_float2, .detail_box.ver2 td .detail_text.tar {
  padding-right: 2px;
}

</style>


<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true"></html:authbutton>
		</span>
	</div>

	<div class='option_label'>구분</div>
	<div class='option_input_bg w100'>
		<select id="S_END_YN" style="width:100%"></select>&nbsp;
	</div>

</div>

<div id='freeform' class='detail_box ver2'>

	<div id='freeform31' class='detail_box ver2 has_title'>
		<label class='sub_title'>요청사항 등록</label>
	  <table>
	    <colgroup>
	      <col width="100" />
	      <col width="150" />
	      <col width="750" />
	    </colgroup>
	    <tr>
	      <th class="text-center">보낸날짜</th>
	      <td><input type='text' id='ROW_INPUT_DATE' class='detail_input_fix' style="text-align:center" readonly /></td>
	      <th class="text-center" >*내용</th>
	    </tr>
	    <tr>
	      <th class="text-center">*요청구분</th>
	      <td style="padding-top: 2px;padding-left: 3px;padding-right: 3px;"><select id="REQ_DIV" style="width:100%"></select></td>
	      <td rowspan="2"><textarea id="CONTENTS" style="height:50px;"></textarea></td>
	    </tr>
	    <tr>
	      <th class="text-center">확인유무</th>
	      <td><input type='text' id='END_YN' class='detail_input_fix' style="text-align:center" readonly /></td>
	    </tr>
	  </table>
	</div>
</div>


<div id='grid_1' class='grid_div has_title'>
	<label class='sub_title'>요청사항 현황</label>
	<div id="dg_1" class="slick-grid"></div>
</div>


<script type="text/javascript">
	topsearch.ResizeInfo	= {init_width:1000, init_height: 35, width_increase:1, height_increase:0};
	freeform.ResizeInfo		= {init_width:1000, init_height:120, width_increase:1, height_increase:0};
	grid_1.ResizeInfo			= {init_width:1000, init_height:545, width_increase:1, height_increase:1};

</script>


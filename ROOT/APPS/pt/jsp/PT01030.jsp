<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true"></html:authbutton>
		</span>
	</div>

</div>

<div id='grid_1' class='grid_div mr5'>
	<div id="dg_1" class="slick-grid"></div>
</div>

<div id='freeform' class='detail_box ver2'>
	<div class='detail_row'>
		<label class='detail_label' style="width:120px">학교명</label>
		<div class='detail_input_bg' style="width:250px">
			<input type='text' id='CUST_NAME' class='detail_input_fix' style="width:180px" readonly>
			<input type='text' id='LOC_DIV_NAME' class='detail_input_fix' style="width:58px" readonly>
		</div>
		<div class='detail_input_bg' style="width:280px">
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:120px">*전화번호</label>
		<div class='detail_input_bg' style="width:250px">
			<input type='text' id='TEL_NO' class='detail_input_fix'>
		</div>
		<label class='detail_label' style="width:120px">*FAX</label>
		<div class='detail_input_bg' style="width:160px">
			<input type='text' id='FAX' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:120px">*주소</label>
		<div class='detail_input_bg' style="width:530px">
			<input type='text' id='ZIP_CODE' class='detail_input_fix' style="width:68px" maxlength='6'>
			<input type='text' id='ADDR' class='detail_input_fix' style="width:450px">
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:120px">*상세주소</label>
		<div class='detail_input_bg' style="width:530px">
			<input type='text' id='ADDR2' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:120px">보증금등급</label>
		<div class='detail_input_bg' style="width:250px">
			<select id="SUPPORT_GRADE" style="width:100%"></select>
		</div>
		<label class='detail_label' style="width:120px">*대표자</label>
		<div class='detail_input_bg' style="width:160px">
			<input type='text' id='OWNER_NAME' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:120px">배송업체</label>
		<div class='detail_input_bg' style="width:250px">
			<input type='text' id='DELIVERY_NAME' class='detail_input_fix' style="width:140px" readonly>
			<input type='text' id='DELIVERY_CAR_NAME' class='detail_input_fix' style="width:98px" readonly>
		</div>
		<div class='detail_input_bg' style="width:280px">
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:120px">*비고</label>
		<div class='detail_input_bg' style="width:250px">
			<input type='text' id='REMARKS' class='detail_input_fix'>
		</div>
		<label class='detail_label' style="width:120px">*매출그룹</label>
		<div class='detail_input_bg' style="width:160px">
			<select id="SGRP_CODE" style="width:100%"></select>
		</div>
	</div>

	<div class='detail_row detail_row_bb'>
		<label class='detail_label' style="width:120px">*국공립구분</label>
		<div class='detail_input_bg' style="width:250px">
			<select id="SCHOOL_GBN" style="width:100%"></select>
		</div>
		<div class='detail_input_bg' style="width:280px">
		</div>
	</div>

</div>


<div id='grid_chbtn' style='position: absolute; width:100%; margin-top: 263px; text-align:right;'>
	<span style="float:right;">
		<html:authchildbutton id='buttons' grid='dg_2' append='true' insert='false' duplicate='false' delete='true' excel='false' retrieve='false' ></html:authchildbutton>
	</span>
</div>

<div id="tabs_1" class='tabs_div'>
	<div id="ctab_nav" class="tabs_nav">
		<ul>
			<li><a href="#ctab_1">영양(교사)</a></li>
		</ul>
	</div>

	<div id="ctab_1" class='tabc_div'>
		<div id='grid_2' class='grid_div'>
			<div id="dg_2" class="slick-grid"></div>
		</div>
	</div>
</div>


<script type="text/javascript">
	topsearch.ResizeInfo	= {init_width:1000, init_height: 35, width_increase:1, height_increase:0};
	grid_1.ResizeInfo			= {init_width: 350, init_height:665, width_increase:1, height_increase:1};
	freeform.ResizeInfo		= {init_width: 650, init_height:230, width_increase:0, height_increase:0};
	tabs_1.ResizeInfo			= {init_width: 650, init_height:435, width_increase:0, height_increase:1};
	ctab_1.ResizeInfo			= {init_width: 650, init_height:410, width_increase:1, height_increase:1};
	grid_2.ResizeInfo			= {init_width: 650, init_height:410, width_increase:1, height_increase:1};

</script>


<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<jsp:include page="/Mighty/jsp/UploadExcelForm.jsp"/>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true"></html:authbutton>
		</span>
	</div>

	<div class='option_label'>조회일자</div>
	<div class="option_input_bg" >
		<html:fromtodate id="dateimages"  classType="option" dateType="date" moveBtn="true" readOnly="false"></html:fromtodate>
	</div>

	<div class='option_label w100'>학교명</div>
	<div class='option_input_bg'>
		<input id='S_CUST_NAME' type='text' class='option_input_fix'>
	</div>

	<div class='option_label w100'>공급처</div>
	<div class='option_input_bg'>
		<input id='S_SUPPLIER' type='text' class='option_input_fix'>
	</div>

	<div id='userxbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<div class="option_input_bg" style="padding:1px 5px 0px 5px;background-color: #f8f8f8;border-width: 0px 0px 0px 0px;color:#666633;">
				<span style="color:red; font-weight:bold;" >※ 거래명세서는 공급자확인 이후 출력가능합니다!&nbsp;&nbsp;</span>
			</div>
			<button type="button" id='btn_edit'    class="btn btn-success btn-xs" onClick="uf_openedit('');">신청</button>
			<button type="button" id='btn_chk'     class="btn btn-success btn-xs" onClick="uf_buyerchk();">공급자확정</button>
			<button type="button" id='btn_cancel'  class="btn btn-warning btn-xs" onClick="uf_buyerchkcancel();">확정취소</button>
			<button type="button" id='btn_invoice' class="btn btn-primary btn-xs" onClick="uf_invoice();">거래명세서</button>&nbsp;&nbsp;
		</span>
	</div>
</div>

<div id='grid_1' class='grid_div mr5'>
	<div id="dg_1" class="slick-grid"></div>
</div>

<div id='form_right' class='grid_div'>

	<div id='form_free1' class='detail_box ver2'>
		<div class='detail_row'>
			<label class='detail_label' style="width:100px">학교</label>
			<div class='detail_input_bg' style="width:400px">
				<input type='text' id='CUST_NAME' class='detail_input_fix' readonly>
			</div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">신청품목</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='ITEM_NAME' class='detail_input_fix' readonly>
			</div>
			<label class='detail_label' style="width:100px">상태</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='ORDER_STATUS' class='detail_input_fix' readonly>
			</div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">수량</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='ITEM_QTY' class='detail_amt' readonly>
			</div>
			<label class='detail_label' style="width:100px">금액</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='SUPPLY_AMT' class='detail_amt' readonly>
			</div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">배송요청일시</label>
			<div class='detail_input_bg' style="width:400px">
				<input type='text' id='IN_DT' class='detail_input_fix' readonly>
			</div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">공급업체</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='CUST_BUYER_NAME' class='detail_input_fix' readonly>
			</div>
			<label class='detail_label' style="width:100px">연락처</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='TEL_NO' class='detail_input_fix' readonly>
			</div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">메모</label>
			<div class='detail_input_bg' style="width:400px">
				<textarea id="REMARKS2" style="height:90px;" readonly></textarea>
			</div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">공급자확인일시</label>
			<div class='detail_input_bg' style="width:400px">
				<input type='text' id='ORDER_CHK_DATE' class='detail_input_fix' readonly>
			</div>
		</div>

		<div class='detail_row detail_row_bb'>
			<label class='detail_label' style="width:100px">배송확인일시</label>
			<div class='detail_input_bg' style="width:400px">
				<input type='text' id='IN_CHK_DATE' class='detail_input_fix' readonly>
			</div>
		</div>

	</div>

	<div id='form_free2' class='detail_box ver2'>
		<div class='detail_row'>
			<label class='detail_label' style="width:100px">배정예산</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='SUPPORT_AMT' class='detail_amt'>
			</div>
			<label class='detail_label' style="width:100px">조정예산</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='ADD_SUPPORT_AMT' class='detail_amt'>
			</div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">보조금합계</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='SUPPORT_TOT' class='detail_amt' readonly>
				 <!-- support_amt +  add_support_amt  -->
			</div>
			<label class='detail_label' style="width:100px">타사사용금액</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='ALREADY_USED_AMT' class='detail_amt'>
			</div>
		</div>

		<div class='detail_row'>
			<label class='detail_label' style="width:100px">사용금액</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='USED_AMT' class='detail_amt' readonly>
			</div>
			<label class='detail_label' style="width:100px">잔여금액</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='REMAIN_AMT' class='detail_amt' readonly>
				 <!-- support_amt +  add_support_amt - already_used_amt -  used_amt  -->
			</div>
		</div>

		<div class='detail_row detail_row_bb'>
			<label class='detail_label' style="width:100px">구매가능(농협)</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='BUY_NH' class='detail_float1' style="width:90px" readonly>&nbsp;포&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;끼
				<!-- (remain_amt / 21610) -->
			</div>
			<label class='detail_label' style="width:100px">구매가능(조합)</label>
			<div class='detail_input_bg' style="width:150px">
				<input type='text' id='BUY_JH' class='detail_float1' style="width:90px" readonly>&nbsp;포&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;끼
				<!-- (remain_amt / 19610) -->
			</div>
		</div>

	</div>

	<div id='grid_3' class='grid_div has_title'>
		<label class='sub_title'>보조금 사용내역</label>
		<div id="dg_3" class="slick-grid"></div>
	</div>
</div>

<div id='grid_2' class='grid_div' style= "visibility:hidden">
	<div id="dg_2" class='slick-grid'></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo		= {init_width:1000, init_height: 35, width_increase:1, height_increase:0};
	grid_1.ResizeInfo				= {init_width: 500, init_height:665, width_increase:1, height_increase:1};

	form_right.ResizeInfo		= {init_width: 500, init_height:665, width_increase:0, height_increase:1};
		form_free1.ResizeInfo	= {init_width: 500, init_height:300, width_increase:0, height_increase:0};
		form_free2.ResizeInfo	= {init_width: 500, init_height:115, width_increase:0, height_increase:0};
		grid_3.ResizeInfo			= {init_width: 500, init_height:245, width_increase:0, height_increase:1};

	grid_2.ResizeInfo				= {init_width: 100, init_height: 10, width_increase:1, height_increase:0};

</script>


<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<jsp:include page="/Mighty/jsp/UploadExcelForm.jsp"/>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true"></html:authbutton>
		</span>
	</div>

	<div class='option_label' >기준년월</div>
	<div class="option_input_bg" >
		<html:basicdate id="dateimages" classType="search" dateType="month" moveBtn="true" readOnly="true"></html:basicdate>
	</div>
	<div class="option_input_bg" style="padding:5px 3px 0px 5px;background-color: #f8f8f8;border-width: 0px 0px 0px 0px;color:#666633;">
		<span style="color:red; font-weight:bold;" >&nbsp※ 기준연월 선택시 해당 월과 익월 자료가 표시됩니다.&nbsp&nbsp</span>
	</div>

	<div id='userxbtns' class="option_input_bg" style="background-color: #f8f8f8;border-width: 0px 0px 0px 0px;color:#666633;">
		<span style="width:100%;height:100%;vertical-align:middle;">
			<div class="option_input_bg" style="padding:1px 5px 0px 5px;background-color: #f8f8f8;border-width: 0px 0px 0px 0px;color:#666633;">
				<span style="color:red; font-weight:bold;" >&nbsp※ 매주 목요일 09:00이전 등록</span>
			</div>
			<button type="button" id='btn_excelupload' class="btn btn-success btn-xs" onClick="uf_ExcelImport();">EXCEL 불러오기</button>
		</span>
	</div>

	<div class="option_line" style="border-top:0px"></div>

	<div class="option_input_bg" style="padding:5px 3px 0px 5px;background-color: #f8f8f8;border-width: 0px 0px 0px 0px;color:#666633;">
		<span style="color:green; font-weight:bold;">&nbsp* 나이스의 발주서를 등록하시고, MS Excel 97-2003형태로 저장해주세요.</span>
	</div>

	<div class="option_input_bg" style="float:right;padding:5px 3px 0px 5px;background-color: #f8f8f8;border-width: 0px 0px 0px 0px;color:#666633;">
		<a href="/manual/OrderQuickGuide20140311.pdf" target="_blank"><span style="color:magenta; font:bold 12">* 프로그램 설명서 보기(2014.03.11 업데이트)</span></a>
	</div>

	<div class="option_line" style="border-top:0px"></div>

	<div class="option_input_bg" style="padding:2px 3px 0px 2px;background-color: #f8f8f8;border-width: 0px 0px 0px 0px;color:#666633;">
		<button type="button" id='btn_exceldown' class="btn btn-success btn-xs" onClick="uf_fileopen();">EXCEL 다운로드</button>
	</div>

	<div class="option_input_bg" style="background-color: #f8f8f8;border-width: 0px 0px 0px 0px;color:#666633;;position:absolute;top:58px;left:355px">
		<button type="button" id='btn_ordr_del'  class="btn btn-success btn-xs" onClick="uf_order_del();">발주삭제</button>
	</div>

	<div id='userxbtns'>
		<button type="button" id='btn_ordr_esti' class="btn btn-success btn-xs" onClick="uf_order_estimate();">발주예정보기</button>
		<button type="button" id='btn_cnfm_amt'  class="btn btn-success btn-xs" onClick="uf_confirm_amt();">발주금액비교완료</button>
	</div>

</div>

<div id='form_left' class='grid_div'>
	<div id='grid_3' class='grid_div'>
		<div id="dg_3" class="slick-grid"></div>
	</div>

	<div id='grid_2' class='grid_div mt5'>
		<div id="dg_2" class="slick-grid"></div>
	</div>
</div>

<div id='grid_1' class='grid_div ml5'>
	<div id="dg_1" class="slick-grid"></div>
</div>




<script type="text/javascript">
	topsearch.ResizeInfo	= {init_width:1000, init_height: 83, width_increase:1, height_increase:0};
	form_left.ResizeInfo	= {init_width: 420, init_height:615, width_increase:0, height_increase:1};
		grid_3.ResizeInfo		= {init_width: 420, init_height:465, width_increase:0, height_increase:1};
		grid_2.ResizeInfo		= {init_width: 420, init_height:150, width_increase:0, height_increase:0};
	grid_1.ResizeInfo			= {init_width: 580, init_height:615, width_increase:1, height_increase:1};

</script>


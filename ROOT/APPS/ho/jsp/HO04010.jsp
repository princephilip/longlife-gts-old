<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true" pgmcode="HO04010"></html:authbutton>
		</span>
	</div>

	<div class='option_label_left'>작성일자</div>
	<div class='option_input_bg'>
		<html:fromtodate id="dateimages" classType="search" dateType="today" moveBtn="true" readOnly="false"></html:fromtodate>
	</div>

	<div class='option_label_left'>검색</div>
	<div class="option_input_bg">
		<select id="S_DIV" style="width:80px">
			<option value="1">제목</option>
	    <option value="2">내용</option>
	    <option value="3">작성자</option>
		</select>
	</div>
	<div class="option_input_bg">
		<input id='S_FIND' type='text' class='detail_input_fix w150'>
	</div>


</div>

<div id='grid_1' class='grid_div'>
	<div id="dg_1" class="slick-grid"></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo			= {init_width:1000, init_height: 33, width_increase:1, height_increase:0};
	grid_1.ResizeInfo					= {init_width:1000, init_height:660, width_increase:1, height_increase:1};
</script>


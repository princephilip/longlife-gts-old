<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<script type="text/javascript" src="/Mighty/js/md5.js"></script>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_2' gridCall="true" pgmcode="HO03030"></html:authbutton>
		</span>
	</div>

	<div class="option_input_bg" style="padding:5px 3px 0px 5px;background-color: #f8f8f8;border-width: 0px 0px 0px 0px;color:#666633;">
		<span style="color:red; font-weight:bold;" >&nbsp ※ 새로운 지사명이 조회되지 않을 때는 '본사 > 일반관리 > 사용자-센터권한설정' 메뉴에서 센터 접근권한이 있는지 확인하세요.</span>
	</div>

	<div class="option_line" style="border-top:0px"></div>

	<div class='option_label w100'>기존지사</div>
	<div class='option_input_bg'>
		<select id="S_DEPT_ID" style="width:260px"></select>
		<img id="btn_finddept" src="/Theme/images/btn/btn_find.gif" class="search_find_img" onClick="uf_findDept('S_DEPT_ID')"/>
	</div>
	<div class="option_input_bg" style="width:300px;padding:3px 3px 0px 5px;background-color: #f8f8f8;border-width: 0px 0px 0px 0px;color:#666633;">
		<span>&nbsp(현재 사용중인 기존 지사)</span>
	</div>

	<div class='option_label w100'>새로운지사</div>
	<div class='option_input_bg'>
		<select id="S_DEPT_ID2" style="width:260px"></select>
		<img id="btn_finddept2" src="/Theme/images/btn/btn_find.gif" class="search_find_img" onClick="uf_findDept('S_DEPT_ID2')"/>
	</div>
	<div class="option_input_bg" style="width:300px;padding:3px 3px 0px 5px;background-color: #f8f8f8;border-width: 0px 0px 0px 0px;color:#666633;">
		<span>&nbsp(새로 사용할 예정인 지사)</span>
	</div>

</div>



<div id='form_1' class='grid_div has_title has_button'>
   	<label class='sub_title'>요보사 이동</label>
		<div class='child_button float_right'>
			<button type="button" id='B_MEMBER' class="btn btn-success btn-xs" onClick="uf_copy_member();">요보사 이동</button>
		</div>

    <div id='grid_1' class='grid_div mr10'>
      <div id="dg_1" class='slick-grid'></div>
    </div>

    <div id='grid_2' class='grid_div'>
      <div id="dg_2" class='slick-grid'></div>
    </div>
</div>

<div id='form_2' class='grid_div has_title has_button'>
		<label class='sub_title'>대상자 이동</label>
		<div class='child_button float_right'>
			<button type="button" id='B_CUST' class="btn btn-success btn-xs" onClick="uf_copy_cust();">대상자 이동</button>
		</div>

 		<div id='grid_3' class='grid_div mr10'>
			<div id="dg_3" class='slick-grid'></div>
		</div>

    <div id='grid_4' class='grid_div'>
      <div id="dg_4" class='slick-grid'></div>
    </div>
</div>



<script type="text/javascript">
	topsearch.ResizeInfo		= {init_width:1000, init_height: 60, width_increase:1, height_increase:0};
	form_1.ResizeInfo  			= {init_width:1000, init_height:310, width_increase:1, height_increase:0.5};
		grid_1.ResizeInfo  		= {init_width: 450, init_height:275, width_increase:0.45, height_increase:1};
		grid_2.ResizeInfo  		= {init_width: 550, init_height:275, width_increase:0.55, height_increase:1};

	form_2.ResizeInfo  			= {init_width:1000, init_height:320, width_increase:1, height_increase:0.5};
		grid_3.ResizeInfo  		= {init_width: 450, init_height:295, width_increase:0.45, height_increase:1};
		grid_4.ResizeInfo  		= {init_width: 550, init_height:295, width_increase:0.55, height_increase:1};
</script>


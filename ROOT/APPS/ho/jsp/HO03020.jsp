<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<script type="text/javascript" src="/Mighty/js/md5.js"></script>



<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true" pgmcode="HO03020"></html:authbutton>
		</span>
	</div>

	<div class='option_label w100'>지사찾기</div>
	<div class='option_input_bg'>
		<input id='S_FIND' type='text' class='option_input_fix' style="width:200px">
		<select id="S_USE_YN" class="option_input_fix" style="width:100px"></select>
	</div>
</div>


<div id='form_left' class='grid_div'>
	<div id='grid_1' class='grid_div mr5'>
		<div id="dg_1" class="slick-grid"></div>
	</div>

  <div id='grid_3' class='grid_div has_button mr5 mt5'>
		<div class='child_button float_right'>
			<html:authchildbutton id='buttons' grid='dg_3' append='true' insert='false' duplicate='false' delete='true' excel='false' retrieve='false' ></html:authchildbutton>
		</div>
    <div id="dg_3" class='slick-grid'></div>
  </div>
</div>


<div id='form_free' class='detail_box ver2'>
	<div class='detail_row'>
		<label class='detail_label' style="width:100px">*지사ID</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='DEPT_ID' class='detail_input_fix'>
		</div>

		<label class='detail_label' style="width:100px">*지사명</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='DEPT_NAME' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">직영구분</label>
		<div class='detail_input_bg' style="width:150px">
			<select id="OPER_DIV" class='detail_input_fix'></select>
		</div>

		<label class='detail_label' style="width:100px">기관코드</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='SERVICE_CODE' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">우편번호</label>
		<div class="detail_input_bg has_button" style="width:100px">
      <input type="text" id="ZIP_CODE">
      <button id="btn_zipfind" class="search_find_img" onClick="uf_ZipFind('ZIP_CODE')"></button>
		</div>
    <div class="detail_input_bg noborder" style="width:300px">
      <input type="text" id="ADDR1">
    </div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">주소</label>
		<div class='detail_input_bg' style="width:400px">
			<input type='text' id='ADDR2' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">전화번호</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='PHONE' class='detail_input_fix'>
		</div>

		<label class='detail_label' style="width:100px">팩스번호</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='FAX' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">지사장명</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='OWNER_NAME' class='detail_input_fix'>
		</div>

		<label class='detail_label' style="width:100px">지사장핸드폰</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='OWNER_HP' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">소장명</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='MANAGER_NAME' class='detail_input_fix'>
		</div>

		<label class='detail_label' style="width:100px">소장핸드폰</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='MANAGER_HP' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">사업자번호</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='BIZ_NO' class='detail_input_fix'>
		</div>

		<label class='detail_label' style="width:100px">사업장명</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='BIZ_NAME' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">업태</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='BIZ_UPTAE' class='detail_input_fix'>
		</div>

		<label class='detail_label' style="width:100px">업종</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='BIZ_UPJONG' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">사업장주소1</label>
		<div class='detail_input_bg' style="width:400px">
			<input type='text' id='BIZ_ADDR1' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">사업장주소2</label>
		<div class='detail_input_bg' style="width:400px">
			<input type='text' id='BIZ_ADDR2' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">임차보증금</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='RENT_BAMT' class='detail_amt'>
		</div>

		<label class='detail_label' style="width:100px">월임대료</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='RENT_MAMT' class='detail_amt'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">초기운영</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='OPER_AMT' class='detail_amt'>
		</div>

		<div class='detail_input_bg' style="width:250px">
			<div style='width:100px'><input id='SALECENTER_YN' type='checkbox' disabled=true />&nbsp;물류센터</div>
			<div style='width:150px'><input id='SALECENTER_MAIN' type='checkbox' disabled=true />&nbsp;물품요청 물류센터</div>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">폐업여부</label>
		<div class='detail_input_bg' style="width:150px">
			<select id="USE_YN" class='detail_input_fix'></select>
		</div>

		<label class='detail_label' style="width:100px">급여계산구분</label>
		<div class='detail_input_bg' style="width:150px">
			<select id="PAY_CALC_DIV" class='detail_input_fix'></select>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">대리지사ID</label>
		<div class="detail_input_bg has_button" style="width:150px">
      <input type="text" id="PROXY_DEPTID" class='detail_input_fix'>
      <button id="btn_deptfind" class="search_find_img" onClick="uf_findDept('PROXY_DEPTID')"></button>
		</div>
		<label class='detail_label' style="width:100px">대리지사명</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='PROXY_DEPTNAME' class='detail_input_fix' readonly>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">물류센터ID</label>
		<div class="detail_input_bg has_button" style="width:150px">
      <input type="text" id="UPPER_DEPTID" class='detail_input_fix'>
      <button id="btn_deptfind" class="search_find_img" onClick="uf_FindSalecenterCode('UPPER_DEPTID')"></button>
		</div>
		<label class='detail_label' style="width:100px">물류센터명</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='UPPER_DEPTNAME' class='detail_input_fix' readonly>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">담당자</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='CHARGE_MAN' class='detail_input_fix'>
		</div>

		<label class='detail_label' style="width:100px">담당자연락처</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='CHARGE_TEL' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row detail_row_bb'>
		<label class='detail_label' style="width:100px">담당자FAX</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='CHARGE_FAX' class='detail_input_fix'>
		</div>

		<label class='detail_label' style="width:100px"></label>
		<div class='detail_input_bg' style="width:150px">
		</div>
	</div>

</div>

<div id='grid_4' class='grid_div has_title has_button'>
	<label class='sub_title'>센터 권한 사용자 등록</label>
	<div class='child_button float_right'>
		<html:authchildbutton id='buttons' grid='dg_4' append='true' insert='false' duplicate='false' delete='true' excel='false' retrieve='false' ></html:authchildbutton>
	</div>
	<div id="dg_4" class="slick-grid"></div>
</div>




<script type="text/javascript">
	topsearch.ResizeInfo				= {init_width:1000, init_height: 35, width_increase:1, height_increase:0};

	form_left.ResizeInfo				= {init_width: 500, init_height:660, width_increase:1, height_increase:1};
	grid_1.ResizeInfo						= {init_width: 500, init_height:460, width_increase:1, height_increase:1};
	grid_3.ResizeInfo  					= {init_width: 500, init_height:200, width_increase:1, height_increase:0};

	form_free.ResizeInfo				= {init_width: 500, init_height:500, width_increase:0, height_increase:0};
	grid_4.ResizeInfo  					= {init_width: 500, init_height:155, width_increase:0, height_increase:1};
</script>


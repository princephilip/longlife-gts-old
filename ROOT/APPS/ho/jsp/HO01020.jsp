<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<jsp:include page="/Mighty/jsp/UploadExcelForm.jsp"/>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true" pgmcode="HO01020"></html:authbutton>
		</span>
	</div>

	<div class='option_label w100'>품목구분</div>
	<div class='option_input_bg'>
		<select id="S_PRODUCT" class="option_input_fix" style="width:120px"></select>
	</div>

	<div class='option_label w100'>사용여부</div>
	<div class='option_input_bg'>
		<select id="S_USEYN" class="option_input_fix" style="width:100px"></select>
	</div>

	<div class='option_label'>기준일자</div>
	<div class="option_input_bg" >
		<html:basicdate id="dateimages" classType="search" dateType="date" moveBtn="true" readOnly="true"></html:basicdate>
	</div>

	<div id='userxbtns'>
		<button type="button" id='btn_excelupload' class="btn btn-success btn-xs" onClick="uf_ExcelUpload();">EXCEL 업로드</button>
	</div>



</div>

<div id='grid_1' class='grid_div'>
	<div id="dg_1" class="slick-grid"></div>
</div>

<div id='grid_excel' class='grid_div' style="visibility:hidden">
	<div id="dg_excel" class="slick-grid"></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo			= {init_width:1000, init_height: 33, width_increase:1, height_increase:0};
	grid_1.ResizeInfo					= {init_width:1000, init_height:660, width_increase:1, height_increase:1};
	grid_excel.ResizeInfo			= {init_width: 100, init_height:  5, width_increase:0, height_increase:0};
</script>


<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<script type="text/javascript" src="/Mighty/js/md5.js"></script>

<script type="text/javaScript" src="/Mighty/3rd/bootstrap/js/bootstrap.min.js"></script>

<style>
  .wrap-btn-grids { padding-top: 2px; min-width: 100px; }
  .wrap-btn-grids .btn { margin-left: 6px; }
  .modal-lg { width: 90% !important; }
  .wr30 { width: 30% !important; }
  .wi400 { width: 400px !important; }

  /*#grid-sql { height: 400px; }*/
  select.input-sm { height: 22px !important; padding: 1px 5px !important; font-size: 12px !important; }
</style>


<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true" pgmcode="HO03010"></html:authbutton>
		</span>
	</div>

	<div class='option_label w100'>지사명</div>
	<div class='option_input_bg'>
		<select id="S_DEPT_ID" style="width:260px"></select>
		<img id="btn_finddept" src="/Theme/images/btn/btn_find.gif" class="search_find_img" onClick="uf_findDept('S_DEPT_ID')"/>
	</div>

	<div class='option_label w100'>직급</div>
	<div class='option_input_bg'>
		<select id="S_GUBUN_DIV" class="option_input_fix" style="width:120px"></select>
		<select id="S_WORK_YN" class="option_input_fix" style="width:80px"></select>
	</div>

	<div class='option_label w100'>사번/성명</div>
	<div class='option_input_bg'>
		<input id='S_FIND' type='text' class='option_input_fix' style="width:100px">
	</div>

	<div id='userxbtns'>
		<button type="button" id="btn-change-id" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#changeModal2">사번변경</button>
	</div>


</div>

<div id='form_left' class='grid_div'>
	<div id='grid_1' class='grid_div mr5'>
		<div id="dg_1" class="slick-grid"></div>
	</div>


	<div id='tabs_1' class='tabs_div mt5'>
		<div id="ctab_nav" class="tabs_nav mr5">
		  <ul>
		    <li><a href="#ctab_1">요양보호사이력</a></li>
		    <li><a href="#ctab_2">배상보험이력</a></li>
		  </ul>
		</div>

		<!-- 요양보호사이력 -->
		<div id='ctab_1' class='tabc_div'>
	    <div id='grid_3' class='grid_div mr5'>
	      <div id="dg_3" class='slick-grid'></div>
	    </div>
		</div>

		<!-- 배상보험이력 -->
		<div id='ctab_2' class='tabc_div'>
	 		<div id='grid_4' class='grid_div mr5'>
				<div id="dg_4" class='slick-grid'></div>
			</div>
		</div>
	</div>
</div>



<!-- 사진 -->
<div id='form_photo' class='detail_box ver2'>
	<div class='grid_div' style='border: 1px solid #b4d5e8;'>
		<img id="EMP_PHOTO" src='<c:url value="/Theme/images/noimage.jpg"/>' onclick="pf_ImageUpload(this)" align="middle;" style="border:0; width:95px; height:107px; cursor:pointer;">
	</div>
</div>

<div id='form_free1' class='detail_box ver2'>
	<div class='detail_row'>
		<label class='detail_label' style="width:80px">*사번</label>
		<div class='detail_input_bg' style="width:120px">
			<input type='text' id='MEMBER_ID' class='detail_input_fix'>
		</div>

		<label class='detail_label' style="width:80px">*사원명</label>
		<div class='detail_input_bg' style="width:120px">
			<input type='text' id='NAME' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:80px">전화번호</label>
		<div class='detail_input_bg' style="width:120px">
			<input type='text' id='PHONE' class='detail_input_fix'>
		</div>

		<label class='detail_label' style="width:80px">핸드폰</label>
		<div class='detail_input_bg' style="width:120px">
			<input type='text' id='HP' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:80px">정임직구분</label>
		<div class='detail_input_bg' style="width:120px">
			<select id="MEMBER_DIV" class='detail_input_fix'></select>
		</div>

		<label class='detail_label' style="width:80px">직급</label>
		<div class='detail_input_bg' style="width:120px">
			<select id="GUBUN_DIV" class='detail_input_fix'></select>
		</div>
	</div>

	<div class='detail_row detail_row_bb'>
		<label class='detail_label' style="width:80px">지사</label>
		<div class="detail_input_bg has_button" style="width:100px">
      <input type="text" id="DEPT_ID" class='detail_input_fix'>
      <button id="btn_deptfind" class="search_find_img" onClick="uf_findDept('DEPT_ID')"></button>
		</div>
		<div class='detail_input_bg noborder' style="width:220px">
			<input type='text' id='DEPT_NAME' class='detail_input_fix' readonly>
		</div>
	</div>
</div>

<div id='form_free2' class='detail_box ver2'>
	<div class='detail_row'>
		<label class='detail_label' style="width:100px">*주민번호</label>
		<div class='detail_input_bg' style="width:108px">
			<input type='text' id='INPUT_RRN_NO' class='detail_input_fix'>
		</div>
    <div class="detail_input_bg noborder" style="width:42px">
			<button type="button" id='btn_jumin' class="btn btn-success btn-xs" onClick="uf_chk_jumin();">중복</button>
    </div>

		<label class='detail_label' style="width:100px">*비밀번호</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='password' id='INPUT_PWD' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">MENU구분</label>
		<div class='detail_input_bg' style="width:150px">
			<select id="PAGE_TAG" class='detail_input_fix'></select>
		</div>

		<label class='detail_label' style="width:100px">로그인LOCK</label>
		<div class='detail_input_bg' style="width:150px;padding-left:10px;">
			<input id='LOGIN_LOCK_YESNO' type='checkbox' >
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">복수근무지사</label>
		<div class='detail_input_bg' style="width:400px">
			<input type='text' id='DUP_DEPT_NAME' class='detail_input_fix' readonly>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">자격등급</label>
		<div class='detail_input_bg' style="width:100px">
			<select id="LICENSELEVEL_DIV" class='detail_input_fix'></select>
		</div>

		<label class='detail_label' style="width:100px">자격번호</label>
		<div class='detail_input_bg' style="width:200px">
			<input type='text' id='LICENSE_NO' class='detail_input_fix'>
		</div>
	</div>


	<div class='detail_row'>
		<label class='detail_label' style="width:100px">이메일</label>
		<div class='detail_input_bg' style="width:400px">
			<input type='text' id='EMAIL' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">업무메일</label>
		<div class='detail_input_bg' style="width:400px">
			<input type='text' id='EMAIL2' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">우편번호</label>
		<div class="detail_input_bg has_button" style="width:100px">
      <input type="text" id="ZIP_CODE">
      <button id="btn_zipfind" class="search_find_img" onClick="uf_ZipFind('ZIP_CODE')"></button>
		</div>
    <div class="detail_input_bg noborder" style="width:300px">
      <input type="text" id="ADDR1">
    </div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">주소</label>
		<div class='detail_input_bg' style="width:400px">
			<input type='text' id='ADDR2' class='detail_input_fix'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">급여계좌정보</label>
		<div class='detail_input_bg' style="width:100px">
			<select id="BANK_DIV" class='detail_input_fix'></select>
		</div>
    <div class="detail_input_bg noborder" style="width:220px">
			<input type='text' id='DEPOSITNO' class='detail_input_fix'>
    </div>
    <div class="detail_input_bg noborder" style="width:80px">
			<input type='text' id='DEPOSITOWNER' class='detail_input_fix'>
    </div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">입사일</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='IN_DATE' class='detail_date'>
		</div>

		<label class='detail_label' style="width:100px">퇴사일</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='RETIRE_DATE' class='detail_date'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">재직여부</label>
		<div class='detail_input_bg' style="width:150px;padding-left:10px;">
			<input id='WORK_YN' type='checkbox' >
		</div>

		<label class='detail_label' style="width:100px">등록일</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='REG_DATE' class='detail_date'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">프로그램권한</label>
		<div class='detail_input_bg' style="width:150px">
			<select id="AUTHORITY_DIV" class='detail_input_fix'></select>
		</div>

		<label class='detail_label' style="width:100px">회계관리자</label>
		<div class='detail_input_bg' style="width:150px;padding-left:10px;">
			<input id='AMMASTER_YN' type='checkbox' >
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">연차일수</label>
		<div class='detail_input_bg' style="width:130px">
			<input type='text' id='YEAR_VACATION' class='detail_amt'>
		</div>
		<div class='detail_input_bg noborder' style="width:20px;text-align:center">일</div>

		<label class='detail_label' style="width:100px">연차사용일수</label>
		<div class='detail_input_bg' style="width:130px;">
			<input type='text' id='USED_VACATION' class='detail_amt' readonly>
		</div>
		<div class='detail_input_bg noborder' style="width:20px;text-align:center">일</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">기본시급</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='HOURPAY' class='detail_amt'>
		</div>

		<label class='detail_label' style="width:100px">급여종류</label>
		<div class='detail_input_bg' style="width:150px;">
			<select id="PAYTYPE" class='detail_input_fix'></select>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:350px">최초 5대보험 신고된 기본급</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='REPORTEDINCOMEAMT' class='detail_amt'>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">장애유무</label>
		<div class='detail_input_bg' style="width:150px;padding-left:10px;">
			<input id='HANDICAP_YN' type='checkbox' >
		</div>

		<label class='detail_label' style="width:100px">퇴직정산방법</label>
		<div class='detail_input_bg' style="width:150px;">
			<select id="RETIRECALCTYPE" class='detail_input_fix'></select>
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">공제인원수</label>
		<div class='detail_input_bg' style="width:130px">
			<input type='text' id='DEDUCT_CNT' class='detail_amt'>
		</div>
		<div class='detail_input_bg noborder' style="width:20px;text-align:center">명</div>


		<label class='detail_label' style="width:100px"></label>
		<div class='detail_input_bg' style="width:150px;">
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">배상보험가입일</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='LIINSURANCEDATE' class='detail_date'>
		</div>

		<label class='detail_label' style="width:100px">보험가입구분</label>
		<div class='detail_input_bg' style="width:108px;">
			<select id="LIINSURANCETYPE" class='detail_input_fix'></select>
		</div>
    <div class="detail_input_bg noborder" style="width:42px">
			<button type="button" id='btn_end' class="btn btn-success btn-xs" onClick="uf_insurend();">만료</button>
    </div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">배상보험종료일</label>
		<div class='detail_input_bg' style="width:150px">
			<input type='text' id='LIINSURANCEENDDATE' class='detail_date'>
		</div>

		<label class='detail_label' style="width:100px">가족케어여부</label>
		<div class='detail_input_bg' style="width:150px;padding-left:10px;">
			<input id='FAMILY_YN' type='checkbox' >
		</div>
	</div>

	<div class='detail_row'>
		<label class='detail_label' style="width:100px">건강보험적용</label>
		<div class='detail_input_bg' style="width:150px">
			<select id="HEALTHINSURETAG" class='detail_input_fix'></select>
		</div>

		<label class='detail_label' style="width:100px">국민연금적용</label>
		<div class='detail_input_bg' style="width:150px;">
			<select id="NATIONALPENSIONTAG" class='detail_input_fix'></select>
		</div>
	</div>

	<div class='detail_row detail_row_bb'>
		<label class='detail_label' style="width:100px">고용보험적용</label>
		<div class='detail_input_bg' style="width:150px">
			<select id="GOYONGTAG" class='detail_input_fix'></select>
		</div>

		<label class='detail_label' style="width:100px"></label>
		<div class='detail_input_bg' style="width:150px;">
		</div>
	</div>

</div>


</div> <!-- #pageLayout 밖에 Modal 놓기 위한 Trick -->

<div class="modal fade" id="changeModal2" tabindex="-1" role="dialog" aria-labelledby="changeModalLabel">
  <div class="modal-dialog modal-lg wi400" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="freeformModalLabel">사번 변경</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <label class='option_label w140'>현재사번</label>
          <div class='option_input_bg'>
          	<input type='text' id='OLD_MEMBER_ID' class='detail_input_fix' readonly>
          </div>
        </div><!-- .row -->
        <div class="row">
          <label class='option_label w140'>변경사번</label>
          <div class='option_input_bg'>
          	<input type='text' id='CHG_MEMBER_ID' class='detail_input_fix'>
          </div>
        </div><!-- .row -->
      </div><!-- .modal-body -->
      <div class="modal-footer">
        <div class="text-center">
          <button type="button" id="btn-changeModal2" class="btn btn-danger">변경</button>
          <button type="button" id="btn-changeClose2" class="btn btn-default" data-dismiss="modal">취소</button>
        </div>
      </div>
    </div>
  </div>
</div>



<script type="text/javascript">
	topsearch.ResizeInfo				= {init_width:1000, init_height: 35, width_increase:1, height_increase:0};

	form_left.ResizeInfo				= {init_width: 500, init_height:660, width_increase:1, height_increase:1};
	grid_1.ResizeInfo						= {init_width: 500, init_height:360, width_increase:1, height_increase:1};

	tabs_1.ResizeInfo						= {init_width: 500, init_height:300, width_increase:1, height_increase:0};
		ctab_nav.ResizeInfo  			= {init_width: 500, init_height: 25, width_increase:1, height_increase:0};
			ctab_1.ResizeInfo  			= {init_width: 500, init_height:275, width_increase:1, height_increase:1};
				grid_3.ResizeInfo  		= {init_width: 500, init_height:275, width_increase:1, height_increase:1};
			ctab_2.ResizeInfo  			= {init_width: 500, init_height:275, width_increase:1, height_increase:1};
				grid_4.ResizeInfo    	= {init_width: 500, init_height:275, width_increase:1, height_increase:1};

	form_photo.ResizeInfo				= {init_width: 100, init_height:113, width_increase:0, height_increase:0};
	form_free1.ResizeInfo				= {init_width: 400, init_height:113, width_increase:0, height_increase:0};
	form_free2.ResizeInfo				= {init_width: 500, init_height:530, width_increase:0, height_increase:1};
</script>


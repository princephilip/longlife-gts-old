<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true" pgmcode="HO02010"></html:authbutton>
		</span>
	</div>

	<div class='option_label w100'>지사명</div>
	<div class='option_input_bg'>
		<select id="S_DEPT_ID" style="width:260px"></select>
		<img id="btn_finddept" src="/Theme/images/btn/btn_find.gif" class="search_find_img" onClick="uf_findDept()"/>
	</div>

	<div class='option_input_bg' style="float:left;background-color: #f8f8f8;border-width: 0px 0px 0px 0px;margin-left:10px">
		<INPUT onclick=xe_EditChanged(rb_emp,this.value) id=rb_emp_1 type=radio value='1' name=rb_emp CHECKED>
		<A href="javascript:if(!rb_emp_1.disabled){rb_emp_1.checked=true;xe_EditChanged(rb_emp,rb_emp_1.value);}" target=_self>사번없슴</A>
		<INPUT onclick=xe_EditChanged(rb_emp,this.value) id=rb_emp_2 type=radio value='2' name=rb_emp>
		<A href="javascript:if(!rb_emp_2.disabled){rb_emp_2.checked=true;xe_EditChanged(rb_emp,rb_emp_2.value);}" target=_self>사번있음</A>
		<INPUT onclick=xe_EditChanged(rb_emp,this.value) id=rb_emp_3 type=radio value='3' name=rb_emp>
		<A href="javascript:if(!rb_emp_3.disabled){rb_emp_3.checked=true;xe_EditChanged(rb_emp,rb_emp_3.value);}" target=_self>전체</A>
	</div>


	<div class='option_line'></div>

	<div class='option_label w100'>직급</div>
	<div class='option_input_bg'>
		<select id="S_GUBUN_DIV" class="option_input_fix" style="width:120px"></select>
		<select id="S_WORK_YN" class="option_input_fix" style="width:80px"></select>
	</div>

	<div class='option_label'>기준일자</div>
	<div class="option_input_bg" >
		<html:fromtodate id="dateimages"  classType="option" dateType="date" moveBtn="true" readOnly="false"></html:fromtodate>
	</div>

	<div class="option_label">
		<A href="javascript:if(!CHK_ALLDATE.disabled){CHK_ALLDATE.checked=!CHK_ALLDATE.checked;xe_EditChanged(CHK_ALLDATE,CHK_ALLDATE.value)}" target=_self><INPUT onclick=xe_EditChanged(this,this.value) id=CHK_ALLDATE type=checkbox value="" name=CHK_ALLDATE checked=true>전체기간</A>
	</div>

	<div class='option_label w100'>고객</div>
	<div class='option_input_bg'>
		<input id='S_CUST_NAME' type='text' class='option_input_fix'>
	</div>



</div>

<div id='grid_1' class='grid_div'>
	<div id="dg_1" class="slick-grid"></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo			= {init_width:1000, init_height: 60, width_increase:1, height_increase:0};
	grid_1.ResizeInfo					= {init_width:1000, init_height:630, width_increase:1, height_increase:1};
</script>


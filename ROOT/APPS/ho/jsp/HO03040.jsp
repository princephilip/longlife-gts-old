<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>
<script type="text/javascript" src="/Mighty/js/md5.js"></script>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_2' gridCall="true" pgmcode="HO03040"></html:authbutton>
		</span>
	</div>

	<div class='option_label w100'>지사명</div>
	<div class='option_input_bg'>
		<select id="S_DEPT_ID" style="width:260px"></select>
		<img id="btn_finddept" src="/Theme/images/btn/btn_find.gif" class="search_find_img" onClick="uf_findDept('S_DEPT_ID')"/>
	</div>

	<div class='option_label w100'>재직구분</div>
	<div class='option_input_bg'>
		<select id="S_WORK_YN" class="option_input_fix" style="width:80px"></select>
	</div>

	<div class='option_label w100'>사번/성명</div>
	<div class='option_input_bg'>
		<input id='S_FIND' type='text' class='option_input_fix noborder' style="width:100px">
	</div>

</div>

<div id='grid_1' class='grid_div mr5'>
	<div id="dg_1" class="slick-grid"></div>
</div>

<div id='grid_2' class='grid_div mr5'>
	<div id="dg_2" class="slick-grid"></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo				= {init_width:1000, init_height: 35, width_increase:1, height_increase:0};
	grid_1.ResizeInfo						= {init_width: 670, init_height:660, width_increase:0, height_increase:1};
	grid_2.ResizeInfo						= {init_width: 330, init_height:660, width_increase:1, height_increase:1};
</script>


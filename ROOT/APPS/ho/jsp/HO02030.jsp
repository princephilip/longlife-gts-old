<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="html" uri="/WEB-INF/tlds/mighty.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/Mighty/template/js/XG_GRID1_template.js"></script>

<div id='topsearch' class='option_box2' >
	<div id='xbtns'>
		<span style="width:100%;height:100%;vertical-align:middle;">
			<html:authbutton id='buttons' grid='dg_1' gridCall="true" pgmcode="HO02030"></html:authbutton>
		</span>
	</div>

	<div class='option_label w100'>지사명</div>
	<div class='option_input_bg'>
		<select id="S_DEPT_ID" style="width:260px"></select>
		<img id="btn_finddept" src="/Theme/images/btn/btn_find.gif" class="search_find_img" onClick="uf_findDept()"/>
	</div>

	<div class='option_label w100'>시급구분</div>
	<div class='option_input_bg'>
		<select id="S_PAY_DIV" class="option_input_fix" style="width:100px"></select>
	</div>

	<div class='option_label w100'>시급조정</div>
	<div class='option_input_bg'>
		<input id='S_HOURPAY' type='text' class='option_input_fix' style="width:80px">
	</div>

	<div class="option_input_bg" style="background-color: #f8f8f8;border-width: 0px 0px 0px 0px;color:#666633;">
		<span> 원 == [조회] ==> </span>
	</div>

	<div class='option_input_bg'>
		<input id='S_NEWPAY' type='text' class='option_input_fix' style="width:80px">
	</div>

	<div class="option_input_bg" style="background-color: #f8f8f8;border-width: 0px 0px 0px 0px;color:#666633;">
		<span> 원 </span>
	</div>

	<div class="option_input_bg" style="background-color: #f8f8f8;border-width: 0px 0px 0px 0px;color:#666633;">
		<button type="button" id='btn_apply'  class="btn btn-success btn-xs" onClick="uf_change_pay();">일괄적용</button>
	</div>

	<div class="option_input_bg" style="background-color: #f8f8f8;border-width: 0px 0px 0px 0px;color:#666633;">
		<span> ===> [저장]</span>
	</div>


</div>

<div id='grid_1' class='grid_div'>
	<div id="dg_1" class="slick-grid"></div>
</div>

<script type="text/javascript">
	topsearch.ResizeInfo			= {init_width:1000, init_height: 33, width_increase:1, height_increase:0};
	grid_1.ResizeInfo					= {init_width:1000, init_height:660, width_increase:1, height_increase:1};
</script>


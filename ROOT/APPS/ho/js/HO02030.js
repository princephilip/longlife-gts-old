//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : [HO02030] 요보사시급 일괄변경
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
function x_InitForm2(){
	if(!_X.IsHeadoffice())	{
		$("#btn_finddept").hide();
	}

	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "ho", "HO02030", "HO02030|HO02030_U01", false, true);

	var la_DEPT = _X.XmlSelect("com", "COMMON", "CODE_DEPT_AUTHORITY_R01" , new Array(mytop._UserID), 'json2');
	_X.DDLB_SetData(S_DEPT_ID, la_DEPT, null, true, (_X.IsHeadoffice() ? true : false));
	S_DEPT_ID.value = mytop._DeptCode;
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		//시급구분
		var ls_PAYDIV = [{"code":"1","label":"시급(방문)"},{"code":"2","label":"시급(가족)"},{"code":"3","label":"시급(목욕)"}];
		_X.DDLB_SetData(S_PAY_DIV, ls_PAYDIV, null, false, true);

		//직급
		var ls_GUBUN_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0103", 'Y'), 'json2');
		dg_1.SetCombo("GUBUN_DIV", ls_GUBUN_DIV);

		// 장애등급
		var ls_HANDICAP_LEVEL = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0145", 'Y'), 'json2');
		dg_1.SetCombo("HANDICAP_LEVEL", ls_HANDICAP_LEVEL);

		// 장애등급
		var ls_HANDICAP_LEVEL = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0145", 'Y'), 'json2');
		dg_1.SetCombo("HANDICAP_LEVEL", ls_HANDICAP_LEVEL);

		// 재직여부
		var ls_WORKYN = [{"code":"Y","label":"재직"},{"code":"N","label":"퇴직"}];
		dg_1.SetCombo("WORK_YN", ls_WORKYN);

		//setTimeout("x_DAO_Retrieve()",100);
	}
}

function x_DAO_Retrieve2(a_dg){

	var li_amt = S_HOURPAY.value;
	li_amt = li_amt.replace(/\,/gi, '');
	
	if(S_PAY_DIV.value != "%") {
		if( isNaN(li_amt) ) {
		 	_X.MsgBox('시급은 숫자만 입력해야 합니다.');
			return 0;
		}
	}
	
	if(li_amt=="") li_amt = 999999999;
	
	dg_1.Retrieve([S_DEPT_ID.value, S_PAY_DIV.value, li_amt]);
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id) {
		case	"S_DEPT_ID":
		case	"S_PAY_DIV":
		case	"S_HOURPAY":
					x_DAO_Retrieve();
					break;
	}

}


function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

//저장후 호출
function x_DAO_Saved2(){
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
}


function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
	if(_FindCode.returnValue == null) return;

	switch(_FindCode.finddiv) {
		case	"S_DEPT_ID" :
					S_DEPT_ID.value = _FindCode.returnValue.DEPT_ID;
					x_DAO_Retrieve();
					break;
	}
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

// 지사찾기
function uf_findDept() {
	_X.FindDeptCode2('S_DEPT_ID','');
}

function uf_change_pay(){
	var li_amt2 = S_NEWPAY.value;
	li_amt2 = li_amt2.replace(/\,/gi, '');
	
	if(S_PAY_DIV.value == "%") {
		_X.MsgBox('[전체]를 조회한 상태에서 변경 시급을 적용할 수 없습니다.');
		return;
	}
	
	if( isNaN(li_amt2) ) {
	 	_X.MsgBox('변경 시급은 숫자만 입력해야 합니다.');
		return;
	}
	
	for(var i=1;i<=dg_1.RowCount();i++) {
		if(dg_1.GetItem(i, "SEL")=="Y") {
			switch(S_PAY_DIV.value) {
				case "1" :
					dg_1.SetItem(i, "HOURPAY", Number(li_amt2));
					break;
				case "2" :
					dg_1.SetItem(i, "HOURPAY_2", Number(li_amt2));
					break;
				case "3" :
					dg_1.SetItem(i, "HOURPAY_3", Number(li_amt2));
					break;
			}
		}
	}

}


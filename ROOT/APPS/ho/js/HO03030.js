//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : [HO03030] 센터데이터복사
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
function x_InitForm2(){
	if(!_X.IsHeadoffice())	{
		$("#btn_finddept").hide();
		$("#btn_finddept2").hide();
	}

	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "ho", "HO03030", "HO03030|HO03030_R01", false, false);
	_X.InitGrid(grid_2, "dg_2", "100%", "100%", "ho", "HO03030", "HO03030|HO03030_R01", false, false);
	_X.InitGrid(grid_3, "dg_3", "100%", "100%", "ho", "HO03030", "HO03030|HO03030_R03", false, false);
	_X.InitGrid(grid_4, "dg_4", "100%", "100%", "ho", "HO03030", "HO03030|HO03030_R03", false, false);

	var la_DEPT = _X.XmlSelect("com", "COMMON", "CODE_DEPT_AUTHORITY_R01" , new Array(mytop._UserID), 'json2');
	_X.DDLB_SetData(S_DEPT_ID, la_DEPT, null, true, false);
	_X.DDLB_SetData(S_DEPT_ID2, la_DEPT, null, true, false);
	S_DEPT_ID.value = mytop._DeptCode;
	S_DEPT_ID2.value = mytop._DeptCode;
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		//직급
		var ls_GUBUN_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0103", 'Y'), 'json2');
		dg_1.SetCombo("GUBUN_DIV", ls_GUBUN_DIV);
		dg_2.SetCombo("GUBUN_DIV", ls_GUBUN_DIV);

		// 장애등급
		var ls_HANDICAP_LEVEL = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0145", 'Y'), 'json2');
		dg_1.SetCombo("HANDICAP_LEVEL", ls_HANDICAP_LEVEL);
		dg_2.SetCombo("HANDICAP_LEVEL", ls_HANDICAP_LEVEL);

		dg_3.SetAutoFooterValue("CUST_NAME", function(idx, ele) {
			return '전체 : ' + dg_3.RowCount().toString() + ' 명';
		});

		dg_4.SetAutoFooterValue("CUST_NAME", function(idx, ele) {
			return '전체 : ' + dg_4.RowCount().toString() + ' 명';
		});

		dg_3.SetAutoFooterValue("DDAY_REMK", function(idx, ele) {
			var datas = dg_3.GetData();
			var li_tot = dg_3.RowCount();
			var li_cnt = 0
			var i, ii;
			for (i = 0, ii = datas.length; i < ii; i++ ) {
				if(datas[i].CARE_EDATE_DDAY > 0) li_cnt++;
			}
			return "기간내 : " + li_cnt.toString() + "명,   만료 : " + li_tot.toString() + "명";
		});

		dg_4.SetAutoFooterValue("DDAY_REMK", function(idx, ele) {
			var datas = dg_4.GetData();
			var li_tot = dg_4.RowCount();
			var li_cnt = 0
			var i, ii;
			for (i = 0, ii = datas.length; i < ii; i++ ) {
				if(datas[i].CARE_EDATE_DDAY > 0) li_cnt++;
			}
			return "기간내 : " + li_cnt.toString() + "명,   만료 : " + li_tot.toString() + "명";
		});

		setTimeout("uf_retrieve_src()",100);
	}
}

function x_DAO_Retrieve2(a_dg){
	uf_retrieve_src();
	uf_retrieve_tgt();
}

function uf_retrieve_src() {
	if(!S_DEPT_ID.value) {
		_X.MsgBox("확인", "기존 지사를 선택하세요.");
		return;
	}
	dg_1.Retrieve([S_DEPT_ID.value, "%", "Y"]);
	dg_3.Retrieve(['%', S_DEPT_ID.value, 'Y', '', '', '0']);
}

function uf_retrieve_tgt() {
	if(!S_DEPT_ID2.value) {
		_X.MsgBox("확인", "새로운 지사를 선택하세요.");
		return;
	}
	dg_2.Retrieve([S_DEPT_ID2.value, "%", "Y"]);	
	dg_4.Retrieve(['%', S_DEPT_ID2.value, 'Y', '', '', '0']);
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id) {
		case	"S_DEPT_ID":
					uf_retrieve_src();
					break;
		case	"S_DEPT_ID2":
					uf_retrieve_tgt();
					break;
	}
}

function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

//저장후 호출
function x_DAO_Saved2(){
}

function x_DAO_Insert2(a_dg, row){
	return 100;
}


function x_Insert_After2(a_dg, rowIdx){
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;	// 템플릿의 하위 로직을 수행하지않고 Pass
	//return 1;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	xe_BodyResize();
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
	if(a_dg.id == 'dg_1' || a_dg.id == 'dg_2') {
		uf_Set_Color_Member(a_dg);
	}

	if(a_dg.id == 'dg_3' || a_dg.id == 'dg_4') {
		uf_Set_Color_Cust(a_dg);
	}
}


function uf_Set_Color_Member(a_dg) {
	var s_blu = { "color": "#0000FF" };
	var s_blk = { "color": "#000000" };

	for(var i = 1; i <= a_dg.RowCount(); i++){
		var ls_dept = a_dg.GetItem(i, "DUP_DEPT_NAME");
		a_dg.SetCellStyle(i, "NAME"    , ( ls_dept == "" ) ? s_blk : s_blu);
	}
}

function uf_Set_Color_Cust(a_dg) {
	var s_red = { "color": "#FF0000" };
	var s_blk = { "color": "#000000" };
	var s_gry = { "color": "#C8C8C8" };

	for(var i = 1; i <= a_dg.RowCount(); i++){
		var ll_dday = _X.ToInt(a_dg.GetItem(i, "CARE_EDATE_DDAY"));
		var li_ord  = _X.ToInt(a_dg.GetItem(i, "SORT_ORDER"));
		a_dg.SetCellStyle(i, "CUST_NAME"    , ( ll_dday <= 90 && ll_dday > 0 )  ? s_red : s_blk);

		a_dg.SetCellStyle(i, "BIRTH_DATE"   , ( li_ord==0 && ll_dday <= 90 && ll_dday>0) ? s_red : ( li_ord==0 && ll_dday > 90 ) ? s_blk : s_gry);
		a_dg.SetCellStyle(i, "DDAY_REMK", ( li_ord==0 && ll_dday <= 90 && ll_dday>0) ? s_red : ( li_ord==0 && ll_dday > 90 ) ? s_blk : s_gry);
	}
}


function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
	switch(_FindCode.finddiv) {
		case	"S_DEPT_ID" :
					S_DEPT_ID.value = _FindCode.returnValue.DEPT_ID;
					x_DAO_Retrieve(dg_1);
					break;
		case	"S_DEPT_ID2" :
					S_DEPT_ID2.value = _FindCode.returnValue.DEPT_ID;
					x_DAO_Retrieve(dg_2);
					break;
	}
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

// 지사찾기
function uf_findDept(a_param) {
	_X.FindDeptCode(a_param,'');
}


function uf_copy_member() {
	// 기존지사 요보사 숫자 세기 (요보사 GUBUN_DIV : 0205)
	var ls_dept_src = S_DEPT_ID.value;
	var ls_dept_src_n = S_DEPT_ID.options[S_DEPT_ID.selectedIndex].text;

	var ls_dept_tgt = S_DEPT_ID2.value;
	var ls_dept_tgt_n = S_DEPT_ID2.options[S_DEPT_ID2.selectedIndex].text;
	
	if(!ls_dept_src||!ls_dept_tgt) {
		_X.MsgBox("확인", "기존 지사명 또는 새로운 지사명을 선택하세요.");
		return;
	}
	
	if(ls_dept_src==ls_dept_tgt) {
		_X.MsgBox("확인", "기존 지사명과 새로운 지사명이 일치합니다.");
		return;
	}
	
	var ls_result = _X.XmlSelect("ho", "HO03030", "MEMBER_CNT_R01", [ls_dept_src, ls_dept_tgt], "array")[0];
	
	if(_X.MsgBoxYesNo("확인", "요보사를 이동하시겠습니까?\n이름과 생년월일이 다른 요보사를 이동합니다.\n다시 되돌릴 수 없고, 요양사를 수동으로 삭제하셔야 합니다.\n\n기존지사 : "+ls_dept_src_n+" ( "+ls_result[0]+"명 )\n새로운지사 : "+ls_dept_tgt_n+" ( "+ls_result[1]+"명 )") == 1){

		var ll_rtn = _X.ES("ho", "HO03030", "MEMBER_MOVE", [ls_dept_src, ls_dept_tgt]);
		if ( ll_rtn >= 0){
			_X.MsgBox("확인", "요보사를 이동했습니다.");
			uf_retrieve_tgt();
		}
	}
}

function uf_copy_cust() {
	var ls_dept_src = S_DEPT_ID.value;
	var ls_dept_src_n = S_DEPT_ID.options[S_DEPT_ID.selectedIndex].text;
	var ls_dept_tgt = S_DEPT_ID2.value;
	var ls_dept_tgt_n = S_DEPT_ID2.options[S_DEPT_ID2.selectedIndex].text;
	
	if(!ls_dept_src||!ls_dept_tgt) {
		_X.MsgBox("확인", "기존 지사명 또는 새로운 지사명을 선택하세요.");
		return;
	}
	
	if(ls_dept_src==ls_dept_tgt) {
		_X.MsgBox("확인", "기존 지사명과 새로운 지사명이 일치합니다.");
		return;
	}
	
	var ls_result = _X.XmlSelect("ho", "HO03030", "CUST_CNT_R01", [ls_dept_src, ls_dept_tgt], "array")[0];

	if(_X.MsgBoxYesNo("확인", "대상자를 이동하시겠습니까?\n다시 되돌릴 수 없고, 대상자를 수동으로 삭제하셔야 합니다.\n\n기존지사 : "+ls_dept_src_n+" ( "+ls_result[0]+"명 )\n새로운지사 : "+ls_dept_tgt_n+" ( "+ls_result[1]+"명 )") == 1){

		var ll_rtn = _X.ES("ho", "HO03030", "CUST_MOVE", [ls_dept_src, ls_dept_tgt]);
		if ( ll_rtn >= 0){
			_X.MsgBox("확인", "대상자를 이동했습니다.");
			uf_retrieve_tgt();
		}
	}
}

//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : [HO01010] 오류목록체크
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
function x_InitForm2(){
	if(!_X.IsHeadoffice())	{
		$("#btn_finddept").hide();
	}

	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "ho", "HO01010", "HO01010|HO01010_R01", false, false);
	_X.InitGrid(grid_2, "dg_2", "100%", "100%", "ho", "HO01010", "HO01010|HO01010_R02", false, false);

	var la_DEPT = _X.XmlSelect("com", "COMMON", "CODE_DEPT_AUTHORITY_R01" , new Array(mytop._UserID), 'json2');
	_X.DDLB_SetData(S_DEPT_ID, la_DEPT, null, true, (_X.IsHeadoffice() ? true : false));
	S_DEPT_ID.value = mytop._DeptCode;

}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		setTimeout("x_DAO_Retrieve()",100);
	}
}

function x_DAO_Retrieve2(a_dg){
  dg_2.Reset();
 	dg_1.Retrieve([_X.StrPurify(sle_basicmonth.value), S_DEPT_ID.value]);
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id) {
		case	"S_DEPT_ID":
		case	"sle_basicmonth":
					x_DAO_Retrieve();
					break;
	}
}


function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

//저장후 호출
function x_DAO_Saved2(){
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
	if(a_dg.id == 'dg_1') {
	 	dg_2.Retrieve([dg_1.GetItem(a_newrow, 'DEPT_ID'), dg_1.GetItem(a_newrow, 'MEMBER_ID'), dg_1.GetItem(a_newrow, 'CUST_ID'), _X.StrPurify(sle_basicmonth.value)]);
	}
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
	uf_Set_Color(a_dg);	
}

function uf_Set_Color(a_dg){
	var s_red = { "color": "#FF0000" };
	var s_blk = { "color": "#000000" };

	if(a_dg.id == 'dg_2') {
		for(var i = 1; i <= dg_2.RowCount(); i++){
			var ls_plan = dg_2.GetItem(i, "PLAN_DATE");
			var ls_sdt1 = dg_2.GetItem(i, "CONT_SDATE");
			var ls_edt1 = dg_2.GetItem(i, "CONT_EDATE");
			var ls_sdt2 = dg_2.GetItem(i, "CARE_SDATE");
			var ls_edt2 = dg_2.GetItem(i, "CARE_EDATE");
			var ls_cncl = dg_2.GetItem(i, "CANCEL_DATE");

			dg_2.SetCellStyle(i, "CONT_SDATE", ( ls_plan >= ls_sdt1 && ls_plan <= ls_edt1 ) ? s_blk : s_red);
			dg_2.SetCellStyle(i, "CONT_EDATE", ( ls_plan >= ls_sdt1 && ls_plan <= ls_edt1 ) ? s_blk : s_red);
			dg_2.SetCellStyle(i, "CARE_SDATE", ( ls_plan >= ls_sdt2 && ls_plan <= ls_edt2 ) ? s_blk : s_red);
			dg_2.SetCellStyle(i, "CARE_EDATE", ( ls_plan >= ls_sdt2 && ls_plan <= ls_edt2 ) ? s_blk : s_red);
			dg_2.SetCellStyle(i, "CANCEL_DATE", ( ls_plan > ls_cncl ) ? s_red : s_blk);
		}
	}



}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
	if(_FindCode.returnValue == null) return;

	switch(_FindCode.finddiv) {
		case	"S_DEPT_ID" :
					S_DEPT_ID.value = _FindCode.returnValue.DEPT_ID;
					x_DAO_Retrieve();
					break;
	}
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

// 지사찾기
function uf_findDept() {
	_X.FindDeptCode2('S_DEPT_ID','');
}

// 월별일정변경
function uf_month_change() {
	if(dg_1.RowCount() > 0) {
		var ls_custid    = dg_1.GetItem(dg_1.GetRow(), "CUST_ID");
		var ls_custname  = dg_1.GetItem(dg_1.GetRow(), "CUST_NAME");
		var ls_deptid    = dg_1.GetItem(dg_1.GetRow(), "DEPT_ID");
		var ls_dept_name = dg_1.GetItem(dg_1.GetRow(), "DEPT_NAME");

		if(ls_dept_name.indexOf("주야간")>0) {
			mytop._X.MDI_Open('GS01050', mytop._MyPgmList['GS01050'].PGM_NAME, '/Mighty/template/XG_Slick1.jsp?pcd=GS01050&params='+ls_custid+'@'+ls_custname+'@'+ls_deptid);
		} else {
			mytop._X.MDI_Open('GS01040', mytop._MyPgmList['GS01040'].PGM_NAME, '/Mighty/template/XG_Slick1.jsp?pcd=GS01040&params='+ls_custid+'@'+ls_custname+'@'+ls_deptid);
		}
	}
}


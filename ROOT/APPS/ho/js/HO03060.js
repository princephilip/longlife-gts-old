//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : [HO03060] 시스템마감관리
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
function x_InitForm2(){
	if(!_X.IsHeadoffice())	{
		$("#btn_finddept").hide();
	}

	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "ho", "HO03060", "HO03060|HO03060_U01", false, true);

	var la_DEPT = _X.XmlSelect("com", "COMMON", "CODE_DEPT_AUTHORITY_R01" , new Array(mytop._UserID), 'json2');
	_X.DDLB_SetData(S_DEPT_ID, la_DEPT, null, true, (_X.IsHeadoffice() ? true : false));
	S_DEPT_ID.value = '%';

}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		// 처우개선비
		dg_1.SetAutoFooterValue("DEPT_GONGCLOSEYN", function(idx, ele) {
			var datas = dg_1.GetData();
			var li_y   = 0;
			var i, ii;
			for (i = 0, ii = datas.length; i < ii; i++ ) {
				if(datas[i].DEPT_GONGCLOSEYN == "Y") li_y ++;
			}
			return li_y.toString() + '/' + dg_1.RowCount().toString();
		});

		dg_1.SetAutoFooterValue("GONGCLOSEYN", function(idx, ele) {
			var datas = dg_1.GetData();
			var li_y   = 0;
			var i, ii;
			for (i = 0, ii = datas.length; i < ii; i++ ) {
				if(datas[i].GONGCLOSEYN == "Y") li_y ++;
			}
			return li_y.toString() + '/' + dg_1.RowCount().toString();
		});

		// 재가서비스
		dg_1.SetAutoFooterValue("DEPT_SERVICECLOSEYN", function(idx, ele) {
			var datas = dg_1.GetData();
			var li_y   = 0;
			var i, ii;
			for (i = 0, ii = datas.length; i < ii; i++ ) {
				if(datas[i].DEPT_SERVICECLOSEYN == "Y") li_y ++;
			}
			return li_y.toString() + '/' + dg_1.RowCount().toString();
		});

		dg_1.SetAutoFooterValue("SERVICECLOSEYN", function(idx, ele) {
			var datas = dg_1.GetData();
			var li_y   = 0;
			var i, ii;
			for (i = 0, ii = datas.length; i < ii; i++ ) {
				if(datas[i].SERVICECLOSEYN == "Y") li_y ++;
			}
			return li_y.toString() + '/' + dg_1.RowCount().toString();
		});

		// 급여
		dg_1.SetAutoFooterValue("DEPT_PAYCLOSEYN", function(idx, ele) {
			var datas = dg_1.GetData();
			var li_y   = 0;
			var i, ii;
			for (i = 0, ii = datas.length; i < ii; i++ ) {
				if(datas[i].DEPT_PAYCLOSEYN == "Y") li_y ++;
			}
			return li_y.toString() + '/' + dg_1.RowCount().toString();
		});

		dg_1.SetAutoFooterValue("PAYCLOSEYN", function(idx, ele) {
			var datas = dg_1.GetData();
			var li_y   = 0;
			var i, ii;
			for (i = 0, ii = datas.length; i < ii; i++ ) {
				if(datas[i].PAYCLOSEYN == "Y") li_y ++;
			}
			return li_y.toString() + '/' + dg_1.RowCount().toString();
		});

		// 특별수당
		dg_1.SetAutoFooterValue("DEPT_SPECIALCLOSEYN", function(idx, ele) {
			var datas = dg_1.GetData();
			var li_y   = 0;
			var i, ii;
			for (i = 0, ii = datas.length; i < ii; i++ ) {
				if(datas[i].DEPT_SPECIALCLOSEYN == "Y") li_y ++;
			}
			return li_y.toString() + '/' + dg_1.RowCount().toString();
		});

		dg_1.SetAutoFooterValue("SPECIALCLOSEYN", function(idx, ele) {
			var datas = dg_1.GetData();
			var li_y   = 0;
			var i, ii;
			for (i = 0, ii = datas.length; i < ii; i++ ) {
				if(datas[i].SPECIALCLOSEYN == "Y") li_y ++;
			}
			return li_y.toString() + '/' + dg_1.RowCount().toString();
		});

		// 회계
		dg_1.SetAutoFooterValue("DEPT_ACNTCLOSEYN", function(idx, ele) {
			var datas = dg_1.GetData();
			var li_y   = 0;
			var i, ii;
			for (i = 0, ii = datas.length; i < ii; i++ ) {
				if(datas[i].DEPT_ACNTCLOSEYN == "Y") li_y ++;
			}
			return li_y.toString() + '/' + dg_1.RowCount().toString();
		});

		dg_1.SetAutoFooterValue("ACNTCLOSEYN", function(idx, ele) {
			var datas = dg_1.GetData();
			var li_y   = 0;
			var i, ii;
			for (i = 0, ii = datas.length; i < ii; i++ ) {
				if(datas[i].ACNTCLOSEYN == "Y") li_y ++;
			}
			return li_y.toString() + '/' + dg_1.RowCount().toString();
		});

		// 판매
		dg_1.SetAutoFooterValue("DEPT_SELLCLOSEYN", function(idx, ele) {
			var datas = dg_1.GetData();
			var li_y   = 0;
			var i, ii;
			for (i = 0, ii = datas.length; i < ii; i++ ) {
				if(datas[i].DEPT_SELLCLOSEYN == "Y") li_y ++;
			}
			return li_y.toString() + '/' + dg_1.RowCount().toString();
		});

		dg_1.SetAutoFooterValue("SELLCLOSEYN", function(idx, ele) {
			var datas = dg_1.GetData();
			var li_y   = 0;
			var i, ii;
			for (i = 0, ii = datas.length; i < ii; i++ ) {
				if(datas[i].SELLCLOSEYN == "Y") li_y ++;
			}
			return li_y.toString() + '/' + dg_1.RowCount().toString();
		});

		setTimeout("x_DAO_Retrieve()",100);
	}
}

function x_DAO_Retrieve2(a_dg){
	_X.EP("gs", "PROC_GS_INIT_CLOSING", [_X.StrPurify(sle_basicmonth.value), mytop._UserID, mytop._ClientIP]);

 	dg_1.Retrieve([_X.StrPurify(sle_basicmonth.value), S_DEPT_ID.value]);
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id) {
		case	"S_DEPT_ID":
		case	"sle_basicmonth":
					x_DAO_Retrieve();
					break;
	}
}


function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

//저장후 호출
function x_DAO_Saved2(){
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
	dg_1.SetAllFooterData();
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
	if(_FindCode.returnValue == null) return;

	switch(_FindCode.finddiv) {
		case	"S_DEPT_ID" :
					S_DEPT_ID.value = _FindCode.returnValue.DEPT_ID;
					x_DAO_Retrieve();
					break;
	}
}

function xe_GridHeaderClick2(a_dg, a_colname){
	if(a_dg.id == 'dg_1') {
		if(a_colname.indexOf('YN') >=0 && dg_1.RowCount() > 0) {
			var cval = dg_1.GetItem(1,a_colname)=="Y"?"N":"Y";
			for(var i=1; i<=dg_1.RowCount(); i++) {
				dg_1.SetItem(i,a_colname,cval);
			}
			dg_1.SetAllFooterData();
		}
	}

}

function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

// 지사찾기
function uf_findDept() {
	_X.FindDeptCode2('S_DEPT_ID','');
}

function uf_SelectAll(val) {
	var colName = new Array('ACNTCLOSEYN','SERVICECLOSEYN','PAYCLOSEYN','SELLCLOSEYN','SPECIALCLOSEYN');
	for(var i=1; i<=dg_1.RowCount(); i++) {
		for(var j=0; j<colName.length; j++)	{
			if(dg_1.GetItem(i,colName[j]) != val) {
				dg_1.SetItem(i,colName[j],val)
			}
		}
	}
	dg_1.SetAllFooterData();
}

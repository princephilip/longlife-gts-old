//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : [HO03010] 임직원관리
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
function x_InitForm2(){
	_X.Tabs(tabs_1, 0);

	if(!_X.IsHeadoffice())	{
		$("#btn_finddept").hide();
	}

	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "ho", "HO03010", "HO03010|HO03010_C01", true,  true);
	_X.InitGrid(grid_3, "dg_3", "100%", "100%", "ho", "HO03010", "HO03010|HO03010_R03", false, false);
	_X.InitGrid(grid_4, "dg_4", "100%", "100%", "ho", "HO03010", "HO03010|HO03010_R04", false, false);

	var la_DEPT = _X.XmlSelect("com", "COMMON", "CODE_DEPT_AUTHORITY_R01" , new Array(mytop._UserID), 'json2');
	_X.DDLB_SetData(S_DEPT_ID, la_DEPT, null, true, (_X.IsHeadoffice() ? true : false));
	S_DEPT_ID.value = mytop._DeptCode;

	// 사버변경
	$('#changeModal2').on('shown.bs.modal', function () {
    if(dg_1.GetRow() <= 0) {
			_X.MsgBox("변경대상 사원이 없습니다");
			$('#changeModal').modal('hide');
			return;
    }

    OLD_MEMBER_ID.value = dg_1.GetItem(dg_1.GetRow(), "MEMBER_ID");
	});

  // $("#btn-change-id").click(function() {
  //   if(dg_1.GetRow() <= 0) {
		// 	_X.Noty("변경대상 사원이 없습니다");
		// 	return false;
  //   }

  //   OLD_MEMBER_ID.value = dg_1.GetItem(dg_1.GetRow(), "MEMBER_ID");
  // });

  $("#btn-changeModal2").click(function() {
    if($("#CHG_MEMBER_ID").val()=="" || $("#CHG_MEMBER_ID").val()==null) {
      _X.Noty("변경할 사번을 입력 하세요");
      $("#CHG_MEMBER_ID").focus();
      return;
    }

		var ls_oldid = OLD_MEMBER_ID.value;
		var ls_id = CHG_MEMBER_ID.value;

		if(_X.MsgBoxYesNo("확인", "사번을 변경 합니다.\n" +
															"\n현대 사번 : " + ls_oldid +
															"\n변경후 사번 : " + ls_id +
															"\n\n계속 하시겠습니까?") == 2) {
			return ;
		}


		// db에서 체크
		var li_dupcnt = _X.XmlSelect("ho", "HO03010", "MEMBER_DUP_CHK", [ls_id], "array");
		if(li_dupcnt > 0) {
			_X.MsgBox('확인', '사번 : ' + ls_id + '\n이미 등록되어 있습니다.');
			return;
		}

		// 변경처리
		var ll_rtn = _X.ES("ho", "HO03010", "MEMBER_ID_CHANGE", [ls_oldid, ls_id]);
		if ( ll_rtn < 0){
			_X.MsgBox('처리중 오류가 발생했습니다.\n오류가 지속되면 시스템 담당자에게 문의해 주세요.');
			return;
		}

		setTimeout("x_DAO_Retrieve()",100);
    $('#changeModal2').modal('hide');
  });

}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {

		//직급
		var ls_GUBUN_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0103", 'Y'), 'json2');
		_X.DDLB_SetData(S_GUBUN_DIV, ls_GUBUN_DIV, null, false, true);
		_X.DDLB_SetData(GUBUN_DIV, ls_GUBUN_DIV, null, false, false);
		dg_1.SetCombo("GUBUN_DIV", ls_GUBUN_DIV);

		//재직여부
		var ls_WORK_YN = [{"code":"Y","label":"재직중"},{"code":"N","label":"퇴직"}];
		_X.DDLB_SetData(S_WORK_YN, ls_WORK_YN, "Y", false, true);

		// 장애등급
		var ls_HANDICAP_LEVEL = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0145", 'Y'), 'json2');
		dg_1.SetCombo("HANDICAP_LEVEL", ls_HANDICAP_LEVEL);

		var ls_MEMBER_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0102", 'Y'), 'json2');
		_X.DDLB_SetData(MEMBER_DIV, ls_MEMBER_DIV, null, false, false);

		var ls_LICENSELEVEL_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0104", 'Y'), 'json2');
		_X.DDLB_SetData(LICENSELEVEL_DIV, ls_LICENSELEVEL_DIV, null, false, false);

		var ls_BANK_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0100", 'Y'), 'json2');
		_X.DDLB_SetData(BANK_DIV, ls_BANK_DIV, null, false, false);

		var ls_AUTHORITY_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("9999", 'Y'), 'json2');
		_X.DDLB_SetData(AUTHORITY_DIV, ls_AUTHORITY_DIV, null, false, false);

		var ls_PAYTYPE = _X.XmlSelect("com", "COMMON", "SM_COMCODE_D" , ['SM','PAYTYPE'], 'json2');
		_X.DDLB_SetData(PAYTYPE, ls_PAYTYPE, null, false, false);

		var ls_RETIRECALCTYPE = _X.XmlSelect("com", "COMMON", "SM_COMCODE_D" , ['SM','RETIRECALCTYPE'], 'json2');
		_X.DDLB_SetData(RETIRECALCTYPE, ls_RETIRECALCTYPE, null, false, false);

		var ls_LIINSURANCETYPE = _X.XmlSelect("com", "COMMON", "SM_COMCODE_D" , ['SM','LIINSURANCETYPE'], 'json2');
		_X.DDLB_SetData(LIINSURANCETYPE, ls_LIINSURANCETYPE, null, false, false);
		dg_4.SetCombo("TYPE", ls_LIINSURANCETYPE);

		var ls_HEALTHINSURETAG = _X.XmlSelect("com", "COMMON", "SM_COMCODE_D" , ['SM','HEALTHINSURETAG'], 'json2');
		_X.DDLB_SetData(HEALTHINSURETAG, ls_HEALTHINSURETAG, null, false, false);

		var ls_NATIONALPENSIONTAG = _X.XmlSelect("com", "COMMON", "SM_COMCODE_D" , ['SM','NATIONALPENSIONTAG'], 'json2');
		_X.DDLB_SetData(NATIONALPENSIONTAG, ls_NATIONALPENSIONTAG, null, false, false);

		var ls_GOYONGTAG = _X.XmlSelect("com", "COMMON", "SM_COMCODE_D" , ['SM','GOYONGTAG'], 'json2');
		_X.DDLB_SetData(GOYONGTAG, ls_GOYONGTAG, null, false, false);
	
		var ls_PAGE_TAG = _X.XmlSelect("com", "COMMON", "SM_COMCODE_D" , ['SM','PAGE_TAG'], 'json2');
		_X.DDLB_SetData(PAGE_TAG, ls_PAGE_TAG, null, false, false);
	
		var ls_rtn = _X.XmlSelect("com", "COMMON", "GET_AUTHORITY_DIV", [mytop._UserID], "array")[0][0];
		if(ls_rtn != "9800" && ls_rtn != "9900" ) {
			_X.FormSetDisable([AUTHORITY_DIV, AUTHORITY_DIV, PAGE_TAG, LOGIN_LOCK_YESNO], true);
		}


		setTimeout("x_DAO_Retrieve()",100);
	}
}

function x_DAO_Retrieve2(a_dg){
 	dg_1.Retrieve([S_DEPT_ID.value, S_GUBUN_DIV.value, S_WORK_YN.value, S_FIND.value]);
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id) {
		case	"S_DEPT_ID":
		case	"S_GUBUN_DIV":
		case	"S_WORK_YN":
		case	"S_FIND":
					x_DAO_Retrieve();
					break;
		case	"MEMBER_ID":
					var ls_replace = _X.StrPurify(a_val);
					dg_1.SetItem(dg_1.GetRow(),"MEMBER_ID",ls_replace);
					dg_1.SetItem(dg_1.GetRow(),"EMP_NO",ls_replace);
					break;
		case	"INPUT_PWD":
					dg_1.SetItem(dg_1.GetRow(),"PWD", MD5(a_val));
					break;
		case	"INPUT_RRN_NO":
					var ls_result = _X.XmlSelect("com", "COMMON", "GET_ENCRYPT", [a_val], "array")[0][0];
					dg_1.SetItem(dg_1.GetRow(), 'RRN_NO', ls_result);
					dg_1.SetItem(dg_1.GetRow(), 'REG_CODE', a_val.substr(0,7)+dg_1.GetItem(dg_1.GetRow(), "MEMBER_ID").substr(4, 6));//, 6, '0').substr(4, 6);
					break;
	}

}


function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){

	var cData	= dg_1.GetChangedData();
	for (i=0 ; i < cData.length; i++){
		if(cData[i].job == 'D') continue;

		if(cData[i].job == 'I'){
			var ls_id = dg_1.GetItem(cData[i].idx, "MEMBER_ID");

			// 그리드에서 체크
			if(dg_1.GetDuplicateRowByValue(ls_id, "MEMBER_ID", cData[i].idx) > 0){
				_X.MsgBox('확인', '사번 : ' + ls_id + '\n이미 등록되어 있습니다.');
				dg_1.SetRow(cData[i].idx);
				return false;
			}

			// db에서 체크
			var li_dupcnt = _X.XmlSelect("ho", "HO03010", "MEMBER_DUP_CHK", [ls_id], "array");
			if(li_dupcnt > 0) {
				_X.MsgBox('확인', '사번 : ' + ls_id + '\n이미 등록되어 있습니다.');
				dg_1.SetRow(cData[i].idx);
				return false;
			}

		}
	}

	return true;
}

//저장후 호출
function x_DAO_Saved2(){
	if(dg_1.GetItem(dg_1.GetRow(), "DEPT_ID")=="01001") {
		_X.EP("gs", "PR_GS_DEPT_AUTH", [dg_1.GetItem(dg_1.GetRow(), "MEMBER_ID")]);
	}
}

function x_DAO_Insert2(a_dg, row){
  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
	if(a_dg.id == 'dg_1') {
		dg_1.SetItem(rowIdx, "WORK_YN"    , "Y");
		dg_1.SetItem(rowIdx, "HANDICAP_YN", "N");
		dg_1.SetItem(rowIdx, "REG_DATE"   , _X.ToString(_X.GetSysDate(), "yyyy-mm-dd"));

		if(S_DEPT_ID.value != '%') {
			dg_1.SetItem(rowIdx, "DEPT_ID"  , S_DEPT_ID.value);
			dg_1.SetItem(rowIdx, "DEPT_NAME", S_DEPT_ID.options[S_DEPT_ID.selectedIndex].text);
		}
	}
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;	// 템플릿의 하위 로직을 수행하지않고 Pass
	//return 1;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	xe_BodyResize();
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
	if(a_dg.id == 'dg_1') {
		pf_setPicture();
		var ls_regcode  = dg_1.GetItem(a_newrow, "REG_CODE");
		var ls_memberid = dg_1.GetItem(a_newrow, "MEMBER_ID");

		dg_3.Retrieve([ls_regcode]);
		dg_4.Retrieve([ls_memberid]);
	}
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
	if(a_dg.id == 'dg_1') {
		uf_Set_Color_Member(a_dg);
	}
}

function uf_Set_Color_Member(a_dg) {
	var s_blu = { "color": "#0000FF" };
	var s_blk = { "color": "#000000" };

	for(var i = 1; i <= a_dg.RowCount(); i++){
		var ls_dept = a_dg.GetItem(i, "DUP_DEPT_NAME");
		a_dg.SetCellStyle(i, "NAME"    , ( ls_dept == "" ) ? s_blk : s_blu);
	}
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
	switch(_FindCode.finddiv) {
		case	"ZIP_CODE" :
					var ll_row = dg_1.GetRow();
					dg_1.SetItem(ll_row, "ZIP_CODE"   , a_retVal.zipNo);	// 주소찾이만 이런형식으로 받음
					dg_1.SetItem(ll_row, "ADDR1"      , a_retVal.roadAddrPart1 + a_retVal.roadAddrPart2);
					dg_1.SetItem(ll_row, "ADDR2"      , a_retVal.addrDetail);
					ADDR2.focus();
					break;
	}

	if(_FindCode.returnValue == null) return;

	switch(_FindCode.finddiv) {
		case	"S_DEPT_ID" :
					S_DEPT_ID.value = _FindCode.returnValue.DEPT_ID;
					x_DAO_Retrieve();
					break;
		case	"DEPT_ID" :
					var ll_row = dg_1.GetRow();
					dg_1.SetItem(ll_row, "DEPT_ID"  , _FindCode.returnValue.DEPT_ID);
					dg_1.SetItem(ll_row, "DEPT_NAME", _FindCode.returnValue.DEPT_NAME);
					break;
	}
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

// 지사찾기
function uf_findDept(a_param) {
	_X.FindDeptCode2(a_param,'');
}


//우편번호찾기
function uf_ZipFind(a_param) {
	if(dg_1.RowCount() == 0) return;
	_X.FindStreetAddr(window,a_param,"", "");
}

// 주민번호체크
function uf_chk_jumin() {
	var li_row = dg_1.GetRow();
  var ls_memberid = _X.Trim(dg_1.GetItem(li_row,'MEMBER_ID'));
  var ls_regcode = _X.StrPurify(_X.Trim(dg_1.GetItem(li_row,'REG_CODE')));
  var ls_jumin = _X.StrPurify(_X.Trim(dg_1.GetItem(li_row,'INPUT_RRN_NO')));
  var ls_results = _X.XmlSelect("ho", "HO03010", "CHK_JUMIN", [ls_jumin, ls_memberid], "array");

  //if(ls_jumin.length == 13 || ls_jumin.length == 10) {
  if(ls_jumin.length == 13) {
	  if(ls_results!=null && ls_results!="" && ls_results.length>0) {
	  	var ls_result = "";
	  	for(var i=0; i<ls_results.length; i++) {
	  		ls_result += ls_results[i][2] + " " + ls_results[i][1] + "\n"
	  	}
	  	_X.MsgBox("중복된 데이타가 존재합니다.\n" + ls_result);
	  } else {
	    _X.MsgBox("중복된 주민번호가 없습니다.");
 	  }
  } else {
    _X.MsgBox("주민번호 자릿수를 확인해 주시기 바랍니다.");
  }

}

// 보험가입만료
function uf_insurend() {
	var li_row = dg_1.GetRow();
	var ls_sdate = _X.ToString(dg_1.GetItem(li_row, 'LIINSURANCEDATE'),"YYYYMMDD");
	var ls_edate = _X.ToString(dg_1.GetItem(li_row, 'LIINSURANCEENDDATE'),"YYYYMMDD");
	var ls_etype = dg_1.GetItem(li_row,'LIINSURANCETYPE') ;
	var ls_member = _X.Trim(dg_1.GetItem(li_row,'MEMBER_ID'));

	if(ls_sdate == null || ls_sdate == '') {
		 _X.MsgBox("배상보험 시작일을 입력해주십시요.");
		 return 100;
	} else if(ls_edate == null || ls_edate == '') {
		 _X.MsgBox("배상보험 종료일을 입력해주십시요.");
		 return 100;
	} else {
		var ll_rtn = _X.ES("ho", "HO03010", "CODE_LIINSURANCE_C01", [ls_member, ls_sdate, ls_edate, ls_etype, mytop._UserID, mytop._ClientIP]);
		if ( ll_rtn >= 0){
			dg_1.SetItem(li_row, 'LIINSURANCEDATE', '');
			dg_1.SetItem(li_row, 'LIINSURANCEENDDATE', '')

			dg_4.Retrieve([ls_member]);
			_X.MsgBox('저장되었습니다');
		}
	}

}

// 사진 셋팅 =============================================================================================
function pf_setPicture(){
	var empPhoto = dg_1.GetItem(dg_1.GetRow(), 'PIMG');

	if ( empPhoto != "" ) {
		$("#EMP_PHOTO").attr("src", mytop._rootFileUpload +  dg_1.GetItem(dg_1.GetRow(), 'PIMG_PATH'));
	} else {
		$("#EMP_PHOTO").attr("src", mytop._CPATH +"/Theme/images/noimage.jpg");
	}
}

function pf_ImageUpload(a_obj) {
	if(dg_1.RowCount() <= 0) {
		_X.MsgBox("선택된 데이타가 없습니다.\r\n데이타 조회 후 다시 시도하여 주십시오.");
		return;
	}
		var fileid =  dg_1.GetItem(dg_1.GetRow(), "PIMG");
		_X.FileUpload(1, 'HO', "PIMG", fileid,false);
}

//image uploaded callback
function x_FileUploaded(obj, ReturnValue) {
	if(ReturnValue==null){
		return;
	}
	switch(obj) {
		case "PIMG":
			dg_1.SetItem(dg_1.GetRow(), "PIMG", ReturnValue[0].atchFileId);
			dg_1.SetItem(dg_1.GetRow(), "PIMG_PATH", ReturnValue[0].fileStreCours + "/" + ReturnValue[0].streFileNm);
			break;
	}

	dg_1.Save(null, null, "Y");
	d = new Date();
	$("#EMP_PHOTO").attr("src", mytop._rootFileUpload + ReturnValue[0].fileStreCours + "/" + ReturnValue[0].streFileNm + "?"+d.getTime());
}

//파일삭제
function x_FileDeleted(obj) {
	switch(obj) {
		case "PIMG":
			dg_1.SetItem(dg_1.GetRow(),"PIMG","");
			dg_1.SetItem(dg_1.GetRow(),"PIMG_PATH","\\");
		  break;
	}
	dg_1.Save(null, null, "N");

	$("#EMP_PHOTO").attr("src", mytop._CPATH+"/Theme/images/noimage.jpg" + "?" + new Date().getTime());
}


//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : [HO01020] 서비스품목관리
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
var is_working = false;

function x_InitForm2(){
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "ho", "HO01020", "HO01020|HO01020_C01", false, true);
	_X.InitGrid(grid_excel, "dg_excel", "100%", "100%", "ho", "HO01020", "HO01020|HO01020_C01", false, false);
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		//품목구분
		var ls_PRODUCT = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0128", 'Y'), 'json2');
		_X.DDLB_SetData(S_PRODUCT, ls_PRODUCT, null, false, true);
		dg_1.SetCombo("PRODUCT_DIV", ls_PRODUCT);
		dg_excel.SetCombo("PRODUCT_DIV", ls_PRODUCT);

		//사용여부
		var ls_USEYN = [{"code":"Y","label":"사용"},{"code":"N","label":"미사용"}];
		_X.DDLB_SetData(S_USEYN, ls_USEYN, "Y", false, true);

		// 장기요양긍급
		var la_CUST_LEVEL_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0125", 'Y'), 'json2');
		dg_1.SetCombo("CUST_LEVEL_DIV", la_CUST_LEVEL_DIV);
		dg_excel.SetCombo("CUST_LEVEL_DIV", la_CUST_LEVEL_DIV);

		setTimeout("x_DAO_Retrieve()",100);
	}
}

function x_DAO_Retrieve2(a_dg){
 	dg_1.Retrieve([S_PRODUCT.value, S_USEYN.value, _X.StrPurify(sle_basicdate.value)]);
}

function xe_EditChanged2(a_obj, a_val){
	// switch(a_obj.id) {
	// 	case	"S_DEPT_ID":
	// 	case	"sle_basicmonth":
	// 				x_DAO_Retrieve();
	// 				break;
	// }

	x_DAO_Retrieve();
}


function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	var cData1 	= dg_1.GetChangedData();

	if(cData1.length > 0){
		for (i=0 ; i < cData1.length; i++){
			if(cData1[i].job == 'I'){
				var ls_product = _X.XmlSelect("ho", "HO01020", "GetDataProductId", [], "array")[0][0];
	      dg_1.SetItem(cData1[i].idx, "PRODUCT_ID", ls_product);
			}
		}
	}

	return true;
}

//저장후 호출
function x_DAO_Saved2(){
	is_working = false;
}

function x_DAO_Insert2(a_dg, row){
	if (is_working) return 0;
	is_working = true;

  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
	if(a_dg.id == 'dg_1') {
		dg_1.SetItem(rowIdx, "PROOFFI_DIV", "O");
		dg_1.SetItem(rowIdx, "PSEP_DIV"   , "S");
		dg_1.SetItem(rowIdx, "USE_YN"     , "Y");
		dg_1.SetItem(rowIdx, "ROUND_YN"   , "Y");
		dg_1.SetItem(rowIdx, "FAMILY_YN"  , "N");
	}
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	if (is_working) return 0;
	is_working = true;

	return 100;
}

function x_DAO_Excel2(a_dg){
	dg_excel.Retrieve([S_PRODUCT.value, S_USEYN.value, _X.StrPurify(sle_basicdate.value)]);
	dg_excel.ExcelExport(true);

	return 0;
	//return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
}


function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

function uf_ExcelUpload(){
	// 엑셀임시파일삭제
	_X.ES("com", "COMMON", "XM_EXCEL_DATA_D01", ["HO01020", mytop._UserID]);

	dg_1.ExcelUpload("ho", function (args) {
		_X.EP("ho", "SP_CODE_PRODUCT_UPLOAD", ["HO01020", mytop._UserID, mytop._ClientIP], function (res) {
			if ( res != -1 ) {
				_X.MsgBox("엑셀데이타 업로드 완료.");
				x_DAO_Retrieve();
			}
			else {
				_X.MsgBox("엑셀데이타 업로드 중 오류가 발생 하였습니다.");
			}
		});
	},
	null, 2, "HO01020", false);
}

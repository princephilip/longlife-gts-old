//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : [HO03020] 센터관리
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
function x_InitForm2(){
	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "ho", "HO03020", "HO03020|HO03020_C01", true,  true);
	_X.InitGrid(grid_3, "dg_3", "100%", "100%", "ho", "HO03020", "HO03020|HO03020_C03", false, true);
	_X.InitGrid(grid_4, "dg_4", "100%", "100%", "ho", "HO03020", "HO03020|HO03020_C04", false, true);
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		//
		var ls_USE_YN = [{"code":"Y","label":"사업중"},{"code":"N","label":"폐업"}];
		_X.DDLB_SetData(S_USE_YN, ls_USE_YN, "Y", false, true);
		_X.DDLB_SetData(USE_YN, ls_USE_YN, null, false, false);
		dg_1.SetCombo("USE_YN", ls_USE_YN);

		//직영구분
		var ls_OPER_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0126", 'Y'), 'json2');
		_X.DDLB_SetData(OPER_DIV, ls_OPER_DIV, null, false, false);
		dg_1.SetCombo("OPER_DIV", ls_OPER_DIV);

		// // 시설종류
		// var ls_DEPT_DIV = [{"code":"","label":"선택"},{"code":"방문요양","label":"방문요양"},{"code":"방문목욕","label":"방문목욕"},{"code":"주야간보호","label":"주야간보호"}];
		// _X.DDLB_SetData(DEPT_DIV, ls_DEPT_DIV, null, false, false);

		//은행
		var ls_BANK_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0100", 'Y'), 'json2');
		dg_3.SetCombo("BANK_DIV", ls_BANK_DIV);

		//계좌용도
		var ls_DEPOSIT_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0101", 'Y'), 'json2');
		dg_3.SetCombo("DEPOSIT_DIV", ls_DEPOSIT_DIV);

		setTimeout("x_DAO_Retrieve()",100);
	}
}

function x_DAO_Retrieve2(a_dg){
	dg_3.Reset();
	dg_4.Reset();
 	dg_1.Retrieve(['%', S_USE_YN.value]);
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id) {
		case	"S_USE_YN":
					x_DAO_Retrieve();
					break;
	}

}


function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	if(a_obj.id == 'S_FIND'){
		if(S_FIND.value != ""){
			var li_fRow =  dg_1.SetFocusByElement('S_FIND',['DEPT_ID','DEPT_NAME'], false);
			if(li_fRow > 0) {
				setTimeout("dg_1.SetRow(" + li_fRow.toString() + ")",100);
			}
		}
	}
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

//저장후 호출
function x_DAO_Saved2(){
}

function x_DAO_Insert2(a_dg, row){
	if(a_dg.id == 'dg_3') {
		if(dg_1.RowCount() < 1) return 0;
	}

	if(a_dg.id == 'dg_4') {
		if(dg_1.RowCount() < 1) return 0;

		var ls_dept = dg_1.GetItem(dg_1.GetRow(), "DEPT_ID");
		_X.SelectEmp(window,"G_EMP", '', [ls_dept]);
		return 0;
	}

  return 100;
}


function x_Insert_After2(a_dg, rowIdx){
	if(a_dg.id == 'dg_1') {
		dg_1.SetItem(rowIdx, "USE_YN", "Y");
	}

	if(a_dg.id == 'dg_3') {
		dg_3.SetItem(rowIdx, "DEPT_ID", dg_1.GetItem(dg_1.GetRow(), "DEPT_ID"));
	}

	if(a_dg.id == 'dg_4') {
		dg_4.SetItem(rowIdx, "DEPT_ID", dg_1.GetItem(dg_1.GetRow(), "DEPT_ID"));
	}
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
	if(a_dg.id == 'dg_1') {
		dg_1.SetItem(rowIdx, "DEPT_ID", "");
	}
}

function x_DAO_Delete2(a_dg, row){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;	// 템플릿의 하위 로직을 수행하지않고 Pass
	//return 1;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	xe_BodyResize();
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
	if(a_dg.id == 'dg_1') {
		var ls_dept = dg_1.GetItem(a_newrow, "DEPT_ID");

		dg_3.Retrieve([ls_dept]);
		dg_4.Retrieve([ls_dept]);
	}
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
	switch(_FindCode.finddiv) {
		case	"ZIP_CODE" :
					var ll_row = dg_1.GetRow();
					dg_1.SetItem(ll_row, "ZIP_CODE"   , a_retVal.zipNo);	// 주소찾이만 이런형식으로 받음
					dg_1.SetItem(ll_row, "ADDR1"      , a_retVal.roadAddrPart1 + a_retVal.roadAddrPart2);
					dg_1.SetItem(ll_row, "ADDR2"      , a_retVal.addrDetail);
					ADDR2.focus();
					break;
	}

	if(_FindCode.returnValue == null) return;

	switch(_FindCode.finddiv) {
		case	"S_DEPT_ID" :
					S_DEPT_ID.value = _FindCode.returnValue.DEPT_ID;
					x_DAO_Retrieve();
					break;
		case	"PROXY_DEPTID" :
					var ll_row = dg_1.GetRow();
					dg_1.SetItem(ll_row, "PROXY_DEPTID"  , _FindCode.returnValue.DEPT_ID);
					dg_1.SetItem(ll_row, "PROXY_DEPTNAME", _FindCode.returnValue.DEPT_NAME);
					break;
		case	"UPPER_DEPTID" :
					var ll_row = dg_1.GetRow();
					dg_1.SetItem(ll_row, "UPPER_DEPTID"  , _FindCode.returnValue.DEPT_ID);
					dg_1.SetItem(ll_row, "UPPER_DEPTNAME", _FindCode.returnValue.DEPT_NAME);
					break;
	}
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

// 지사찾기
function uf_findDept(a_param) {
	_X.FindDeptCode(a_param,'');
}

// 물류센터찾기
function uf_FindSalecenterCode(a_param) {
	_X.FindSalecenterCode(a_param,'');
}


//우편번호찾기
function uf_ZipFind(a_param) {
	if(dg_1.RowCount() == 0) return;
	_X.FindStreetAddr(window,a_param,"", "");
}


// 사용자추가
function pf_addcall(a_dg){
	var cRows = a_dg.GetCheckedRows();

	var ls_dept = dg_1.GetItem(dg_1.GetRow(), "DEPT_ID");

  for(var i=0;i<cRows.length;i++){

  	var ar_col = new Array();
  	var ar_val = new Array();
  	ar_col.push("MEMBER_ID");
  	ar_val.push(String(a_dg.GetItem(cRows[i], "MEMBER_ID")));

  	var res = dg_4.GetDuplicateRowByKey(ar_val, ar_col);
  	if(res > 0) continue;

  	newrow = dg_4.InsertRow(0);
    dg_4.SetItem(newrow, "DEPT_NAME", a_dg.GetItem(cRows[i], "DEPT_NAME"));
    dg_4.SetItem(newrow, "MEMBER_ID", a_dg.GetItem(cRows[i], "MEMBER_ID"));
    dg_4.SetItem(newrow, "NAME"     , a_dg.GetItem(cRows[i], "MEMBER_NAME"));
  	dg_4.SetItem(newrow, "DEPT_ID"  , ls_dept);
  }
}


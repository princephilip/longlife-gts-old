//========================================================================================
// Copyright ⓒ X-Internet Info.. All Rights Reserved!!!
// Contact www.x-web.co.kr for more Information. Don't remove this message. Thank You!!!
//========================================================================================
// 프로그램명 : [HO03040] 사용자-센터권한설정
// --------------------------------------- 변경이력 --------------------------------------
//   작 성 자           소  속          작 성 일               비   고
// ------------    -----------------   ----------    -------------------------------------
//	    						X-INTERNETINFO
//
//========================================================================================
//
//========================================================================================
function x_InitForm2(){
	if(!_X.IsHeadoffice())	{
		$("#btn_finddept").hide();
	}

	_X.InitGrid(grid_1, "dg_1", "100%", "100%", "ho", "HO03040", "HO03040|HO03040_R01", false, false);
	_X.InitGrid(grid_2, "dg_2", "100%", "100%", "ho", "HO03040", "HO03040|HO03040_C02", false, true);

	var la_DEPT = _X.XmlSelect("com", "COMMON", "CODE_DEPT_AUTHORITY_R01" , new Array(mytop._UserID), 'json2');
	_X.DDLB_SetData(S_DEPT_ID, la_DEPT, null, true, (_X.IsHeadoffice() ? true : false));
	S_DEPT_ID.value = mytop._DeptCode;
}

function xe_GridLayoutComplete2(a_dg, ab_allcompleted){
	if(ab_allcompleted) {
		//재직여부
		var ls_WORK_YN = [{"code":"Y","label":"재직중"},{"code":"N","label":"퇴직"}];
		_X.DDLB_SetData(S_WORK_YN, ls_WORK_YN, "Y", false, true);

		//직급
		var ls_GUBUN_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0103", 'Y'), 'json2');
		dg_1.SetCombo("GUBUN_DIV", ls_GUBUN_DIV);

		//정임직구분
		var ls_MEMBER_DIV = _X.XmlSelect("com", "COMMON", "CODE_COMDIV" , new Array("0102", 'Y'), 'json2');
		dg_1.SetCombo("MEMBER_DIV", ls_MEMBER_DIV);

		setTimeout("x_DAO_Retrieve()",100);
	}
}

function x_DAO_Retrieve2(a_dg){
 	dg_1.Retrieve([S_DEPT_ID.value, S_WORK_YN.value]);
}

function xe_EditChanged2(a_obj, a_val){
	switch(a_obj.id) {
		case	"S_DEPT_ID":
		case	"S_WORK_YN":
					x_DAO_Retrieve();
					break;
	}

}


function xe_InputKeyEnter2(a_obj, a_event, a_ctrlKey, a_altKey, a_shiftKey){
	if(a_obj.id == 'S_FIND'){
		if(S_FIND.value != ""){
			var li_fRow =  dg_1.SetFocusByElement('S_FIND',['MEMBER_ID','NAME'], false);
			if(li_fRow > 0) {
				setTimeout("dg_1.SetRow(" + li_fRow.toString() + ")",100);
			}
		}
	}
}

function xe_InputKeyDown2(a_obj, a_event, a_keyCode, a_ctrlKey, a_altKey, a_shiftKey){}

function x_DAO_Save2(a_dg){
	return 100;
}

function x_DAO_ChkErr2(){
	return true;
}

//저장후 호출
function x_DAO_Saved2(){
}

function x_DAO_Insert2(a_dg, row){
	_X.SelectDept(window,"G_DEPT", '', []);

	return 0;
  //return 100;
}


function x_Insert_After2(a_dg, rowIdx){
	// if(a_dg.id == 'dg_2') {
	// 	var li_max = dg_2.GetTotalValue("PRINT_SEQ", "MAX");

	// 	dg_2.SetItem(rowIdx, "MEMBER_ID", dg_1.GetItem(dg_1.GetRow(), "MEMBER_ID"));
	// 	dg_2.SetItem(rowIdx, "PRINT_SEQ", li_max+1);
	// }
}

function x_DAO_Duplicate2(a_dg, row){
	return 100;
}

function x_Duplicate_After2(a_dg, rowIdx){
}

function x_DAO_Delete2(a_dg, row){
	return 100;
}

function x_DAO_Excel2(a_dg){
	return 100;
}

function x_DAO_Print2(){
}

function xe_TabChanging2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	return 100;	// 템플릿의 하위 로직을 수행하지않고 Pass
	//return 1;
}

function xe_TabChanged2(a_tab, a_new_idx, a_old_idx, a_new_tab, a_old_tab){
	xe_BodyResize();
}

function xe_GridDataChanged2(a_dg, a_row, a_col, a_newvalue, a_oldvalue){
}

function xe_GridRowFocusChange2(a_dg, a_newrow, a_oldrow){
	return true;
}

function xe_GridRowFocusChanged2(a_dg, a_newrow){
	if(a_dg.id == 'dg_1') {
		var ls_memberid = dg_1.GetItem(a_newrow, "MEMBER_ID");
		dg_2.Retrieve([ls_memberid]);
	}
}

function xe_GridItemFocusChange2(a_dg, a_newrow, a_newcol, a_oldrow, a_oldcol){
}

function xe_GridScrollToBottom2(a_dg){}

function xe_GridDataLoad2(a_dg){
}

function xe_GridButtonClick2(a_dg, a_row, a_colname, a_col){
}

function x_ReceivedCode2(a_retVal){
	switch(_FindCode.finddiv) {
		case	"S_DEPT_ID" :
					S_DEPT_ID.value = _FindCode.returnValue.DEPT_ID;
					x_DAO_Retrieve();
					break;
	}
}

function xe_GridHeaderClick2(a_dg, a_col, a_colname){}
function xe_GridItemDoubleClick2(a_dg, a_row, a_col, a_colname){
}

function xe_GridItemChecked2(a_dg, a_itemIndex, a_checked){}
function xe_GridItemClick2(a_dg, a_row, a_col, a_colname){}
function xe_TreeItemExpanding2(a_dg, a_itemIndex, a_rowId){}
function xe_ChartPointClick2(a_dg, a_event){}
function xe_ChartPointSelect2(a_dg, a_event){}
function xe_ChartPointMouseOver2(a_dg){}
function xe_ChartPointMouseOut2(a_dg, a_event){}

function x_Close2(){
	return 100;
}

// 지사찾기
function uf_findDept(a_param) {
	_X.FindDeptCode(a_param,'');
}


function pf_addcall(a_dg){
	var cRows = a_dg.GetCheckedRows();

	var li_max = dg_2.GetTotalValue("PRINT_SEQ", "MAX");
	var ls_member = dg_1.GetItem(dg_1.GetRow(), "MEMBER_ID");

  for(var i=0;i<cRows.length;i++){

  	var ar_col = new Array();
  	var ar_val = new Array();
  	ar_col.push("DEPT_ID");
  	ar_val.push(String(a_dg.GetItem(cRows[i], "DEPT_ID")));

  	var res = dg_2.GetDuplicateRowByKey(ar_val, ar_col);
  	if(res > 0) continue;

  	newrow = dg_2.InsertRow(0);

		li_max += 1;
  	dg_2.SetItem(newrow, "MEMBER_ID", ls_member);
    dg_2.SetItem(newrow, "DEPT_ID"  , a_dg.GetItem(cRows[i], "DEPT_ID"));
    dg_2.SetItem(newrow, "DEPT_NAME", a_dg.GetItem(cRows[i], "DEPT_NAME"));
    dg_2.SetItem(newrow, "PRINT_SEQ", li_max);
  }
}

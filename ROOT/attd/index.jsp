<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<%@ page import ="xgov.core.vo.XLoginVO" %>
<%@ page import ="org.springframework.util.StopWatch" %>
<%@ taglib prefix="html"  uri="/WEB-INF/tlds/mighty.tld" %>
<%
 /**
  * @JSP Name : XG010.jsp
  * @Description : 기본 페이지 구조
  * @Modification Information
  * 
  *   수정일     수정자         수정내용
  *  ----------  --------       ---------------------------
  *  2012.12.01  김양열         최초 생성
  *  2015.06.08  SKLEE          SLICKGRID 버전으로 변경
  *
  * author (주)엑스인터넷정보 
  * Copyright (C) 2015 by X-Internet Info.  All right reserved.
  */
%>

<jsp:include page="/Mighty/include/COMM_JS.jsp"/>

<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>세방테크 출력 모니터링 시스템</title>

    <!-- Bootstrap -->
    <link href="/Mighty/3rd/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/Mighty/3rd/gentelella/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/Mighty/3rd/gentelella/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="/Mighty/3rd/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="/Mighty/3rd/gentelella/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="/Mighty/3rd/gentelella/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="/Mighty/3rd/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="./attd.css" rel="stylesheet">


    <!-- jQuery -->
    <script src="/Mighty/3rd/gentelella/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/Mighty/3rd/gentelella/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="/Mighty/3rd/gentelella/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/Mighty/3rd/gentelella/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="/Mighty/3rd/gentelella/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="/Mighty/3rd/gentelella/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="/Mighty/3rd/gentelella/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="/Mighty/3rd/gentelella/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="/Mighty/3rd/gentelella/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="/Mighty/3rd/gentelella/vendors/Flot/jquery.flot.js"></script>
    <script src="/Mighty/3rd/gentelella/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="/Mighty/3rd/gentelella/vendors/Flot/jquery.flot.time.js"></script>
    <script src="/Mighty/3rd/gentelella/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="/Mighty/3rd/gentelella/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="/Mighty/3rd/gentelella/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="/Mighty/3rd/gentelella/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="/Mighty/3rd/gentelella/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="/Mighty/3rd/gentelella/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="/Mighty/3rd/gentelella/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="/Mighty/3rd/gentelella/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="/Mighty/3rd/gentelella/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="/Mighty/3rd/gentelella/vendors/moment/min/moment.min.js"></script>
    <script src="/Mighty/3rd/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="./attd.js"></script>

    <!-- fullcalendar  관련 -->
    <link href='/Mighty/3rd/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
    <link href='/Mighty/3rd/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    <script src='/Mighty/3rd/fullcalendar/lib/moment.min.js'></script>
    <script src='/Mighty/3rd/fullcalendar/fullcalendar.min.js'></script>
    <script src='/Mighty/3rd/fullcalendar/locale/ko.js'></script>


  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.jsp" class="site_title"><span>STEP's 출력 정보</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img id="img_profile" src="./kyy.png" alt="..." class="img-circle profile_img" >
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2 id="profile_name">김양열</h2>
                <br>
                <h3 id="team_name">배관팀</h3>
                <br>
                <h2 id="const_name">옥외배관공사</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">

                <h3>일반사항</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i> 출력정보 <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="#">Dashboard</a></li>
                      <li><a href="#">Dashboard2</a></li>
                      <li><a href="#">Dashboard3</a></li>
                    </ul>
                  </li>
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
  
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">

                <div class="row x_title">
                  <div class="col-md-12">

                    <div id='calendar'></div>

                  </div>
                </div>
                
              </div>
            </div>

          </div>

        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>
    </div>

	
  </body>




  <script>

    $(document).ready(function() {

        var date = new Date(),
          d = date.getDate(),
          m = date.getMonth(),
          y = date.getFullYear(),
          started,
          categoryClass;

        var calendar = $('#calendar').fullCalendar({
          header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay,listMonth'
          },
          selectable: true,
          selectHelper: true,
          lang: "ko",
          firstDay: 1,
          navLinks: true, // can click day/week names to navigate views
          businessHours: true, // display business hours
          select: function(start, end, allDay) {
            $('#fc_create').click();

            started = start;
            ended = end;

            $(".antosubmit").on("click", function() {
              var title = $("#title").val();
              if (end) {
              ended = end;
              }

              categoryClass = $("#event_type").val();

              if (title) {
              calendar.fullCalendar('renderEvent', {
                title: title,
                start: started,
                end: end,
                allDay: allDay
                },
                true // make the event "stick"
              );
              }

              $('#title').val('');

              calendar.fullCalendar('unselect');

              $('.antoclose').click();

              return false;
            });
          },
          eventClick: function(calEvent, jsEvent, view) {
            $('#fc_edit').click();
            $('#title2').val(calEvent.title);

            categoryClass = $("#event_type").val();

            $(".antosubmit2").on("click", function() {
              calEvent.title = $("#title2").val();

              calendar.fullCalendar('updateEvent', calEvent);
              $('.antoclose2').click();
            });

            calendar.fullCalendar('unselect');
          },
          editable: true,

          // dayRender: function (date, cell) {
          //   $(cell).css({"background": "url('/attd/kyy.png')", 'background-repeat' : 'no-repeat', 'background-position':'center center'});
          // },

          events: [
          ]
        });

        checkAttd();
         //setTimeout("checkAttd()", 3000);
      
    });

    var _CurLabor = "";

    function checkAttd() {

        var la_attd  = _X.XmlSelect("cm", "CHECKTIME", "CUR_LABOR" , new Array() , 'json2');
        if(_CurLabor != la_attd[0].badgenumber ) {
          _CurLabor = la_attd[0].badgenumber;
          var la_labor = _X.XmlSelect("cm", "CHECKTIME", "CUR_LABOR_INFO" , new Array('100', la_attd[0].badgenumber) , 'json2');


           $("#img_profile").attr("src", "/fileATTD/" + la_attd[0].badgenumber + ".jpg");
           $("#profile_name").html( la_labor[0].labor_name );
           $("#team_name").html( la_labor[0].team_name );
           $("#const_name").html( la_labor[0].const_name );

           $(".fc-widget-content").css({"background": ""});

          for(var i=0; i<la_attd.length; i++) {
            $(".fc-widget-content[data-date='" + la_attd[i].checkdate + "']").css({"background": "url('" + la_attd[i].photo_url  + "')", 'background-repeat' : 'no-repeat', 'background-size' : '80% 80%', 'background-position':'center center'});
          }          
        }
      setTimeout("checkAttd()", 1000);
      //element.find('div').find('div.fc-day-content').addClass('day_hours').text(random);
    }

  </script>


</html>
